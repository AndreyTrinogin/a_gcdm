/******************************************************************
 *                                                                *
 *   unfold.c                                                     *
 *                                                                *
 ******************************************************************
 *                                                                *
 *   Version 9.3.2                                                *
 *   written by JR, modified by Yoginho                           *
 *   -g option by Yousong Wang, Feb 2002                          *
 *   -a option by Marcel Wolf, Apr 2002                           *
 *   -G (guts) option by Manu, June 2002
 *                                                                *
 ******************************************************************
 *                                                                *
 * unfold.c contains main for the unfold utility. unfold runs     *
 * the fly model using the parameters and the maternal contribu-  *
 * tions stored for the specified genotype in the data file       *
 * specified by filename.                                         *
 *                                                                *
 ******************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>                                          /* for getopt */

#include <error.h>
#include <ODESolve.h>
#include <maternal.h>
#include <score.h>
#include <zygotic.h>
#include <integrate.h>
#include <tweak.h>


/*** Constants *************************************************************/

#define  OPTS      ":a:DE:f:F:g:GHhi:j:l:m:oO:p:r:s:t:vX:x:Yz:Z"   /* cmd line opt string */

/*** Help, usage and version messages **************************************/

static const char usage[]  =

"Usage: unfold [-a <accuracy>] [-D] [-f <float_prec>] [-g <g(u)>] [-G]\n"
"              [-h] [-i <stepsize>] [-j <timefile>] [-o] [-p <pstep>]\n"
"              [-s <solver>] [-t <time>] [-v] [-x <sect_title>]\n"
"              [-z <gast_time>]\n"
"              <datafile> [<genotype>]\n";

static const char help[]   =

"Usage: unfold [options] <datafile>\n\n"

"Arguments:\n"
"  <datafile>          data file for which we evaluate score and RMS\n\n"

"Options:\n"
"  -A <cost flag>      msq or abs\n"
"  -a <accuracy>       solver accuracy for adaptive stepsize ODE solvers\n"
"  -d                  print details\n"
"  -D                  debugging mode, prints all kinds of debugging info\n"
"  -E <index of input> switch off external input, can be given multiple times for several inputs\n"
"  -f <float_prec>     float precision of output is <float_prec>\n"
"  -F <index of gene>  switch off diffusion for this gene, can be given multiple times for several genes\n"
"  -g <g(u)>           chooses g(u): e = exp, h = hvs, s = sqrt, t = tanh\n"
"  -G                  gut mode: prints squared diffs for all datapoints\n"
"  -h                  prints this help message\n"
"  -i <stepsize>       sets ODE solver stepsize (in minutes)\n"
"  -j <timefile>       \n"
"  -l <scale   >       \n"
"  -n                  print number of datapoints\n"
"  -o                  use oldstyle cell division times (3 div only)\n"
"  -O <model>          hyb, simple, nomit or double\n"
"  -p                  prints penalty in addition to score and RMS\n"
"  -s <solver>         selects solver\n"
"  -v                  print version and compilation date\n"
"  -x <sect_title>     uses equation paramters from section <sect_title>\n\n"
"  -X <equations>      selects equations\n\n"
"  -z <gast_time>      set custom gastrulation time (max. 10'000 (min));\n"
"                      custom gastrulation MUST be later than the normal one\n"

"Please report bugs to <kozlov@spbcas.ru>. Thank you!\n";

static const char verstring[] =

"%s version %s\n"
"compiled by:      %s\n"
"         on:      %s\n"
"      using:      %s\n"
"      flags:      %s\n"
"       date:      %s at %s\n";

static int extra_file_flag = 0;

/*** Main program **********************************************************/

int main(int argc, char **argv)
{
	char            *infile;                   /* pointer to input file name */
	FILE            *fp;                       /* pointer to input data file */
	int             c;                 /* used to parse command line options */
	int i;                                       /* loop counter */
	int numguts;           /* number of guts elements calculated */
	char            *dumpfile;             /* name of debugging model output */
	FILE            *dumpptr;        /* pointer for dumping raw model output */
	char            *slogfile;                    /* name of solver log file */
	FILE            *slog;                        /* solver log file pointer */
/* the follwoing few variables are read as arguments to command line opts */
	int             ndigits  = 6;        /* precision of output, default = 6 */
	int             guts     = 0;                  /* flag for printing guts */
	double          stepsize   = 1.;                  /* stepsize for solver */
	double          accuracy   = 0.001;              /* accuracy for solvers */
	double          p_stepsize = 0.;                      /* output stepsize */
	double          time = -999999999.;                /* time for -t option */
	char            *timefile = NULL;                  /* file for -j option */
	char            *section_title;                /* parameter section name */
	char *mat_genotype = NULL;
/* stuff used as input/output for blastoderm */
	NArrPtr *outtab;                            /* times to be output */
	NArrPtr *outtab2;                            /* times to be output */
	NArrPtr *goutput;       /* output of guts is stored in this */
	DArrPtr *tt;           /* array with times for which there's data */
	char            *genotype  = NULL;                    /* genotype string */
	char **gutsdefs = NULL; /* array of strings to hold guts defs */
	char            *gutstitle = NULL;
	int             length;                     /* length of genotype string */
/* the following lines define a pointers to:                               */
/*            - pd:    dvdt function, currently only DvdtOrig in zygotic.c */
/*            - pj:    Jacobian function, in zygotic.c                     */
/*                                                                         */
/* NOTE: ps (solver) is declared as global in integrate.h                  */
/*  void (*pd)(double *, double, double *, int, int);
  void (*pj)(double, double *, double *, double **, int);*/
/* we need only solver name */
	char *solvername, *solvername0, *solver;
/* external declarations for command line option parsing (unistd.h) */
	extern char *optarg;                     /* command line option argument */
	extern int  optind;                /* pointer to current element of argv */
	extern int  optopt;               /* contain option character upon error */
/* following part sets default values for deriv, Jacobian and solver funcs */
/* and section title                                                       */
	Derivative2 pd;
	Derivative pd1;
	Blastoderm*Genotype;
	short isCustom = 0;
	Slist*mat_genotypes;
	int nalleles;
	int rhs, zero_bias;
	double dosage;
	int e_mutate = 0;
	int *e_mutate_genes = NULL;
	int d_mutate = 0;
	int *d_mutate_genes = NULL;
	int m_mutate = 0;
	double m_mutate_arg = 0.0;
	pd = Dvdt2Orig;
	pd1 = DvdtOrig;
	solvername = (char *)calloc(MAX_RECORD, sizeof(char));
	solvername = strcpy(solvername, "rkso");
	solvername0 = (char *)calloc(MAX_RECORD, sizeof(char));
	solvername0 = strcpy(solvername0, "bs");
	rhs = 1;
	dosage = 2.0;
	zero_bias = 0;
	solver = NULL;
	section_title = (char *)calloc(MAX_RECORD, sizeof(char));
	section_title = strcpy(section_title, "eqparms");  /* default is eqparms */
/* following part parses command line for options and their arguments      */
/* modified from original getopt manpage                                   */
	optarg = NULL;
	while ( (c = getopt(argc, argv, OPTS)) != -1 )
		switch (c) {
			case 'a':
				accuracy = atof(optarg);
				if ( accuracy <= 0 )
					error("unfold: accuracy (%g) is too small", accuracy);
			break;
			case 'D':                                 /* -D runs in debugging mode */
				debug = 1;
			break;
			case 'E':
				e_mutate_genes = (int*)realloc(e_mutate_genes, ( e_mutate + 1 ) * sizeof(int));
				e_mutate_genes[e_mutate] = atoi(optarg);
				e_mutate++;
			break;
			case 'f':
				ndigits = atoi(optarg);             /* -f determines float precision */
				if ( ndigits < 0 )
					error("unfold: what exactly would a negative precision be???");
				if ( ndigits > MAX_PRECISION )
					error("unfold: max. float precision is %d!", MAX_PRECISION);
			break;
			case 'F':
				d_mutate_genes = (int*)realloc(d_mutate_genes, ( d_mutate + 1 ) * sizeof(int));
				d_mutate_genes[d_mutate] = atoi(optarg);
				d_mutate++;
			break;
			case 'g':                                    /* -g choose g(u) function */
				if( !(strcmp(optarg, "s")) )
					gofu = Sqrt;
				else if ( !(strcmp(optarg, "t")))
					gofu = Tanh;
				else if ( !(strcmp(optarg, "e")))
					gofu = Exp;
				else if ( !(strcmp(optarg, "h")))
					gofu = Hvs;
				else if ( !(strcmp(optarg, "l")))
					gofu = Lin;
				else if ( !(strcmp(optarg, "n")))
					gofu = He;
				else
					error("unfold: %s is an invalid g(u), should be e, h, s, t or l", optarg);
			break;
			case 'G':                                           /* -G guts options */
				guts = 1;
			break;
			case 'h':                                            /* -h help option */
				PrintMsg(help, 0);
			break;
			case 'H':
				integrate_set_zero_mrna(1);
			break;
			case 'i':                           /* -i sets stepsize for the solver */
				stepsize = atof(optarg);
				if ( stepsize < 0 )
					error("unfold: going backwards? (hint: check your -i)");
				if ( stepsize == 0 )
					error("unfold: going nowhere? (hint: check your -i)");
				if ( stepsize > MAX_STEPSIZE )
					error("unfold: stepsize %g too large (max. is %g)", stepsize, MAX_STEPSIZE);
			break;
			case 'j':
				if ( p_stepsize > 0. )
					error("unfold: can't use -p and -j together!");
				if ( time != -999999999. )
					error("unfold: can't use -t and -j together!");
				timefile = (char *)calloc(MAX_RECORD, sizeof(char));
				timefile = strcpy(timefile, optarg);
			break;
			case 'l':                           /* -i sets stepsize for the solver */
				gcdm_set_maternal_scaling( 1, atof(optarg));
			break;
			case 'm':
				m_mutate = 1;
				m_mutate_arg = atof(optarg);
			break;
			case 'o':             /* -o sets old division style (ndivs = 3 only! ) */
				olddivstyle = 1;
			break;
			case 'O':
				if (!strcmp(optarg, "hyb")) {
					model = HYBRID;
				} else if (!strcmp(optarg, "simple")) {
					model = SIMPLE;
				} else if (!strcmp(optarg, "nomit")) {
					model = NOMITOSIS;
				} else if (!strcmp(optarg, "double")) {
					model = DOUBLING;
				} else if (!strcmp(optarg, "hed")) {
					model = HEDIRECT;
				} else if (!strcmp(optarg, "hes")) {
					model = HESRR;
				} else if (!strcmp(optarg, "heu")) {
					model = HEURR;
				} else if (!strcmp(optarg, "heq")) {
					model = HEQUENCHING;
				} else {
					error("unfold: invalid model (-O %s)", optarg);
				}
			break;
			case 'p':                         /* -p sets print stepsize for output */
				if ( timefile )
					error("unfold: can't use -j and -p together!");
				if ( time != -999999999. )
					error("unfold: can't use -t and -p together!");
				p_stepsize = atof(optarg);
				if ( p_stepsize < 0.001 )
					error("unfold: output stepsize (%g) too small (min 0.001)", p_stepsize);
			break;
			case 'r':
				mat_genotype = (char *)calloc(MAX_RECORD, sizeof(char));
				mat_genotype = strcpy(mat_genotype, optarg);
			break;
			case 's':
				solver = (char *)calloc(MAX_RECORD, sizeof(char));
				solver = strcpy(solver, optarg);
			break;
			case 't':
				if ( timefile )
					error("unfold: can't use -j and -t together!");
				if ( p_stepsize > 0. )
					error("unfold: can't use -p and -t together!");
				time = atof(optarg);
				if ( (time < 0) && (time != -999999999))
					error("unfold: the time (%g) doesn't make sense", time);
				break;
			case 'v':                                  /* -v prints version message */
				fprintf(stderr, verstring, *argv, VERS, USR, MACHINE, COMPILER, FLAGS, __DATE__, __TIME__);
				exit(0);
			case 'x':
				if ( (strcmp(optarg, "input")) && (strcmp(optarg, "eqparms")) && (strcmp(optarg, "parameters")) )
					error("unfold: invalid section title (%s)", optarg);
				section_title = strcpy(section_title, optarg);
			break;
			case 'X':
				if (!strcmp(optarg, "orig2")) {
					rhs = 0;
					pd = Dvdt2Orig;
				} else if (!strcmp(optarg, "orig3")) {
					rhs = 0;
					pd = Dvdt3Orig;
				} else if (!strcmp(optarg, "orig31stDir")) {
					rhs = 0;
					pd = Dvdt31stTelDirichlet;
            } else if (!strcmp(optarg, "orig31stNeu")) {
					rhs = 0;
					pd = Dvdt31stTelNeumann;
				} else if (!strcmp(optarg, "orig32ndNeu")) {
					rhs = 0;
					pd = Dvdt32ndTelNeumann;
				} else if (!strcmp(optarg, "orig3M")) {
					rhs = 0;
					pd = Dvdt3TelMick;
            } else if (!strcmp(optarg, "orig31stJeffDir")) {
					rhs = 0;
					pd = Dvdt31stJeffDirichlet;
            } else if (!strcmp(optarg, "orig31stJeffNeu")) {
					rhs = 0;
					pd = Dvdt31stJeffNeumann;
            } else if (!strcmp(optarg, "bump")) {
					rhs = 0;
					pd = Dvdt2Bump;
				} else if (!strcmp(optarg, "mob1")) {
					rhs = 0;
					pd = Dvdt2Mob1;
				} else if (!strcmp(optarg, "orig")) {
					rhs = 1;
#ifdef XTRA
				} else if (!strcmp(optarg, "wmrna")) {
					rhs = 1;
					pd1 = DvwdtOrig;
				} else if (!strcmp(optarg, "dmrna")) {
					rhs = 1;
					pd1 = DdvwdtOrig;
					ODESolve_set_dde(gcdm_add_delayed);
				} else if (!strcmp(optarg, "ddmrna")) {
					rhs = 1;
					pd1 = DddvwdtOrig;
					ODESolve_set_dde(gcdm_add_delayed);
				} else if (!strcmp(optarg, "ddual")) {
					rhs = 1;
					pd1 = DddvwdtDual;
					ODESolve_set_dde(gcdm_add_delayed);
				} else if (!strcmp(optarg, "dbmrna")) {
					rhs = 1;
					pd1 = DddwwdtOrig;
					ODESolve_set_dde(gcdm_add_delayed);
#endif
				} else {
					error("stat: invalid deriv (-X %s)", optarg);
				}
			case 'Y':
				extra_file_flag = 2;
			break;
			case 'z':
				isCustom = 1;
				custom_gast = atof(optarg);
				if ( custom_gast < 0. )
					error("unfold: gastrulation time must be positive");
				if ( custom_gast > 10000. )
					error("unfold: gastrulation time must be smaller than 10'000");
			break;
			case 'Z':
				extra_file_flag = 1;
			break;
			case ':':
				error("unfold: need an argument for option -%c", optopt);
			case '?':
			default:
				error("unfold: unrecognized option -%c", optopt);
		}
/* error check */
	if ( stepsize > p_stepsize && p_stepsize != 0 )
		error("unfold: print-stepsize (%g) smaller than stepsize (%g)!", p_stepsize, stepsize);
/*	if ( (argc - (optind - 1)) < 2 || (argc - (optind - 1)) > 3 )
		PrintMsg(usage, 1);*/
/* let's get started and open the data file here */
/*  infile = argv[optind];*/
	infile = (char *)calloc(MAX_RECORD, sizeof(char));
	infile = strcpy(  infile , argv[optind]);
	fp = fopen(infile, "r");
	if( !fp )
		file_error("unfold");
/* Initialization code here */
	InitZygote(fp, section_title);
                                     /* initializes parameters, defs, bias */
                                     /* and bicoid and defines static vars */
                                     /* for the dvdt function              */
	if ( extra_file_flag == 1 ) {
		FILE*extra;
		EqParms* eq = GetParameters();
		extra = fopen(argv[optind + 1], "r");
		ReReadParameters(eq, extra, section_title);
		fclose(extra);
		optind++;
	}
	if ( extra_file_flag == 2 ) {
		FILE*extra;
		EqParms* eq = GetParameters();
		extra = fopen(argv[optind + 1], "r");
		ReReadParameters1(eq, extra);
		fclose(extra);
		optind++;
	}
#ifdef XTRA
	gcdm_init_seq(fp);
#endif
	fclose(fp);
	if ( !((optind+1) == argc) ) {
		genotype = (char *)calloc(MAX_RECORD, sizeof(char));
		genotype = strcpy(genotype, argv[optind+1]);        /* genotype string */
	}
	if ( e_mutate > 0 ) {
		for ( i = 0 ; i < e_mutate ; i++ ) {
			E_Mutate( e_mutate_genes[i] );
		}
	}
	if ( d_mutate > 0 ) {
		for ( i = 0 ; i < d_mutate ; i++ ) {
			D_Mutate( d_mutate_genes[i] );
		}
	}
	mat_genotypes = GetMatGenotypes(&nalleles);
	if ( !mat_genotype ) {
		mat_genotype = (char *)calloc(MAX_RECORD, sizeof(char));
		mat_genotype = strcpy(mat_genotype, mat_genotypes->name);
	}
/* initialize genotype if necessary, otherwise check for errors */
	if ( genotype ) {
		length = (int)strlen(genotype);
		if ( length != defs->ngenes )
			error("unfold: genotype %s length doesn't match number of genes (%d)", genotype, defs->ngenes);
		if ( m_mutate == 0 ) {
			for ( i = 0; i < defs->ngenes; i++) {
				c = (int)*(genotype+i);
				if ( ( c != 87 ) && (( c < 82 ) || ( c > 84 )) )
					error("unfold: genotype string can only contain R, S, T or W");
				mat_genotypes->genotype[i] = genotype[i];
			}
		}
/*		free(genotype);*/
	}
/* initialize debugging file names */
	if ( debug ) {
		slogfile = (char *)calloc(MAX_RECORD, sizeof(char));
		dumpfile = (char *)calloc(MAX_RECORD, sizeof(char));
		sprintf(slogfile, "%s.slog", infile);
		sprintf(dumpfile, "%s.%s.uout", infile, genotype);
		slog = fopen(slogfile, "w");              /* delete existing slog file */
		fclose(slog);
		slog = fopen(slogfile, "a");            /* now keep open for appending */
	}
/* the following establishes the tabulated times according to either:      */
/* -t (output single time step)                                            */
/* -j (read times from file)                                               */
/* -p (stepsize set by argument)                                           */
/* default behavior is producing output for gastrulation time only         */
	if ( time != -999999999. ) {
		if ( time > GetGastTime() )
			error("unfold: time (%g) larger than gastrulation time!", time);
		tt = (DArrPtr*)malloc(sizeof(DArrPtr));
		tt->size  = 1;
		tt->array = (double *)calloc(1,sizeof(double));
		tt->array[0] = time;
	} else if ( timefile != NULL ) {
		tt = ReadTimes(timefile);
		free(timefile);
	} else if ( p_stepsize != 0. ) {
		tt = MakeTable(p_stepsize);
	} else {
		tt = (DArrPtr*)malloc(sizeof(DArrPtr));
		tt->size  = 1;
		tt->array = (double *)calloc(1,sizeof(double));
		tt->array[0] = GetGastTime();
	}
/* Run the model... */
	if ( solver ) {
		if ( rhs == 0 )
			solvername = strcpy (solvername, solver);
		if ( rhs == 1 )
			solvername0 = strcpy (solvername0, solver);
	}
	Genotype = gcdm_blastoderm_new(mat_genotype, tt, pd1, pd, NULL, NULL, NULL, solvername0, solvername);
	if ( m_mutate == 1 ) {
		MMutate( genotype, m_mutate_arg);
	}
	gcdm_blastoderm_solve_conc_with_hints(Genotype, stepsize, accuracy, slog, dosage, zero_bias, rhs);
/* if debugging: print out the innards of the model to unfold.out */
	if ( debug ) {
		dumpptr = fopen(dumpfile, "w");
		if( !dumpptr ) {
			perror("unfold");
			exit(1);
		}
		PrintBlastoderm(dumpptr, Genotype->Solution, "debug_output", MAX_PRECISION, defs->ngenes);
		fclose(dumpptr);
	}
/* strip output of anything that's not in tt */
	outtab = ConvertAnswer(Genotype->Solution, tt);
/* code for printing guts... first read gutsdefs section */
	if ( guts ) {
		fp = fopen(infile, "r");
		if( !fp ) {
			perror("unfold");
			exit(1);
		}
		gutsdefs = ReadGuts(fp);
		fclose(fp);
		gutstitle = (char *)calloc(MAX_RECORD, sizeof(char));
/* then calculate guts and print them */
		outtab2 = ConvertAnswer(Genotype->Solution2, tt);
		for ( i = 0; *(gutsdefs+i); i++) {
			if ( (goutput = CalcGuts(Genotype->genotype_ID, outtab, outtab2, &numguts, *(gutsdefs+i))) ) {
				gutstitle = strcpy(gutstitle, "guts_for_");
				PrintBlastoderm(stdout, goutput, strcat(gutstitle, *(gutsdefs+i)), ndigits, numguts);
				FreeNArrPtr(goutput);
			}
			free(*(gutsdefs+i));
		}
		free(gutsdefs);
		free(gutstitle);
/* code for printing model output */
	} else
		PrintBlastoderm(stdout, outtab, "output\n", ndigits, defs->ngenes);
/* ... and then clean up before you go home. */
/*	Blastoderm_delete(Genotype);
	FreeZygote();
	free(outtab->array);
	free(tt->array);
	free(section_title);*/
	if ( debug ) {
		fclose(slog);
		free(dumpfile);
		free(slogfile);
	}
	return 0;
}
