/***************************************************************************
 *            maternal.h
 *
 *  Wed May 11 13:54:07 2005
 *  Copyright  2005  $USER
 *  kozlov@spbcas.ru
 ****************************************************************************/

/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef _MATERNAL_H
#define _MATERNAL_H

#ifdef __cplusplus
extern "C"
{
#endif

/*****************************************************************
 *                                                               *
 *   maternal.h                                                  *
 *                                                               *
 *****************************************************************
 *                                                               *
 *   written by JR, modified by Yoginho                          *
 *                                                               *
 *****************************************************************
 *                                                               *
 * This header contains several kinds of things:                 *
 *                                                               *
 * 1. structs and constants for things used throughout the fly   *
 *    model specific part of the code (since this file will al-  *
 *    ways get included when we run the fly model); therefore    *
 *    all fly-specific things that used to be in global.h are    *
 *    now in here                                                *
 * 2. stuff that deals with that part of the blastoderm which is *
 *    fixed by the maternal genotype, i.e. division schedules    *
 *    (including the rules for zygotic.c, the number of nuclei   *
 *    at each cleavage cycle and the lineage number of the most  *
 *    anterior nucleus for each cleavage cycle), bicoid gra-     *
 *    dients and diffusion schedules                             *
 * 3. bias-related stuff is also in here since it's needed to    *
 *    run the model                                              *
 *                                                               *
 *****************************************************************/

#include <stdio.h>

/*** CONSTANTS *************************************************************/

/* The following is the maximum stepsize for solvers (now set to the whole */
/* time it takes to model to run (100.8 minutes))                          */

#define MAX_STEPSIZE        100.8

/* masks for lineage numbers */

#define     CYCLE1            1
#define     CYCLE2            2
#define     CYCLE3            4
#define     CYCLE4            8
#define     CYCLE5           16
#define     CYCLE6           32
#define     CYCLE7           64
#define     CYCLE8          128
#define     CYCLE9          256
#define     CYCLE10         512
#define     CYCLE11        1024
#define     CYCLE12        2048
#define     CYCLE13        4096
#define     CYCLE14        8192

/* Data points that have a concentration of -1 will be ignored! */
/* this needs to be here rather than in score.h, since it's used to */
/* calculate the number of data points in ReadData */

/* CONSTANTS FOR DBL_EPSILON ***********************************************/
/* EPSILON is used as minimal time step and corresponds to the minimal     */
/* double (or float) that can still be handled by C                        */
/* maybe the following declarations
 * can be restricted, but for now they go here */
#include <float.h>

/* DBL_EPSILON is about 2 x 10^-16. so BIG_EPSILON is ~10^-11 min. */
/*#define      DBL_EPSILON 0.000000000000001*/
#define      EPSILON     (DBL_EPSILON * 1000.)     /* This assumes all times < 100. min. !! */
#ifdef       FLOAT_EVERYTHING
#undef       EPSILON
#define      EPSILON     (FLT_EPSILON * 100.)             /* see above */
#endif
#define      BIG_EPSILON (EPSILON * 1000.)
#define      HALF_EPSILON (EPSILON * .5)

#define TOTAL_DIVS 6
#define IGNORE          -1.

#define     HYBRID           0                           /* hybrid model is DDE's */
#define     SIMPLE           1   /* PDE's:the same rule for R's as in hybryd model*/
#define     NOMITOSIS        2           /* PDE's: synthesis is present in mitosis*/
#define     DOUBLING         3                            /* PDE's:R's are doubled*/

#define     HEDIRECT 4
#define     HESRR 5
#define     HEQUENCHING 6
#define     HELOGISTIC 7
#define     HEURR 8

/*** STRUCTS ***************************************************************/

/* general struct used for sized array of doubles */

typedef struct DArrPtr {
  int      size;
  double   *array;
} DArrPtr;

typedef enum ProblemGeneType {
	maternal,
	zygotic,
	external,
	ntypes
} ProblemGeneType;

typedef struct Couple {
	char g1;
	char g2;
	int first_gene_id;
	int second_gene_id;
	ProblemGeneType first_gene_type;
	ProblemGeneType second_gene_type;
} Couple;

/* this is the problem at hand */

typedef struct TheProblem {
	int ngenes;                            /* number of genes to consider */
	int egenes;                            /* number of external inputs */
	int mgenes;                            /* number of maternal (constant in time) inputs */
	int ncouples;                            /* number of maternal (constant in time) inputs */
	char *gene_ids;                       /* pointer to char with gene IDs */
	char *egene_ids;                       /* pointer to char with external gene IDs */
	char *mgene_ids;                       /* pointer to char with external gene IDs */
	Couple*couples;
	int ndivs;                                /* number of cell divisions */
	int nnucs;                        /* number of nuclei at gastrulation */
	char diff_schedule;                              /* diffusion schedule */
	char mob_schedule;                              /* diffusion schedule */
} TheProblem;

/* two structs for bicoid gradients */

typedef struct BcdGrad {
	int ccycle;                                   /* the cleavage cycle */
	DArrPtr *gradient;                  /* the array of concs for a gradient */
} BcdGrad;

typedef struct BArrPtr {      /* points to an array of BcdStates of 'size' */
	int size;
	BcdGrad *array;
} BArrPtr;

/* two structures for bias data and Blastoderm() output (solution) */

typedef struct NucState {
	double time;                                               /* the time */
	DArrPtr *state;                /* the array of v's, and how many of them */
} NucState;

typedef struct NArrPtr {
	int size;                   /* How many NucState elements in array? */
	NucState *array;    /* points to 1st element of array of NucState elems. */
} NArrPtr;

/* The following three structs are used to store facts data. ***************
 * The idea is to encode sparse data against non-sparse v[index] state     *
 * arrays. This will in most of the present cases use more memory, but     *
 * that won't always be true. Also, it is more convenient for those        *
 *'don't care' points.                                                     *
 * NOTE: needs to be in this generic header for union definition below     *
 ***************************************************************************/

typedef struct DataPoint {
	int index;
	double conc;
	double difference;/* added by MacKoel for difference between facts and data */
} DataPoint;

typedef struct DataRecord {
	int size;
	double time;
	DataPoint *array;
	double*dfacts;
} DataRecord;

typedef struct DataTable {
	int size;
	DataRecord *record;
} DataTable;

/* struct and union for genotype number and pointer to data ****************/
/* used for bicoid, bias, facts and time tables                            */

typedef union DataPtr {
	DArrPtr *times;                   /* used for bias and tab times */
	BArrPtr *bicoid;                  /* for bicoid stuff            */
	NArrPtr *bias;                    /* for the bias                */
	DataTable *facts;                  /* for facts                   */
} DataPtr;

typedef struct GenoType {
	char *ID;
	char *genotype;
	DataPtr *ptr;
} GenoType;

/* linked lists for reading bicoid (Blist) and bias/facts (Dlist) */

typedef struct Blist {                  /* linked list used to read bicoid */
	unsigned int lineage;
	double conc;
	struct Blist *next;
} Blist;


typedef struct Dlist {            /* linked list used to read bias & facts */
	unsigned int lineage;
	double       *d;
	struct Dlist *next;
} Dlist;


/* linked list used to read in genotype specific sections from datafile */

typedef struct Slist {
	char *name;
	char *genotype;        /* genotype string (see dataformatX.X) */
	char *mat_section;    /* bcd section title */
	char *bias_section;   /* bias section title */
	char *fact_section;    /* fact section title */
	char *hist_section;    /* fact section title */
	char  *ext_section;    /* fact section title */
	struct Slist *next;
} Slist;


typedef struct ChaosData {
	double *bc;
	double *deltamc;
	int ng;
	int ntc;
	double w;
	int *ltc;
	int nltc;
	int nbc;
} ChaosData;


/*** GLOBALS ** ************************************************************/

int debug;                                            /* debugging flag */
int olddivstyle;                    /* flag: old or new division times? */
int nalleles;                  /* number of alleles (genotypes) in data */

double maxconc;                /* max prot conc: 12 (old-), 255 (newstyle) */
double custom_gast;         /* custom gastrulation time set by -S */

int model;           /* model can be hybrid, simple, nomitosis, doubling*/

/***************************************************************************
 * - defs:     is the global problem struct; must be visible to scoring    *
 *             functions, to Blastoderm in integrate.c, to stuff in mater- *
 *             nal.c and to functions in zygotic.c (need any more justifi- *
 *             cation for this to be global?)                              *
 ***************************************************************************/

TheProblem *defs;

/*** FUNCTION PROTOTYPES ***************************************************/

/* Initialization Functions */

/*** InitBicoid: Copies the Blist read by ReadBicoid into the DArrPtr ******
 *               structure. The bicoid DArrPtr contains pointers to bcd    *
 *               arrays for each cell division cycle. Also initializes the *
 *               lin_start array for keeping track of lineage numbers.     *
 ***************************************************************************/

void         InitBicoid(FILE *fp);

/*** InitBias:  puts bias records in a form where get_bias can use them. ***
 *              It expects times in increasing order. It expects a non-    *
 *              sparse entry, with no genes or nuclei missing.             *
 ***************************************************************************/

void         InitBias(FILE *fp);

/*** InitBTs: initializes the static BT struct that holds all times for ****
 *            which we have bias.                                          *
 ***************************************************************************/

void         InitBTs(void);

/*** InitNNucs: takes the global defs->nnucs and calculates number of nucs **
 *              for each cleavage cycle which are then stored in reverse   *
 *              order in the static nnucs[] array                          *
 *   CAUTION:   defs struct needs to be initialized before this!           *
 ***************************************************************************/

void         InitNNucs(void);
/* Functions that return info about embryo */

/*** GetBicoid: returns a bicoid gradients (in form of a DArrPtr) for a ****
 *              specific time and genotype.                                *
 ***************************************************************************/

DArrPtr*GetBicoid(double time, char *genotype);

/*** GetBias: This function returns bias values for a given time and *******
 *            genotype.                                                    *
 ***************************************************************************/

DArrPtr*GetBias(double time);

/*** GetBTimes: returns times for which there's bias ***********************
 ***************************************************************************/

DArrPtr*GetBTimes(char *genotype);

/*** GetNNucs: returns number of nuclei for a given time *******************
 ***************************************************************************/

int          GetNNucs(double t);

/*** GetStartLin: returns the lineage number of the most anterior nucleus **
 *                for a given time                                         *
 ***************************************************************************/

int          GetStartLin(double t);

/*** GetCCycle: returns cleavage cycle for a given time ********************
 ***************************************************************************/

unsigned int GetCCycle(double time);

/*** ParseLineage: takes lineage number as input and returns the cleavage **
 *                 cycle the nucleus belongs to.                           *
 ***************************************************************************/

unsigned int ParseLineage(unsigned int lin);

/*** GetDivtable: returns times of cell divisions depending on ndivs and ***
 *                olddivstyle; returns NULL in case of an error            *
 ***************************************************************************/

double *GetDivtable(void);

/*** GetDurations: returns pointer to durations of cell divisions de- ******
 *                 pending on ndivs and olddivstyle; returns NULL in case  *
 *                 of an error                                             *
 ***************************************************************************/

double *GetDurations(void);

/*** GetGastTime: returns time of gastrulation depending on ndivs and ******
 *                olddivstyle; returns 0 in case of an error               *
 ***************************************************************************/

double GetGastTime(void);

/*** GetD: returns diffusion parameters D according to the diff. params. ***
 *         in the data file and the diffusion schedule used.               *
 *   NOTE: Caller must allocate D_tab                                      *
 ***************************************************************************/

void         GetD(double t, double *d, char diff_schedule, double *D_tab);

/*** GetRule: returns the appropriate rule for a given time; used by the ***
 *            derivative function                                          *
 ***************************************************************************/

int          GetRule(double time);

/*** Theta: the value of theta(t) used by DvdtOrig ***
 *            for autonomous eqns.                                          *
 ***************************************************************************/

int          Theta(double time);
/*** GetIndex: this functions returns the genotype index for a given *******
 *             genotype number for reading the GenoType struct.            *
 ***************************************************************************/

int          GetIndex(char *name);

/*** MakeTable: Returns the appropriate time table from uftimes.c **********
 ***************************************************************************/

DArrPtr*MakeTable(double p_stepsize);

/* Following functions read data from file into structs for bias and bcd */

/*** ReadTheProblem: reads the problem section of a data file into the *****
 *                   TheProblem struct.                                    *
 ***************************************************************************/

TheProblem*ReadTheProblem(FILE *fp);

/*** ReadGenotypes: This function reads all the genotypes in a datafile & **
 *                  returns an SList with genotype number and pointers to  *
 *                  the corresponding section titles for bias, bcd & facts *
 ***************************************************************************/

Slist*ReadGenotypes(FILE *fp);

/*** ReadBicoid: Reads the bcd section of a data file into a linked list, **
 ***************************************************************************/

Blist*ReadBicoid(FILE *fp, char *section);

/*** ReadData: reads in a data or bias section and puts it in a linked *****
 *             list of arrays, one line per array; ndp is used to count    *
 *             the number of data points in a data file (ndp), which is    *
 *             used to calculate the root mean square (RMS) if required    *
 *                                                                         *
 *             ReadData allows comments that start with a letter or punc-  *
 *             tuation mark, and treats lines starting with a number or    *
 *             .num or -.num as data. It expects to read an int and        *
 *             ngenes + 1 data points: lineage, time and ngenes protein    *
 *             concentrations. It expects data to be in increasing spatial *
 *             and temporal order.                                         *
 ***************************************************************************/

Dlist*ReadData(FILE *fp, char *section, int *ndp, int size);

/*** ReadTimes: reads a time table from a file and returns a DArrPtr *******
 * FILE FORMAT: one time per line separated with newlines                  *
 *        NOTE: max. times tab size is 1000                                *
 ***************************************************************************/

DArrPtr*ReadTimes(char *timefile);

/*** List2Bicoid: takes a Blist and returns the corresponding BArrPtr ******
 *                structure.                                               *
 ***************************************************************************/

BArrPtr*List2Bicoid(Blist *inlist);

/*** ReadGuts: reads the $gutsdefs section in a data file into an array ****
 *             of strings which then need to get parsed                    *
 ***************************************************************************/

char**ReadGuts(FILE *fp);

/*** List2Bias: takes a Dlist and returns the corresponding DArrPtr ********
 *              structure.                                                 *
 ***************************************************************************/

NArrPtr*List2Bias(Dlist *inlist);

/* Following functions are utility functions for different linked lists ****
 * which are used to read in data of unknown size. All utility functions   *
 * follow the same scheme (X stands for the different letters below):      *
 *                                                                         *
 * - init_Xlist:         allocates first element and returns pointer to it *
 * - addto_Xlist:        adds the adduct to an already existing linkd list *
 * - free_Xlist:         frees memory of linked list                       *
 * - count_Xlist:        counts elements in the linked list                *
 *                                                                         *
 ***************************************************************************/

/* Utility functions for Blist (for reading bicoid) */
Blist        *init_Blist    (void);
Blist        *addto_Blist   (Blist *start, Blist *adduct);
void         free_Blist     (Blist *start);
int          count_Blist    (Blist *start);

/* Utility functions for Dlist (for reading bias and facts) */
Dlist        *init_Dlist    (int size);
Dlist        *addto_Dlist   (Dlist *start, Dlist *adduct);
void         free_Dlist     (Dlist *start);
int          count_Dlist    (Dlist *start);
/* Utility functions for Slist (for reading genotypes ) */
Slist        *init_Slist    (void);
Slist        *addto_Slist   (Slist *start, Slist *adduct);
void         free_Slist     (Slist *start);
int          count_Slist    (Slist *start);
/* added by MacKoel */

typedef struct InterpObject {
	double *fact_discons;
	int fact_discons_size;
	NArrPtr func;
	NArrPtr slope;
	int maxsize;
	double maxtime;
} InterpObject;

/*** FreeSolution: frees memory of the solution structure created by *******
 *                 Blastoderm() or gut functions                           *
 ***************************************************************************/

void FreeNArrPtr(NArrPtr*Solution);

void FreeDArrPtr(DArrPtr*state);

void FreeFacts(GenoType*tab);
void FreeGType(GenoType*fact);
void FreeDataPtr(DataPtr*data);
void FreeBArrPtr(BArrPtr*arg);
void FreeDataTable(DataTable*arg);
void FreeDataRecord(DataRecord*record);
void GetDCoef(double t, char diff_schedule, double *D_tab);
void GetRCoef(double t, double*factor);
void applyBicoidDosage(double dosage, char *genotype, int gindex);

int GetStartLinIndex(double t);
NArrPtr*List2Externals(Dlist*inlist);
int descend(const void* a, const void* b);
InterpObject *DoInterp(NArrPtr *Nptrfacts, int num_genes);
void InitGenotypeIndex(char *g_type);
BArrPtr*List2Maternals(Dlist *inlist);
void Go_Forward(double *output, double *input, int output_ind, int input_ind, int num_genes);
void Go_Backward(double *output, double *input, int output_ind, int input_ind, int num_genes);
void ExternalInputs(double t, double t_size, double *yd, int n);
int Index2NNuc(int index);
int Index2StartLin(int index);
void InitMaternal(FILE *fp);
void InitMaternals(FILE *fp);
void InitExternals(FILE *fp);
void InitFullNNucs(void);
void gcmd_get_external_inputs(double t, double*vext, int n);
double *gcmd_get_maternal_inputs(unsigned int ccycle);
void gcmd_get_coupled_inputs(double t, double*vcouple, double*wcouple, int n, double*v, double*w, int nv, double*vext, int next, double*vmat, int nmat);
Slist*GetMatGenotypes(int *n);
double *gcdm_get_data_times(int *n);
int gcdm_get_nuclear_index(int x,  double time);
void GetM(double t, double *m, char diff_schedule, double *M_tab);
void GetMM(double t, double *m, char diff_schedule, double *M_tab);
void GetL(double t, double *m, char diff_schedule, double *M_tab);
void GetLL(double t, double *m, char diff_schedule, double *M_tab);
void InitGenotypeIndexWithBicoidDosage(char *g_type, double dosage);
void gcmd_get_coupled_inputs_1(double t, double*vcouple, int n, double*v, int nv, double*vext, int next, double*vmat, int nmat);

void gcdm_scale_table(double factor, NArrPtr *table, int offset);
void gcdm_scale_maternals(double factor, GenoType *maternals);
void gcdm_scale_bias(double factor, GenoType *start_point);
void gcdm_set_maternal_scaling(int flag, double factor);

void SlopeExtInputs(double t, double t_size, double *yd, int n);
void gcdm_get_curveSlopeExtInputs(double t, double* slopeE, int n);

char**gcdm_init_acid(FILE*fp);

void gcdm_get_delayed(double *v, double t);

void gcdm_init_delayed(int n);
void gcdm_rem_delayed();
void gcdm_add_delayed(double *v, double t);
/*
#include "DDEHistory.h"
*/

typedef struct DDEHistoryValues {
  double t;
  double *v;
  struct DDEHistoryValues *next;
} DDEHistoryValues;

typedef struct DDEHistory {
  double tstart, tend;
  double max_delay, min_delay;
  int n;
  int nlags;
  DDEHistoryValues *first;
  DDEHistoryValues *last;
} DDEHistory;

/* DDEHistory_create creates and fill with initial
 *                   values History for solving dde with
 *                   adaptive step size
 */
DDEHistory* DDEHistory_create(double *delays, int nlags, int n);


/* DDEHistory_addHistory add values v at time t to history
                         and control size of storing values
                         only v values from t-max_delay to t should
                         be stored
*/
void DDEHistory_addHistory(DDEHistory *history, double *v, double t);

/* DDEHistory_evaluateAtTime - get history at time t-delay and store
 *                            them in v
 */
void DDEHistory_interpolateLinear(DDEHistory *history, double *v, double delay, double t);

void DDEHistory_free(DDEHistory* history);

#ifdef __cplusplus
}
#endif

#endif /* _MATERNAL_H */
