/***************************************************************************
                          ffio.h  -  description
                             -------------------
    begin                :  23 2004
    copyright            : (C) 2004 by Konstantin Kozlov
    email                : kostya@spbcas.ru
 ***************************************************************************/

#ifndef OUTPUT_INCLUDED
#define OUTPUT_INCLUDED

#ifdef __cplusplus
extern "C"
{
#endif

#define FFIO_MAX_RECORD 255

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>

#include <ctype.h>
#include <sys/times.h>
#include <sys/time.h>

FILE *FindSection(FILE *fp, char *input_section);
void KillSection(char *filename, char *title);

void GetSectionPosition(FILE**outfile,FILE**tmpfile,char*title_of_this,char*title_of_above,char*name,char**temp);
void WriteRest(FILE**outfile,FILE**tmpfile,char*name,char**temp);

FILE *FindOptionTitle(FILE *fp, char *title);

/*** CopyDatafile: erases the section with 'title' from the file 'fp' *******
 ***************************************************************************/

void CopyDatafile(char *oldfilename, char *newfilename);

#ifdef __cplusplus
}
#endif

#endif
