/***************************************************************************
 *            integrate.h
 *
 *  Wed May 11 13:55:03 2005
 *  Copyright  2005  $USER
 *  kozlov@spbcas.ru
 ****************************************************************************/

/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef _INTEGRATE_H
#define _INTEGRATE_H

#ifdef __cplusplus
extern "C"
{
#endif

/*****************************************************************
 *                                                               *
 *   integrate.h                                                 *
 *                                                               *
 *****************************************************************
 *                                                               *
 *   written by JR, modified by Yoginho                          *
 *                                                               *
 *****************************************************************
 *                                                               *
 * This file has problem-specific stuff not needed for right     *
 * hand of ODE. The function Blastoderm runs the fly model and   *
 * calls the appropriate solver for propagating the equations.   *
 * PrintBlastoderm formats and prints the output of the Blasto-  *
 * derm function and FreeSolution frees the solution allocated   *
 * by Blastoderm. integrate.h also comes with a few utility      *
 * functions for TLists which are linked lists used to initia-   *
 * lize the time/mode table for Blastoderm.                      *
 *                                                               *
 *****************************************************************
 *                                                               *
 * NOTE: all right-hand-of-ODE-specific stuff is in zygotic.h    *
 *                                                               *
 *****************************************************************/
#include <stdio.h>
#include <IntegralSolve.h>
#include <ODESolve.h>
#include <maternal.h>

/* MORE CONSTANTS: OPS FOR BLASTODERM -- WITH PRIORITIES *******************/

#define ADD_BIAS     1    /* can happen any step */
#define NO_OP        2    /* ops below: first found set executed      */
#define DIVIDE       4    /* NO_OP does nothing, DIVIDE divides nucs  */
#define PROPAGATE    8    /* and PROPAGATE propagates the equations   */
#define MITOTATE     16    /* and MITOTATE carries forward the system by epsilon (to handle rounding errors from the solver */

/* A STRUCT ****************************************************************/

typedef struct TList {          /* Tlist is a linked list we use to set up */
	double time;            /* the solution struct in integrate.c; the */
	int n;               /* TList has an element for each time con- */
	int op;              /* taining the number of elements for the  */
	struct TList *next;           /* solution struct at that time (n) and a  */
} TList;                        /* rule (op) to tell the solver what to do */

/*added by MacKoel*/

typedef struct Blastoderm {
	char *genotype_ID;
	TList*entries;
	ODESolve*Concentrations;
	ODESolve*Lagrangians;
	double *divtable;           /* cell div times in reverse order */
	double *transitions;           /* this is when cell divs start */
	double *durations;              /* durations of cell divisions */
	DArrPtr *biastimes;               /* times at which bias is added */
	NArrPtr *Solution;
	NArrPtr *Solution2;
	int *what2do;               /* what to do at each time step */
	int *havedata;
	int rhs;
	int zero_bias;
	double dosage;
} Blastoderm;

Blastoderm*Blastoderm_create(char*genotype, DArrPtr* tabtimes, Derivative cDDt, Derivative2 c2DDt, Jacobian cD2Dt2, Derivative pDDt, Jacobian pD2Dt2, char*name);

/* FUNCTION PROTOTYPES *****************************************************/

/* Blastoderm Functions */

/*** Blastoderm: runs embryo model and returns an array of concentration ***
 *               arrays for each requested time (given by TabTimes) using  *
 *               stephint as a suggested stepsize and accuracy as the re-  *
 *               quired numerical accuracy (in case of adaptive stepsize   *
 *               solvers or global stepsize control) for the solver.       *
 *         NOTE: TabTimes *must* start from 0 and have increasing times.   *
 *               It includes times when bias is added, cell division times *
 *               and times for which we have data and ends with the time   *
 *               of gastrulation.                                          *
 ***************************************************************************/

void Blastoderm_SolveConc(Blastoderm*Genotype, double stephint, double accuracy, FILE *slog);

/*** PrintBlastoderm: writes the output of the model to a stream specified *
 *                    by the fp file pointer. The Table is a solution of   *
 *                    the model as returned by Blastoderm, the id speci-   *
 *                    fies the title of the output and ndigits specifies   *
 *                    the floating point precision to be printed.          *
 *                    PrintBlastoderm adjusts its format automatically to  *
 *                    the appropriate number of genes.                     *
 ***************************************************************************/

void PrintBlastoderm(FILE *fp, NArrPtr*table, char *id, int ndigits, int columns);

/*** ConvertAnswer: little function that gets rid of bias times, division **
 *                  times and such and only returns the times in the tab-  *
 *                  times struct as its output. This is used to produce    *
 *                  unfold which is nothing but the requested times.       *
 ***************************************************************************/

NArrPtr *ConvertAnswer(NArrPtr *answer, DArrPtr *tabtimes);

/* TList Utility Functions */

/*** InitTList: initializes TList and adds first (t=0) and last ************
 *              (t=gastrulation) element of Tlist.                         *
 ***************************************************************************/

TList *InitTList(void);

/*** InsertTList: takes pointer to first element of TList plus time and ****
 *                desired op for new TList element and inserts a new TList *
 *                element for time at the appropriate place within the     *
 *                linked list. This function returns a pointer to the      *
 *                first element of the TList if insertion worked out fine. *
 ***************************************************************************/
TList *InsertTList(TList *first, double time, int op);
/*** CountEntries: counts how many entries we have in a TList **************
 ***************************************************************************/
int CountEntries(TList *first);
/*** FreeTList: frees the memory of a TList ********************************
 ***************************************************************************/
void FreeTList(TList *first);
DataRecord* (*JumperPsi)(char* gindex, double time_point);
void SetInterval(int arg);
void V_Tconc(double t, double*conc);
void V_Pconc(double t, double*conc);
void BlastodermGrad(Blastoderm*Genotype, double **integ, int NumPar, double stephint, double accuracy, FILE *slog);
void Blastoderm_SolvePsi(Blastoderm*Genotype, double stephint, double accuracy, FILE *slog);
void Blastoderm_SolveJump(Blastoderm*Genotype, double stephint, double accuracy, FILE *slog);
TList*BlastList(Blastoderm*Genotype, DArrPtr* tabtimes);
void Blastoderm_setThisGenotype(Blastoderm*p);
void Blastoderm_delete(Blastoderm*p);
void Blastoderm_includeAttr(Blastoderm*Genotype);
char*Name2Gtype(char *g_type);
void gcdm_blastoderm_solve_conc_with_hints(Blastoderm*Genotype, double stephint, double accuracy, FILE *slog, double dosage, int zero_bias, int rhs);
void gcdm_blastoderm_set_hints(Blastoderm*Genotype, double dosage, int zero_bias, int rhs);
Blastoderm*gcdm_blastoderm_new(char*genotype, DArrPtr* tabtimes, Derivative cDDt, Derivative2 c2DDt, Jacobian cD2Dt2, Derivative pDDt, Jacobian pD2Dt2, char*name, char*name2);

NArrPtr *calcgthing(Blastoderm*Genotype, int witch);
void PrintBlastodermUgly(FILE *fp, NArrPtr*table, char *id, int ndigits, int columns, int flaggs);

NArrPtr *gcdm_clone_answer(NArrPtr *answer, DArrPtr *tabtimes);

NArrPtr *gcdm_combine_answer(double alpha_g, NArrPtr *gsol, double alpha_a, NArrPtr *answer, DArrPtr *tabtimes);

void integrate_set_zero_mrna(int arg);
#ifdef LIBSVM
#include <libsvm/svm.h>
#elif FANN
#include <floatfann.h>
#endif
#ifdef LIBSVM
	struct svm_node *ConvertAnswerToLibSVM(NArrPtr *answer, NArrPtr *answer_mut, int *max_nr_attr);
#elif FANN
	fann_type *ConvertAnswerToFANN(NArrPtr *answer, NArrPtr *answer_mut);
#endif

#ifdef __cplusplus
}
#endif

#endif /* _INTEGRATE_H */
