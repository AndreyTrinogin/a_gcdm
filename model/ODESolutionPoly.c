#include <float.h>
#ifdef ICC
#include <mathimf.h>
#else
#include <math.h>
#endif
#include <stdlib.h>
#include <stdio.h>

#include <ODESolutionPoly.h>
#include <InterpolatingFunctionPoly.h>
#include <SolutionTimeInterval.h>

static int order = 2;

void ODESolutionPoly_setInterpolatingOrder(int interpolating_order)
{
	order = interpolating_order;
}
  
ODESolutionPoly*ODESolutionPoly_createWith(double tin,double tout,int n)
{
        int i;
	ODESolutionPoly*Solution;
	
	Solution = (ODESolutionPoly*)malloc(sizeof(ODESolutionPoly));

	Solution->NFuncs = n;
	Solution->interpolating_func = (InterpolatingFunctionPoly**)calloc(n,sizeof(InterpolatingFunctionPoly*));
	for ( i = 0; i < n; i++ ) {
		Solution->interpolating_func[i] = InterpolatingFunctionPoly_createWith(order);
	}

	Solution->time_interval = SolutionTimeInterval_creatWith(tin,tout);

	return Solution;
}


void ODESolutionPoly_setStepsize(ODESolutionPoly*Solution, double stepsize)
{
	Solution->Stepsize = stepsize;
}


void ODESolutionPoly_setNSteps(ODESolutionPoly*Solution, int nsteps)
{
	Solution->NSteps = nsteps;
}

void ODESolutionPoly_initInterpolating(ODESolutionPoly*Solution, double*v)
{
	int i;
	for ( i = 0; i < Solution->NFuncs; i++ ) {
		InterpolatingFunctionPoly_init(Solution->interpolating_func[i], v[i],Solution->time_interval);
	}
}

void ODESolutionPoly_addtoInterpolating(ODESolutionPoly*Solution,double*v,double time)
{
	int i;
	for ( i = 0; i < Solution->NFuncs; i++ ) {
		InterpolatingFunctionPoly_addto(Solution->interpolating_func[i], v[i],time);
	}
}

void ODESolutionPoly_makeInterpolating(ODESolutionPoly*Solution)
{
	int i;
	for ( i = 0; i < Solution->NFuncs; i++ ) {
		InterpolatingFunctionPoly_make(Solution->interpolating_func[i]);
	}
}

void ODESolutionPoly_delete(ODESolutionPoly*Solution)
{
        int i;

	for ( i = 0; i < Solution->NFuncs; i++ ) {
		InterpolatingFunctionPoly_delete(Solution->interpolating_func[i]);
	}
	free(Solution->interpolating_func);

	SolutionTimeInterval_delete(Solution->time_interval);

	free(Solution);
}

void ODESolutionPoly_evaluateAtTime(ODESolutionPoly*Solution,double*v,double time)
{
	int i;
	for ( i = 0; i < Solution->NFuncs; i++ ) {
		v[i] = InterpolatingFunctionPoly_evaluate(Solution->interpolating_func[i], time);
	}
}

