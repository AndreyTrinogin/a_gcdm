#ifdef ICC
#include <mathimf.h>
#else
#include <math.h>
#endif
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>                                          /* for getopt */

#include <error.h>
#include <ffio.h>
#include <ODESolve.h>
#include <maternal.h>
#include <score.h>
#include <zygotic.h>
#include <integrate.h>
#include <tweak.h>


/*** Constants *************************************************************/

#define  OPTS      ":A:a:dDf:g:Ghi:noO:pr:s:S:vw:x:X:z:"  /* command line option string */


/*** Help, usage and version messages **************************************/

static const char usage[]  =

"Usage: stat [-a <accuracy>] [-D] [-f <float_prec>] [-g <g(u)>] [-G]\n"
"                  [-h] [-i <stepsize>] [-o] [-p] [-s <solver>] [-v]\n"
"                  [-x <sect_title>]\n"
"                  <datafile>\n";

static const char help[]   =

"Usage: gcdm6_stat [options] <file> <output file name>\n\n"

"Arguments:\n"
"  <file>              file containing the list of datafiles\n\n"

"  -A <cost flag>      msq or abs\n"
"  -a <accuracy>       solver accuracy for adaptive stepsize ODE solvers\n"
"  -d                  print details\n"
"  -D                  debugging mode, prints all kinds of debugging info\n"
"  -f <float_prec>     float precision of output is <float_prec>\n"
"  -g <g(u)>           chooses g(u): e = exp, h = hvs, s = sqrt, t = tanh\n"
"  -G                  gut mode: prints squared diffs for all datapoints\n"
"  -h                  prints this help message\n"
"  -i <stepsize>       sets ODE solver stepsize (in minutes)\n"
"  -n                  print number of datapoints\n"
"  -o                  use oldstyle cell division times (3 div only)\n"
"  -O <model>          hyb, simple, nomit or double\n"
"  -p                  prints penalty in addition to score and RMS\n"
"  -r <threshold>      topology threshold\n"
"  -s <solver>         selects solver\n"
"  -S <cost func>      selects quality functional: chisq (default), abs or deriv\n"
"  -v                  print version and compilation date\n"
"  -w <out_file>       write output to <out_file> instead of <output file name>\n"
"  -x <sect_title>     uses equation paramters from section <sect_title>\n\n"
"  -X <equations>      selects equations\n\n"
"  -z <gast_time>      set custom gastrulation time (max. 10'000 (min));\n"
"                      custom gastrulation MUST be later than the normal one\n"

"Please report bugs to <kozlov@spbcas.ru>. Thank you!\n";

static const char verstring[] = 

"%s version %s\n"
"compiled by:      %s\n"
"         on:      %s\n"
"      using:      %s\n"
"      flags:      %s\n"
"       date:      %s at %s\n";

static int functional_flag = 0;
static int dist_flag = 0;

typedef struct Stat {
	EqParms*parms;
	Tweak*topology;
	int nalleles;
	double penalty;
	int *ndps;
	double *scores;
	double *rmss;
	double *aics;
	double *bics;
	int *neqps;
	double rms;
	double score;
	double aic;
	double bic;
	int neqp;
	int ndp;
	double kappa;
	int i_index;
	int j_index;
	EqParms*kappa_parms;
} Stat;

int tweak_equal(Tweak*y1, Tweak*y2);
double eqparms_distance(EqParms*Pout, EqParms*Pin);
double eqparms_distance_dist(EqParms*Pout, EqParms*Pin, EqParms*dist, int*i_index, int*j_index);
/*** Main program **********************************************************/

int main(int argc, char **argv)
{
	int           c;                   /* used to parse command line options */
	FILE          *infile, *fptr;                     /* pointer to input data file */
	char          *dumpfile;               /* name of debugging model output */
	FILE          *dumpptr;          /* pointer for dumping raw model output */
	char          *slogfile;                      /* name of solver log file */
	FILE          *slog;                          /* solver log file pointer */
/* the follwoing few variables are read as arguments to command line opts  */
	int           ndigits     = 12;     /* precision of output, default = 12 */
	int           gutndigits  = 6;      /* precision for gut output, def = 6 */
	int           penaltyflag = 0;              /* flag for printing penalty */
	int           ndataflag = 0;              /* flag for printing penalty */
	int           rmsflag     = 1;     /* flag for printing root mean square */
	int           gutflag     = 0;         /* flag for root square diff guts */
	int           detailflag     = 0;         /* flag for root square diff guts */
	double        stepsize    = 1.;                   /* stepsize for solver */
	double        accuracy    = 0.001;                /* accuracy for solver */
	char          *section_title;                  /* parameter section name */
/* two format strings */
	char          *format;                           /* output format string */
	char          *precision;   /* precision string for output format string */
/* output values */
	double        chisq       = 0.; /* sum of squared differences for output */
	double        penalty     = 0.;                  /* variable for penalty */
	double        rms         = 0.;                /* root mean square (RMS) */
	double        ms          = 0.;             /* mean square (= rms * rms) */
	int           ndp         = 0;                   /* number of datapoints */
 /* the following lines define a pointers to:                               */
/*            - pd:    dvdt function, currently only DvdtOrig in zygotic.c */
/*            - pj:    Jacobian function, in zygotic.c                     */
/*                                                                         */
/* NOTE: ps (solver) is declared as global in integrate.h                  */

/*  void (*pd)(double *, double, double *, int, int);
  void (*pj)(double, double *, double *, double **, int);*/
/* we need only solver name */
	char *solvername, *solvername0, *solver;
	Derivative2 pd = Dvdt2Orig;
/* external declarations for command line option parsing (unistd.h) */
	extern char   *optarg;                   /* command line option argument */
	extern int    optind;              /* pointer to current element of argv */
	extern int    optopt;             /* contain option character upon error */
	int loop_kounter;
	short isCustom = 0;
	char**file_array;
	int n_files_in_array;
	char*buffer_infile;
	int i_files_in_array, i, j, k;
	int rhs;
	Stat*staty;
	EqParms*avg_parms, *stdev_parms, *dist_parms;
	double kappa;
	Tweak*topology_m, *topology_0, *topology_p, *topology;
	double topology_threshold = 0.005;
	char          *outfile;               /* name of debugging model output */
	FILE          *outptr;          /* pointer for dumping raw model output */
	int *ndps;
	double*scores, dot, adot;
/* following part sets default values for deriv, Jacobian and solver funcs */
	solvername = (char *)calloc(MAX_RECORD, sizeof(char));
	solvername = strcpy(solvername, "rkso");
	solvername0 = (char *)calloc(MAX_RECORD, sizeof(char));
	solvername0 = strcpy(solvername0, "bs");
	solver = NULL;
	rhs = 1;
	section_title = (char *)calloc(MAX_RECORD, sizeof(char));
	section_title = strcpy(section_title, "eqparms");  /* default is eqparms */
/* following part parses command line for options and their arguments      */
	optarg = NULL;
	outfile = NULL;
	while ( (c = getopt(argc, argv, OPTS)) != -1 ) 
		switch (c) {
			case 'a':
				accuracy = atof(optarg);
				if ( accuracy <= 0 )
					error("stat: accuracy (%g) is too small", accuracy);
			break;
			case 'A':
				if( !(strcmp(optarg, "msq")) )
					functional_flag = 0;
				else if ( !(strcmp(optarg, "abs"))) 
					functional_flag = 3;
				else 
					error("stat: %s is an invalid flag", optarg); 
			break;
			case 'd':                                 /* -D runs in debugging mode */
				detailflag = 1;
			break;
			case 'D':                                 /* -D runs in debugging mode */
				debug = 1;
			break;
			case 'f':
				ndigits    = atoi(optarg);          /* -f determines float precision */
				gutndigits = atoi(optarg);
				if ( ndigits < 0 )
					error("stat: what exactly would a negative precision be???");
				if ( ndigits > MAX_PRECISION )
					error("stat: max. float precision is %d!", MAX_PRECISION);
			break;
			case 'g':                                    /* -g choose g(u) function */
				if( !(strcmp(optarg, "s")) )
					gofu = Sqrt;                          
				else if ( !(strcmp(optarg, "t"))) 
					gofu = Tanh;
				else if ( !(strcmp(optarg, "e"))) 
					gofu = Exp;
				else if ( !(strcmp(optarg, "h"))) 
					gofu = Hvs;
        else if ( !(strcmp(optarg, "l"))) 
					gofu = Lin;
				else 
					error("stat: %s is an invalid g(u), should be e, h, s, t or l", optarg); 
			break;
			case 'G':                                              /* -G guts mode */
				gutflag = 1;
			break;
			case 'h':                                            /* -h help option */
				PrintMsg(help, 0);
			break;
			case 'i':                                      /* -i sets the stepsize */
				stepsize = atof(optarg);
				if ( stepsize < 0 )
					error("stat: going backwards? (hint: check your -i)");
				if ( stepsize == 0 ) 
					error("stat: going nowhere? (hint: check your -i)");
				if ( stepsize > MAX_STEPSIZE )
					error("stat: stepsize %g too large (max. is %g)", stepsize, MAX_STEPSIZE);
			break;
			case 'n':                                              /* -G guts mode */
				ndataflag = 1;
			break;
			case 'o':             /* -o sets old division style (ndivs = 3 only! ) */
				olddivstyle = 1;
			break;
			case 'O':
				if (!strcmp(optarg, "hyb")) {
					model = HYBRID;
				} else if (!strcmp(optarg, "simple")) {
					model = SIMPLE;
				} else if (!strcmp(optarg, "nomit")) {
					model = NOMITOSIS;
				} else if (!strcmp(optarg, "double")) {
					model = DOUBLING;
				} else {
					error("stat: invalid model (-O %s)", optarg);
				}
			break;
			case 'p':
				penaltyflag = 1;
			break;
			case 'r':
				topology_threshold = atof(optarg);
			break;
			case 's':
				solver = (char *)calloc(MAX_RECORD, sizeof(char));
				solver = strcpy(solver, optarg);
			break;
			case 'S':
				if (!strcmp(optarg, "deriv")) {
					gcdm_score_init_eval("deriv");
				} else if (!strcmp(optarg, "chisq")) {
					gcdm_score_init_eval("chisq");
				} else if (!strcmp(optarg, "abs")) {
					gcdm_score_init_eval("abs");
				} else {
					error("printscore: invalid deriv (-S %s)", optarg);
				}
			break;
			case 'v':                                  /* -v prints version number */
				fprintf(stderr, verstring, *argv, VERS, USR, MACHINE, COMPILER, FLAGS, __DATE__, __TIME__);
				exit(0);
			break;
			case 'w':
				outfile = (char *)calloc(MAX_RECORD, sizeof(char));
				outfile = strcpy(outfile, optarg);
			break;
			case 'x':
				if ( (strcmp(optarg, "input")) && (strcmp(optarg, "eqparms")) && (strcmp(optarg, "tonameters")) && (strcmp(optarg, "toneqparms")) && (strcmp(optarg, "parameters")) )
					error("unfold: invalid section title (%s)", optarg);
				section_title = strcpy(section_title, optarg);
			break;
			case 'X':
				if (!strcmp(optarg, "orig2")) {
					rhs = 0;
					set_deriv(Dvdt2Orig);
				} else if (!strcmp(optarg, "orig3")) {
					rhs = 0;
					set_deriv(Dvdt3Orig);
				} else if (!strcmp(optarg, "orig31stDir")) {
					rhs = 0;
					set_deriv(Dvdt31stTelDirichlet);
            } else if (!strcmp(optarg, "orig31stNeu")) {
					rhs = 0;
					set_deriv(Dvdt31stTelNeumann);
				} else if (!strcmp(optarg, "orig32ndNeu")) {
					rhs = 0;
					set_deriv(Dvdt32ndTelNeumann);
				} else if (!strcmp(optarg, "orig3M")) {
					rhs = 0;
					set_deriv(Dvdt3TelMick);
            } else if (!strcmp(optarg, "orig31stJeffDir")) {
					rhs = 0;
					set_deriv(Dvdt31stJeffDirichlet);
            } else if (!strcmp(optarg, "orig31stJeffNeu")) {
					rhs = 0;
					set_deriv(Dvdt31stJeffNeumann);
				} else if (!strcmp(optarg, "bump")) {
					rhs = 0;
					set_deriv(Dvdt2Bump);
				} else if (!strcmp(optarg, "mob1")) {
					rhs = 0;
					set_deriv(Dvdt2Mob1);
				} else if (!strcmp(optarg, "orig")) {
					rhs = 1;
				} else {
					error("stat: invalid deriv (-X %s)", optarg);
				}
			break;
			case 'z':
				isCustom = 1;
				custom_gast = atof(optarg);
				if ( custom_gast < 0. )
					error("stat: gastrulation time must be positive");
				if ( custom_gast > 10000. )
					error("stat: gastrulation time must be smaller than 10'000");
			break;
			case ':':
				error("stat: need an argument for option -%c", optopt);
			break;
			case '?':
			default:
				error("stat: unrecognized option -%c", optopt);
		}
/* error check 
	if ( (argc - (optind - 1 )) != 2 )
		PrintMsg(usage, 1);
/* dynamic allocation of output format strings */
	precision = (char *)calloc(MAX_RECORD, sizeof(char));
	format    = (char *)calloc(MAX_RECORD, sizeof(char));
/* initialize guts */
	if ( gutflag ) 
		SetGuts(gutflag, gutndigits);
/* let's get started and open data file here */
	infile = fopen(argv[optind],"r");
	if ( !infile )
		file_error("stat");
	if ( !outfile ) {
		outfile = (char *)calloc(MAX_RECORD, sizeof(char));
		outfile = strcpy(outfile, argv[optind + 1]);
	}
	n_files_in_array = 0;
	file_array = NULL;
	buffer_infile = (char *)calloc(MAX_RECORD, sizeof(char));
	while ( ( 1 == fscanf(infile, "%s", buffer_infile) ) ) {
		file_array = (char **)realloc(file_array, (n_files_in_array + 1) * sizeof(char*));
		file_array[ n_files_in_array ] = (char *)calloc(MAX_RECORD, sizeof(char));
		file_array[ n_files_in_array ] = strcpy(file_array[ n_files_in_array ] , buffer_infile);
		n_files_in_array++;
	}
	fclose(infile);
	free(buffer_infile);
	staty = (Stat*)calloc(n_files_in_array, sizeof(Stat));
/* if debugging: initialize solver log file */
	if ( debug ) {
		slogfile = (char *)calloc(MAX_RECORD, sizeof(char));
		sprintf(slogfile, "%s.slog", argv[optind]);
		slog = fopen(slogfile, "w");              /* delete existing slog file */
		fclose(slog);
		slog = fopen(slogfile, "a");            /* now keep open for appending */
	}
/* Initialization code here: InitZygote() initializes everything needed    *
 * for running the model, InitScoring() everything for scoring; InitStep-  *
 * size sets solver stepsize and accuracy in score.c to pass it on to      *
 * Blastoderm                                                              */
	i_files_in_array = 0;
	infile = fopen(file_array[i_files_in_array],"r");
	if ( !infile )
		file_error("stat");
	InitZygote(infile, section_title);
/*	Scoring_setSolverName(solvername);*/
/*	if ( rhs == 0 )*/
/*		gcdm_score_set_solver(0, solvername);*/
/*	if ( rhs == 1 )*/
/*		gcdm_score_set_solver(1, solvername0);*/
/*	InitScoring(infile);*/
	if ( rhs == 0 ) {
		if ( solver )
			gcdm_score_set_solver(0, solver);
		else
			gcdm_score_set_solver(0, solvername);
		gcdm_score_set_solver(1, solvername0);
	} else if ( rhs == 1 ) {
		gcdm_score_set_solver(0, solvername);
		if ( solver )
			gcdm_score_set_solver(1, solver);
		else
			gcdm_score_set_solver(1, solvername0);
	}
	gcdm_score_init(infile);
	gcdm_score_set_hints(2, 0, rhs);
	topology = ReadTweak(infile);
	topology_m = ReadTweak(infile);
	topology_0 = ReadTweak(infile);
	topology_p = ReadTweak(infile);
	kappa = 0;
	for ( i = 0; i < defs->ngenes; i++ ) {
		for ( j = 0; j < defs->ngenes; j++ ) {
			topology_m->Ttweak[i * defs->ngenes + j] =0;
			topology_p->Ttweak[i * defs->ngenes + j] =0;
			topology_0->Ttweak[i * defs->ngenes + j] =0;
		}
		for ( j = 0; j < defs->egenes; j++ ) {
			topology_m->Etweak[i * defs->egenes + j] =0;
			topology_p->Etweak[i * defs->egenes + j] =0;
			topology_0->Etweak[i * defs->egenes + j] =0;
		}
		for ( j = 0; j < defs->mgenes; j++ ) {
			topology_m->Mtweak[i * defs->mgenes + j] =0;
			topology_p->Mtweak[i * defs->mgenes + j] =0;
			topology_0->Mtweak[i * defs->mgenes + j] =0;
		}
		for ( j = 0; j < defs->ncouples; j++ ) {
			topology_m->Ptweak[i * defs->ncouples + j] =0;
			topology_p->Ptweak[i * defs->ncouples + j] =0;
			topology_0->Ptweak[i * defs->ncouples + j] =0;
		}
	}
	InitStepsize(stepsize, accuracy, slog, file_array[i_files_in_array]);
	if ( ( fptr = FindSection(infile, "parameters") ) ) {
		dist_parms = InstallParm();
		dist_flag = 1;
		ReReadParameters(dist_parms, fptr, "parameters");
		staty[i_files_in_array].kappa_parms = InstallParm();
		staty[i_files_in_array].kappa = eqparms_distance_dist(GetParameters(), dist_parms, staty[i_files_in_array].kappa_parms, &(staty[i_files_in_array].i_index), &(staty[i_files_in_array].j_index));
		kappa += staty[i_files_in_array].kappa;
	}
	avg_parms = InstallParm();
	stdev_parms = InstallParm();
	staty[i_files_in_array].parms = CopyParm(GetParameters());
	staty[i_files_in_array].penalty = GetPenalty();
/*	staty[i_files_in_array].score = Score();*/
 	staty[i_files_in_array].score = gcdm_score_score();
	staty[i_files_in_array].ndp = GetNDatapoints();
	staty[i_files_in_array].topology = ReadTweak(infile);
	staty[i_files_in_array].neqp = gcdm_tweak_get_ntweaks(staty[i_files_in_array].topology);
	fclose(infile);
	staty[i_files_in_array].rms = sqrt( (staty[i_files_in_array].score - staty[i_files_in_array].penalty) / (double)staty[i_files_in_array].ndp );
	staty[i_files_in_array].aic = 2.0 * staty[i_files_in_array].neqp + staty[i_files_in_array].ndp * log( staty[i_files_in_array].score / (double)staty[i_files_in_array].ndp );
	staty[i_files_in_array].bic = log( (double)staty[i_files_in_array].ndp ) * staty[i_files_in_array].neqp + staty[i_files_in_array].ndp * log( staty[i_files_in_array].score / (double)staty[i_files_in_array].ndp );
	ndps = gcdm_get_ndp();
	scores = gcdm_get_score();
	staty[i_files_in_array].nalleles = nalleles;
	staty[i_files_in_array].scores = (double*)calloc(staty[i_files_in_array].nalleles, sizeof(double));
	staty[i_files_in_array].rmss = (double*)calloc(staty[i_files_in_array].nalleles, sizeof(double));
	staty[i_files_in_array].aics = (double*)calloc(staty[i_files_in_array].nalleles, sizeof(double));
	staty[i_files_in_array].bics = (double*)calloc(staty[i_files_in_array].nalleles, sizeof(double));
	staty[i_files_in_array].neqps = (int*)calloc(staty[i_files_in_array].nalleles, sizeof(int));
	staty[i_files_in_array].ndps = (int*)calloc(staty[i_files_in_array].nalleles, sizeof(int));
	for ( k = 0; k < nalleles; k++ ) {
		staty[i_files_in_array].scores[k] = scores[k];
		staty[i_files_in_array].ndps[k] = ndps[k];
		staty[i_files_in_array].rmss[k] = sqrt(scores[k]/(double)ndps[k]);
		staty[i_files_in_array].neqps[k] = staty[i_files_in_array].neqp;
		staty[i_files_in_array].aics[k] = 2.0 * staty[i_files_in_array].neqps[k] + staty[i_files_in_array].ndps[k] * log( staty[i_files_in_array].scores[k] / (double)staty[i_files_in_array].ndps[k] );
		staty[i_files_in_array].bics[k] = log( (double)staty[i_files_in_array].ndps[k] ) * staty[i_files_in_array].neqps[k] + staty[i_files_in_array].ndps[k] * log( staty[i_files_in_array].scores[k] / (double)staty[i_files_in_array].ndps[k] );
	}
	for ( i = 0; i < defs->ngenes; i++ ) {
		avg_parms->R[i] = staty[i_files_in_array].parms->R[i];
		avg_parms->h[i] = staty[i_files_in_array].parms->h[i];
		avg_parms->d[i] = staty[i_files_in_array].parms->d[i];
		avg_parms->m[i] = staty[i_files_in_array].parms->m[i];
		avg_parms->mm[i] = staty[i_files_in_array].parms->mm[i];
		avg_parms->tau[i] = staty[i_files_in_array].parms->tau[i];
		avg_parms->lambda[i] = staty[i_files_in_array].parms->lambda[i];
		stdev_parms->R[i] = staty[i_files_in_array].parms->R[i] * staty[i_files_in_array].parms->R[i];
		stdev_parms->h[i] = staty[i_files_in_array].parms->h[i] * staty[i_files_in_array].parms->h[i];
		stdev_parms->d[i] = staty[i_files_in_array].parms->d[i] * staty[i_files_in_array].parms->d[i];
		stdev_parms->m[i] = staty[i_files_in_array].parms->m[i] * staty[i_files_in_array].parms->m[i];
		stdev_parms->mm[i] = staty[i_files_in_array].parms->mm[i] * staty[i_files_in_array].parms->mm[i];
		stdev_parms->tau[i] = staty[i_files_in_array].parms->tau[i] * staty[i_files_in_array].parms->tau[i];
		stdev_parms->lambda[i] = staty[i_files_in_array].parms->lambda[i] * staty[i_files_in_array].parms->lambda[i];
		for ( j = 0; j < defs->ngenes; j++ ) {
			avg_parms->T[i * defs->ngenes + j] = staty[i_files_in_array].parms->T[i * defs->ngenes + j];
			stdev_parms->T[i * defs->ngenes + j] = staty[i_files_in_array].parms->T[i * defs->ngenes + j] * staty[i_files_in_array].parms->T[i * defs->ngenes + j];
			if ( staty[i_files_in_array].parms->T[i * defs->ngenes + j] < -topology_threshold ) {
				staty[i_files_in_array].topology->Ttweak[i * defs->ngenes + j] = -1;
				topology_m->Ttweak[i * defs->ngenes + j] +=1;
			} else if ( staty[i_files_in_array].parms->T[i * defs->ngenes + j] > topology_threshold ) {
				staty[i_files_in_array].topology->Ttweak[i * defs->ngenes + j] = 1;
				topology_p->Ttweak[i * defs->ngenes + j] +=1;
			} else {
				staty[i_files_in_array].topology->Ttweak[i * defs->ngenes + j] = 0;
				topology_0->Ttweak[i * defs->ngenes + j] +=1;
			}
		}
		for ( j = 0; j < defs->egenes; j++ ) {
			avg_parms->E[i * defs->egenes + j] = staty[i_files_in_array].parms->E[i * defs->egenes + j];
			stdev_parms->E[i * defs->egenes + j] = staty[i_files_in_array].parms->E[i * defs->egenes + j] * staty[i_files_in_array].parms->E[i * defs->egenes + j];
			if ( staty[i_files_in_array].parms->E[i * defs->egenes + j] < -topology_threshold ) {
				staty[i_files_in_array].topology->Etweak[i * defs->egenes + j] = -1;
				topology_m->Etweak[i * defs->egenes + j] +=1;
			} else if ( staty[i_files_in_array].parms->E[i * defs->egenes + j] > topology_threshold ) {
				staty[i_files_in_array].topology->Etweak[i * defs->egenes + j] = 1;
				topology_p->Etweak[i * defs->egenes + j] +=1;
			} else {
				staty[i_files_in_array].topology->Etweak[i * defs->egenes + j] = 0;
				topology_0->Etweak[i * defs->egenes + j] +=1;
			}
		}
		for ( j = 0; j < defs->mgenes; j++ ) {
			avg_parms->M[i * defs->mgenes + j] = staty[i_files_in_array].parms->M[i * defs->mgenes + j];
			stdev_parms->M[i * defs->mgenes + j] = staty[i_files_in_array].parms->M[i * defs->mgenes + j] * staty[i_files_in_array].parms->M[i * defs->mgenes + j];
			if ( staty[i_files_in_array].parms->M[i * defs->mgenes + j] < -topology_threshold ) {
				staty[i_files_in_array].topology->Mtweak[i * defs->mgenes + j] = -1;
				topology_m->Mtweak[i * defs->mgenes + j] +=1;
			} else if ( staty[i_files_in_array].parms->M[i * defs->mgenes + j] > topology_threshold ) {
				staty[i_files_in_array].topology->Mtweak[i * defs->mgenes + j] = 1;
				topology_p->Mtweak[i * defs->mgenes + j] +=1;
			} else {
				staty[i_files_in_array].topology->Mtweak[i * defs->mgenes + j] = 0;
				topology_0->Mtweak[i * defs->mgenes + j] +=1;
			}
		}
		for ( j = 0; j < defs->ncouples; j++ ) {
			avg_parms->P[i * defs->ncouples + j] = staty[i_files_in_array].parms->P[i * defs->ncouples + j];
			stdev_parms->P[i * defs->ncouples + j] = staty[i_files_in_array].parms->P[i * defs->ncouples + j] * staty[i_files_in_array].parms->P[i * defs->ncouples + j];
			if ( staty[i_files_in_array].parms->P[i * defs->ncouples + j] < -topology_threshold ) {
				staty[i_files_in_array].topology->Ptweak[i * defs->ncouples + j] = -1;
				topology_m->Ptweak[i * defs->ncouples + j] +=1;
			} else if ( staty[i_files_in_array].parms->P[i * defs->ncouples + j] > topology_threshold ) {
				staty[i_files_in_array].topology->Ptweak[i * defs->ncouples + j] = 1;
				topology_p->Ptweak[i * defs->ncouples + j] +=1;
			} else {
				staty[i_files_in_array].topology->Ptweak[i * defs->ncouples + j] = 0;
				topology_0->Ptweak[i * defs->ncouples + j] +=1;
			}
		}
	}
	for ( i_files_in_array = 1; i_files_in_array < n_files_in_array; i_files_in_array++ ) {
		infile = fopen(file_array[i_files_in_array],"r");
		if ( !infile )
			file_error("stat");
		ReReadParameters(GetParameters(), infile, section_title);
		if ( dist_flag == 1 ) {
			staty[i_files_in_array].kappa_parms = InstallParm();
			staty[i_files_in_array].kappa = eqparms_distance_dist(GetParameters(), dist_parms, staty[i_files_in_array].kappa_parms, &(staty[i_files_in_array].i_index), &(staty[i_files_in_array].j_index));
/*			staty[i_files_in_array].kappa = eqparms_distance(GetParameters(), dist_parms);*/
			kappa += staty[i_files_in_array].kappa;
		}
		staty[i_files_in_array].parms = CopyParm(GetParameters());
		staty[i_files_in_array].penalty = GetPenalty();
		staty[i_files_in_array].score = gcdm_score_score();
		staty[i_files_in_array].ndp = GetNDatapoints();
		staty[i_files_in_array].topology = ReadTweak(infile);
		staty[i_files_in_array].neqp = gcdm_tweak_get_ntweaks(staty[i_files_in_array].topology);
		staty[i_files_in_array].rms = sqrt( (staty[i_files_in_array].score - staty[i_files_in_array].penalty) / (double)staty[i_files_in_array].ndp );
		staty[i_files_in_array].aic = 2.0 * staty[i_files_in_array].neqp + staty[i_files_in_array].ndp * log( staty[i_files_in_array].score / (double)staty[i_files_in_array].ndp );
		staty[i_files_in_array].bic = log( (double)staty[i_files_in_array].ndp ) * staty[i_files_in_array].neqp + staty[i_files_in_array].ndp * log( staty[i_files_in_array].score / (double)staty[i_files_in_array].ndp );
		ndps = gcdm_get_ndp();
		scores = gcdm_get_score();
		staty[i_files_in_array].nalleles = nalleles;
		staty[i_files_in_array].scores = (double*)calloc(staty[i_files_in_array].nalleles, sizeof(double));
		staty[i_files_in_array].rmss = (double*)calloc(staty[i_files_in_array].nalleles, sizeof(double));
		staty[i_files_in_array].aics = (double*)calloc(staty[i_files_in_array].nalleles, sizeof(double));
		staty[i_files_in_array].bics = (double*)calloc(staty[i_files_in_array].nalleles, sizeof(double));
		staty[i_files_in_array].neqps = (int*)calloc(staty[i_files_in_array].nalleles, sizeof(int));
		staty[i_files_in_array].ndps = (int*)calloc(staty[i_files_in_array].nalleles, sizeof(int));
		for ( k = 0; k < nalleles; k++ ) {
			staty[i_files_in_array].scores[k] = scores[k];
			staty[i_files_in_array].ndps[k] = ndps[k];
			staty[i_files_in_array].rmss[k] = sqrt((scores[k] - penalty)/(double)ndps[k]);
			staty[i_files_in_array].neqps[k] = staty[i_files_in_array].neqp;
			staty[i_files_in_array].aics[k] = 2.0 * staty[i_files_in_array].neqps[k] + staty[i_files_in_array].ndps[k] * log( staty[i_files_in_array].scores[k] / (double)staty[i_files_in_array].ndps[k] );
			staty[i_files_in_array].bics[k] = log( (double)staty[i_files_in_array].ndps[k] ) * staty[i_files_in_array].neqps[k] + staty[i_files_in_array].ndps[k] * log( staty[i_files_in_array].scores[k] / (double)staty[i_files_in_array].ndps[k] );
		}
		fclose(infile);
		for ( i = 0; i < defs->ngenes; i++ ) {
			avg_parms->R[i] += staty[i_files_in_array].parms->R[i];
			avg_parms->h[i] += staty[i_files_in_array].parms->h[i];
			avg_parms->d[i] += staty[i_files_in_array].parms->d[i];
			avg_parms->m[i] += staty[i_files_in_array].parms->m[i];
			avg_parms->mm[i] += staty[i_files_in_array].parms->mm[i];
			avg_parms->tau[i] += staty[i_files_in_array].parms->tau[i];
			avg_parms->lambda[i] += staty[i_files_in_array].parms->lambda[i];
			stdev_parms->R[i] += staty[i_files_in_array].parms->R[i] * staty[i_files_in_array].parms->R[i];
			stdev_parms->h[i] += staty[i_files_in_array].parms->h[i] * staty[i_files_in_array].parms->h[i];
			stdev_parms->d[i] += staty[i_files_in_array].parms->d[i] * staty[i_files_in_array].parms->d[i];
			stdev_parms->m[i] += staty[i_files_in_array].parms->m[i] * staty[i_files_in_array].parms->m[i];
			stdev_parms->mm[i] += staty[i_files_in_array].parms->mm[i] * staty[i_files_in_array].parms->mm[i];
			stdev_parms->tau[i] += staty[i_files_in_array].parms->tau[i] * staty[i_files_in_array].parms->tau[i];
			stdev_parms->lambda[i] += staty[i_files_in_array].parms->lambda[i] * staty[i_files_in_array].parms->lambda[i];
			for ( j = 0; j < defs->ngenes; j++ ) {
				avg_parms->T[i * defs->ngenes + j] += staty[i_files_in_array].parms->T[i * defs->ngenes + j];
				stdev_parms->T[i * defs->ngenes + j] += staty[i_files_in_array].parms->T[i * defs->ngenes + j] * staty[i_files_in_array].parms->T[i * defs->ngenes + j];
				if ( staty[i_files_in_array].parms->T[i * defs->ngenes + j] < -topology_threshold ) {
					staty[i_files_in_array].topology->Ttweak[i * defs->ngenes + j] = -1;
					topology_m->Ttweak[i * defs->ngenes + j] +=1;
				} else if ( staty[i_files_in_array].parms->T[i * defs->ngenes + j] > topology_threshold ) {
					staty[i_files_in_array].topology->Ttweak[i * defs->ngenes + j] = 1;
					topology_p->Ttweak[i * defs->ngenes + j] +=1;
				} else {
					staty[i_files_in_array].topology->Ttweak[i * defs->ngenes + j] = 0;
					topology_0->Ttweak[i * defs->ngenes + j] +=1;
				}
			}
			for ( j = 0; j < defs->egenes; j++ ) {
				avg_parms->E[i * defs->egenes + j] += staty[i_files_in_array].parms->E[i * defs->egenes + j];
				stdev_parms->E[i * defs->egenes + j] += staty[i_files_in_array].parms->E[i * defs->egenes + j] * staty[i_files_in_array].parms->E[i * defs->egenes + j];
				if ( staty[i_files_in_array].parms->E[i * defs->egenes + j] < -topology_threshold ) {
					staty[i_files_in_array].topology->Etweak[i * defs->egenes + j] = -1;
					topology_m->Etweak[i * defs->egenes + j] +=1;
				} else if ( staty[i_files_in_array].parms->E[i * defs->egenes + j] > topology_threshold ) {
					staty[i_files_in_array].topology->Etweak[i * defs->egenes + j] = 1;
					topology_p->Etweak[i * defs->egenes + j] +=1;
				} else {
					staty[i_files_in_array].topology->Etweak[i * defs->egenes + j] = 0;
					topology_0->Etweak[i * defs->egenes + j] +=1;
				}
			}
			for ( j = 0; j < defs->mgenes; j++ ) {
				avg_parms->M[i * defs->mgenes + j] += staty[i_files_in_array].parms->M[i * defs->mgenes + j];
				stdev_parms->M[i * defs->mgenes + j] += staty[i_files_in_array].parms->M[i * defs->mgenes + j] * staty[i_files_in_array].parms->M[i * defs->mgenes + j];
				if ( staty[i_files_in_array].parms->M[i * defs->mgenes + j] < -topology_threshold ) {
					staty[i_files_in_array].topology->Mtweak[i * defs->mgenes + j] = -1;
					topology_m->Mtweak[i * defs->mgenes + j] +=1;
				} else if ( staty[i_files_in_array].parms->M[i * defs->mgenes + j] > topology_threshold ) {
					staty[i_files_in_array].topology->Mtweak[i * defs->mgenes + j] = 1;
					topology_p->Mtweak[i * defs->mgenes + j] +=1;
				} else {
					staty[i_files_in_array].topology->Mtweak[i * defs->mgenes + j] = 0;
					topology_0->Mtweak[i * defs->mgenes + j] +=1;
				}
			}
			for ( j = 0; j < defs->ncouples; j++ ) {
				avg_parms->P[i * defs->ncouples + j] += staty[i_files_in_array].parms->P[i * defs->ncouples + j];
				stdev_parms->P[i * defs->ncouples + j] += staty[i_files_in_array].parms->P[i * defs->ncouples + j] * staty[i_files_in_array].parms->P[i * defs->ncouples + j];
				if ( staty[i_files_in_array].parms->P[i * defs->ncouples + j] < -topology_threshold ) {
					staty[i_files_in_array].topology->Ptweak[i * defs->ncouples + j] = -1;
					topology_m->Ptweak[i * defs->ncouples + j] +=1;
				} else if ( staty[i_files_in_array].parms->P[i * defs->ncouples + j] > topology_threshold ) {
					staty[i_files_in_array].topology->Ptweak[i * defs->ncouples + j] = 1;
					topology_p->Ptweak[i * defs->ncouples + j] +=1;
				} else {
					staty[i_files_in_array].topology->Ptweak[i * defs->ncouples + j] = 0;
					topology_0->Ptweak[i * defs->ncouples + j] +=1;
				}
			}
		}
	}
	for ( i = 0; i < defs->ngenes; i++ ) {
		avg_parms->R[i] /= n_files_in_array;
		dot = stdev_parms->R[i];
		adot = avg_parms->R[i];
		stdev_parms->R[i] = dot / ( n_files_in_array * ( n_files_in_array - 1.0 ) ) - adot * adot / ( n_files_in_array - 1.0 );
		avg_parms->h[i] /= n_files_in_array;
		dot = stdev_parms->h[i];
		adot = avg_parms->h[i];
		stdev_parms->h[i] = dot / ( n_files_in_array * ( n_files_in_array - 1 ) ) - adot * adot / ( n_files_in_array - 1 );
		avg_parms->d[i] /= n_files_in_array;
		dot = stdev_parms->d[i];
		adot = avg_parms->d[i];
		stdev_parms->d[i] = dot / ( n_files_in_array * ( n_files_in_array - 1 ) ) - adot * adot / ( n_files_in_array - 1 );
		avg_parms->m[i] /= n_files_in_array;
		dot = stdev_parms->m[i];
		adot = avg_parms->m[i];
		stdev_parms->m[i] = dot / ( n_files_in_array * ( n_files_in_array - 1 ) ) - adot * adot / ( n_files_in_array - 1 );
		avg_parms->mm[i] /= n_files_in_array;
		dot = stdev_parms->mm[i];
		adot = avg_parms->mm[i];
		stdev_parms->mm[i] = dot / ( n_files_in_array * ( n_files_in_array - 1 ) ) - adot * adot / ( n_files_in_array - 1 );
		avg_parms->tau[i] /= n_files_in_array;
		dot = stdev_parms->tau[i];
		adot = avg_parms->tau[i];
		stdev_parms->tau[i] = dot / ( n_files_in_array * ( n_files_in_array - 1 ) ) - adot * adot / ( n_files_in_array - 1 );
		avg_parms->lambda[i] /= n_files_in_array;
		dot = stdev_parms->lambda[i];
		adot = avg_parms->lambda[i];
		stdev_parms->lambda[i] = dot / ( n_files_in_array * ( n_files_in_array - 1 ) ) - adot * adot / ( n_files_in_array - 1 );
		for ( j = 0; j < defs->ngenes; j++ ) {
			avg_parms->T[i * defs->ngenes + j] /= n_files_in_array;
			dot = stdev_parms->T[i * defs->ngenes + j];
			adot = avg_parms->T[i * defs->ngenes + j];
			stdev_parms->T[i * defs->ngenes + j] = dot / ( n_files_in_array * ( n_files_in_array - 1 ) ) - adot * adot / ( n_files_in_array - 1 );
			if ( topology_m->Ttweak[i * defs->ngenes + j] == topology_0->Ttweak[i * defs->ngenes + j] && topology_m->Ttweak[i * defs->ngenes + j] == topology_p->Ttweak[i * defs->ngenes + j] )
				topology->Ttweak[i * defs->ngenes + j] = 2;
			else if ( topology_m->Ttweak[i * defs->ngenes + j] >= topology_0->Ttweak[i * defs->ngenes + j] && topology_m->Ttweak[i * defs->ngenes + j] >= topology_p->Ttweak[i * defs->ngenes + j] )
				topology->Ttweak[i * defs->ngenes + j] = -1;
			else if ( topology_0->Ttweak[i * defs->ngenes + j] >= topology_m->Ttweak[i * defs->ngenes + j] && topology_0->Ttweak[i * defs->ngenes + j] >= topology_p->Ttweak[i * defs->ngenes + j] )
				topology->Ttweak[i * defs->ngenes + j] = 0;
			else if ( topology_p->Ttweak[i * defs->ngenes + j] >= topology_0->Ttweak[i * defs->ngenes + j] && topology_p->Ttweak[i * defs->ngenes + j] >= topology_m->Ttweak[i * defs->ngenes + j] )
				topology->Ttweak[i * defs->ngenes + j] = 1;
		}
		for ( j = 0; j < defs->egenes; j++ ) {
			avg_parms->E[i * defs->egenes + j] /= n_files_in_array;
			dot = stdev_parms->E[i * defs->egenes + j];
			adot = avg_parms->E[i * defs->egenes + j];
			stdev_parms->E[i * defs->egenes + j] = dot / ( n_files_in_array * ( n_files_in_array - 1 ) ) - adot * adot / ( n_files_in_array - 1 );
			if ( topology_m->Etweak[i * defs->egenes + j] == topology_0->Etweak[i * defs->egenes + j] && topology_m->Etweak[i * defs->egenes + j] == topology_p->Etweak[i * defs->egenes + j] )
				topology->Etweak[i * defs->egenes + j] = 2;
			else if ( topology_m->Etweak[i * defs->egenes + j] >= topology_0->Etweak[i * defs->egenes + j] && topology_m->Etweak[i * defs->egenes + j] >= topology_p->Etweak[i * defs->egenes + j] ) {
				topology->Etweak[i * defs->egenes + j] = -1;
			} else if ( topology_0->Etweak[i * defs->egenes + j] >= topology_m->Etweak[i * defs->egenes + j] && topology_0->Etweak[i * defs->egenes + j] >= topology_p->Etweak[i * defs->egenes + j] )
				topology->Etweak[i * defs->egenes + j] = 0;
			else if ( topology_p->Etweak[i * defs->egenes + j] >= topology_0->Etweak[i * defs->egenes + j] && topology_p->Etweak[i * defs->egenes + j] >= topology_m->Etweak[i * defs->egenes + j] )
				topology->Etweak[i * defs->egenes + j] = 1;
		}
		for ( j = 0; j < defs->mgenes; j++ ) {
			avg_parms->M[i * defs->mgenes + j] /= n_files_in_array;
			dot = stdev_parms->M[i * defs->mgenes + j];
			adot = avg_parms->M[i * defs->mgenes + j];
			stdev_parms->M[i * defs->mgenes + j] = dot / ( n_files_in_array * ( n_files_in_array - 1 ) ) - adot * adot / ( n_files_in_array - 1 );
			if ( topology_m->Mtweak[i * defs->mgenes + j] == topology_0->Mtweak[i * defs->mgenes + j] && topology_m->Mtweak[i * defs->mgenes + j] == topology_p->Mtweak[i * defs->mgenes + j] )
				topology->Mtweak[i * defs->mgenes + j] = 2;
			else if ( topology_m->Mtweak[i * defs->mgenes + j] >= topology_0->Mtweak[i * defs->mgenes + j] && topology_m->Mtweak[i * defs->mgenes + j] >= topology_p->Mtweak[i * defs->mgenes + j] )
				topology->Mtweak[i * defs->mgenes + j] = -1;
			else if ( topology_0->Mtweak[i * defs->mgenes + j] >= topology_m->Mtweak[i * defs->mgenes + j] && topology_0->Mtweak[i * defs->mgenes + j] >= topology_p->Mtweak[i * defs->mgenes + j] )
				topology->Mtweak[i * defs->mgenes + j] = 0;
			else if ( topology_p->Mtweak[i * defs->mgenes + j] >= topology_0->Mtweak[i * defs->mgenes + j] && topology_p->Mtweak[i * defs->mgenes + j] >= topology_m->Mtweak[i * defs->mgenes + j] )
				topology->Mtweak[i * defs->mgenes + j] = 1;
		}
		for ( j = 0; j < defs->ncouples; j++ ) {
			avg_parms->P[i * defs->ncouples + j] /= n_files_in_array;
			dot = stdev_parms->P[i * defs->ncouples + j];
			adot = avg_parms->P[i * defs->ncouples + j];
			stdev_parms->P[i * defs->ncouples + j] = dot / ( n_files_in_array * ( n_files_in_array - 1 ) ) - adot * adot / ( n_files_in_array - 1 );
			if ( topology_m->Ptweak[i * defs->ncouples + j] == topology_0->Ptweak[i * defs->ncouples + j] && topology_m->Ptweak[i * defs->ncouples + j] == topology_p->Ptweak[i * defs->ncouples + j] )
				topology->Ptweak[i * defs->ncouples + j] = 2;
			else if ( topology_m->Ptweak[i * defs->ncouples + j] >= topology_0->Ptweak[i * defs->ncouples + j] && topology_m->Ptweak[i * defs->ncouples + j] >= topology_p->Ptweak[i * defs->ncouples + j] )
				topology->Ptweak[i * defs->ncouples + j] = -1;
			else if ( topology_0->Ptweak[i * defs->ncouples + j] >= topology_m->Ptweak[i * defs->ncouples + j] && topology_0->Ptweak[i * defs->ncouples + j] >= topology_p->Ptweak[i * defs->ncouples + j] )
				topology->Ptweak[i * defs->ncouples + j] = 0;
			else if ( topology_p->Ptweak[i * defs->ncouples + j] >= topology_0->Ptweak[i * defs->ncouples + j] && topology_p->Ptweak[i * defs->ncouples + j] >= topology_m->Ptweak[i * defs->ncouples + j] )
				topology->Ptweak[i * defs->ncouples + j] = 1;
		}
	}
	outptr = fopen(outfile, "w");
	if ( !outptr )
		file_error("stat");
	PrintParameters(outptr, avg_parms, "avg_parms", ndigits);
	fprintf(outptr,"\n\n");
	PrintParameters(outptr, stdev_parms, "stdev_parms", ndigits);
	fprintf(outptr,"\n\n");
	PrintTweak(outptr, topology_m, "topology_m");
	fprintf(outptr,"\n\n");
	PrintTweak(outptr, topology_0, "topology_0");
	fprintf(outptr,"\n\n");
	PrintTweak(outptr, topology_p, "topology_p");
	fprintf(outptr,"\n\n");
	fclose(outptr);
	outfile = strcat(outfile, ".csv");
	outptr = fopen(outfile, "w");
	if ( !outptr )
		file_error("stat");
	fprintf(outptr,"Par,");
	for ( i_files_in_array = 0; i_files_in_array < n_files_in_array; i_files_in_array++ ) {
		fprintf(outptr,"%s,", file_array[i_files_in_array]);
	}
	fprintf(outptr,"avg,stdev,top_m,top_0,top_p\n");
	for ( i = 0; i < defs->ngenes; i++ ) {
		fprintf(outptr,"R[%c],", defs->gene_ids[i]);
		for ( i_files_in_array = 0; i_files_in_array < n_files_in_array; i_files_in_array++ ) {
			fprintf(outptr,"%.*f,", ndigits, staty[i_files_in_array].parms->R[i]);
		}
		fprintf(outptr,"%.*f,%.*f,%d,%d,%d\n", ndigits, avg_parms->R[i], ndigits, stdev_parms->R[i], topology_m->Rtweak[i],topology_0->Rtweak[i],topology_p->Rtweak[i]);
		fprintf(outptr,"h[%c],", defs->gene_ids[i]);
		for ( i_files_in_array = 0; i_files_in_array < n_files_in_array; i_files_in_array++ ) {
			fprintf(outptr,"%.*f,", ndigits, staty[i_files_in_array].parms->h[i]);
		}
		fprintf(outptr,"%.*f,%.*f,%d,%d,%d\n", ndigits, avg_parms->h[i], ndigits, stdev_parms->h[i], topology_m->htweak[i],topology_0->htweak[i],topology_p->htweak[i]);
		fprintf(outptr,"d[%c],", defs->gene_ids[i]);
		for ( i_files_in_array = 0; i_files_in_array < n_files_in_array; i_files_in_array++ ) {
			fprintf(outptr,"%.*f,", ndigits, staty[i_files_in_array].parms->d[i]);
		}
		fprintf(outptr,"%.*f,%.*f,%d,%d,%d\n", ndigits, avg_parms->d[i], ndigits, stdev_parms->d[i], topology_m->dtweak[i],topology_0->dtweak[i],topology_p->dtweak[i]);
		fprintf(outptr,"m[%c],", defs->gene_ids[i]);
		for ( i_files_in_array = 0; i_files_in_array < n_files_in_array; i_files_in_array++ ) {
			fprintf(outptr,"%.*f,", ndigits, staty[i_files_in_array].parms->m[i]);
		}
		fprintf(outptr,"%.*f,%.*f,%d,%d,%d\n", ndigits, avg_parms->m[i], ndigits, stdev_parms->m[i], topology_m->mtweak[i],topology_0->mtweak[i],topology_p->mtweak[i]);
		fprintf(outptr,"mm[%c],", defs->gene_ids[i]);
		for ( i_files_in_array = 0; i_files_in_array < n_files_in_array; i_files_in_array++ ) {
			fprintf(outptr,"%.*f,", ndigits, staty[i_files_in_array].parms->mm[i]);
		}
		fprintf(outptr,"%.*f,%.*f,%d,%d,%d\n", ndigits, avg_parms->mm[i], ndigits, stdev_parms->mm[i], topology_m->mmtweak[i],topology_0->mmtweak[i],topology_p->mmtweak[i]);
		fprintf(outptr,"tau[%c],", defs->gene_ids[i]);
		for ( i_files_in_array = 0; i_files_in_array < n_files_in_array; i_files_in_array++ ) {
			fprintf(outptr,"%.*f,", ndigits, staty[i_files_in_array].parms->tau[i]);
		}
		fprintf(outptr,"%.*f,%.*f,%d,%d,%d\n", ndigits, avg_parms->tau[i], ndigits, stdev_parms->tau[i], topology_m->tautweak[i],topology_0->tautweak[i],topology_p->tautweak[i]);
		fprintf(outptr,"lambda[%c],", defs->gene_ids[i]);
		for ( i_files_in_array = 0; i_files_in_array < n_files_in_array; i_files_in_array++ ) {
			fprintf(outptr,"%.*f,", ndigits, staty[i_files_in_array].parms->lambda[i]);
		}
		fprintf(outptr,"%.*f,%.*f,%d,%d,%d\n", ndigits, avg_parms->lambda[i], ndigits, stdev_parms->lambda[i], topology_m->lambdatweak[i],topology_0->lambdatweak[i],topology_p->lambdatweak[i]);
		for ( j = 0; j < defs->ngenes; j++ ) {
			fprintf(outptr,"T[%c<-%c],", defs->gene_ids[i], defs->gene_ids[j]);
			for ( i_files_in_array = 0; i_files_in_array < n_files_in_array; i_files_in_array++ ) {
				fprintf(outptr,"%.*f,", ndigits, staty[i_files_in_array].parms->T[i * defs->ngenes + j]);
			}
			fprintf(outptr,"%.*f,%.*f,%d,%d,%d\n", ndigits, avg_parms->T[i * defs->ngenes + j], ndigits, stdev_parms->T[i * defs->ngenes + j], topology_m->Ttweak[i * defs->ngenes + j],topology_0->Ttweak[i * defs->ngenes + j],topology_p->Ttweak[i * defs->ngenes + j]);
		}
		for ( j = 0; j < defs->egenes; j++ ) {
			fprintf(outptr,"E[%c<-%c],", defs->gene_ids[i], defs->egene_ids[j]);
			for ( i_files_in_array = 0; i_files_in_array < n_files_in_array; i_files_in_array++ ) {
				fprintf(outptr,"%.*f,", ndigits, staty[i_files_in_array].parms->E[i * defs->egenes + j]);
			}
			fprintf(outptr,"%.*f,%.*f,%d,%d,%d\n", ndigits, avg_parms->E[i * defs->egenes + j], ndigits, stdev_parms->E[i * defs->egenes + j], topology_m->Etweak[i * defs->egenes + j],topology_0->Etweak[i * defs->egenes + j],topology_p->Etweak[i * defs->egenes + j]);
		}
		for ( j = 0; j < defs->mgenes; j++ ) {
			fprintf(outptr,"M[%c<-%c],", defs->gene_ids[i], defs->mgene_ids[j]);
			for ( i_files_in_array = 0; i_files_in_array < n_files_in_array; i_files_in_array++ ) {
				fprintf(outptr,"%.*f,", ndigits, staty[i_files_in_array].parms->M[i * defs->mgenes + j]);
			}
			fprintf(outptr,"%.*f,%.*f,%d,%d,%d\n", ndigits, avg_parms->M[i * defs->mgenes + j], ndigits, stdev_parms->M[i * defs->mgenes + j], topology_m->Mtweak[i * defs->mgenes + j],topology_0->Mtweak[i * defs->mgenes + j],topology_p->Mtweak[i * defs->mgenes + j]);
		}
		for ( j = 0; j < defs->ncouples; j++ ) {
			fprintf(outptr,"P[%c<-%c%c],", defs->gene_ids[i], defs->couples[j].g1, defs->couples[j].g2);
			for ( i_files_in_array = 0; i_files_in_array < n_files_in_array; i_files_in_array++ ) {
				fprintf(outptr,"%.*f,", ndigits, staty[i_files_in_array].parms->P[i * defs->ncouples + j]);
			}
			fprintf(outptr,"%.*f,%.*f,%d,%d,%d\n", ndigits, avg_parms->P[i * defs->ncouples + j], ndigits, stdev_parms->P[i * defs->ncouples + j], topology_m->Ptweak[i * defs->ncouples + j],topology_0->Ptweak[i * defs->ncouples + j],topology_p->Ptweak[i * defs->ncouples + j]);
		}
	}
	dot = 0;
	adot = 0;
	fprintf(outptr,"Score,");
	for ( i_files_in_array = 0; i_files_in_array < n_files_in_array; i_files_in_array++ ) {
		dot += staty[i_files_in_array].score;
		adot += staty[i_files_in_array].score * staty[i_files_in_array].score;
		fprintf(outptr,"%.*f,", ndigits, staty[i_files_in_array].score);
	}
	dot /= n_files_in_array;
	fprintf(outptr,"%.*f,%.*f\n", ndigits, dot, ndigits, sqrt(adot / ( n_files_in_array - 1 ) - n_files_in_array * dot * dot / ( n_files_in_array - 1 )));
	dot = 0;
	adot = 0;
	fprintf(outptr,"rms,");
	for ( i_files_in_array = 0; i_files_in_array < n_files_in_array; i_files_in_array++ ) {
		dot += staty[i_files_in_array].rms;
		adot += staty[i_files_in_array].rms * staty[i_files_in_array].rms;
		fprintf(outptr,"%.*f,", ndigits, staty[i_files_in_array].rms);
	}
	dot /= n_files_in_array;
	fprintf(outptr,"%.*f,%.*f\n", ndigits, dot, ndigits, sqrt(adot / ( n_files_in_array - 1 ) - n_files_in_array * dot * dot / ( n_files_in_array - 1 )));
	dot = 0;
	adot = 0;
	fprintf(outptr,"penalty,");
	for ( i_files_in_array = 0; i_files_in_array < n_files_in_array; i_files_in_array++ ) {
		dot += staty[i_files_in_array].penalty;
		adot += staty[i_files_in_array].penalty * staty[i_files_in_array].penalty;
		fprintf(outptr,"%.*f,", ndigits, staty[i_files_in_array].penalty);
	}
	dot /= n_files_in_array;
	fprintf(outptr,"%.*f,%.*f\n", ndigits, dot, ndigits, sqrt(adot / ( n_files_in_array - 1 ) - n_files_in_array * dot * dot / ( n_files_in_array - 1 )));
	dot = 0;
	adot = 0;
	fprintf(outptr,"aic,");
	for ( i_files_in_array = 0; i_files_in_array < n_files_in_array; i_files_in_array++ ) {
		dot += staty[i_files_in_array].aic;
		adot += staty[i_files_in_array].aic * staty[i_files_in_array].aic;
		fprintf(outptr,"%.*f,", ndigits, staty[i_files_in_array].aic);
	}
	dot /= n_files_in_array;
	fprintf(outptr,"%.*f,%.*f\n", ndigits, dot, ndigits, sqrt(adot / ( n_files_in_array - 1 ) - n_files_in_array * dot * dot / ( n_files_in_array - 1 )));
	dot = 0;
	adot = 0;
	fprintf(outptr,"bic,");
	for ( i_files_in_array = 0; i_files_in_array < n_files_in_array; i_files_in_array++ ) {
		dot += staty[i_files_in_array].bic;
		adot += staty[i_files_in_array].bic * staty[i_files_in_array].bic;
		fprintf(outptr,"%.*f,", ndigits, staty[i_files_in_array].bic);
	}
	dot /= n_files_in_array;
	fprintf(outptr,"%.*f,%.*f\n", ndigits, dot, ndigits, sqrt(adot / ( n_files_in_array - 1 ) - n_files_in_array * dot * dot / ( n_files_in_array - 1 )));
	for ( k = 0; k < nalleles; k++ ) {
		dot = 0;
		adot = 0;
		fprintf(outptr,"score[%d],", k);
		for ( i_files_in_array = 0; i_files_in_array < n_files_in_array; i_files_in_array++ ) {
			dot += staty[i_files_in_array].scores[k];
			adot += staty[i_files_in_array].scores[k] * staty[i_files_in_array].scores[k];
			fprintf(outptr,"%.*f,", ndigits, staty[i_files_in_array].scores[k]);
		}
		dot /= n_files_in_array;
		fprintf(outptr,"%.*f,%.*f\n", ndigits, dot, ndigits, sqrt(adot / ( n_files_in_array - 1 ) - n_files_in_array * dot * dot / ( n_files_in_array - 1 )));
		dot = 0;
		adot = 0;
		fprintf(outptr,"rms[%d],", k);
		for ( i_files_in_array = 0; i_files_in_array < n_files_in_array; i_files_in_array++ ) {
			dot += staty[i_files_in_array].rmss[k];
			adot += staty[i_files_in_array].rmss[k] * staty[i_files_in_array].rmss[k];
			fprintf(outptr,"%.*f,", ndigits, staty[i_files_in_array].rmss[k]);
		}
		dot /= n_files_in_array;
		fprintf(outptr,"%.*f,%.*f\n", ndigits, dot, ndigits, sqrt(adot / ( n_files_in_array - 1 ) - n_files_in_array * dot * dot / ( n_files_in_array - 1 )));
		dot = 0;
		adot = 0;
		fprintf(outptr,"aic[%d],", k);
		for ( i_files_in_array = 0; i_files_in_array < n_files_in_array; i_files_in_array++ ) {
			dot += staty[i_files_in_array].aics[k];
			adot += staty[i_files_in_array].aics[k] * staty[i_files_in_array].aics[k];
			fprintf(outptr,"%.*f,", ndigits, staty[i_files_in_array].aics[k]);
		}
		dot /= n_files_in_array;
		fprintf(outptr,"%.*f,%.*f\n", ndigits, dot, ndigits, sqrt(adot / ( n_files_in_array - 1 ) - n_files_in_array * dot * dot / ( n_files_in_array - 1 )));
		dot = 0;
		adot = 0;
		fprintf(outptr,"bic[%d],", k);
		for ( i_files_in_array = 0; i_files_in_array < n_files_in_array; i_files_in_array++ ) {
			dot += staty[i_files_in_array].bics[k];
			adot += staty[i_files_in_array].bics[k] * staty[i_files_in_array].bics[k];
			fprintf(outptr,"%.*f,", ndigits, staty[i_files_in_array].bics[k]);
		}
		dot /= n_files_in_array;
		fprintf(outptr,"%.*f,%.*f\n", ndigits, dot, ndigits, sqrt(adot / ( n_files_in_array - 1 ) - n_files_in_array * dot * dot / ( n_files_in_array - 1 )));
	}
	if ( dist_flag == 1 ) {
		dot = 0;
		fprintf(outptr,"kappa,");
		for ( i_files_in_array = 0; i_files_in_array < n_files_in_array; i_files_in_array++ ) {
			dot += staty[i_files_in_array].kappa;
			fprintf(outptr,"%.*f,", ndigits, staty[i_files_in_array].kappa);
		}
		fprintf(outptr,"%.*f\n", ndigits, dot / n_files_in_array);
		fprintf(outptr,"kappa_i_index,");
		for ( i_files_in_array = 0; i_files_in_array < n_files_in_array; i_files_in_array++ ) {
			fprintf(outptr,"%d,", staty[i_files_in_array].i_index);
		}
		fprintf(outptr,"\n");
		fprintf(outptr,"kappa_j_index,");
		for ( i_files_in_array = 0; i_files_in_array < n_files_in_array; i_files_in_array++ ) {
			fprintf(outptr,"%d,", staty[i_files_in_array].j_index);
		}
		fprintf(outptr,"\n");
	}
	fprintf(outptr,"\n\n");
	fprintf(outptr,"Topology threshold,%.*f", ndigits, topology_threshold);
	fprintf(outptr,"\nTopology\n");
	fprintf(outptr,"Target");
	for ( j = 0; j < defs->ngenes; j++ ) {
		fprintf(outptr,",%c", defs->gene_ids[j]);
	}
	for ( j = 0; j < defs->egenes; j++ ) {
		fprintf(outptr,",%c", defs->egene_ids[j]);
	}
	for ( j = 0; j < defs->mgenes; j++ ) {
		fprintf(outptr,",%c", defs->mgene_ids[j]);
	}
	for ( j = 0; j < defs->ncouples; j++ ) {
		fprintf(outptr,",%c%c", defs->couples[j].g1, defs->couples[j].g2);
	}
	fprintf(outptr,"\n");
	for ( i = 0; i < defs->ngenes; i++ ) {
		fprintf(outptr,"%c", defs->gene_ids[i]);
		for ( j = 0; j < defs->ngenes; j++ ) {
			fprintf(outptr,",(%d/%d/%d)", topology_p->Ttweak[i * defs->ngenes + j], topology_0->Ttweak[i * defs->ngenes + j], topology_m->Ttweak[i * defs->ngenes + j]);
		}
		for ( j = 0; j < defs->egenes; j++ ) {
			fprintf(outptr,",(%d/%d/%d)", topology_p->Etweak[i * defs->egenes + j], topology_0->Etweak[i * defs->egenes + j], topology_m->Etweak[i * defs->egenes + j]);
		}
		for ( j = 0; j < defs->mgenes; j++ ) {
			fprintf(outptr,",(%d/%d/%d)", topology_p->Mtweak[i * defs->mgenes + j], topology_0->Mtweak[i * defs->mgenes + j], topology_m->Mtweak[i * defs->mgenes + j]);
		}
		for ( j = 0; j < defs->ncouples; j++ ) {
			fprintf(outptr,",(%d/%d/%d)", topology_p->Ptweak[i * defs->ncouples + j], topology_0->Ptweak[i * defs->ncouples + j], topology_m->Ptweak[i * defs->ncouples + j]);
		}
		fprintf(outptr,"\n");
	}
	fprintf(outptr,"\nAvg\n");
	fprintf(outptr,"Target");
	for ( j = 0; j < defs->ngenes; j++ ) {
		fprintf(outptr,",%c", defs->gene_ids[j]);
	}
	for ( j = 0; j < defs->egenes; j++ ) {
		fprintf(outptr,",%c", defs->egene_ids[j]);
	}
	for ( j = 0; j < defs->mgenes; j++ ) {
		fprintf(outptr,",%c", defs->mgene_ids[j]);
	}
	for ( j = 0; j < defs->ncouples; j++ ) {
		fprintf(outptr,",%c%c", defs->couples[j].g1, defs->couples[j].g2);
	}
	fprintf(outptr,"\n");
	for ( i = 0; i < defs->ngenes; i++ ) {
		fprintf(outptr,"%c", defs->gene_ids[i]);
		for ( j = 0; j < defs->ngenes; j++ ) {
			fprintf(outptr,",%.*f", ndigits, avg_parms->T[i * defs->ngenes + j]);
		}
		for ( j = 0; j < defs->egenes; j++ ) {
			fprintf(outptr,",%.*f", ndigits, avg_parms->E[i * defs->egenes + j]);
		}
		for ( j = 0; j < defs->mgenes; j++ ) {
			fprintf(outptr,",%.*f", ndigits, avg_parms->M[i * defs->mgenes + j]);
		}
		for ( j = 0; j < defs->ncouples; j++ ) {
			fprintf(outptr,",%.*f", ndigits, avg_parms->P[i * defs->ncouples + j]);
		}
		fprintf(outptr,"\n");
	}
	fprintf(outptr,"\nCons\n");
	for ( i_files_in_array = 0; i_files_in_array < n_files_in_array; i_files_in_array++ ) {
		if ( tweak_equal(topology, staty[i_files_in_array].topology) ) {
			fprintf(outptr,"%s\n", file_array[i_files_in_array]);
		}
	}
	fprintf(outptr,"\nBy topology:\n");
	for ( i_files_in_array = 0; i_files_in_array < n_files_in_array - 1; i_files_in_array++ ) {
		fprintf(outptr,"%s,", file_array[i_files_in_array]);
	}
	fprintf(outptr,"%s", file_array[n_files_in_array - 1]);
	fprintf(outptr,"\n");
	for ( i = 0; i < defs->ngenes; i++ ) {
		for ( j = 0; j < defs->ngenes; j++ ) {
			fprintf(outptr,"T[%c<-%c],", defs->gene_ids[i], defs->gene_ids[j]);
			for ( i_files_in_array = 0; i_files_in_array < n_files_in_array - 1; i_files_in_array++ ) {
				fprintf(outptr,"%d,", staty[i_files_in_array].topology->Ttweak[i * defs->ngenes + j]);
			}
			fprintf(outptr,"%d", staty[n_files_in_array - 1].topology->Ttweak[i * defs->ngenes + j]);
			fprintf(outptr,"\n");
		}
		for ( j = 0; j < defs->egenes; j++ ) {
			fprintf(outptr,"E[%c<-%c],", defs->gene_ids[i], defs->egene_ids[j]);
			for ( i_files_in_array = 0; i_files_in_array < n_files_in_array - 1; i_files_in_array++ ) {
				fprintf(outptr,"%d,", staty[i_files_in_array].topology->Etweak[i * defs->egenes + j]);
			}
			fprintf(outptr,"%d", staty[n_files_in_array - 1].topology->Etweak[i * defs->egenes + j]);
			fprintf(outptr,"\n");
		}
		for ( j = 0; j < defs->mgenes; j++ ) {
			fprintf(outptr,"M[%c<-%c],", defs->gene_ids[i], defs->mgene_ids[j]);
			for ( i_files_in_array = 0; i_files_in_array < n_files_in_array - 1; i_files_in_array++ ) {
				fprintf(outptr,"%d,", staty[i_files_in_array].topology->Mtweak[i * defs->mgenes + j]);
			}
			fprintf(outptr,"%d", staty[n_files_in_array - 1].topology->Mtweak[i * defs->mgenes + j]);
			fprintf(outptr,"\n");
		}
		for ( j = 0; j < defs->ncouples; j++ ) {
			fprintf(outptr,"P[%c<-%c%c],", defs->gene_ids[i], defs->couples[j].g1, defs->couples[j].g2);
			for ( i_files_in_array = 0; i_files_in_array < n_files_in_array - 1; i_files_in_array++ ) {
				fprintf(outptr,"%d,", staty[i_files_in_array].topology->Ptweak[i * defs->ncouples + j]);
			}
			fprintf(outptr,"%d", staty[n_files_in_array - 1].topology->Ptweak[i * defs->ncouples + j]);
			fprintf(outptr,"\n");
		}
	}

	
	fclose(outptr);
	
	FreeZygote();
	free(precision);
	free(format);
	free(section_title);
	return 0;
}

int tweak_equal(Tweak*y1, Tweak*y2)
{
	int i, j, res;
	res = 1;
	for ( i = 0; i < defs->ngenes; i++ ) {
		for ( j = 0; j < defs->ngenes; j++ ) {
			if ( y1->Ttweak[i * defs->ngenes + j] != y2->Ttweak[i * defs->ngenes + j] ) {
				res = 0;
			}
		}
		for ( j = 0; j < defs->egenes; j++ ) {
			if ( y1->Etweak[i * defs->egenes + j] != y2->Etweak[i * defs->egenes + j]) {
				res = 0;
			}
		}
		for ( j = 0; j < defs->mgenes; j++ ) {
			if ( y1->Mtweak[i * defs->mgenes + j] != y2->Mtweak[i * defs->mgenes + j]) {
				res = 0;
			}
		}
		for ( j = 0; j < defs->ncouples; j++ ) {
			if ( y1->Ptweak[i * defs->ncouples + j] != y2->Ptweak[i * defs->ncouples + j]) {
				res = 0;
			}
		}
	}
	return res;
}

double eqparms_distance(EqParms*Pout, EqParms*Pin)
{
	double distance, dot;
	int i,j;
	distance = 0;
	for ( i = 0; i < defs->ngenes; i++ ) {
		dot = fabs( ( Pout->R[i] - Pin->R[i]) / Pin->R[i] );
		if ( distance < dot ) 
			distance = dot;
		for (j=0; j<defs->ngenes; j++ ) {
			dot = fabs( ( Pout->T[(i*defs->ngenes)+j] - Pin->T[(i*defs->ngenes)+j] ) / Pin->T[(i*defs->ngenes)+j] );
			if ( distance < dot ) 
				distance = dot;
		}
		for ( j = 0; j < defs->egenes; j++ ) {
			dot = fabs( ( Pout->E[(i*defs->egenes)+j] - Pin->E[(i*defs->egenes)+j] ) / Pin->E[(i*defs->egenes)+j] );
			if ( distance < dot ) 
				distance = dot;
		}
		for ( j = 0; j < defs->mgenes; j++ ) {
			dot = fabs( ( Pout->M[(i*defs->mgenes)+j] - Pin->M[(i*defs->mgenes)+j] ) / Pin->M[(i*defs->mgenes)+j] );
			if ( distance < dot ) 
				distance = dot;
		}
		for (j=0; j<defs->ncouples; j++ ) {
			dot = fabs( ( Pout->P[(i*defs->ncouples)+j] - Pin->P[(i*defs->ncouples)+j] ) / Pin->P[(i*defs->ncouples)+j] );
			if ( distance < dot ) 
				distance = dot;
		}
		dot = fabs( ( Pout->h[i] - Pin->h[i] ) / Pin->h[i] );
		if ( distance < dot ) 
			distance = dot;
		dot = fabs( ( Pout->tau[i] - Pin->tau[i] ) / Pin->tau[i] );
		if ( distance < dot ) 
			distance = dot;
		dot = fabs( ( Pout->lambda[i] - Pin->lambda[i] ) / Pin->lambda[i] );
		if ( distance < dot ) 
			distance = dot;
	}
	if ( (defs->diff_schedule == 'A') || (defs->diff_schedule == 'C') ) {
		dot = fabs( ( Pout->d[0] - Pin->d[0] ) / Pin->d[0] );
		if ( distance < dot ) 
			distance = dot;
	} else {
		for (i=0; i<defs->ngenes; i++) {
			dot = fabs( ( Pout->d[i] - Pin->d[i] ) / Pin->d[i] );
			if ( distance < dot ) 
				distance = dot;
		}
	}
	if ( (defs->mob_schedule == 'A') || (defs->mob_schedule == 'C') ) {
		dot = fabs( ( Pout->m[0] - Pin->m[0] ) / Pin->m[0] );
		if ( distance < dot ) 
			distance = dot;
	} else {
		for (i=0; i<defs->ngenes; i++) {
			dot = fabs( ( Pout->m[i] - Pin->m[i] ) / Pin->m[i] );
			if ( distance < dot ) 
				distance = dot;
		}
	}
	if ( (defs->mob_schedule == 'A') || (defs->mob_schedule == 'C') ) {
		dot = fabs( ( Pout->mm[0] - Pin->mm[0] ) / Pin->mm[0] );
		if ( distance < dot ) 
			distance = dot;
	} else {
		for (i=0; i<defs->ngenes; i++) {
			dot = fabs( ( Pout->mm[i] - Pin->mm[i] ) / Pin->mm[i] );
			if ( distance < dot ) 
				distance = dot;
		}
	}
	return distance;
}

double eqparms_distance_dist(EqParms*Pout, EqParms*Pin, EqParms*dist, int*i_index, int*j_index)
{
	double distance, dot;
	int i,j;
	distance = 0;
	for ( i = 0; i < defs->ngenes; i++ ) {
		dot = fabs( ( Pout->R[i] - Pin->R[i]) / Pin->R[i] );
		dist->R[i] = dot;
		if ( distance < dot ) {
			*j_index = 0;
			*i_index = i;
			distance = dot;
		}
		for (j=0; j<defs->ngenes; j++ ) {
			dot = fabs( ( Pout->T[(i*defs->ngenes)+j] - Pin->T[(i*defs->ngenes)+j] ) / Pin->T[(i*defs->ngenes)+j] );
			dist->T[(i*defs->ngenes)+j] = dot;
			if ( distance < dot ) {
				*j_index = 1;
				*i_index = i*defs->ngenes + j;
				distance = dot;
			}
		}
		for ( j = 0; j < defs->egenes; j++ ) {
			dot = fabs( ( Pout->E[(i*defs->egenes)+j] - Pin->E[(i*defs->egenes)+j] ) / Pin->E[(i*defs->egenes)+j] );
			dist->E[(i*defs->egenes)+j] = dot;
			if ( distance < dot ) {
				*j_index = 2;
				*i_index = i*defs->egenes+j;
				distance = dot;
			}
		}
		for ( j = 0; j < defs->mgenes; j++ ) {
			dot = fabs( ( Pout->M[(i*defs->mgenes)+j] - Pin->M[(i*defs->mgenes)+j] ) / Pin->M[(i*defs->mgenes)+j] );
			dist->M[(i*defs->mgenes)+j] = dot;
			if ( distance < dot ) {
				*j_index = 3;
				*i_index = i*defs->mgenes+j;
				distance = dot;
			}
		}
		for (j=0; j<defs->ncouples; j++ ) {
			dot = fabs( ( Pout->P[(i*defs->ncouples)+j] - Pin->P[(i*defs->ncouples)+j] ) / Pin->P[(i*defs->ncouples)+j] );
			dist->P[(i*defs->ncouples)+j] = dot;
			if ( distance < dot ) {
				*j_index = 4;
				*i_index = i*defs->ncouples+j;
				distance = dot;
			}
		}
		dot = fabs( ( Pout->h[i] - Pin->h[i] ) / Pin->h[i] );
		dist->h[i] = dot;
		if ( distance < dot ) {
			*j_index = 5;
			*i_index = i;
			distance = dot;
		}
		dot = fabs( ( Pout->tau[i] - Pin->tau[i] ) / Pin->tau[i] );
		dist->tau[i] = dot;
		if ( distance < dot ) {
			*j_index = 6;
			*i_index = i;
			distance = dot;
		}
		dot = fabs( ( Pout->lambda[i] - Pin->lambda[i] ) / Pin->lambda[i] );
		dist->lambda[i] = dot;
		if ( distance < dot ) {
			*j_index = 7;
			*i_index = i;
			distance = dot;
		}
	}
	if ( (defs->diff_schedule == 'A') || (defs->diff_schedule == 'C') ) {
		dot = fabs( ( Pout->d[0] - Pin->d[0] ) / Pin->d[0] );
		dist->d[0] = dot;
		if ( distance < dot ) {
			*j_index = 8;
			*i_index = 0;
			distance = dot;
		}
	} else {
		for (i=0; i<defs->ngenes; i++) {
			dot = fabs( ( Pout->d[i] - Pin->d[i] ) / Pin->d[i] );
			dist->d[i] = dot;
			if ( distance < dot ) {
				*j_index = 9;
				*i_index = i;
				distance = dot;
			}
		}
	}
	if ( (defs->mob_schedule == 'A') || (defs->mob_schedule == 'C') ) {
		dot = fabs( ( Pout->m[0] - Pin->m[0] ) / Pin->m[0] );
		dist->m[0] = dot;
		if ( distance < dot ) {
			*j_index = 10;
			*i_index = 0;
			distance = dot;
		}
	} else {
		for (i=0; i<defs->ngenes; i++) {
			dot = fabs( ( Pout->m[i] - Pin->m[i] ) / Pin->m[i] );
			dist->m[i] = dot;
			if ( distance < dot ) {
				*j_index = 10;
				*i_index = i;
				distance = dot;
			}
		}
	}
	if ( (defs->mob_schedule == 'A') || (defs->mob_schedule == 'C') ) {
		dot = fabs( ( Pout->mm[0] - Pin->mm[0] ) / Pin->mm[0] );
		dist->m[0] = dot;
		if ( distance < dot ) {
			*j_index = 11;
			*i_index = 0;
			distance = dot;
		}
	} else {
		for (i=0; i<defs->ngenes; i++) {
			dot = fabs( ( Pout->mm[i] - Pin->mm[i] ) / Pin->mm[i] );
			dist->mm[i] = dot;
			if ( distance < dot ) {
				*j_index = 11;
				*i_index = i;
				distance = dot;
			}
		}
	}
	return distance;
}

