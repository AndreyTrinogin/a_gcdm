/***************************************************************************
 *            tweak.c
 *
 *  Tue Oct 18 14:00:37 2005
 *  Copyright  2005  $USER
 *  kozlov@spbcas.ru
 ****************************************************************************/

/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */
 

#include <ctype.h>
#include <stdlib.h>
#include <string.h>

#include <zygotic.h>
#include <maternal.h>
#include <tweak.h>
#include <ffio.h>
#include <error.h>

/* Reading: following reads tweak struct from file ($tweak section) */

/*** ReadTweak: reads the tweak array from the $tweak section in the data **
 *              file; this array has a value of 1 or 0 for each parameter  *
 *              in the model and is used by Translate to create the array  *
 *              of pointers to the parameters-to-be-tweaked                *
 ***************************************************************************/

Tweak*ReadTweak(FILE *fp)
{
	Tweak*             l_tweak;                         /* local Tweak struct */
	int *temptweak, *temptweak1, *temptweak2, *temptweak3;          /* temporary array to read tweaks */
	int               i;                               /* local loop counter */
	int               c;                         /* used to parse text lines */
	int               linecount = 0;        /* keep track of # of lines read */
	int               Tcount    = 0;           /* keep track of T lines read */
	int               Ecount    = 0;           /* keep track of T lines read */
	int               Mcount    = 0;           /* keep track of T lines read */
	int               Pcount    = 0;           /* keep track of T lines read */
	char              *base;          /* pointer to beginning of line string */
	char              *record;    /* string for reading whole line of params */
	char              **fmt, **fmt1, **fmt2, **fmt3;   /* array of format strings for reading params */
	char              *skip, *skip1, *skip2, *skip3;               /* string of values to be skipped */
	const char        read_fmt[] = "%d";                      /* read an int */
	const char        skip_fmt[] = "%*d ";                  /* ignore an int */
	l_tweak = (Tweak*)malloc(sizeof(Tweak));
	base = (char *)calloc(MAX_RECORD, sizeof(char *));
	skip = (char *)calloc(MAX_RECORD, sizeof(char *));
	skip1 = (char *)calloc(MAX_RECORD, sizeof(char *));
	skip2 = (char *)calloc(MAX_RECORD, sizeof(char *));
	skip3 = (char *)calloc(MAX_RECORD, sizeof(char *));
	fmt  = (char **)calloc(defs->ngenes, sizeof(char *));
	fmt1  = (char **)calloc(defs->egenes, sizeof(char *));
	fmt2  = (char **)calloc(defs->mgenes, sizeof(char *));
	fmt3  = (char **)calloc(defs->ncouples, sizeof(char *));
	temptweak = (int *)calloc(defs->ngenes, sizeof(int *));
	temptweak1 = (int *)calloc(defs->egenes, sizeof(int *));
	temptweak2 = (int *)calloc(defs->mgenes, sizeof(int *));
	temptweak3 = (int *)calloc(defs->ncouples, sizeof(int *));
/* create format strings according to the number of genes */
	for ( i = 0; i < defs->ngenes; i++ ) {  
		fmt[i] = (char *)calloc(MAX_RECORD, sizeof(char));   
		fmt[i] = strcpy(fmt[i], skip);
		fmt[i] = strcat(fmt[i], read_fmt);
		skip   = strcat(skip, skip_fmt);
	}
	for ( i = 0; i < defs->egenes; i++ ) {  
		fmt1[i] = (char *)calloc(MAX_RECORD, sizeof(char));   
		fmt1[i] = strcpy(fmt1[i], skip1);
		fmt1[i] = strcat(fmt1[i], read_fmt);
		skip1   = strcat(skip1, skip_fmt);
	}
	for ( i = 0; i < defs->mgenes; i++ ) {  
		fmt2[i] = (char *)calloc(MAX_RECORD, sizeof(char));   
		fmt2[i] = strcpy(fmt2[i], skip2);
		fmt2[i] = strcat(fmt2[i], read_fmt);
		skip2   = strcat(skip2, skip_fmt);
	}
	for ( i = 0; i < defs->ncouples; i++ ) {  
		fmt3[i] = (char *)calloc(MAX_RECORD, sizeof(char));   
		fmt3[i] = strcpy(fmt3[i], skip3);
		fmt3[i] = strcat(fmt3[i], read_fmt);
		skip3   = strcat(skip3, skip_fmt);
	}
/* initialize the Tweak struct */
	l_tweak->Rtweak = (int *)calloc(defs->ngenes, sizeof(int));
	l_tweak->Ttweak = (int *)calloc(defs->ngenes * defs->ngenes, sizeof(int));
	l_tweak->Mtweak = (int *)calloc(defs->ngenes * defs->mgenes, sizeof(int));
	l_tweak->Etweak = (int *)calloc(defs->ngenes * defs->egenes, sizeof(int));
	l_tweak->Ptweak = (int *)calloc(defs->ngenes * defs->ncouples, sizeof(int));
	l_tweak->htweak = (int *)calloc(defs->ngenes, sizeof(int));
	if ( (defs->diff_schedule == 'A') || (defs->diff_schedule == 'C') ) {
		l_tweak->dtweak = (int *)malloc(sizeof(int));
	} else {
		l_tweak->dtweak = (int *)calloc(defs->ngenes, sizeof(int));
	}
	if ( (defs->mob_schedule == 'A') || (defs->mob_schedule == 'C') ) {
		l_tweak->mtweak = (int *)malloc(sizeof(int));
	} else {
		l_tweak->mtweak = (int *)calloc(defs->ngenes, sizeof(int));
	}
	if ( (defs->mob_schedule == 'A') || (defs->mob_schedule == 'C') ) {
		l_tweak->mmtweak = (int *)malloc(sizeof(int));
	} else {
		l_tweak->mmtweak = (int *)calloc(defs->ngenes, sizeof(int));
	}
	l_tweak->lambdatweak = (int *)calloc(defs->ngenes, sizeof(int));
	l_tweak->tautweak = (int *)calloc(defs->ngenes, sizeof(int));
	fp = FindSection(fp, "tweak");                     /* find tweak section */
	if( !fp )
		error("ReadTweak: could not locate tweak\n");
	while ( strncmp(( base=fgets(base, MAX_RECORD, fp)), "$$", 2)) {
		record = base;
		c = (int)*record;
		while ( c != '\0' ) {
			if ( isdigit(c) ) {                            /* line contains data */
				record = base;
/* usually read ngenes parameters, but for diff. schedule A or C only read *
 * one d parameter                                                         */
				if ((linecount == 6) && ((defs->diff_schedule=='A') || (defs->diff_schedule=='C'))) {
					if ( 1 != sscanf(record, fmt[0], &temptweak[0]) )
						error("ReadTweak: error reading tweaks");
				}
				if ((linecount == 9) && ((defs->mob_schedule=='A') || (defs->mob_schedule=='C'))) {
					if ( 1 != sscanf(record, fmt[0], &temptweak[0]) )
						error("ReadTweak: error reading mtweaks");
				}
				if ((linecount == 10) && ((defs->mob_schedule=='A') || (defs->mob_schedule=='C'))) {
					if ( 1 != sscanf(record, fmt[0], &temptweak[0]) )
						error("ReadTweak: error reading mtweaks");
				} else if (linecount == 2) {
					for ( i=0; i < defs->egenes; i++ ) {
						if ( 1 != sscanf(record, fmt1[i], &temptweak1[i]) )
							error("ReadTweak: error reading e parms");
					}
				} else if (linecount == 3) {
					for ( i=0; i < defs->mgenes; i++ ) {
						if ( 1 != sscanf(record, fmt2[i], &temptweak2[i]) )
							error("ReadParameters: error reading m parms");
					}
				} else if (linecount == 4) {
					for ( i=0; i < defs->ncouples; i++ ) {
						if ( 1 != sscanf(record, fmt3[i], &temptweak3[i]) )
							error("ReadParameters: error reading couples parms");
					}
				} else {
					for ( i=0; i < defs->ngenes; i++ ) {
						if ( 1 != sscanf(record, fmt[i], &temptweak[i]) )
							error("ReadTweak: error reading tweak variables");
					}
				}
				switch (linecount) {  /* copy read parameters into the right array */
					case 0:
						for ( i=0; i < defs->ngenes; i++ )                    /* R tweaks */
							l_tweak->Rtweak[i] = temptweak[i];
						linecount++;
					break;
					case 1:          /* T tweaks: keep track of read lines with Tcount */
						for ( i=0; i < defs->ngenes; i++ )
							l_tweak->Ttweak[i+Tcount*defs->ngenes] = temptweak[i];
						Tcount++;
						if ( Tcount == defs->ngenes ) {
							linecount++;
							if ( defs->egenes == 0 ) {
								linecount++;
								if ( defs->mgenes == 0 ) {
									linecount++;
									if ( defs->ncouples == 0 )
										linecount++;
								}
							}
						}
					break;
					case 2:          /* T tweaks: keep track of read lines with Tcount */
						for ( i=0; i < defs->egenes; i++ )
							l_tweak->Etweak[i+Ecount*defs->egenes] = temptweak1[i];
						Ecount++;
						if ( Ecount == defs->ngenes ) {
							linecount++;
							if ( defs->mgenes == 0 ) {
								linecount++;
								if ( defs->ncouples == 0 )
									linecount++;
							}
						}
					break;
					case 3:          /* T tweaks: keep track of read lines with Tcount */
						for ( i=0; i < defs->mgenes; i++ )
							l_tweak->Mtweak[i+Mcount*defs->mgenes] = temptweak2[i];
						Mcount++;
						if ( Mcount == defs->ngenes ) {
							linecount++;
							if ( defs->ncouples == 0 )
								linecount++;
						}
					break;
					case 4:          /* T tweaks: keep track of read lines with Tcount */
						for ( i=0; i < defs->ncouples; i++ )
							l_tweak->Ptweak[i+Pcount*defs->ncouples] = temptweak3[i];
						Pcount++;
						if ( Pcount == defs->ngenes )
							linecount++;
					break;
					case 5:
						for ( i=0; i < defs->ngenes; i++ )                    /* h tweaks */
							l_tweak->htweak[i] = temptweak[i];
						linecount++;
					break;
					case 6:                       /* d tweaks: consider diff. schedule */
						if ((defs->diff_schedule == 'A') || (defs->diff_schedule == 'C' )) {
							l_tweak->dtweak[0] = temptweak[0];
						} else {
							for ( i=0; i < defs->ngenes; i++ )
								l_tweak->dtweak[i] = temptweak[i];
						}
						linecount++;
					break;
					case 7:                                           /* lambda tweaks */
					for ( i=0; i < defs->ngenes; i++ )
						l_tweak->lambdatweak[i] = temptweak[i];
					linecount++;
					break;
					case 8:                                           /* lambda tweaks */
					for ( i=0; i < defs->ngenes; i++ )
						l_tweak->tautweak[i] = temptweak[i];
					linecount++;
					break;
					case 9:                       /* d tweaks: consider diff. schedule */
						if ((defs->mob_schedule == 'A') || (defs->mob_schedule == 'C' )) {
							l_tweak->mtweak[0] = temptweak[0];
						} else {
							for ( i=0; i < defs->ngenes; i++ )
								l_tweak->mtweak[i] = temptweak[i];
						}
						linecount++;
					break;
					case 10:                       /* d tweaks: consider diff. schedule */
						if ((defs->mob_schedule == 'A') || (defs->mob_schedule == 'C' )) {
							l_tweak->mmtweak[0] = temptweak[0];
						} else {
							for ( i=0; i < defs->ngenes; i++ )
								l_tweak->mmtweak[i] = temptweak[i];
						}
						linecount++;
					break;
					default:
						error("ReadTweak: too many data lines in tweak section");
				}
				break;                           /* don't do rest of loop anymore! */
			} else if ( isspace(c) ) {               /* ignore leading white space */
				c = (int)*(++record);
			} else {                  /* anything but space or digit means comment */
				break;
			}
		}
	}
	free(temptweak);
	free(temptweak1);
	free(temptweak2);
	free(temptweak3);
	free(base);
	free(skip);
	free(skip1);
	free(skip2);
	free(skip3);
	for (i=0; i<defs->ngenes; i++)
		free(fmt[i]);
	free(fmt);
	for (i=0; i<defs->egenes; i++)
		free(fmt1[i]);
	free(fmt1);
	for (i=0; i<defs->mgenes; i++)
		free(fmt2[i]);
	free(fmt2);
	for (i=0; i<defs->ncouples; i++)
		free(fmt3[i]);
	free(fmt3);
	return l_tweak;
}

void TweakDelete(Tweak*p)
{
	free(p->Rtweak);
	free(p->Ttweak);
	free(p->Mtweak);
	free(p->Ptweak);
	free(p->Etweak);
	free(p->htweak);
	free(p->dtweak);
	free(p->lambdatweak);
	free(p->tautweak);
	free(p->mtweak);
	free(p->mmtweak);
	free(p);
}

void WriteTweak(char *name, Tweak *p, char *title)
{
	char   *temp;                                     /* temporary file name */
	FILE   *outfile;                                  /* name of output file */
	FILE   *tmpfile;                               /* name of temporary file */
	char*title_of_above;
	title_of_above=(char*)calloc(MAX_RECORD,sizeof(char));
	if ( !strcmp(title, "tweak") ) {
		sprintf(title_of_above,"limits");
	} else if ( ( !strncmp(title, "topology", 8)) ) {
		sprintf(title_of_above,"problem");
	}
	GetSectionPosition(&outfile, &tmpfile, title, title_of_above, name, &temp);
/* now we write the eqparms section into the tmpfile */
	PrintTweak(tmpfile, p, title);
	WriteRest(&outfile,&tmpfile,name,&temp);
	free(title_of_above);
}

void PrintTweak(FILE *fp, Tweak *p, char *title)
{
	int    i, j;                                      /* local loop counters */
	fprintf(fp, "$%s\n", title);
	fprintf(fp, "promoter_strengths:\n");             /* Rs are written here */
	for ( i=0; i<defs->ngenes; i++ )
		fprintf(fp, "%d ", p->Rtweak[i]);
	fprintf(fp, "\n");
	fprintf(fp, "genetic_interconnect_matrix:\n");        /* Ts written here */
	for ( i=0; i<defs->ngenes; i++ ) {
		for ( j=0; j<defs->ngenes; j++ )
			fprintf(fp, "%d ", p->Ttweak[(i*defs->ngenes)+j]);
		fprintf(fp, "\n");
	}
	fprintf(fp, "external_input_strengths:\n");        /* Es written here */
	if ( defs->egenes != 0 )
		for ( i=0; i<defs->ngenes; i++ ) {
			for ( j=0; j<defs->egenes; j++ ) 
				fprintf(fp, "%d ", p->Etweak[(i*defs->egenes)+j]);
			fprintf(fp, "\n");
		}
	fprintf(fp, "maternal_connection_strengths:\n");      /* ms written here */
	if ( defs->mgenes != 0 )
		for ( i=0; i<defs->ngenes; i++ ) {
			for ( j=0; j<defs->mgenes; j++ ) 
				fprintf(fp, "%d ", p->Mtweak[(i*defs->mgenes)+j]);
			fprintf(fp, "\n");
		}
	fprintf(fp, "couples:\n");      /* ms written here */
	if ( defs->ncouples != 0 )
		for ( i=0; i<defs->ngenes; i++ ) {
			for ( j=0; j<defs->ncouples; j++ ) 
				fprintf(fp, "%d ", p->Ptweak[(i*defs->ncouples)+j]);
			fprintf(fp, "\n");
		}
	fprintf(fp, "promoter_thresholds:\n");            /* hs are written here */
	for ( i=0; i<defs->ngenes; i++ )
		fprintf(fp, "%d ", p->htweak[i]);
	fprintf(fp, "\n");
	fprintf(fp, "diffusion_parameter(s):\n");         /* ds are written here */
	if ( (defs->diff_schedule == 'A') || (defs->diff_schedule == 'C') )
		fprintf(fp, "%d ", p->dtweak[0]);
	else
		for ( i=0; i<defs->ngenes; i++ )
			fprintf(fp, "%d ", p->dtweak[i]);
	fprintf(fp, "\n");
	fprintf(fp, "protein_half_lives:\n");        /* lambdas are written here */
	for ( i=0; i<defs->ngenes; i++ ) {
		fprintf(fp, "%d ", p->lambdatweak[i]);
	}
	fprintf(fp, "\n");
	fprintf(fp, "translational_transcriptional_delays:\n");        /* taus are written here */
	for ( i = 0; i < defs->ngenes; i++ ) 
		fprintf(fp, "%d ", p->tautweak[i]);
	fprintf(fp, "\n");
	fprintf(fp, "mobility:\n");         /* ds are written here */
	if ( (defs->mob_schedule == 'A') || (defs->mob_schedule == 'C') )
		fprintf(fp, "%d ", p->mtweak[0]);
	else
		for ( i=0; i<defs->ngenes; i++ )
			fprintf(fp, "%d ", p->mtweak[i]);
	fprintf(fp, "\n");
	fprintf(fp, "mitosis_mobility:\n");         /* ds are written here */
	if ( (defs->mob_schedule == 'A') || (defs->mob_schedule == 'C') )
		fprintf(fp, "%d ", p->mmtweak[0]);
	else
		for ( i=0; i<defs->ngenes; i++ )
			fprintf(fp, "%d ", p->mmtweak[i]);
	fprintf(fp, "\n$$\n");
}

int gcdm_tweak_get_ntweaks(Tweak*p)
{
	int i, j, ntweaks;
	ntweaks = 0;
	for ( i=0; i<defs->ngenes; i++ ) {
		if ( p->Rtweak[i] == 1 )
			ntweaks++;
		if ( p->htweak[i] == 1 )
			ntweaks++;
		if ( p->mtweak[i] == 1 )
			ntweaks++;
		if ( p->mmtweak[i] == 1 )
			ntweaks++;
		if ( p->dtweak[i] == 1 )
			ntweaks++;
		if ( p->lambdatweak[i] == 1 )
			ntweaks++;
		if ( p->tautweak[i] == 1 )
			ntweaks++;
		for ( j=0; j<defs->ngenes; j++ )
			if ( p->Ttweak[(i*defs->ngenes)+j] == 1 )
				ntweaks++;
		for ( j=0; j<defs->egenes; j++ ) 
			if ( p->Etweak[(i*defs->egenes)+j] == 1 )
				ntweaks++;
		for ( j=0; j<defs->mgenes; j++ ) 
			if ( p->Mtweak[(i*defs->mgenes)+j] == 1 )
				ntweaks++;
		for ( j=0; j<defs->ncouples; j++ )
			if ( p->Ptweak[(i*defs->ncouples)+j] == 1 )
				ntweaks++;
	}
	return ntweaks;
}
