/***************************************************************************
                          ffio.c  -  description
                             -------------------
    begin                : ðÔÎ áÐÒ 23 2004
    copyright            : (C) 2004 by Konstantin Kozlov
    email                : kostya@spbcas.ru
 ***************************************************************************/

#include <stdio.h>
#include <stdlib.h>

/* these for umask*/
#include <sys/types.h>
#include <sys/stat.h>
#include <error.h>
#include <ffio.h>

void GetSectionPosition(FILE**outfile,FILE**tmpfile,char*title_of_this,char*title_of_above,char*name,char**temp)
{
	char   *record;                         /* record to be read and written */
	char   *record_ptr;        /* pointer used to remember record for 'free' */
	char *cmp_title_of_above;
	int cmp_length;
	int    filedescriptor;                     /*for correct usage of mkstemp*/
	mode_t mode, old_mode;
/* S_IRUSR|S_IWUSR|S_IRGRP|S_IWGRP|S_IROTH|S_IWOTH|S_IXUSR|S_IXGRP|S_IXOTH*/
	mode = S_IXOTH;
	old_mode = umask(mode);
	if ( title_of_above != NULL ) {
		cmp_title_of_above      = (char *)calloc(FFIO_MAX_RECORD, sizeof(char));
		sprintf(cmp_title_of_above,"$%s",title_of_above);
		cmp_length=strlen(cmp_title_of_above);
	}
	(*temp)      = (char *)calloc(FFIO_MAX_RECORD, sizeof(char));
	record    = (char *)calloc(FFIO_MAX_RECORD, sizeof(char));
	record_ptr = record;            /* this is to remember record for 'free' */
/* open output and temporary file */
	(*outfile) = fopen(name, "r");              /* open outfile for reading */
	if ( !(*outfile) )                              /* sorry for the confusion! */
		error("WriteOu: error opening output file");
	(*temp) = strcpy((*temp),"getspXXXXXX");               /* required by mkstemp() */
	filedescriptor=mkstemp((*temp));
	if ( filedescriptor == -1 )              /* get unique name for temp file */
		error("GetSectionPosition: error creating temporary file");
	(*tmpfile) = fdopen(filedescriptor, "w");               /* ... and open it for writing */
	if ( !(*tmpfile) )
		error("GetSectionPosition: error opening temporary file");
	if ( FindSection((*outfile), title_of_this) ) { /* erase section if already present */
		fclose((*outfile));                       /* this is a little kludgey but */
		KillSection(name, title_of_this);      /* since KillSection needs to be in */
		(*outfile) = fopen(name, "r");           /* total control of the file */
	}
	rewind((*outfile));
/* the follwoing two loops look for the appropriate file position to write */
/* the eqparms section (alternatives are input and eqparms)                */
	if ( title_of_above != NULL) {/*version at the begining*/
		while ( strncmp(record=fgets(record, FFIO_MAX_RECORD, (*outfile)), cmp_title_of_above, cmp_length) )
			fputs(record, (*tmpfile));
		fputs(record, (*tmpfile));
		while ( strncmp(record=fgets(record, FFIO_MAX_RECORD, (*outfile)), "$$", 2) )
			fputs(record, (*tmpfile));
		fputs(record, (*tmpfile));
		do {
			record = fgets(record, FFIO_MAX_RECORD, (*outfile));
			if ( !record ) break;
		} while ( strncmp(record, "$", 1) );
		if ( record )
			fseek((*outfile), -strlen(record)*sizeof(char), SEEK_CUR);
		fputs("\n", (*tmpfile));
		free(cmp_title_of_above);
	}/*version at the begining*/
	free(record_ptr);
}


void WriteRest(FILE**outfile,FILE**tmpfile,char*name,char**temp)
{
	char   *record;                         /* record to be read and written */
	record    = (char *)calloc(FFIO_MAX_RECORD, sizeof(char));
	fputs("\n", (*tmpfile));
	while ( (record=fgets(record, FFIO_MAX_RECORD, (*outfile))) )
		fputs(record, (*tmpfile));
	fclose((*outfile));
	fclose((*tmpfile));
	if ( -1 == rename((*temp), name) ) {
		error("WriteRest: error renaming temp file %s", (*temp));
	}
	free((*temp));
	free(record);
}

FILE *FindOptionTitle(FILE *fp, char *title)
{
  int      c;                      /* input happens character by character */
  int      nsought;                       /* holds length of section title */
  char     *base;                              /* string for section title */
  long     looksite;                            /* file position indicator */

  rewind(fp);                /* start looking at the beginning of the file */

  nsought = strlen(title);
  base = (char *)calloc(FFIO_MAX_RECORD, sizeof(char));

/*** while loop goes through the file character by character... ************/
   while ( (c=getc(fp)) != EOF) {               /* ...until it finds a '#' */

    if ( c == '#') {                  /* found a option title string */
      looksite = ftell(fp);                               /* where are we? */
      base = fgets(base, FFIO_MAX_RECORD, fp);   /* get option title title (without #)*/
      if ( !(strncmp(base, title, nsought)) ) {
        fseek(fp, looksite, 0);        /* found the sought string: reposi- */
        fscanf(fp, "%*s\n");              /* tion the pointer to '#', then */
      	free(base);
        return(fp);                    /* advance the pointer to after the */
      }                                     /* section title and return it */
      else {                          /* didn't find it: skip this control */
        fseek(fp, looksite, 0);                    /* record, keep looking */
        fscanf(fp, "%*s");                 /* NOTE: "%*s" advances pointer */
      }                                              /* without assignment */
    }
  }

  free(base);

  return(NULL);                         /* couldn't find the right section */
}

/* A FUNCTION WHICH IS NEEDED BY ALL OTHER READING FUNCS *******************/

/*** FindSection: This function finds a given section of the input file & **
 *                returns a pointer positioned to the first record of that *
 *                section. Section titles should be passed without the pre-*
 *                ceding '$'. If it can't find the right section, the func-*
 *                tion returns NULL.                                       *
 ***************************************************************************/

FILE *FindSection(FILE *fp, char *input_section)
{
  int      c;                      /* input happens character by character */
  int      nsought;                       /* holds length of section title */
  char     *base;                              /* string for section title */
  long     looksite;                            /* file position indicator */

  rewind(fp);                /* start looking at the beginning of the file */

  nsought = strlen(input_section);
  base = (char *)calloc(FFIO_MAX_RECORD, sizeof(char));

/*** while loop goes through the file character by character... ************/
   while ( (c=getc(fp)) != EOF) {               /* ...until if finds a '$' */

    if ( c == '$') {                  /* found a sectioning control string */
      looksite = ftell(fp);                               /* where are we? */
      base = fgets(base, FFIO_MAX_RECORD, fp);   /* get sect title (without $)*/
      if ( !(strncmp(base, input_section, nsought)) ) {
        fseek(fp, looksite, 0);        /* found the sought string: reposi- */
        fscanf(fp, "%*s\n");              /* tion the pointer to '$', then */
	free(base);
        return(fp);                    /* advance the pointer to after the */
      }                                     /* section title and return it */
      else {                          /* didn't find it: skip this control */
        fseek(fp, looksite, 0);                    /* record, keep looking */
        fscanf(fp, "%*s");                 /* NOTE: "%*s" advances pointer */
      }                                              /* without assignment */
    }
  }

  free(base);

  return(NULL);                         /* couldn't find the right section */
}

/*** KillSection: erases the section with 'title' from the file 'fp' *******
 ***************************************************************************/

void KillSection(char *filename, char *title)
{
	size_t length;                                 /* length of title string */
	char   *fulltitle;                                      /* title incl. $ */
	char   *temp;                                     /* temporary file name */
	char   *record;                         /* record to be read and written */
	char   *record_ptr;        /* pointer used to remember record for 'free' */
/*	char   *shell_cmd;                                used by system below */
	FILE   *fp;             /* name of file where section needs to be killed */
	FILE   *tmpfile;                               /* name of temporary file */
	int    filedescriptor;                     /*for correct usage of mkstemp*/
	mode_t mode, old_mode;
/* S_IRUSR|S_IWUSR|S_IRGRP|S_IWGRP|S_IROTH|S_IWOTH|S_IXUSR|S_IXGRP|S_IXOTH*/
	mode = S_IXOTH;
	old_mode = umask(mode);
	fulltitle = (char *)calloc(FFIO_MAX_RECORD, sizeof(char));
	temp      = (char *)calloc(FFIO_MAX_RECORD, sizeof(char));
	record    = (char *)calloc(FFIO_MAX_RECORD, sizeof(char));
/*	shell_cmd = (char *)calloc(FFIO_MAX_RECORD, sizeof(char));*/
	record_ptr = record;            /* this is to remember record for 'free' */
	fp = fopen(filename, "r");                      /* open file for reading */
	if ( !fp )
		error("KillSection: error opening file %s", filename);
	temp = strcpy(temp,"killXXXXXX");               /* required by mkstemp() */
	filedescriptor=mkstemp(temp);
	if ( filedescriptor == -1 )              /* get unique name for temp file */
		error("KillSection: error creating temporary file");
	tmpfile = fdopen(filedescriptor, "w");               /* ... and open it for writing */
	if ( !tmpfile )
		error("KillSection: error opening temporary file");
	if ( !FindSection(fp, title) )
		error("KillSection: section to be killed not found");
	rewind(fp);
/* remove section by simply ignoring it while copying the file */
	fulltitle = strcpy(fulltitle, "$");
	fulltitle = strcat(fulltitle, title);
	length    = strlen(fulltitle);
	while (strncmp((record=fgets(record, FFIO_MAX_RECORD, fp)),
		 fulltitle, length))
	fputs(record, tmpfile);
	while (strncmp((record=fgets(record, FFIO_MAX_RECORD, fp)), "$$", 2))
	;
	do {
		record = fgets(record, FFIO_MAX_RECORD, fp);
		if ( !record ) break;
	} while ( strncmp(record, "$", 1) );
	if ( record )
		fputs(record, tmpfile);
	while ( (record=fgets(record, FFIO_MAX_RECORD, fp)) )
		fputs(record, tmpfile);
	fclose(fp);
	fclose(tmpfile);
/* rename tmpfile into new file 
	sprintf(shell_cmd, "cp -f %s %s", temp, filename);
	if ( -1 == system(shell_cmd) )
		error("KillSection: error renaming temp file %s", temp);
	if ( remove(temp) )
		warning("KillSection: temp file %s could not be deleted", temp);
	free(shell_cmd);*/
	if ( -1 == rename(temp, filename) ) {
		error("KillSection: error renaming temp file %s", temp);
	}
/* clean up */
	free(record_ptr);
	free(temp);
	free(fulltitle);
}

/*** CopyDatafile: erases the section with 'title' from the file 'fp' *******
 ***************************************************************************/

void CopyDatafile(char *oldfilename, char *newfilename)
{
	size_t length;                                 /* length of title string */
	char   *fulltitle;                                      /* title incl. $ */
	char   *temp;                                     /* temporary file name */
	char   *record;                         /* record to be read and written */
	char   *record_ptr;        /* pointer used to remember record for 'free' */
/*	char   *shell_cmd;                                used by system below */
	FILE   *fp;             /* name of file where section needs to be killed */
	FILE   *tmpfile;                               /* name of temporary file */
	int    filedescriptor;                     /*for correct usage of mkstemp*/
	mode_t mode, old_mode;
/* S_IRUSR|S_IWUSR|S_IRGRP|S_IWGRP|S_IROTH|S_IWOTH|S_IXUSR|S_IXGRP|S_IXOTH*/
	mode = S_IXOTH;
	old_mode = umask(mode);
	fulltitle = (char *)calloc(FFIO_MAX_RECORD, sizeof(char));
	temp      = (char *)calloc(FFIO_MAX_RECORD, sizeof(char));
	record    = (char *)calloc(FFIO_MAX_RECORD, sizeof(char));
/*	shell_cmd = (char *)calloc(FFIO_MAX_RECORD, sizeof(char));*/
	record_ptr = record;            /* this is to remember record for 'free' */
	fp = fopen(oldfilename, "r");                      /* open file for reading */
	if ( !fp )
		error("CopyDatafile: error opening file %s", oldfilename);
	temp = strcpy(temp,"killXXXXXX");               /* required by mkstemp() */
	filedescriptor=mkstemp(temp);
	if ( filedescriptor == -1 )              /* get unique name for temp file */
		error("CopyDatafile: error creating temporary file");
	tmpfile = fdopen(filedescriptor, "w");               /* ... and open it for writing */
	if ( !tmpfile )
		error("CopyDatafile: error opening temporary file");
	rewind(fp);
	while ( (record=fgets(record, FFIO_MAX_RECORD, fp)) ) {
		fputs(record, tmpfile);
	}
	fclose(fp);
	fclose(tmpfile);
	if ( -1 == rename(temp, newfilename) ) {
		error("CopyDatafile: error renaming temp file %s", temp);
	}
/* clean up */
	free(record_ptr);
	free(temp);
}
