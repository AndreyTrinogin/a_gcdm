/*****************************************************************
 *                                                               *
 *   integrate.c                                                 *
 *                                                               *
 *****************************************************************
 *                                                               *
 *   written by JR, modified by Yoginho                          *
 *                                                               *
 *****************************************************************
 *                                                               *
 * This file has problem-specific stuff not needed for right     *
 * hand of ODE. The function Blastoderm runs the fly model and   *
 * calls the appropriate solver for propagating the equations.   *
 * PrintBlastoderm formats and prints the output of the Blasto-  *
 * derm function and FreeSolution frees the solution allocated   *
 * by Blastoderm. integrate.h also comes with a few utility      *
 * functions for TLists which are linked lists used to initia-   *
 * lize the time/mode table for Blastoderm.                      *
 *                                                               *
 *****************************************************************
 *                                                               *
 * NOTE: all right-hand-of-ODE-specific stuff is in zygotic.h    *
 *                                                               *
 *****************************************************************/


#include <stdio.h>
#include <stdlib.h>
#ifdef ICC
#include <mathimf.h>
#else
#include <math.h>
#endif
#include <float.h>
#include <string.h>

#include <error.h>               /* for error handling functions */
#include <maternal.h>        /* for GetBTimes to fetch bias info */
#include <integrate.h>                              /* obviously */

#include <IntegralSolve.h>
#include <ODESolve.h>
#include <zygotic.h>                /* still needed for mutators */
#ifdef LIBSVM
#include <libsvm/svm.h>
#elif FANN
#include <floatfann.h>
#endif
/**********************************************************************
 **********************************************************************
 * The routines below, and all executible programs which use them,    *
 * are dedicated to Jerry Garcia, who died on August 9, 1995.         *
 * If you are using any of these functions, please do not             *
 * alter the following external array, which should remain            *
 * readable in the executable, using the strings(1) or what(1) cmds.  *
 * You may remove the const qualifier for non-ANSI compilers, and     *
 * you are invited to add your own functions  and  copy  this         *
 * notice to your own code. JR, August 14, 1995. Jerry, we miss you.  *
 **********************************************************************
 **********************************************************************/

const char Jerry[] = "@(#) In Memoriam Jerome John Garcia, 8/1/42-8/9/95";

/*added by MacKoel*/

static int INTERVAL;
static Blastoderm*thisGenotype;
static int zero_mrna = 0;

/******************/

void Blastoderm_setThisGenotype(Blastoderm*p)
{
	thisGenotype = p;
}

void integrate_set_zero_mrna(int arg)
{
	zero_mrna = arg;
}

/*** BLASTODERM FUNCTIONS **************************************************/

/*** BlastList:                                                            *
 ***************************************************************************/

TList*BlastList(Blastoderm*Genotype, DArrPtr* tabtimes)
{
	const  double    epsilon     = EPSILON;      /* epsilons: very small in- */
	int i;
	TList            *entries    = NULL;   /* temp linked list for times and */
/* allocate transitions array (skip if ndivs == 0) */
	if ( defs->ndivs > 0 )
		Genotype->transitions = (double *)calloc(defs->ndivs, sizeof(double));
/* INITIALIZATION OF THE MODEL STRUCTS AND ARRAYS **************************/
/* free structures that are already present */
/* get bias times and initialize information about cell divisions */
	Genotype->biastimes = GetBTimes(Genotype->genotype_ID);
	if ( !(Genotype->biastimes->array) )
		error("Blastoderm: error getting bias times");
	if ( defs->ndivs > 0 ) {
		if ( !(Genotype->divtable = GetDivtable()) )
			error("Blastoderm: error getting division table");
		if ( !(Genotype->durations = GetDurations()) )
			error("Blastoderm: error getting division durations");
		for( i = 0; i < defs->ndivs; i++)
			Genotype->transitions[i] = Genotype->divtable[i] - Genotype->durations[i];
	}
/* entries is a linked list, which we use to set up the solution struct    *
 * and the what2do array; each of these needs an entry for:                *
 * - start and end (gastrulation) time                                     *
 * - times for mitoses: - beginning of mitosis                             *
 *                      - cell division time (still belongs to previous    *
 *                        cleavage cycle)                                  *
 *                      - time right after cell division (+EPSILON), be-   *
 *                        longs to new cell cycle with doubled nnucs       *
 * - times at which we add bias                                            *
 * - tabulated times for which we have data or which we want to display    */
/* add start and end (gastrulation) time */
	entries = InitTList();
/* add all times required for mitoses (skip this for 0 div schedule) */
	if ( defs->ndivs > 0 )
		for ( i = 0; i < defs->ndivs; i++) {
			entries = InsertTList(entries, Genotype->divtable[i], DIVIDE);
			if( (GetCCycle(Genotype->divtable[i])) == (GetCCycle(Genotype->divtable[i] + epsilon)) )
				error("Blastoderm: epsilon of %g too small!", epsilon);
			entries = InsertTList(entries, Genotype->divtable[i] + epsilon, PROPAGATE);
			entries = InsertTList(entries, Genotype->transitions[i], MITOTATE);
			if( (GetCCycle(Genotype->transitions[i])) != (GetCCycle(Genotype->transitions[i] + epsilon)) )
				error("Blastoderm: division within epsilon of %g!", epsilon);
			entries = InsertTList(entries, Genotype->transitions[i] + epsilon, PROPAGATE);
/*			entries = InsertTList(entries, Genotype->transitions[i] , PROPAGATE);*/
		}
/* add bias times */
	for(i=0; i < Genotype->biastimes->size; i++)
		entries = InsertTList(entries, Genotype->biastimes->array[i], ADD_BIAS | PROPAGATE);
/* tabulated times */
	for(i=0; i < tabtimes->size; i++)
		entries = InsertTList(entries, tabtimes->array[i], PROPAGATE);
	return entries;
}

Blastoderm*Blastoderm_create(char*genotype, DArrPtr* tabtimes, Derivative cDDt, Derivative2 c2DDt, Jacobian cD2Dt2, Derivative pDDt, Jacobian pD2Dt2, char*name)
{
	Blastoderm*Genotype;
	TList*current;
	int i,j;
	Genotype = (Blastoderm*)malloc(sizeof(Blastoderm));
	Genotype->genotype_ID = (char*)calloc(MAX_RECORD, sizeof(char));
	Genotype->genotype_ID = strcpy(Genotype->genotype_ID, genotype);
	Genotype->entries = BlastList(Genotype, tabtimes);
	Genotype->Solution = (NArrPtr*)malloc(sizeof(NArrPtr));
	Genotype->Solution->size  = CountEntries(Genotype->entries);
	Genotype->Solution->array = (NucState *)calloc(Genotype->Solution->size, sizeof(NucState));
	Genotype->Solution2 = (NArrPtr*)malloc(sizeof(NArrPtr));
	Genotype->Solution2->size  = CountEntries(Genotype->entries);
	Genotype->Solution2->array = (NucState *)calloc(Genotype->Solution2->size, sizeof(NucState));
	Genotype->what2do = (int *)calloc(Genotype->Solution->size, sizeof(int));
	Genotype->havedata = (int *)calloc(Genotype->Solution->size, sizeof(int));
	current = Genotype->entries;
	for( i = 0; i < Genotype->Solution->size; i++) {
		Genotype->Solution->array[i].time = current->time;
		Genotype->Solution->array[i].state = (DArrPtr*)malloc(sizeof(DArrPtr));
		Genotype->Solution->array[i].state->size = current->n;
		Genotype->Solution->array[i].state->array = (double *)calloc(current->n, sizeof(double));
		for( j = 0; j < current->n; j++)
			Genotype->Solution->array[i].state->array[j] = 0;
		Genotype->Solution2->array[i].time = current->time;
		Genotype->Solution2->array[i].state = (DArrPtr*)malloc(sizeof(DArrPtr));
		Genotype->Solution2->array[i].state->size = current->n;
		Genotype->Solution2->array[i].state->array = (double *)calloc(current->n, sizeof(double));
		for( j = 0; j < current->n; j++)
			Genotype->Solution2->array[i].state->array[j] = 0;
		Genotype->what2do[i] = current->op;
		Genotype->havedata[i] = 0;
/* data is not allowed at t=0 - see InitTTs comment */
		for( j = 1; j < tabtimes->size; j++)
			if ( tabtimes->array[j] == current->time )
				Genotype->havedata[i] = 1;
		current = current->next;
	}
	if ( c2DDt ) {
		Genotype->Concentrations = ODESolve_create2(name, c2DDt);
		Genotype->Lagrangians = ODESolve_create2(name, c2DDt);
	} else {
		Genotype->Concentrations = ODESolve_create(name, cDDt, cD2Dt2);
		Genotype->Lagrangians = ODESolve_create(name, pDDt, pD2Dt2);
	}
	Genotype->Concentrations->nintervals = Genotype->Solution->size - 1 - defs->ndivs - defs->ndivs;
	Genotype->Concentrations->intervals = (ODESolutionPoly**)calloc(Genotype->Concentrations->nintervals, sizeof(ODESolutionPoly*));
	Genotype->Lagrangians->nintervals = Genotype->Solution->size - 1 - defs->ndivs - defs->ndivs;
	Genotype->Lagrangians->intervals = (ODESolutionPoly**)calloc(Genotype->Lagrangians->nintervals, sizeof(ODESolutionPoly*));
	Genotype->rhs = 0;
	return Genotype;
}

Blastoderm*gcdm_blastoderm_new(char*genotype, DArrPtr* tabtimes, Derivative cDDt, Derivative2 c2DDt, Jacobian cD2Dt2, Derivative pDDt, Jacobian pD2Dt2, char*name, char*name2)
{
	Blastoderm*Genotype;
	TList*current;
	int i,j;
	Genotype = (Blastoderm*)malloc(sizeof(Blastoderm));
	Genotype->genotype_ID = (char*)calloc(MAX_RECORD, sizeof(char));
	Genotype->genotype_ID = strcpy(Genotype->genotype_ID, genotype);
	Genotype->entries = BlastList(Genotype, tabtimes);
	Genotype->Solution = (NArrPtr*)malloc(sizeof(NArrPtr));
	Genotype->Solution->size  = CountEntries(Genotype->entries);
	Genotype->Solution->array = (NucState *)calloc(Genotype->Solution->size, sizeof(NucState));
	Genotype->Solution2 = (NArrPtr*)malloc(sizeof(NArrPtr));
	Genotype->Solution2->size  = CountEntries(Genotype->entries);
	Genotype->Solution2->array = (NucState *)calloc(Genotype->Solution2->size, sizeof(NucState));
	Genotype->what2do = (int *)calloc(Genotype->Solution->size, sizeof(int));
	Genotype->havedata = (int *)calloc(Genotype->Solution->size, sizeof(int));
	current = Genotype->entries;
	for( i = 0; i < Genotype->Solution->size; i++) {
		Genotype->Solution->array[i].time = current->time;
		Genotype->Solution->array[i].state = (DArrPtr*)malloc(sizeof(DArrPtr));
		Genotype->Solution->array[i].state->size = current->n;
		Genotype->Solution->array[i].state->array = (double *)calloc(current->n, sizeof(double));
		for( j = 0; j < current->n; j++)
			Genotype->Solution->array[i].state->array[j] = 0;
		Genotype->Solution2->array[i].time = current->time;
		Genotype->Solution2->array[i].state = (DArrPtr*)malloc(sizeof(DArrPtr));
		Genotype->Solution2->array[i].state->size = current->n;
		Genotype->Solution2->array[i].state->array = (double *)calloc(current->n, sizeof(double));
		for( j = 0; j < current->n; j++)
			Genotype->Solution2->array[i].state->array[j] = 0;
		Genotype->what2do[i] = current->op;
		Genotype->havedata[i] = 0;
/* data is not allowed at t=0 - see InitTTs comment */
		for( j = 1; j < tabtimes->size; j++)
			if ( tabtimes->array[j] == current->time )
				Genotype->havedata[i] = 1;
		current = current->next;
	}
	Genotype->Concentrations = ODESolve_new(name2, c2DDt, name, cDDt, cD2Dt2);
	Genotype->Lagrangians = ODESolve_new(name2, c2DDt, name, pDDt, pD2Dt2);
	Genotype->Concentrations->nintervals = Genotype->Solution->size - 1 - defs->ndivs - defs->ndivs;
	Genotype->Concentrations->intervals = (ODESolutionPoly**)calloc(Genotype->Concentrations->nintervals, sizeof(ODESolutionPoly*));
	Genotype->Lagrangians->nintervals = Genotype->Solution->size - 1 - defs->ndivs - defs->ndivs;
	Genotype->Lagrangians->intervals = (ODESolutionPoly**)calloc(Genotype->Lagrangians->nintervals, sizeof(ODESolutionPoly*));
	Genotype->rhs = 0;
	Genotype->dosage = 2;
	Genotype->zero_bias = 0;
	return Genotype;
}

/*** Blastoderm: runs embryo model and returns an array of concentration ***
 *               arrays for each requested time (given by TabTimes) using  *
 *               stephint as a suggested stepsize and accuracy as the re-  *
 *               quired numerical accuracy (in case of adaptive stepsize   *
 *               solvers or global stepsize control) for the solver.       *
 *         NOTE: TabTimes *must* start from 0 and have increasing times.   *
 *               It includes times when bias is added, cell division times *
 *               an d times for which we have data and ends with the time   *
 *               of gastrulation.                                          *
 ***************************************************************************/

void Blastoderm_SolveConc(Blastoderm*Genotype, double stephint, double accuracy, FILE *slog)
{
	const  double    big_epsilon = BIG_EPSILON; /* creases used for division */
	DArrPtr*bias;                 /* bias for given time & genotype */
	int              i, ii, j;                                /* loop counters */
	int              k;                    /* index of gene k in current nuc */
	int              ap;                         /* nuc. position on AP axis */
	int              lin;             /* first lineage number at each ccycle */
	int              rule;                         /* MITOSIS or INTERPHASE? */
	int interval;
	unsigned int ccycle;                   /* what cleavage cycle are we in? */
	int num_nucs;
/* for each genotype, the 'genotype' variable has to be made static to zy- *
 * gotic.c so that the derivative functions know which genotype they're    *
 * dealing with (i.e. they need to get the appropriate bcd gradient)       */
	InitGenotypeIndex(Genotype->genotype_ID);
/* RUNNING THE MODEL *******************************************************/
/* Before running the model, mutate zygotic params appropriately
	Mutate(Name2Gtype(Genotype->genotype));*/
	if ( debug ) {
		fprintf(slog, "\n--------------------------------------------------");
		fprintf(slog, "--------------------------------------------------\n");
		fprintf(slog, "Blastoderm: mutated genotype to %s.\n", Genotype->genotype_ID);
	}
/* Below is the loop that evaluates the solution and saves it in the solu- *
 * tion struct                                                             */
	interval = 0;
	for ( j = 0; j < Genotype->Solution->array[0].state->size; j++)
		Genotype->Solution->array[0].state->array[j] = 0.0;
	for(i = 0; i < Genotype->Solution->size; i++) {
/* First we have to set the propagation rule in zygotic.c (either MITOSIS  *
 * or INTERPHASE) to make sure that the correct differential equations are *
 * propagated (rules are defined in zygotic.h)                             */
		rule = GetRule(Genotype->Solution->array[i].time);
		SetRule(rule);
		ccycle = GetCCycle(Genotype->Solution->array[i].time);
		SetCCycle(ccycle);
		num_nucs = GetNNucs(Genotype->Solution->array[i].time);
		SetNNucs(num_nucs);
		if ( debug ) {
			if ( rule == 0 )
				fprintf(slog, "Blastoderm: rule is INTERPHASE.\n");
			else if ( rule == 1 && !(Genotype->what2do[i] & DIVIDE) )
				fprintf(slog, "Blastoderm: rule is MITOSIS.\n");
			else if ( Genotype->what2do[i] & DIVIDE )
				fprintf(slog, "Blastoderm: rule is DIVISION.\n");
		}
/* ADD_BIAS is a special op in that it can be combined with any other op   *
 * (see also next comment); we can add (or subtract) protein conentrations *
 * at any time; this is for adding initial conditions for genes that are   *
 * both maternal and zygotic (e.g. cad and hb) or to simulated perturba-   *
 * tions like heat shocks or induced overexpression of certain genes;      *
 * note that the bias lives in maternal.c and has to be fetched from there */
		if (Genotype->what2do[i] & ADD_BIAS) {
			for ( j = 0; j < Genotype->biastimes->size; j++) {
				if ( fabs( Genotype->Solution->array[i].time - Genotype->biastimes->array[j] ) < big_epsilon ) {
					bias = GetBias(Genotype->biastimes->array[j]);
					for (ii=0; ii < bias->size; ii++)
						Genotype->Solution->array[i].state->array[ii] += bias->array[ii];
				}
			}
			if ( debug )
				fprintf(slog, "Blastoderm: added bias at time %f.\n", Genotype->Solution->array[i].time);
		}
/* The ops below can be executed in addition to ADD_BIAS but they cannot   *
 * be combined between themselves; if more than one op is set, the prio-   *
 * rities are as follows: NO_OP > DIVIDE > PROPAGATE; please make sure     *
 * that you do not set double ops, which will only obfuscate the code;     *
 *                                                                         *
 * NO_OP simply does nothing (used for gastrulation time)                  */
		if (Genotype->what2do[i] & NO_OP)
			;
/* This is the DIVIDE rule: only symmetrical division is supported yet,    *
 * i.e. both daughter nuclei inherit all concentrations from their mother; *
 * we have to take special care of the fact that we might not need the     *
 * most anterior and most posterior daughter nuclei, depending on the ran- *
 * ge of nuclei in cycle 14 we've chosen to anneal on; e.g. if the most    *
 * anterior nucleus in cycle 14 has an odd lineage number, we need to push *
 * its sibling off the anterior limit when we divide after cycle 13; or in *
 * other words: our most anterior cell in cycle 14 is the posterior daugh- *
 * ter of our most anterior cell in cycle 13; therefore, we need to 'loose'*
 * its anterior sibling; the same applies for the posterior end, where we  *
 * want to loose the most posterior daughter cell if we don't need it any  *
 * more at the later cycle                                                 *
 *                                                                         *
 * This is implemented as follows below: lin is the lineage number of the  *
 * most anterior cell of the next cell cycle; if it's odd numbered, we'll  *
 * push off the most anterior daughter by subtracting the number of genes  *
 * from the daughter indices (i.e. we shift the solution array for the     *
 * next cycle posteriorly by one nucleus); if on the other hand, the most  *
 * posterior nucleus lies outside our new array, we just forget about it   */
		else if ( Genotype->what2do[i] & DIVIDE ) {
			if(model == HYBRID || model == HEDIRECT || model == HESRR || model == HEURR) {
				lin = GetStartLin(Genotype->Solution->array[i+1].time);
				for (j=0; j < Genotype->Solution->array[i].state->size; j++) {
					k  = j % defs->ngenes;     /* k: index of gene k in current nucleus */
					ap = j / defs->ngenes;      /* ap: rel. nucleus position on AP axis */
/* evaluate ii: index of anterior daughter nucleus */
					if ( lin % 2 )
						ii = 2 * ap * defs->ngenes + k - defs->ngenes;
					else
						ii = 2 * ap * defs->ngenes + k;
/* skip the first most anterior daughter nucleus in case lin is odd */
					if ( ii >= 0 ) {
						Genotype->Solution->array[i+1].state->array[ii] = Genotype->Solution->array[i].state->array[j];
						Genotype->Solution2->array[i+1].state->array[ii] = Genotype->Solution2->array[i].state->array[j];
					}
/* the second daughter only exists if it is still within the region */
					if ( ii + defs->ngenes < Genotype->Solution->array[i+1].state->size ) {
						Genotype->Solution->array[i+1].state->array[ii+defs->ngenes] = Genotype->Solution->array[i].state->array[j];
						Genotype->Solution2->array[i+1].state->array[ii+defs->ngenes] = Genotype->Solution2->array[i].state->array[j];
					}
				}
				if ( debug )
					fprintf(slog, "Blastoderm: nuclear division %d -> %d nuclei.\n", Genotype->Solution->array[i].state->size / defs->ngenes, Genotype->Solution->array[i+1].state->size / defs->ngenes);
			} else {
				for (j=0; j < Genotype->Solution->array[i].state->size; j++) {
					Genotype->Solution->array[i+1].state->array[j] = Genotype->Solution->array[i].state->array[j];
					Genotype->Solution2->array[i+1].state->array[j] = Genotype->Solution2->array[i].state->array[j];
				}
			}
		} else if ( Genotype->what2do[i] & MITOTATE ) {
/*		printf("aAAHHH! in MITOTATE, t=%.32f, t+epilon=%.32f\n",
						Genotype->Solution->array[i].time,
								Genotype->Solution->array[i+1].time );*/
			for ( j = 0; j < Genotype->Solution->array[i].state->size; j++) {
				Genotype->Solution->array[i+1].state->array[j] = Genotype->Solution->array[i].state->array[j];
				Genotype->Solution2->array[i+1].state->array[j] = Genotype->Solution2->array[i].state->array[j];
			}
		} else if ( Genotype->what2do[i] & PROPAGATE ) {
/* In case we have to PROPAGATE the differential equeations, we call the   *
 * solver; you have to make sure that the appropriate rule has been set in *
 * zygotic.c (use SetRule(), rules are MITOSIS or INTERPHASE), otherwise   *
 * you will propagate the wrong equations; the solver needs pointers to    *
 * arrays of concentration for start and end time as well as those start   *
 * and end times themselves; all solvers need a suggested stepsize, adap-  *
 * tive stepsize solvers need the accuracy argument; we also need the accu-*
 * racy argument for global stepsize control (if ever implemented); lastly *
 * we need to tell the solver how big input and output arrays are          */
			if (Genotype->Concentrations->intervals[interval]) {
				ODESolutionPoly_delete(Genotype->Concentrations->intervals[interval]);
			}
			Genotype->Concentrations->intervals[interval]=ODESolve_Solve2(Genotype->Concentrations,Genotype->Solution->array[i].state->array,Genotype->Solution2->array[i].state->array, Genotype->Solution->array[i+1].state->array,Genotype->Solution2->array[i+1].state->array, Genotype->Solution->array[i].time, Genotype->Solution->array[i+1].time, stephint, accuracy, Genotype->Solution->array[i].state->size, slog);
			interval++;
		} else {
/* unknown op? -> error! */
			error("op was %d!?", Genotype->what2do[i]);
		}
	}
}

void gcdm_blastoderm_set_hints(Blastoderm*Genotype, double dosage, int zero_bias, int rhs)
{
	Genotype->rhs = rhs;
	Genotype->dosage = dosage;
	Genotype->zero_bias = zero_bias;
}

void gcdm_blastoderm_solve_conc_with_hints(Blastoderm*Genotype, double stephint, double accuracy, FILE *slog, double dosage, int zero_bias, int rhs)
{
	const  double    big_epsilon = BIG_EPSILON; /* creases used for division */
	DArrPtr*bias;                 /* bias for given time & genotype */
	int              i, ii, j;                                /* loop counters */
	int              k;                    /* index of gene k in current nuc */
	int              ap;                         /* nuc. position on AP axis */
	int              lin;             /* first lineage number at each ccycle */
	int              rule;                         /* MITOSIS or INTERPHASE? */
	int interval;
	unsigned int ccycle;                   /* what cleavage cycle are we in? */
	int num_nucs;
	int nmrna = defs->ngenes / 2;
/* for each genotype, the 'genotype' variable has to be made static to zy- *
 * gotic.c so that the derivative functions know which genotype they're    *
 * dealing with (i.e. they need to get the appropriate bcd gradient)       */
	InitGenotypeIndexWithBicoidDosage(Genotype->genotype_ID, dosage);
/* RUNNING THE MODEL *******************************************************/
/* Before running the model, mutate zygotic params appropriately
	Mutate(Name2Gtype(Genotype->genotype));*/
	if ( debug ) {
		fprintf(slog, "\n--------------------------------------------------");
		fprintf(slog, "--------------------------------------------------\n");
		fprintf(slog, "Blastoderm: mutated genotype to %s.\n", Genotype->genotype_ID);
	}
/* Below is the loop that evaluates the solution and saves it in the solu- *
 * tion struct                                                             */
	interval = 0;
	for ( j = 0; j < Genotype->Solution->array[0].state->size; j++) {
		Genotype->Solution->array[0].state->array[j] = 0.0;
		Genotype->Solution2->array[0].state->array[j] = 0.0;
	}
	gcdm_init_delayed(Genotype->Solution->array[0].state->size);
	for(i = 0; i < Genotype->Solution->size; i++) {
/* First we have to set the propagation rule in zygotic.c (either MITOSIS  *
 * or INTERPHASE) to make sure that the correct differential equations are *
 * propagated (rules are defined in zygotic.h)                             */
		rule = GetRule(Genotype->Solution->array[i].time);
		SetRule(rule);
		ccycle = GetCCycle(Genotype->Solution->array[i].time);
		SetCCycle(ccycle);
		num_nucs = GetNNucs(Genotype->Solution->array[i].time);
		SetNNucs(num_nucs);
		if ( debug ) {
			if ( rule == 0 )
				fprintf(slog, "Blastoderm: rule is INTERPHASE.\n");
			else if ( rule == 1 && !(Genotype->what2do[i] & DIVIDE) )
				fprintf(slog, "Blastoderm: rule is MITOSIS.\n");
			else if ( Genotype->what2do[i] & DIVIDE )
				fprintf(slog, "Blastoderm: rule is DIVISION.\n");
		}
/* ADD_BIAS is a special op in that it can be combined with any other op   *
 * (see also next comment); we can add (or subtract) protein conentrations *
 * at any time; this is for adding initial conditions for genes that are   *
 * both maternal and zygotic (e.g. cad and hb) or to simulated perturba-   *
 * tions like heat shocks or induced overexpression of certain genes;      *
 * note that the bias lives in maternal.c and has to be fetched from there */
		if (Genotype->what2do[i] & ADD_BIAS) {
			if ( zero_bias == 0 ) {
				for ( j = 0; j < Genotype->biastimes->size; j++) {
					if ( fabs( Genotype->Solution->array[i].time - Genotype->biastimes->array[j] ) < big_epsilon ) {
						bias = GetBias(Genotype->biastimes->array[j]);
						for (ii=0; ii < bias->size; ii++) {
							Genotype->Solution->array[i].state->array[ii] += bias->array[ii];
							Genotype->Solution2->array[i].state->array[ii] += bias->array[ii];
						}
					}
				}
			}
			if ( debug )
				fprintf(slog, "Blastoderm: added bias at time %f.\n", Genotype->Solution->array[i].time);
		}
/* The ops below can be executed in addition to ADD_BIAS but they cannot   *
 * be combined between themselves; if more than one op is set, the prio-   *
 * rities are as follows: NO_OP > DIVIDE > PROPAGATE; please make sure     *
 * that you do not set double ops, which will only obfuscate the code;     *
 *                                                                         *
 * NO_OP simply does nothing (used for gastrulation time)                  */
		if (Genotype->what2do[i] & NO_OP) {
            gcdm_rem_delayed();
		}
/* This is the DIVIDE rule: only symmetrical division is supported yet,    *
 * i.e. both daughter nuclei inherit all concentrations from their mother; *
 * we have to take special care of the fact that we might not need the     *
 * most anterior and most posterior daughter nuclei, depending on the ran- *
 * ge of nuclei in cycle 14 we've chosen to anneal on; e.g. if the most    *
 * anterior nucleus in cycle 14 has an odd lineage number, we need to push *
 * its sibling off the anterior limit when we divide after cycle 13; or in *
 * other words: our most anterior cell in cycle 14 is the posterior daugh- *
 * ter of our most anterior cell in cycle 13; therefore, we need to 'loose'*
 * its anterior sibling; the same applies for the posterior end, where we  *
 * want to loose the most posterior daughter cell if we don't need it any  *
 * more at the later cycle                                                 *
 *                                                                         *
 * This is implemented as follows below: lin is the lineage number of the  *
 * most anterior cell of the next cell cycle; if it's odd numbered, we'll  *
 * push off the most anterior daughter by subtracting the number of genes  *
 * from the daughter indices (i.e. we shift the solution array for the     *
 * next cycle posteriorly by one nucleus); if on the other hand, the most  *
 * posterior nucleus lies outside our new array, we just forget about it   */
		else if ( Genotype->what2do[i] & DIVIDE ) {
			if(model == HYBRID || model == HEDIRECT || model == HESRR || model == HEURR || model == HEQUENCHING) {
				lin = GetStartLin(Genotype->Solution->array[i+1].time);
				for (j=0; j < Genotype->Solution->array[i].state->size; j++) {
					k  = j % defs->ngenes;     /* k: index of gene k in current nucleus */
					ap = j / defs->ngenes;      /* ap: rel. nucleus position on AP axis */
/* evaluate ii: index of anterior daughter nucleus */
					if ( lin % 2 )
						ii = 2 * ap * defs->ngenes + k - defs->ngenes;
					else
						ii = 2 * ap * defs->ngenes + k;
/* skip the first most anterior daughter nucleus in case lin is odd */
					if ( ii >= 0 ) {
						Genotype->Solution->array[i+1].state->array[ii] = Genotype->Solution->array[i].state->array[j];
						Genotype->Solution2->array[i+1].state->array[ii] = Genotype->Solution2->array[i].state->array[j];
					}
/* the second daughter only exists if it is still within the region */
					if ( ii + defs->ngenes < Genotype->Solution->array[i+1].state->size ) {
						Genotype->Solution->array[i+1].state->array[ii + defs->ngenes] = Genotype->Solution->array[i].state->array[j];
						Genotype->Solution2->array[i+1].state->array[ii + defs->ngenes] = Genotype->Solution2->array[i].state->array[j];
					}
				}
/*				if ( zero_mrna == 1 ) {
					int nmrna = defs->ngenes / 2;
					for ( ap = 0; ap < Genotype->Solution->array[i + 1].state->size; ap += defs->ngenes ) {
						for ( k = nmrna; k < defs->ngenes; k++ ) {
							Genotype->Solution->array[i+1].state->array[ap + k] = 0;
						}
					}
				}*/
				if ( debug )
					fprintf(slog, "Blastoderm: nuclear division %d -> %d nuclei.\n", Genotype->Solution->array[i].state->size / defs->ngenes, Genotype->Solution->array[i+1].state->size / defs->ngenes);
			} else {
				for (j=0; j < Genotype->Solution->array[i].state->size; j++) {
					Genotype->Solution->array[i+1].state->array[j] = Genotype->Solution->array[i].state->array[j];
					Genotype->Solution2->array[i+1].state->array[j] = Genotype->Solution2->array[i].state->array[j];
				}
			}
			gcdm_rem_delayed();
			gcdm_init_delayed(Genotype->Solution->array[i + 1].state->size);
		} else if ( Genotype->what2do[i] & MITOTATE ) {
/*		printf("aAAHHH! in MITOTATE, t=%.32f, t+epilon=%.32f\n",
						Genotype->Solution->array[i].time,
								Genotype->Solution->array[i+1].time );*/
			for ( j = 0; j < Genotype->Solution->array[i].state->size; j++) {
				Genotype->Solution->array[i+1].state->array[j] = Genotype->Solution->array[i].state->array[j];
				Genotype->Solution2->array[i+1].state->array[j] = Genotype->Solution2->array[i].state->array[j];
			}
            if ( zero_mrna == 1 ) {
				for ( ap = 0; ap < Genotype->Solution->array[i + 1].state->size; ap += defs->ngenes ) {
					for ( k = nmrna; k < defs->ngenes; k++ ) {/* k: index of gene k in current nucleus */
						Genotype->Solution->array[i+1].state->array[ap + k] = 0;
					}
				}
			}
		} else if ( Genotype->what2do[i] & PROPAGATE ) {
/* In case we have to PROPAGATE the differential equeations, we call the   *
 * solver; you have to make sure that the appropriate rule has been set in *
 * zygotic.c (use SetRule(), rules are MITOSIS or INTERPHASE), otherwise   *
 * you will propagate the wrong equations; the solver needs pointers to    *
 * arrays of concentration for start and end time as well as those start   *
 * and end times themselves; all solvers need a suggested stepsize, adap-  *
 * tive stepsize solvers need the accuracy argument; we also need the accu-*
 * racy argument for global stepsize control (if ever implemented); lastly *
 * we need to tell the solver how big input and output arrays are          */
			if (Genotype->Concentrations->intervals[interval]) {
				ODESolutionPoly_delete(Genotype->Concentrations->intervals[interval]);
			}
			if ( rhs == 0 )
				Genotype->Concentrations->intervals[interval] = ODESolve_Solve2(Genotype->Concentrations,Genotype->Solution->array[i].state->array,Genotype->Solution2->array[i].state->array, Genotype->Solution->array[i+1].state->array,Genotype->Solution2->array[i+1].state->array, Genotype->Solution->array[i].time, Genotype->Solution->array[i+1].time, stephint, accuracy, Genotype->Solution->array[i].state->size, slog);
			else if ( rhs == 1 )
				Genotype->Concentrations->intervals[interval] = ODESolve_Solve(Genotype->Concentrations,Genotype->Solution->array[i].state->array, Genotype->Solution->array[i+1].state->array, Genotype->Solution->array[i].time, Genotype->Solution->array[i+1].time, stephint, accuracy, Genotype->Solution->array[i].state->size, slog);
			interval++;
		} else {
/* unknown op? -> error! */
			error("op was %d!?", Genotype->what2do[i]);
		}
	}
}

/*** PrintBlastoderm: writes the output of the model to a stream specified *
 *                    by the fp file pointer; the table is a solution of   *
 *                    the model as returned by Blastoderm(), the id speci- *
 *                    fies the title of the output and ndigits specifies   *
 *                    the floating point precision of the concentrations   *
 *                    to be printed                                        *
 ***************************************************************************/

void PrintBlastoderm(FILE *fp, NArrPtr*table, char *id, int ndigits, int columns)
{
	int                i, j, k;                       /* local loop counters */
	int                lineage;                /* lineage number for nucleus */
/* print title (id) */
	fprintf(fp, "$%s\n", id);
/* print table with correct lineage numbers (obtained from maternal.c) */
	for (i=0; i < table->size; i++) {
		for(j=0; j < (table->array[i].state->size / columns); j++) {
			lineage = GetStartLin(table->array[i].time) + j;
			fprintf(fp, "%5d %9.3f", lineage, table->array[i].time);
			for (k=0; k < columns; k++)
				fprintf(fp, " %*.*f", ndigits+5, ndigits, table->array[i].state->array[k+(j*columns)]);
			fprintf(fp, "\n");
		}
		fprintf(fp, "\n\n");
	}
	fprintf(fp,"$$\n");
	fflush(fp);
}

void PrintBlastodermUgly(FILE *fp, NArrPtr*table, char *id, int ndigits, int columns, int flaggs)
{
	int                i, j, k;                       /* local loop counters */
	int                lineage;                /* lineage number for nucleus */
	char sep = ',';
	if ( flaggs == 3 ) {
		fprintf(fp, "LinN");
		for (i=1; i < table->size; i++) {
			for(j=0; j < (table->array[i].state->size / columns); j++) {
				lineage = GetStartLin(table->array[i].time) + j;
				for (k=0; k < columns; k++) {
					fprintf(fp, "%c%d", sep, lineage);
				}
			}
		}
		fprintf(fp, "\n");
		fprintf(fp, "Time");
		for (i=1; i < table->size; i++) {
			for(j=0; j < (table->array[i].state->size / columns); j++) {
				for (k=0; k < columns; k++) {
					fprintf(fp, "%c%6.3f", sep, table->array[i].time);
				}
			}
		}
		fprintf(fp, "\n");
	} else if ( flaggs == 2 ) {
		for (i = 1; i < table->size; i++) {
			for(j = 0; j < (table->array[i].state->size / columns); j++) {
				for (k=0; k < columns; k++) {
                    if ( i == 1 && j == 0 && k == 0 ) {
                        fprintf(fp, "%cW%d", sep, i * table->array[i].state->size + j * columns + k);
                    } else {
                        fprintf(fp, "%cW%d", sep, i * table->array[i].state->size + j * columns + k);
                    }
				}
			}
		}
		fprintf(fp, "\n");
		return;
	}
	fprintf(fp, "%s", id);
	if ( ndigits < 0 ) {
		int nd = -ndigits;
		for (i=1; i < table->size; i++) {
			for(j=0; j < (table->array[i].state->size / columns); j++) {
				for (k=0; k < columns; k++) {
                    if ( flaggs == 1 && i == 1 && j == 0 && k == 0 ) {
                        fprintf(fp, "%.*e", nd, table->array[i].state->array[k+(j*columns)]);
                    } else {
                        fprintf(fp, "%c%.*e", sep, nd, table->array[i].state->array[k+(j*columns)]);
                    }
				}
			}
		}
	} else {
		for (i=1; i < table->size; i++) {
			for(j=0; j < (table->array[i].state->size / columns); j++) {
				for (k=0; k < columns; k++) {
                    if ( flaggs == 1 && i == 1 && j == 0 && k == 0 ) {
                        fprintf(fp, "%.*f", ndigits, table->array[i].state->array[k+(j*columns)]);
                    } else {
                        fprintf(fp, "%c%.*f", sep, ndigits, table->array[i].state->array[k+(j*columns)]);
                    }
				}
			}
		}
	}
	fprintf(fp,"\n");
	fflush(fp);
}

void PrintBlastodermUgly1(FILE *fp, NArrPtr*table, char *id, int ndigits, int columns, int flaggs)
{
	int                i, j, k;                       /* local loop counters */
	int                lineage;                /* lineage number for nucleus */
	double summ;
	char sep = ',';
	if ( flaggs == 0 ) {
		fprintf(fp, "LinN");
		for (i=0; i < table->size; i++) {
			for(j=0; j < (table->array[i].state->size / columns); j++) {
				lineage = GetStartLin(table->array[i].time) + j;
//				for (k=0; k < columns; k++) {
					fprintf(fp, "%c%d", sep, lineage);
//				}
			}
		}
		fprintf(fp, "\n");
		fprintf(fp, "Time");
		for (i=0; i < table->size; i++) {
			for(j=0; j < (table->array[i].state->size / columns); j++) {
//				for (k=0; k < columns; k++) {
					fprintf(fp, "%c%6.3f", sep, table->array[i].time);
//				}
			}
		}
		fprintf(fp, "\n");
	}
	fprintf(fp, "%s", id);
	if ( ndigits < 0 ) {
		int nd = -ndigits;
		for (i=0; i < table->size; i++) {
			for(j=0; j < (table->array[i].state->size / columns); j++) {
				summ = 0;
				for (k=0; k < columns; k++) {
//					fprintf(fp, "%c%.*e", sep, nd, table->array[i].state->array[k+(j*columns)]);
					summ += table->array[i].state->array[k+(j*columns)];
				}
				fprintf(fp, "%c%.*e", sep, nd, summ);
			}
		}
	} else {
		for (i=0; i < table->size; i++) {
			for(j=0; j < (table->array[i].state->size / columns); j++) {
				summ = 0;
				for (k=0; k < columns; k++) {
//					fprintf(fp, "%c%*.*f", sep, ndigits+5, ndigits, table->array[i].state->array[k+(j*columns)]);
					summ += table->array[i].state->array[k+(j*columns)];
				}
				fprintf(fp, "%c%*.*f", sep, ndigits + 5, ndigits, summ);
			}
		}
	}
	fprintf(fp,"\n");
	fflush(fp);
}

/*** ConvertAnswer: little function that gets rid of bias times, division **
 *                  times and such and only returns the times in the tab-  *
 *                  times struct as its output; this is used to produce    *
 *                  unfold output that contains only the requested times;  *
 *                  it also makes sure that we return the right solution   *
 *                  at cell division, i.e. the solution right *after* the  *
 *                  cells have divided                                     *
 ***************************************************************************/

NArrPtr *ConvertAnswer(NArrPtr *answer, DArrPtr *tabtimes)
{
	int       i;                                  /* loop counter for answer */
	int       outindex = 0;                     /* loop counter for tabtimes */
	double    nexttime;                 /* used below to avoid memory errors */
	NArrPtr *outtab;                               /* answer to be returned */
/* allocate the outtab array */
	outtab = (NArrPtr*)malloc(sizeof(NArrPtr));
	outtab->size = tabtimes->size;
	outtab->array = (NucState *)calloc(outtab->size, sizeof(NucState));
/* the following loop goes through all answer elements and copies only     */
/* those which correspond to a tabtime into the outtab struct              */
	for ( i = 0; ( i < answer->size ) && ( outindex < tabtimes->size); i++ ) {
/* this kludge makes sure that we don't bomb below when there is no next   *
 * time anymore                                                            */
		if ( i == ( answer->size-1) )
			nexttime = 999999999;
		else
			nexttime = answer->array[i+1].time;
/* this if makes sure that we only return the requested times and that     *
 * we return the solution *after* the cells have divided for times that    *
 * correspond exactly to the cell division time (i.e. the end of mitosis); *
 * note that times before and after cell divisions are separated by EPIS-  *
 * LON and that BIG_EPSILON is always (much) bigger than EPSILON (but      *
 * still small enough for all practical purposes...)                       */
		if ( (fabs(answer->array[i].time - tabtimes->array[outindex]) < BIG_EPSILON) && (fabs(answer->array[i].time - nexttime) > BIG_EPSILON) ) {
			outtab->array[outindex].time = answer->array[i].time;
/*			outtab->array[outindex].state = (DArrPtr*)malloc(sizeof(DArrPtr));
			outtab->array[outindex].state->size = answer->array[i].state->size;
			outtab->array[outindex].state->array = answer->array[i].state->array;
*/			outtab->array[outindex].state = answer->array[i].state;
			outindex++;
		}
	}
	return outtab;
}

/*** THE FOLLOWING FUNCTIONS ARE FOR HANDLING TLIST, a linked list used to *
 *   initialize the structure that tells the solver for which time there's *
 *   data, how many nuclei there are and what to do.                       *
 ***************************************************************************/

/*** InitTList: initializes TList and adds first (t=0) and last ************
 *              (t=gastrulation) element of Tlist.                         *
 ***************************************************************************/

TList *InitTList(void)
{
	TList *first, *last;                  /* first and last element of TList */
/* initialize the first element (t=0) */
	first = (TList *)malloc(sizeof(TList));
	first->time = 0. + EPSILON;
	first->n = defs->ngenes * GetNNucs(0);
	first->op = PROPAGATE;
/* initialize the last element (t=gast_time) */
	last = (TList *)malloc(sizeof(TList));
	if ( !(last->time = GetGastTime()) )
		error("InitTList: error getting gastrulation time");
	last->n = defs->ngenes * GetNNucs(last->time);
	last->op = NO_OP;
/* link the two elements and return the list */
	first->next = last;
	last->next = NULL;
	return first;
}

/*** InsertTList: takes pointer to first element of TList plus time and ****
 *                desired op for new TList element and inserts a new TList *
 *                element for time at the appropriate place within the     *
 *                linked list. This function returns a pointer to the      *
 *                first element of the TList if insertion worked out fine. *
 ***************************************************************************/

TList *InsertTList(TList *first, double time, int op)
{
	TList *current;                            /* used to step through TList */
	TList *newt;                   /* used to allocate memory for new element */
	int n;                                      /* how many nuclei at 'time' */
	unsigned int ccycle1, ccycle2;       /* for PDEs: cleavage cycle numbers */
	n = defs->ngenes * GetNNucs(time);     /* get number of nuclei for 'time' */
	if (first == NULL)
		error("InsertTList: TList pointer is NULL at time %f!", time);
/* the following loop steps through the linked list and places the new     *
 * element at the appropriate position depending on its time               */
	current = first;
	ccycle1 = GetCCycle(time); /* for PDEs: checking to which cycle new time belongs */
	do {
		ccycle2 = GetCCycle(current->time); /* for PDEs: checking to which cycle existing time belongs */
		if( fabs(time - current->time) < BIG_EPSILON ) {
			if ( ccycle1 == ccycle2 ) {  /* if new time really close to an exist- */
				if ( current->op == MITOTATE) {
/* if we are skipping epsilon after MITOSIS start */
					newt = (TList *)malloc(sizeof(TList));
					newt->time = time;  /* -> allocate new element for right after */
					newt->n = n;               /* cell division has occurred */
					newt->op = op;
					newt->next = current->next;
					current->next = newt;
					return first;
		 		}
				current->op |= op; /* ing time point and no cell division happened */
				return first;                /* -> just add new op to existing one */
			} else if ( ccycle1 != ccycle2 ) {                /* if, on the other hand */
				newt = (TList *)malloc(sizeof(TList));     /* cell div HAS happened */
				newt->time = time;       /* -> allocate new element for right after */
				newt->n = n;                          /* cell division has occurred */
				newt->op = op;
				newt->next = current->next;
				current->next = newt;
				return first;
			} else         /* a sudden reduction of nuclei will be hard to explain */
				error("InsertTList: sudden reduction of nuclei at time %g!",current->time);
			} else if ( time > current->time ) {     /* new time > than current time */
				if ( fabs(time - current->next->time) < BIG_EPSILON ) {     /* is it */
					current = current->next;   /* really close to the next time point? */
				} else if ( time > current->next->time ) {         /* or if time is >> */
					current = current->next;               /* than the next time point */
				} else if ( time < current->next->time ) {         /* but if time is < */
					newt = (TList *)malloc(sizeof(TList));       /* the next time point */
					newt->time = time;                       /* -> allocate new element */
					newt->n = n;
					newt->op = op;
					newt->next = current->next;
					current->next = newt;
					return first;
				} else {      /* if all the above don't apply, there's something wrong */
					error("InsertTList: impossible error at time %g!", current->time);
				}
			} else {          /* if time < current time, there's something wrong too */
				error("InsterTList: we missed our exit at time %g!", current->time);
			}
	} while (current != NULL);
	return current;
}

/*** CountEntries: counts how many entries we have in a TList **************
 ***************************************************************************/

int CountEntries(TList *first)
{
	int n = 0;
	while(first != NULL) {
		n++;
		first = first->next;
	}
	return n;
}

/*** FreeTList: frees the memory of a TList ********************************
 ***************************************************************************/

void FreeTList(TList *first)
{
	if(first->next != NULL)
		FreeTList(first->next);
	free(first);
}

/*added by MacKoel*/

void Blastoderm_SolvePsi(Blastoderm*Genotype, double stephint, double accuracy, FILE *slog)
{
	DataRecord* jump;
	DataPoint point;
	int              i,ii,j;                                /* loop counters */
	int              k;                    /* index of gene k in current nuc */
	int              ap;                         /* nuc. position on AP axis */
	int              lin;             /* first lineage number at each ccycle */
	int              rule;                         /* MITOSIS or INTERPHASE? */
	int interval;
	int ri;
	double time;
	unsigned int ccycle;                   /* what cleavage cycle are we in? */
	int num_nucs;
/* for each genotype, the 'genotype' variable has to be made static to zy- *
 * gotic.c so that the derivative functions know which genotype they're    *
 * dealing with (i.e. they need to get the appropriate bcd gradient)       */
	InitGenotypeIndex(Genotype->genotype_ID);
/*This is feedback from score*/
	ri = Genotype->Solution->size - 1;
	time = Genotype->Solution->array[ri].time;
	if ( Genotype->havedata[ri] == 1 ) {
		jump = JumperPsi(Genotype->genotype_ID, time);
		for (j = 0; j < jump->size; j++) {
			point = jump->array[j];
			Genotype->Solution->array[ri].state->array[point.index] = 2*point.difference;
		}
	} else {
		for(j=0; j < Genotype->Solution->array[ri].state->size; j++) {
			Genotype->Solution->array[ri].state->array[j] = 0.0;
		}
	}
/* RUNNING THE MODEL *******************************************************/
/* Before running the model, mutate zygotic params appropriately
	Mutate(Name2Gtype(Genotype->genotype));*/
	if ( debug ) {
		fprintf(slog, "\n--------------------------------------------------");
		fprintf(slog, "--------------------------------------------------\n");
		fprintf(slog, "BlastodermPsi: mutated genotype to %s.\n", Genotype->genotype_ID);
	}
/* Below is the loop that evaluates the solution and saves it in the solu- *
 * tion struct                                                             */
	interval = 0;
	for ( i = 1; i < Genotype->Solution->size; i++) {
/*we are going backward for psi*/
		ri = Genotype->Solution->size - 1 - i;
/* First we have to set the propagation rule in zygotic.c (either MITOSIS  *
 * or INTERPHASE) to make sure that the correct differential equations are *
 * propagated (rules are defined in zygotic.h)                             */
		rule = GetRule(Genotype->Solution->array[ri].time);
		SetRule(rule);
		ccycle = GetCCycle(Genotype->Solution->array[ri].time);
		SetCCycle(ccycle);
		num_nucs = GetNNucs(Genotype->Solution->array[ri].time);
		SetNNucs(num_nucs);
		if ( debug ) {
			if ( rule == 0 )
				fprintf(slog, "BlastodermPsi: rule is INTERPHASE.\n");
			else if ( rule == 1 && !(Genotype->what2do[ri] & DIVIDE) )
				fprintf(slog, "BlastodermPsi: rule is MITOSIS.\n");
			else if ( Genotype->what2do[ri] & DIVIDE )
				fprintf(slog, "BlastodermPsi: rule is DIVISION.\n");
			fprintf(slog, "BlastodermPsi: havedata is %d.\n",Genotype->havedata[ri]);
		}
/* ADD_BIAS is a special op in that it can be combined with any other op   *
 * (see also next comment); we can add (or subtract) protein conentrations *
 * at any time; this is for adding initial conditions for genes that are   *
 * both maternal and zygotic (e.g. cad and hb) or to simulated perturba-   *
 * tions like heat shocks or induced overexpression of certain genes;      *
 * note that the bias lives in maternal.c and has to be fetched from there */
		if (Genotype->what2do[ri] & ADD_BIAS) {
			if ( debug )
				fprintf(slog, "BlastodermPsi: added bias at time %f.\n", Genotype->Solution->array[ri].time);
		}
/* The ops below can be executed in addition to ADD_BIAS but they cannot   *
 * be combined between themselves; if more than one op is set, the prio-   *
 * rities are as follows: NO_OP > DIVIDE > PROPAGATE; please make sure     *
 * that you do not set double ops, which will only obfuscate the code;     *
 *                                                                         *
 * NO_OP simply does nothing (used for gastrulation time)                  */
		if (Genotype->what2do[ri] & NO_OP)
			;
/* This is the DIVIDE rule: only symmetrical division is supported yet,    *
 * i.e. both daughter nuclei inherit all concentrations from their mother; *
 * we have to take special care of the fact that we might not need the     *
 * most anterior and most posterior daughter nuclei, depending on the ran- *
 * ge of nuclei in cycle 14 we've chosen to anneal on; e.g. if the most    *
 * anterior nucleus in cycle 14 has an odd lineage number, we need to push *
 * its sibling off the anterior limit when we divide after cycle 13; or in *
 * other words: our most anterior cell in cycle 14 is the posterior daugh- *
 * ter of our most anterior cell in cycle 13; therefore, we need to 'loose'*
 * its anterior sibling; the same applies for the posterior end, where we  *
 * want to loose the most posterior daughter cell if we don't need it any  *
 * more at the later cycle                                                 *
 *                                                                         *
 * This is implemented as follows below: lin is the lineage number of the  *
 * most anterior cell of the next cell cycle; if it's odd numbered, we'll  *
 * push off the most anterior daughter by subtracting the number of genes  *
 * from the daughter indices (i.e. we shift the solution array for the     *
 * next cycle posteriorly by one nucleus); if on the other hand, the most  *
 * posterior nucleus lies outside our new array, we just forget about it   */
		else if ( Genotype->what2do[ri] & DIVIDE ) {
			if(model == HYBRID || model == HEDIRECT || model == HESRR || model == HEURR) {
				lin = GetStartLin(Genotype->Solution->array[ri+1].time);
				for (j=0; j < Genotype->Solution->array[ri].state->size; j++) {
					k  = j % defs->ngenes;     /* k: index of gene k in current nucleus */
					ap = j / defs->ngenes;      /* ap: rel. nucleus position on AP axis */
/* evaluate ii: index of anterior daughter nucleus */
					if ( lin % 2 )
						ii = 2 * ap * defs->ngenes + k - defs->ngenes;
					else
						ii = 2 * ap * defs->ngenes + k;
/* skip the first most anterior daughter nucleus in case lin is odd */
					if ( ii >= 0 )
						Genotype->Solution->array[ri].state->array[j] = 0.5 * Genotype->Solution->array[ri+1].state->array[ii];
/* the second daughter only exists if it is still within the region */
					if ( ii + defs->ngenes < Genotype->Solution->array[ri+1].state->size )
						Genotype->Solution->array[ri].state->array[j] += 0.5 * Genotype->Solution->array[ri+1].state->array[ii+defs->ngenes];
				}
			} else {
				for ( j = 0; j < Genotype->Solution->array[ri].state->size; j++) {
					Genotype->Solution->array[ri].state->array[j] = Genotype->Solution->array[ri+1].state->array[j];
				}
			}
			if ( debug )
				fprintf(slog, "BlastodermPsi: nuclear division %d -> %d nuclei.\n", Genotype->Solution->array[ri].state->size / defs->ngenes, Genotype->Solution->array[ri+1].state->size / defs->ngenes);
		} else if ( Genotype->what2do[ri] & MITOTATE ) {
/*		printf("aAAHHH! in MITOTATE, t=%.16f, t+epilon=%.16f\n",
						solution.array[i].time,
								solution.array[i+1].time );	*/
			for ( j = 0; j < Genotype->Solution->array[ri].state->size; j++) {
				Genotype->Solution->array[ri].state->array[j] = Genotype->Solution->array[ri+1].state->array[j];
			}
		}
/* In case we have to PROPAGATE the differential equeations, we call the   *
 * solver; you have to make sure that the appropriate rule has been set in *
 * zygotic.c (use SetRule(), rules are MITOSIS or INTERPHASE), otherwise   *
 * you will propagate the wrong equations; the solver needs pointers to    *
 * arrays of concentration for start and end time as well as those start   *
 * and end times themselves; all solvers need a suggested stepsize, adap-  *
 * tive stepsize solvers need the accuracy argument; we also need the accu-*
 * racy argument for global stepsize control (if ever implemented); lastly *
 * we need to tell the solver how big input and output arrays are          */
		else if ( Genotype->what2do[ri] & PROPAGATE ) {
			SetInterval(Genotype->Concentrations->nintervals-1-interval);
			if (Genotype->Lagrangians->intervals[Genotype->Concentrations->nintervals-1-interval])
				ODESolutionPoly_delete(Genotype->Lagrangians->intervals[Genotype->Concentrations->nintervals-1-interval]);
/*for ( j = 0; j < Genotype->Solution->array[ri+1].state->size; j++) {
	fprintf(stdout,"psi t = %f i = %d v = %f\n", Genotype->Solution->array[ri+1].time, j, Genotype->Solution->array[ri+1].state->array[j]);
}*/
			Genotype->Lagrangians->intervals[Genotype->Concentrations->nintervals-1-interval] = ODESolve_Solve(Genotype->Lagrangians,Genotype->Solution->array[ri+1].state->array, Genotype->Solution->array[ri].state->array, Genotype->Solution->array[ri+1].time, Genotype->Solution->array[ri].time, stephint, accuracy, Genotype->Solution->array[ri].state->size, slog);
			interval++;
			if(Genotype->havedata[ri]==1) {
				time = Genotype->Solution->array[ri].time;
				jump = JumperPsi(Genotype->genotype_ID, time);
				for (j = 0; j < jump->size; j++) {
					point = jump->array[j];
					Genotype->Solution->array[ri].state->array[point.index] += 2*point.difference;
				}
			}
		}
/* unknown op? -> error! */
		else
			error("op was %d!?", Genotype->what2do[ri]);
	}
	if ( debug ) {
		PrintBlastoderm(slog, Genotype->Solution, "psi_debug_output", MAX_PRECISION, defs->ngenes);
	}
}

void BlastodermGrad(Blastoderm*Genotype, double **integ, int NumPar, double stephint, double accuracy, FILE *slog)
{
	int              i, j;                                /* loop counters */
	double time;
	int              rule;                         /* MITOSIS or INTERPHASE? */
	int interval;
	unsigned int ccycle;                   /* what cleavage cycle are we in? */
	int num_nucs;
	interval = 0;
	for(i=0; i<Genotype->Solution->size; i++) {
		if (Genotype->what2do[i] & ADD_BIAS)
			;
		if (Genotype->what2do[i] & NO_OP)
			;
		else if ( Genotype->what2do[i] & DIVIDE )
			;
		 else if ( Genotype->what2do[i] & MITOTATE )
			;
		else if ( Genotype->what2do[i] & PROPAGATE ) {
			time = Genotype->Concentrations->intervals[interval]->time_interval->time_start;
			rule = GetRule(time);
			SetRule(rule);
			ccycle = GetCCycle(time);
			SetCCycle(ccycle);
			num_nucs = GetNNucs(time);
			SetNNucs(num_nucs);
			SetInterval(interval);
/*for ( j = 0; j < NumPar; j++) {
	fprintf(stdout,"grad t = %f i = %d v = %f\n", time, j, *(integ[j]));
}*/
			V_IntSimpson(NumPar, integ, Genotype->Concentrations->intervals[interval]->time_interval, DLdqOrig, stephint, accuracy);
			interval++;
		}
/* unknown op? -> error! */
		else
			error("op was %d!?", Genotype->what2do[i]);
	}
}

NArrPtr *calcgthing(Blastoderm*Genotype, int witch)
{
	int           m;
	int           i;
	int           l_rule;
	unsigned int l_ccycle = 0;
	int interval;
	NArrPtr       *gutsy;
	gutsy = (NArrPtr*)malloc(sizeof(NArrPtr));
	gutsy->size  = Genotype->Solution->size;
	gutsy->array = (NucState *)calloc(Genotype->Solution->size, sizeof(NucState));
	InitGenotypeIndex(Genotype->genotype_ID);
	interval = 0;
	for (i=0; i < Genotype->Solution->size; i++) {
		gutsy->array[i].time = Genotype->Solution->array[i].time;
/* calculate size of 2D array that holds guts and allocate memory */
		m = Genotype->Solution->array[i].state->size / defs->ngenes;
		gutsy->array[i].state = (DArrPtr*)malloc(sizeof(DArrPtr));
		gutsy->array[i].state->size  = Genotype->Solution->array[i].state->size;
		gutsy->array[i].state->array = (double *)calloc(gutsy->array[i].state->size, sizeof(double));
/* set whether it is MITOSIS or INTERPHASE when calculating guts */
		if (Genotype->what2do[i] & ADD_BIAS)
			;
		if (Genotype->what2do[i] & NO_OP)
			;
		else if ( Genotype->what2do[i] & DIVIDE )
			;
		 else if ( Genotype->what2do[i] & MITOTATE )
			;
		else if ( Genotype->what2do[i] & PROPAGATE ) {
			l_rule = GetRule(gutsy->array[i].time);
			SetRule(l_rule);
			l_ccycle = GetCCycle(gutsy->array[i].time);
			SetCCycle(l_ccycle);
			SetNNucs(m);
			SetInterval(interval);
			DLdqOrig_g( gutsy->array[i].time, gutsy->array[i].state->array, witch);
			interval++;
		}
	}
	return gutsy;
}

void V_Tconc(double t,double*conc)
{
	ODESolutionPoly_evaluateAtTime(thisGenotype->Concentrations->intervals[INTERVAL], conc, t);
}

void V_Pconc(double t,double*conc)
{
	ODESolutionPoly_evaluateAtTime(thisGenotype->Lagrangians->intervals[INTERVAL], conc, t);
}

void SetInterval(int arg)
{
	INTERVAL=arg;
}

void Blastoderm_SolveJump(Blastoderm*Genotype, double stephint, double accuracy, FILE *slog)
{
	DataRecord* jump;
	DataPoint point;
	int              i,ii,j;                                /* loop counters */
	int              k;                    /* index of gene k in current nuc */
	int              ap;                         /* nuc. position on AP axis */
	int              lin;             /* first lineage number at each ccycle */
	int              rule;                         /* MITOSIS or INTERPHASE? */
	int interval;
	int ri;
	double time;
	unsigned int ccycle;                   /* what cleavage cycle are we in? */
/* for each genotype, the 'genotype' variable has to be made static to zy- *
 * gotic.c so that the derivative functions know which genotype they're    *
 * dealing with (i.e. they need to get the appropriate bcd gradient)       */
	InitGenotypeIndex(Genotype->genotype_ID);
/*This is feedback from score*/
	ri = Genotype->Solution->size - 1;
	time = Genotype->Solution->array[ri].time;
	if ( Genotype->havedata[ri] == 1 ) {
		jump = JumperPsi(Genotype->genotype_ID, time);
		for (j = 0; j < jump->size; j++) {
			point = jump->array[j];
			Genotype->Solution->array[ri].state->array[point.index] = 2*point.difference;
		}
	} else {
		for( j = 0; j < Genotype->Solution->array[ri].state->size; j++) {
			Genotype->Solution->array[ri].state->array[j] = 0.0;
		}
	}
/* RUNNING THE MODEL *******************************************************/
	if ( debug ) {
		fprintf(slog, "\n--------------------------------------------------");
		fprintf(slog, "--------------------------------------------------\n");
		fprintf(slog, "BlastodermPsi: mutated genotype to %s.\n", Genotype->genotype_ID);
	}
/* Below is the loop that evaluates the solution and saves it in the solu- *
 * tion struct                                                             */
	interval = 0;
	for( i = 1; i < Genotype->Solution->size; i++) {
/*we are going backward for psi*/
		ri = Genotype->Solution->size - 1 - i;
/* First we have to set the propagation rule in zygotic.c (either MITOSIS  *
 * or INTERPHASE) to make sure that the correct differential equations are *
 * propagated (rules are defined in zygotic.h)                             */
		rule = GetRule(Genotype->Solution->array[ri].time);
		SetRule(rule);
		ccycle = GetCCycle(Genotype->Solution->array[ri].time);
		SetCCycle(ccycle);
		if ( debug ) {
			if ( rule == 0 )
				fprintf(slog, "BlastodermPsi: rule is INTERPHASE.\n");
			else if ( rule == 1 && !(Genotype->what2do[ri] & DIVIDE) )
				fprintf(slog, "BlastodermPsi: rule is MITOSIS.\n");
			else if ( Genotype->what2do[ri] & DIVIDE )
				fprintf(slog, "BlastodermPsi: rule is DIVISION.\n");
			fprintf(slog, "BlastodermPsi: havedata is %d.\n",Genotype->havedata[ri]);
		}
/* ADD_BIAS is a special op in that it can be combined with any other op   *
 * (see also next comment); we can add (or subtract) protein conentrations *
 * at any time; this is for adding initial conditions for genes that are   *
 * both maternal and zygotic (e.g. cad and hb) or to simulated perturba-   *
 * tions like heat shocks or induced overexpression of certain genes;      *
 * note that the bias lives in maternal.c and has to be fetched from there */
		if (Genotype->what2do[ri] & ADD_BIAS) {
			if ( debug )
				fprintf(slog, "BlastodermPsi: added bias at time %f.\n",
			Genotype->Solution->array[ri].time);
		}
/* The ops below can be executed in addition to ADD_BIAS but they cannot   *
 * be combined between themselves; if more than one op is set, the prio-   *
 * rities are as follows: NO_OP > DIVIDE > PROPAGATE; please make sure     *
 * that you do not set double ops, which will only obfuscate the code;     *
 *                                                                         *
 * NO_OP simply does nothing (used for gastrulation time)                  */
		if (Genotype->what2do[ri] & NO_OP)
			;
/* This is the DIVIDE rule: only symmetrical division is supported yet,    *
 * i.e. both daughter nuclei inherit all concentrations from their mother; *
 * we have to take special care of the fact that we might not need the     *
 * most anterior and most posterior daughter nuclei, depending on the ran- *
 * ge of nuclei in cycle 14 we've chosen to anneal on; e.g. if the most    *
 * anterior nucleus in cycle 14 has an odd lineage number, we need to push *
 * its sibling off the anterior limit when we divide after cycle 13; or in *
 * other words: our most anterior cell in cycle 14 is the posterior daugh- *
 * ter of our most anterior cell in cycle 13; therefore, we need to 'loose'*
 * its anterior sibling; the same applies for the posterior end, where we  *
 * want to loose the most posterior daughter cell if we don't need it any  *
 * more at the later cycle                                                 *
 *                                                                         *
 * This is implemented as follows below: lin is the lineage number of the  *
 * most anterior cell of the next cell cycle; if it's odd numbered, we'll  *
 * push off the most anterior daughter by subtracting the number of genes  *
 * from the daughter indices (i.e. we shift the solution array for the     *
 * next cycle posteriorly by one nucleus); if on the other hand, the most  *
 * posterior nucleus lies outside our new array, we just forget about it   */
		else if ( Genotype->what2do[ri] & DIVIDE ) {
			if(model == HYBRID || model == HEDIRECT || model == HESRR || model == HEURR) {
				lin = GetStartLin(Genotype->Solution->array[ri+1].time);
				for (j=0; j < Genotype->Solution->array[ri].state->size; j++) {
					k  = j % defs->ngenes;     /* k: index of gene k in current nucleus */
					ap = j / defs->ngenes;      /* ap: rel. nucleus position on AP axis */
/* evaluate ii: index of anterior daughter nucleus */
					if ( lin % 2 )
						ii = 2 * ap * defs->ngenes + k - defs->ngenes;
					else
						ii = 2 * ap * defs->ngenes + k;
/* skip the first most anterior daughter nucleus in case lin is odd */
					if ( ii >= 0 )
						Genotype->Solution->array[ri].state->array[j] = 0.5 * Genotype->Solution->array[ri+1].state->array[ii];
/* the second daughter only exists if it is still within the region */
					if ( ii + defs->ngenes < Genotype->Solution->array[ri+1].state->size )
						Genotype->Solution->array[ri].state->array[j] += 0.5 * Genotype->Solution->array[ri+1].state->array[ii+defs->ngenes];
				}
			} else {
				for (j=0; j < Genotype->Solution->array[ri].state->size; j++) {
					Genotype->Solution->array[ri].state->array[j] = Genotype->Solution->array[ri+1].state->array[j];
				}
			}
			if ( debug )
				fprintf(slog, "BlastodermPsi: nuclear division %d -> %d nuclei.\n", Genotype->Solution->array[ri].state->size / defs->ngenes, Genotype->Solution->array[ri+1].state->size / defs->ngenes);
		}
/* In case we have to PROPAGATE the differential equeations, we call the   *
 * solver; you have to make sure that the appropriate rule has been set in *
 * zygotic.c (use SetRule(), rules are MITOSIS or INTERPHASE), otherwise   *
 * you will propagate the wrong equations; the solver needs pointers to    *
 * arrays of concentration for start and end time as well as those start   *
 * and end times themselves; all solvers need a suggested stepsize, adap-  *
 * tive stepsize solvers need the accuracy argument; we also need the accu-*
 * racy argument for global stepsize control (if ever implemented); lastly *
 * we need to tell the solver how big input and output arrays are          */
		else if ( Genotype->what2do[ri] & PROPAGATE ) {
			SetInterval(Genotype->Concentrations->nintervals-1-interval);
			if (Genotype->Lagrangians->intervals[Genotype->Concentrations->nintervals-1-interval])
				ODESolutionPoly_delete(Genotype->Lagrangians->intervals[Genotype->Concentrations->nintervals-1-interval]);
			Genotype->Lagrangians->intervals[Genotype->Concentrations->nintervals-1-interval] = ODESolve_Solve(Genotype->Lagrangians,Genotype->Solution->array[ri+1].state->array, Genotype->Solution->array[ri].state->array, Genotype->Solution->array[ri+1].time, Genotype->Solution->array[ri].time, stephint, accuracy, Genotype->Solution->array[ri].state->size, slog);
			interval++;
			if( Genotype->havedata[ri] == 1) {
				jump = JumperPsi(Genotype->genotype_ID, time);
				for (j = 0; j < jump->size; j++) {
					point = jump->array[j];
					Genotype->Solution->array[ri].state->array[point.index] = 2*point.difference;
				}
			}
		}
/* unknown op? -> error! */
		else
			error("op was %d!?", Genotype->what2do[ri]);
	}
	if ( debug ) {
		PrintBlastoderm(slog, Genotype->Solution, "psi_debug_output", MAX_PRECISION, defs->ngenes);
	}
}

void Blastoderm_delete(Blastoderm*p)
{
	int i;
	FreeTList(p->entries);
	for(i=0;i<p->Concentrations->nintervals ;i++) {
		if (p->Concentrations->intervals[i]) ODESolutionPoly_delete(p->Concentrations->intervals[i]);
		if (p->Lagrangians->intervals[i]) ODESolutionPoly_delete(p->Lagrangians->intervals[i]);
	}
	free(p->genotype_ID);
	if ( defs->ndivs > 0 ) {
		free(p->transitions);
	}
	free(p->what2do);
	free(p->havedata);
	FreeNArrPtr( p->Solution );
	FreeNArrPtr( p->Solution2 );
	free(p);
}

NArrPtr *gcdm_clone_answer(NArrPtr *answer, DArrPtr *tabtimes)
{
	int       i, j;
	int       outindex = 0;
	double    nexttime;
	NArrPtr *outtab;
	outtab = (NArrPtr*)malloc(sizeof(NArrPtr));
	outtab->size = tabtimes->size;
	outtab->array = (NucState *)calloc(outtab->size, sizeof(NucState));
/* the following loop goes through all answer elements and copies only     */
/* those which correspond to a tabtime into the outtab struct              */
	for ( i = 0; ( i < answer->size ) && ( outindex < tabtimes->size); i++ ) {
/* this kludge makes sure that we don't bomb below when there is no next   *
 * time anymore                                                            */
		if ( i == ( answer->size-1) )
			nexttime = 999999999;
		else
			nexttime = answer->array[i+1].time;
/* this if makes sure that we only return the requested times and that     *
 * we return the solution *after* the cells have divided for times that    *
 * correspond exactly to the cell division time (i.e. the end of mitosis); *
 * note that times before and after cell divisions are separated by EPIS-  *
 * LON and that BIG_EPSILON is always (much) bigger than EPSILON (but      *
 * still small enough for all practical purposes...)                       */
		if ( (fabs(answer->array[i].time - tabtimes->array[outindex]) < BIG_EPSILON) && (fabs(answer->array[i].time - nexttime) > BIG_EPSILON) ) {
			outtab->array[outindex].time = answer->array[i].time;
			outtab->array[outindex].state = (DArrPtr*)malloc(sizeof(DArrPtr));
			outtab->array[outindex].state->size = answer->array[i].state->size;
			outtab->array[outindex].state->array = (double*)calloc(outtab->array[outindex].state->size, sizeof(double));
			for ( j = 0; j < outtab->array[outindex].state->size; j++ ) {
				outtab->array[outindex].state->array[j] = answer->array[i].state->array[j];
			}
			outindex++;
		}
	}
	return outtab;
}

NArrPtr *gcdm_combine_answer(double alpha_g, NArrPtr *gsol, double alpha_a, NArrPtr *answer, DArrPtr *tabtimes)
{
	int       i, j;
	int       outindex = 0;
	double    nexttime;
	NArrPtr *outtab;
	outtab = (NArrPtr*)malloc(sizeof(NArrPtr));
	outtab->size = tabtimes->size;
	outtab->array = (NucState *)calloc(outtab->size, sizeof(NucState));
/* the following loop goes through all answer elements and copies only     */
/* those which correspond to a tabtime into the outtab struct              */
	for ( i = 0; ( i < answer->size ) && ( outindex < tabtimes->size); i++ ) {
/* this kludge makes sure that we don't bomb below when there is no next   *
 * time anymore                                                            */
		if ( i == ( answer->size-1) )
			nexttime = 999999999;
		else
			nexttime = answer->array[i+1].time;
/* this if makes sure that we only return the requested times and that     *
 * we return the solution *after* the cells have divided for times that    *
 * correspond exactly to the cell division time (i.e. the end of mitosis); *
 * note that times before and after cell divisions are separated by EPIS-  *
 * LON and that BIG_EPSILON is always (much) bigger than EPSILON (but      *
 * still small enough for all practical purposes...)                       */
		if ( (fabs(answer->array[i].time - tabtimes->array[outindex]) < BIG_EPSILON) && (fabs(answer->array[i].time - nexttime) > BIG_EPSILON) ) {
			outtab->array[outindex].time = answer->array[i].time;
			outtab->array[outindex].state = (DArrPtr*)malloc(sizeof(DArrPtr));
			outtab->array[outindex].state->size = answer->array[i].state->size;
			outtab->array[outindex].state->array = (double*)calloc(outtab->array[outindex].state->size, sizeof(double));
			for ( j = 0; j < outtab->array[outindex].state->size; j++ ) {
				outtab->array[outindex].state->array[j] = alpha_g * gsol->array[outindex].state->array[j] + alpha_a * answer->array[i].state->array[j];
			}
			outindex++;
		}
	}
	return outtab;
}

#ifdef LIBSVM
	struct svm_node *ConvertAnswerToLibSVM(NArrPtr *answer, NArrPtr *answer_mut, int *max_nr_attr)
	{
		struct svm_node *x = NULL;
		(*max_nr_attr) = 8 * defs->ngenes * defs->nnucs + 8 * (defs->ngenes - 1) * defs->nnucs + 1;
		x = (struct svm_node *) realloc(x, (*max_nr_attr) * sizeof(struct svm_node));
		int                i, j, k;                       /* local loop counters */
		int                l;
		int columns = 4;
		l = 0;
		for (k = 0; k < columns; k++) {
			for (i = 1; i < answer->size; i++) {
				for (j = 0; j < defs->nnucs; j++) {
					x[l].index = l + 1;
					x[l].value = answer->array[i].state->array[k + (j * defs->ngenes)];
				}
			}
			l++;
		}
		for (k = 0; k < columns; k++) {
			if (k == 1) continue;
			for (i = 1; i < answer->size; i++) {
				for (j = 0; j < defs->nnucs; j++) {
					x[l].index = l + 1;
					x[l].value = answer->array[i].state->array[k + (j * defs->ngenes)];
				}
			}
			l++;
		}
		x[(*max_nr_attr) - 1].index = -1;
		return(x);
	}
#elif FANN
	fann_type *ConvertAnswerToFANN(NArrPtr *answer, NArrPtr *answer_mut)
	{
		fann_type *x = NULL;
		int                i, j, k;                       /* local loop counters */
		int                l;
		int columns = 4;
		int max_nr_attr = 8 * columns * defs->nnucs + 8 * (columns - 1) * defs->nnucs;
		x = (fann_type *) realloc(x, max_nr_attr * sizeof(fann_type));
		l = 0;
		for (k = 0; k < columns; k++) {
			for (i = 1; i < answer->size; i++) {
				for (j = 0; j < defs->nnucs; j++) {
					x[l] = answer->array[i].state->array[k + (j * defs->ngenes)];
					l++;
				}
			}
		}
		for (k = 0; k < columns; k++) {
			if (k == 1) continue;
			for (i = 1; i < answer->size; i++) {
				for (j = 0; j < defs->nnucs; j++) {
					x[l] = answer_mut->array[i].state->array[k + (j * defs->ngenes)];
					l++;
				}
			}
		}
/*		fprintf(stdout, "max=%d; length=%d\n", max_nr_attr, l);*/
		return(x);
	}
#endif
