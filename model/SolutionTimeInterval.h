#ifndef SOLUTIONTIMEINTERVAL_INCLUDED
#define SOLUTIONTIMEINTERVAL_INCLUDED

#include <float.h>
#ifdef ICC
#include <mathimf.h>
#else
#include <math.h>
#endif
#include <stdlib.h>
#include <stdio.h>

typedef struct SolutionTimeInterval {
  double time_start;
  double time_finish;
} SolutionTimeInterval;

SolutionTimeInterval*SolutionTimeInterval_creatWith(double tin,double tout);
void SolutionTimeInterval_delete(SolutionTimeInterval*p);

#endif
