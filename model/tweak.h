/***************************************************************************
 *            tweak.h
 *
 *  Tue Oct 18 14:00:52 2005
 *  Copyright  2005  $USER
 *  kozlov@spbcas.ru
 ****************************************************************************/

/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef _TWEAK_H
#define _TWEAK_H

#include <stdio.h>
#include <stdlib.h>

#ifdef __cplusplus
extern "C"
{
#endif

/* Tweak struct is for tweaking individual parameters or not; each pointer *
 * below points to an array of ints which represent each paramter          *
 * 1 means tweak, 0 means leave it alone                                   *
 * see ../doc/dataformatX.X for further details on the tweak section       */

typedef struct Tweak {
	int       *Rtweak;                             /* which Rs to be tweaked */
	int       *Ttweak;                             /* which Ts to be tweaked */
	int       *Mtweak;                             /* which ms to be tweaked */
	int       *Etweak;                             /* which ms to be tweaked */
	int       *Ptweak;                             /* which ms to be tweaked */
	int       *htweak;                             /* which hs to be tweaked */
	int       *dtweak;                             /* which ds to be tweaked */
	int       *lambdatweak;                   /* which lambdas to be tweaked */
	int       *tautweak;                   /* which lambdas to be tweaked */
	int       *mtweak;                   /* which lambdas to be tweaked */
	int       *mmtweak;                   /* which lambdas to be tweaked */
} Tweak;

Tweak*ReadTweak(FILE *fp);
void TweakDelete(Tweak*p);
void PrintTweak(FILE *fp, Tweak *p, char *title);
void WriteTweak(char *name, Tweak *p, char *title);
int gcdm_tweak_get_ntweaks(Tweak*p);

#ifdef __cplusplus
}
#endif

#endif
