/***************************************************************************
 *            printgrad.c
 *
 *  Fri Apr 29 09:47:40 2011
 *  Copyright  2011  Konstantin Kozlov
 *  <kozlov@spbcas.ru>
 ****************************************************************************/

/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Library General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor Boston, MA 02110-1301,  USA
 */

#ifdef ICC
#include <mathimf.h>
#else
#include <math.h>
#endif
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <error.h>
#include <ODESolve.h>
#include <maternal.h>
#include <score.h>
#include <zygotic.h>
#include <integrate.h>
#include <tweak.h>


/*** Constants *************************************************************/

#define  OPTS      ":A:a:DE:f:F:g:G:hH:i:oO:p:r:s:S:T:vx:X:z:Z"  /* command line option string */


/*** Help, usage and version messages **************************************/

static const char usage[]  =

"Usage: printgrad [-a <accuracy>] [-D] [-f <float_prec>] [-g <g(u)>] [-G]\n"
"                 [-h] [-i <stepsize>] [-o] [-p] [-s <solver>] [-v]\n"
"                 [-x <sect_title>]\n"
"                 <datafile>\n";

static const char help[]   =

"Usage: gcdm7_printgrad [options] <datafile>\n\n"

"Arguments:\n"
"  <datafile>          data file for which we evaluate score and RMS\n\n"

"Options:\n"
"  -a <accuracy>       solver accuracy for adaptive stepsize ODE solvers\n"
"  -D                  debugging mode, prints all kinds of debugging info\n"
"  -E <index of input> switch off external input, can be given multiple times for several inputs\n"
"  -f <float_prec>     float precision of output is <float_prec>\n"
"  -F <index of gene>  switch off diffusion for this gene, can be given multiple times for several genes\n"
"  -g <g(u)>           chooses g(u): e = exp, h = hvs, s = sqrt, t = tanh\n"
"  -G                  gut mode: prints squared diffs for all datapoints\n"
"  -h                  prints this help message\n"
"  -i <stepsize>       sets ODE solver stepsize (in minutes)\n"
"  -o                  use oldstyle cell division times (3 div only)\n"
"  -p                  prints penalty in addition to score and RMS\n"
"  -s <solver>         selects solver\n"
"  -S <cost func>      selects quality functional: chisq (default), abs or deriv\n"
"  -v                  print version and compilation date\n"
"  -x <sect_title>     uses equation paramters from section <sect_title>\n\n"
"  -X <equations>      selects equations\n\n"
"  -z <gast_time>      set custom gastrulation time (max. 10'000 (min));\n"
"                      custom gastrulation MUST be later than the normal one\n"

"Please report bugs to <kozlov@spbcas.ru>. Thank you!\n";

static const char verstring[] =

"%s version %s\n"
"compiled by:      %s\n"
"         on:      %s\n"
"      using:      %s\n"
"      flags:      %s\n"
"       date:      %s at %s\n";

static int functional_flag = 0;
static int extra_file_flag = 0;

/*** Main program **********************************************************/

int main(int argc, char **argv)
{
	int           c;                   /* used to parse command line options */
	FILE          *infile;                     /* pointer to input data file */
	char          *dumpfile;               /* name of debugging model output */
	FILE          *dumpptr;          /* pointer for dumping raw model output */
	char          *slogfile;                      /* name of solver log file */
	FILE          *slog;                          /* solver log file pointer */
/* the follwoing few variables are read as arguments to command line opts  */
	int           ndigits     = 12;     /* precision of output, default = 12 */
	double        stepsize    = 1.;                   /* stepsize for solver */
	double        parderstep = 0.05;
	double        accuracy    = 0.001;                /* accuracy for solver */
	char          *section_title;                  /* parameter section name */
/* two format strings */
	char          *format;                           /* output format string */
	char          *precision;   /* precision string for output format string */
/* output values */
	double        chisq       = 0.; /* sum of squared differences for output */
	double time;
 /* the following lines define a pointers to:                               */
/*            - pd:    dvdt function, currently only DvdtOrig in zygotic.c */
/*            - pj:    Jacobian function, in zygotic.c                     */
/*                                                                         */
/* NOTE: ps (solver) is declared as global in integrate.h                  */

/*  void (*pd)(double *, double, double *, int, int);
  void (*pj)(double, double *, double *, double **, int);*/
/* we need only solver name */
	char *solvername, *solvername0, *solver;
	Derivative2 pd = Dvdt2Orig;
/* external declarations for command line option parsing (unistd.h) */
	extern char   *optarg;                   /* command line option argument */
	extern int    optind;              /* pointer to current element of argv */
	extern int    optopt;             /* contain option character upon error */
	int loop_kounter;
	short isCustom = 0;
/* following part sets default values for deriv, Jacobian and solver funcs */
	int rhs, zero_bias;
	double dosage;
	int i;
	int e_mutate = 0;
	int *e_mutate_genes = NULL;
	int d_mutate = 0;
	int *d_mutate_genes = NULL;
	int g_mode = 0;

	solvername = (char *)calloc(MAX_RECORD, sizeof(char));
	solvername = strcpy(solvername, "rkso");
	solvername0 = (char *)calloc(MAX_RECORD, sizeof(char));
	solvername0 = strcpy(solvername0, "bs");
	rhs = 1;
	dosage = 2.0;
	zero_bias = 0;
	solver = NULL;
	section_title = (char *)calloc(MAX_RECORD, sizeof(char));
	section_title = strcpy(section_title, "eqparms");  /* default is eqparms */
/* following part parses command line for options and their arguments      */
	optarg = NULL;
	integrate_set_zero_mrna(1);
	while ( (c = getopt(argc, argv, OPTS)) != -1 )
		switch (c) {
			case 'a':
				accuracy = atof(optarg);
				if ( accuracy <= 0 )
					error("printgrad: accuracy (%g) is too small", accuracy);
			break;
			case 'D':                                 /* -D runs in debugging mode */
				debug = 1;
			break;
			case 'E':
				e_mutate_genes = (int*)realloc(e_mutate_genes, ( e_mutate + 1 ) * sizeof(int));
				e_mutate_genes[e_mutate] = atoi(optarg);
				e_mutate++;
			break;
			case 'f':
				ndigits    = atoi(optarg);          /* -f determines float precision */
/*				if ( ndigits < 0 )
					error("printscore: what exactly would a negative precision be???");*/
				if ( ndigits > MAX_PRECISION )
					error("printscore: max. float precision is %d!", MAX_PRECISION);
			break;
			case 'F':
				d_mutate_genes = (int*)realloc(d_mutate_genes, ( d_mutate + 1 ) * sizeof(int));
				d_mutate_genes[d_mutate] = atoi(optarg);
				d_mutate++;
			break;
			case 'g':                                    /* -g choose g(u) function */
				if( !(strcmp(optarg, "s")) )
					gofu = Sqrt;
				else if ( !(strcmp(optarg, "t")))
					gofu = Tanh;
				else if ( !(strcmp(optarg, "e")))
					gofu = Exp;
				else if ( !(strcmp(optarg, "h")))
					gofu = Hvs;
                else if ( !(strcmp(optarg, "l")))
					gofu = Lin;
				else if ( !(strcmp(optarg, "n")))
					gofu = He;
				else
					error("printgrad: %s is an invalid g(u), should be e, h, s, t or l", optarg);
			break;
			case 'h':                                            /* -h help option */
				PrintMsg(help, 0);
			break;
			case 'H':                                 /* -D runs in debugging mode */
				g_mode = atoi(optarg);
			break;
			case 'i':                                      /* -i sets the stepsize */
				stepsize = atof(optarg);
				if ( stepsize < 0 )
					error("printscore: going backwards? (hint: check your -i)");
				if ( stepsize == 0 )
					error("printscore: going nowhere? (hint: check your -i)");
				if ( stepsize > MAX_STEPSIZE )
					error("printscore: stepsize %g too large (max. is %g)", stepsize, MAX_STEPSIZE);
			break;
			case 'o':             /* -o sets old division style (ndivs = 3 only! ) */
				olddivstyle = 1;
			break;
			case 'O':
				if (!strcmp(optarg, "hyb")) {
					model = HYBRID;
				} else if (!strcmp(optarg, "simple")) {
					model = SIMPLE;
				} else if (!strcmp(optarg, "nomit")) {
					model = NOMITOSIS;
				} else if (!strcmp(optarg, "double")) {
					model = DOUBLING;
				} else if (!strcmp(optarg, "hed")) {
					model = HEDIRECT;
				} else if (!strcmp(optarg, "hes")) {
					model = HESRR;
				} else if (!strcmp(optarg, "heu")) {
					model = HEURR;
				} else if (!strcmp(optarg, "heq")) {
					model = HEQUENCHING;
				} else {
					error("printgrad: invalid model (-O %s)", optarg);
				}
			break;
			case 'p':                                      /* -i sets the stepsize */
				parderstep = atof(optarg);
			break;
			case 's':
				solver = (char *)calloc(MAX_RECORD, sizeof(char));
				solver = strcpy(solver, optarg);
			break;
			case 'S':
				if (!strcmp(optarg, "deriv")) {
					gcdm_score_init_eval("deriv");
				} else if (!strcmp(optarg, "freq")) {
					gcdm_score_init_eval("freq");
				} else if (!strcmp(optarg, "freq2d")) {
					gcdm_score_init_eval("freq2d");
				} else if (!strcmp(optarg, "lsq")) {
					gcdm_score_init_eval("lsq");
				} else if (!strcmp(optarg, "vst")) {
					gcdm_score_init_eval("vst");
				} else if (!strcmp(optarg, "chisq")) {
					gcdm_score_init_eval("chisq");
				} else if (!strcmp(optarg, "abs")) {
					gcdm_score_init_eval("abs");
				} else if (!strcmp(optarg, "wpgp")) {
					gcdm_score_init_eval("wpgp");
				} else {
					error("printscore: invalid deriv (-S %s)", optarg);
				}
			break;
			case 'T':
				time = atof(optarg);
				set_need_time(time);
			break;
			case 'v':                                  /* -v prints version number */
				fprintf(stderr, verstring, *argv, VERS, USR, MACHINE, COMPILER, FLAGS, __DATE__, __TIME__);
				exit(0);
			case 'x':
				if ( (strcmp(optarg, "input")) && (strcmp(optarg, "eqparms")) && (strcmp(optarg, "tonameters")) && (strcmp(optarg, "toneqparms")) && (strcmp(optarg, "parameters")) )
					error("unfold: invalid section title (%s)", optarg);
				section_title = strcpy(section_title, optarg);
			break;
			case 'X':
				if (!strcmp(optarg, "orig2")) {
					rhs = 0;
					set_deriv(Dvdt2Orig);
				} else if (!strcmp(optarg, "orig3")) {
					rhs = 0;
					set_deriv(Dvdt3Orig);
				} else if (!strcmp(optarg, "orig31stDir")) {
					rhs = 0;
					set_deriv(Dvdt31stTelDirichlet);
            } else if (!strcmp(optarg, "orig31stNeu")) {
					rhs = 0;
					set_deriv(Dvdt31stTelNeumann);
				} else if (!strcmp(optarg, "orig32ndNeu")) {
					rhs = 0;
					set_deriv(Dvdt32ndTelNeumann);
				} else if (!strcmp(optarg, "orig3M")) {
					rhs = 0;
					set_deriv(Dvdt3TelMick);
            } else if (!strcmp(optarg, "orig31stJeffDir")) {
					rhs = 0;
					set_deriv(Dvdt31stJeffDirichlet);
            } else if (!strcmp(optarg, "orig31stJeffNeu")) {
					rhs = 0;
					set_deriv(Dvdt31stJeffNeumann);
				} else if (!strcmp(optarg, "bump")) {
					rhs = 0;
					set_deriv(Dvdt2Bump);
				} else if (!strcmp(optarg, "mob1")) {
					rhs = 0;
					set_deriv(Dvdt2Mob1);
				} else if (!strcmp(optarg, "orig")) {
					rhs = 1;
#ifdef XTRA
				} else if (!strcmp(optarg, "wmrna")) {
					rhs = 1;
					set_deriv0(DvwdtOrig);
				} else if (!strcmp(optarg, "dmrna")) {
					rhs = 1;
					set_deriv0(DdvwdtOrig);
					ODESolve_set_dde(gcdm_add_delayed);
				} else if (!strcmp(optarg, "ddmrna")) {
					rhs = 1;
					set_deriv0(DddvwdtOrig);
					ODESolve_set_dde(gcdm_add_delayed);
				} else if (!strcmp(optarg, "ddual")) {
					rhs = 1;
					set_deriv0(DddvwdtDual);
					ODESolve_set_dde(gcdm_add_delayed);
				} else if (!strcmp(optarg, "dbmrna")) {
					rhs = 1;
					set_deriv0(DddwwdtOrig);
					ODESolve_set_dde(gcdm_add_delayed);
#endif
				} else {
					error("printgrad: invalid deriv (-X %s)", optarg);
				}
			break;
			case 'z':
				isCustom = 1;
				custom_gast = atof(optarg);
				if ( custom_gast < 0. )
					error("printgrad: gastrulation time must be positive");
				if ( custom_gast > 10000. )
					error("printgrad: gastrulation time must be smaller than 10'000");
			break;
			case 'Z':
				extra_file_flag = 1;
			break;
			case ':':
				error("printgrad: need an argument for option -%c", optopt);
			break;
			case '?':
			default:
				error("printgrad: unrecognized option -%c", optopt);
		}
/* error check */
	if ( (argc - (optind - 1 )) < 2 )
		PrintMsg(usage, 1);
/* dynamic allocation of output format strings */
	precision = (char *)calloc(MAX_RECORD, sizeof(char));
	format    = (char *)calloc(MAX_RECORD, sizeof(char));
/* let's get started and open data file here */
	infile = fopen(argv[optind],"r");
	if ( !infile )
		file_error("printgrad");
/* if debugging: initialize solver log file */
	if ( debug ) {
		slogfile = (char *)calloc(MAX_RECORD, sizeof(char));
		sprintf(slogfile, "%s.slog", argv[optind]);
		slog = fopen(slogfile, "w");              /* delete existing slog file */
		fclose(slog);
		slog = fopen(slogfile, "a");            /* now keep open for appending */
	}
/* Initialization code here: InitZygote() initializes everything needed    *
 * for running the model, InitScoring() everything for scoring; InitStep-  *
 * size sets solver stepsize and accuracy in score.c to pass it on to      *
 * Blastoderm                                                              */
	InitZygote(infile, section_title);
	InitZygoteGrad();
	if ( extra_file_flag == 1 ) {
		FILE*extra;
		EqParms* eq = GetParameters();
		extra = fopen(argv[optind + 1], "r");
		ReReadParameters(eq, extra, section_title);
		fclose(extra);
	}
#ifdef XTRA
	gcdm_init_seq(infile);
#endif
	if ( e_mutate > 0 ) {
		for ( i = 0 ; i < e_mutate ; i++ ) {
			E_Mutate( e_mutate_genes[i] );
		}
	}
	if ( d_mutate > 0 ) {
		for ( i = 0 ; i < d_mutate ; i++ ) {
			D_Mutate( d_mutate_genes[i] );
		}
	}
	if ( rhs == 0 ) {
		if ( solver )
			gcdm_score_set_solver(0, solver);
		else
			gcdm_score_set_solver(0, solvername);
		gcdm_score_set_solver(1, solvername0);
	} else if ( rhs == 1 ) {
		gcdm_score_set_solver(0, solvername);
		if ( solver )
			gcdm_score_set_solver(1, solver);
		else
			gcdm_score_set_solver(1, solvername0);
	}
	gcdm_score_init(infile);
	gcdm_score_set_hints(dosage, zero_bias, rhs);
	InitStepsize(stepsize, accuracy, slog, argv[optind]);
	InitScoring2();
	gcdm_score_calc_g_num(ndigits, g_mode, parderstep);
	fclose(infile);
/* clean up before you go home...*/
	FreeZygote();
	free(precision);
	free(format);
	free(section_title);
	return 0;
}
