#include <float.h>
#ifdef ICC
#include <mathimf.h>
#else
#include <math.h>
#endif
#include <stdlib.h>
#include <stdio.h>
#include <InterpolatingFunctionPoly.h>
#include <SolutionTimeInterval.h>

InterpolatingFunctionPoly*InterpolatingFunctionPoly_createWith(int order)
{
	InterpolatingFunctionPoly*Func;
	Func = (InterpolatingFunctionPoly*)malloc(sizeof(InterpolatingFunctionPoly));
	Func->order = order;

	if ( Func->order !=2 ) {
	 	fprintf(stdout,"\n InterpolatingFunctionPoly: only order 2 is implemented so far not %d", Func->order);
	 	exit(0);
	}

	Func->ac = (double*)calloc(order+1, sizeof(double));

	Func->Info = (InterpolatingInfoPoly*)malloc(sizeof(InterpolatingInfoPoly));

	return Func;
}

void InterpolatingFunctionPoly_init(InterpolatingFunctionPoly*Func, double v, SolutionTimeInterval*time_interval)
{
	Func->Info->x_0 = time_interval->time_start;
	Func->Info->x_N = time_interval->time_finish;
	Func->Info->z_0 = v;
	Func->Info->A = 0;
	Func->Info->zA = 0;
	Func->Info->A2 = 0;
	Func->Info->BA = 0;
}

void InterpolatingFunctionPoly_addto(InterpolatingFunctionPoly*Func, double v, double time)
{
        double A_i;
        double BA_i;
        double x_0;
        double x_i;
        double x_N;

       	Func->Info->z_N = v;
        x_0 = Func->Info->x_0;
        x_i = time;
        x_N = Func->Info->x_N;

        A_i = x_i * x_i - x_0 * x_0 - ( x_N * x_N - x_0 * x_0 ) * ( x_i - x_0 ) / ( x_N - x_0 );

	Func->Info->A += A_i;
	Func->Info->zA += v * A_i;
	Func->Info->A2 += A_i * A_i;

        BA_i = A_i * ( x_i - x_0 ) / ( x_N - x_0 );
	
	Func->Info->BA += BA_i;
/*
fprintf(stdout,"\n %f %f %f %f",x_i, A_i, BA_i, v);
fflush(stdout);
*/
}

void InterpolatingFunctionPoly_make(InterpolatingFunctionPoly*Func)
{
	double a;
	double b;
	double c;

	a = ( Func->Info->A2 == 0 ) ? 0.0 : ( Func->Info->zA - Func->Info->z_0 * Func->Info->A - ( Func->Info->z_N - Func->Info->z_0 ) * Func->Info->BA ) / Func->Info->A2;

	b = ( ( Func->Info->z_N - Func->Info->z_0 ) - a * ( Func->Info->x_N * Func->Info->x_N - Func->Info->x_0 * Func->Info->x_0 ) ) / ( Func->Info->x_N - Func->Info->x_0 );

	c = Func->Info->z_0 - a * Func->Info->x_0 * Func->Info->x_0 - b * Func->Info->x_0;

	Func->ac[0] = c;
	Func->ac[1] = b;
	Func->ac[2] = a;
/*
fprintf(stdout,"\n ac: %f %f %f",c,b,a);
fflush(stdout);
*/
}

void InterpolatingFunctionPoly_delete(InterpolatingFunctionPoly*Func)
{
	free(Func->Info);
	free(Func->ac);
	free(Func);
}

double InterpolatingFunctionPoly_evaluate(InterpolatingFunctionPoly*Func, double time)
{
	int i;
	register double value = 0;
	register double var = 1;
	for ( i = 0; i < Func->order+1; i++ ) {
		value += Func->ac[i] * var;
		var *= time;
	}
	return value;
}
