#ifndef INTERPOLATINGFUNCTIONPOLY_INCLUDED
#define INTERPOLATINGFUNCTIONPOLY_INCLUDED

#include <float.h>
#ifdef ICC
#include <mathimf.h>
#else
#include <math.h>
#endif
#include <stdlib.h>
#include <stdio.h>

#include <SolutionTimeInterval.h>

typedef struct InterpolatingInfoPoly {
 double x_0;
 double z_0;
 double x_N;
 double z_N;
 double A;
 double zA;
 double A2;
 double BA;
} InterpolatingInfoPoly;

typedef struct InterpolatingFunctionPoly {
  int order;
  double *ac;
  InterpolatingInfoPoly*Info;
} InterpolatingFunctionPoly;

InterpolatingFunctionPoly*InterpolatingFunctionPoly_createWith(int order);
void InterpolatingFunctionPoly_init(InterpolatingFunctionPoly*Func, double v, SolutionTimeInterval*time_interval);
void InterpolatingFunctionPoly_addto(InterpolatingFunctionPoly*Func, double v, double time);
void InterpolatingFunctionPoly_make(InterpolatingFunctionPoly*Func);
void InterpolatingFunctionPoly_delete(InterpolatingFunctionPoly*Func);
double InterpolatingFunctionPoly_evaluate(InterpolatingFunctionPoly*Func, double time);

#endif
