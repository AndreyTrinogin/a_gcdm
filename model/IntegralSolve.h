/***************************************************************************
                          IntegaralSolve.h  -  description
                             -------------------
    begin                : ��� ��� 5 2004
    copyright            : (C) 2004 by Konstantin Kozlov
    email                : kostya@spbcas.ru
 ***************************************************************************/
#ifndef INTEGRALSOLVE_INCLUDED
#define INTEGRALSOLVE_INCLUDED

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
/*#include <malloc.h>*/
#include <limits.h>
#include <float.h>
#ifdef ICC
#include <mathimf.h>
#else
#include <math.h>
#endif

#include <SolutionTimeInterval.h>

typedef void (*V_IntKernel)(double,double*);

void V_IntSimpson(int,double**,SolutionTimeInterval*,V_IntKernel,double,double);

#endif
