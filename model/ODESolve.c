/***************************************************************************
                          ODESolve.c  -  description
                             -------------------
    begin                : 5 2004
    copyright            : (C) 2004 by Konstantin Kozlov
    email                : kostya@spbcas.ru
 ***************************************************************************/
/*****************************************************************
 *                                                               *
 *   solvers.c                                                   *
 *                                                               *
 *****************************************************************
 *                                                               *
 *   originally written by JR, modified by Yoginho               *
 *   additional solvers by: Joel Linton (Rk4)                    *
 *                          Johannes Jaeger (Rk2, Meuler, Heun)  *
 *                          Konstantin Kozlov (Milne, Adams)     *
 *                          Marcel Wolf (Rkck, Rkf)              *
 *                          Manu (Adams, BuSt)                   *
 *                                                               *
 *****************************************************************
 *                                                               *
 * solvers.c contains the solver functions (can be toggled by -s *
 * option) that propagate the equations                          *
 *                                                               *
 *****************************************************************
 *                                                               *
 * NOTE: *ONLY* general solvers allowed here; they *MUST* comply *
 *       to the generic solver interface                         *
 *                                                               *
 ** WARNING !!!!!!!!!!!! *****************************************
 *                                                               *
 * The functions in this file return vin and vout unchanged iff  *
 * stepsize = 0. It is the responsibility of the calling func-   *
 * tion to be sure that this is not a problem!!!                 *
 *                                                               *
 ** USAGE: *******************************************************
 *                                                               *
 *  All solvers adhere to the same interface:                    *
 *                                                               *
 *  Solver(double *vin, double *vout, double tin, double tout,   *
 *         double stephint, double accuracy, int n);             *
 *                                                               *
 *  Arguments:                                                   *
 *                                                               *
 *  - vin       array of dependent variables at 'tin' (start)    *
 *  - vout      array of dependent variables at 'tout' (end);    *
 *              this is what's returned by the solver            *
 *  - tin       time at start                                    *
 *  - tout      time at end                                      *
 *  - stephint  suggested stepsize for fixed stepsize solvers;   *
 *              see extensive comment below; the embedded Rk     *
 *              solvers (Rkck, Rkf) take stephint as their ini-  *
 *              tial stepsize                                    *
 *  - accuracy  accuracy for adaptive stepsize solvers; accuracy *
 *              is always relative, i.e. 0.1 means 10% of the    *
 *              actual v we've evaluated                         *
 *  - n         size of vin and vout arrays; the user is respon- *
 *              sible for checking that the number of elements   *
 *              does not change in the model between vin/vout    *
 *                                                               *
 *  Note that if tin = tout the solvers will just returns with-  *
 *  out doing anything. The caller is responsible for handling   *
 *  such a situation.                                            *
 *                                                               *
 ** SOLVER NAMING CONVENTIONS: ***********************************
 *                                                               *
 *  There are various contradictory solver nomenclatures in the  *
 *  literature. If not stated otherwise, we use the ones from    *
 *  the following two books, on which most of our solvers are    *
 *  based:                                                       *
 *                                                               *
 *  Press, Teukolsky, Vetterling & Flannery (1992). Numerical    *
 *  Recipes in C. Cambridge University Press, Cambridge, U.K.    *
 *                                                               *
 *  Burden & Faires (1993). Numerical Analysis, 5th Edition.     *
 *  PWS Publishing Co., Boston MA, U.S.A.                        *
 *                                                               *
 *
 ** IMPORTANT COMMENT: *******************************************
 *                                                               *
 * stephint and the stepsize of fixed-stepsize solvers:          *
 *                                                               *
 * The stepsize which is passed to a fixed-stepsize solvers is   *
 * actually a stephint, i.e. only an approximation to the actual *
 * stepsize which depends on the length of the interval over     *
 * which the equations are propagated:                           *
 *                                                               *
 * if the remainder of (deltat div stephint) is:                 *
 *                                                               *
 *   0                                   stepsize = stephint     *
 *   > 0 && < stephint/2                 stepsize > stephint     *
 *   >= stephint/2 && < stephint         stepsize < stephint     *
 *                                                               *
 * The exact amount of the difference between stepsize and step- *
 * hint depends on the size of deltat and stephint:              *
 *  - the larger deltat, the smaller the maximum possible diffe- *
 *    rence for a given stephint                                 *
 *  - the larger stephint, the larger the maximum possible di-   *
 *    fference for a given stephint                              *
 *                                                               *
 * Since mitoses only last about 3-5 minutes, these differences  *
 * are quite significant for large stepsizes ( > 0.1 ). For the  *
 * 4div schedule (newstyle datafiles) we have maximum relative   *
 * differences between stephint and actual stepsize of about:    *
 *                                                               *
 *   stephint                     max. rel. difference           *
 *                                                               *
 *        1.0                                    10.0%           *
 *        0.5                                     5.8%           *
 *        0.3                                     4.3%           *
 *        0.1                                     1.0%           *
 *                                                               *
 * See ../doc/step4div.ps for more details.                      *
 *                                                               *
 *****************************************************************/

#include <float.h>
#ifdef ICC
#include <mathimf.h>
#else
#include <math.h>
#endif
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include <error.h>
#include <ODESolve.h>
#include <ODESolutionPoly.h>

static double static_attr_time;
static double static_attr_time_step;
static double static_actual_attr_time;
static double static_attr_accuracy;
static double *static_attr_score_buffer;
static int *static_attr_ndatapoints_buffer;

static Derivative2 p_deriv2;
static Derivative p_deriv;
static Jacobian p_jacobn;

static int debug = 0;

static double *d;

static double *hpoints;

/*** STATIC VARIABLES AND MACROS *******************************************/

/* three macros used in various solvers below */

static double dqrarg;
#define DSQR(a) ((dqrarg=(a)) == 0.0 ? 0.0 : dqrarg*dqrarg)

static double dmaxarg1, dmaxarg2;
#define DMAX(a,b) (dmaxarg1 = (a), dmaxarg2 = (b), (dmaxarg1) > \
(dmaxarg2) ?  (dmaxarg1) : (dmaxarg2))

static double dminarg1, dminarg2;
#define DMIN(a,b) (dminarg1 = (a), dminarg2 = (b), (dminarg1) < \
(dminarg2) ?  (dminarg1) : (dminarg2))

ODESolve*ODESolve_new(char*name2, Derivative2 efunc2, char*name, Derivative efunc, Jacobian ifunc)
{
	ODESolve*p;
	p = (ODESolve*)malloc(sizeof(ODESolve));
	if ( !(strcmp(name2, "rkso")) )
		p->integrator2 = Rkso;
	else if ( !(strcmp(name2, "efd")) )
		p->integrator2 = exFinDif;
	else
		error("odesolve: invalid solver (%s), use: rkso, efd", name2);
	p->ekernel2 = efunc2;
	p->nintervals = 0;
	p->ikernel = ifunc;
	p->ekernel = efunc;
	if ( !(strcmp(name, "a")) )
		p->integrator = Adams;
	else if ( !(strcmp(name, "bd")) )
		p->integrator = BaDe;
	else if ( !(strcmp(name, "bs")) )
		p->integrator = BuSt;
	else if ( !(strcmp(name, "e")) )
		p->integrator = Euler;
	else if ( !(strcmp(name, "h")) )
		p->integrator = Heun;
	else if ( !(strcmp(name, "mi")) || !(strcmp(name, "m")) )
		p->integrator = Milne;
	else if ( !(strcmp(name, "me")) )
		p->integrator = Meuler;
	else if ( !(strcmp(name, "r4")) || !(strcmp(name, "r")) )
		p->integrator = Rk4;
	else if ( !(strcmp(name, "r2")) )
		p->integrator = Rk2;
	else if ( !(strcmp(name, "rck")) )
		p->integrator = Rkck;
	else if ( !(strcmp(name, "rf")) )
		p->integrator = Rkf;
	else if ( !(strcmp(name, "dde11")) )
		p->integrator = dde11;
	else if ( !(strcmp(name, "dde21")) )
		p->integrator = dde21;
	else if ( !(strcmp(name, "dde21d")) )
		p->integrator = dde21d;
	else if ( !(strcmp(name, "dde41")) )
		p->integrator = dde41;
	else
		error("odesolve: invalid solver (%s), use: a,bs,e,h,mi,me,r{2,4,ck,f}", name);
	return p;
}

ODESolve*ODESolve_create2(char*name, Derivative2 efunc)
{
	ODESolve*p;
	p = (ODESolve*)malloc(sizeof(ODESolve));
	if ( !(strcmp(name, "rkso")) )
		p->integrator2 = Rkso;
	else if ( !(strcmp(name, "efd")) )
		p->integrator2 = exFinDif;
  else
		error("odesolve: invalid solver (%s), use: rkso, efd", name);
	p->ekernel2 = efunc;
	p->nintervals = 0;
	p->intervals = NULL;
	p->integrator = NULL;
	return p;
}

void ODESolve_SetIntegrator2(char*name, ODESolve*p)
{
	if ( !(strcmp(name, "rkso")) )
		p->integrator2 = Rkso;
	else if ( !(strcmp(name, "efd")) )
		p->integrator2 = exFinDif;
  else
		error("odesolve: invalid solver (%s), use: rkso, efd", name);
}

ODESolve*ODESolve_create(char*name, Derivative efunc, Jacobian ifunc)
{
	ODESolve*p;
	p = (ODESolve*)malloc(sizeof(ODESolve));
	p->ikernel = ifunc;
	p->ekernel = efunc;
	if ( !(strcmp(name, "a")) )
		p->integrator = Adams;
	else if ( !(strcmp(name, "bd")) )
		p->integrator = BaDe;
	else if ( !(strcmp(name, "bs")) )
		p->integrator = BuSt;
	else if ( !(strcmp(name, "e")) )
		p->integrator = Euler;
	else if ( !(strcmp(name, "h")) )
		p->integrator = Heun;
	else if ( !(strcmp(name, "mi")) || !(strcmp(name, "m")) )
		p->integrator = Milne;
	else if ( !(strcmp(name, "me")) )
		p->integrator = Meuler;
	else if ( !(strcmp(name, "r4")) || !(strcmp(name, "r")) )
		p->integrator = Rk4;
	else if ( !(strcmp(name, "r2")) )
		p->integrator = Rk2;
	else if ( !(strcmp(name, "rck")) )
		p->integrator = Rkck;
	else if ( !(strcmp(name, "rf")) )
		p->integrator = Rkf;
	else
		error("odesolve: invalid solver (%s), use: a,bs,e,h,mi,me,r{2,4,ck,f}", name);
	p->nintervals = 0;
	p->intervals = NULL;
	p->integrator2 = NULL;
	return p;
}

void ODESolve_SetIntegrator(char*name, ODESolve*p)
{
	if ( !(strcmp(name, "a")) )
		p->integrator = Adams;
	else if ( !(strcmp(name, "bd")) )
		p->integrator = BaDe;
	else if ( !(strcmp(name, "bs")) )
		p->integrator = BuSt;
	else if ( !(strcmp(name, "e")) )
		p->integrator = Euler;
	else if ( !(strcmp(name, "h")) )
		p->integrator = Heun;
	else if ( !(strcmp(name, "mi")) || !(strcmp(name, "m")) )
		p->integrator = Milne;
	else if ( !(strcmp(name, "me")) )
		p->integrator = Meuler;
	else if ( !(strcmp(name, "r4")) || !(strcmp(name, "r")) )
		p->integrator = Rk4;
	else if ( !(strcmp(name, "r2")) )
		p->integrator = Rk2;
	else if ( !(strcmp(name, "rck")) )
		p->integrator = Rkck;
	else if ( !(strcmp(name, "rf")) )
		p->integrator = Rkf;
	else
		error("odesolve: invalid solver (%s), use: rkso", name);
}


void ODESolve_SetIntegratorAttr(char*name, ODESolve*p)
{
	if ( !(strcmp(name, "bs")) )
		p->integrator_attr = BuStAttr;
	else
		error("odesolve: invalid attr solver (%s), use: bs", name);
}

ODESolutionPoly* ODESolve_SolveAttr(ODESolve*p,double *vin, double *vout, double tin, double tout, double stephint, double accuracy, int n, FILE *slog)
{
  p_deriv = p->ekernel;
  p_jacobn = p->ikernel;

  return p->integrator_attr(vin, vout, tin, tout, stephint, accuracy, n, slog);
}

ODESolutionPoly* ODESolve_Solve(ODESolve*p,double *vin, double *vout, double tin, double tout, double stephint, double accuracy, int n, FILE *slog)
{
	p_deriv = p->ekernel;
	p_jacobn = p->ikernel;
	return p->integrator(vin, vout, tin, tout, stephint, accuracy, n, slog);
}

ODESolutionPoly* ODESolve_Solve2(ODESolve*p,double *vin, double *win, double *vout, double *wout, double tin, double tout, double stephint, double accuracy, int n, FILE *slog)
{
	p_deriv2 = p->ekernel2;
	return p->integrator2(vin, win, vout, wout, tin, tout, stephint, accuracy, n, slog);
}

void ODESolve_delete(ODESolve*p)
{
  free(p);
}

/*** SOLVERS ***************************************************************/

/*** Euler: propagates vin (of size n) from tin to tout by the Euler *******
 *          method; the result is returned by vout                         *
 ***************************************************************************/

ODESolutionPoly* Euler(double *vin, double *vout, double tin, double tout,
	   double stephint, double accuracy, int n, FILE *slog)
{
  int    i;                                          /* local loop counter */

  double **v;                 /* intermediate v's, used to toggle v arrays */
  int    toggle = 0;               /* used to toggle between v[0] and v[1] */

  double *vnow;                                /* ptr to v at current time */
  double *vnext;                    /* ptr to v at current time + stepsize */

  double *deriv;             /* the derivatives at the beginning of a step */

  double deltat;                                             /* tout - tin */
  double t;                                                /* current time */

  double m;         /* tmp var to calculate precise stepsize from stephint */
  double stepsize;                                        /* real stepsize */
  int    step;                                   /* loop counter for steps */
  int    nsteps;                  /* total number of steps we have to take */

  int    nd = 0;                            /* number of deriv evaluations */

  ODESolutionPoly *Solution;

/* steps too small: just return */

  if (tin == tout)
    return NULL;

/* if steps big enough */

  Solution = ODESolutionPoly_createWith( tin, tout, n);

  v = (double **)calloc(2, sizeof(double *));

  v[0]  = (double *)calloc(n, sizeof(double));
  v[1]  = (double *)calloc(n, sizeof(double));
  deriv = (double *)calloc(n, sizeof(double));

  deltat = tout - tin;                 /* how far do we have to propagate? */
  m = floor(fabs(deltat)/stephint + 0.5);       /* see comment on stephint above */
  if ( m < 1. )
    m = 1.;                          /* we'll have to do at least one step */

  stepsize = deltat / m;                  /* real stepsize calculated here */
  nsteps = (int)m;                                  /* int number of steps */
  t = tin;                                             /* set current time */

  if ( t == t + stepsize )
    error("Euler: stephint of %g too small!", stephint);

  ODESolutionPoly_setStepsize(Solution,stepsize);
  ODESolutionPoly_setNSteps(Solution,nsteps);

  ODESolutionPoly_initInterpolating(Solution,vin);

  vnow = vin;

  if (nsteps == 1)       /* vnext holds the results after the current step */
    vnext = vout;
  else
    vnext = v[0];

  for (step=0; step < nsteps; step++) {                  /* loop for steps */

    p_deriv(vnow, t, deriv, n);  /* call derivative func to evaluate deriv */

    if ( debug ) nd++;

    for(i=0; i < n; i++)
      vnext[i] = vnow[i] + stepsize * deriv[i];           /* Euler formula */

    t += stepsize;                                      /* go to next step */

    ODESolutionPoly_addtoInterpolating(Solution,vnext,t);

    if (step < nsteps - 2) {                   /* CASE 1: many steps to go */
      vnow = v[toggle];               /* toggle results from v[0] and v[1] */
      toggle++;
      toggle %= 2;
      vnext = v[toggle];

    } else if (step == nsteps - 2) {     /* CASE 2: next iteration = final */
      vnow = v[toggle];                                        /* set vout */
      vnext = vout;

    } else if (step > nsteps - 2) {    /* CASE 3: just did final iteration */
      free(v[0]);                                 /* clean up and go home! */
      free(v[1]);
      free(v);
      free(deriv);
    }
  }

  ODESolutionPoly_makeInterpolating(Solution);

  if ( debug )
    WriteSolvLog("Euler", tin, tout, stepsize, nsteps, nd, slog);

  return Solution;
}



/*** Meuler: propagates vin (of size n) from tin to tout by the Modified ***
 *           Euler method (this is NOT the midpoint method, see Rk2());    *
 *           the result is returned by vout                                *
 ***************************************************************************/

ODESolutionPoly*Meuler(double *vin, double *vout, double tin, double tout,
	    double stephint, double accuracy, int n, FILE *slog)
{
  int i;                                             /* local loop counter */

  double **v;                 /* intermediate v's, used to toggle v arrays */
  int toggle = 0;                  /* used to toggle between v[0] and v[1] */

  double *vtemp;                  /* guessed intermediate v's for midpoint */

  double *vnow;                                /* ptr to v at current time */
  double *vnext;                    /* ptr to v at current time + stepsize */

  double *deriv1;            /* the derivatives at the beginning of a step */
  double *deriv2;                 /* the derivatives at midpoint of a step */

  double deltat;                                             /* tout - tin */
  double t;                                                /* current time */
  double th;                         /* time at end of a step (t+stepsize) */

  double m;         /* tmp var to calculate precise stepsize from stephint */
  double stepsize;                                        /* real stepsize */
  int    step;                                   /* loop counter for steps */
  int    nsteps;                  /* total number of steps we have to take */

  int    nd = 0;                            /* number of deriv evaluations */

  ODESolutionPoly *Solution;

/* the do-nothing case; too small steps dealt with under usual */

  if (tin == tout)
    return NULL;

  Solution = ODESolutionPoly_createWith(tin, tout, n);

/* the usual case: steps big enough */

  v      = (double **)calloc(2, sizeof(double *));

  vtemp  = (double *)calloc(n, sizeof(double));
  v[0]   = (double *)calloc(n, sizeof(double));
  v[1]   = (double *)calloc(n, sizeof(double));
  deriv1 = (double *)calloc(n, sizeof(double));
  deriv2 = (double *)calloc(n, sizeof(double));


  deltat = tout - tin;                 /* how far do we have to propagate? */
  m = floor(fabs(deltat)/stephint + 0.5);       /* see comment on stephint above */
  if ( m < 1. )
    m = 1.;                          /* we'll have to do at least one step */

  stepsize = deltat / m;                  /* real stepsize calculated here */
  nsteps = (int)m;                                  /* int number of steps */
  t = tin;                                             /* set current time */

  if ( t == t + stepsize )
    error("Meuler: stephint of %g too small!", stephint);

  ODESolutionPoly_setStepsize(Solution,stepsize);
  ODESolutionPoly_setNSteps(Solution,nsteps);

  ODESolutionPoly_initInterpolating(Solution,vin);

  vnow = vin;

  if (nsteps == 1)       /* vnext holds the results after the current step */
    vnext = vout;
  else
    vnext = v[0];

  for (step=0; step < nsteps; step++) {                  /* loop for steps */

    th = t + stepsize;                            /* time of step endpoint */

    p_deriv(vnow, t, deriv1, n);                    /* first call to deriv */

    if ( debug ) nd++;

    for (i=0; i<n; i++)                        /* evaluate v's at endpoint */
      vtemp[i] = vnow[i] + stepsize * deriv1[i];

    p_deriv(vtemp, th, deriv2, n);                 /* second call to deriv */

    if ( debug ) nd++;

    for(i=0; i < n; i++)                         /* Modified Euler formula */
      vnext[i] = vnow[i] + (stepsize / 2.) * (deriv1[i] + deriv2[i]);

    t += stepsize;                                      /* go to next step */

    ODESolutionPoly_addtoInterpolating(Solution,vnext,t);

    if (step < nsteps - 2) {                   /* CASE 1: many steps to go */
      vnow = v[toggle];               /* toggle results from v[0] and v[1] */
      toggle++;
      toggle %= 2;
      vnext = v[toggle];

    } else if (step == nsteps - 2) {     /* CASE 2: next iteration = final */
      vnow = v[toggle];                                        /* set vout */
      vnext = vout;

    } else if (step > nsteps - 2) {    /* CASE 3: just did final iteration */
      free(v[0]);                                 /* clean up and go home! */
      free(v[1]);
      free(v);
      free(vtemp);
      free(deriv1);
      free(deriv2);
    }
  }

  ODESolutionPoly_makeInterpolating(Solution);

  if ( debug )
    WriteSolvLog("Meuler", tin, tout, stepsize, nsteps, nd, slog);

  return Solution;
}



/*** Heun: propagates vin (of size n) from tin to tout by Heun's method ****
 *         the result is returned by vout                                  *
 ***************************************************************************/

ODESolutionPoly*Heun(double *vin, double *vout, double tin, double tout,
	  double stephint, double accuracy, int n, FILE *slog)
{
  int i;                                             /* local loop counter */

  double **v;                 /* intermediate v's, used to toggle v arrays */
  int    toggle = 0;               /* used to toggle between v[0] and v[1] */

  double *vtemp;                  /* guessed intermediate v's for midpoint */

  double *vnow;                                /* ptr to v at current time */
  double *vnext;                    /* ptr to v at current time + stepsize */

  double *deriv1;            /* the derivatives at the beginning of a step */
  double *deriv2;                 /* the derivatives at midpoint of a step */

  double deltat;                                             /* tout - tin */
  double t;                                                /* current time */
  double thh;                                   /* time at 2/3 of the step */

  double m;         /* tmp var to calculate precise stepsize from stephint */
  double stepsize;                                        /* real stepsize */
  double hh;                                           /* 2/3 the stepsize */
  int    step;                                   /* loop counter for steps */
  int    nsteps;                  /* total number of steps we have to take */

  int    nd = 0;                            /* number of deriv evaluations */

  ODESolutionPoly *Solution;

/* the do-nothing case; too small steps dealt with under usual */

  if (tin == tout)
    return NULL;

  Solution = ODESolutionPoly_createWith(tin, tout, n);

/* the usual case: steps big enough */

  v      = (double **)calloc(2, sizeof(double *));

  vtemp  = (double *)calloc(n, sizeof(double));
  v[0]   = (double *)calloc(n, sizeof(double));
  v[1]   = (double *)calloc(n, sizeof(double));
  deriv1 = (double *)calloc(n, sizeof(double));
  deriv2 = (double *)calloc(n, sizeof(double));


  deltat = tout - tin;                 /* how far do we have to propagate? */
  m = floor(fabs(deltat)/stephint + 0.5);       /* see comment on stephint above */
  if ( m < 1. )
    m = 1.;                          /* we'll have to do at least one step */

  stepsize = deltat / m;                  /* real stepsize calculated here */
  nsteps = (int)m;                                  /* int number of steps */
  t = tin;                                             /* set current time */

  if ( t == t + stepsize )
    error("Heun: stephint of %g too small!", stephint);

  ODESolutionPoly_setStepsize(Solution,stepsize);
  ODESolutionPoly_setNSteps(Solution,nsteps);

  ODESolutionPoly_initInterpolating(Solution,vin);

  vnow = vin;

  if (nsteps == 1)       /* vnext holds the results after the current step */
    vnext = vout;
  else
    vnext = v[0];

  hh  = stepsize * 2. / 3.;                    /* evaluate 2/3 of stepsize */

  for (step=0; step < nsteps; step++) {                  /* loop for steps */

    thh = t + hh;                               /* time at 2/3 of the step */

    p_deriv(vnow, t, deriv1, n);                    /* first call to deriv */

    if ( debug ) nd++;

    for(i=0; i<n; i++)                  /* evaluate v's at 2/3 of the step */
      vtemp[i] = vnow[i] + hh * deriv1[i];

    p_deriv(vtemp, thh, deriv2, n);                /* second call to deriv */

    if ( debug ) nd++;

    for(i=0; i < n; i++)                                 /* Heun's formula */
      vnext[i] = vnow[i] + (stepsize / 4.) * (deriv1[i] + 3. * deriv2[i]);

    t += stepsize;                                      /* go to next step */

    ODESolutionPoly_addtoInterpolating(Solution,vnext,t);

    if (step < nsteps - 2) {                   /* CASE 1: many steps to go */
      vnow = v[toggle];               /* toggle results from v[0] and v[1] */
      toggle++;
      toggle %= 2;
      vnext = v[toggle];

    } else if (step == nsteps - 2) {     /* CASE 2: next iteration = final */
      vnow = v[toggle];                                        /* set vout */
      vnext = vout;

    } else if (step > nsteps - 2) {    /* CASE 3: just did final iteration */
      free(v[0]);                                 /* clean up and go home! */
      free(v[1]);
      free(v);
      free(vtemp);
      free(deriv1);
      free(deriv2);
    }
  }

  ODESolutionPoly_makeInterpolating(Solution);

  if ( debug )
    WriteSolvLog("Heun", tin, tout, stepsize, nsteps, nd, slog);

  return Solution;
}



/*** Rk2: propagates vin (of size n) from tin to tout by the Midpoint or ***
 *        Second-Order Runge-Kutta method; the result is returned by vout  *
 ***************************************************************************/

ODESolutionPoly*Rk2(double *vin, double *vout, double tin, double tout,
	 double stephint, double accuracy, int n, FILE *slog)
{
  int i;                                             /* local loop counter */

  double **v;                 /* intermediate v's, used to toggle v arrays */
  int    toggle = 0;               /* used to toggle between v[0] and v[1] */

  double *vtemp;                  /* guessed intermediate v's for midpoint */

  double *vnow;                                /* ptr to v at current time */
  double *vnext;                    /* ptr to v at current time + stepsize */

  double *deriv1;            /* the derivatives at the beginning of a step */
  double *deriv2;                 /* the derivatives at midpoint of a step */

  double deltat;                                             /* tout - tin */
  double t;                                                /* current time */
  double thh;                                  /* time at half of the step */

  double m;         /* tmp var to calculate precise stepsize from stephint */
  double stepsize;                                        /* real stepsize */
  double hh;                                          /* half the stepsize */
  int    step;                                   /* loop counter for steps */
  int    nsteps;                  /* total number of steps we have to take */

  int    nd = 0;                            /* number of deriv evaluations */

  ODESolutionPoly *Solution;

/* the do-nothing case; too small steps dealt with under usual */

  if (tin == tout)
    return NULL;

  Solution = ODESolutionPoly_createWith(tin, tout, n);

/* the usual case: steps big enough */

  v      = (double **)calloc(2, sizeof(double *));

  vtemp  = (double *)calloc(n, sizeof(double));
  v[0]   = (double *)calloc(n, sizeof(double));
  v[1]   = (double *)calloc(n, sizeof(double));
  deriv1 = (double *)calloc(n, sizeof(double));
  deriv2 = (double *)calloc(n, sizeof(double));


  deltat = tout - tin;                 /* how far do we have to propagate? */
  m = floor(fabs(deltat)/stephint + 0.5);       /* see comment on stephint above */
  if ( m < 1. )
    m = 1.;                          /* we'll have to do at least one step */

  stepsize = deltat / m;                  /* real stepsize calculated here */
  nsteps = (int)m;                                  /* int number of steps */
  t = tin;                                             /* set current time */

  if ( t == t + stepsize )
    error("Rk2: stephint of %g too small!", stephint);

  ODESolutionPoly_setStepsize(Solution,stepsize);
  ODESolutionPoly_setNSteps(Solution,nsteps);

  ODESolutionPoly_initInterpolating(Solution,vin);

  vnow = vin;

  if (nsteps == 1)              /* vnext holds the results of current step */
    vnext = vout;
  else
    vnext = v[0];

  hh  = stepsize * 0.5;                       /* evaluate half of stepsize */

  for (step=0; step < nsteps; step++) {                  /* loop for steps */

    thh = t + hh;                            /* time of interval midpoints */

    p_deriv(vnow, t, deriv1, n);                    /* first call to deriv */

    if ( debug ) nd++;

    for(i=0; i<n; i++)                         /* evaluate v's at midpoint */
      vtemp[i] = vnow[i] + hh * deriv1[i];

    p_deriv(vtemp, thh, deriv2, n);                /* second call to deriv */

    if ( debug ) nd++;

    for(i=0; i < n; i++)
      vnext[i] = vnow[i] + stepsize * deriv2[i];       /* Midpoint formula */

    t += stepsize;                                      /* go to next step */

    ODESolutionPoly_addtoInterpolating(Solution,vnext,t);

    if (step < nsteps - 2) {                   /* CASE 1: many steps to go */
      vnow = v[toggle];               /* toggle results from v[0] and v[1] */
      toggle++;
      toggle %= 2;
      vnext = v[toggle];

    } else if (step == nsteps - 2) {     /* CASE 2: next iteration = final */
      vnow = v[toggle];                                        /* set vout */
      vnext = vout;

    } else if (step > nsteps - 2) {    /* CASE 3: just did final iteration */
      free(v[0]);                                 /* clean up and go home! */
      free(v[1]);
      free(v);
      free(vtemp);
      free(deriv1);
      free(deriv2);
    }
  }

  ODESolutionPoly_makeInterpolating(Solution);

  if ( debug )
    WriteSolvLog("Rk2", tin, tout, stepsize, nsteps, nd, slog);

  return Solution;
}



/*** Rk4: propagates vin (of size n) from tin to tout by the Fourth-Order **
 *        Runge-Kutta method; the result is returned by vout               *
 ***************************************************************************
 *                                                                         *
 * written by Joel Linton (somewhere around 1998)                          *
 * fixed and modified by Yoginho (somewhere around 2001)                   *
 *                                                                         *
 ***************************************************************************/

ODESolutionPoly*Rk4(double *vin, double *vout, double tin, double tout,
	 double stephint, double accuracy, int n, FILE *slog)
{
  int i;                                             /* local loop counter */

  double **v;                 /* intermediate v's, used to toggle v arrays */
  int toggle = 0;                  /* used to toggle between v[0] and v[1] */

  double *vtemp;                               /* guessed intermediate v's */

  double *vnow;                                /* ptr to v at current time */
  double *vnext;                    /* ptr to v at current time + stepsize */

  double *deriv1;            /* the derivatives at the beginning of a step */
  double *deriv2;                    /* the derivatives at test-midpoint 1 */
  double *deriv3;                    /* the derivatives at test-midpoint 2 */
  double *deriv4;                      /* the derivatives at test-endpoint */

  double deltat;                                             /* tout - tin */
  double t;                                                /* current time */
  double th;                                /* time at the end of the step */
  double thh;                               /* time for interval midpoints */

  double m;                           /* used to calculate number of steps */
  double stepsize;                                        /* real stepsize */
  double hh;                                          /* half the stepsize */
  double h6;                /* one sixth of the stepsize (for Rk4 formula) */
  int    step;                                   /* loop counter for steps */
  int    nsteps;                        /* number of steps we have to take */

  int    nd = 0;                            /* number of deriv evaluations */

  ODESolutionPoly *Solution;

/* the do-nothing case; too small steps dealt with under usual */

  if (tin == tout)
    return NULL;

  Solution = ODESolutionPoly_createWith(tin, tout, n);

/* the usual case: steps big enough */

  v      = (double **)calloc(2, sizeof(double *));

  vtemp  = (double *)calloc(n, sizeof(double));
  v[0]   = (double *)calloc(n, sizeof(double));
  v[1]   = (double *)calloc(n, sizeof(double));
  deriv1 = (double *)calloc(n, sizeof(double));
  deriv2 = (double *)calloc(n, sizeof(double));
  deriv3 = (double *)calloc(n, sizeof(double));
  deriv4 = (double *)calloc(n, sizeof(double));

  deltat = tout - tin;                 /* how far do we have to propagate? */
  m = floor(fabs(deltat)/stephint + 0.5);       /* see comment on stephint above */
  if ( m < 1. )
    m = 1.;                          /* we'll have to do at least one step */

  stepsize = deltat / m;                  /* real stepsize calculated here */
  nsteps = (int)m;                                  /* int number of steps */
  t = tin;                                             /* set current time */

  if ( t == t + stepsize )
    error("Rk4: stephint of %g too small!", stephint);

  ODESolutionPoly_setStepsize(Solution,stepsize);
  ODESolutionPoly_setNSteps(Solution,nsteps);

  ODESolutionPoly_initInterpolating(Solution,vin);

  vnow = vin;

  if (nsteps == 1)              /* vnext holds the results of current step */
    vnext = vout;
  else
    vnext = v[0];

  hh  = stepsize * 0.5;
  h6  = stepsize / 6.0;

  for (step=0; step < nsteps; step++) {                  /* loop for steps */

    thh = t + hh;                            /* time of interval midpoints */
    th  = t + stepsize;                     /* time at end of the interval */

/* do the Runge-Kutta thing here: first calculate intermediate derivatives */

    p_deriv(vnow, t, deriv1, n);

    if ( debug ) nd++;

    for(i=0; i<n; i++)
      vtemp[i] = vnow[i] + hh * deriv1[i];
    p_deriv(vtemp, thh, deriv2, n);

    if ( debug ) nd++;

    for(i=0; i<n; i++)
      vtemp[i] = vnow[i] + hh * deriv2[i];
    p_deriv(vtemp, thh, deriv3, n);

    if ( debug ) nd++;

    for(i=0; i<n; i++)
      vtemp[i] = vnow[i] + stepsize * deriv3[i];
    p_deriv(vtemp, th, deriv4, n);

    if ( debug ) nd++;

/* ... then feed them to the Fourth-Order Runge-Kutta formula */

   for(i=0; i<n; i++)
      vnext[i] = vnow[i]
	+ h6 * (deriv1[i] + 2.0 * deriv2[i] + 2.0 * deriv3[i] + deriv4[i]);

/* next step */

    t += stepsize;

    ODESolutionPoly_addtoInterpolating(Solution,vnext,t);

    if (step < nsteps - 2) {                   /* CASE 1: many steps to go */
      vnow = v[toggle];               /* toggle results from v[0] and v[1] */
      toggle++;
      toggle %= 2;
      vnext = v[toggle];

    } else if (step == nsteps - 2) {     /* CASE 2: next iteration = final */
      vnow = v[toggle];                                        /* set vout */
      vnext = vout;

    } else if (step > nsteps - 2) {    /* CASE 3: just did final iteration */
      free(v[0]);                                 /* clean up and go home! */
      free(v[1]);
      free(v);
      free(vtemp);
      free(deriv1);
      free(deriv2);
      free(deriv3);
      free(deriv4);
    }
  }

  ODESolutionPoly_makeInterpolating(Solution);

  if ( debug )
    WriteSolvLog("Rk4", tin, tout, stepsize, nsteps, nd, slog);

  return Solution;
}




/*** Rkck: propagates vin (of size n) from tin to tout by the Runge-Kutta **
 *         Cash-Karp method, which is an adaptive-stepsize Rk method; it   *
 *         uses a fifth-order Rk formula with an embedded forth-oder for-  *
 *         mula for calucalting the error; its result is returned by vout  *
 ***************************************************************************
 *                                                                         *
 * This solver was written by Marcel Wolf, Spring 2002.                    *
 *                                                                         *
 ***************************************************************************/

ODESolutionPoly*Rkck(double *vin, double *vout, double tin, double tout,
	  double stephint, double accuracy, int n, FILE *slog)
{
  int i;                                             /* local loop counter */

  double **v;                      /** used for storing intermediate steps */
  int    toggle = 0;               /* used to toggle between v[0] and v[1] */

  double *vtemp;                               /* guessed intermediate v's */

  double *vnow;                                /* ptr to v at current time */
  double *vnext;                    /* ptr to v at current time + stepsize */

  double *verror;                                        /* error estimate */
  double verror_max;                        /* the maximum error in verror */

  double *deriv1;    /* intermediate derivatives for the Cash-Karp formula */
  double *deriv2;
  double *deriv3;
  double *deriv4;
  double *deriv5;
  double *deriv6;

  double t;                                            /* the current time */

  double h = stephint;                                 /* initial stepsize */
  double hnext;                         /* used to calculate next stepsize */
  const double SAFETY = 0.9;      /* safety margin for decreasing stepsize */



/* declare and initialize Cash-Karp parameters */
/* parameters that are zero have been added as comments for clarity */

  static double
    a2  =     0.2,
    a3  =     0.3,
    a4  =     0.6,
    a5  =     1.0,
    a6  =     0.875;

  static double
    b21 =     0.2,
    b31 =     3.0/40.0,
    b32 =     9.0/40.0,
    b41 =     0.3,
    b42 =    -0.9,
    b43 =     1.2,
    b51 =   -11.0/54.0,
    b52 =     2.5,
    b53 =   -70.0/27.0,
    b54 =    35.0/27.0,
    b61 =  1631.0/55296.0,
    b62 =   175.0/512.0,
    b63 =   575.0/13824,
    b64 = 44275.0/110592.0,
    b65 =   253.0/4096.0;

  static double
    c1  =    37.0/378.0,
/*  c2  =     0.0 */
    c3  =   250.0/621.0,
    c4  =   125.0/594.0,
/*  c5  =     0.0 */
    c6  =   512.0/1771.0;

  double
    dc1 = c1 -  2825.0/27648.0,
/*  dc2 =          0.0 */
    dc3 = c3 - 18575.0/48384.0,
    dc4 = c4 - 13525.0/55296.0,
    dc5 =       -277.0/14336.0,
    dc6 = c6 -     0.25;

  ODESolutionPoly *Solution;

/* the do-nothing case; too small steps dealt with under usual */

  if (tin == tout)
    return NULL;

  Solution = ODESolutionPoly_createWith(tin, tout, n);

/* the usual case: steps big enough */

  v      = (double **)calloc(2, sizeof(double *));

  vtemp  = (double *)calloc(n, sizeof(double));
  verror = (double *)calloc(n, sizeof(double));
  v[0]   = (double *)calloc(n, sizeof(double));
  v[1]   = (double *)calloc(n, sizeof(double));
  deriv1 = (double *)calloc(n, sizeof(double));
  deriv2 = (double *)calloc(n, sizeof(double));
  deriv3 = (double *)calloc(n, sizeof(double));
  deriv4 = (double *)calloc(n, sizeof(double));
  deriv5 = (double *)calloc(n, sizeof(double));
  deriv6 = (double *)calloc(n, sizeof(double));
/*
  ODESolutionPoly_setStepsize(Solution,stepsize);
  ODESolutionPoly_setNSteps(Solution,nsteps);
*/
  ODESolutionPoly_initInterpolating(Solution,vin);

  t     = tin;
  vnow  = vin;
  vnext = v[0];

/* initial stepsize cannot be bigger than total time */

  if (tin + h >= tout)
    h = tout - tin;

  while (t < tout) {

/* Take one step and evaluate the error. Repeat until the resulting error  *
 * is less than the desired accuracy                                       */

    while(1) {

/* do the Runge-Kutta thing here: calulate intermediate derivatives */

      p_deriv(vnow, t, deriv1, n);

      for (i=0; i<n; i++)
	vtemp[i] = vnow[i] + h
	  * (b21 * deriv1[i]);
      p_deriv(vtemp, t + a2 * h, deriv2, n);

      for (i=0; i<n; i++)
	vtemp[i] = vnow[i] + h
	  * (b31 * deriv1[i] + b32 * deriv2[i]);
      p_deriv(vtemp, t + a3 * h, deriv3, n);

      for (i=0; i<n; i++)
	vtemp[i] = vnow[i] + h
	  * (b41 * deriv1[i] + b42 * deriv2[i] + b43 * deriv3[i]);
      p_deriv(vtemp, t + a4 * h, deriv4, n);

      for (i=0; i<n; i++)
	vtemp[i] = vnow[i] + h
	  * (b51 * deriv1[i] + b52 * deriv2[i] + b53 * deriv3[i]
	   + b54 * deriv4[i]);
      p_deriv(vtemp, t + a5 * h, deriv5, n);

      for (i=0; i<n; i++)
	vtemp[i] = vnow[i] + h
	  * (b61 * deriv1[i] + b62 * deriv2[i] + b63 * deriv3[i]
	   + b64 * deriv4[i] + b65 * deriv5[i]);
      p_deriv(vtemp, t + a6 * h, deriv6, n);

/* ... then feed them to the Cash-Karp formula */

      for (i=0; i<n; i++)
	vnext[i] = vnow[i]
	  + h * (c1 * deriv1[i] + c3 * deriv3[i] + c4 * deriv4[i]
	       + c6 * deriv6[i]);

/* calculate the error estimate using the embedded formula */

      for (i=0; i<n; i++)
	verror[i] = h * (dc1 * deriv1[i] + dc3 * deriv3[i]
			    + dc4 * deriv4[i] + dc5 * deriv5[i]
			    + dc6 * deriv6[i]);

/* find the maximum error */

      verror_max = 0.;
      for (i=0; i<n; i++)
	if ( vnext[i] != 0. )
	  verror_max = DMAX(fabs(verror[i]/vnext[i]), verror_max);
	else
	  verror_max = DMAX(verror[i]/DBL_EPSILON, verror_max);

/* scale error according to desired accuracy */

      verror_max /= accuracy;

/* compare maximum error to the desired accuracy; if error < accuracy, we  *
 * are done with this step; otherwise, the stepsize has to be reduced and  *
 * the step repeated; for detailed comments on the approximation involving *
 * SAFETY and -0.25 see 'Numerical Recipes in C', 2nd Edition, p.718       */

      if (verror_max <= 1.0)
	break;

      hnext = SAFETY * h * pow(verror_max, -0.25);

/* decrease stepsize by no more than a factor of 10; check for underflows */

      h = (hnext > 0.1 * h) ? hnext : 0.1 * h;
      if (h == 0)
	error("Rkck: stepsize underflow");

    }

/* advance the current time by last stepsize */

    t += h;

    ODESolutionPoly_addtoInterpolating(Solution,vnext,t);

    if (t >= tout)
      break;                                /* that was the last iteration */

/* increase stepsize according to error (5th order) for next iteration */

    h = h * pow(verror_max, -0.20);

/* make sure t does not overstep tout */

    if (t + h >= tout)
      h = tout - t;

/* toggle vnow and vnext between v[0] and v[1] */

    vnow = v[toggle];
    toggle++;
    toggle %= 2;
    vnext = v[toggle];

  }

/* copy the last result to vout after the final iteration */

  memcpy(vout, vnext, sizeof(*vnext) * n);

  ODESolutionPoly_makeInterpolating(Solution);

  free(v[0]);
  free(v[1]);
  free(v);
  free(vtemp);
  free(verror);
  free(deriv1);
  free(deriv2);
  free(deriv3);
  free(deriv4);
  free(deriv5);
  free(deriv6);

  return Solution;
}



/*** Rkf: propagates vin (of size n) from tin to tout by the Runge-Kutta ***
 *        Fehlberg method, which is a the original adaptive-stepsize Rk    *
 *        method (Cash-Karp is an improved version of this); it uses a     *
 *        fifth-order Rk formula with an embedded forth-oder formula for   *
 *        calucalting the error; its result is returned by vout            *
 ***************************************************************************
 *                                                                         *
 * This solver was written by Marcel Wolf, Spring 2002.                    *
 *                                                                         *
 ***************************************************************************/

ODESolutionPoly*Rkf(double *vin, double *vout, double tin, double tout,
	 double stephint, double accuracy, int n, FILE *slog)
{
  int i;                                             /* local loop counter */

  double **v;                       /* used for storing intermediate steps */
  int toggle = 0;                  /* used to toggle between v[0] and v[1] */

  double *vtemp;                               /* guessed intermediate v's */

  double *vnow;                                /* ptr to v at current time */
  double *vnext;                    /* ptr to v at current time + stepsize */

  double *verror;                                        /* error estimate */
  double verror_max;                        /* the maximum error in verror */

  double *deriv1;     /* intermediate derivatives for the Fehlberg formula */
  double *deriv2;
  double *deriv3;
  double *deriv4;
  double *deriv5;
  double *deriv6;

  double t;                                            /* the current time */

  double h = stephint;                                 /* initial stepsize */
  double hnext;                         /* used to calculate next stepsize */
  const double SAFETY = 0.9;      /* safety margin for decreasing stepsize */



/* declare and initialize Fehlberg parameters */
/* parameters that are zero have been added as comments for clarity */

  static double
    a2 =      0.25,
    a3 =      0.375,
    a4 =     12.0/13.0,
    a5 =      1.0,
    a6 =      0.5;

  static double
    b21 =     0.25,
    b31 =     3.0/32.0,
    b32 =     9.0/32.0,
    b41 =  1932.0/2197.0,
    b42 = -7200.0/2197.0,
    b43 =  7296.0/2197.0,
    b51 =   439.0/216.0,
    b52 =    -8.0,
    b53 =  3680.0/513.0,
    b54 =  -845.0/4104.0,
    b61 =    -8.0/27.0,
    b62 =     2.0,
    b63 = -3544.0/2565.0,
    b64 =  1859.0/4104.0,
    b65 =   -11.0/40.0;

  static double
    c1 =     25.0/216.0,
/*  c2  =     0.0 */
    c3 =   1408.0/2565.0,
    c4 =   2197.0/4104.0,
    c5 =     -0.2;
/*  c6  =     0.0 */

  static double
    dc1 =     1.0/360.0,
/*  dc2 =     0.0 */
    dc3 =  -128.0/4275.0,
    dc4 = -2197.0/75240.0,
    dc5 =     1.0/50.0,
    dc6 =     2.0/55.0;

  ODESolutionPoly *Solution;

/* the do-nothing case; too small steps dealt with under usual */

  if (tin == tout)
    return NULL;

  Solution = ODESolutionPoly_createWith(tin, tout, n);

/* the usual case: steps big enough */

  v      = (double **)calloc(2, sizeof(double *));

  vtemp  = (double *)calloc(n, sizeof(double));
  verror = (double *)calloc(n, sizeof(double));
  v[0]   = (double *)calloc(n, sizeof(double));
  v[1]   = (double *)calloc(n, sizeof(double));
  deriv1 = (double *)calloc(n, sizeof(double));
  deriv2 = (double *)calloc(n, sizeof(double));
  deriv3 = (double *)calloc(n, sizeof(double));
  deriv4 = (double *)calloc(n, sizeof(double));
  deriv5 = (double *)calloc(n, sizeof(double));
  deriv6 = (double *)calloc(n, sizeof(double));
/*
  ODESolutionPoly_setStepsize(Solution,stepsize);
  ODESolutionPoly_setNSteps(Solution,nsteps);
*/
  ODESolutionPoly_initInterpolating(Solution,vin);

  t     = tin;
  vnow  = vin;
  vnext = v[0];

/* initial stepsize cannot be bigger than total time */

  if (tin + h >= tout)
    h = tout - tin;

  while (t < tout) {

/* Take one step and evaluate the error. Repeat until the resulting error  *
 * is less than the desired accuracy                                       */

    while(1) {

/* do the Runge-Kutta thing here: calulate intermediate derivatives */

      p_deriv(vnow, t, deriv1, n);

      for (i=0; i<n; i++)
	vtemp[i] = vnow[i] + h
	  * (b21 * deriv1[i]);
      p_deriv(vtemp, t + a2 * h, deriv2, n);

      for (i=0; i<n; i++)
	vtemp[i] = vnow[i] + h
	  * (b31 * deriv1[i] + b32 * deriv2[i]);
      p_deriv(vtemp, t + a3 * h, deriv3, n);

      for (i=0; i<n; i++)
	vtemp[i] = vnow[i] + h
	  * (b41 * deriv1[i] + b42 * deriv2[i] + b43 * deriv3[i]);
      p_deriv(vtemp, t + a4 * h, deriv4, n);

      for (i=0; i<n; i++)
	vtemp[i] = vnow[i] + h
	  * (b51 * deriv1[i] + b52 * deriv2[i] + b53 * deriv3[i]
	   + b54 * deriv4[i]);
      p_deriv(vtemp, t + a5 * h, deriv5, n);

      for (i=0; i<n; i++)
	vtemp[i] = vnow[i] + h
	  * (b61 * deriv1[i] + b62 * deriv2[i] + b63 * deriv3[i]
	   + b64 * deriv4[i] + b65 * deriv5[i]);
      p_deriv(vtemp, t + a6 * h, deriv6, n);

/* ... then feed them to the Fehlberg formula */

      for (i=0; i<n; i++)
	vnext[i] = vnow[i]
	  + h * (c1 * deriv1[i] + c3 * deriv3[i] + c4 * deriv4[i]
	       + c5 * deriv5[i]);

/* calculate the error estimate using the embedded formula */

      for (i=0; i<n; i++)
	verror[i] = fabs(h * (dc1 * deriv1[i] + dc3 * deriv3[i]
			    + dc4 * deriv4[i] + dc5 * deriv5[i]
                            + dc6 * deriv6[i]));

/* find the maximum error */

      verror_max = 0.;
      for (i=0; i<n; i++)
	if ( vnext[i] != 0. )
	  verror_max = DMAX(verror[i]/vnext[i], verror_max);
	else
	  verror_max = DMAX(verror[i]/DBL_EPSILON, verror_max);

/* scale error according to desired accuracy */

      verror_max /= accuracy;

/* compare maximum error to the desired accuracy; if error < accuracy, we  *
 * are done with this step; otherwise, the stepsize has to be reduced and  *
 * the step repeated; for detailed comments on the approximation involving *
 * SAFETY and -0.25 see 'Numerical Recipes in C', 2nd Edition, p.718       */

      if (verror_max <= 1.0)
	break;

      hnext = SAFETY * h * pow(verror_max, -0.25);

/* decrease stepsize by no more than a factor of 10; check for underflows */

      h = (hnext > 0.1 * h) ? hnext : 0.1 * h;
      if (h == 0)
	error("Rkf: stepsize underflow");

    }

/* advance the current time by last stepsize */

    t += h;

    ODESolutionPoly_addtoInterpolating(Solution,vnext,t);

    if (t >= tout)
      break;                                /* that was the last iteration */

/* increase stepsize according to error for next iteration */

    h = h * pow(verror_max, -0.20);

/* make sure t does not overstep tout */

    if (t + h >= tout)
      h = tout - t;

/* toggle vnow and vnext between v[0] and v[1] */

    vnow = v[toggle];
    toggle++;
    toggle %= 2;
    vnext = v[toggle];

  }

/* copy the last result to vout after the final iteration */

  memcpy(vout, vnext, sizeof(*vnext) * n);

  ODESolutionPoly_makeInterpolating(Solution);

  free(v[0]);
  free(v[1]);
  free(v);
  free(vtemp);
  free(verror);
  free(deriv1);
  free(deriv2);
  free(deriv3);
  free(deriv4);
  free(deriv5);
  free(deriv6);

  return Solution;
}




/***** Milne: propagates vin (of size n) from tin to tout by Milne-Simpson *
 *            which is a predictor-corrector method; the result is retur-  *
 *            ned by vout                                                  *
 ***************************************************************************
 *                                                                         *
 * This solver was implemented by Konstantin Koslov, Dec 2001/Jan 2002     *
 *                                                                         *
 ***************************************************************************/

ODESolutionPoly*Milne(double *vin, double *vout, double tin, double tout,
	   double stephint, double accuracy, int n, FILE *slog)
{
  int i;                                             /* local loop counter */

  double **v;                       /* array to store intermediate results */
  int toggle = 0;                  /* used to toggle between v[0] and v[1] */

  double *vtemp;                               /* guessed intermediate v's */

  double *vnow;                                   /* current protein concs */
  double *vnext;                                     /* next protein concs */

  double *deriv1;            /* the derivatives at the beginning of a step */
  double *deriv2;                        /* derivatives at test-midpoint 1 */
  double *deriv3;                        /* derivatives at test-midpoint 2 */
  double *deriv4;                          /* derivatives at test-endpoint */

  double **history_dv;                       /* history of the derivatives */
  double **history_v;                 /* history of the v: v_{i-3} v_{i-1} */
  double *dblank;                       /* pointer for history_dv rotation */

  double deltat;                                             /* tout - tin */
  double t;                                                /* current time */
  double th;                                    /* time at end of interval */
  double thh;                               /* time for interval midpoints */

  double m;                           /* used to calculate number of steps */
  double stepsize;                                        /* real stepsize */
  double hh;                                          /* half the stepsize */
  double h6;                /* one sixth of the stepsize (for Rk4 formula) */
  int    step;                                   /* loop counter for steps */
  int    nsteps;                        /* number of steps we have to take */

  double hp;                        /* multiplier for the predictor derivs */
  double hc;                        /* multiplier for the corrector derivs */

  double mistake;                                       /* error estimator */
  double corr;                          /* temp variable for the corrector */
  double ee;                      /* temp variable for the error estimator */
  double cc29;                            /* 1/29: used for calculating ee */

  int    nd = 0;                            /* number of deriv evaluations */

  ODESolutionPoly *Solution;

/* the do-nothing case; too small steps dealt with under usual */

  if (tin == tout)
    return NULL;

  Solution = ODESolutionPoly_createWith(tin, tout, n);

/* the usual case: steps big enough */

  v      = (double **)calloc(2, sizeof(double *));

  vtemp  = (double *)calloc(n, sizeof(double));
  v[0]   = (double *)calloc(n, sizeof(double));
  v[1]   = (double *)calloc(n, sizeof(double));
  deriv1 = (double *)calloc(n, sizeof(double));
  deriv2 = (double *)calloc(n, sizeof(double));
  deriv3 = (double *)calloc(n, sizeof(double));
  deriv4 = (double *)calloc(n, sizeof(double));

  deltat = tout - tin;                 /* how far do we have to propagate? */
  m = floor(fabs(deltat)/stephint + 0.5);       /* see comment on stephint above */

  if ( m < 1. )
    m = 1.;                          /* we'll have to do at least one step */

  stepsize = deltat / m;                  /* real stepsize calculated here */
  nsteps = (int)m;                                  /* int number of steps */
  t = tin;                                             /* set current time */

  if ( t == t + stepsize )
    error("Milne: stephint of %g too small!", stephint);

  ODESolutionPoly_setStepsize(Solution,stepsize);
  ODESolutionPoly_setNSteps(Solution,nsteps);

  ODESolutionPoly_initInterpolating(Solution,vin);

  vnow = vin;

  if (nsteps == 1)              /* vnext holds the results of current step */
    vnext = vout;
  else
    vnext = v[0];

  hh  = stepsize * 0.5;
  h6  = stepsize / 6.0;

/* do we need Milne? no, not for less than 4 steps! */

 if (nsteps < 4) {

    for (step=0; step < nsteps; step++) {                /* loop for steps */

      thh = t + hh;                          /* time of interval midpoints */
      th  = t + stepsize;                   /* time at end of the interval */

/* do the Runge-Kutta thing here: first calculate intermediate derivatives */

      p_deriv(vnow, t, deriv1, n);

      if ( debug ) nd++;

      for(i=0; i<n; i++)
	vtemp[i] = vnow[i] + hh * deriv1[i];
      p_deriv(vtemp, thh, deriv2, n);

      if ( debug ) nd++;

      for(i=0; i<n; i++)
	vtemp[i] = vnow[i] + hh * deriv2[i];
      p_deriv(vtemp, thh, deriv3, n);

      if ( debug ) nd++;

      for(i=0; i<n; i++)
	vtemp[i] = vnow[i] + stepsize * deriv3[i];
      p_deriv(vtemp, th, deriv4, n);

      if ( debug ) nd++;


/* ... then feed them to the Fourth-Order Runge-Kutta formula */

      for(i=0; i<n; i++)
	vnext[i] = vnow[i]
	  + h6 * (deriv1[i] + 2.0 * deriv2[i] + 2.0 * deriv3[i] + deriv4[i]);

/* next step */

      t += stepsize;

    ODESolutionPoly_addtoInterpolating(Solution,vnext,t);

      if (step < nsteps - 2) {                 /* CASE 1: many steps to go */
	vnow = v[toggle];             /* toggle results from v[0] and v[1] */
	toggle++;
	toggle %= 2;
	vnext = v[toggle];

      } else if (step == nsteps - 2) {   /* CASE 2: next iteration = final */
	vnow = v[toggle];                                      /* set vout */
	vnext = vout;

      } else if (step > nsteps - 2) {  /* CASE 3: just did final iteration */
	free(v[0]);                               /* clean up and go home! */
	free(v[1]);
	free(v);
	free(deriv1);
	free(deriv2);
	free(deriv3);
	free(deriv4);
	free(vtemp);
      }
    }

/* more than 3 steps: yes, we need Milne */

  } else {

    history_dv = (double **)calloc(3, sizeof(double *));
    history_v  = (double **)calloc(4, sizeof(double *));

    for (step=0; step < 3; step++) {
      history_dv[step] = (double *)calloc(n, sizeof(double ));}

    for (step=0; step < 4; step++) {
      history_v[step]  = (double *)calloc(n, sizeof(double ));}

    mistake = 0.;
    cc29    = 1. / 29.;

/* loop for initial steps using Rk4 */

    for (step=0; step < 3; step++) {

      thh = t + hh;                          /* time of interval midpoints */
      th  = t + stepsize;                   /* time at end of the interval */

/* do the Runge-Kutta thing here: first calculate intermediate derivatives */

      p_deriv(vnow, t, deriv1, n);

      if ( debug ) nd++;

      if (step > 0)
	for(i=0; i<n; i++)
	  history_dv[step-1][i] = deriv1[i];

      for(i=0; i<n; i++)
	vtemp[i] = vnow[i] + hh * deriv1[i];
      p_deriv(vtemp, thh, deriv2, n);

      if ( debug ) nd++;

      for(i=0; i<n; i++)              /* evaluate deriv at second midpoint */
	vtemp[i] = vnow[i] + hh * deriv2[i];
      p_deriv(vtemp, thh, deriv3, n);

      if ( debug ) nd++;

      for(i=0; i<n; i++)                    /* evaluate deriv at end point */
	vtemp[i] = vnow[i] + stepsize * deriv3[i];
      p_deriv(vtemp, th, deriv4, n);

      if ( debug ) nd++;

/* ... then feed them to the Fourth-Order Runge-Kutta formula */

      for(i=0; i<n; i++)
	vnext[i] = vnow[i]
	  + h6 * (deriv1[i] + 2.0 * deriv2[i] + 2.0 * deriv3[i] + deriv4[i]);

/* next step */

      t += stepsize;

    ODESolutionPoly_addtoInterpolating(Solution,vnext,t);

      for(i=0; i<n; i++)                 /* save old v values in history_v */
	history_v[step][i] = vnow[i];

      if (step == 1) {
	vnow  = vnext;
	vnext = vout;
      } else {
	vnow = v[toggle];             /* toggle results from v[0] and v[1] */
	toggle++;
	toggle %= 2;
	vnext = v[toggle];
      }
    }

/* we have calculated 4 initial points with rk4: start multistepping */

    hc = stepsize / 3.0;        /* stepsize multipliers: for the corrector */
    hp = 4. * hc;                             /* ... and for the predictor */

    for(i=0; i<n; i++)            /* save current values of v in history_v */
      history_v[3][i] = vout[i];

    for (step=3; step < nsteps; step++) {          /* loop for Milne steps */

      th  = t + stepsize;                   /* time at end of the interval */

/* do the Milne thing here */

      p_deriv(vout, t, deriv1, n);  /* evaluate deriv at start of interval */

      if ( debug ) nd++;

      for(i=0; i<n; i++)              /* save current derivs in history_dv */
	history_dv[2][i] = deriv1[i];

/* Predictor step (P): extrapolate derivatives */

      for(i=0; i<n; i++)
	v[0][i] = history_v[0][i]
	  + hp * (2 * history_dv[0][i] - history_dv[1][i]
                + 2 * history_dv[2][i]);

/* Evaluate the extrapolated derivatives (E) */

      p_deriv(v[0], th, deriv1, n);

      if ( debug ) nd++;

/* Corrector step (C): interpolate current v (+ calculate error estimator) */

      for (i=0; i<n; i++) {

	corr = history_v[2][i]
	  + hc * (history_dv[1][i] + 4 * history_dv[2][i] + deriv1[i]);
	ee = fabs(corr - v[0][i]) * cc29;               /* error estimator */

	if (ee > mistake)
	  mistake = ee;
	vout[i] = corr;

      }

      t += stepsize;                                    /* go to next step */

    ODESolutionPoly_addtoInterpolating(Solution,vout,t);

/* update the arrays */

      if (step <= nsteps - 2) {                /* CASE 1: many steps to go */

	dblank        = history_dv[0];
	history_dv[0] = history_dv[1];
	history_dv[1] = history_dv[2];
	history_dv[2] = dblank;

	dblank       = history_v[0];
	history_v[0] = history_v[1];
	history_v[1] = history_v[2];
	history_v[2] = history_v[3];
	history_v[3] = dblank;

	for(i=0;i<n;i++)
	  history_v[3][i]=vout[i];

      } else {                         /* CASE 2: just did final iteration */

	free(v[0]);                               /* clean up and go home! */
	free(v[1]);
	free(v);
	free(history_v[0]);
	free(history_v[1]);
	free(history_v[2]);
	free(history_v[3]);
	free(history_v);
	free(history_dv[0]);
	free(history_dv[1]);
	free(history_dv[2]);
	free(history_dv);
	free(deriv1);
	free(deriv2);
	free(deriv3);
	free(deriv4);
	free(vtemp);
      }
    }

/* use below for sanity check if Milne behaves unstably */

/*
    fprintf(stdout,"mistake=%.15f\n", mistake);
    fflush(stdout);
    getchar();
*/

  }

  ODESolutionPoly_makeInterpolating(Solution);

  if ( debug )
    WriteSolvLog("Milne", tin, tout, stepsize, nsteps, nd, slog);

  return Solution;

}



/***** Adams: propagates vin (of size n) from tin to tout by Adams-Moulton *
 *            which is an implicit predictor-corrector method of second    *
 *            order; the result is returned by vout                        *
 ***************************************************************************
 *                                                                         *
 * This solver was implemented by Konstantin Koslov, Spring 2002           *
 * Slightly modified by Manu, July 2002                                    *
 *                                                                         *
 ***************************************************************************/

ODESolutionPoly*Adams(double *vin, double *vout, double tin, double tout,
	   double stephint, double accuracy, int n, FILE *slog)
{
  int i;                                             /* local loop counter */

  double **v;                       /* array to store intermediate results */
  int toggle = 0;                  /* used to toggle between v[0] and v[1] */

  double *vtemp;                               /* guessed intermediate v's */

  double *vnow;                                   /* current protein concs */
  double *vnext;                                     /* next protein concs */

  double *deriv1;            /* the derivatives at the beginning of a step */
  double *deriv2;                        /* derivatives at test-midpoint 1 */
  double *deriv3;                        /* derivatives at test-midpoint 2 */
  double *deriv4;                          /* derivatives at test-endpoint */

  double **history_dv;                       /* history of the derivatives */
  double *dblank;                       /* pointer for history_dv rotation */

  double deltat;                                             /* tout - tin */
  double t;                                                /* current time */
  double th;                                    /* time at end of interval */
  double thh;                               /* time for interval midpoints */

  double m;                           /* used to calculate number of steps */
  double stepsize;                                        /* real stepsize */
  double hh;                                          /* half the stepsize */
  double h6;                /* one sixth of the stepsize (for Rk4 formula) */
  int    step;                                   /* loop counter for steps */
  int    nsteps;                        /* number of steps we have to take */

  double mistake;                                      /* error estimators */
  double eps1;
  double eps2;

  int    nd = 0;                            /* number of deriv evaluations */

  ODESolutionPoly *Solution;

/* the do-nothing case; too small steps dealt with under usual */

  if (tin == tout)
    return NULL;

  Solution = ODESolutionPoly_createWith(tin, tout, n);

/* the usual case: steps big enough */

  v      = (double **)calloc(2, sizeof(double *));

  vtemp  = (double *)calloc(n, sizeof(double));
  v[0]   = (double *)calloc(n, sizeof(double));
  v[1]   = (double *)calloc(n, sizeof(double));
  deriv1 = (double *)calloc(n, sizeof(double));
  deriv2 = (double *)calloc(n, sizeof(double));
  deriv3 = (double *)calloc(n, sizeof(double));
  deriv4 = (double *)calloc(n, sizeof(double));

  deltat = tout - tin;                 /* how far do we have to propagate? */
  m = floor(fabs(deltat)/stephint + 0.5);       /* see comment on stephint above */

  if ( m < 1. )
    m = 1.;                          /* we'll have to do at least one step */

  stepsize = deltat / m;                  /* real stepsize calculated here */
  nsteps = (int)m;                                  /* int number of steps */
  t = tin;                                             /* set current time */

  if ( t == t + stepsize )
    error("Adams: stephint of %g too small!", stephint);

  ODESolutionPoly_setStepsize(Solution,stepsize);
  ODESolutionPoly_setNSteps(Solution,nsteps);

  ODESolutionPoly_initInterpolating(Solution,vin);

  vnow = vin;

  if(nsteps == 1)               /* vnext holds the results of current step */
    vnext = vout;
  else
    vnext = v[0];

  hh  = stepsize * 0.5;
  h6  = stepsize / 6.0;

/* do we need Adams? no, not for less than 4 steps! */

  if (nsteps < 4) {

    for (step=0; step < nsteps; step++) {                /* loop for steps */

      thh = t + hh;                          /* time of interval midpoints */
      th  = t + stepsize;                   /* time at end of the interval */

/* do the Runge-Kutta thing here: first calculate intermediate derivatives */

      p_deriv(vnow, t, deriv1, n);

      if ( debug ) nd++;

      for(i=0; i<n; i++)
	vtemp[i] = vnow[i] + hh * deriv1[i];
      p_deriv(vtemp, thh, deriv2, n);

      if ( debug ) nd++;

      for(i=0; i<n; i++)
	vtemp[i] = vnow[i] + hh * deriv2[i];
      p_deriv(vtemp, thh, deriv3, n);

      if ( debug ) nd++;

      for(i=0; i<n; i++)
	vtemp[i] = vnow[i] + stepsize * deriv3[i];
      p_deriv(vtemp, th, deriv4, n);

      if ( debug ) nd++;

/* ... then feed them to the Fourth-Order Runge-Kutta formula */

      for(i=0; i<n; i++)
	vnext[i] = vnow[i]
	  + h6 * (deriv1[i] + 2.0 * deriv2[i] + 2.0 * deriv3[i] + deriv4[i]);

/* next step */

      t += stepsize;

    ODESolutionPoly_addtoInterpolating(Solution,vnext,t);

      if(step < nsteps - 2) {                  /* CASE 1: many steps to go */
	vnow = v[toggle];             /* toggle results from v[0] and v[1] */
	toggle++;
	toggle %= 2;
	vnext = v[toggle];

      } else if (step == nsteps - 2) {   /* CASE 2: next iteration = final */
	vnow = v[toggle];                                      /* set vout */
	vnext = vout;

      } else if (step > nsteps - 2) {  /* CASE 3: just did final iteration */
	free(v[0]);                               /* clean up and go home! */
	free(v[1]);
	free(v);
	free(vtemp);
	free(deriv1);
	free(deriv2);
	free(deriv3);
	free(deriv4);
      }
    }

  } else {

/* more than 3 steps: yes, we need Adams*/

    history_dv = (double **)calloc(4, sizeof(double *));
    for (step=0; step < 4; step++) {
      history_dv[step] = (double *)calloc(n, sizeof(double ));}

    mistake = 0.;

/* loop for initial steps using Rk4 */

    for (step=0; step < 3; step++) {

      thh = t + hh;                          /* time of interval midpoints */
      th  = t + stepsize;                   /* time at end of the interval */

/* do the Runge-Kutta thing here: first calculate intermediate derivatives */

      p_deriv(vnow, t, deriv1, n);

      if ( debug ) nd++;

      for(i=0; i<n; i++)
	history_dv[step][i] = deriv1[i];

      for(i=0; i<n; i++)
	vtemp[i] = vnow[i] + hh * deriv1[i];
      p_deriv(vtemp, thh, deriv2, n);

      if ( debug ) nd++;

      for(i=0; i<n; i++)
	vtemp[i] = vnow[i] + hh * deriv2[i];
      p_deriv(vtemp, thh, deriv3, n);

      if ( debug ) nd++;

      for(i=0; i<n; i++)
	vtemp[i] = vnow[i] + stepsize * deriv3[i];
      p_deriv(vtemp, th, deriv4, n);

      if ( debug ) nd++;

/* ... then feed them to the Fourth-Order Runge-Kutta formula */

      for(i=0; i<n; i++)
	vnext[i] = vnow[i]
	  + h6 * (deriv1[i] + 2.0 * deriv2[i] + 2.0 * deriv3[i] + deriv4[i]);

/* next step */

      t += stepsize;

    ODESolutionPoly_addtoInterpolating(Solution,vnext,t);

      vnow = v[toggle];               /* toggle results from v[0] and v[1] */
      toggle++;
      toggle %= 2;
      vnext = v[toggle];

    }

/* we have calculated 4 initial points with rk4: start multistepping */

    hh  = stepsize / 24.0;                 /* reset hh to 1/24 of stepsize */

    if (nsteps == 4)
      vnext = vout;

    for (step=3; step < nsteps; step++) {                /* loop for steps */

      th  = t + stepsize;                   /* time at end of the interval */

/* do the Adams thing here */

      p_deriv(vnow, t, deriv1, n);  /* evaluate deriv at start of interval */

      if ( debug ) nd++;

      for(i=0; i<n; i++)
	history_dv[3][i] = deriv1[i];

      for(i=0; i<n; i++)
	vnext[i] = vnow[i]
	  + hh * (55 * history_dv[3][i] - 59 * history_dv[2][i]
		+ 37 * history_dv[1][i] -  9 * history_dv[0][i]);

      p_deriv(vnext, th, deriv1, n);

      if ( debug ) nd++;


/* the following evaluates the error, but then nothing is done with it */

/*    for(i=0; i<n; i++) {
	eps1 = hh * (           deriv1[i] - 3 * history_dv[3][i]
		   + 3 * history_dv[2][i] -     history_dv[1][i]);
	eps2 = fabs(eps1);
	if (eps2 > mistake)
	  mistake=eps2;
      }
*/
      for(i=0; i<n; i++)
	vnext[i] = vnow[i]
	  + hh * (9 *        deriv1[i] + 19 * history_dv[3][i]
		- 5 * history_dv[2][i] +      history_dv[1][i]);

      t += stepsize;                                    /* go to next step */

    ODESolutionPoly_addtoInterpolating(Solution,vnext,t);

      if (step <= nsteps - 2) {                /* CASE 1: many steps to go */
	dblank=history_dv[0];
	history_dv[0]=history_dv[1];
	history_dv[1]=history_dv[2];
	history_dv[2]=history_dv[3];
	history_dv[3]=dblank;

	if (step < nsteps - 2) {
	  vnow = v[toggle];           /* toggle results from v[0] and v[1] */
	  toggle++;
	  toggle %= 2;
	  vnext = v[toggle];

	} else if (step == nsteps - 2) { /* CASE 2: next iteration = final */
	  vnow = v[toggle];                                    /* set vout */
	  vnext = vout;
	}

      } else {                         /* CASE 2: just did final iteration */

	free(v[0]);                               /* clean up and go home! */
	free(v[1]);
	free(v);
	free(history_dv[0]);
	free(history_dv[1]);
	free(history_dv[2]);
	free(history_dv[3]);
	free(history_dv);
	free(deriv1);
	free(deriv2);
	free(deriv3);
	free(deriv4);
	free(vtemp);
      }
    }
  }

  ODESolutionPoly_makeInterpolating(Solution);

  if ( debug )
    WriteSolvLog("Adams", tin, tout, stepsize, nsteps, nd, slog);

  return Solution;

}

void ODESolve_SetAttrScoreBuffer(double*AttrScoreBuffer,int*AttrNDatapoitsBuffer)
{
	static_attr_score_buffer = AttrScoreBuffer;
	static_attr_ndatapoints_buffer = AttrNDatapoitsBuffer;
}

void ODESolve_SetAttrFeatures(double error, double time, double step)
{
	static_attr_time = time;
	static_attr_time_step = step;
	static_attr_accuracy = error;
}

void ODESolve_GetActualAttrTime(double *time)
{
	*time = static_actual_attr_time;
}

void ODESolve_ResetScoreBuffer()
{
	(*static_attr_score_buffer) = 0;
	(*static_attr_ndatapoints_buffer) = 0;
}

void ODESolve_FillScoreBuffer(double *vin, double *vout, int n)
{
	int i;
	register double dot = 0;
	for ( i = 0; i < n; i++) {
		register double r = vin[i] - vout[i];
		dot += r*r;
	}

	(*static_attr_score_buffer) += dot;
	(*static_attr_ndatapoints_buffer) += n;
}

/***** BuSt: propagates v(t) from t1 to t2 by Bulirsch-Stoer; this method **
 *           uses Richardson extrapolation to estimate v's at a hypothe-   *
 *           tical stepsize of 0; the extrapolation also yields an error   *
 *           estimate, which is used to adapt stepsize and change the or-  *
 *           der of the method as required; the result is returned by vout *
 ***************************************************************************
 *                                                                         *
 * This solver was implemented by Manu, July 2002                          *
 *                                                                         *
 ***************************************************************************/

ODESolutionPoly*BuStAttr(double *vin, double *vout, double tin, double tout,
	 double stephint, double accuracy, int n, FILE *slog)
{

double AttrError(double*vout, double*vstep, int n);

/* bsstep is the BuSt stepper function */

  void bsstep(double *v, double *deriv, int n, double *t, double htry,
	      double accuracy, double *hdid, double *hnext);

  int    i;                                          /* local loop counter */

  double t;                                                /* current time */

  double ht;                                      /* stepsize of next step */
  double hd;                               /* stepsize of step we just did */

  double *initderiv;                      /* array for initial derivatives */

  ODESolutionPoly *Solution;
	double *vstep;
	double attr_time;
	double step;
	double rror;

/* the do-nothing case; too small steps dealt with under usual */

  if (tin == tout)
    return NULL;

  Solution = ODESolutionPoly_createWith(tin, tout, n);

/* the usual case: steps big enough -> initialize variables */

  initderiv = (double *)calloc(n, sizeof(double));
  vstep = (double *)calloc(n, sizeof(double));

  ODESolutionPoly_initInterpolating(Solution,vin);

  t  = tin;

/* initial stepsize is either stephint or tout-tin */

  ht = DMIN(stephint, (tout - tin));
/*
fprintf(stdout,"\n iht %30.20f",ht);
fflush(stdout);
*/
/* we're saving old v's in vin and propagate vout below */

  for (i=0; i<n; i++)
    vout[i] = vin[i];

	for (i=0; i<n; i++)
		vstep[i] = vout[i];
	attr_time = 0;
	step = static_attr_time_step;
	static_actual_attr_time = t;
	ODESolve_ResetScoreBuffer();

/* this loop steps through the whole interval tout-tin */
/*fprintf(stdout,"\n bs %6.3f",t);*/
  do {

    p_deriv(vout, t, initderiv, n);
    bsstep(vout, initderiv, n, &t, ht, accuracy, &hd, &ht);

    ODESolutionPoly_addtoInterpolating(Solution,vout,t);
/*
fprintf(stdout,"\n %6.3f %8.3f %8.3f %8.3f %8.3f",t,vout[10],vout[12],initderiv[10],initderiv[12]);
*/

	step -= hd;
	if ( step < 0 ) {
		step = static_attr_time_step;
		ODESolve_FillScoreBuffer(vin, vout, n);
	}

	rror =  AttrError(vout, vstep, n);

	if ( attr_time > 0 && rror < static_attr_accuracy ) {
		attr_time += hd;
	}
	if ( attr_time == 0 && rror < static_attr_accuracy ) {
		attr_time += hd;
		for (i=0; i<n; i++)
			vstep[i] = vout[i];
	}
	if ( attr_time == 0 && rror >= static_attr_accuracy ) {
		static_actual_attr_time = t;
		for (i=0; i<n; i++)
			vstep[i] = vout[i];
	}
	if ( attr_time > 0 && rror >= static_attr_accuracy ) {
		attr_time = 0;
		static_actual_attr_time = t;
		for (i=0; i<n; i++)
			vstep[i] = vout[i];
	}


    if (t + ht > tout)
      ht = tout - t;
/*
fprintf(stdout,"hd %10.5f ht %8.3f e %10.5f at %10.5f t %10.5f",hd,ht,rror,attr_time,t);
fflush(stdout);
*/

  } while ( t < tout && attr_time < static_attr_time );

	if ( t < tout ) {
		ODESolutionPoly_addtoInterpolating(Solution,vout,tout);
	}

	if ( step != static_attr_time_step) {
		ODESolve_FillScoreBuffer(vin, vout, n);
	}

/* clean up and go home */

  free(initderiv);
	free(vstep);

  ODESolutionPoly_makeInterpolating(Solution);

  return Solution;
}

double AttrError(double*vout, double*vstep, int n)
{
	register double max = 0;
	int i;
	for ( i = 0; i < n; i++) {
		register double dot = fabs(vout[i] - vstep[i]);
		if ( max < dot )  max = dot;
	}
	return max;
}

/***** BuSt: propagates v(t) from t1 to t2 by Bulirsch-Stoer; this method **
 *           uses Richardson extrapolation to estimate v's at a hypothe-   *
 *           tical stepsize of 0; the extrapolation also yields an error   *
 *           estimate, which is used to adapt stepsize and change the or-  *
 *           der of the method as required; the result is returned by vout *
 ***************************************************************************
 *                                                                         *
 * This solver was implemented by Manu, July 2002                          *
 *                                                                         *
 ***************************************************************************/

ODESolutionPoly*BuSt(double *vin, double *vout, double tin, double tout,
	 double stephint, double accuracy, int n, FILE *slog)
{

/* bsstep is the BuSt stepper function */

  void bsstep(double *v, double *deriv, int n, double *t, double htry,
	      double accuracy, double *hdid, double *hnext);

  int    i;                                          /* local loop counter */

  double t;                                                /* current time */

  double ht;                                      /* stepsize of next step */
  double hd;                               /* stepsize of step we just did */

  double *initderiv;                      /* array for initial derivatives */

  ODESolutionPoly *Solution;

/* the do-nothing case; too small steps dealt with under usual */

  if (tin == tout)
    return NULL;

  Solution = ODESolutionPoly_createWith(tin, tout, n);

/* the usual case: steps big enough -> initialize variables */

  initderiv = (double *)calloc(n, sizeof(double));
/*
  ODESolutionPoly_setStepsize(Solution,stepsize);
  ODESolutionPoly_setNSteps(Solution,nsteps);
*/
  ODESolutionPoly_initInterpolating(Solution,vin);

  t  = tin;

/* initial stepsize is either stephint or tout-tin */

  ht = DMIN(stephint, (tout - tin));

/* we're saving old v's in vin and propagate vout below */

  for (i=0; i<n; i++)
    vout[i] = vin[i];

/* this loop steps through the whole interval tout-tin */
/*fprintf(stdout,"\n bs %6.3f",t);*/
  do {

    p_deriv(vout, t, initderiv, n);
    bsstep(vout, initderiv, n, &t, ht, accuracy, &hd, &ht);

    ODESolutionPoly_addtoInterpolating(Solution,vout,t);
/*
fprintf(stdout,"\n %6.3f %8.3f %8.3f",t,vout[84],vout[79]);
*/
    if (t + ht > tout)
      ht = tout - t;

  } while (t < tout);

/* clean up and go home */

  free(initderiv);

  ODESolutionPoly_makeInterpolating(Solution);

  return Solution;
}


/*** bsstep: this is the Bulirsch-Stoer stepper function; it propagates ****
 *           the equations at a given overall stepsize and order, then     *
 *           evaluates the error and repeats the step with increased order *
 *           if necessary, until the required accuracy is achieved; its    *
 *           arguments are:                                                *
 *           - v:        input and output v's over a give total stepsize   *
 *           - deriv:    array of initial derivatives (at tin)             *
 *           - n:        size of v and deriv                               *
 *           - t:        pointer to the current time                       *
 *           - htry:     initial stepsize to be tried                      *
 *           - accuracy: relative accuracy to be achieved                  *
 *           - hdid:     returns the stepsize of the step we just did      *
 *           - hnext:    returns the stepsize of the next step to be done  *
 ***************************************************************************/

void bsstep(double *v, double *deriv, int n, double *t, double htry,
	    double accuracy, double *hdid, double *hnext)
{

/* func prototypes: these funcs should not be visible outside solvers.c    *
 *            mmid: implements the modified midpoint method                *
 *          pzextr: implements the Richardson extrapolation (polynomial)   */

  void mmid(double *vin, double *vout, double *deriv, double tin,
	    double htot, int nstep, int n);

  void pzextr(int iest, double hest, double *yest,
	      double *yz, double *dy, int nv, const int KMAXX);

/*** constants *************************************************************/

  const int    KMAXX  = 8;         /* this is how many successive stepsize */
  const int    IMAXX  = KMAXX + 1;       /* refinements we're going to try */

  const double SAFE1  = 0.25; /* safety margins for errors and h reduction */
  const double SAFE2  = 0.7;

  const double REDMAX = 1.0e-5;       /* boundaries for stepsize reduction */
  const double REDMIN = 0.7;

  const double SCALMX = 0.1;         /* maximum scaling value for stepsize */

/*** variables *************************************************************/

  int i, k, km;                                           /* loop counters */

  double *vsav;                            /* v's at beginning of interval */
  double *vseq;             /* v's at end of interval for a given stepsize */

  double *verror;                                       /* error estimates */
  double verror_max;                        /* the maximum error in verror */
  double *err;                               /* normalized error estimates */

  double h;                       /* stepsize for solver method used below */
  double hest;             /* square of h passed to extrapolation function */
  double red;                    /* reduction factor for reducing stepsize */
  double scale;                        /* scaling factor for next stepsize */

  double work;            /* temp vars for calculating row for convergence */
  double wrkmin;
  double fact;

  static double old_accuracy = -1.0;          /* used to save old accuracy */
  static double tnew;                       /* used to save old start time */

/* the following two arrays are used for Deuflhard's error estimation; a   *
 * contains the work coefficients and alf (alpha) the correction factors   */

  static double *a = NULL;
  static double **alf = NULL;

  double accuracy1;         /* error (< accuracy) used to calculate alphas */

  static int kmax;                                /* used for finding kopt */
  static int kopt;                   /* optimal row number for convergence */

/* sequence of separate attempts to cross interval htot with increasing    *
 * values of nsteps as suggested by Deuflhard (Num Rec, p. 726)            */

  static int nseq[] = {0,2,4,6,8,10,12,14,16,18};

/* miscellaneous flags */

  static int first = 1;         /* is this the first try for a given step? */
  int reduct;           /* flag indicating if we have reduced stepsize yet */
  int exitflag = 0;                     /* exitflag: when set, we exit (!) */

/* static global arrays */

  extern double *d;                /* D's used for extrapolation in pzextr */
  extern double *hpoints;        /* stepsizes h (=H/n) which we have tried */

/* allocate arrays */

  if ( !(err = (double *)calloc(KMAXX, sizeof(double))))
    error("BuSt: error allocating err.\n");

  if ( !(verror = (double *)calloc(n, sizeof(double))))
    error("BuSt: error allocating verror.\n");

  if ( !(vsav = (double *)calloc(n, sizeof(double))))
    error("BuSt: error allocating vsav.\n");

  if ( !(vseq = (double *)calloc(n, sizeof(double))))
    error("BuSt: error allocating vseq.\n");

  if ( !(d = (double *)calloc(KMAXX * n, sizeof(double))))
    error("BuSt: error allocating d.\n");

  if ( !(hpoints = (double *)calloc(KMAXX, sizeof(double))))
    error("BuSt: error allocating hpoints.\n");

/* set initial stepsize to try to htry */

  h = htry;

/* new accuracy? -> initialize (here, this only applies at start) */

  if (accuracy != old_accuracy) {

    *hnext = tnew = -1.0e29;             /* init these to impossible value */

/* allocate memory if necessary */

    if ( !a ) {
      a = (double *)calloc(IMAXX+1, sizeof(double));
      alf = (double **)calloc(KMAXX+1, sizeof(double *));
      for (i=0; i<KMAXX+1; i++)
	alf[i] = (double *)calloc(KMAXX+1, sizeof(double));
    }

/* initialize the work coefficients */

    a[1]   = nseq[1] + 1;
    for (k=1; k<=KMAXX; k++)
      a[k+1] = a[k] + nseq[k+1];

/* initialize the correction factors (alpha) */

    accuracy1 = SAFE1 * accuracy;    /* accuracy1 used to calculate alphas */
    for (i=2; i<=KMAXX; i++)
      for (k=1; k<i; k++)
	alf[k][i] = pow(accuracy1, ((a[k+1] - a[i+1]) /
			 ((a[i+1] - a[1] + 1.0) * (2*k+1))));
    old_accuracy = accuracy;

/* determine optimal row number for convergence */

    for (kopt=2; kopt<KMAXX; kopt++)
      if (a[kopt+1] > a[kopt]*alf[kopt-1][kopt])
	break;
    kmax = kopt;

  }

/* actual stepping starts here: first save starting values of v[] in vsav  */

  for (i=0; i<n; i++)
    vsav[i] = v[i];

/* new integration or stepsize? -> re-establish the order window */

  if (*t != tnew || h != (*hnext)) {
    first = 1;
    kopt  = kmax;
  }

  reduct = 0;                          /* we have not reduced stepsize yet */

  for (;;) {

/* try increasing numbers of steps over the interval h */

    for (k=1; k<=kmax; k++) {

      tnew = (*t) + h;
      if (tnew == (*t))
	error("BuSt: stepsize underflow in bsstep\n");

/* propagate the equations from t to t+h with nseq[k] steps using vsav and *
 * deriv as input; mmid returns vseq                                       */

      mmid(vsav, vseq, deriv, *t, h, nseq[k], n);

/* extrapolate v's for h->0 based on the different stepsizes (hest's) we   *
 * have tried already; the result is returned in v (errors in verror);     *
 * hest is squared since the error series is even                          */

      hest = DSQR(h / nseq[k]);
      pzextr(k-1, hest, vseq, v, verror, n, KMAXX);

      if (k > 1) {

/* find the maximum error */

      verror_max = 0.;
      for (i=0; i<n; i++)
	if ( v[i] != 0. )
	  verror_max = DMAX(fabs(verror[i]/v[i]), verror_max);
	else
	  verror_max = DMAX(fabs(verror[i]/DBL_EPSILON), verror_max);

/* scale error according to desired accuracy */

	verror_max /= accuracy;

/* compute normalized error estimates (epsilons) */

	km = k-1;
	err[km-1] = pow(verror_max/SAFE1, 1.0/(2*km+1));

      }

/* are we in the order window? -> converged */

      if (k > 1 && (k >= kopt - 1 || first)) {

	if (verror_max < 1.0) {            /* exit if accuracy good enough */
	  exitflag = 1;
	  break;
	}

	if (k == kmax || k == kopt + 1) {  /* stepsize reduction possible? */
	  red = SAFE2 / err[km-1];
	  break;
	} else if (k == kopt && alf[kopt-1][kopt] < err[km-1]) {
	  red = 1.0 / err[km-1];
	  break;
	} else if (kopt == kmax && alf[km][kmax-1] < err[km-1]) {
	  red = alf[km][kmax-1] * SAFE2 / err[km-1];
	  break;
	} else if (alf[km][kopt] < err[km-1]) {
	  red = alf[km][kopt-1] / err[km-1];
	  break;
	}
      }
    }

    if (exitflag)
      break;

/* reduce stepsize by at least REDMIN and at most REDMAX, then try again */

    red = DMIN(red, REDMIN);
    red = DMAX(red, REDMAX);

    h *= red;
    reduct = 1;
  }

/* we've taken a successful step */

  *t     = tnew;
  *hdid  = h;
  first  = 0;
  wrkmin = 1.0e35;

/* compute optimal row for convergence and corresponding stepsize */

  for (i=1; i <= km; i++) {

    fact = DMAX(err[i-1], SCALMX);
    work = fact * a[i+1];

    if (work < wrkmin) {

      scale  = fact;
      wrkmin = work;
      kopt   = i + 1;

    }
  }

  *hnext = h / scale;

/* check for possible order increase, but not if stepsize was just reduced */

  if (kopt >= k && kopt != kmax && !reduct) {

    fact = DMAX(scale / alf[kopt-1][kopt], SCALMX);

    if (a[kopt+1] * fact <= wrkmin) {
      *hnext = h / fact;
      kopt++;
    }
  }

/* clean up */

  free(d);
  free(hpoints);
  free(vseq);
  free(verror);
  free(err);
  free(vsav);
}



/*** pzextr: uses polynomial extrapolation (Neville's algorithm) to evalu- *
 *           ate v's at a the hypothetical stepsize 0; this is called Ri-  *
 *           chardson extrapolation; the arguments are:                    *
 *           - iest: how many steps have we done already before?           *
 *           - hest: current stepsize h                                    *
 *           - vest: v's obtained using current stepsize h                 *
 *           - vout: extrapolated v's that will be returned                *
 *           - dv:   array of error estimates to be returned               *
 *           - n:    size of verst, vout and dv arrays                     *
 *           Neville's algorithm uses a recursive approach to determine a  *
 *           suitable Lagrange polynomial for extrapolation by traversing  *
 *           a tableau of differences between Lagrange polynomials of in-  *
 *           creasing order; these differences are called C and D below    *
 *           (see Numerical Recipes in C, Chapter 3.1 for details)         *
 ***************************************************************************/

void pzextr(int iest, double hest, double *vest, double *vout, double *dv,
	    int n, const int KMAXX)
{
  int i, j;                                               /* loop counters */

  double q;                       /* temp vars for calculating C's and D's */
  double f1, f2;
  double delta;
  double *c;                                 /* C's used for extrapolation */

  extern double *d;                          /* D's used for extrapolation */
  extern double *hpoints;        /* stepsizes h (=H/n) which we have tried */

  if ( !(c = (double *)calloc(n, sizeof(double))))
    error("pzextr: error allocating c.\n");

  hpoints[iest] = hest;          /* stepsizes h (=H/n) which we have tried */

  for (i=0; i<n; i++)
    dv[i] = vout[i] = vest[i];

/* first time around? -> store first estimate in first column of d */

  if (iest == 0)
    for (i=0; i<n; i++)
      d[i * KMAXX] = vest[i];

/* more than one point? -> do polynomial extrapolation to h->0 */

  else {

    for (i=0; i<n; i++)
      c[i] = vest[i];

    for (i=1; i<=iest; i++) {

/* calculate new values of C's and D's to traverse Neville's tableau */

      delta = 1.0 / (hpoints[iest-i] - hest);
      f1    = hest * delta;
      f2    = hpoints[iest-i] * delta;

/* propagate tableau one diagonal more; the error is calculated using the  *
 * values of the D's */

      for (j=0; j<n; j++) {
	q                    = d[j * KMAXX + (i-1)];
	d[j * KMAXX + (i-1)] = dv[j];
	delta                = c[j] - q;
	dv[j]                = f1 * delta;
	c[j]                 = f2 * delta;
	vout[j]             += dv[j];


      }
    }

/* save current D's for future calls to this function in the appropriate   *
 * column of the D tableau                                                 */

    for (i=0; i<n; i++)
      d[i * KMAXX + iest] = dv[i];

  }

  free(c);
}



/*** mmid: implements the modified midpoint method used by BuSt; it subdi- *
 *         vides a large step (htot) into nstep intervals and uses a mid-  *
 *         point method over the whole of htot, with a stepsize of 2*h     *
 *         except for the first and the last step, hence *modified* mid-   *
 *         point method); the nice thing about this method is that the     *
 *         error follows a power law depending on the stepsize, i.e. its   *
 *         error converges to zero really fast as h is diminished; this    *
 *         allows for good extrapolation to h=0 (see pzextr() above)       *
 ***************************************************************************/

void mmid(double *vin, double *vout, double *deriv, double tin,
          double htot, int nstep, int n)
{

  int i, j;                                               /* loop counters */

  double *vm;                                   /* v's at beginning of 2*h */
  double *vn;                                    /* v's at midpoint of 2*h */
  double swap;                      /* tmp var used for swapping vm and vn */

  double t;                                                /* current time */

  double h;                    /* small (h) stepsize (equals htot / nstep) */
  double h2;                              /* double the small (h) stepsize */


  vm = (double*)calloc(n, sizeof(double));
  vn = (double*)calloc(n, sizeof(double));

/* calculate h from H and n */

  h = htot / nstep;

/* calculate the first step (according to Euler) */

  for (i=0; i<n; i++) {
    vm[i] = vin[i];
    vn[i] = vin[i] + h * deriv[i];
  }

/* do the intermediate steps */

  h2 = 2.0 * h;           /* mmid uses 2 * h as stepsize for interm. steps */
  t = tin + h;
  p_deriv(vn, t, vout, n);

  for (i=2; i<=nstep; i++) {
    for (j=0; j<n; j++) {
      swap  = vm[j] + h2 * vout[j];
      vm[j] = vn[j];
      vn[j] = swap;
    }

    t += h;
    p_deriv(vn, t, vout, n);
  }

/* calculate the last step */

  for (i=0; i<n; i++)
    vout[i] = 0.5 * (vm[i] + vn[i] + h * vout[i]);

/* clean up */

  free(vm);
  free(vn);
}



/***** BaDe: propagates v(t) from t1 to t2 by Bader-Deuflhard; this method *
 *           uses Richardson extrapolation to estimate v's at a hypothe-   *
 *           tical stepsize of 0; the extrapolation also yields an error   *
 *           estimate, which is used to adapt stepsize and change the or-  *
 *           der of the method as required; the result is returned by vout *
 ***************************************************************************
 *                                                                         *
 * This solver was implemented by Yogi, based on BuSt, Aug 2002            *
 *                                                                         *
 ***************************************************************************/

ODESolutionPoly*BaDe(double *vin, double *vout, double tin, double tout,
	  double stephint, double accuracy, int n, FILE *slog)
{

/* stifbs is the BaDe stepper function */

  void stifbs(double *v, double *deriv, int n, double *t, double htry,
	      double accuracy, double *hdid, double *hnext);

  int    i;                                          /* local loop counter */

  double t;                                                /* current time */

  double ht;                                      /* stepsize of next step */
  double hd;                               /* stepsize of step we just did */

  double *initderiv;                      /* array for initial derivatives */

  ODESolutionPoly *Solution;

/* the do-nothing case; too small steps dealt with under usual */

  if (tin == tout)
    return NULL;

  Solution = ODESolutionPoly_createWith(tin, tout, n);

/* the usual case: steps big enough -> initialize variables */

  initderiv = (double *)calloc(n, sizeof(double));
/*
  ODESolutionPoly_setStepsize(Solution,stepsize);
  ODESolutionPoly_setNSteps(Solution,nsteps);
*/
  ODESolutionPoly_initInterpolating(Solution,vin);

  t  = tin;

/* initial stepsize is either stephint or tout-tin */

  ht = DMIN(stephint, (tout - tin));

/* we're saving old v's in vin and propagate vout below */

  for (i=0; i<n; i++)
    vout[i] = vin[i];

/* this loop steps through the whole interval tout-tin */

  do {

    p_deriv(vout, t, initderiv, n);
    stifbs(vout, initderiv, n, &t, ht, accuracy, &hd, &ht);

    ODESolutionPoly_addtoInterpolating(Solution,vout,t);

    if (t + ht > tout)
      ht = tout - t;

  } while (t < tout);

/* clean up and go home */

  free(initderiv);

  ODESolutionPoly_makeInterpolating(Solution);

  return Solution;
}


/*** stifbs: this is the Bader-Deuflhard stepper function; it propagates ***
 *           the equations at a given overall stepsize and order, then     *
 *           evaluates the error and repeats the step with increased order *
 *           if necessary, until the required accuracy is achieved; its    *
 *           arguments are:                                                *
 *           - v:        input and output v's over a give total stepsize   *
 *           - deriv:    array of initial derivatives (at tin)             *
 *           - n:        size of v and deriv                               *
 *           - t:        pointer to the current time                       *
 *           - htry:     initial stepsize to be tried                      *
 *           - accuracy: relative accuracy to be achieved                  *
 *           - hdid:     returns the stepsize of the step we just did      *
 *           - hnext:    returns the stepsize of the next step to be done  *
 ***************************************************************************/

void stifbs(double *v, double *deriv, int n, double *t, double htry,
	    double accuracy, double *hdid, double *hnext)
{

/* func prototypes: these funcs should not be visible outside solvers.c    *
 *           simpr: implements the semi-implicit mid-point rule            *
 *          pzextr: implements the Richardson extrapolation (polynomial)   */

  void simpr(double *vin, double *vout, double *deriv, double *dfdt,
	     double **jac, double tin, double htot, int nstep, int n);

  void pzextr(int iest, double hest, double *yest,
	      double *yz, double *dy, int nv, const int KMAXX);

/*** constants *************************************************************/

  const int    KMAXX  = 7;         /* this is how many successive stepsize */
  const int    IMAXX  = KMAXX + 1;       /* refinements we're going to try */

  const double SAFE1  = 0.25; /* safety margins for errors and h reduction */
  const double SAFE2  = 0.7;

  const double REDMAX = 1.0e-5;       /* boundaries for stepsize reduction */
  const double REDMIN = 0.7;

  const double SCALMX = 0.1;         /* maximum scaling value for stepsize */

/*** variables *************************************************************/

  int i, k, km;                                           /* loop counters */

  double *vsav;                            /* v's at beginning of interval */
  double *vseq;             /* v's at end of interval for a given stepsize */

  double *verror;                                       /* error estimates */
  double verror_max;                        /* the maximum error in verror */
  double *err;                               /* normalized error estimates */

  double *dfdt;
  double **jac;                                     /* the Jacobian matrix */

  double h;                       /* stepsize for solver method used below */
  double hest;             /* square of h passed to extrapolation function */
  double red;                    /* reduction factor for reducing stepsize */
  double scale;                        /* scaling factor for next stepsize */

  double work;            /* temp vars for calculating row for convergence */
  double wrkmin;
  double fact;

  static double old_accuracy = -1.0;          /* used to save old accuracy */
  static double tnew;                       /* used to save old start time */
  static int nold = -1;                       /* for saving old value of n */

/* the following two arrays are used for Deuflhard's error estimation; a   *
 * contains the work coefficients and alf (alpha) the correction factors   */

  static double *a = NULL;
  static double **alf = NULL;

  double accuracy1;         /* error (< accuracy) used to calculate alphas */

  static int kmax;                                /* used for finding kopt */
  static int kopt;                   /* optimal row number for convergence */

/* sequence of separate attempts to cross interval htot with increasing    *
 * values of nsteps as suggested by Deuflhard (Num Rec, p. 726)            */

  static int nseq[] = {0,2,6,10,14,22,34,50,70};

/* miscellaneous flags */

  static int first = 1;         /* is this the first try for a given step? */
  int reduct;           /* flag indicating if we have reduced stepsize yet */
  int exitflag = 0;                     /* exitflag: when set, we exit (!) */

/* static global arrays */

  extern double *d;                /* D's used for extrapolation in pzextr */
  extern double *hpoints;        /* stepsizes h (=H/n) which we have tried */

/* allocate arrays */

  if ( !(d = (double *)calloc(KMAXX * n, sizeof(double))))
    error("BaDe: error allocating d.\n");

  if ( !(dfdt = (double *)calloc(n, sizeof(double))))
    error("BaDe: error allocating dfdt.\n");

  if ( !(jac = (double **)calloc(n, sizeof(double *))))
    error("BaDe: error allocating jac.\n");

  for (i=0; i<n; i++)
    if ( !(jac[i] = (double *)calloc(n, sizeof(double))))
      error("BaDe: error allocating jac's second dimension.\n");

  if ( !(err = (double *)calloc(KMAXX, sizeof(double))))
    error("BaDe: error allocating err.\n");

  if ( !(verror = (double *)calloc(n, sizeof(double))))
    error("BaDe: error allocating verror.\n");

  if ( !(vsav = (double *)calloc(n, sizeof(double))))
    error("BaDe: error allocating vsav.\n");

  if ( !(vseq = (double *)calloc(n, sizeof(double))))
    error("BaDe: error allocating vseq.\n");

  if ( !(hpoints = (double *)calloc(KMAXX, sizeof(double))))
    error("BaDe: error allocating hpoints.\n");

/* set initial stepsize to try to htry */

  h = htry;

/* allocate memory if necessary */

  if ( !a ) {
    a = (double *)calloc(IMAXX+1, sizeof(double));
    alf = (double **)calloc(KMAXX+1, sizeof(double *));
    for (i=0; i<KMAXX+1; i++)
      alf[i] = (double *)calloc(KMAXX+1, sizeof(double));
  }

/* new accuracy? -> initialize (here, this only applies at start) */

  if ( (accuracy != old_accuracy) || (n != nold) ) {

    *hnext = tnew = -1.0e29;             /* init these to impossible value */

/* initialize the work coefficients */

    a[1]   = nseq[1] + 1;
    for (k=1; k<=KMAXX; k++)
      a[k+1] = a[k] + nseq[k+1];

/* initialize the correction factors (alpha) */

    accuracy1 = SAFE1 * accuracy;    /* accuracy1 used to calculate alphas */
    for (i=2; i<=KMAXX; i++)
      for (k=1; k<i; k++)
	alf[k][i] = pow(accuracy1, ((a[k+1] - a[i+1]) /
			 ((a[i+1] - a[1] + 1.0) * (2*k+1))));
    old_accuracy = accuracy;
    nold = n;

/* add cost of Jacobian evaluations to work coefficients */

    a[1] += n;
    for (k=1; k<=KMAXX; k++)
      a[k+1] = a[k] + nseq[k+1];

/* determine optimal row number for convergence */

    for (kopt=2; kopt<KMAXX; kopt++)
      if (a[kopt+1] > a[kopt]*alf[kopt-1][kopt])
	break;
    kmax = kopt;

  }

/* actual stepping starts here: first save starting values of v[] in vsav  */

  for (i=0; i<n; i++)
    vsav[i] = v[i];

/* evaluate jacobian */

  p_jacobn(*t, v, dfdt, jac, n);

/* new integration or stepsize? -> re-establish the order window */

  if (*t != tnew || h != (*hnext)) {
    first = 1;
    kopt  = kmax;
  }

  reduct = 0;                          /* we have not reduced stepsize yet */

  for (;;) {

/* try increasing numbers of steps over the interval h */

    for (k=1; k<=kmax; k++) {

      tnew = (*t) + h;
      if (tnew == (*t))
	error("BaDe: stepsize underflow in stifbs.\n");

/* propagate the equations from t to t+h with nseq[k] steps using vsav and *
 * deriv as input; dfdt are the function derivs and jac is the Jacobian    *
 * trix; simpr returns vseq                                                */

      simpr(vsav, vseq, deriv, dfdt, jac, *t, h, nseq[k], n);

/* extrapolate v's for h->0 based on the different stepsizes (hest's) we   *
 * have tried already; the result is returned in v (errors in verror);     *
 * hest is squared since the error series is even                          */

      hest = DSQR(h / nseq[k]);
      pzextr(k-1, hest, vseq, v, verror, n, KMAXX);

      if (k > 1) {

/* find the maximum error */

      verror_max = 0.;
      for (i=0; i<n; i++)
	if ( v[i] != 0. )
	  verror_max = DMAX(fabs(verror[i]/v[i]), verror_max);
	else
	  verror_max = DMAX(fabs(verror[i]/DBL_EPSILON), verror_max);

/* scale error according to desired accuracy */

	verror_max /= accuracy;

/* compute normalized error estimates (epsilons) */

	km = k-1;
	err[km-1] = pow(verror_max/SAFE1, 1.0/(2*km+1));

      }

/* are we in the order window? -> converged */

      if (k > 1 && (k >= kopt - 1 || first)) {

	if (verror_max < 1.0) {            /* exit if accuracy good enough */
	  exitflag = 1;
	  break;
	}

	if (k == kmax || k == kopt + 1) {  /* stepsize reduction possible? */
	  red = SAFE2 / err[km-1];
	  break;
	} else if (k == kopt && alf[kopt-1][kopt] < err[km-1]) {
	  red = 1.0 / err[km-1];
	  break;
	} else if (kopt == kmax && alf[km][kmax-1] < err[km-1]) {
	  red = alf[km][kmax-1] * SAFE2 / err[km-1];
	  break;
	} else if (alf[km][kopt] < err[km-1]) {
	  red = alf[km][kopt-1] / err[km-1];
	  break;
	}
      }
    }

    if (exitflag)
      break;

/* reduce stepsize by at least REDMIN and at most REDMAX, then try again */

    red = DMIN(red, REDMIN);
    red = DMAX(red, REDMAX);

    h *= red;
    reduct = 1;
  }

/* we've taken a successful step */

  *t     = tnew;
  *hdid  = h;
  first  = 0;
  wrkmin = 1.0e35;

/* compute optimal row for convergence and corresponding stepsize */

  for (i=1; i <= km; i++) {

    fact = DMAX(err[i-1], SCALMX);
    work = fact * a[i+1];

    if (work < wrkmin) {

      scale  = fact;
      wrkmin = work;
      kopt   = i + 1;

    }
  }

  *hnext = h / scale;

/* check for possible order increase, but not if stepsize was just reduced */

  if (kopt >= k && kopt != kmax && !reduct) {

    fact = DMAX(scale / alf[kopt-1][kopt], SCALMX);

    if (a[kopt+1] * fact <= wrkmin) {
      *hnext = h / fact;
      kopt++;
    }
  }

/* clean up */

  for (i=0; i<n; i++)
    free(jac[i]);
  free(jac);

  free(d);
  free(dfdt);
  free(hpoints);
  free(vseq);
  free(verror);
  free(err);
  free(vsav);
}



/*** simpr: implements the semi-implicit midpoint rule used by BaDe; it ****
 *          subdivides a large step (htot) into nstep intervals and uses a *
 *          semi-implicit midpoint rule over the whole of htot, with a     *
 *          stepsize of 2*h except for the first and the last step; the    *
 *          nice thing about this method is that the error follows a power *
 *          law depending on the stepsize, i.e. its error converges to 0   *
 *          really fast as h is diminished; this allows for good extrapo-  *
 *          lation to h=0 (see pzextr() above)                             *
 ***************************************************************************/

void simpr(double *vin, double *vout, double *deriv, double *dfdt,
	   double **jac, double tin, double htot, int nstep, int n)
{
/* func prototypes: these funcs should not be visible outside solvers.c    *
 *          lubcmp: does DU decomposition of a matrix                      *
 *          lubksb: solves linear system a * b                             */

  void ludcmp(double **a, int n, int *indx, double *d);
  void lubksb(double **a, int n, int *indx, double *b);

/*** variables *************************************************************/

  int i, j;                                               /* loop counters */

  double t;                                                /* current time */
  double h;                    /* small (h) stepsize (equals htot / nstep) */
  double d;         /* d indicates if eve/odd rows were switched in ludcmp */

  int    *indx;            /* array for permutated row indices of matrix a */

  double *del;                     /* delta: difference in v between steps */
  double *vtemp;                          /* array for temp derivs and v's */

  double **a;                                          /* matrix [1 - hf'] */

/* allocate arrays */

  indx = (int *)calloc(n, sizeof(int));

  del = (double *)calloc(n, sizeof(double));
  vtemp = (double *)calloc(n, sizeof(double));

  a = (double **)calloc(n, sizeof(double *));
  for (i=0; i<n; i++)
    a[i] = (double *)calloc(n, sizeof(double));

/* calculate h from H and n */

  h = htot / nstep;

/* set up the matrix [1 - hf'] */

  for (i=0; i<n; i++) {
    for (j=0; j<n; j++)
      a[i][j] = -h * jac[i][j];
    ++a[i][i];
  }

/* the following is slightly bizarre */

/* do LU decomposition of matrix [1 - hf']; this is needed for all steps   *
 * below, which use linearization of the equations to get estimates of the *
 * derivatives at the endpoint of a step (which is done in lubksb)         */

  ludcmp(a, n, indx, &d);

/* do the first step */

/* set up the right-hand side for the first step; use vout for temp sto-   *
 * rage; note that since our equations are autonomous, all dfdt's are 0.0; *
 * lubksb solves the linear system given by a and vout and then returns    *
 * the solution vector in vout; this vector contains the difference be-    *
 * tween this step's v's and the next steps' v's which are stored in del   */

  for (i=0; i<n; i++)
    vout[i] = h * (deriv[i] + h * dfdt[i]);
  lubksb(a, n, indx, vout);

  for (i=0; i<n; i++)
    vtemp[i] = vin[i] + (del[i] = vout[i]);

/* do the intermediate steps */

  t = tin + h;
  p_deriv(vtemp, t, vout, n);

  for (i=2; i<=nstep; i++) {

/* set up the right hand side */

    for (j=0; j<n; j++)
      vout[j] = h * vout[j] - del[j];
    lubksb(a, n, indx, vout);

/* take the step */

    for (j=0; j<n; j++)
      vtemp[j] += (del[j] += 2.0 * vout[j]);

/* go to next step */

    t += h;
    p_deriv(vtemp, t, vout, n);
  }

/* do the last step */

/* set up the right-hand side for the last step */

  for (i=0; i<n; i++)
    vout[i] = h * vout[i] - del[i];
  lubksb(a, n, indx, vout);

/* take the last step */

  for (i=0; i<n; i++)
    vout[i] += vtemp[i];

/* clean up */

  for (i=0; i<n; i++)
    free(a[i]);
  free(a);

  free(vtemp);
  free(del);
  free(indx);

}



/*** ludcmp: does an LU decomposition of matrix a, which is needed for in- *
 *           verting the matrix; LU decomposition dissects the matrix into *
 *           a lower and an upper triangular matrix that when multiplied,  *
 *           equal matrix a again; this function uses Crout's algorithm    *
 *           for the decomposition; the result is returned in a and con-   *
 *           tains the LU decomposition of a rowwise permutation of a;     *
 *           indx returns the order of the permutated rows; n is the size  *
 *           of matrix a (as in n x n); d returns +1 if number of row in-  *
 *           terchanges was even, -1 if odd.                               *
 ***************************************************************************/

void ludcmp(double **a, int n, int *indx, double *d)
{
  int i, j, k;                                            /* loop counters */
  int imax;                 /* index of row of largest element in a column */

  double big;                          /* largest value, used for pivoting */
  double dum;                                   /* dummy used for pivoting */
  double sum;                                      /* temp var for summing */
  double temp;                             /* temp var for absolute values */
  double *vv;                   /* stores the implicit scaling information */

  const double TINY = 1.0e-20;                    /* this is a tiny number */

  vv = (double *)calloc(n, sizeof(double));

  *d = 1.0;                                         /* no interchanges yet */

/* loop over rows to get the implicit scaling information */

  for (i=0; i<n; i++) {

    big = 0.0;
    for (j=0; j<n; j++)
      if ((temp = fabs(a[i][j])) > big)
	big = temp;
                                      /* whole row == 0 -> singular matrix */
    if (big == 0.0)
      error("ludcmp: cannot decompose singular matrix");

    vv[i] = 1.0 / big;                                 /* save the scaling */
  }

/* loop over columns of Crout's method; this is done in two loops which    *
 * reflect the two different formulas of Crout's method, except that the   *
 * first formula is used for the diagonal elements in the second loop; in  *
 * the second loop we're also finding the largest element and its row num- *
 * ber for pivoting                                                        */

  for (j=0; j<n; j++) {

    for (i=0; i<j; i++) {
      sum = a[i][j];
      for (k=0; k<i; k++)
	sum -= a[i][k] * a[k][j];
      a[i][j] = sum;
    }

    big = 0.0;                 /* used for search of largest pivot element */

    for (i=j; i<n; i++) {

      sum = a[i][j];                               /* summing happens here */
      for (k=0; k<j; k++)
	sum -= a[i][k] * a[k][j];
      a[i][j] = sum;

      if ( (dum = vv[i] * fabs(sum)) >= big ) {       /* pivoting stuff here */
	big = dum;
	imax = i;
      }

    }

/* pivoting: do we need to interchange rows? if yes -> do so! */

    if (j != imax) {
      for (k=0; k<n; k++) {
	dum = a[imax][k];
	a[imax][k] = a[j][k];
	a[j][k] = dum;
      }

      *d = -(*d);                                    /* change parity of d */
      vv[imax] = vv[j];                         /* rearrange scale factors */
    }

    indx[j] = imax;                       /* store index of permutated row */

/* if the pivot element is zero, the matrix is singular; TINY avoids div/0 *
 * below (some applications can deal with singular matrices, we don't)     */

    if (a[j][j] == 0.0)
      a[j][j] = TINY;

/* divide by the pivot element */

    if (j != n-1) {
      dum = 1.0 / (a[j][j]);
      for (i=j+1; i<n; i++)
	a[i][j] *= dum;
    }                                          /* end of loop: next column */

  }

  free(vv);
}



/*** lubksb: does forward and backsubstitution of matrix a; in fact, a is **
 *           not passed in its original form but as the LU decomposition   *
 *           of a rowwise permutation of a as returned by the function     *
 *           ludcmp(); the right hand side vector is called b, which also  *
 *           returns the solution vector to the calling function; indx is  *
 *           a vector that indicates the order of rows in the permutated   *
 *           matrix; n is the dimension of the matrix a (as in n x n).     *
 ***************************************************************************/

void lubksb(double **a, int n, int *indx, double *b)
{
  int i, j;                                           /* counter variables */
  int ii = -1;                     /* index of first non-zero element of b */
  int ip;                                  /* index of permutated matrix a */

  double sum;                                      /* temp var for summing */

/* first loop does the forward substitution; we don't worry about the dia- *
 * gonal elements of the a matrix since here they're all 1 by definition   *
 * (see description of Crout's algorithm in Num Rec, chapter 2.3)          */

  for (i=0; i<n; i++) {

    ip = indx[i];                 /* get the index of the (real) first row */
    sum = b[ip];                                    /* get the according b */
    b[ip] = b[i];                             /* un-permutate the b-vector */
                           /* start summing at first non-zero element of b */
    if (ii >= 0)
      for (j=ii; j<=i-1; j++)
	sum -= a[i][j] * b[j];
    else if (sum)
      ii = i;

    b[i] = sum;          /* b's are now in the right (un-permutated) order */

  }

/* second loop does the backsubstitution */

  for (i=n-1; i>=0; i--) {
    sum = b[i];
    for (j=i+1; j<=n-1; j++)
      sum -= a[i][j] * b[j];
    b[i] = sum / a[i][i];
  }
}



/*** DEBUGGING AND SOLVER LOG FUNCS ***************************************/

void WriteSolvLog(char *solver, double tin, double tout, double h, int n,
		  int nderivs, FILE *slog)
{
  double nds;                 /* Number of Derivative evaluations per Step */
  double ttot;                       /* total time of propagation interval */

  nds = (double)nderivs/(double)n;
  ttot = tout - tin;

  fprintf(slog, "%s:   tin = %7.3f   tout = %7.3f   ttot = %7.3f   ",
	  solver, tin, tout, ttot);
  fprintf(slog, "h = %5.3f   nsteps = %4d    nderivs/step = %4.1f\n",
	  h, n, nds);
}



/*** Rkso: propagates vin (of size n) from tin to tout by the Fourth-Order **
 *        Runge-Kutta method; the result is returned by vout               *
 ***************************************************************************/

ODESolutionPoly*Rkso(double *vin, double *win, double *vout, double *wout, double tin, double tout,
	 double stephint, double accuracy, int n, FILE *slog)
{
	int i;                                             /* local loop counter */
	double **v;                 /* intermediate v's, used to toggle v arrays */
	double **w;                 /* intermediate v's, used to toggle v arrays */
	int toggle = 0;                  /* used to toggle between v[0] and v[1] */
	double *vtemp;                               /* guessed intermediate v's */
	double *vnow;                                /* ptr to v at current time */
	double *wtemp;                                /* ptr to v at current time */
	double *wnow;                                /* ptr to v at current time */
	double *vnext;                    /* ptr to v at current time + stepsize */
	double *wnext;                    /* ptr to v at current time + stepsize */
	double *deriv1;            /* the derivatives at the beginning of a step */
	double *deriv2;                    /* the derivatives at test-midpoint 1 */
	double *deriv3;                    /* the derivatives at test-midpoint 2 */
	double *deriv4;                      /* the derivatives at test-endpoint */
	double deltat;                                             /* tout - tin */
	double t;                                                /* current time */
	double th;                                /* time at the end of the step */
	double thh;                               /* time for interval midpoints */
	double m;                           /* used to calculate number of steps */
	double stepsize;                                        /* real stepsize */
	double h;                                          /* half the stepsize */
	double hh;                                          /* half the stepsize */
	double h4;                                          /* half the stepsize */
	double h6;                /* one sixth of the stepsize (for Rk4 formula) */
	int    step;                                   /* loop counter for steps */
	int    nsteps;                        /* number of steps we have to take */
	int    nd = 0;                            /* number of deriv evaluations */
	ODESolutionPoly *Solution;
/* the do-nothing case; too small steps dealt with under usual */
	if (tin == tout)
		return NULL;
	Solution = ODESolutionPoly_createWith(tin, tout, n);
/* the usual case: steps big enough */
	v      = (double **)calloc(2, sizeof(double *));
	w      = (double **)calloc(2, sizeof(double *));
	vtemp  = (double *)calloc(n, sizeof(double));
	wtemp  = (double *)calloc(n, sizeof(double));
	v[0]   = (double *)calloc(n, sizeof(double));
	v[1]   = (double *)calloc(n, sizeof(double));
	w[0]   = (double *)calloc(n, sizeof(double));
	w[1]   = (double *)calloc(n, sizeof(double));
	deriv1 = (double *)calloc(n, sizeof(double));
	deriv2 = (double *)calloc(n, sizeof(double));
	deriv3 = (double *)calloc(n, sizeof(double));
	deriv4 = (double *)calloc(n, sizeof(double));
	deltat = tout - tin;                 /* how far do we have to propagate? */
	m = floor(fabs(deltat)/stephint + 0.5);       /* see comment on stephint above */
	if ( m < 1. )
		m = 1.;                          /* we'll have to do at least one step */
	stepsize = deltat / m;                  /* real stepsize calculated here */
	nsteps = (int)m;                                  /* int number of steps */
	t = tin;                                             /* set current time */
	if ( t == t + stepsize )
		error("Rkso: stephint of %g too small!", stephint);
	ODESolutionPoly_setStepsize(Solution,stepsize);
	ODESolutionPoly_setNSteps(Solution,nsteps);
	ODESolutionPoly_initInterpolating(Solution,vin);
	vnow = vin;
	wnow = win;
	if (nsteps == 1) {              /* vnext holds the results of current step */
		vnext = vout;
		wnext = wout;
	} else {
		vnext = v[0];
		wnext = w[0];
	}
	h = stepsize;
	hh  = stepsize * 0.5;
	h4  = stepsize * 0.25;
	h6  = stepsize / 6.0;
	for (step=0; step < nsteps; step++) {                  /* loop for steps */
		thh = t + hh;                            /* time of interval midpoints */
		th  = t + stepsize;                     /* time at end of the interval */
/* do the Runge-Kutta thing here: first calculate intermediate derivatives */
		p_deriv2(vnow, wnow, t, deriv1, n);
		if ( debug )
			nd++;
		for(i = 0; i < n; i++) {
			vtemp[i] = vnow[i] + hh * wnow[i];
			wtemp[i] = wnow[i] + hh * deriv1[i];
		}
		p_deriv2(vtemp, wtemp, thh, deriv2, n);
		for(i = 0; i < n; i++) {
			vtemp[i] += h4 * deriv1[i] * h;
			wtemp[i] = wnow[i] + hh * deriv2[i];
		}
		p_deriv2(vtemp, wtemp, thh, deriv3, n);
		for(i = 0; i < n; i++) {
			vtemp[i] = vnow[i] + h * wnow[i] + hh * h * deriv2[i];
			wtemp[i] = wnow[i] + h * deriv3[i];
		}
		p_deriv2(vtemp, wtemp, th, deriv4, n);
		for(i = 0; i < n; i++)
			vnext[i] = vnow[i] + h * ( wnow[i] + h6 * (deriv1[i] + deriv2[i] + deriv3[i]));
		for(i = 0; i < n; i++)
			wnext[i] = wnow[i] + h6 * (deriv1[i] + 2.0 * deriv2[i] + 2.0 * deriv3[i] + deriv4[i]);
/* next step */
		t += stepsize;
		ODESolutionPoly_addtoInterpolating(Solution,vnext,t);
		if (step < nsteps - 2) {                   /* CASE 1: many steps to go */
			vnow = v[toggle];               /* toggle results from v[0] and v[1] */
			wnow = w[toggle];
			toggle++;
			toggle %= 2;
			vnext = v[toggle];
			wnext = w[toggle];
		} else if (step == nsteps - 2) {     /* CASE 2: next iteration = final */
			vnow = v[toggle];                                        /* set vout */
			vnext = vout;
			wnow = w[toggle];                                        /* set vout */
			wnext = wout;
		} else if (step > nsteps - 2) {    /* CASE 3: just did final iteration */
			free(v[0]);                                 /* clean up and go home! */
			free(v[1]);
			free(v);
			free(w[0]);                                 /* clean up and go home! */
			free(w[1]);
			free(w);
			free(vtemp);
			free(wtemp);
			free(deriv1);
			free(deriv2);
			free(deriv3);
			free(deriv4);
		}
	}
	ODESolutionPoly_makeInterpolating(Solution);
	if ( debug )
		WriteSolvLog("Rk4", tin, tout, stepsize, nsteps, nd, slog);
	return Solution;
}


/***    exFinDif: propagates vin (of size n) from tin to tout by the      ***
 *     explicit Finite-Difference method of 1st or 2nd order accuracy;      *
 *                       the result is returned by vout                     *
 *                                                                          *
 * ************************************************************************ *
 *                                                                          *
 * NOTATIONS :                                                              *
 * vin        <--   input meanings of concentration                         *
 * win        <--   input meanings of concentration function gradient       *
 * vout       <--   output meanings of concentration                        *
 * wout       <--   output meanings of concentration function gradient      *
 * tin        <--   start time                                              *
 * tout       <--   end time                                                *
 * stephint   <--   stepsize                                                *
 * accuracy   <--   accuracy for adaptive stepsize solvers                  *
 * n          <--   length of vin, win, vout, wout                          *
 *                                                                          *
 *** ******************************************************************* ***/

ODESolutionPoly* exFinDif(double *vin, double *win, double *vout, double *wout, double tin, double tout,
	 double stephint, double accuracy, int n, FILE *slog)
{
	double **v;               /* three time layers of solution */
   double *temp;             /* temporary variable */
	double theta;             /* time step size */
	double t;                 /* current time moment */
	double m;         		  /* used to calculate time step */
	int flag;                 /* help variable for calculating time step */
	int timeNum;              /* time layer index */
	int i;                    /* help indexes */
	int M;                    /* time layer count */
	ODESolutionPoly *Solution;
   /* the do-nothing case: too small steps dealt with under usual */
	if (tin == tout) {
		return NULL;
	}
	Solution = ODESolutionPoly_createWith(tin, tout, n);
	/* allocating memory */
	v = (double**) malloc(3 * sizeof(double*));
	v[0] = (double*) malloc(n * sizeof(double));
	v[1] = (double*) malloc(n * sizeof(double));
	v[2] = (double*) malloc(n * sizeof(double));
   /* calculating time step */
	m = floor(fabs(tout - tin) / stephint + 0.5);  /* see comment on stephint above */
	if (m < 1.) {
		m = 1.;                                /* we'll have to do at least one step */
	}
	theta = (tout - tin) / m;                 /* real stepsize calculated here */
	M = (int)m;                              /* int number of steps */
   t = tin;                                 /* set current time */
   if (t == t + theta) {
		error("Finite Difference: stephint of %g too small!", theta);
	}
   /* filling solution vectors by initial values */
	for (i = 0; i < n; i++) {
		v[0][i] = win[i];
		v[1][i] = vin[i];
		v[2][i] = 0.0;
	}
   /* main loop for time steps */
   /* for (timeNum = 0; timeNum < M; timeNum++) {
      v[2][0] = theta;
      v[2][1] = tin;
      p_deriv2(v[1], v[0], t, v[2], n);
      t += theta;
      ODESolutionPoly_addtoInterpolating(Solution, v[2], t);
      // preparing for calculating next time layer solution
      for (i = 0; i < n; i++) {
         v[0][i] = v[1][i];
         v[1][i] = v[2][i];
         v[2][i] = 0.0;
      }
   } */

   for (timeNum = 0; timeNum < M - 2; timeNum++) {
      v[2][0] = theta;
      v[2][1] = tin;
      p_deriv2(v[1], v[0], t, v[2], n);
      t += theta;
      ODESolutionPoly_addtoInterpolating(Solution, v[2], t);
      /* preparing for calculating next time layer solution */
      temp = v[0];
      v[0] = v[1];
      v[1] = v[2];
      v[2] = temp;
   }

   /* last but one time step */
   free(v[2]);
   v[2] = wout;
   v[2][0] = theta;
   v[2][1] = tin;
   p_deriv2(v[1], v[0], t, v[2], n);
   t += theta;
   ODESolutionPoly_addtoInterpolating(Solution, v[2], t);
   /* preparing for calculating last time layer solution */
   free(v[0]);
   v[0] = v[1];
   v[1] = v[2];
   v[2] = vout;

   /* last time step */
   v[2][0] = theta;
   v[2][1] = tin;
   p_deriv2(v[1], v[0], t, v[2], n);
   t += theta;
   ODESolutionPoly_addtoInterpolating(Solution, v[2], t);

   ODESolutionPoly_makeInterpolating(Solution);
   free(v[0]);
   free(v);
   return Solution;
}


/***************************************************************************
                          DDESolve.c  -  description
                             -------------------
    begin                : Jan 2013
    copyright            : (C) 2013 by Muzhichenko Vladimir
    email                : vmuzhichenko@gmail.com
 ***************************************************************************/
/*                                                               *
 * DDESolve.c contains the solver functions (can be toggled by -s *
 * option) that propagate the equations with time delays         *
 *                                                               *
 *****************************************************************
 *                                                               *
 * NOTE: *ONLY* general solvers allowed here; they *MUST* comply *
 *       to the generic solver interface                         *
 *                                                               *
 ** WARNING !!!!!!!!!!!! *****************************************
 *                                                               *
 * The functions in this file return vin and vout unchanged iff  *
 * stepsize = 0. It is the responsibility of the calling func-   *
 * tion to be sure that this is not a problem!!!                 *
 *                                                               *
 ** USAGE: *******************************************************
 *                                                               *
 *  All solvers adhere to the same interface:                    *
 *                                                               *
 *  Solver(double *vin, double *vout, double *delays,            *
 *         double tin, double tout, double stephint,             *
 *         double accuracy, int n, int nlags, FILE *slog);       *
 *                                                               *
 *  Arguments:                                                   *
 *                                                               *
 *  - vin       array of dependent variables at 'tin' (start)    *
 *  - vout      array of dependent variables at 'tout' (end);    *
 *              this is what's returned by the solver            *
 *  - tin       time at start                                    *
 *  - tout      time at end                                      *
 *  - stephint  suggested stepsize for fixed stepsize solvers;   *
 *              see extensive comment below; the embedded Rk     *
 *              solvers (Rkck, Rkf) take stephint as their ini-  *
 *              tial stepsize                                    *
 *  - accuracy  accuracy for adaptive stepsize solvers; accuracy *
 *              is always relative, i.e. 0.1 means 10% of the    *
 *              actual v we've evaluated                         *
 *  - n         size of vin and vout arrays; the user is respon- *
 *              sible for checking that the number of elements   *
 *              does not change in the model between vin/vout    *
 *  - nlags     number of different delays                       *
 *  - delays    vector of lenght nlags containinig delays        *
 *  - history   vector of lenght n containig constant history for*
 *              each equation                                    *
 *                                                               *
 *  Note that if tin = tout the solvers will just returns with-  *
 *  out doing anything. The caller is responsible for handling   *
 *  such a situation.                                            *
 *                                                               */

static addHistory p_addHistory;

int dirichlet_boundary_kount;

void ODESolve_set_dir(int p)
{
    dirichlet_boundary_kount = p;
}

void ODESolve_set_dde(addHistory p)
{
    p_addHistory = p;
}

/*** dde11: propagates vin (of size n) from tin to tout by method based on *
 * the Euler  method with introduced history of v the result is returned by*
 * vout                                                                    *
 ***************************************************************************
 * written by Vladimir Muzhichenko Jan 2014                                *
*/

ODESolutionPoly*dde11(double *vin, double *vout,
                     double tin, double tout, double stephint, double accuracy,
                     int n, FILE *slog)
{
    int    i, j;                                       /* local loop counter */

    double **v;                 /* intermediate v's, used to toggle v arrays */
    int    toggle = 0;               /* used to toggle between v[0] and v[1] */

    double *vnow;                                /* ptr to v at current time */
    double *vnext;                    /* ptr to v at current time + stepsize */

    double *deriv;             /* the derivatives at the beginning of a step */

    double deltat;                                             /* tout - tin */
    double t;                                                /* current time */

    double m;         /* tmp var to calculate precise stepsize from stephint */
    double stepsize;                                        /* real stepsize */
    int    step;                                   /* loop counter for steps */
    int    nsteps;                  /* total number of steps we have to take */

    int    nd = 0;                            /* number of deriv evaluations */

    ODESolutionPoly *Solution;

  /* steps too small: just return */

    if (tin == tout)
      return NULL;


    Solution = ODESolutionPoly_createWith( tin, tout, n);

    v = (double **)calloc(2, sizeof(double *));

    v[0]  = (double *)calloc(n, sizeof(double));
    v[1]  = (double *)calloc(n, sizeof(double));
    deriv = (double *)calloc(n, sizeof(double));

//    vdiscont = (double *)calloc(n, sizeof(double));

    deltat = tout - tin;                 /* how far do we have to propagate? */
    m = floor(fabs(deltat)/stephint + 0.5);       /* see comment on stephint above */
    if ( m < 1. )
      m = 1.;                          /* we'll have to do at least one step */

    stepsize = deltat / m;                  /* real stepsize calculated here */
    nsteps = (int)m;                                  /* int number of steps */
    t = tin;                                             /* set current time */

    if ( t == t + stepsize )
      error("Euler: stephint of %g too small!", stephint);

    ODESolutionPoly_setStepsize(Solution,stepsize);
    ODESolutionPoly_setNSteps(Solution,nsteps);

    ODESolutionPoly_initInterpolating(Solution,vin);

    vnow = vin;

    ODESolutionPoly_setStepsize(Solution,stepsize);
    ODESolutionPoly_setNSteps(Solution,nsteps);

    ODESolutionPoly_initInterpolating(Solution,vin);
    p_addHistory(vin, tin);

    vnow = vin;

    if (nsteps == 1)       /* vnext holds the results after the current step */
      vnext = vout;
    else
      vnext = v[0];

    for (step=0; step < nsteps; step++) {                  /* loop for steps */

      p_deriv(vnow, t, deriv, n);  /* call derivative func to evaluate deriv */

      for(i=0; i < n; i++)
        vnext[i] = vnow[i] + stepsize * deriv[i];           /* Euler formula */

      t += stepsize;                                      /* go to next step */

      if(debug && nsteps <= 30){
        printf("\n%3i, v(%.2f)= ", step, t);
        for(i = 0; i < n; i++)
            printf("%5.2f  ", vnow[i]);

        printf("||  deriv(%.2f)= ", t);
        for(i = 0; i < n; i++)
            printf("%5.2f  ", deriv[i]);

        nd++;
      }

      p_addHistory(vnext, t);

      ODESolutionPoly_addtoInterpolating(Solution,vnext,t);

      if (step < nsteps - 2) {                   /* CASE 1: many steps to go */
        vnow = v[toggle];               /* toggle results from v[0] and v[1] */
        toggle++;
        toggle %= 2;
        vnext = v[toggle];

      } else if (step == nsteps - 2) {     /* CASE 2: next iteration = final */
        vnow = v[toggle];                                        /* set vout */
        vnext = vout;

      } else if (step > nsteps - 2) {    /* CASE 3: just did final iteration */
        free(v[0]);                                 /* clean up and go home! */
        free(v[1]);
        free(v);
        free(deriv);
      }
    }

    ODESolutionPoly_makeInterpolating(Solution);

    if ( debug )
      WriteSolvLog("DDE Euler", tin, tout, stepsize, nsteps, nd, slog);

    return Solution;
}

/*** dde21: propagates vin (of size n) from tin to tout by method based on
 * the Midpoint or Second-Order Runge-Kutta method with introduced history of v
 *; the result is returned by vout  *
 ***************************************************************************
 * written by Vladimir Muzhichenko Jan 2014                                *
*/

ODESolutionPoly*dde21(double *vin, double *vout,
                    double tin, double tout, double stephint, double accuracy,
                    int n, FILE *slog)
{
  int i, j;                                          /* local loop counter */

  double **v;                 /* intermediate v's, used to toggle v arrays */
  int    toggle = 0;               /* used to toggle between v[0] and v[1] */

  double *vtemp;                  /* guessed intermediate v's for midpoint */

  double *vnow;                                /* ptr to v at current time */
  double *vnext;                    /* ptr to v at current time + stepsize */

  double *deriv1;            /* the derivatives at the beginning of a step */
  double *deriv2;                 /* the derivatives at midpoint of a step */

  double deltat;                                             /* tout - tin */
  double t;                                                /* current time */
  double thh;                                  /* time at half of the step */

  double m;         /* tmp var to calculate precise stepsize from stephint */
  double stepsize;                                        /* real stepsize */
  double hh;                                          /* half the stepsize */
  int    step;                                   /* loop counter for steps */
  int    nsteps;                  /* total number of steps we have to take */

  int    nd = 0;                            /* number of deriv evaluations */

  ODESolutionPoly *Solution;

/* the do-nothing case; too small steps dealt with under usual */

  if (tin == tout)
    return NULL;

  Solution = ODESolutionPoly_createWith(tin, tout, n);

/* the usual case: steps big enough */

  v      = (double **)calloc(2, sizeof(double *));

  vtemp  = (double *)calloc(n, sizeof(double));
  v[0]   = (double *)calloc(n, sizeof(double));
  v[1]   = (double *)calloc(n, sizeof(double));
  deriv1 = (double *)calloc(n, sizeof(double));
  deriv2 = (double *)calloc(n, sizeof(double));


  deltat = tout - tin;                 /* how far do we have to propagate? */
  m = floor(fabs(deltat)/stephint + 0.5);       /* see comment on stephint above */
  if ( m < 1. )
    m = 1.;                          /* we'll have to do at least one step */

  stepsize = deltat / m;                  /* real stepsize calculated here */
  nsteps = (int)m;                                  /* int number of steps */
  t = tin;                                             /* set current time */

  if ( t == t + stepsize )
    error("dde21: stephint of %g too small!", stephint);

  ODESolutionPoly_setStepsize(Solution,stepsize);
  ODESolutionPoly_setNSteps(Solution,nsteps);

  ODESolutionPoly_initInterpolating(Solution,vin);

  vnow = vin;

  p_addHistory(vin, tin);

  if (nsteps == 1)              /* vnext holds the results of current step */
    vnext = vout;
  else
    vnext = v[0];

  hh  = stepsize * 0.5;                       /* evaluate half of stepsize */

  for (step=0; step < nsteps; step++) {                  /* loop for steps */

    thh = t + hh;                            /* time of interval midpoints */

    p_deriv(vnow, t, deriv1, n);                    /* first call to deriv */

    if ( debug ) nd++;

    for(i=0; i<n; i++)                         /* evaluate v's at midpoint */
      vtemp[i] = vnow[i] + hh * deriv1[i];

    p_deriv(vtemp, thh, deriv2, n);                /* second call to deriv */

    if ( debug ) nd++;

    for(i=0; i < n; i++)
      vnext[i] = vnow[i] + stepsize * deriv2[i];       /* Midpoint formula */

    t += stepsize;                                      /* go to next step */

    if(debug && nsteps <= 30){
      printf("\n%3i, v(%.2f)= ", step, t);
      for(i = 0; i < n; i++)
          printf("%5.2f  ", vnow[i]);

      printf("||  deriv(%.2f)= ", t);
      for(i = 0; i < n; i++)
          printf("%5.2f  ", deriv2[i]);
    }

    p_addHistory(vnext, t);

    ODESolutionPoly_addtoInterpolating(Solution,vnext,t);

    if (step < nsteps - 2) {                   /* CASE 1: many steps to go */
      vnow = v[toggle];               /* toggle results from v[0] and v[1] */
      toggle++;
      toggle %= 2;
      vnext = v[toggle];

    } else if (step == nsteps - 2) {     /* CASE 2: next iteration = final */
      vnow = v[toggle];                                        /* set vout */
      vnext = vout;

    } else if (step > nsteps - 2) {    /* CASE 3: just did final iteration */
      free(v[0]);                                 /* clean up and go home! */
      free(v[1]);
      free(v);
      free(vtemp);
      free(deriv1);
      free(deriv2);
    }
  }

  ODESolutionPoly_makeInterpolating(Solution);

  if ( debug )
    WriteSolvLog("dde21", tin, tout, stepsize, nsteps, nd, slog);

  return Solution;
}

ODESolutionPoly*dde21d(double *vin, double *vout,
                    double tin, double tout, double stephint, double accuracy,
                    int n, FILE *slog)
{
  int i, j;                                          /* local loop counter */

  double **v;                 /* intermediate v's, used to toggle v arrays */
  int    toggle = 0;               /* used to toggle between v[0] and v[1] */

  double *vtemp;                  /* guessed intermediate v's for midpoint */

  double *vnow;                                /* ptr to v at current time */
  double *vnext;                    /* ptr to v at current time + stepsize */

  double *deriv1;            /* the derivatives at the beginning of a step */
  double *deriv2;                 /* the derivatives at midpoint of a step */

  double deltat;                                             /* tout - tin */
  double t;                                                /* current time */
  double thh;                                  /* time at half of the step */

  double m;         /* tmp var to calculate precise stepsize from stephint */
  double stepsize;                                        /* real stepsize */
  double hh;                                          /* half the stepsize */
  int    step;                                   /* loop counter for steps */
  int    nsteps;                  /* total number of steps we have to take */

  int    nd = 0;                            /* number of deriv evaluations */

  ODESolutionPoly *Solution;

/* the do-nothing case; too small steps dealt with under usual */

  if (tin == tout)
    return NULL;

  Solution = ODESolutionPoly_createWith(tin, tout, n);

/* the usual case: steps big enough */

  v      = (double **)calloc(2, sizeof(double *));

  vtemp  = (double *)calloc(n, sizeof(double));
  v[0]   = (double *)calloc(n, sizeof(double));
  v[1]   = (double *)calloc(n, sizeof(double));
  deriv1 = (double *)calloc(n, sizeof(double));
  deriv2 = (double *)calloc(n, sizeof(double));


  deltat = tout - tin;                 /* how far do we have to propagate? */
  m = floor(fabs(deltat)/stephint + 0.5);       /* see comment on stephint above */
  if ( m < 1. )
    m = 1.;                          /* we'll have to do at least one step */

  stepsize = deltat / m;                  /* real stepsize calculated here */
  nsteps = (int)m;                                  /* int number of steps */
  t = tin;                                             /* set current time */

  if ( t == t + stepsize )
    error("dde21: stephint of %g too small!", stephint);

  ODESolutionPoly_setStepsize(Solution,stepsize);
  ODESolutionPoly_setNSteps(Solution,nsteps);

  ODESolutionPoly_initInterpolating(Solution,vin);

  vnow = vin;

  p_addHistory(vin, tin);

  if (nsteps == 1)              /* vnext holds the results of current step */
    vnext = vout;
  else
    vnext = v[0];

  hh  = stepsize * 0.5;                       /* evaluate half of stepsize */

  for (step=0; step < nsteps; step++) {                  /* loop for steps */

    thh = t + hh;                            /* time of interval midpoints */

    p_deriv(vnow, t, deriv1, n);                    /* first call to deriv */

    if ( debug ) nd++;
    for(i = dirichlet_boundary_kount; i < n - dirichlet_boundary_kount; i++)                         /* evaluate v's at midpoint */
      vtemp[i] = vnow[i] + hh * deriv1[i];
    for ( i = 0; i < dirichlet_boundary_kount; i++) {
        vtemp[i] = deriv1[i];
        vtemp[n - 2 * dirichlet_boundary_kount + i] = deriv1[n - 2 * dirichlet_boundary_kount + i];
    }

    p_deriv(vtemp, thh, deriv2, n);                /* second call to deriv */

    if ( debug ) nd++;
    for(i = dirichlet_boundary_kount; i < n - dirichlet_boundary_kount; i++)
      vnext[i] = vnow[i] + stepsize * deriv2[i];       /* Midpoint formula */
    for ( i = 0; i < dirichlet_boundary_kount; i++) {
        vnext[i] = deriv2[i];
        vnext[n - 2 * dirichlet_boundary_kount + i] = deriv2[n - 2 * dirichlet_boundary_kount + i];
    }

    t += stepsize;                                      /* go to next step */

    if(debug && nsteps <= 30){
      printf("\n%3i, v(%.2f)= ", step, t);
      for(i = 0; i < n; i++)
          printf("%5.2f  ", vnow[i]);

      printf("||  deriv(%.2f)= ", t);
      for(i = 0; i < n; i++)
          printf("%5.2f  ", deriv2[i]);
    }

    p_addHistory(vnext, t);

    ODESolutionPoly_addtoInterpolating(Solution,vnext,t);

    if (step < nsteps - 2) {                   /* CASE 1: many steps to go */
      vnow = v[toggle];               /* toggle results from v[0] and v[1] */
      toggle++;
      toggle %= 2;
      vnext = v[toggle];

    } else if (step == nsteps - 2) {     /* CASE 2: next iteration = final */
      vnow = v[toggle];                                        /* set vout */
      vnext = vout;

    } else if (step > nsteps - 2) {    /* CASE 3: just did final iteration */
      free(v[0]);                                 /* clean up and go home! */
      free(v[1]);
      free(v);
      free(vtemp);
      free(deriv1);
      free(deriv2);
    }
  }

  ODESolutionPoly_makeInterpolating(Solution);

  if ( debug )
    WriteSolvLog("dde21d", tin, tout, stepsize, nsteps, nd, slog);

  return Solution;
}

/*** dde41: propagates vin (of size n) from tin to tout by by method based on
 *        the Fourth-Order Runge-Kutta method; the result is returned by vout *
 ***************************************************************************
 *                                                                         *
 * written by Vladimir Muzhichenko Jan 2014                                *
 *                                                                         *
 ***************************************************************************/

ODESolutionPoly*dde41(double *vin, double *vout,
                    double tin, double tout, double stephint, double accuracy,
                    int n, FILE *slog)
{
  int i, j;                                          /* local loop counter */

  double **v;                 /* intermediate v's, used to toggle v arrays */
  int toggle = 0;                  /* used to toggle between v[0] and v[1] */

  double *vtemp;                               /* guessed intermediate v's */

  double *vnow;                                /* ptr to v at current time */
  double *vnext;                    /* ptr to v at current time + stepsize */

  double *deriv1;            /* the derivatives at the beginning of a step */
  double *deriv2;                    /* the derivatives at test-midpoint 1 */
  double *deriv3;                    /* the derivatives at test-midpoint 2 */
  double *deriv4;                      /* the derivatives at test-endpoint */

  double deltat;                                             /* tout - tin */
  double t;                                                /* current time */
  double th;                                /* time at the end of the step */
  double thh;                               /* time for interval midpoints */

  double m;                           /* used to calculate number of steps */
  double stepsize;                                        /* real stepsize */
  double hh;                                          /* half the stepsize */
  double h6;                /* one sixth of the stepsize (for Rk4 formula) */
  int    step;                                   /* loop counter for steps */
  int    nsteps;                        /* number of steps we have to take */

  int    nd = 0;                            /* number of deriv evaluations */

  ODESolutionPoly *Solution;

/* the do-nothing case; too small steps dealt with under usual */

  if (tin == tout)
    return NULL;

  Solution = ODESolutionPoly_createWith(tin, tout, n);

/* the usual case: steps big enough */

  v      = (double **)calloc(2, sizeof(double *));

  vtemp  = (double *)calloc(n, sizeof(double));
  v[0]   = (double *)calloc(n, sizeof(double));
  v[1]   = (double *)calloc(n, sizeof(double));
  deriv1 = (double *)calloc(n, sizeof(double));
  deriv2 = (double *)calloc(n, sizeof(double));
  deriv3 = (double *)calloc(n, sizeof(double));
  deriv4 = (double *)calloc(n, sizeof(double));

  deltat = tout - tin;                 /* how far do we have to propagate? */
  m = floor(fabs(deltat)/stephint + 0.5);       /* see comment on stephint above */
  if ( m < 1. )
    m = 1.;                          /* we'll have to do at least one step */

  stepsize = deltat / m;                  /* real stepsize calculated here */
  nsteps = (int)m;                                  /* int number of steps */
  t = tin;                                             /* set current time */

  if ( t == t + stepsize )
    error("dde41: stephint of %g too small!", stephint);

  ODESolutionPoly_setStepsize(Solution,stepsize);
  ODESolutionPoly_setNSteps(Solution,nsteps);

  ODESolutionPoly_initInterpolating(Solution,vin);

  vnow = vin;

  p_addHistory(vin, tin);

  if (nsteps == 1)              /* vnext holds the results of current step */
    vnext = vout;
  else
    vnext = v[0];

  hh  = stepsize * 0.5;
  h6  = stepsize / 6.0;

  for (step=0; step < nsteps; step++) {                  /* loop for steps */

    thh = t + hh;                            /* time of interval midpoints */
    th  = t + stepsize;                     /* time at end of the interval */

/* do the Runge-Kutta thing here: first calculate intermediate derivatives */

    p_deriv(vnow, t, deriv1, n);

    if ( debug ) nd++;

    for(i = 0; i < n; i++)
      vtemp[i] = vnow[i] + hh * deriv1[i];

    p_deriv(vtemp, thh, deriv2, n);

    if ( debug ) nd++;

    for(i = 0; i < n; i++)
      vtemp[i] = vnow[i] + hh * deriv2[i];
    p_deriv(vtemp, thh, deriv3, n);

    if ( debug ) nd++;

    for(i = 0; i < n; i++)
      vtemp[i] = vnow[i] + stepsize * deriv3[i];
    p_deriv(vtemp, th, deriv4, n);

    if ( debug ) nd++;

/* ... then feed them to the Fourth-Order Runge-Kutta formula */

   for(i = 0; i < n; i++)
      vnext[i] = vnow[i] + h6*(deriv1[i] + 2.0*deriv2[i] + 2.0*deriv3[i] + deriv4[i]);

/* next step */

    t += stepsize;

    if(debug && nsteps <= 30){
      printf("\n%3i, v(%.2f)= ", step, t);
      for(i = 0; i < n; i++)
          printf("%5.2f  ", vnow[i]);

      printf("||  deriv(%.2f)= ", t);
      for(i = 0; i < n; i++)
          printf("%5.2f  ", deriv4[i]);
    }

    p_addHistory(vnext, t);

    ODESolutionPoly_addtoInterpolating(Solution,vnext,t);

    if (step < nsteps - 2) {                   /* CASE 1: many steps to go */
      vnow = v[toggle];               /* toggle results from v[0] and v[1] */
      toggle++;
      toggle %= 2;
      vnext = v[toggle];

    } else if (step == nsteps - 2) {     /* CASE 2: next iteration = final */
      vnow = v[toggle];                                        /* set vout */
      vnext = vout;

    } else if (step > nsteps - 2) {    /* CASE 3: just did final iteration */
      free(v[0]);                                 /* clean up and go home! */
      free(v[1]);
      free(v);
      free(vtemp);
      free(deriv1);
      free(deriv2);
      free(deriv3);
      free(deriv4);
    }
  }

  ODESolutionPoly_makeInterpolating(Solution);

  if ( debug )
    WriteSolvLog("dde41", tin, tout, stepsize, nsteps, nd, slog);

  return Solution;
}
