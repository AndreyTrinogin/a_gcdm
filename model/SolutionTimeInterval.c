#include <float.h>
#ifdef ICC
#include <mathimf.h>
#else
#include <math.h>
#endif
#include <stdlib.h>
#include <stdio.h>
#include <SolutionTimeInterval.h>

SolutionTimeInterval*SolutionTimeInterval_creatWith(double tin,double tout)
{
	SolutionTimeInterval*p;
	p = (SolutionTimeInterval*)malloc(sizeof(SolutionTimeInterval));
	p->time_start = tin;
        p->time_finish = tout;
	return p;
}

void SolutionTimeInterval_delete(SolutionTimeInterval*p)
{
	free(p);
}
