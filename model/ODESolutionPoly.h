#ifndef ODESOLUTIONPOLY_INCLUDED
#define ODESOLUTIONPOLY_INCLUDED

#include <float.h>
#ifdef ICC
#include <mathimf.h>
#else
#include <math.h>
#endif
#include <stdlib.h>
#include <stdio.h>

#include <InterpolatingFunctionPoly.h>
#include <SolutionTimeInterval.h>

typedef struct ODESolutionPoly {
  double quality;
  double Stepsize;
  int NSteps;
  SolutionTimeInterval*time_interval;
  int NFuncs;
  InterpolatingFunctionPoly**interpolating_func;
} ODESolutionPoly;

void ODESolutionPoly_setInterpolatingOrder(int interpolating_order);
ODESolutionPoly*ODESolutionPoly_createWith(double tin,double tout,int n);
void ODESolutionPoly_setStepsize(ODESolutionPoly*Solution, double stepsize);
void ODESolutionPoly_setNSteps(ODESolutionPoly*Solution, int nsteps);
void ODESolutionPoly_initInterpolating(ODESolutionPoly*Solution, double*v);
void ODESolutionPoly_addtoInterpolating(ODESolutionPoly*Solution,double*v,double time);
void ODESolutionPoly_makeInterpolating(ODESolutionPoly*Solution);
void ODESolutionPoly_delete(ODESolutionPoly*Solution);
void ODESolutionPoly_evaluateAtTime(ODESolutionPoly*Solution,double*v,double time);

#endif
