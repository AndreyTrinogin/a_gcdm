/*****************************************************************
 *                                                               *
 *   maternal.c                                                  *
 *                                                               *
 *****************************************************************
 *                                                               *
 *   written by JR, modified by Yoginho                          *
 *                                                               *
 *****************************************************************
 *                                                               *
 * This file contains several kinds of things:                   *
 *                                                               *
 * 1. some small I/O functions (FindSection(), KillSection())    *
 *    that are used throughout in the fly code                   *
 * 2. stuff that deals with that part of the blastoderm which is *
 *    fixed by the maternal genotype, i.e. functions dealing     *
 *    with division schedules (including the rules for zygotic.c,*
 *    the number of nuclei at each cleavage cycle and the li-    *
 *    neage number of the most anterior nucleus for each clea-   *
 *    vage cycle), bicoid gradients and diffusion schedules      *
 * 3. bias-related stuff is also in here since it's needed to    *
 *    run the model                                              *
 *                                                               *
 *****************************************************************/


#include <float.h>                                      /* for DBL_EPSILON */
#include <stdlib.h>
#ifdef ICC
#include <mathimf.h>
#else
#include <math.h>
#endif
#include <ctype.h>
#include <string.h>

/* these for umask*/
#include <sys/types.h>
#include <sys/stat.h>

#include <error.h>                 /* for error and linked list functions  */
#include <maternal.h>
#include <zygotic.h>                      /* for derivative function rules */
#include <ffio.h>

#include <gsl/gsl_errno.h>
#include <gsl/gsl_spline.h>


/*** MITOSIS SCHEDULE: hard-wired cell division tables *********************
 *                                                                         *
 * From Foe & Alberts, '83 (Table 1, TOTAL ELAPSED added by JR and JJ):    *
 *                                                                         *
 * The authors observed anterior tips of growing embryos during cycles 10  *
 * to 14 and measured the time of somatic bud cycles and recorded the      *
 * visibility of the nuclei, which indicate the duration of mitoses.       *
 *                                                                         *
 * TOTAL ELAPSED is the total time which elapsed since the start of a sim- *
 * ulation. I.e. we assume                                                 *
 *                                                                         *
 * t=0 at end of cell cycle 10 for NDIVS=3                                 *
 * t=0 1 min into interphase of cell cycle 10 for NDIVS=4 (see (*) below)  *
 *                                                                         *
 * Times are given in minutes, standard devs are between parentheses)      *
 *                                                                         *
 * CYCLE  TOTAL DURATION      NO NUCLEUS            TOTAL ELAPSED          *
 *                                              NDIVS=3       NDIVS=4      *
 *                                                                         *
 *  10 (*)  7.8 (0.6)          3.3 (0.9)                        0.0        *
 *  11      9.5 (0.7)          3.0 (0.9)          0.0           7.8        *
 *  12     12.4 (0.9)          3.3 (0.9)          9.5          17.3        *
 *  13     21.1 (1.5)          5.1 (0.9)         21.9          29.7        *
 *     	             	       				                   *
 *  14        50+          mitosis post gast     43.0          50.8        *
 *                                                                         *
 *  gastrulation time:                           93.0         100.8        *
 *                                                                         *
 * (*) NOTE: cell cycle 10 actually takes 8.8 minutes, but the migration   *
 *           of nuclei to the cell surface is only completed 1 min into    *
 *           interphase of cell cycle 10. This is our simulation starting  *
 *           point for NDIVS=4.                                            *
 *                                                                         *
 * The following arrays are the hard-wired stuff about times and durations *
 * of cell divisions and time of gastrulation; each function in maternal.c *
 * that returns information about timing in the blastoderm needs to choose *
 * the appropriate tables depending on the problem, i.e. the mitosis sche- *
 * dule used.                                                              *
 *                                                                         *
 * Oldstyle division times were a coarse approximation based on the 4 min  *
 * time units intially used by JR and DS. They were refined into 4/3 min   *
 * units in pre 9.2 code.                                                  *
 *                                                                         *
 * IMPORTANT: Post 9.2 code does NOT require rounded division times any-   *
 * more. These old division times are only included for backward compati-  *
 * bility and should not be used for annealing to new data!                *
 *                                                                         *
 * NOTE: The times must be in reverse order!!!                             *
 * ALSO NOTE: divtimes are at the END of the cell division!!!              *
 * LAST NOTE: there are 3 arrays each, which are (in order of appearance): *
 *            - oldstyle (see above)                                       *
 *            - 3-celldivision schedule (starting at cycle 11)             *
 *            - 4-celldivision schedule (starting at cycle 10)             *
 *                                                                         *
 * JJ (04/04/02):                                                          *
 *            I've added a 'zero'-division schedule which we will use to   *
 *            check if we can get patterns without cell divisions; since   *
 *            this division schedule doesn't have any divisions, it also   *
 *            doesn't need any division tables below                       *
 *                                                                         *
 ***************************************************************************/


/* division times: at end of mitosis! */

static const double old_divtimes[3]     = {52.0, 28.0, 12.0};

static const double divtimes1[1]        = {21.1};
static const double divtimes2[2]        = {33.5, 12.4};
static const double divtimes3[3]        = {43.0, 21.9,  9.5};
static const double divtimes4[4]        = {50.8, 29.7, 17.3,  7.8};

static double *divtimes;

/* division durations */

static const double old_div_duration[3] = { 4.0,  4.0,  4.0};
static const double div_duration1[1]    = { 5.1};
static const double div_duration2[2]    = { 5.1,  3.3};
static const double div_duration3[3]    = { 5.1,  3.3,  3.0};
static const double div_duration4[4]    = { 5.1,  3.3,  3.0,  3.3};

static double *div_durations;

/* gastrulation times */

static const double old_gast_time       = 88.;
static const double gast_time0          = 50.;
static const double gast_time1          = 71.1;
static const double gast_time2          = 83.5;
static const double gast_time3          = 93.;
static const double gast_time4          = 100.8;

static double gast_time;

/* full division times: including t<0 */

static const double full_divtimes0[TOTAL_DIVS]        = {0.0, -21.1, -33.5, -43.0, -51.8, -57.8};
static const double full_divtimes1[TOTAL_DIVS]        = {21.1, 0.0, -12.4, -21.9, -30.7, -36.7};
static const double full_divtimes2[TOTAL_DIVS]        = {33.5, 12.4, 0.0, -9.5, -18.3, -24.3};
static const double full_divtimes3[TOTAL_DIVS]        = {43.0, 21.9,  9.5, 0.0, -8.8, -14.8};
static const double full_divtimes4[TOTAL_DIVS]        = {50.8, 29.7, 17.3, 7.8, -1.0, -7.0};

static double *full_divtimes;

/* division durations */

static const double full_div_durations[TOTAL_DIVS]    = { 5.1,  3.3,  3.0, 3.3, 3.0, 3.0};

/* data times as for 03.2007 */

static const double data_times0[8] = {24.225, 30.475, 36.725, 42.975, 49.225, 55.475, 61.725, 67.975};
static const int ndata_times0 = 8;
static const double data_times1[9] = { 10.550, 24.225, 30.475, 36.725, 42.975, 49.225, 55.475, 61.725, 67.975};
static const int ndata_times1 = 9;

static double *data_times;
static int ndata_times;

/*** STATIC VARIABLES ******************************************************/

/* following is number of nucs in each cleavage cycle, reverse order */

static int *nnucs;
static int *full_nnucs=NULL;

/* following contains lineage numbers at which nuclei at each ccycle start */

static int   *lin_start;
static int   *full_lin_start=NULL;
static int    full_ccycles;


/* The following two store bicoid gradients and bias static to maternal.c  */
/* bias is found here because it contains the maternal contributions to    */
/* some zygotic genes (such as hunchback), but it can also be used to add  */
/* heatshocks during the simulation                                        */

static GenoType    *bcdtype;
static GenoType    *bcdtype_orig;
static GenoType    *biastype;

/* these two static structs are used for storing the times for which       */
/* there is bias; these times can be retrieved by using GetBTimes          */

static GenoType    *bt;                    /* bias times for each genotype */

static Slist *mat_genotypes;         /* temporary linked list for geno- */

static int         bt_init_flag = 0;                   /* flag for BTtable */

static InterpObject **extinp_interp_object; /* what is needed by ExternalInputs() */
static int GINDEX;

static int scale_maternal_data_flag = 0;
static double scale_maternal_data_factor = 1.0;

/*** INITIALIZATION FUNCTIONS **********************************************/

Slist*GetMatGenotypes(int *n)
{
	*n = nalleles;
	return mat_genotypes;
}

void gcdm_set_maternal_scaling(int flag, double factor)
{
	scale_maternal_data_flag = flag;
	scale_maternal_data_factor = factor;
}

void InitMaternal(FILE *fp)
{
	mat_genotypes = ReadGenotypes(fp);
	if ( nalleles == 0 )
		nalleles  = count_Slist(mat_genotypes);

	if ( olddivstyle ) {
		if ( defs->ndivs != 3 )
			error("InitMaternal: only 3 cell divisions allowed for oldstyle (-o)");
		full_divtimes = (double *)full_divtimes4;
		divtimes = (double *)old_divtimes;
		div_durations = (double *)old_div_duration;
		full_divtimes = (double *)old_divtimes;
		if ( custom_gast > old_gast_time )
			gast_time = custom_gast;
		else
			gast_time =  old_gast_time;
	} else if ( defs->ndivs == 0 ) {
		div_durations = NULL;
		full_divtimes = (double *)full_divtimes0;
		data_times = (double *)data_times0;
		ndata_times = ndata_times0;
		if ( custom_gast > gast_time0 )
			gast_time = custom_gast;
		else
			gast_time = gast_time0;
	} else if ( defs->ndivs == 1 ) {
		divtimes = (double *)divtimes1;
		div_durations = (double *)div_duration1;
		full_divtimes = (double *)full_divtimes1;
		data_times = (double *)data_times1;
		ndata_times = ndata_times1;
		if ( custom_gast > gast_time1 )
			gast_time = custom_gast;
		else
			gast_time = gast_time1;
	} else if ( defs->ndivs == 2 ) {
		divtimes = (double *)divtimes2;
		div_durations = (double *)div_duration2;
		full_divtimes = (double *)full_divtimes2;
		if ( custom_gast > gast_time2 )
			gast_time = custom_gast;
		else
			gast_time = gast_time2;
	} else if ( defs->ndivs == 3 ) {
		divtimes = (double *)divtimes3;
		div_durations = (double *)div_duration3;
		full_divtimes = (double *)full_divtimes3;
		if ( custom_gast > gast_time3 )
			gast_time = custom_gast;
		else
			gast_time = gast_time3;
	} else if ( defs->ndivs == 4 ) {
		divtimes = (double *)divtimes4;
		div_durations = (double *)div_duration4;
		full_divtimes = (double *)full_divtimes4;
		if ( custom_gast > gast_time4 )
			gast_time = custom_gast;
		else
			gast_time = gast_time4;
	} else
		error("GetNNucs: can't handle %d cell divisions!", defs->ndivs);
}

void InitExternals(FILE *fp)
{
	int               i;                               /* local loop counter */
	int fake_nd;
	Dlist             *inlist;            /* temporary linked list for facts */
	Slist             *current;                      /* types from data file */
	NArrPtr*External;
	if ( !(extinp_interp_object = (InterpObject**)calloc(nalleles, sizeof(InterpObject*))) )
		error("InitExternals: could not allocate");
/*** for loop: read the data for each genotype *****************************/
	for( current = mat_genotypes, i = 0; current; current = current->next, i++) {
		if( !( inlist = ReadData(fp, current->ext_section, &fake_nd, defs->egenes + 1) ) )
			error("InitFacts: no Dlist to initialize facts");
		else {
			External = List2Externals(inlist);
			free_Dlist(inlist);
			if ( scale_maternal_data_flag == 1 ) {
				gcdm_scale_table(scale_maternal_data_factor, External, defs->egenes);
			}
			extinp_interp_object[i] = DoInterp(External, defs->egenes);
			FreeNArrPtr(External);
		}
	}
}


void gcdm_scale_table(double factor, NArrPtr *table, int offset)
{
	int i, n, j, k, ap, ap_ast, ap_length;
	double *buf, ap_zero;
	for ( j = 0; j < table->size; j++ ) {
		buf = (double*)calloc(table->array[j].state->size, sizeof(double));
		ap_length = (int)((double)(table->array[j].state->size) / (double)(offset) + 0.5);
		ap_zero = (double)(table->array[j].state->size) * 0.5 / (double)offset;
		for ( k = 0, ap = 0; k < table->array[j].state->size; k += offset, ap++) {
			ap_ast = (int)(factor * ( (double)ap - ap_zero ) + ap_zero + 0.5);
/* fprintf(stderr, "%d %d\n", ap, ap_ast);*/
			if ( ap_ast < 0 || ap_ast > ap_length - 1 ) {
				for( n = 0; n < offset; n++ ) {
					buf[ap * offset + n] = 0.0;
				}
			} else {
				for( n = 0; n < offset; n++ ) {
					buf[ap * offset + n] = table->array[j].state->array[ ap_ast * offset + n ];
				}
			}
		}
		for ( k = 0; k < table->array[j].state->size; k++) {
			table->array[j].state->array[ k ] = buf[k];
		}
	}
}


/*** InitBicoid: Copies the Blist read by ReadBicoid into the DArrPtr ******
 *               structure; the bicoid DArrPtr contains pointers to bcd    *
 *               arrays for each cell division cycle                       *
 ***************************************************************************/
/* new version */
void InitMaternals(FILE *fp)
{
	int               i;                               /* local loop counter */
	int fake_nd;
	Dlist             *inlist;              /* temporary linked list for bcd */
	Slist             *current;                      /* types from data file */
	if ( !(bcdtype=(GenoType *)calloc(nalleles, sizeof(GenoType))) )
		error("InitMaternals: Could not allocate bcdtype struct");
/*** for loop: read bicoid for each genotype *******************************/
	for ( current = mat_genotypes, i=0; current; current = current->next, i++) {
		if ( !( inlist = ReadData( fp, current->mat_section, &fake_nd, defs->mgenes) ) )   /* read bicoid */
			error("InitMaternals: error reading %s", current->mat_section);
		else {
			if ( !(bcdtype[i].genotype = (char *)calloc(MAX_RECORD, sizeof(char))) )
				error("InitMaternals: could not allocate bcd genotype string");
			bcdtype[i].genotype = strcpy(bcdtype[i].genotype, current->genotype);
			bcdtype[i].ptr = (DataPtr*)malloc(sizeof(DataPtr));
			bcdtype[i].ptr->bicoid = List2Maternals(inlist);
			free_Dlist(inlist);
		}
	}
	if ( scale_maternal_data_flag == 1 ) {
		gcdm_scale_maternals(scale_maternal_data_factor, bcdtype);
	}
}

void gcdm_scale_maternals(double factor, GenoType *maternals)
{
	int i, n, j, k, ap, ap_ast, ap_length;
	double *buf, ap_zero;
	for ( i = 0; i < nalleles; i++ ) {
		for ( j = 0; j < maternals[i].ptr->bicoid->size; j++ ) {
			buf = (double*)calloc(maternals[i].ptr->bicoid->array[j].gradient->size, sizeof(double));
			ap_length = (int)((double)(maternals[i].ptr->bicoid->array[j].gradient->size) / (double)(defs->mgenes) + 0.5);
			ap_zero = (double)(ap_length) * 0.5;
			for ( k = 0, ap = 0; k < maternals[i].ptr->bicoid->array[j].gradient->size; k += defs->mgenes, ap++) {
				ap_ast = (int)(factor * ( (double)ap - ap_zero ) + ap_zero + 0.5);
				if ( ap_ast < 0 || ap_ast > ap_length - 1 ) {
					for( n = 0; n < defs->mgenes; n++ ) {
						buf[ap * defs->mgenes + n] = 0.0;
					}
				} else {
					for( n = 0; n < defs->mgenes; n++ ) {
						buf[ap * defs->mgenes + n] = maternals[i].ptr->bicoid->array[j].gradient->array[ ap_ast * defs->mgenes + n ];
					}
				}
			}
			for ( k = 0; k < maternals[i].ptr->bicoid->array[j].gradient->size; k++) {
				maternals[i].ptr->bicoid->array[j].gradient->array[ k ] = buf[k];
			}
		}
	}
}

/*** InitBias:  puts bias records in a form where get_bias can use them; ***
 *              it expects times in increasing order; it expects a non-    *
 *              sparse entry, with no genes or nuclei missing              *
 ***************************************************************************/

void InitBias(FILE *fp)
{
	int       i;                                            /* loop counters */
	Dlist     *inlist;                     /* temporary linked list for bias */
/*	Slist     *genotypes;                 temporary linked list for geno- */
	Slist     *current;                              /* types from data file */
	int       ndp = 0;  /* dummy for ReadData, no need to count datapts here */
/*	genotypes = ReadGenotypes(fp);
	if ( nalleles == 0 )
		nalleles  = count_Slist(genotypes);*/
	if ( !(biastype=(GenoType *)calloc(nalleles, sizeof(GenoType))) )
		error("InitBias: Could not allocate biastype struct");
/*** for loop: read bicoid for each genotype *******************************/
	for ( current = mat_genotypes, i = 0; current; current=current->next, i++) {
		if( !( inlist = ReadData(fp, current->bias_section, &ndp, defs->ngenes + 1) ) ) /* read bias */
			error("InitBias: error reading %s", current->bias_section);
		else {
			if (!(biastype[i].genotype=(char *)calloc(MAX_RECORD, sizeof(char))))
				error("InitBias: could not allocate bias genotype string");
			biastype[i].genotype = strcpy(biastype[i].genotype, current->name);
			biastype[i].ptr = (DataPtr*)malloc(sizeof(DataPtr));
			biastype[i].ptr->bias = List2Bias(inlist);
			free_Dlist(inlist);
		}
	}
	InitBTs();                     /* initialize the static bias time struct */
/*	free_Slist(genotypes);*/
	if ( scale_maternal_data_flag == 1 ) {
		gcdm_scale_bias(scale_maternal_data_factor, biastype);
	}
}

void gcdm_scale_bias(double factor, GenoType *start_point)
{
	int i, n, j, k, ap, ap_ast;
	double *buf, ap_zero;
	for ( i = 0; i < nalleles; i++ ) {
		gcdm_scale_table(factor, start_point[i].ptr->bias, defs->ngenes);
	}
}

/*** InitBTs: initializes the static BT struct that holds all times for ****
 *            which we have bias.                                          *
 ***************************************************************************/

void InitBTs(void)
{
	int       i, j;
	if ( !bt_init_flag ) {
		if ( !( bt = (GenoType *)calloc(nalleles, sizeof(GenoType))) )
			error("InitBTs: could not allocate bt struct");
		for ( i = 0; i < nalleles; i++) {
			if ( !( bt[i].genotype = (char *)calloc(MAX_RECORD, sizeof(char))) )
				error("InitBTs: could not allocate BT genotype string");
			bt[i].genotype = strcpy(bt[i].genotype, biastype[i].genotype);
			bt[i].ptr = (DataPtr*)malloc(sizeof(DataPtr));
			bt[i].ptr->times = (DArrPtr*)malloc(sizeof(DArrPtr));
			bt[i].ptr->times->size = biastype[i].ptr->bias->size;
			if ( !(bt[i].ptr->times->array = (double *)calloc(bt[i].ptr->times->size, sizeof(double))) )
				error("InitBTs: could not allocate bt array");
			for ( j = 0; j < bt[i].ptr->times->size; j++)
				bt[i].ptr->times->array[j] = biastype[i].ptr->bias->array[j].time;
		}
		bt_init_flag = 1;
	}
}

/*** InitNNucs: takes the global defs->nnucs and calculates number of nucs **
 *              for each cleavage cycle which are then stored in reverse   *
 *              order in the static nnucs[] array                          *
 *   CAUTION:   defs struct and lin_start need to be initialized before!   *
 ***************************************************************************/

void InitNNucs(void)
{
	int           i;                                         /* loop counter */
	int           n; /* used to calculate number of nucs for each cell cycle */
	if ( !(nnucs = (int *)calloc(defs->ndivs+1, sizeof(int))) )
		error("InitNNucs: could not allocate nnucs array");
	if ( model == HYBRID || model == HEDIRECT || model == HESRR || model == HEURR || model == HEQUENCHING || model == HELOGISTIC ) {
		n = defs->nnucs;
/* below we have to take into account two cases: a) most anterior lineage  *
 * number is odd-numbered -> always add a nucleus to the earlier cycle or  *
 * b) most anterior lineage number is even-numbered: just add an additio-  *
 * nal nucleus if last nucleus is odd-numbered (see also exhaustive com-   *
 * ments about this at the DIVIDE rule in Blastoderm() in integrate.c)     */
		for( i = 0; i <= defs->ndivs; i++) {
			nnucs[i] = n;
			if ( lin_start[i] % 2 )
				n = n/2 + 1;
			else
				n = (n % 2) ? n/2 + 1 : n/2;
		}
	} else {
		for( i = 0; i <= defs->ndivs; i++)  nnucs[i] = defs->nnucs;
	}
}

void InitFullNNucs(void)
{
	int           i;                                         /* loop counter */
	int           n; /* used to calculate number of nucs for each cell cycle */
	if (full_nnucs)
		return;
	if ( !(full_nnucs = (int *)calloc(full_ccycles, sizeof(int))) )
		error("InitFullNNucs: could not allocate full_nnucs array");
	if ( model == HYBRID || model == HEDIRECT || model == HESRR || model == HEURR || model == HEQUENCHING || model == HELOGISTIC ) {
		n = defs->nnucs;
/* below we have to take into account two cases: a) most anterior lineage  *
 * number is odd-numbered -> always add a nucleus to the earlier cycle or  *
 * b) most anterior lineage number is even-numbered: just add an additio-  *
 * nal nucleus if last nucleus is odd-numbered (see also exhaustive com-   *
 * ments about this at the DIVIDE rule in Blastoderm() in integrate.c)     */
		for( i = 0; i < full_ccycles; i++) {
			full_nnucs[i] = n;
			if ( full_lin_start[i] % 2 )
				n = n/2 + 1;
			else
				n = (n % 2) ? n/2 + 1 : n/2;
		}
	} else {
		for( i = 0; i <= defs->ndivs; i++)  nnucs[i] = defs->nnucs;
	}
/*  for (i=0; i < full_ccycles; i++)
  	printf("History lineages %d, nnucs %d\n", full_lin_start[i],full_nnucs[i]);*/
}

/*** FUNCTIONS THAT RETURN INFO ABOUT THE EMBRYO **************************/

/*** GetBias: This function returns bias values for a given time and *******
 *            genotype.                                                    *
 ***************************************************************************/

DArrPtr *GetBias(double time)
{
	int j;
	for( j = 0; j < biastype[GINDEX].ptr->bias->size; j++) {
		if (biastype[GINDEX].ptr->bias->array[j].time == time)
			break;
	}
	return biastype[GINDEX].ptr->bias->array[j].state;
}

/*** GetBTimes: returns a sized array of times for which there is bias *****
 ***************************************************************************/
DArrPtr* GetBTimes(char *genotype)
{
	int index;                                               /* loop counter */
	for(index=0; index<nalleles; index++)
		if ( !(strcmp(biastype[index].genotype, genotype)) )
			break;
/* if no explicit bias times for this genotype -> use wt bias times */
	if ( index == nalleles )
		index = 0;
/* check if we actually have biastimes at all or otherwise -> error! */
	if ( bt_init_flag )
		return bt[index].ptr->times;
	else
		error("GetBTimes: called without initialized BTs");
	return bt[index].ptr->times;
}

/*** GetNNucs: reads number of nuclei for a given time *********************
 ***************************************************************************/
int GetNNucs(double t)
{
	int      i;                                              /* loop counter */
	if ( defs->ndivs == 0 )
		return nnucs[0];
/* evaluate nnucs for current time; note that for the *exact* time of cell *
 * division, we'll return the number of nuclei before the division has ac- *
 * tually occurred                                                         */
	for ( i = 0; i < defs->ndivs; i++)
		if ( t > divtimes[i] )
			return nnucs[i];
	return nnucs[i];
}

/*** GetStartLin: returns the lineage number of the most anterior nucleus **
 *                for a given time                                         *
 ***************************************************************************/

int GetStartLin(double t)
{
	int      i;                                              /* loop counter */
/* evaluate lineage number of most anterior nucleus for current time; note *
 * that for the *exact* time of cell division, we'll return the lineage    *
 * number of the most anterior nucleus of the previous cell cycle          */
	for ( i = 0; i < defs->ndivs; i++)
		if ( t > divtimes[i] )
			return lin_start[i];
	return lin_start[i];
}

/*** GetCCycle: returns cleavage cycle number for a given time *************
 ***************************************************************************/
unsigned int GetCCycle(double time)
{
	int      i;                                              /* loop counter */
	if ( defs->ndivs == 0 )
		return 14;           /* if defs->ndivs == 0: we're always in cycle 14 */
/* evaluate number of cell cycle for current time; note that for the exact *
 * time of cell division, we'll return the number of the previous cell cy- *
 * cyle                                                                    */
	for ( i = 0; i < defs->ndivs; i++)
		if ( time > divtimes[i] )
			return 14 - i;
	return 14-(i++);
}

/*** ParseLineage: takes lineage number as input and returns the cleavage **
 *                 cycle the nucleus belongs to.                           *
 ***************************************************************************/
unsigned int ParseLineage(unsigned int lin)
{
	if ( lin & CYCLE14 )
		return 14;
	else if ( lin & CYCLE13 )
		return 13;
	else if ( lin & CYCLE12 )
		return 12;
	else if ( lin & CYCLE11 )
		return 11;
	else if ( lin & CYCLE10 )
		return 10;
	else
		error("ParseLineage: illegal lineage number %d", lin);
}

/*** GetDivtable: returns times of cell divisions depending on ndivs and ***
 *                olddivstyle; returns NULL in case of an error            *
 ***************************************************************************/
double *GetDivtable(void)
{
	return divtimes;
}

/*** GetDurations: returns pointer to durations of cell divisions de- ******
 *                 pending on ndivs and olddivstyle; returns NULL in case  *
 *                 of an error                                             *
 ***************************************************************************/
double *GetDurations(void)
{
	return div_durations;
}

/*** GetGastTime: returns time of gastrulation depending on ndivs and ******
 *                olddivstyle; returns 0 in case of an error; if a custom  *
 *                gastrulation time is chosen with -S, it will be returned *
 *                only if it's bigger than the normal gastrulation time    *
 ***************************************************************************/
double GetGastTime(void)
{
	return gast_time;
}

/*** GetD: returns diffusion parameters D according to the diff. params. ***
 *         in the data file and the diffusion schedule used                *
 *   NOTE: Caller must allocate D_tab                                      *
 ***************************************************************************/
void GetD(double t, double *d, char diff_schedule, double *D_tab)
{
	int           i;                                         /* loop counter */
	double        cutoff;                                     /* cutoff time */
	double        lscale = 1;                              /* scaling factor */
	double        length;
	if ( model != HYBRID && model != HEDIRECT && model != HESRR && model != HEURR && model != HEQUENCHING && model != HELOGISTIC ) {
		length = (double)defs->nnucs - 1.0;
/* to be compatible with the older versions
 * of codes for continuum model we have to
 * to do the following:*/
		if((diff_schedule != 'A') &&(diff_schedule != 'D') )
			for(i = 0; i < defs->ndivs; i++)
				length *=0.5;
		if(diff_schedule == 'A')
			for(i=0; i<defs->ngenes; i++)
				D_tab[i] = d[0] * length * length;
/* diffusion schedule B: all genes have different D's that depend on in-   *
 * verse l-square                                                          */
		else if (diff_schedule == 'B')
			for(i=0; i<defs->ngenes; i++ )
				D_tab[i] = d[i] * length * length;
/* diffusion schedule C: all genes have the same D that depends on inverse *
 * l-square                                                                */
		else if (diff_schedule == 'C')
			for(i=0; i<defs->ngenes; i++)
				D_tab[i] = d[0]  * length * length;
/* diffusion schedule D: all genes have different D's which don't change   *
 * over time                                                               */
		else if (diff_schedule == 'D')
			for(i=0; i<defs->ngenes; i++ )
				D_tab[i] = d[i]  * length * length;
/* diffusion schedule E: used cutoff at gast-12 otherwise just like B */
		else if (diff_schedule == 'E') {
			cutoff = gast_time - 12.0;      /* This value probably wrong; see Merrill88 */
			for(i=0; i<defs->ngenes; i++ )
				D_tab[i] = (t < cutoff) ?  d[i]  * length * length : 0.;
/* any other diffusion schedule: error! */
		} else
			error("GetD: no code for schedule %c!", diff_schedule);
	} else {
/* first time GetD is called: set pointer to the right division table */
/* this loop takes lscale square for each cell division, i.e. the earlier  *
 * we are the bigger lscale (and the smaller the D's that we return        */
		for( i = 0; i < defs->ndivs; i++)
			if ( t < divtimes[i])
				lscale *= 2;
/* diffusion schedule A: all Ds always the same */
		if(diff_schedule == 'A')
			for( i = 0; i < defs->ngenes; i++)
				D_tab[i] = d[0];
/* diffusion schedule B: all genes have different D's that depend on in-   *
 * verse l-square                                                          */
		else if (diff_schedule == 'B')
			for( i = 0; i < defs->ngenes; i++ )
				D_tab[i] = d[i] / (lscale * lscale);
/* diffusion schedule C: all genes have the same D that depends on inverse *
 * l-square                                                                */
		else if (diff_schedule == 'C')
			for( i = 0; i < defs->ngenes; i++)
				D_tab[i] = d[0] / (lscale * lscale);
/* diffusion schedule D: all genes have different D's which don't change   *
 * over time                                                               */
		else if (diff_schedule == 'D')
			for( i = 0; i < defs->ngenes; i++ )
				D_tab[i] = d[i];
/* diffusion schedule E: used cutoff at gast-12 otherwise just like B */
		else if (diff_schedule == 'E') {
			cutoff = gast_time - 12.0;      /* This value probably wrong; see Merrill88 */
			for(i=0; i<defs->ngenes; i++ )
				D_tab[i] = (t < cutoff) ?  d[i] / (lscale * lscale) : 0.;
		} else if (diff_schedule == 'F') {
			unsigned int ccycle = GetCCycle(t);
			if ( ccycle < 14 )
				for(i=0; i<defs->ngenes; i++ )
					D_tab[i] = d[i] / (lscale * lscale);
			else
				for(i=0; i<defs->ngenes; i++ )
					D_tab[i] = 0.0;
/* any other diffusion schedule: error! */
		} else
			error("GetD: no code for schedule %c!", diff_schedule);
	}
}

void GetM(double t, double *m, char mob_schedule, double *M_tab)
{
	int           i;                                         /* loop counter */
	double        cutoff;                                     /* cutoff time */
	double        lscale = 1;                              /* scaling factor */
	double        length;
	if ( model != HYBRID && model != HEDIRECT && model != HESRR && model != HEURR && model != HEQUENCHING && model != HELOGISTIC ) {
		length = (double)defs->nnucs - 1.0;
/* to be compatible with the older versions
 * of codes for continuum model we have to
 * to do the following:*/
		if((mob_schedule != 'A') &&(mob_schedule != 'D') )
			for(i = 0; i < defs->ndivs; i++)
				length *=0.5;
		if(mob_schedule == 'A')
			for(i=0; i<defs->ngenes; i++)
				M_tab[i] = m[0] * length * length * length * length;
/* diffusion schedule B: all genes have different D's that depend on in-   *
 * verse l-square                                                          */
		else if (mob_schedule == 'B')
			for(i=0; i<defs->ngenes; i++ )
				M_tab[i] = m[i] * length * length * length * length;
/* diffusion schedule C: all genes have the same D that depends on inverse *
 * l-square                                                                */
		else if (mob_schedule == 'C')
			for(i=0; i<defs->ngenes; i++)
				M_tab[i] = m[0]  * length * length * length * length;
/* diffusion schedule D: all genes have different D's which don't change   *
 * over time                                                               */
		else if (mob_schedule == 'D')
			for(i=0; i<defs->ngenes; i++ )
				M_tab[i] = m[i]  * length * length * length * length;
/* diffusion schedule E: used cutoff at gast-12 otherwise just like B */
		else if (mob_schedule == 'E') {
			cutoff = gast_time - 12.0;      /* This value probably wrong; see Merrill88 */
			for(i=0; i<defs->ngenes; i++ )
				M_tab[i] = (t < cutoff) ?  m[i]  * length * length * length * length : 0.;
/* any other diffusion schedule: error! */
		} else
			error("GetD: no code for schedule %c!", mob_schedule);
	} else {
/* first time GetD is called: set pointer to the right division table */
/* this loop takes lscale square for each cell division, i.e. the earlier  *
 * we are the bigger lscale (and the smaller the D's that we return        */
		for( i = 0; i < defs->ndivs; i++)
			if ( t < divtimes[i])
				lscale *= 2;
/* diffusion schedule A: all Ds always the same */
		if(mob_schedule == 'A')
			for( i = 0; i < defs->ngenes; i++)
				M_tab[i] = m[0];
/* diffusion schedule B: all genes have different D's that depend on in-   *
 * verse l-square                                                          */
		else if (mob_schedule == 'B')
			for( i = 0; i < defs->ngenes; i++ )
				M_tab[i] = m[i] / (lscale * lscale * lscale * lscale);
/* diffusion schedule C: all genes have the same D that depends on inverse *
 * l-square                                                                */
		else if (mob_schedule == 'C')
			for( i = 0; i < defs->ngenes; i++)
				M_tab[i] = m[0] / (lscale * lscale * lscale * lscale);
/* diffusion schedule D: all genes have different D's which don't change   *
 * over time                                                               */
		else if (mob_schedule == 'D')
			for( i = 0; i < defs->ngenes; i++ )
				M_tab[i] = m[i];
/* diffusion schedule E: used cutoff at gast-12 otherwise just like B */
		else if (mob_schedule == 'E') {
			cutoff = gast_time - 12.0;      /* This value probably wrong; see Merrill88 */
			for(i=0; i<defs->ngenes; i++ )
				M_tab[i] = (t < cutoff) ?  m[i] / (lscale * lscale * lscale * lscale) : 0.;
		} else if (mob_schedule == 'F') {
			unsigned int ccycle = GetCCycle(t);
			if ( ccycle >= 14 )
				for(i=0; i<defs->ngenes; i++ )
					M_tab[i] = m[i] / (lscale * lscale * lscale * lscale);
			else
				for(i=0; i<defs->ngenes; i++ )
					M_tab[i] = 0.0;
/* any other diffusion schedule: error! */
		} else
			error("GetD: no code for schedule %c!", mob_schedule);
	}
}

void GetMM(double t, double *m, char mob_schedule, double *M_tab)
{
	int           i;                                         /* loop counter */
	double        cutoff;                                     /* cutoff time */
	double        lscale = 1;                              /* scaling factor */
	double        length;
	if ( model != HYBRID && model != HEDIRECT && model != HESRR && model != HEURR && model != HEQUENCHING && model != HELOGISTIC ) {
		length = (double)defs->nnucs - 1.0;
/* to be compatible with the older versions
 * of codes for continuum model we have to
 * to do the following:*/
		if((mob_schedule != 'A') &&(mob_schedule != 'D') )
			for(i = 0; i < defs->ndivs; i++)
				length *=0.5;
		if(mob_schedule == 'A')
			for(i=0; i<defs->ngenes; i++)
				M_tab[i] = m[0] * length * length * length * length;
/* diffusion schedule B: all genes have different D's that depend on in-   *
 * verse l-square                                                          */
		else if (mob_schedule == 'B')
			for(i=0; i<defs->ngenes; i++ )
				M_tab[i] = m[i] * length * length * length * length;
/* diffusion schedule C: all genes have the same D that depends on inverse *
 * l-square                                                                */
		else if (mob_schedule == 'C')
			for(i=0; i<defs->ngenes; i++)
				M_tab[i] = m[0]  * length * length * length * length;
/* diffusion schedule D: all genes have different D's which don't change   *
 * over time                                                               */
		else if (mob_schedule == 'D')
			for(i=0; i<defs->ngenes; i++ )
				M_tab[i] = m[i]  * length * length * length * length;
/* diffusion schedule E: used cutoff at gast-12 otherwise just like B */
		else if (mob_schedule == 'E') {
			cutoff = gast_time - 12.0;      /* This value probably wrong; see Merrill88 */
			for(i=0; i<defs->ngenes; i++ )
				M_tab[i] = (t < cutoff) ?  m[i]  * length * length * length * length : 0.;
/* any other diffusion schedule: error! */
		} else
			error("GetD: no code for schedule %c!", mob_schedule);
	} else {
/* first time GetD is called: set pointer to the right division table */
/* this loop takes lscale square for each cell division, i.e. the earlier  *
 * we are the bigger lscale (and the smaller the D's that we return        */
		for( i = 0; i < defs->ndivs; i++)
			if ( t < divtimes[i])
				lscale *= 2;
/* diffusion schedule A: all Ds always the same */
		if(mob_schedule == 'A')
			for( i = 0; i < defs->ngenes; i++)
				M_tab[i] = m[0];
/* diffusion schedule B: all genes have different D's that depend on in-   *
 * verse l-square                                                          */
		else if (mob_schedule == 'B')
			for( i = 0; i < defs->ngenes; i++ )
				M_tab[i] = m[i] / (lscale * lscale * lscale * lscale);
/* diffusion schedule C: all genes have the same D that depends on inverse *
 * l-square                                                                */
		else if (mob_schedule == 'C')
			for( i = 0; i < defs->ngenes; i++)
				M_tab[i] = m[0] / (lscale * lscale * lscale * lscale);
/* diffusion schedule D: all genes have different D's which don't change   *
 * over time                                                               */
		else if (mob_schedule == 'D')
			for( i = 0; i < defs->ngenes; i++ )
				M_tab[i] = m[i];
/* diffusion schedule E: used cutoff at gast-12 otherwise just like B */
		else if (mob_schedule == 'E') {
			cutoff = gast_time - 12.0;      /* This value probably wrong; see Merrill88 */
			for(i=0; i<defs->ngenes; i++ )
				M_tab[i] = (t < cutoff) ?  m[i] / (lscale * lscale * lscale * lscale) : 0.;
		} else if (mob_schedule == 'F') {
			unsigned int ccycle = GetCCycle(t);
			if ( ccycle >= 13 )
				for(i=0; i<defs->ngenes; i++ )
					M_tab[i] = m[i] / (lscale * lscale * lscale * lscale);
			else
				for(i=0; i<defs->ngenes; i++ )
					M_tab[i] = 0.0;
/* any other diffusion schedule: error! */
		} else
			error("GetD: no code for schedule %c!", mob_schedule);
	}
}


void GetL(double t, double *m, char mob_schedule, double *M_tab)
{
	int           i;                                         /* loop counter */
	double        cutoff;                                     /* cutoff time */
	double        lscale = 1;                              /* scaling factor */
	double        length;
	if ( model != HYBRID && model != HEDIRECT && model != HESRR && model != HEURR && model != HEQUENCHING && model != HELOGISTIC ) {
		length = (double)defs->nnucs - 1.0;
		if((mob_schedule != 'A') &&(mob_schedule != 'D') )
			for(i = 0; i < defs->ndivs; i++)
				length *=0.5;
		if(mob_schedule == 'A')
			for(i=0; i<defs->ngenes; i++)
				M_tab[i] = m[0] * length * length * length * length;
		else if (mob_schedule == 'B')
			for(i=0; i<defs->ngenes; i++ )
				M_tab[i] = m[i] * length * length;
		else if (mob_schedule == 'C')
			for(i=0; i<defs->ngenes; i++)
				M_tab[i] = m[0]  * length * length;
		else if (mob_schedule == 'D')
			for(i=0; i<defs->ngenes; i++ )
				M_tab[i] = m[i]  * length * length * length * length;
		else if (mob_schedule == 'E') {
			cutoff = gast_time - 12.0;      /* This value probably wrong; see Merrill88 */
			for(i=0; i<defs->ngenes; i++ )
				M_tab[i] = (t < cutoff) ?  m[i]  * length * length * length * length : 0.;
		} else
			error("GetL: no code for schedule %c!", mob_schedule);
	} else {
		for( i = 0; i < defs->ndivs; i++)
			if ( t < divtimes[i])
				lscale *= 2;
		if(mob_schedule == 'A')
			for( i = 0; i < defs->ngenes; i++)
				M_tab[i] = m[0];
		else if (mob_schedule == 'B')
			for( i = 0; i < defs->ngenes; i++ )
				M_tab[i] = m[i] / (lscale * lscale);
		else if (mob_schedule == 'C')
			for( i = 0; i < defs->ngenes; i++)
				M_tab[i] = m[0] / (lscale * lscale);
		else if (mob_schedule == 'D')
			for( i = 0; i < defs->ngenes; i++ )
				M_tab[i] = m[i];
		else if (mob_schedule == 'E') {
			cutoff = gast_time - 12.0;      /* This value probably wrong; see Merrill88 */
			for(i=0; i<defs->ngenes; i++ )
				M_tab[i] = (t < cutoff) ?  m[i] / (lscale * lscale) : 0.;
		} else if (mob_schedule == 'F') {
			unsigned int ccycle = GetCCycle(t);
			if ( ccycle >= 14 )
				for(i=0; i<defs->ngenes; i++ )
					M_tab[i] = m[i] / (lscale * lscale);
			else
				for(i=0; i<defs->ngenes; i++ )
					M_tab[i] = 0.0;
		} else
			error("GetL: no code for schedule %c!", mob_schedule);
	}
}

void GetLL(double t, double *m, char mob_schedule, double *M_tab)
{
	int           i;                                         /* loop counter */
	double        cutoff;                                     /* cutoff time */
	double        lscale = 1;                              /* scaling factor */
	double        length;
	if ( model != HYBRID && model != HEDIRECT && model != HESRR && model != HEURR && model != HEQUENCHING && model != HELOGISTIC ) {
		length = (double)defs->nnucs - 1.0;
		if((mob_schedule != 'A') &&(mob_schedule != 'D') )
			for(i = 0; i < defs->ndivs; i++)
				length *=0.5;
		if(mob_schedule == 'A')
			for(i=0; i<defs->ngenes; i++)
				M_tab[i] = m[0] * length * length;
		else if (mob_schedule == 'B')
			for(i=0; i<defs->ngenes; i++ )
				M_tab[i] = m[i] * length * length;
		else if (mob_schedule == 'C')
			for(i=0; i<defs->ngenes; i++)
				M_tab[i] = m[0] * length * length;
		else if (mob_schedule == 'D')
			for(i=0; i<defs->ngenes; i++ )
				M_tab[i] = m[i] * length * length;
		else if (mob_schedule == 'E') {
			cutoff = gast_time - 12.0;      /* This value probably wrong; see Merrill88 */
			for(i=0; i<defs->ngenes; i++ )
				M_tab[i] = (t < cutoff) ?  m[i] * length * length : 0.;
		} else
			error("GetLL: no code for schedule %c!", mob_schedule);
	} else {
		for( i = 0; i < defs->ndivs; i++)
			if ( t < divtimes[i])
				lscale *= 2;
		if(mob_schedule == 'A')
			for( i = 0; i < defs->ngenes; i++)
				M_tab[i] = m[0];
		else if (mob_schedule == 'B')
			for( i = 0; i < defs->ngenes; i++ )
				M_tab[i] = m[i] / (lscale * lscale);
		else if (mob_schedule == 'C')
			for( i = 0; i < defs->ngenes; i++)
				M_tab[i] = m[0] / (lscale * lscale);
		else if (mob_schedule == 'D')
			for( i = 0; i < defs->ngenes; i++ )
				M_tab[i] = m[i];
		else if (mob_schedule == 'E') {
			cutoff = gast_time - 12.0;      /* This value probably wrong; see Merrill88 */
			for(i=0; i<defs->ngenes; i++ )
				M_tab[i] = (t < cutoff) ?  m[i] / (lscale * lscale) : 0.;
		} else if (mob_schedule == 'F') {
			unsigned int ccycle = GetCCycle(t);
			if ( ccycle >= 13 )
				for(i=0; i<defs->ngenes; i++ )
					M_tab[i] = m[i] / (lscale * lscale);
			else
				for(i=0; i<defs->ngenes; i++ )
					M_tab[i] = 0.0;
		} else
			error("GetLL: no code for schedule %c!", mob_schedule);
	}
}

/*** GetRule: returns the appropriate rule for a given time; used by the ***
 *            derivative function                                          *
 ***************************************************************************/

int GetRule(double time)
{
	int i;
/* the following block of code are implemented like this (instead of       */
/* calling GetDivtable or GetDurations) to save function calls and increa- */
/* se performance (GetRule is called from within the inner loop)           */
	if ( model == NOMITOSIS )
		return INTERPHASE;
	if ( defs->ndivs == 0 )
		return INTERPHASE;                /* no cell division? no MITOSIS! */
/* checks if we're in a mitosis; we need the 10*DBL_EPSILON kludge for gcc *
 * on Linux which can't handle truncation errors very well                 */
	for( i = 0; i < defs->ndivs; i++)
		if ((time <= (*(divtimes+i) + HALF_EPSILON)) && (time >= (*(divtimes+i) - *(div_durations+i) - 10*HALF_EPSILON)))
			return MITOSIS;
	return INTERPHASE;
}

/*** GetIndex: this functions returns the genotype index for a given *******
 *             genotype number for reading the GenoType struct.            *
 ***************************************************************************/
int GetIndex(char *genotype)
{
	int i;
	Slist *curr;
	for ( i = 0, curr = mat_genotypes; i < nalleles; i++, curr = curr->next) /* nalleles static to score.c */
		if( !(strcmp(curr->name, genotype)) )
			return i;
	error("GetIndex: could not find index for genotype %s", genotype);
	return -1000;
}

/*** MakeTable: this function constructs a time table for Blastoderm *******
 *             based on a comand line option (print_stepsize) and the      *
 *             cell division tables here in maternal.c                     *
 * DISCLAIMER: this thing is written in a very bad way; but hey it does    *
 *             its job!!!!!!!                                              *
 ***************************************************************************/
DArrPtr* MakeTable(double p_stepsize)
{
	int        i;                                      /* local loop counter */
	int        t;                                            /* time counter */
	double     time;                                       /* double counter */
	double     gast;                                    /* gastrulation time */
	DArrPtr    *table;                           /* time table to be returned */
	table = (DArrPtr*)malloc(sizeof(DArrPtr));
	gast = GetGastTime();                           /* get gastrulation time */
	if ( p_stepsize > gast )
		error("GetTable: output stepsize can't be larger than gast time");
	t=0;
	for ( time=0; time<gast; time+=p_stepsize ) {
		t++;
	}
	table->size = t+1;                /* add one for gastrulation time itself */
	if ( !(table->array=(double *)calloc(table->size, sizeof(double))) )
		error("GetTable: error allocating times array");
	time=0;
	for ( i=0; i<table->size-1; i++ ) {
		table->array[i] = time;
		time+=p_stepsize;
	}
	table->array[i] = gast;
	return table;
}

/*** FUNCTIONS THAT READ DATA FROM FILE INTO STRUCTS (BIAS & BCD) *********/

/*** ReadTheProblem: reads the problem section of a data file into the *****
 *                   TheProblem struct.                                    *
 ***************************************************************************/
TheProblem *ReadTheProblem(FILE *fp)
{
	TheProblem*p;           /* local copy of TheProblem struct */
	char g1, g2;
	int i, j, k, f;
	char*couples_string;
	fp = FindSection(fp, "problem");                 /* find problem section */
	if( !fp )
		error("ReadTheProblem: cannot locate problem section");
	fscanf(fp, "%*s\n");             /* advance pointer past first text line */
	p = (TheProblem*)malloc(sizeof(TheProblem));
	if ( 1 != (fscanf(fp, "%d\n", &(p->ngenes)) ))
		error("ReadTheProblem: error reading problem section (ngenes)");
	fscanf(fp, "%*s\n");    /* advance the pointer past the second text line */
	p->gene_ids = (char *)calloc(p->ngenes+1, sizeof(char));
	if ( 1 != (fscanf(fp, "%s\n", p->gene_ids)) ) /* read geneID string */
		error("ReadTheProblem: error reading problem section (gene_ids)");
	fscanf(fp, "%*s\n");                       /* next line (ignore comment) */
	if ( 1 != (fscanf(fp, "%d\n", &(p->egenes)) ))
		error("ReadTheProblem: error reading problem section (ngenes)");
	fscanf(fp, "%*s\n");    /* advance the pointer past the second text line */
	p->egene_ids = (char *)calloc(p->egenes+1, sizeof(char));
	if (p->egenes != 0)
		if ( 1 != (fscanf(fp, "%s\n", p->egene_ids)) ) /* read geneID string */
			error("ReadTheProblem: error reading problem section (gene_ids)");
	fscanf(fp, "%*s\n");                       /* next line (ignore comment) */
	if ( 1 != (fscanf(fp, "%d\n", &(p->mgenes)) ))
		error("ReadTheProblem: error reading problem section (ngenes)");
	fscanf(fp, "%*s\n");    /* advance the pointer past the second text line */
	p->mgene_ids = (char *)calloc(p->mgenes+1, sizeof(char));
	if (p->mgenes != 0)
		if ( 1 != (fscanf(fp, "%s\n", p->mgene_ids)) ) /* read geneID string */
			error("ReadTheProblem: error reading problem section (gene_ids)");
	fscanf(fp, "%*s\n");                       /* next line (ignore comment) */
	if ( 1 != (fscanf(fp, "%d\n", &(p->ncouples)) ))
		error("ReadTheProblem: error reading problem section (ngenes)");
	fscanf(fp, "%*s\n");    /* advance the pointer past the second text line */
	couples_string = (char *)calloc(MAX_RECORD, sizeof(char));
	if ( p->ncouples != 0) {
		if ( 1 != (fscanf(fp, "%s\n", couples_string)) ) /* read geneID string */
			error("ReadTheProblem: error reading problem section (couples_string)");
		p->couples = (Couple*)calloc(p->ncouples, sizeof(Couple));
		for ( i = 0, j = 0; i < p->ncouples; i++, j+=3 ) {
			g1 = couples_string[j];
			g2 = couples_string[j + 1];
			f = 0;
			for ( k = 0; k < p->ngenes; k++ ) {
				if ( g1 == p->gene_ids[k] ) {
					f = 1;
					p->couples[i].first_gene_type = zygotic;
					p->couples[i].first_gene_id = k;
				}
				if ( g2 == p->gene_ids[k] ) {
					f = 1;
					p->couples[i].second_gene_type = zygotic;
					p->couples[i].second_gene_id = k;
				}
			}
			for ( k = 0; k < p->egenes; k++ ) {
				if ( g1 == p->egene_ids[k] ) {
					f = 1;
					p->couples[i].first_gene_type = external;
					p->couples[i].first_gene_id = k;
				}
				if ( g2 == p->egene_ids[k] ) {
					f = 1;
					p->couples[i].second_gene_type = external;
					p->couples[i].second_gene_id = k;
				}
			}
			for ( k = 0; k < p->mgenes; k++ ) {
				if ( g1 == p->mgene_ids[k] ) {
					f = 1;
					p->couples[i].first_gene_type = maternal;
					p->couples[i].first_gene_id = k;
				}
				if ( g2 == p->mgene_ids[k] ) {
					f = 1;
					p->couples[i].second_gene_type = maternal;
					p->couples[i].second_gene_id = k;
				}
			}
			p->couples[i].g1 = g1;
			p->couples[i].g2 = g2;
		}
	}
	fscanf(fp, "%*s\n");                       /* next line (ignore comment) */
	if ( 1 != (fscanf(fp, "%d\n", &(p->ndivs)) ))
		error("ReadTheProblem: error reading problem section (ndivs)");
	fscanf(fp, "%*s\n");     /* advance the pointer past the third text line */
	if ( 1 != (fscanf(fp, "%d\n", &(p->nnucs)) ))
		error("ReadTheProblem: error reading problem section (nnucs)");
	fscanf(fp, "%*s\n");                    /* advance the pointer once more */
	if ( 1 != (fscanf(fp, "%c\n", &(p->diff_schedule))))
		error("ReadTheProblem: error reading problem section (diff. schedule)");
	fscanf(fp, "%*s\n");                    /* advance the pointer once more */
	if ( 1 != (fscanf(fp, "%c\n", &(p->mob_schedule))))
		error("ReadTheProblem: error reading problem section (mob. schedule)");
	free(couples_string);
	return p;
}


/*** ReadGenotypes: This function reads all the genotypes in a datafile & **
 *                  returns an SList with genotype number and pointers to  *
 *                  the corresponding section titles for bias, bcd & facts *
 ***************************************************************************/
Slist *ReadGenotypes(FILE *fp)
{
	char      namebuf[MAX_RECORD];      /* title of genotype     */
	char      biasbuf[MAX_RECORD];      /* section title of bias section     */
	char      factsbuf[MAX_RECORD];     /* section title of data section     */
	char      matbuf[MAX_RECORD];       /* section title of bcd section      */
	char      gtbuf[MAX_RECORD];        /* genotype string                   */
	char      histbuf[MAX_RECORD];       /* section title of history section      */
	char      extbuf[MAX_RECORD];       /* section title of external  input section      */
	char      *record;                  /* pointer to current data record    */
	Slist     *current;                 /* holds current element of Slist    */
	Slist     *first;                   /* pointer to first element of Slist */
	Slist     *last = NULL;             /* pointer to last element of Slist  */
/*** open the data file and locate genotype section ************************/
	fp = FindSection(fp, "genotypes");
	if( !fp )
		error("ReadGenotypes: cannot locate genotypes");
	if ( !(record=(char *)calloc(MAX_RECORD, sizeof(char))) )
		error("ReadGenotypes: error allocating record");
	first   = init_Slist();
	current = first;
/*** read titles of bias, data and bcd sections and genotype number for    *
 *   each genotype *********************************************************/
	while ( strncmp((record=fgets(record, MAX_RECORD, fp)), "$$", 2)) {
		if ( 7 != sscanf(record, "%s %s %s %s %s %s %s", namebuf, biasbuf, factsbuf, matbuf, histbuf, extbuf, gtbuf) )
			error("ReadGenotypes: error reading %s", record);
/* we eventually want to get rid of the hard wired genotype identifiers    *
 * and replace them by some kind of mechanism by which we can include any  *
 * genes we want in a scoring or annealing run. Think about this!          */
		if ( strlen(gtbuf) != defs->ngenes )
			error("ReadGenotypes: bad genotype string %s (does not match ngenes)", gtbuf);
		if ( !current )
			current = init_Slist();
		if (!(current->name = (char *)calloc(MAX_RECORD, sizeof(char))))
			error("ReadGenotypes: error allocating name");
		current->name = strcpy(current->name, namebuf);
		if (!(current->bias_section = (char *)calloc(MAX_RECORD, sizeof(char))))
			error("ReadGenotypes: error allocating bias_section");
		current->bias_section = strcpy(current->bias_section, biasbuf);
		if (!(current->fact_section = (char *)calloc(MAX_RECORD, sizeof(char))))
			error("ReadGenotypes: error allocating fact_section");
		current->fact_section = strcpy(current->fact_section, factsbuf);
		if (!(current->mat_section  = (char *)calloc(MAX_RECORD, sizeof(char))))
			error("ReadGenotypes: error allocating bcd_section");
		current->mat_section  = strcpy(current->mat_section, matbuf);
		if (!(current->hist_section  = (char *)calloc(MAX_RECORD, sizeof(char))))
			error("ReadGenotypes: error allocating hist_section");
		current->hist_section  = strcpy(current->hist_section, histbuf);
		if (!(current->ext_section  = (char *)calloc(MAX_RECORD, sizeof(char))))
			error("ReadGenotypes: error allocating ext_section");
		current->ext_section  = strcpy(current->ext_section, extbuf);
		if (!(current->genotype     = (char *)calloc(MAX_RECORD, sizeof(char))))
			error("ReadGenotypes: error allocating genotype string");
		current->genotype     = strcpy(current->genotype, gtbuf);
		addto_Slist(last, current);
		last = current;
		current = NULL;
	}
	free(record);
	return first;
}

/*** ReadBicoid: reads the bcd section of a data file into a linked list; **
 *               also determines maxconc from the bicoid gradient          *
 ***************************************************************************/
Blist *ReadBicoid(FILE *fp, char *section)
{
  int       c;                           /* holds char for parser          */
  int       lead_punct;                  /* flag for leading punctuation   */

  double    maxv = -1.;                  /* maximum v (protein conc.)      */

  char      *base;                       /* pointer to beginning of string */
  char      *record;                     /* pointer to string, used as     */
                                         /* counter                        */
  Blist     *current;                    /* holds current element of Blist */
  Blist     *inlist;                     /* holds whole read Blist         */

  if ( (fp=FindSection(fp, section)) ) {                /* position the fp */

    base    = (char *)calloc(MAX_RECORD, sizeof(char));
    current = NULL;
    inlist  = NULL;

/* while loop: reads and processes lines from file until sections ends *****/

    while ( strncmp(( base=fgets(base, MAX_RECORD, fp)), "$$", 2 )) {

      record     = base;                    /* base always points to start */
      lead_punct = 0;                                         /* of string */

/* for loop: parses and processes each line from the data file *************/

      c=(int)*record;
      while ( c != '\0' ) {

	if( isdigit(c) ) {                            /* number means data */

	  record = base;                  /* reset pointer to start of str */
	  current = init_Blist();	  /* allocate memory for lnkd list */

	  if ( 2 != sscanf(record, "%d %lg",
			   &(current->lineage), &(current->conc)) )
	    error("ReadBicoid: error reading %s", base);

	  if ( current->conc > maxv )
	    maxv = current->conc;

	  inlist = addto_Blist(inlist, current);
	  break;

	}
	else if ( isalpha(c) ) {                   /* letter means comment */
	  break;
	}
	else if ( c == '-' ) {                /* next two elsifs for punct */
	  if ( ((int) *(record+1)) == '.' )
	    record++;
	  lead_punct = 1;
	  c=(int)*(++record);
	}
	else if ( c == '.' ) {
	  lead_punct = 1;
	  c=(int)*(++record);
	}
	else if ( ispunct(c) ) {              /* other punct means comment */
	  break;
	}
	else if ( isspace(c) ) {             /* ignore leading white space */
	  if ( lead_punct )               /* white space after punct means */
	    break;                                              /* comment */
	  else {
	    c=(int)*(++record);            /* get next character in record */
	  }
	}
	else {
	  error("ReadBicoid: illegal character in %s", base);
	}
      }
    }

    if ( maxv > 12. )                        /* oldstyle or newstyle data? */
      maxconc = 255.;
    else
      maxconc = 12.;

    free(base);
    return inlist;

  } else {

    return NULL;
  }
}

/*** ReadData: reads in a data or bias section and puts it in a linked *****
 *             list of arrays, one line per array; ndp is used to count    *
 *             the number of data points in a data file (ndp), which is    *
 *             used to calculate the root mean square (RMS) if required    *
 *                                                                         *
 *             ReadData allows comments that start with a letter or punc-  *
 *             tuation mark, and treats lines starting with a number or    *
 *             .num or -.num as data. It expects to read an int and        *
 *             ngenes + 1 data points: lineage, time and ngenes protein    *
 *             concentrations. It expects data to be in increasing spatial *
 *             and temporal order.                                         *
 ***************************************************************************/

Dlist *ReadData(FILE *fp , char *section, int *ndp, int size)
{
	int     c;                           /* holds char for parser            */
	int     lead_punct;                  /* flag for leading punctuation     */
	int     i;                           /* loop counter                     */
	char    *base;                       /* pointer to beginning of string   */
	char    *record;                     /* pointer to string, used as counter                          */
	Dlist   *current;                    /* holds current element of Dlist   */
	Dlist   *inlist;                     /* holds whole read Dlist           */
/* the following chars are used for parsing lines of data (record)         */
/* using sscanf                                                            */
	char       *fmt  = NULL;             /* used as format for sscanf        */
	char       *skip = NULL;             /* skip this stuff!                 */
	const char init_fmt[] = "%*d ";      /* initial format string            */
	const char skip_fmt[] = "%*lg ";     /* skip one more float              */
	const char read_fmt[] = "%lg ";      /* skip one more float              */
	if ( (fp=FindSection(fp,section)) ) {                 /* position the fp */
		base    = (char *)calloc(MAX_RECORD, sizeof(char));
		fmt     = (char *)calloc(MAX_RECORD, sizeof(char));
		skip    = (char *)calloc(MAX_RECORD, sizeof(char));
		current = NULL;
		inlist  = NULL;
/* while loop: reads and processes lines from file until sections ends *****/
		while ( strncmp(( base=fgets(base, MAX_RECORD, fp)), "$$", 2)) {
			record     = base;                  /* base always points to start   */
			lead_punct = 0;                     /* of string                     */
/* while loop: parses and processes each line from the data file *************/
			c=(int)*record;
			while ( c != '\0' ) {
				if( isdigit(c) ) {                            /* number means data */
					record = base;                  /* reset pointer to start of str */
					current = init_Dlist(size);
					if ( 1 != sscanf(record, "%d ", &(current->lineage)) )
						error("ReadData: error reading %s", base);
/* the following loop reads a line of data of variable length into a d-    */
/* array, skipping lineage number and all previously read data values      */
						skip = strcpy(skip, init_fmt); /* format to be skipped by sscanf */
						for(i=0; i < size; i++) {
							fmt  = strcpy(fmt, skip);     /* all this stuff here is to get */
							fmt  = strcat(fmt, read_fmt);  /* the fmt string for sscanf to */
							skip = strcat(skip, skip_fmt);           /* the correct format */
							if ( 1 != sscanf(record, (const char *)fmt, &(current->d[i])) )
								error("ReadData: error reading %s", base);
/* update number of data points */
							if ( ( i != 0) && (current->d[i] != IGNORE) )
								(*ndp)++;
						}
/* now add this to the lnkd list */
						inlist = addto_Dlist(inlist, current);
						break;
				} else if ( isalpha(c) ) {                    /* letter means comment */
					break;
				} else if ( c == '-' ) {                /* next two elsifs for punct */
					if ( ((int)*(record+1)) == '.')
						record++;
					lead_punct = 1;
					c=(int)*(++record);
				} else if ( c == '.' ) {
					lead_punct = 1;
					c=(int)*(++record);
				} else if ( ispunct(c) ) {               /* other punct means comment */
					break;
				} else if ( isspace(c) ) {             /* ignore leading white space */
					if ( lead_punct )               /* white space after punct means */
						break;                                              /* comment */
					else {
						c=(int)*(++record);            /* get next character in record */
					}
				} else {
					error ("ReadData: Illegal character in %s", base);
				}
			}
		}
		free(base);
		free(fmt);
		free(skip);
		return inlist;
	} else {
		return NULL;
	}
}

/*** ReadTimes: reads a time table from a file and returns a DArrPtr *******
 * FILE FORMAT: one time per line separated with newlines                  *
 *        NOTE: max. times tab size is 10000                               *
 ***************************************************************************/

DArrPtr *ReadTimes(char *timefile)
{
	FILE         *fp;
	int          count = 0;
	double       gast;
	DArrPtr *tabtimes;
	double  *tabptr;
	tabtimes = (DArrPtr *)malloc(sizeof(DArrPtr));
	gast = GetGastTime();
	tabtimes->size  = 10000;
	tabtimes->array = (double *)calloc(10000, sizeof(double));
	tabptr = tabtimes->array;
	fp = fopen(timefile, "r");
	if ( !fp )
		file_error("ReadTimes");
	if ( 1 != (fscanf(fp, "%lg\n", tabptr)) )
		error("ReadTimes: time file %s empty!", timefile);
	tabptr++;
	count++;
	while ( (fscanf(fp, "%lg\n", tabptr)) != EOF ) {
		if ( (*tabptr < 0) || (*tabptr<=*(tabptr-1)) || (*tabptr > gast) )
			error("ReadTimes: invalid time(s) in %s!", timefile);
		tabptr++;
		count++;
	}
	fclose(fp);
	tabtimes->size = count;
	tabtimes->array = (double *)realloc(tabtimes->array, count*sizeof(double));
	return tabtimes;
}



/*** ReadGuts: reads the $gutsdefs section in a data file into an array ****
 *             of strings which then need to get parsed                    *
 ***************************************************************************/

char **ReadGuts(FILE *fp)
{
  char      **gutsbuf = NULL;                /* buffer strings for reading */
  char       *record  = NULL;
  int	      i;                                        /* loop counters */

  if ( !(gutsbuf = (char **)calloc(MAX_RECORD, sizeof(char *)) ) )
    error("ReadGuts: error allocating memory for gutsdefs");

  if ( !(record  = (char *)calloc(MAX_RECORD, sizeof(char))) )
    error("ReadGuts: error allocating record");

/*** locate gutsdefs section ***********************************************/

  fp = FindSection(fp, "gutsdefs");
  if( !fp )
    error("ReadGuts: cannot locate gutsdefs");

/* Remember the location of the first line and count the number of lines **/

  i = 0;
  while ( strncmp( (record=fgets(record, MAX_RECORD, fp)), "$$", 2) ) {
    if (! (*(gutsbuf+i) = (char *)calloc(MAX_RECORD, sizeof(char))))
      error("ReadGuts: error allocating memory for gutsdefs strings");
    *(gutsbuf+i) = strcpy(*(gutsbuf+i), record);
    i++;
  }

  if (!(*gutsbuf))
    error("ReadGuts: gutsdefs section is empty!");

  free(record);

  return gutsbuf;
}

/*** List2Maternals: takes a Dlist and returns the corresponding BArrPtr ******
 *                structure; also initializes the lin_start array which    *
 *                contains the lineage number of the most anterior nucleus *
 *                for each cell cycle (we need this for cell division and  *
 *                printing model output)                                   *
 ***************************************************************************/

BArrPtr*List2Maternals(Dlist *inlist)
{
	int          i = 0, j;                                /* local loop counter */
	int          n;                          /* used to evaluate # of nuclei */
	int          lin_count = 0;        /* counter for initializing lin_start */
	unsigned int ccycle;                   /* what cleavage cycle are we in? */
	unsigned int samecycle = 0;            /* same cleavage cycle as before? */
	Dlist        *current;                       /* current element of Blist */
	Dlist        *start;                     /* used to evaluate # of nuclei */
	BArrPtr     *bicoid;                           /* BArrPtr to be returned */
	bicoid = (BArrPtr*)malloc(sizeof(BArrPtr));
	bicoid->size  = 0;
	bicoid->array = NULL;
	if ( !(lin_start = (int *)calloc(defs->ndivs+1, sizeof(int))) )
		error("List2Maternals: could not allocate lin_start array");
/*** for loop: step through linked list and copies values into an array    *
 *             of BcdGrads; there's one BcdGrad for each cleavage cycle;   *
 *             each BcdGrad has: - ccycle (the cleavage cycle number)      *
 *                               - gradient.size (# of nuclei for grad.)   *
 *                               - gradient.array (pointer to array)       *
 ***************************************************************************/
	for ( current = inlist; current; current=current->next ) {
		ccycle = ParseLineage( current->lineage );       /* which cycle is it? */
/* allocate new gradient struct for new cell cycle */
		if ( ccycle != samecycle ) {
			samecycle = ccycle;
			bicoid->size++;
			bicoid->array = (BcdGrad *)realloc(bicoid->array, bicoid->size*sizeof(BcdGrad));
/* this loop determines the number of nuclei in each cycle */
			n = 0;
			for ( start = current; ParseLineage(current->lineage) == samecycle; current = current->next ) {
				n++;
				if ( !(current->next) )    /* don't count garbage -> core dumps... */
					break;
			}
			current = start;                   /* reset pointer to where we were */
/* initialize lin_start: this array is later used by Blastoderm and such   */
			lin_start[defs->ndivs - lin_count] = current->lineage;
			lin_count++;
/* allocate array for gradient here and reset the counter */
			bicoid->array[bicoid->size-1].ccycle = samecycle;        /* next three lines define BcdGrad for each cleavage cycle */
			bicoid->array[bicoid->size-1].gradient = (DArrPtr*)malloc(sizeof(DArrPtr));
			bicoid->array[bicoid->size-1].gradient->array = (double *)calloc(n * defs->mgenes, sizeof(double));
			bicoid->array[bicoid->size-1].gradient->size  = n * defs->mgenes;
			i = 0;
		}
/* in any case: read concentration into gradient array */
		for ( j = 0; j < defs->mgenes; j++ ) {
			bicoid->array[bicoid->size-1].gradient->array[i] = current->d[j];
			i++;
		}
	}
	return bicoid;
}

/*** List2Bias: takes a Dlist and returns the corresponding DArrPtr ********
 *              structure.                                                 *
 ***************************************************************************/

NArrPtr*List2Bias(Dlist *inlist)
{
	int         i = 0;
	int         j;                                    /* local loop counters */
	int         n;                           /* used to evaluate # of nuclei */
	double      now = -999999999.0;                     /* variable for time */
	Dlist       *current;                  /* holds current element of Dlist */
	Dlist       *start;                  /* pointer used for evaluating # of */
	NArrPtr*bias;                              /* DArrPtr to be returned */
	bias = (NArrPtr*)malloc(sizeof(NArrPtr));
	bias->size  = 0;
	bias->array = NULL;
/*** for loop: steps through linked list and copies values into an array   *
 *             of NucStates. There's one NucState for each time step       *
 *             Each NucState has: - time (the time)                        *
 *                                - state.size (# of genes * # of nucs)    *
 *                                - state.array (pointer to array)         *
 ***************************************************************************/
	for ( current=inlist; current; current=current->next) {
		if ( current->d[0] != now ) {            /* a new time point: allocate */
			now = current->d[0];                             /* the time is now! */
			bias->size++;                      /* one NucState for each time step */
			bias->array =                       /* allocate for one more NucState */
									(NucState *)realloc(bias->array, bias->size * sizeof(NucState));
/* determine number of nuclei per time step */
			n = 0;
			for ( start = current; current->d[0] == now; current=current->next ) {
				n++;
				if ( !(current->next) )      /* don't count garbage and cause dis- */
					break;                                  /* may and core dumps... */
			}
			current = start;                  /* reset list ptr to where we were */
/* allocate a bias array for each biastime */
			bias->array[bias->size - 1].time = now;
			bias->array[bias->size - 1].state = (DArrPtr*)malloc(sizeof(DArrPtr));
			bias->array[bias->size - 1].state->array = (double *)calloc(n*defs->ngenes, sizeof(double));
			bias->array[bias->size - 1].state->size = n*defs->ngenes;
			i = 0;
		}
/* always: read concs into array */
		for( j = 1; j <= defs->ngenes; j++ ) {
			bias->array[bias->size - 1].state->array[i] = current->d[j];
			i++;
		}
	}
	return bias;
}

/* FOLLOWING FUNCTIONS ARE UTILITY FUNCTIONS FOR DIFFERENT LINKED LISTS ****
 * which are used to read in data of unknown size. All utility functions   *
 * follow the same scheme (X stands for the different letters below):      *
 *                                                                         *
 * - init_Xlist:         allocates first element and returns pointer to it *
 * - addto_Xlist:        adds the adduct to an already existing linkd list *
 * - free_Xlist:         frees memory of linked list                       *
 * - count_Xlist:        counts elements in the linked list                *
 *                                                                         *
 ***************************************************************************/

/*** Utility functions for Blist *******************************************/

Blist *init_Blist(void)
{
	Blist     *p;
	if( (p =(Blist *)malloc(sizeof(Blist))) ) {
		p->lineage = 0;
    p->conc    = 0;
    p->next    = NULL;

  }
  else
    error("init_Blist: couldn't allocate!");

  return p;
}



Blist *addto_Blist(Blist *start, Blist *adduct)
{
  Blist     *current;

  if( !start )
    return adduct;

  current = start;

  while (current->next) {
    current = current->next;
  }

  current->next = adduct;
  return start;
}



void free_Blist(Blist *start)
{
  if(start->next)
    free_Blist(start->next);

  free(start);
}



int count_Blist(Blist *start)
{
  int        n = 0;

  while( start != NULL ) {
    n++;
    start = start->next;
  }

  return n;
}



/*** Utility functions for Dlist *******************************************/

Dlist *init_Dlist(int size)
{
  Dlist    *p;

  if( (p=(Dlist *)malloc(sizeof(Dlist))) ) {

    if ( !(p->d=(double *)calloc(size, sizeof(double))) )
      error("initDlist: couldn't allocate!");

    p->lineage = 0;
    p->next    = NULL;

}
  else
    error("initDlist: couldn't allocate!");

  return p;
}



Dlist *addto_Dlist(Dlist *start, Dlist *adduct)
{
  Dlist         *current;

  if(!start)
    return adduct;

  current = start;

  while (current->next) {
    current = current->next;
  }

  current->next = adduct;
  return start;
}


void free_Dlist(Dlist *start)
{
  if(start->next)
    free_Dlist(start->next);

  free(start->d);
  free(start);
}



int count_Dlist(Dlist *start)
{
  int n = 0;
  while(start != NULL) {
    n++;
    start = start->next;
  }

  return n;
}



/*** Utility functions for Slist *******************************************/

Slist *init_Slist(void)
{
	Slist     *p;
	if( (p=(Slist *)malloc(sizeof(Slist))) ) {
		p->name =       NULL;
		p->bias_section =       NULL;
		p->fact_section =       NULL;
		p->mat_section  =       NULL;
		p->genotype     =       NULL;
		p->next         =       NULL;
	} else
		error("initSlist: couldn't allocate");
	return p;
}



Slist *addto_Slist(Slist *start, Slist *adduct)
{
  Slist      *current;

  if(!start)
    return adduct;

  current = start;

  while (current->next) {
    current = current->next;
  }

  current->next = adduct;
  return start;
}



void free_Slist(Slist *start)
{
	if(start->next)
		free_Slist(start->next);
	free(start->bias_section);
	free(start->fact_section);
	free(start->mat_section);
	free(start->genotype);
	free(start->name);
	free(start);
}



int count_Slist(Slist *start)
{
  int n = 0;
  while(start != NULL) {
    n++;
    start = start->next;
  }

  return n;
}

/* added by MacKoel */

void FreeFacts(GenoType*tab)
{
	int gindex;
	for( gindex = 0; gindex < nalleles; gindex++){
		FreeGType(&tab[gindex]);
	}
}

void  FreeGType(GenoType*fact)
{
	free(fact->genotype);
	FreeDataPtr(fact->ptr);
}

void FreeDataPtr(DataPtr*data)
{
/* FreeDArrPtr(&(data->times));
 FreeBArrPtr(&(data->bicoid));
 FreeNArrPtr(&(data->bias));
 FreeDataTable(data->facts);   */
}

void FreeBArrPtr(BArrPtr*arg)
{
	free(arg->array);
	free(arg);
}

void FreeDataTable(DataTable*arg)
{
	int i;
	for( i = 0; i < arg->size; i++) {
		FreeDataRecord(&(arg->record[i]));
	}
	free(arg);
}

void FreeDataRecord(DataRecord*record)
{
	free(record->array);
	free(record);
}

/*** GetDCoef: returns diffusion scaling coefficient according          ***
 *             the diffusion schedule used            			   *
 *   NOTE: Caller must allocate D_tab                                      *
 ***************************************************************************/

void GetDCoef(double t, char diff_schedule, double *D_tab)
{
	int           i;                                         /* loop counter */
	double        cutoff;                                     /* cutoff time */
	double        lscale = 1;                              /* scaling factor */
	double        length;
	if ( model != HYBRID && model != HEDIRECT && model != HESRR && model != HEURR && model != HEQUENCHING && model != HELOGISTIC ) {
		length = (double)defs->nnucs - 1.0;
/* to be compatible with the older versions
 * of codes for continuum model we have to
 * to do the following:*/
		if((diff_schedule != 'A') &&(diff_schedule != 'D') )
			for(i=0; i<defs->ndivs; i++)
				length *=0.5;
		if(diff_schedule == 'A')
			for(i=0; i<defs->ngenes; i++)
				D_tab[i] = length * length;
/* diffusion schedule B: all genes have different D's that depend on in-   *
 * verse l-square                                                          */
		else if (diff_schedule == 'B')
			for(i=0; i<defs->ngenes; i++ )
				D_tab[i] = length * length;
/* diffusion schedule C: all genes have the same D that depends on inverse *
 * l-square                                                                */
		else if (diff_schedule == 'C')
			for(i=0; i<defs->ngenes; i++)
				D_tab[i] = length * length;
/* diffusion schedule D: all genes have different D's which don't change   *
 * over time                                                               */
		else if (diff_schedule == 'D')
			for(i=0; i<defs->ngenes; i++ )
			D_tab[i] = length * length;
/* diffusion schedule E: used cutoff at gast-12 otherwise just like B */
		else if (diff_schedule == 'E') {
			cutoff = gast_time - 12.0;      /* This value probably wrong; see Merrill88 */
			for(i=0; i<defs->ngenes; i++ )
				D_tab[i] = (t < cutoff) ?  length * length : 0.;
/* any other diffusion schedule: error! */
		} else
			error("GetDCoef: no code for schedule %c!", diff_schedule);
	} else {
/* first time GetDCoef is called: set pointer to the right division table */
/* this loop takes lscale square for each cell division, i.e. the earlier  *
 * we are the bigger lscale (and the smaller the D's that we return        */
		for( i = 0; i < defs->ndivs; i++)
			if ( t < divtimes[i])
				lscale *= 2;
/* diffusion schedule A: all Ds always the same */
		if(diff_schedule == 'A')
			for( i = 0; i < defs->ngenes; i++)
				D_tab[i] = 1.0;
/* diffusion schedule B: all genes have different D's that depend on in-   *
 * verse l-square                                                          */
		else if (diff_schedule == 'B')
			for(i=0; i<defs->ngenes; i++ )
				D_tab[i] = 1.0 / (lscale * lscale);
/* diffusion schedule C: all genes have the same D that depends on inverse *
 * l-square                                                                */
		else if (diff_schedule == 'C')
			for(i=0; i<defs->ngenes; i++)
				D_tab[i] = 1.0 / (lscale * lscale);
/* diffusion schedule D: all genes have different D's which don't change   *
 * over time                                                               */
		else if (diff_schedule == 'D')
			for(i=0; i<defs->ngenes; i++ )
				D_tab[i] = 1.0;
/* diffusion schedule E: used cutoff at gast-12 otherwise just like B */
		else if (diff_schedule == 'E') {
			cutoff = gast_time - 12.0;      /* This value probably wrong; see Merrill88 */
			for(i=0; i<defs->ngenes; i++ )
				D_tab[i] = (t < cutoff) ?  1.0 / (lscale * lscale) : 0.;
/* any other diffusion schedule: error! */
		} else
			error("GetDCoef: no code for schedule %c!", diff_schedule);
	}
}

void GetRCoef(double t, double*factor)
{
	int           i;                                         /* loop counter */
	double        lscale = 1;                              /* scaling factor */
	if ( model != DOUBLING ) {
		*factor = 1.0;
		return;
	} else {
/* first time GetRCoef is called: set pointer to the right division table */
/* this loop takes lscale square for each cell division, i.e. the earlier  *
 * we are the bigger lscale (and the smaller the D's that we return        */
		for ( i = 0; i < defs->ndivs; i++)
			if (t>divtimes[defs->ndivs-1-i])
				lscale *= 2;
		*factor = lscale;
		return;
	}
}

double *gcmd_get_maternal_inputs(unsigned int ccycle)
{
	int cc;
	for ( cc = 0; cc < bcdtype[GINDEX].ptr->bicoid->size; cc++ )
		if ( bcdtype[GINDEX].ptr->bicoid->array[cc].ccycle == ccycle )
			return bcdtype[GINDEX].ptr->bicoid->array[cc].gradient->array;
}

void applyBicoidDosage(double dosage, char *genotype, int gindex)
{
	int ccyle;
	int nnuc;
	int gtyp;
	double bcd_factor;
	static short static_flag_called = 0;
	if ( !static_flag_called ) {
		bcdtype_orig = (GenoType*)calloc(nalleles, sizeof(GenoType));
		for ( gtyp = 0; gtyp < nalleles; gtyp++ ) {
			bcdtype_orig[gtyp].genotype = (char*)calloc(MAX_RECORD, sizeof(char));
			bcdtype_orig[gtyp].genotype = strcpy(bcdtype_orig[gtyp].genotype, bcdtype[gtyp].genotype);
			bcdtype_orig[gtyp].ptr = (DataPtr*)malloc(sizeof(DataPtr));
			bcdtype_orig[gtyp].ptr->bicoid = (BArrPtr*)malloc(sizeof(BArrPtr));
			bcdtype_orig[gtyp].ptr->bicoid->size = bcdtype[gtyp].ptr->bicoid->size;
			bcdtype_orig[gtyp].ptr->bicoid->array = (BcdGrad*)calloc(bcdtype[gtyp].ptr->bicoid->size, sizeof(BcdGrad));
			for ( ccyle = 0; ccyle < bcdtype_orig[gtyp].ptr->bicoid->size; ccyle++ ) {
				bcdtype_orig[gtyp].ptr->bicoid->array[ccyle].ccycle = bcdtype[gtyp].ptr->bicoid->array[ccyle].ccycle;
				bcdtype_orig[gtyp].ptr->bicoid->array[ccyle].gradient = (DArrPtr*)malloc(sizeof(DArrPtr));
				bcdtype_orig[gtyp].ptr->bicoid->array[ccyle].gradient->size = bcdtype[gtyp].ptr->bicoid->array[ccyle].gradient->size;
				bcdtype_orig[gtyp].ptr->bicoid->array[ccyle].gradient->array = (double*)calloc(bcdtype[gtyp].ptr->bicoid->array[ccyle].gradient->size, sizeof(double));
				for ( nnuc = 0; nnuc < bcdtype_orig[gtyp].ptr->bicoid->array[ccyle].gradient->size; nnuc++ ) {
					bcdtype_orig[gtyp].ptr->bicoid->array[ccyle].gradient->array[nnuc] = bcdtype[gtyp].ptr->bicoid->array[ccyle].gradient->array[nnuc];
				}
			}
		}
		static_flag_called = 1;
	}
	bcd_factor = 0.5 * dosage;
	for ( gtyp = 0; gtyp < nalleles; gtyp++ ) {
		if ( !(strcmp(bcdtype_orig[gtyp].genotype, genotype)) )
			break;
	}
	if ( gtyp == nalleles )             /* if no explicit bcd -> use wt bcd */
		gtyp = 0;
	for ( ccyle = 0; ccyle < bcdtype_orig[gtyp].ptr->bicoid->size; ccyle++ ) {
		for ( nnuc = gindex; nnuc < bcdtype_orig[gtyp].ptr->bicoid->array[ccyle].gradient->size; nnuc+=defs->mgenes ) {
			bcdtype[gtyp].ptr->bicoid->array[ccyle].gradient->array[nnuc] = bcd_factor * bcdtype_orig[gtyp].ptr->bicoid->array[ccyle].gradient->array[nnuc];
		}
	}
}

NArrPtr*List2Externals(Dlist*inlist)
{
	NArrPtr*Record;
	int       i = 0;
	int       j;                                      /* local loop counters */
	double    now = -999999999.;            /* assigns data to specific time */
	Dlist     *current;                    /* holds current element of Dlist */
	Record = (NArrPtr*)malloc(sizeof(NArrPtr));
	Record->size = 0;
	Record->array = NULL;
/*** for loop: steps through linked list and transfers facts into Data-    *
 *             Records, one for each time step                             *
 ***************************************************************************/
	if (!full_lin_start) {
		if ( !(full_lin_start = (int *)calloc(defs->ndivs + 1, sizeof(int))) )
			error("List2Externals: could not allocate full_lin_start array");
		full_ccycles =  defs->ndivs + 1;
		for (i=0; i < full_ccycles; i++)
			full_lin_start[i] = lin_start[i];
	}
	for ( current = inlist; current; current = current->next) {
		if ( current->d[0] != now ) {             /* a new time point: allocate */
			now = current->d[0];                             /* the time is now! */
			Record->size++;
			Record->array = (NucState *)realloc(Record->array, Record->size*sizeof(NucState));
			Record->array[Record->size-1].time = now;
			Record->array[Record->size-1].state = (DArrPtr*)malloc(sizeof(DArrPtr));
			Record->array[Record->size-1].state->size = 0;
			Record->array[Record->size-1].state->array = NULL;
			i = 0;
		}
		for( j =1; j <= defs->egenes; j++) {
			Record->array[Record->size-1].state->size++;
			Record->array[Record->size-1].state->array = (double*)realloc(Record->array[Record->size-1].state->array, Record->array[Record->size-1].state->size * sizeof(double));
			Record->array[Record->size-1].state->array[i] = current->d[j];
			i++;
		}
		if (ParseLineage(full_lin_start[full_ccycles - 1]) != ParseLineage(current->lineage)  ) {
			full_ccycles++;
			if ( !(full_lin_start = (int *)realloc(full_lin_start, full_ccycles*sizeof(int))) )
				error("List2Interp: could not allocate full_lin_start array");
			full_lin_start[full_ccycles-1] = current->lineage;
		}
	}
	qsort((void *) full_lin_start, full_ccycles, sizeof(int),(int (*) (const void*, const void*)) descend);
/*  for (i=0; i < full_ccycles; i++)
	printf("History lineages before removing dups %d\n", full_lin_start[i]);*/
/* Now lets remove duplicates */
	i = 0;
	while ( i < full_ccycles-1 ) {
		if (full_lin_start[i] == full_lin_start[ i + 1]) {
			memmove((full_lin_start + i), (full_lin_start + i + 1), ( full_ccycles - i - 1)*sizeof(int));
/*			printf("Shifted %d elements to %d\n",full_ccycles-i-1,i);*/
			full_ccycles--;
			i--;
		}
		i++;
		full_lin_start = (int *) realloc(full_lin_start, full_ccycles * sizeof(int));
	}
	InitFullNNucs();
	return Record;
}

int descend(const void* a, const void* b)
{
	int *x, *y;
	x = (int*)a;
	y = (int*)b;
	if (*x < *y) return 1;
	else if (*x > *y) return -1;
	else return 0;
}

/* Sets up the interpolation functions for the lineage that has most
nuclei, these functions will be used later in conjunction with
Go_Backward to return history for particular times */

InterpObject *DoInterp(NArrPtr *Nptrfacts, int num_genes)
{
	InterpObject *interp_res;
	int i, j, kk;
	int maxind;
	double *x, *y;
	int accepted, currsize;
	gsl_spline *temp_spline;
	gsl_interp_accel *temp_acc;
	interp_res = (InterpObject *)malloc(sizeof(InterpObject));
	maxind = 0;
	j = Nptrfacts->array[maxind].state->size;
	for ( i = 1; i < Nptrfacts->size; i++ ) {
		if ( Nptrfacts->array[i].state->size > j ) {
			j = Nptrfacts->array[i].state->size;
			maxind = i;
		}
	}
	interp_res->maxsize = Nptrfacts->array[maxind].state->size;
	interp_res->maxtime = Nptrfacts->array[maxind].time + EPSILON;
/*	printf("maxsize:%d, maxtime:%d\n", interp_res->maxsize, maxind);
	PrintBlastoderm(stdout, Nptrfacts, "Wloo", 2, num_genes);*/
	interp_res->func.size = Nptrfacts->size;
	interp_res->func.array = (NucState *) calloc(interp_res->func.size, sizeof(NucState));
	interp_res->slope.size = Nptrfacts->size;
	interp_res->slope.array = (NucState *) calloc(interp_res->slope.size, sizeof(NucState));
	interp_res->fact_discons_size = Nptrfacts->size;
	interp_res->fact_discons = (double *) calloc(interp_res->fact_discons_size, sizeof(double));
	for (i=0; i < interp_res->func.size; i++) {
		interp_res->func.array[i].time=Nptrfacts->array[maxind].time + EPSILON;
		interp_res->slope.array[i].time=Nptrfacts->array[i].time + EPSILON;
		interp_res->fact_discons[i] = Nptrfacts->array[i].time + EPSILON;
		interp_res->func.array[i].state = (DArrPtr*)malloc(sizeof(DArrPtr));
		interp_res->slope.array[i].state = (DArrPtr*)malloc(sizeof(DArrPtr));
		interp_res->func.array[i].state->size = Nptrfacts->array[maxind].state->size;
		interp_res->slope.array[i].state->size = Nptrfacts->array[maxind].state->size;
		interp_res->func.array[i].state->array=(double *)calloc(interp_res->func.array[i].state->size, sizeof(double));
		interp_res->slope.array[i].state->array=(double *)calloc(interp_res->slope.array[i].state->size, sizeof(double));
/*		printf("Going from: %d to %d\n",Nptrfacts->array[i].state->size, interp_res->func.array[i].state->size);*/
		if (interp_res->func.array[i].state->size >= Nptrfacts->array[i].state->size)
			Go_Forward(interp_res->func.array[i].state->array, Nptrfacts->array[i].state->array, GetStartLinIndex(interp_res->func.array[i].time), GetStartLinIndex(Nptrfacts->array[i].time + EPSILON), num_genes);
		else
			Go_Backward(interp_res->func.array[i].state->array, Nptrfacts->array[i].state->array, GetStartLinIndex(interp_res->func.array[i].time), GetStartLinIndex(Nptrfacts->array[i].time + EPSILON), num_genes);
/*		interp_res->func.array[i].time = Nptrfacts.array[i].time;*/
	}
	for (i=0; i < interp_res->maxsize; i++) {
		/* If the last data point is zero, make it equal to the last
		 * non-zero value, so that the concentration at a time
		 * after the last non -1 is maintained at that value */
		if (interp_res->func.array[Nptrfacts->size-1].state->array[i] == -1.) {
			kk = Nptrfacts->size-2;
			while ((kk >= 0) && (interp_res->func.array[kk].state->array[i] == -1.))
				kk--;
			if (kk == -1) {
				printf("All data is -1!\n");
				exit(1);
			}
			interp_res->func.array[Nptrfacts->size-1].state->array[i] = interp_res->func.array[kk].state->array[i];
		}
		/* If the first value in a data set is -1, set it to 0. */
		if (interp_res->func.array[0].state->array[i] == -1.)
			interp_res->func.array[0].state->array[i] = 0.;
		/* Now weed-out the -1s so you can interpolate linearly
		 * between time points for which you have data */
		x = (double *) calloc(Nptrfacts->size, sizeof(double));
		y = (double *) calloc(Nptrfacts->size, sizeof(double));
		accepted = 0;
		currsize = Nptrfacts->size;
		for (j=0; j < Nptrfacts->size; j++) {
			if (interp_res->func.array[j].state->array[i] != -1.) {
				x[accepted] = Nptrfacts->array[j].time + EPSILON;
				y[accepted] = interp_res->func.array[j].state->array[i];
				accepted++;
			} else {
				currsize--;
				x = (double*)realloc(x, currsize*sizeof(double));
				y = (double*)realloc(y, currsize*sizeof(double));
			}
		}
		temp_acc = gsl_interp_accel_alloc();
		temp_spline = gsl_spline_alloc(gsl_interp_linear, currsize);
		gsl_spline_init(temp_spline,x,y,currsize);
		free(x);
		free(y);
		for (j=0; j < Nptrfacts->size; j++) {
			interp_res->func.array[j].state->array[i] = gsl_spline_eval(temp_spline, interp_res->slope.array[j].time, temp_acc);
		}
		for (j=0; j < Nptrfacts->size; j++) {
			if (j < Nptrfacts->size - 1)
				interp_res->slope.array[j].state->array[i] = (interp_res->func.array[j+1].state->array[i] - interp_res->func.array[j].state->array[i]) / (interp_res->slope.array[j+1].time - interp_res->slope.array[j].time);
			else
				interp_res->slope.array[j].state->array[i] = 0.;
		}
		gsl_interp_accel_free(temp_acc);
	 	gsl_spline_free(temp_spline);
	}
/*	PrintBlastoderm(stdout, &interp_res->func, "loo", 2, num_genes);
	PrintBlastoderm(stdout, &interp_res->slope, "sloo", 2, num_genes);*/
	return interp_res;
}


void gcmd_get_external_inputs(double t, double*vext, int n)
{
	ExternalInputs(t, t, vext, n);
}

void ExternalInputs(double t, double t_size, double *yd, int n)
{
	int j, k;
	double *blug;
	double t_interp, t_diff;
/*	printf("Going from %d to %d, time:%f time for size:%f\n", extinp_interp_object[GINDEX]->maxsize, n, t, t_size);*/
	blug = (double *) calloc(extinp_interp_object[GINDEX]->maxsize, sizeof(double));
	k = -1;
	do {
		k++;
	} while ( (k < extinp_interp_object[GINDEX]->slope.size ) && (t > extinp_interp_object[GINDEX]->slope.array[k].time));
	if (k == 0)
		t_interp = extinp_interp_object[GINDEX]->slope.array[k].time;
	else {
		k--;
		t_interp = t;
	}
	t_diff = t_interp - extinp_interp_object[GINDEX]->slope.array[k].time;
	for (j=0; j < extinp_interp_object[GINDEX]->maxsize; j++) {
		blug[j] = extinp_interp_object[GINDEX]->func.array[k].state->array[j] + extinp_interp_object[GINDEX]->slope.array[k].state->array[j] * t_diff;
	}
	if (n >= extinp_interp_object[GINDEX]->maxsize)
		Go_Forward(yd, blug, GetStartLinIndex(t_size), GetStartLinIndex(extinp_interp_object[GINDEX]->maxtime), defs->egenes);
	else
		Go_Backward(yd, blug, GetStartLinIndex(t_size), GetStartLinIndex(extinp_interp_object[GINDEX]->maxtime), defs->egenes);
	free(blug);
	return;
}

void gcdm_get_curveSlopeExtInputs(double t, double* slopeE, int n)
{
	SlopeExtInputs(t, t, slopeE, n);
}

void SlopeExtInputs(double t, double t_size, double *yd, int n)
{
	int j, k;
	double *blug;
	double t_interp, t_diff;
/*	printf("Going from %d to %d, time:%f time for size:%f\n", extinp_interp_object[GINDEX]->maxsize, n, t, t_size);*/
	blug = (double *) calloc(extinp_interp_object[GINDEX]->maxsize, sizeof(double));
	k = -1;
	do {
		k++;
	} while ( (k < extinp_interp_object[GINDEX]->slope.size ) && (t > extinp_interp_object[GINDEX]->slope.array[k].time));
	if (k == 0)
		t_interp = extinp_interp_object[GINDEX]->slope.array[k].time;
	else {
		k--;
		t_interp = t;
	}
	t_diff = t_interp - extinp_interp_object[GINDEX]->slope.array[k].time;
	for (j=0; j < extinp_interp_object[GINDEX]->maxsize; j++) {
		blug[j] = extinp_interp_object[GINDEX]->slope.array[k].state->array[j];
	}
	if (n >= extinp_interp_object[GINDEX]->maxsize)
		Go_Forward(yd, blug, GetStartLinIndex(t_size), GetStartLinIndex(extinp_interp_object[GINDEX]->maxtime), defs->egenes);
	else
		Go_Backward(yd, blug, GetStartLinIndex(t_size), GetStartLinIndex(extinp_interp_object[GINDEX]->maxtime), defs->egenes);
	free(blug);
	return;
}

void FreeInterpObject(InterpObject *interp_obj)
{
	FreeNArrPtr(&(interp_obj->func));
	FreeNArrPtr(&(interp_obj->slope));
	if (interp_obj->fact_discons)
		free(interp_obj->fact_discons);
}

void Go_Forward(double *output, double *input, int output_ind, int input_ind, int num_genes)
{
	double *y;
	int output_lin, size, newsize;
	int k, ap, j, ii;
	output_lin = Index2StartLin(output_ind);
	newsize = Index2NNuc(output_ind)*num_genes;
/*	printf("output lineage start, indices:%d, %d, %d\n", output_lin, output_ind, input_ind);*/
	if (output_ind < input_ind - 1) {
		size = Index2NNuc(output_ind+1)*num_genes;
		y = (double *) calloc(size, sizeof(double));
/*		printf("Passing on to another fwd with targets %d %d %d\n",size,output_ind+1,input_ind);*/
		Go_Forward(y, input, output_ind+1, input_ind, num_genes);
	} else if  (output_ind == input_ind - 1) {
		size = Index2NNuc(input_ind)*num_genes;
		y = (double *) calloc(size, sizeof(double));
/*		printf("Goin' to do the tranfer:%d %d\n",size,newsize);*/
		y = (double*)memcpy(y,input,size*sizeof(double));
	} else if (output_ind == input_ind) {
		output = (double*)memcpy(output,input,newsize*sizeof(double));
		return;
	} else
		error("You are trying to go from nnucs %d to %d!",Index2NNuc(input_ind), Index2NNuc(output_ind));
	for (j=0; j < size; j++) {
		k  = j % num_genes;     /* k: index of gene k in current nucleus */
		ap = j / num_genes;      /* ap: rel. nucleus position on AP axis */
/* evaluate ii: index of anterior daughter nucleus */
		if ( output_lin % 2 )
			ii = 2 * ap * num_genes + k - num_genes;
		else
			ii = 2 * ap * num_genes + k;
/* skip the first most anterior daughter nucleus in case output_lin is odd */
		if ( ii >= 0 )
			output[ii] = y[j];
/* the second daughter only exists if it is still within the region */
		if ( ii + num_genes < newsize )
			output[ii+num_genes] =  y[j];
	}
	free(y);
	return;
}


void Go_Backward(double *output, double *input, int output_ind, int input_ind, int num_genes)
{
	double *y;
	int output_lin, input_lin, size, newsize;
	int k, ap, j, ii;
	output_lin = Index2StartLin(output_ind);
	input_lin  = Index2StartLin(input_ind);
	newsize = Index2NNuc(output_ind)*num_genes;
/*	printf("output lineage start, indices, newsize:%d, %d, %d, %d\n",output_lin, output_ind, input_ind, newsize);*/
	if (output_ind > input_ind + 1) {
		size = Index2NNuc(output_ind-1)*num_genes;
		y = (double *) calloc(size, sizeof(double));
/*		printf("Passing on to another bkd with targets %d %d %d\n",size,output_ind-1,input_ind);*/
		Go_Backward(y,input,output_ind-1,input_ind,num_genes);
		input_lin  = Index2StartLin(output_ind-1);
	} else if  (output_ind == input_ind + 1 ) {
		size = Index2NNuc(input_ind) * num_genes;
		y = (double *) calloc(size, sizeof(double));
/*		printf("Goin' to do the tranfer\n");*/
		memcpy(y, input, size * sizeof(double));
	} else if (output_ind == input_ind){
		memcpy(output,input,newsize*sizeof(double));
		return;
	} else
		error("You are trying to go from nnucs %d to %d!", Index2NNuc(input_ind), Index2NNuc(output_ind));
	for (j=0; j < newsize; j++) {
		k  = j % num_genes;     /* k: index of gene k in current nucleus */
		ap = j / num_genes;      /* ap: rel. nucleus position on AP axis */
/* evaluate ii: index of anterior daughter nucleus */
		if ( input_lin % 2 )
			ii = 2 * ap * num_genes + k - num_genes;
		else
			ii = 2 * ap * num_genes + k;
/* skip the first most anterior daughter nucleus in case output_lin is odd */
		if (ii < 0)
			output[j] = y[ii + num_genes];
		if (( ii >= 0 ) && (ii + num_genes < size))
			output[j] = .5*(y[ii] + y[ii+num_genes]);
		if (ii + num_genes >= size)
			output[j] = y[ii];
	}
	free(y);
	return;
}

/*** GetStartLinIndex: returns the index of the lineage array
* for agiven time                                         *
************************************************************************** */

int GetStartLinIndex(double t)
{
	int i;                                              /* loop counter */
/* evaluate lineage number of most anterior nucleus for current time; note *
 * that for the *exact* time of cell division, we'll return the lineage    *
 * number of the most anterior nucleus of the previous cell cycle          */
	for ( i = 0; i < full_ccycles - 1; i++)
		if ( t > full_divtimes[i]+HALF_EPSILON )
			return i;
	return i;
}

int Index2StartLin(int index)
{
	return full_lin_start[index];
}

int Index2NNuc(int index)
{
	return full_nnucs[index];
}

/*** InitGenotypeIndex: function used to make genotype static to maternal.c ******
 *                 since derivative functions later need to read it for    *
 *                 getting the right bicoid gradients from maternal.c      *
 ***************************************************************************/

void InitGenotypeIndex(char *g_type)
{
	Slist*curr;
	int i;
	GINDEX = 0;
	for ( i = 0, curr = mat_genotypes; i < nalleles; i++, curr = curr->next )
		if ( !strcmp(g_type, curr->name) ) {
			Mutate(curr->genotype);
			GINDEX = i;
			break;
		}
}

void InitGenotypeIndexWithBicoidDosage(char *g_type, double dosage)
{
	Slist*curr;
	int i;
	GINDEX = 0;
	for ( i = 0, curr = mat_genotypes; i < nalleles; i++, curr = curr->next )
		if ( !strcmp(g_type, curr->name) ) {
			MutateWithBicoidDosage(curr->genotype, dosage);
			GINDEX = i;
			break;
		}
}

char*Name2Gtype(char *g_type)
{
	Slist*curr;
	for ( curr = mat_genotypes; curr; curr = curr->next )
		if ( !strcmp(g_type, curr->name) )
			return curr->genotype;
}

/*** FreeSolution: frees memory of the solution structure created by *******
 *                 Blastoderm() or gut functions                           *
 ***************************************************************************/

void FreeNArrPtr(NArrPtr*Solution)
{
	int i;                                                   /* loop counter */
	for( i = 0; i < Solution->size; i++) {
		FreeDArrPtr( Solution->array[i].state );
	}
	free( Solution->array );
	free(Solution);
}

void FreeDArrPtr(DArrPtr*state)
{
	free(state->array);
	free(state);
}

/* eo added by MacKoel*/

double *gcdm_get_data_times(int *n)
{
	*n = ndata_times;
	return data_times;
}

int gcdm_get_nuclear_index(int x,  double time)
{
	int y, i;
	y = x;
	for ( i = 0; i < defs->ndivs; i++ )
		if ( time < divtimes[i] )
			y = (int)(0.5 * y);
	return y;
}

/*#define HUGE_EPSILON 1e+0*/

void gcmd_get_coupled_inputs(double t, double*vcouple, double*wcouple, int n, double*v, double*w, int nv, double*vext, int next, double*vmat, int nmat)
{
	int i, ap, ncouple;
	double vv, ww1, ww2;
	for ( ap = 0, i = 0; i < n; i += defs->ncouples, ap++ ) {
		for ( ncouple = 0; ncouple < defs->ncouples; ncouple++ ) {
			switch ( defs->couples[ncouple].first_gene_type ) {
				case maternal:
					vv = vmat[ap * defs->mgenes + defs->couples[ncouple].first_gene_id];
					ww1 = 0;
					ww2 = vv;
				break;
				case external:
					vv = vext[ap * defs->egenes + defs->couples[ncouple].first_gene_id];
					ww1 = 0;
					ww2 = vv;
				break;
				case zygotic:
					vv = v[ap * defs->ngenes + defs->couples[ncouple].first_gene_id];
					ww1 = w[ap * defs->ngenes + defs->couples[ncouple].first_gene_id];
					ww2 = vv;
				break;
			}
/*			if ( vv < HUGE_EPSILON ) vv = 0.0;
			if ( ww1 < HUGE_EPSILON ) ww1 = 0.0;
			if ( ww2 < HUGE_EPSILON ) ww2 = 0.0;*/
			switch ( defs->couples[ncouple].second_gene_type ) {
				case maternal:
					vv *= vmat[ap * defs->mgenes + defs->couples[ncouple].second_gene_id];
					ww1 *= vmat[ap * defs->mgenes + defs->couples[ncouple].second_gene_id];
					ww2 *= 0;
				break;
				case external:
					vv *= vext[ap * defs->egenes + defs->couples[ncouple].second_gene_id];
					ww1 *= vext[ap * defs->egenes + defs->couples[ncouple].second_gene_id];
					ww2 *= 0;
				break;
				case zygotic:
					vv *= v[ap * defs->ngenes + defs->couples[ncouple].second_gene_id];
					ww1 *= v[ap * defs->ngenes + defs->couples[ncouple].second_gene_id];
					ww2 *= w[ap * defs->ngenes + defs->couples[ncouple].second_gene_id];
				break;
			}
/*			if ( vv < HUGE_EPSILON ) vv = 0.0;
			if ( ww1 < HUGE_EPSILON ) ww1 = 0.0;
			if ( ww2 < HUGE_EPSILON ) ww2 = 0.0;*/
			vcouple[ i + ncouple ] = vv / 255.0;
			wcouple[ i + ncouple ] = ( ww1 + ww2 ) / 255.0;
		}
	}
}

void gcmd_get_coupled_inputs_1(double t, double*vcouple, int n, double*v, int nv, double*vext, int next, double*vmat, int nmat)
{
	int i, ap, ncouple;
	double vv;
	for ( ap = 0, i = 0; i < n; i += defs->ncouples, ap++ ) {
		for ( ncouple = 0; ncouple < defs->ncouples; ncouple++ ) {
			switch ( defs->couples[ncouple].first_gene_type ) {
				case maternal:
					vv = vmat[ap * defs->mgenes + defs->couples[ncouple].first_gene_id];
				break;
				case external:
					vv = vext[ap * defs->egenes + defs->couples[ncouple].first_gene_id];
				break;
				case zygotic:
					vv = v[ap * defs->ngenes + defs->couples[ncouple].first_gene_id];
				break;
			}
			switch ( defs->couples[ncouple].second_gene_type ) {
				case maternal:
					vv *= vmat[ap * defs->mgenes + defs->couples[ncouple].second_gene_id];
				break;
				case external:
					vv *= vext[ap * defs->egenes + defs->couples[ncouple].second_gene_id];
				break;
				case zygotic:
					vv *= v[ap * defs->ngenes + defs->couples[ncouple].second_gene_id];
				break;
			}
			vcouple[ i + ncouple ] = vv / 255.0;
		}
	}
}

void gcmd_get_coupling_matrix(double t, double*values, int n, double*v, int nv, double*vext, int next, double*vmat, int nmat)
{
	int i, j, k, ncouple;
	k = defs->ngenes + defs->egenes;
	for ( ncouple = 0; ncouple < defs->ncouples; ncouple++ ) {
		switch ( defs->couples[ncouple].first_gene_type ) {
			case maternal:
				i = defs->ngenes + defs->egenes + defs->couples[ncouple].first_gene_id;
			break;
			case external:
				i = defs->ngenes + defs->couples[ncouple].first_gene_id;
			break;
			case zygotic:
				i = defs->couples[ncouple].first_gene_id;
			break;
		}
		switch ( defs->couples[ncouple].second_gene_type ) {
			case maternal:
				j = defs->ngenes + defs->egenes + defs->couples[ncouple].second_gene_id;
			break;
			case external:
				j = defs->ngenes + defs->couples[ncouple].second_gene_id;
			break;
			case zygotic:
				j = defs->couples[ncouple].second_gene_id;
			break;
		}
		v[ i * k + j ] = values[ncouple];
	}
}

char**gcdm_init_acid(FILE*fp)
{
    char**acid_tables;
    acid_tables = (char**)calloc(3, sizeof(char*));
	fp = FindSection(fp, "acid_tables");
	if( !fp )
		error("gcdm_init_acid: cannot locate acid_tables section");
	fscanf(fp, "%*s\n");             /* advance pointer past first text line */
	acid_tables[0] = (char *)calloc(MAX_RECORD, sizeof(char));
	if ( 1 != (fscanf(fp, "%s\n", acid_tables[0])) )
		error("gcdm_init_acid: error reading acid_tables section (1)");
	fscanf(fp, "%*s\n");             /* advance pointer past first text line */
	acid_tables[1] = (char *)calloc(MAX_RECORD, sizeof(char));
	if ( 1 != (fscanf(fp, "%s\n", acid_tables[1])) )
		error("gcdm_init_acid: error reading acid_tables section (2)");
    if ( !strcmp(acid_tables[1], "NA") ) {
        acid_tables[1] = (char*)strcpy(acid_tables[1], "");
    }
	fscanf(fp, "%*s\n");             /* advance pointer past first text line */
	acid_tables[2] = (char *)calloc(MAX_RECORD, sizeof(char));
	if ( 1 != (fscanf(fp, "%s\n", acid_tables[2])) )
		error("gcdm_init_acid: error reading acid_tables section (3)");
	if ( !strcmp(acid_tables[2], "NA") ) {
	    acid_tables[2] = (char*)strcpy(acid_tables[2], "");
	}
    return acid_tables;
}

static DDEHistory* solution_history;

void gcdm_init_delayed(int n)
{
    EqParms*parms = GetParameters();
    int nmrna = defs->ngenes / 2;
    solution_history = DDEHistory_create(parms->tau, nmrna, n);
}

void gcdm_add_delayed(double *v, double t)
{
    DDEHistory_addHistory(solution_history, v, t);
}

void gcdm_rem_delayed()
{
    DDEHistory_free(solution_history);
}

void gcdm_get_delayed(double *v, double t)
{
  int i;
  DDEHistoryValues *values = solution_history->first;
/* We were asked for values at time earlier than our first known data */
  if(values->t > t){
    for(i = 0; i < solution_history->n; i++)
      v[i] = values->v[i];
    return;
  }

  while (values->next != NULL){
    if(values->next->t > t-10*DBL_EPSILON)
      break;
    values = values->next;
  }

  if(values->next == NULL && values->t < t)
    error("dde evaluate at time t-delay > tend");

  if(values->next == NULL){
    for(i = 0; i < solution_history->n; i++)
      v[i] = values->v[i];
    return;
  }

  double tt = values->next->t - values->t;
  double *v1 = values->v, *v2 = values->next->v;
  for(i = 0; i < solution_history->n; i++)
    v[i] = v1[i]+(v2[i]-v1[i])/tt*(t - values->t);
}

/*
#include "DDEHistory.h"
*/

/* DDEHistory_create creates and fill with initial              *
 *                   values History for solving dde with        *
 *                   adaptive step size                         *
 */
DDEHistory* DDEHistory_create(double *delays, int nlags, int n){
  int i;
  double max_delay = 0, min_delay = DBL_MAX;
  for(i = 0; i < nlags; i++){
    if(max_delay < delays[i])
      max_delay = delays[i];
    if(min_delay > delays[i])
      min_delay = delays[i];
  }

  DDEHistory* History = (DDEHistory*)malloc(sizeof(DDEHistory));
  History->n = n;
  History->nlags = nlags;
  History->max_delay = max_delay;
  History->min_delay = min_delay;
  History->tstart = 0;
  History->tend = 0;
  History->first = NULL;
  History->last = NULL;
  return History;
}

/* DDEHistory_addHistory add values v at time t to history          *
                         and control size of storing values         *
                         only v values from t-max_delay to t should *
                         be stored                                  *
*/
void DDEHistory_addHistory(DDEHistory *history, double *v, double t){
  if(history->first == NULL){
    DDEHistoryValues *value = (DDEHistoryValues*)malloc(sizeof(DDEHistoryValues));
    value->t = t;
    value->v = (double*)malloc(history->n*sizeof(double));

    int i;
    for(i = 0; i < history->n; i++)
      value->v[i] = v[i];
    value->next = NULL;

    history->first = value;
    history->last = value;
    history->tstart = t;
    history->tend = t;
    return;
  }

  double tstart = history->first->next == NULL ? history->tstart : history->first->next->t;

  if(t - tstart <= history->max_delay){
    DDEHistoryValues *value = (DDEHistoryValues*)malloc(sizeof(DDEHistoryValues));
    value->t = t;
    value->v = (double*)malloc(history->n*sizeof(double));

    int i;
    for(i = 0; i < history->n; i++)
      value->v[i] = v[i];
    value->next = NULL;

    history->last->next = value;
    history->last = value;
    history->tend = t;
  }else{
    history->tstart = tstart;
    history->tend = t;

    DDEHistoryValues *value = history->first;
    value->t = t;

    int i;
    for(i = 0; i < history->n; i++)
      value->v[i] = v[i];

    history->first = value->next;
    history->last->next = value;
    history->last = value;
    history->last->next = NULL;
  }

}

/* DDEHistory_interpolateLinear - get linearly interolated history    *
 *                               at time t-delay and store  them in v *
 */
void DDEHistory_interpolateLinear(DDEHistory *history, double *v, double delay, double t){
  int i;
  DDEHistoryValues *values = history->first;
  double tdelayed = t-delay;
//  double eps = 10*DBL_EPSILON;
//  while (values->next != NULL){
//    if(fabs(values->t - tdelayed) < eps)
//      break;
//    values = values->next;
//  }
  while (values->next != NULL){
    if(values->next->t > tdelayed-10*DBL_EPSILON)
      break;
    values = values->next;
  }

  if(values->next == NULL && values->t < tdelayed)
    error("dde evaluate at time t-delay > tend");

//  for(i = 0; i < history->n; i++)
//    v[i] = values->v[i];
  if(values->next == NULL){
    for(i = 0; i < history->n; i++)
      v[i] = values->v[i];
    return;
  }

  double tt = values->next->t - values->t;
  double *v1 = values->v, *v2 = values->next->v;
  for(i = 0; i < history->n; i++)
    v[i] = v1[i]+(v2[i]-v1[i])/tt*(tdelayed - values->t);
}

/* DDEHistory_interpolateErmit - get cubic Hermit interolated history *
 *                               at time t-delay and store  them in v *
 */
void DDEHistory_interpolateErmit(DDEHistory *history, double *v, double delay, double t){
  int i;
  DDEHistoryValues *values = history->first;
  double tdelayed = t-delay;
  while (values->next != NULL){
    if(values->next->t > tdelayed)
      break;
    values = values->next;
  }

  if(values->next == NULL && values->t < tdelayed)
    error("dde evaluate at time t-delay > tend");


  double tt = values->next->t - values->t;
  double *v1 = values->v, *v2 = values->next->v;
  for(i = 0; i < history->n; i++)
    v[i] = v1[i]+(v2[i]-v1[i])/tt*(tdelayed - values->t);
}

void DDEHistory_free(DDEHistory* history){
  DDEHistoryValues *values = history->first;
  while (values != NULL){
    DDEHistoryValues *tmp = values->next;
    free(values->v);
    free(values);
    values = tmp;
  }
  free(history);
}

