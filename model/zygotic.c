/*****************************************************************
 *                                                               *
 *   zygotic.c                                                   *
 *                                                               *
 *****************************************************************
 *                                                               *
 *   written by JR, modified by Yoginho                          *
 *   additional g(u)'s by Yousong Wang, Feb 2002                 *
 *   guts code by Manu, June/July 2002                           *
 *                                                               *
 *****************************************************************
 *                                                               *
 * This file is for functions that deal with the right hand side *
 * of the ODEs. We have an initializing function called          *
 * InitZygote that reads parameters, defs data and search space  *
 * limits from a data file and installs a couple of things like  *
 * the solver and temporary arrays for the dvdt function.        *
 * Then we have the DvdtOrig's, which represent the inner loop   *
 * of the model. Each of these derivative functions calculates   *
 * the derivatives of a particular right-hand-side at a given    *
 * time point. They are called by the solver and need to know if *
 * we're in mitosis or interphase, since the equations differ    *
 * in each case.                                                 *
 * Last but not least, we have a few mutator functions in this   *
 * file. They change the local lparm EqParm struct.              *
 *                                                               *
 *****************************************************************/

#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#ifdef ICC
#include <mathimf.h>
#else
#include <math.h>
#endif

#include <error.h>                                   /* for error handling */
#ifdef XTRA
#include <cbgs.h>
#endif
#include <maternal.h>  /* need this for BArrPtr (for the Bicoid structure) */
#include <zygotic.h>                                          /* obviously */
#include <ffio.h>

#ifdef MKL
#include <mkl_vml.h>
#endif

#include <ODESolve.h>

/* STATIC VARIABLES PRIVATE TO THIS FILE ***********************************
 * most of these static variables are used to speed up the inner loop (the *
 * dvdt function of the model for annealing purposes                       *
 ***************************************************************************/

/* following two structs are used for equation paramters; parm holds       */
/* parameters BEFORE mutation and is returned by GetParameters (i.e. to    */
/* Score(), which would produce an error on mutated parameters); lparm is  */
/* a copy of the struct that gets mutated (either Rs or Ts get set to zero */
/* and therefore violate limits if sent to Score()) and is then used by    */
/* the derivative function DvdtOrig                                        */

static EqParms *parm;              /* static struct for equation parameters */
static EqParms *lparm;         /* local copy of parameter that gets mutated */
/* following arrays are used by DvdtOrig */
static double *D;                 /* contains info about diffusion sched. */
static double *M;                 /* contains info about mobility. */
static double *MM;                 /* contains info about mobility. */
static double *DCoef;                 /* contains info about diffusion sched. */
static double *vinput;               /* vinput, bot2 and bot are used for */
static double *vinput2;               /* vinput, bot2 and bot are used for */
static double *bot2, *bot, *bot5;       /* storing intermediate stuff for vector functions used by the dvdt function */

/* two local copies for propagation rule and genotype number */
static int rule;                      /* propagation rule for DvdtOrig */

/*added by MacKoel */

static EqParms *grad;
static double *bot3;
static double boundary;
static double mit_factor;
static unsigned int ccycle;
static int num_nucs;

static double *input_He, *params_He;
static double *dirichlet_boundary_coef;
static double *zOn, *zOff;
/* buffers for gcmd */

static double *vext;
static double *vext2;
static double *vmat;
static double *vcouple;
static double *wcouple;
static double **vdelayed;
static int parm_per_gene;
static int t_parm_extension;
static int e_parm_extension;
static int m_parm_extension;
static int mm_parm_extension;
static int h_parm_extension;
static int r_parm_extension;
static int d_parm_extension;
static int lambda_parm_extension;

static int use_external_inputs_only;

/*** INITIALIZATION FUNCTIONS **********************************************/

/*** InitZygote: makes pm and pd visible to all functions in zygotic.c and *
 *               reads EqParms and TheProblem; it then initializes bicoid  *
 *               and bias (including BTimes) in maternal.c; lastly, it     *
 *               allocates memory for structures used by the derivative    *
 *               functions                                                 *
 ***************************************************************************/

void InitZygote(FILE *fp, char *parm_section)
{
	defs = ReadTheProblem(fp);
	parm = ReadParameters(fp, parm_section);
	lparm = InstallParm();
/* install bicoid and bias and nnucs in maternal.c */
/*	InitBicoid(fp);*/
	InitMaternal(fp);
	InitMaternals(fp);
	InitBias(fp);
	InitNNucs();
	if ( defs->egenes )
		InitExternals(fp);
/*	InitFullNNucs();*/

/*** The following things are static to zygotic.c: D contains diffusion ****
 *   parameters converted according to the diffusion schedule; vinput,     *
 *   bot2 and bot are arrays for intermediate results used by DvdtOrig,    *
 *   which are collected for fast DEC vector functions (see DvdtOrig for   *
 *   details)                                                              *
 ***************************************************************************/
	D = (double *)calloc(defs->ngenes, sizeof(double));
	vinput = (double *)calloc(defs->ngenes * defs->nnucs, sizeof(double));
	vext = (double *)calloc(defs->egenes * defs->nnucs, sizeof(double));
#ifdef XTRA
	input_He = (double *)calloc(defs->ngenes + defs->egenes, sizeof(double));
	params_He = (double *)calloc( 3 * ( defs->ngenes + defs->egenes ) + 3, sizeof(double));
	zOn = (double *)calloc(defs->ngenes * defs->nnucs, sizeof(double));
	zOff = (double *)calloc(defs->ngenes * defs->nnucs, sizeof(double));
#else
	bot5 = (double *)calloc(defs->ngenes * defs->nnucs, sizeof(double));
	bot3 = (double *)calloc(defs->ngenes * defs->nnucs, sizeof(double));

	vext2 = (double *)calloc(defs->egenes * defs->nnucs, sizeof(double));
	vcouple = (double *)calloc(defs->ncouples * defs->nnucs, sizeof(double));
	wcouple = (double *)calloc(defs->ncouples * defs->nnucs, sizeof(double));
#endif
	M = (double *)calloc(defs->ngenes, sizeof(double));
	MM = (double *)calloc(defs->ngenes, sizeof(double));
	DCoef = (double *)calloc(defs->ngenes, sizeof(double));
	vinput2 = (double *)calloc(defs->ngenes * defs->nnucs, sizeof(double));
	bot2 = (double *)calloc(defs->ngenes * defs->nnucs, sizeof(double));
	bot = (double *)calloc(defs->ngenes * defs->nnucs, sizeof(double));
	if ( model == HYBRID ) {
		boundary = 1.0;
	} else {
		boundary = 2.0;
	}

	dirichlet_boundary_coef = (double *)calloc( defs->ngenes, sizeof(double));
}

#ifdef XTRA
void gcdm_init_seq(FILE*fp)
{
    int i;
	boundary = 1.0;
	if ( model == HEDIRECT ) {
        char**acid_tables = gcdm_init_acid(fp);
	    int nmrna = defs->ngenes / 2, i;
		expression_init(acid_tables[0], parm->M, acid_tables[1], acid_tables[2], "", "", "", "Direct", debug);
        vdelayed = (double **)calloc(nmrna, sizeof(double *)); /* value of v in v_j(t-delay_i)*/
        for(i = 0; i < nmrna; i++) {
            vdelayed[i] = (double *)calloc(defs->ngenes * defs->nnucs, sizeof(double));
			dirichlet_boundary_coef[i] = 1.0;
			dirichlet_boundary_coef[i + nmrna] = 1.0;
        }
        ODESolve_set_dir(nmrna);
        free(acid_tables[0]);
        free(acid_tables[1]);
        free(acid_tables[2]);
        free(acid_tables);
	} else if ( model == HESRR ) {
        char**acid_tables = gcdm_init_acid(fp);
	    int nmrna = defs->ngenes / 2, i;
		expression_init(acid_tables[0], parm->M, acid_tables[1], acid_tables[2], "", "", "", "ChrMod_Limited", debug);
        vdelayed = (double **)calloc(nmrna, sizeof(double *)); /* value of v in v_j(t-delay_i)*/
        for(i = 0; i < nmrna; i++) {
            vdelayed[i] = (double *)calloc(defs->ngenes * defs->nnucs, sizeof(double));
			dirichlet_boundary_coef[i] = 1.0;
			dirichlet_boundary_coef[i + nmrna] = 1.0;
        }
        ODESolve_set_dir(nmrna);
        free(acid_tables[0]);
        free(acid_tables[1]);
        free(acid_tables[2]);
        free(acid_tables);
	} else if ( model == HEURR ) {
        char**acid_tables = gcdm_init_acid(fp);
	    int nmrna = defs->ngenes / 2, i;
		expression_init(acid_tables[0], parm->M, acid_tables[1], acid_tables[2], "", "", "", "ChrMod_Unlimited", debug);
        vdelayed = (double **)calloc(nmrna, sizeof(double *)); /* value of v in v_j(t-delay_i)*/
        for(i = 0; i < nmrna; i++) {
            vdelayed[i] = (double *)calloc(defs->ngenes * defs->nnucs, sizeof(double));
			dirichlet_boundary_coef[i] = 1.0;
			dirichlet_boundary_coef[i + nmrna] = 1.0;
        }
        ODESolve_set_dir(nmrna);
        free(acid_tables[0]);
        free(acid_tables[1]);
        free(acid_tables[2]);
        free(acid_tables);
	} else if ( model == HEQUENCHING ) {
        char**acid_tables = gcdm_init_acid(fp);
		expression_init(acid_tables[0], parm->M, acid_tables[1], acid_tables[2], "", "", "", "Quenching", debug);
        free(acid_tables[0]);
        free(acid_tables[1]);
        free(acid_tables[2]);
        free(acid_tables);
	} else if ( model == HELOGISTIC ) {
        char**acid_tables = gcdm_init_acid(fp);
		expression_init(acid_tables[0], parm->M, acid_tables[1], acid_tables[2], "", "", "", "Logistic", debug);
        free(acid_tables[0]);
        free(acid_tables[1]);
        free(acid_tables[2]);
        free(acid_tables);
	}
}
#endif

/*** CLEANUP FUNCTIONS *****************************************************/
/*** FreeZygote: frees memory for D, vinput, bot2 and bot arrays ***********
 ***************************************************************************/
void FreeZygote(void)
{
	free(D);
	free(vinput);
	free(vext);
#ifdef XTRA
	free(input_He);
	free(params_He);
#else
	free(M);
	free(MM);
	free(DCoef);
	free(bot5);
	free(bot3);
	free(bot2);
	free(bot);
#endif
}
/*** FreeMutant: frees mutated parameter struct ****************************
 ***************************************************************************/
void FreeMutant(void)
{
	free(lparm->R);
	free(lparm->T);
	free(lparm->E);
	free(lparm->M);
	free(lparm->P);
	free(lparm->h);
	free(lparm->d);
	free(lparm->lambda);
	free(lparm->tau);
	free(lparm->m);
	free(lparm->mm);
}

/*** DERIVATIVE FUNCTIONS **************************************************/
/*** Derivative functions: *************************************************
 *                                                                         *
 *   These functions calculate derivatives for the solver function based   *
 *   on the state variables in array v for the time t; the output is       *
 *   written into array vdot; n describes the size of both v and vdot      *
 *                                                                         *
 *   There are different g(u) functions that can be used in each deriva-   *
 *   tive function (see comments in the code below). They are set by the   *
 *   command line option -g.                                               *
 *                                                                         *
 *   There are two rules for the derivative function, one for INTERPHASE   *
 *   and one for MITOSIS. The difference between the two is that only de-  *
 *   cay and diffusion happen during mitosis and the regulation term is    *
 *   only included during INTERPHASE. The rule gets set in Blastoderm      *
 *   (using SetRule) to ensure that the derivative function is in the      *
 *   right rule.                                                           *
 *                                                                         *
 *   JR: We get rid of 3 of 4 divisions in the inner loop by nesting. We   *
 *   get rid of an if by adding one iteration of the loop before and one   *
 *   after for the diffusion (wall) boundary conditions.                   *
 *                                                                         *
 ***************************************************************************/

/*** DvdtOrig: the original derivative function; implements the equations **
 *             as published in Reinitz & Sharp (1995), Mech Dev 49, 133-58 *
 *             plus different g(u) functions as used by Yousong Wang in    *
 *             spring 2002.                                                *
 ***************************************************************************/
void DvdtOrig(double *v, double t, double *vdot, int n)
{
	int        ap;                 /* nuclear index on AP axis [0,1,...,m-1] */
	int        i, j;                                   /* local loop counters */
	int        k;                   /* index of gene k in a specific nucleus */
	int        base;            /* index of first gene in a specific nucleus */
	static unsigned int samecycle = 0;            /* same cleavage cycle as before? */
/* get D parameters and bicoid gradient according to cleavage cycle */
/* previously it depended on num_nucs*/
	if (ccycle != samecycle) {
		GetD(t, lparm->d, defs->diff_schedule, D);
		GetRCoef(t, &mit_factor);
		vmat = gcmd_get_maternal_inputs(ccycle);
		samecycle = ccycle;
	}
	if ( defs->egenes ) {
		gcmd_get_external_inputs(t, vext, num_nucs * defs->egenes);
	}
#ifdef XTRA
	if ( model != HEDIRECT && model != HESRR && model != HEURR && model != HEQUENCHING && model != HELOGISTIC )
#endif
	if ( defs->ncouples ) {
		gcmd_get_coupled_inputs_1(t, vcouple, num_nucs * defs->ncouples, v, n, vext, num_nucs * defs->egenes, vmat, num_nucs * defs->mgenes);
	}
/* This is how it works (by JR):
    ap      nucleus position on ap axis
    base    index of first gene (0) in current nucleus
    k       index of gene k in current nucleus
            Protein synthesis terms are calculated according to g(u)
           First we do loop for vinput contributions; vinput contains
   the u that goes into g(u)
   Then we do a separate loop or vector func for sqrt or exp
   Then we do vdot contributions (R and lambda part)
   Then we do the spatial part (Ds); we do a special case for
            each end
            Note, however, that before the real loop, we have to do a
            special case for when rhere is one nuc, hence no diffusion
    These loops look a little funky 'cause we don't want any
            divides'                                                       */
	if (rule == INTERPHASE) {
		register double vinput1 = 0;
		for(base = 0, ap=0; base < n ; base += defs->ngenes, ap++) {
			for(i=base; i < base + defs->ngenes; i++) {
				k = i - base;
				vinput1  = lparm->h[k];
				for(j=0; j < defs->mgenes; j++) {
					vinput1  += lparm->M[(k*defs->mgenes)+j] * vmat[ap * defs->mgenes + j];
				}
				for(j=0; j < defs->ngenes; j++) {
					vinput1  += lparm->T[(k*defs->ngenes)+j] * v[base + j];
				}
				for(j=0; j < defs->egenes; j++) {
					vinput1  += lparm->E[(k*defs->egenes)+j] * vext[ap * defs->egenes + j];
				}
				for(j=0; j < defs->ncouples; j++) {
					vinput1 += lparm->P[(k*defs->ncouples)+j] * vcouple[ap * defs->ncouples + j];
				}
				bot2[i]   = 1 + vinput1 * vinput1;
				vinput[i] = vinput1;
			}
		}
/***************************************************************************
 *                                                                         *
 *        g(u) = 1/2 * ( u / sqrt(1 + u^2) + 1)                            *
 *                                                                         *
 ***************************************************************************/
		if ( gofu == Sqrt ) {
/* now calculate sqrt(1+u2); store it in bot[] */
#ifdef MKL
			vdInvSqrt(n, bot2, bot);
#else
			for(i=0; i < n; i++)                  /* slow traditional style sqrt */
				bot[i] = 1 / sqrt(bot2[i]);
#endif
			for(i=0; i < n; i++) {
				vinput1 = bot[i];
				bot[i]= vinput[i] * vinput1+1;
			}
/***************************************************************************
 *                                                                         *
 *        g(u) = 1/2 * (tanh(u) + 1) )                                     *
 *                                                                         *
 ***************************************************************************/
		} else if ( gofu == Tanh ) {
#ifdef MKL
			vdTanh(n, vinput, bot);
			for(i=0; i < n; i++) {
				bot[i] += 1;
			}
#else
			for(i=0; i < n; i++) {
				bot[i]= tanh(vinput[i])+1;
			}
#endif
/***************************************************************************
 *                                                                         *
 *        g(u) = 1 / (1 + exp(-2u))                                        *
 *                                                                         *
 ***************************************************************************/
		} else if ( gofu == Exp ) {
/* now calculate exp(-u); store it in bot[] */
			for(i=0; i < n; i++) {
				bot[i]= 1.0 / ( 1 + exp( -2.0 * vinput[i]) );
			}
		} else {
			error("DvdtOrig: unknown g(u)");
		}
            /* next loop does the rest of the equation (R, Ds and lambdas) */
                                                 /* store result in vdot[] */
		if ( n == defs->ngenes ) {       /* first part: one nuc, no diffusion */
			register double vdot1, g1;
			for( base=0; base<n; base+=defs->ngenes ) {
				for( i=base; i<base+defs->ngenes; i++ ) {
					k = i - base;
					vdot1 = -lparm->lambda[k] * v[i];
					g1 = bot[i];
					vdot1  += lparm->R[k] * 0.5 * g1 * mit_factor;
					vdot[i] = vdot1;
				}
			}
/* It works only for more than 3 nuclei */
		} else {                    /* then for multiple nuclei -> diffusion */
			register double vdot1,g1;
			for(i=0; i < defs->ngenes; i++) {     /* first anterior-most nucleus */
				k = i;
				vdot1 = -lparm->lambda[k] * v[i];
				g1 = bot[i];
				vdot1 += lparm->R[k] * 0.5 * g1  * mit_factor;
				vdot1 += D[i] * (v[i + defs->ngenes] - v[i]) * boundary;
				vdot[i] = vdot1;
			}
/* then middle nuclei */
			for(base = defs->ngenes; base < n - defs->ngenes; base += defs->ngenes) {
				for(i=base; i < base + defs->ngenes; i++) {
					k = i - base;
					vdot1 = -lparm->lambda[k] * v[i];
					g1 = bot[i];
	  				vdot1 += lparm->R[k] * 0.5 * g1  * mit_factor;
  					vdot1 += D[k] * ((v[i - defs->ngenes] - v[i]) + (v[i + defs->ngenes] - v[i]) );
	  				vdot[i] = vdot1;
				}
			}

/* last: posterior-most nucleus */
			base = n - defs->ngenes;
			for(i=base; i < base + defs->ngenes; i++) {
				k = i - base;
				vdot1 = -lparm->lambda[k] * v[i];
				g1 = bot[i];
				vdot1 += lparm->R[k] * 0.5 * g1  * mit_factor;
				vdot1 += D[k] * (v[i - defs->ngenes] - v[i]) * boundary;
				vdot[i] = vdot1;
			}
		}
/* during mitosis only diffusion and decay happen */
	} else if (rule == MITOSIS) {
		if ( n == defs->ngenes ) {         /* first part: one nuc, no diffusion */
			register double vdot1;
			for( base=0; base<n; base+=defs->ngenes ) {
				for( i=base; i<base+defs->ngenes; i++ ) {
					k = i - base;
					vdot1 = -lparm->lambda[k] * v[i];
					vdot[i] = vdot1;
				}
			}
		} else {                      /* then for multiple nuclei -> diffusion */
			register double vdot1;
			for(i=0; i < defs->ngenes; i++) {     /* first anterior-most nucleus */
				k = i;
				vdot1 = -lparm->lambda[k] * v[i];
				vdot1 += D[i] * (v[i + defs->ngenes] - v[i]) * boundary;
				vdot[i] = vdot1;
			}
/* then middle nuclei */
			for( base = defs->ngenes; base < n - defs->ngenes; base += defs->ngenes) {
				for( i =base; i < base + defs->ngenes; i++) {
					k = i - base;
					vdot1 = -lparm->lambda[k] * v[i];
					vdot1 += D[k] * ((v[i - defs->ngenes] - v[i]) + (v[i + defs->ngenes] - v[i]) );
					vdot[i] = vdot1;
				}
			}
/* last: posterior-most nucleus */
			base = n - defs->ngenes;
			for( i = base; i < base + defs->ngenes; i++) {
				k = i - base;
				vdot1 = -lparm->lambda[k] * v[i];
				vdot1 += D[k] * (v[i - defs->ngenes] - v[i]) * boundary;
				vdot[i] = vdot1;
			}
		}
	} else
		error("DvdtOrig: Bad rule %i sent to DvdtOrig", rule);
}
#ifdef XTRA
/*** DvwdtOrig: the original derivative function; implements the equations **
 *             as published in Reinitz & Sharp (1995), Mech Dev 49, 133-58 *
 *             plus different g(u) functions as used by Yousong Wang in    *
 *             spring 2002. Plus He TD model                               *
 ***************************************************************************/
void DvwdtOrig(double *v, double t, double *vdot, int n)
{
	int        ap;                 /* nuclear index on AP axis [0,1,...,m-1] */
	int        i, j;                                   /* local loop counters */
	int        k;                   /* index of gene k in a specific nucleus */
	int        base;            /* index of first gene in a specific nucleus */
	static unsigned int samecycle = 0;            /* same cleavage cycle as before? */
/* get D parameters and bicoid gradient according to cleavage cycle */
/* previously it depended on num_nucs*/
	int nmrna = defs->ngenes / 2;
	if (ccycle != samecycle) {
		GetD(t, lparm->d, defs->diff_schedule, D);
		GetRCoef(t, &mit_factor);
		vmat = gcmd_get_maternal_inputs(ccycle);
		samecycle = ccycle;
	}
	if ( defs->egenes ) {
		gcmd_get_external_inputs(t, vext, num_nucs * defs->egenes);
	}
/* This is how it works (by JR):
    ap      nucleus position on ap axis
    base    index of first gene (0) in current nucleus
    k       index of gene k in current nucleus
            Protein synthesis terms are calculated according to g(u)
           First we do loop for vinput contributions; vinput contains
   the u that goes into g(u)
   Then we do a separate loop or vector func for sqrt or exp
   Then we do vdot contributions (R and lambda part)
   Then we do the spatial part (Ds); we do a special case for
            each end
            Note, however, that before the real loop, we have to do a
            special case for when rhere is one nuc, hence no diffusion
    These loops look a little funky 'cause we don't want any
            divides'                                                       */
	for( i = 0; i < nmrna; i++ ) {
		MM[i] = ( t - lparm->tau[i] ) > 0 ? (1 - GetRule(t - lparm->tau[i])) : 0;
	}
	if (rule == INTERPHASE) {
		register double vinput1 = 0;
		if ( defs->mob_schedule == 'B' ) {
			for(base = 0, ap=0; base < n ; base += defs->ngenes, ap++) {
				for( i = base; i < base + nmrna; i++) {
					int g;
					k = i - base;
					g = 0;
					for (j = 0; j < nmrna; j++) {
						input_He[g] = v[base + j];
						params_He[g] = lparm->T[(k*defs->ngenes)+j];
						g++;
					}
					for (j = 0; j < defs->egenes; j++) {
						input_He[g] = vext[ap * defs->egenes + j];
						params_He[g] = lparm->E[(k*defs->egenes)+j];
						g++;
					}
					params_He[g] = lparm->h[k]; /* basal_txp , n*/
					g++;
					params_He[g] = lparm->m[k]; /* short range repression treshold , n + 1*/
					g++;
					params_He[g] = lparm->mm[k]; /* max contact for all, n + 2*/
					g++;
					for (j = 0; j < 2 * ( defs->egenes + nmrna ); j++) {
						params_He[g] = lparm->P[j]; /* max bindings,  cooperativity*/
						g++;
					}
					vinput[i + nmrna] = expression( input_He, params_He, k, &(zOn[i + nmrna]), &(zOff[i + nmrna]));
					if ( debug ) {
						fprintf(stdout, "Time = %f\tSeq = %d\tZ_off = %f\tZ_on = %f\tefficiency(Z_on / Z_off) = %f\tpromoterOcc(E) = %f\n", t, k, zOff[i + nmrna], zOn[i + nmrna], zOn[i + nmrna]/zOff[i + nmrna], vinput[i + nmrna]);
					}
				}
			}
		} else if ( defs->mob_schedule == 'A' ) {
			for(base = 0, ap=0; base < n ; base += defs->ngenes, ap++) {
				for( i = base; i < base + nmrna; i++) {
					int g;
					k = i - base;
					g = 0;
					for (j = 0; j < nmrna; j++) {
						input_He[g] = v[base + j];
						params_He[g] = lparm->T[j];
						g++;
					}
					params_He[k] = lparm->M[k];
					for (j = 0; j < defs->egenes; j++) {
						input_He[g] = vext[ap * defs->egenes + j];
						params_He[g] = lparm->E[j];
						g++;
					}
					params_He[g] = lparm->h[k]; /* basal_txp , n*/
					g++;
					params_He[g] = lparm->m[k]; /* short range repression treshold , n + 1*/
					g++;
					params_He[g] = lparm->mm[k]; /* max contact for all, n + 2*/
					g++;
					for (j = 0; j < 2 * ( defs->egenes + nmrna ); j++) {
						params_He[g] = lparm->P[j]; /* max bindings,  cooperativity*/
						g++;
					}
					vinput[i + nmrna] = expression( input_He, params_He, k, &(zOn[i + nmrna]), &(zOff[i + nmrna]));
					if ( debug ) {
						fprintf(stdout, "Time = %f\tSeq = %d\tZ_off = %f\tZ_on = %f\tefficiency(Z_on / Z_off) = %f\tpromoterOcc(E) = %f\n", t, k, zOff[i + nmrna], zOn[i + nmrna], zOn[i + nmrna]/zOff[i + nmrna], vinput[i + nmrna]);
					}
				}
			}
		}
            /* next loop does the rest of the equation (R, Ds and lambdas) */
                                                 /* store result in vdot[] */
		if ( n == defs->ngenes ) {       /* first part: one nuc, no diffusion */
			register double vdot1;
			for( base = 0; base < n; base += defs->ngenes ) {
				for( i = base; i < base + nmrna; i++ ) {
					k = i - base;
					vdot1 = -lparm->lambda[k] * v[i];
					vdot1 += lparm->R[k] * v[i + nmrna] * MM[k];
					vdot[i] = vdot1;
				}
				for( i = base + nmrna; i < base + defs->ngenes; i++ ) {
					k = i - base;
					vdot1 = -lparm->lambda[k] * v[i];
					vdot1 += lparm->R[k] * vinput[i];
					vdot[i] = vdot1;
				}
			}
/* It works only for more than 3 nuclei */
		} else {                    /* then for multiple nuclei -> diffusion */
			register double vdot1;
			for( i = 0; i <  nmrna; i++ ) {     /* first anterior-most nucleus */
				k = i;
				vdot1 = -lparm->lambda[k] * v[i];
				vdot1 += lparm->R[k] * v[i + nmrna] * MM[k];
				vdot1 += D[i] * (v[i + defs->ngenes] - v[i]) * boundary;
				vdot[i] = vdot1;
			}
			for( i = nmrna; i < defs->ngenes; i++ ) {     /* first anterior-most nucleus */
				k = i;
				vdot1 = -lparm->lambda[k] * v[i];
				vdot1 += lparm->R[k] * vinput[i];
				vdot[i] = vdot1;
			}
/* then middle nuclei */
			for(base = defs->ngenes; base < n - defs->ngenes; base += defs->ngenes) {
				for( i = base; i < base + nmrna; i++) {
					k = i - base;
					vdot1 = -lparm->lambda[k] * v[i];
	  				vdot1 += lparm->R[k] * v[i + nmrna] * MM[k];
  					vdot1 += D[k] * ((v[i - defs->ngenes] - v[i]) + (v[i + defs->ngenes] - v[i]) );
	  				vdot[i] = vdot1;
				}
				for( i = base + nmrna; i < base + defs->ngenes; i++) {
					k = i - base;
					vdot1 = -lparm->lambda[k] * v[i];
	  				vdot1 += lparm->R[k] * vinput[i];
	  				vdot[i] = vdot1;
				}
			}
/* last: posterior-most nucleus */
			base = n - defs->ngenes;
			for( i = base; i < base + nmrna; i++) {
				k = i - base;
				vdot1 = -lparm->lambda[k] * v[i];
				vdot1 += lparm->R[k] * v[i + nmrna] * MM[k];
				vdot1 += D[k] * (v[i - defs->ngenes] - v[i]) * boundary;
				vdot[i] = vdot1;
			}
			for( i = base + nmrna; i < base + defs->ngenes; i++) {
				k = i - base;
				vdot1 = -lparm->lambda[k] * v[i];
				vdot1 += lparm->R[k] * vinput[i];
				vdot[i] = vdot1;
			}
		}
/* during mitosis only diffusion and decay happen */
	} else if (rule == MITOSIS) {
		if ( n == defs->ngenes ) {       /* first part: one nuc, no diffusion */
			register double vdot1;
			for( base = 0; base < n; base += defs->ngenes ) {
				for( i = base; i < base + nmrna; i++ ) {
					k = i - base;
					vdot1 = -lparm->lambda[k] * v[i];
					vdot[i] = vdot1;
				}
				for( i = base + nmrna; i < base + defs->ngenes; i++ ) {
					k = i - base;
					vdot1 = -lparm->lambda[k] * v[i];
					vdot[i] = vdot1;
				}
			}
/* It works only for more than 3 nuclei */
		} else {                    /* then for multiple nuclei -> diffusion */
			register double vdot1;
			for( i = 0; i <  nmrna; i++ ) {     /* first anterior-most nucleus */
				k = i;
				vdot1 = -lparm->lambda[k] * v[i];
				vdot1 += D[i] * (v[i + defs->ngenes] - v[i]) * boundary;
				vdot[i] = vdot1;
			}
			for( i = nmrna; i < defs->ngenes; i++ ) {     /* first anterior-most nucleus */
				k = i;
				vdot1 = -lparm->lambda[k] * v[i];
				vdot[i] = vdot1;
			}
/* then middle nuclei */
			for(base = defs->ngenes; base < n - defs->ngenes; base += defs->ngenes) {
				for( i = base; i < base + nmrna; i++) {
					k = i - base;
					vdot1 = -lparm->lambda[k] * v[i];
  					vdot1 += D[k] * ((v[i - defs->ngenes] - v[i]) + (v[i + defs->ngenes] - v[i]) );
	  				vdot[i] = vdot1;
				}
				for( i = base + nmrna; i < base + defs->ngenes; i++) {
					k = i - base;
					vdot1 = -lparm->lambda[k] * v[i];
	  				vdot[i] = vdot1;
				}
			}
/* last: posterior-most nucleus */
			base = n - defs->ngenes;
			for( i = base; i < base + nmrna; i++) {
				k = i - base;
				vdot1 = -lparm->lambda[k] * v[i];
				vdot1 += D[k] * (v[i - defs->ngenes] - v[i]) * boundary;
				vdot[i] = vdot1;
			}
			for( i = base + nmrna; i < base + defs->ngenes; i++) {
				k = i - base;
				vdot1 = -lparm->lambda[k] * v[i];
				vdot[i] = vdot1;
			}
		}
	} else
		error("DvwdtOrig: Bad rule %i sent to DvwdtOrig", rule);
}

void DdvwdtOrig(double *v, double t, double *vdot, int n)
{
	int        ap;                 /* nuclear index on AP axis [0,1,...,m-1] */
	int        i, j;                                   /* local loop counters */
	int        k;                   /* index of gene k in a specific nucleus */
	int        base;            /* index of first gene in a specific nucleus */
	static unsigned int samecycle = 0;            /* same cleavage cycle as before? */
/* get D parameters and bicoid gradient according to cleavage cycle */
/* previously it depended on num_nucs*/
	int nmrna = defs->ngenes / 2;
	if (ccycle != samecycle) {
		GetD(t, lparm->d, defs->diff_schedule, D);
		GetRCoef(t, &mit_factor);
		vmat = gcmd_get_maternal_inputs(ccycle);
		samecycle = ccycle;
	}
	if ( defs->egenes ) {
		gcmd_get_external_inputs(t, vext, num_nucs * defs->egenes);
	}
/* This is how it works (by JR):
    ap      nucleus position on ap axis
    base    index of first gene (0) in current nucleus
    k       index of gene k in current nucleus
            Protein synthesis terms are calculated according to g(u)
           First we do loop for vinput contributions; vinput contains
   the u that goes into g(u)
   Then we do a separate loop or vector func for sqrt or exp
   Then we do vdot contributions (R and lambda part)
   Then we do the spatial part (Ds); we do a special case for
            each end
            Note, however, that before the real loop, we have to do a
            special case for when rhere is one nuc, hence no diffusion
    These loops look a little funky 'cause we don't want any
            divides'                                                       */
	if (rule == INTERPHASE) {
		register double vinput1 = 0;
		if ( defs->mob_schedule == 'B' ) {
			for(base = 0, ap=0; base < n ; base += defs->ngenes, ap++) {
				for( i = base; i < base + nmrna; i++) {
					int g;
					k = i - base;
					g = 0;
					for (j = 0; j < nmrna; j++) {
						input_He[g] = v[base + j];
						params_He[g] = lparm->T[(k*defs->ngenes)+j];
						g++;
					}
					for (j = 0; j < defs->egenes; j++) {
						input_He[g] = vext[ap * defs->egenes + j];
						params_He[g] = lparm->E[(k*defs->egenes)+j];
						g++;
					}
					params_He[g] = lparm->h[k]; /* basal_txp , n*/
					g++;
					params_He[g] = lparm->m[k]; /* short range repression treshold , n + 1*/
					g++;
					params_He[g] = lparm->mm[k]; /* max contact for all, n + 2*/
					g++;
					for (j = 0; j < 2 * ( defs->egenes + nmrna ); j++) {
						params_He[g] = lparm->P[j]; /* max bindings,  cooperativity*/
						g++;
					}
					vinput[i + nmrna] = expression( input_He, params_He, k, &(zOn[i + nmrna]), &(zOff[i + nmrna]));
					if ( debug ) {
						fprintf(stdout, "Time = %f\tSeq = %d\tZ_off = %f\tZ_on = %f\tefficiency(Z_on / Z_off) = %f\tpromoterOcc(E) = %f\n", t, k, zOff[i + nmrna], zOn[i + nmrna], zOn[i + nmrna]/zOff[i + nmrna], vinput[i + nmrna]);
					}
				}
			}
		} else if ( defs->mob_schedule == 'A' ) {
			for(base = 0, ap=0; base < n ; base += defs->ngenes, ap++) {
				for( i = base; i < base + nmrna; i++) {
					int g;
					k = i - base;
					g = 0;
					for (j = 0; j < nmrna; j++) {
						input_He[g] = v[base + j];
						params_He[g] = lparm->T[j];
						g++;
					}
					params_He[k] = lparm->M[k];
					for (j = 0; j < defs->egenes; j++) {
						input_He[g] = vext[ap * defs->egenes + j];
						params_He[g] = lparm->E[j];
						g++;
					}
					params_He[g] = lparm->h[k]; /* basal_txp , n*/
					g++;
					params_He[g] = lparm->m[k]; /* short range repression treshold , n + 1*/
					g++;
					params_He[g] = lparm->mm[k]; /* max contact for all, n + 2*/
					g++;
					for (j = 0; j < 2 * ( defs->egenes + nmrna ); j++) {
						params_He[g] = lparm->P[j]; /* max bindings,  cooperativity*/
						g++;
					}
					vinput[i + nmrna] = expression( input_He, params_He, k, &(zOn[i + nmrna]), &(zOff[i + nmrna]));
					if ( debug ) {
						fprintf(stdout, "Time = %f\tSeq = %d\tZ_off = %f\tZ_on = %f\tefficiency(Z_on / Z_off) = %f\tpromoterOcc(E) = %f\n", t, k, zOff[i + nmrna], zOn[i + nmrna], zOn[i + nmrna]/zOff[i + nmrna], vinput[i + nmrna]);
					}
				}
			}
		}
		for( i = 0; i < nmrna; i++ ) {
            gcdm_get_delayed(vdelayed[i], t - lparm->tau[i]);
        }
            /* next loop does the rest of the equation (R, Ds and lambdas) */
                                                 /* store result in vdot[] */
		if ( n == defs->ngenes ) {       /* first part: one nuc, no diffusion */
			register double vdot1;
			for( base = 0; base < n; base += defs->ngenes ) {
				for( i = base; i < base + nmrna; i++ ) {
					k = i - base;
					vdot1 = -lparm->lambda[k] * v[i];
					vdot1 += lparm->R[k] * vdelayed[k][i + nmrna];
					vdot[i] = vdot1;
				}
				for( i = base + nmrna; i < base + defs->ngenes; i++ ) {
					k = i - base;
					vdot1 = -lparm->lambda[k] * v[i];
					vdot1 += lparm->R[k] * vinput[i];
					vdot[i] = vdot1;
				}
			}
/* It works only for more than 3 nuclei */
		} else {                    /* then for multiple nuclei -> diffusion */
			register double vdot1;
			for( i = 0; i <  nmrna; i++ ) {     /* first anterior-most nucleus */
				k = i;
				vdot1 = -lparm->lambda[k] * v[i];
				vdot1 += lparm->R[k] * vdelayed[k][i + nmrna];
				vdot1 += D[k] * (v[i + defs->ngenes] - v[i]) * boundary;
				vdot[i] = vdot1;
			}
			for( i = nmrna; i < defs->ngenes; i++ ) {     /* first anterior-most nucleus */
				k = i;
				vdot1 = -lparm->lambda[k] * v[i];
				vdot1 += lparm->R[k] * vinput[i];
				vdot1 += D[k] * (v[i + defs->ngenes] - v[i]) * boundary;
				vdot[i] = vdot1;
			}
/* then middle nuclei */
			for(base = defs->ngenes; base < n - defs->ngenes; base += defs->ngenes) {
				for( i = base; i < base + nmrna; i++) {
					k = i - base;
					vdot1 = -lparm->lambda[k] * v[i];
	  				vdot1 += lparm->R[k] * vdelayed[k][i + nmrna];
  					vdot1 += D[k] * ((v[i - defs->ngenes] - v[i]) + (v[i + defs->ngenes] - v[i]) );
	  				vdot[i] = vdot1;
				}
				for( i = base + nmrna; i < base + defs->ngenes; i++) {
					k = i - base;
					vdot1 = -lparm->lambda[k] * v[i];
	  				vdot1 += lparm->R[k] * vinput[i];
  					vdot1 += D[k] * ((v[i - defs->ngenes] - v[i]) + (v[i + defs->ngenes] - v[i]) );
	  				vdot[i] = vdot1;
				}
			}
/* last: posterior-most nucleus */
			base = n - defs->ngenes;
			for( i = base; i < base + nmrna; i++) {
				k = i - base;
				vdot1 = -lparm->lambda[k] * v[i];
				vdot1 += lparm->R[k] * vdelayed[k][i + nmrna];
				vdot1 += D[k] * (v[i - defs->ngenes] - v[i]) * boundary;
				vdot[i] = vdot1;
			}
			for( i = base + nmrna; i < base + defs->ngenes; i++) {
				k = i - base;
				vdot1 = -lparm->lambda[k] * v[i];
				vdot1 += lparm->R[k] * vinput[i];
				vdot1 += D[k] * (v[i - defs->ngenes] - v[i]) * boundary;
				vdot[i] = vdot1;
			}
		}
/* during mitosis only diffusion and decay happen */
	} else if (rule == MITOSIS) {
		if ( n == defs->ngenes ) {       /* first part: one nuc, no diffusion */
			register double vdot1;
			for( base = 0; base < n; base += defs->ngenes ) {
				for( i = base; i < base + nmrna; i++ ) {
					k = i - base;
					vdot1 = -lparm->lambda[k] * v[i];
					vdot[i] = vdot1;
				}
				for( i = base + nmrna; i < base + defs->ngenes; i++ ) {
					k = i - base;
					vdot1 = -lparm->lambda[k] * v[i];
					vdot[i] = vdot1;
				}
			}
/* It works only for more than 3 nuclei */
		} else {                    /* then for multiple nuclei -> diffusion */
			register double vdot1;
			for( i = 0; i <  nmrna; i++ ) {     /* first anterior-most nucleus */
				k = i;
				vdot1 = -lparm->lambda[k] * v[i];
				vdot1 += D[k] * (v[i + defs->ngenes] - v[i]) * boundary;
				vdot[i] = vdot1;
			}
			for( i = nmrna; i < defs->ngenes; i++ ) {     /* first anterior-most nucleus */
				k = i;
				vdot1 = -lparm->lambda[k] * v[i];
				vdot1 += D[k] * (v[i + defs->ngenes] - v[i]) * boundary;
				vdot[i] = vdot1;
			}
/* then middle nuclei */
			for(base = defs->ngenes; base < n - defs->ngenes; base += defs->ngenes) {
				for( i = base; i < base + nmrna; i++) {
					k = i - base;
					vdot1 = -lparm->lambda[k] * v[i];
  					vdot1 += D[k] * ((v[i - defs->ngenes] - v[i]) + (v[i + defs->ngenes] - v[i]) );
	  				vdot[i] = vdot1;
				}
				for( i = base + nmrna; i < base + defs->ngenes; i++) {
					k = i - base;
					vdot1 = -lparm->lambda[k] * v[i];
  					vdot1 += D[k] * ((v[i - defs->ngenes] - v[i]) + (v[i + defs->ngenes] - v[i]) );
	  				vdot[i] = vdot1;
				}
			}
/* last: posterior-most nucleus */
			base = n - defs->ngenes;
			for( i = base; i < base + nmrna; i++) {
				k = i - base;
				vdot1 = -lparm->lambda[k] * v[i];
				vdot1 += D[k] * (v[i - defs->ngenes] - v[i]) * boundary;
				vdot[i] = vdot1;
			}
			for( i = base + nmrna; i < base + defs->ngenes; i++) {
				k = i - base;
				vdot1 = -lparm->lambda[k] * v[i];
				vdot1 += D[k] * (v[i - defs->ngenes] - v[i]) * boundary;
				vdot[i] = vdot1;
			}
		}
	} else
		error("DvwdtOrig: Bad rule %i sent to DvwdtOrig", rule);
}

/* This function DddvwdtOrig uses our calced cons as inputs
 * Option ddmrna
 * */

void DddvwdtOrig(double *v, double t, double *vdot, int n)
{
	int        ap;                 /* nuclear index on AP axis [0,1,...,m-1] */
	int        i, j;                                   /* local loop counters */
	int        k;                   /* index of gene k in a specific nucleus */
	int        base;            /* index of first gene in a specific nucleus */
	static unsigned int samecycle = 0;            /* same cleavage cycle as before? */
/* get D parameters and bicoid gradient according to cleavage cycle */
/* previously it depended on num_nucs*/
	int nmrna = defs->ngenes / 2;
	if (ccycle != samecycle) {
		GetD(t, lparm->d, defs->diff_schedule, D);
		GetRCoef(t, &mit_factor);
		samecycle = ccycle;
	}
/* This is how it works (by JR):
    ap      nucleus position on ap axis
    base    index of first gene (0) in current nucleus
    k       index of gene k in current nucleus
            Protein synthesis terms are calculated according to g(u)
           First we do loop for vinput contributions; vinput contains
   the u that goes into g(u)
   Then we do a separate loop or vector func for sqrt or exp
   Then we do vdot contributions (R and lambda part)
   Then we do the spatial part (Ds); we do a special case for
            each end
            Note, however, that before the real loop, we have to do a
            special case for when rhere is one nuc, hence no diffusion
    These loops look a little funky 'cause we don't want any
            divides'                                                       */
	if (rule == INTERPHASE) {
		DddwwdtFocus(v, t, vinput, n);
		for( i = 0; i < nmrna; i++ ) {
            gcdm_get_delayed(vdelayed[i], t - lparm->tau[i]);
        }
            /* next loop does the rest of the equation (R, Ds and lambdas) */
                                                 /* store result in vdot[] */
		if ( n == defs->ngenes ) {       /* first part: one nuc, no diffusion */
			register double vdot1;
			for( base = 0; base < n; base += defs->ngenes ) {
				for( i = base; i < base + nmrna; i++ ) {
					k = i - base;
					vdot1 = -lparm->lambda[k] * v[i];
					vdot1 += lparm->R[k] * vdelayed[k][i + nmrna];
					vdot[i] = vdot1;
				}
				for( i = base + nmrna; i < base + defs->ngenes; i++ ) {
					k = i - base;
					vdot1 = -lparm->lambda[k] * v[i];
					vdot1 += lparm->R[k] * vinput[i];
					vdot[i] = vdot1;
				}
			}
/* It works only for more than 3 nuclei */
		} else {                    /* then for multiple nuclei -> diffusion */
			register double vdot1;
			for( i = 0; i <  nmrna; i++ ) {     /* first anterior-most nucleus */
				k = i;
				vdot[i] = dirichlet_boundary_coef[k] * vext[defs->egenes - nmrna + k];
			}
			for( i = nmrna; i < defs->ngenes; i++ ) {     /* first anterior-most nucleus */
				k = i;
				vdot1 = -lparm->lambda[k] * v[i];
				vdot1 += lparm->R[k] * vinput[i];
				vdot1 += D[k] * (v[i + defs->ngenes] - v[i]) * boundary;
				vdot[i] = vdot1;
			}
/* then middle nuclei */
			for(base = defs->ngenes; base < n - defs->ngenes; base += defs->ngenes) {
				for( i = base; i < base + nmrna; i++) {
					k = i - base;
					vdot1 = -lparm->lambda[k] * v[i];
	  				vdot1 += lparm->R[k] * vdelayed[k][i + nmrna];
  					vdot1 += D[k] * ((v[i - defs->ngenes] - v[i]) + (v[i + defs->ngenes] - v[i]) );
	  				vdot[i] = vdot1;
				}
				for( i = base + nmrna; i < base + defs->ngenes; i++) {
					k = i - base;
					vdot1 = -lparm->lambda[k] * v[i];
	  				vdot1 += lparm->R[k] * vinput[i];
  					vdot1 += D[k] * ((v[i - defs->ngenes] - v[i]) + (v[i + defs->ngenes] - v[i]) );
	  				vdot[i] = vdot1;
				}
			}
/* last: posterior-most nucleus */
			base = n - defs->ngenes;
			for( i = base; i < base + nmrna; i++) {
				k = i - base;
				vdot[i] = dirichlet_boundary_coef[k] * vext[num_nucs * defs->egenes - nmrna + k];
			}
			for( i = base + nmrna; i < base + defs->ngenes; i++) {
				k = i - base;
				vdot1 = -lparm->lambda[k] * v[i];
				vdot1 += lparm->R[k] * vinput[i];
				vdot1 += D[k] * (v[i - defs->ngenes] - v[i]) * boundary;
				vdot[i] = vdot1;
			}
		}
/* during mitosis only diffusion and decay happen */
	} else if (rule == MITOSIS) {
		if ( n == defs->ngenes ) {       /* first part: one nuc, no diffusion */
			register double vdot1;
			for( base = 0; base < n; base += defs->ngenes ) {
				for( i = base; i < base + nmrna; i++ ) {
					k = i - base;
					vdot1 = -lparm->lambda[k] * v[i];
					vdot[i] = vdot1;
				}
				for( i = base + nmrna; i < base + defs->ngenes; i++ ) {
					k = i - base;
					vdot1 = -lparm->lambda[k] * v[i];
					vdot[i] = vdot1;
				}
			}
/* It works only for more than 3 nuclei */
		} else {                    /* then for multiple nuclei -> diffusion */
			register double vdot1;
			for( i = 0; i <  nmrna; i++ ) {     /* first anterior-most nucleus */
				k = i;
				vdot[i] = dirichlet_boundary_coef[k] * vext[defs->egenes - nmrna + k];
			}
			for( i = nmrna; i < defs->ngenes; i++ ) {     /* first anterior-most nucleus */
				k = i;
				vdot1 = -lparm->lambda[k] * v[i];
				vdot1 += D[k] * (v[i + defs->ngenes] - v[i]) * boundary;
				vdot[i] = vdot1;
			}
/* then middle nuclei */
			for(base = defs->ngenes; base < n - defs->ngenes; base += defs->ngenes) {
				for( i = base; i < base + nmrna; i++) {
					k = i - base;
					vdot1 = -lparm->lambda[k] * v[i];
  					vdot1 += D[k] * ((v[i - defs->ngenes] - v[i]) + (v[i + defs->ngenes] - v[i]) );
	  				vdot[i] = vdot1;
				}
				for( i = base + nmrna; i < base + defs->ngenes; i++) {
					k = i - base;
					vdot1 = -lparm->lambda[k] * v[i];
  					vdot1 += D[k] * ((v[i - defs->ngenes] - v[i]) + (v[i + defs->ngenes] - v[i]) );
	  				vdot[i] = vdot1;
				}
			}
/* last: posterior-most nucleus */
			base = n - defs->ngenes;
			for( i = base; i < base + nmrna; i++) {
				k = i - base;
				vdot[i] = dirichlet_boundary_coef[k] * vext[num_nucs * defs->egenes - nmrna + k];
			}
			for( i = base + nmrna; i < base + defs->ngenes; i++) {
				k = i - base;
				vdot1 = -lparm->lambda[k] * v[i];
				vdot1 += D[k] * (v[i - defs->ngenes] - v[i]) * boundary;
				vdot[i] = vdot1;
			}
		}
	} else
		error("DvwdtOrig: Bad rule %i sent to DvwdtOrig", rule);
}

/* This function DddvwdtDual uses our calced cons as inputs
 * and implements dual action of Hb on Kr
*/

void DddvwdtDual(double *v, double t, double *vdot, int n)
{
	int        ap;                 /* nuclear index on AP axis [0,1,...,m-1] */
	int        i, j;                                   /* local loop counters */
	int        k;                   /* index of gene k in a specific nucleus */
	int        base;            /* index of first gene in a specific nucleus */
	static unsigned int samecycle = 0;            /* same cleavage cycle as before? */
/* get D parameters and bicoid gradient according to cleavage cycle */
/* previously it depended on num_nucs*/
	int nmrna = defs->ngenes / 2;
	if (ccycle != samecycle) {
		GetD(t, lparm->d, defs->diff_schedule, D);
		GetRCoef(t, &mit_factor);
		vmat = gcmd_get_maternal_inputs(ccycle);
		samecycle = ccycle;
	}
	gcmd_get_external_inputs(t, vext, num_nucs * defs->egenes);
/* This is how it works (by JR):
    ap      nucleus position on ap axis
    base    index of first gene (0) in current nucleus
    k       index of gene k in current nucleus
            Protein synthesis terms are calculated according to g(u)
           First we do loop for vinput contributions; vinput contains
   the u that goes into g(u)
   Then we do a separate loop or vector func for sqrt or exp
   Then we do vdot contributions (R and lambda part)
   Then we do the spatial part (Ds); we do a special case for
            each end
            Note, however, that before the real loop, we have to do a
            special case for when rhere is one nuc, hence no diffusion
    These loops look a little funky 'cause we don't want any
            divides'                                                       */
	if (rule == INTERPHASE) {
		register double vinput1 = 0;
		if ( defs->mob_schedule == 'B' ) {
			for(base = 0, ap = 0; base < n; base += defs->ngenes, ap++) {
				for( i = base; i < base + nmrna; i++) {
					int g;
					k = i - base;
					g = 0;
					for (j = 0; j < nmrna; j++) {
						input_He[g] = v[base + j];
						params_He[g] = lparm->T[(k*defs->ngenes)+j];
						g++;
					}
					for (j = 0; j < defs->egenes - nmrna; j++) {
						input_He[g] = vext[ap * defs->egenes + j];
						params_He[g] = lparm->E[(k*defs->egenes)+j];
						g++;
					}
					params_He[g] = lparm->h[k]; /* basal_txp , n*/
					g++;
					params_He[g] = lparm->m[k]; /* short range repression treshold , n + 1*/
					g++;
					params_He[g] = lparm->mm[k]; /* max contact for all, n + 2*/
					g++;
					for (j = 0; j < 2 * defs->egenes; j++) {
						params_He[g] = lparm->P[j]; /* max bindings,  cooperativity*/
						g++;
					}
					vinput[i + nmrna] = expression( input_He, params_He, k, &(zOn[i + nmrna]), &(zOff[i + nmrna]));
					if ( debug ) {
						fprintf(stdout, "Time = %f\tSeq = %d\tZ_off = %f\tZ_on = %f\tefficiency(Z_on / Z_off) = %f\tpromoterOcc(E) = %f\n", t, k, zOff[i + nmrna], zOn[i + nmrna], zOn[i + nmrna]/zOff[i + nmrna], vinput[i + nmrna]);
					}
				}
			}
		} else if ( defs->mob_schedule == 'A' ) { /* the same basal, SRR and max cont */
			for(base = 0, ap = 0; base < n; base += defs->ngenes, ap++) {
				for( i = base; i < base + nmrna; i++) {
					int g;
					k = i - base;
					g = 0;
					for (j = 0; j < nmrna; j++) {
						input_He[g] = v[base + j];
						if (lparm->T[(k*defs->ngenes)+j+nmrna] > 1 && v[base + j] < lparm->h[j + nmrna]) {
							params_He[g] = lparm->T[(k*defs->ngenes)+j+nmrna];
						} else {
							params_He[g] = lparm->T[(k*defs->ngenes)+j];
						}
						g++;
					}
					for (j = 0; j < defs->egenes - nmrna; j++) {
						input_He[g] = vext[ap * defs->egenes + j];
						params_He[g] = lparm->E[(k*defs->egenes)+j];
						g++;
					}
					params_He[g] = lparm->h[0]; /* basal_txp , n*/
					g++;
					params_He[g] = lparm->m[0]; /* short range repression treshold , n + 1*/
					g++;
					params_He[g] = lparm->mm[0]; /* max contact for all, n + 2*/
					g++;
					for (j = 0; j < 2 * defs->egenes; j++) {
						params_He[g] = lparm->P[j]; /* max bindings,  cooperativity*/
						g++;
					}
					vinput[i + nmrna] = expression( input_He, params_He, k, &(zOn[i + nmrna]), &(zOff[i + nmrna]));
					if ( debug ) {
						fprintf(stdout, "Time = %f\tSeq = %d\tZ_off = %f\tZ_on = %f\tefficiency(Z_on / Z_off) = %f\tpromoterOcc(E) = %f\n", t, k, zOff[i + nmrna], zOn[i + nmrna], zOn[i + nmrna]/zOff[i + nmrna], vinput[i + nmrna]);
					}
				}
			}
		} else if ( defs->mob_schedule == 'D' ) {
			for(base = 0, ap = 0; base < n; base += defs->ngenes, ap++) {
				for( i = base; i < base + nmrna; i++) {
					int g;
					k = i - base;
					g = 0;
					for (j = 0; j < nmrna; j++) {
						input_He[g] = v[base + j];
						params_He[g] = lparm->T[j];
						g++;
					}
					params_He[k] = lparm->T[k + nmrna];
					for (j = 0; j < defs->egenes - nmrna; j++) {
						input_He[g] = vext[ap * defs->egenes + j];
						params_He[g] = lparm->E[j];
						g++;
					}
					params_He[g] = lparm->h[k]; /* basal_txp , n*/
					g++;
					params_He[g] = lparm->m[k]; /* short range repression treshold , n + 1*/
					g++;
					params_He[g] = lparm->mm[k]; /* max contact for all, n + 2*/
					g++;
					for (j = 0; j < 2 * defs->egenes; j++) {
						params_He[g] = lparm->P[j]; /* max bindings,  cooperativity*/
						g++;
					}
					vinput[i + nmrna] = expression( input_He, params_He, k, &(zOn[i + nmrna]), &(zOff[i + nmrna]));
					if ( debug ) {
						fprintf(stdout, "Time = %f\tSeq = %d\tZ_off = %f\tZ_on = %f\tefficiency(Z_on / Z_off) = %f\tpromoterOcc(E) = %f\n", t, k, zOff[i + nmrna], zOn[i + nmrna], zOn[i + nmrna]/zOff[i + nmrna], vinput[i + nmrna]);
					}
				}
			}
		}
		for( i = 0; i < nmrna; i++ ) {
            gcdm_get_delayed(vdelayed[i], t - lparm->tau[i]);
        }
            /* next loop does the rest of the equation (R, Ds and lambdas) */
                                                 /* store result in vdot[] */
		if ( n == defs->ngenes ) {       /* first part: one nuc, no diffusion */
			register double vdot1;
			for( base = 0; base < n; base += defs->ngenes ) {
				for( i = base; i < base + nmrna; i++ ) {
					k = i - base;
					vdot1 = -lparm->lambda[k] * v[i];
					vdot1 += lparm->R[k] * vdelayed[k][i + nmrna];
					vdot[i] = vdot1;
				}
				for( i = base + nmrna; i < base + defs->ngenes; i++ ) {
					k = i - base;
					vdot1 = -lparm->lambda[k] * v[i];
					vdot1 += lparm->R[k] * vinput[i];
					vdot[i] = vdot1;
				}
			}
/* It works only for more than 3 nuclei */
		} else {                    /* then for multiple nuclei -> diffusion */
			register double vdot1;
			for( i = 0; i <  nmrna; i++ ) {     /* first anterior-most nucleus */
				k = i;
				vdot[i] = dirichlet_boundary_coef[k] * vext[defs->egenes - nmrna + k];
			}
			for( i = nmrna; i < defs->ngenes; i++ ) {     /* first anterior-most nucleus */
				k = i;
				vdot1 = -lparm->lambda[k] * v[i];
				vdot1 += lparm->R[k] * vinput[i];
				vdot1 += D[k] * (v[i + defs->ngenes] - v[i]) * boundary;
				vdot[i] = vdot1;
			}
/* then middle nuclei */
			for(base = defs->ngenes; base < n - defs->ngenes; base += defs->ngenes) {
				for( i = base; i < base + nmrna; i++) {
					k = i - base;
					vdot1 = -lparm->lambda[k] * v[i];
	  				vdot1 += lparm->R[k] * vdelayed[k][i + nmrna];
  					vdot1 += D[k] * ((v[i - defs->ngenes] - v[i]) + (v[i + defs->ngenes] - v[i]) );
	  				vdot[i] = vdot1;
				}
				for( i = base + nmrna; i < base + defs->ngenes; i++) {
					k = i - base;
					vdot1 = -lparm->lambda[k] * v[i];
	  				vdot1 += lparm->R[k] * vinput[i];
  					vdot1 += D[k] * ((v[i - defs->ngenes] - v[i]) + (v[i + defs->ngenes] - v[i]) );
	  				vdot[i] = vdot1;
				}
			}
/* last: posterior-most nucleus */
			base = n - defs->ngenes;
			for( i = base; i < base + nmrna; i++) {
				k = i - base;
				vdot[i] = dirichlet_boundary_coef[k] * vext[num_nucs * defs->egenes - nmrna + k];
			}
			for( i = base + nmrna; i < base + defs->ngenes; i++) {
				k = i - base;
				vdot1 = -lparm->lambda[k] * v[i];
				vdot1 += lparm->R[k] * vinput[i];
				vdot1 += D[k] * (v[i - defs->ngenes] - v[i]) * boundary;
				vdot[i] = vdot1;
			}
		}
/* during mitosis only diffusion and decay happen */
	} else if (rule == MITOSIS) {
		if ( n == defs->ngenes ) {       /* first part: one nuc, no diffusion */
			register double vdot1;
			for( base = 0; base < n; base += defs->ngenes ) {
				for( i = base; i < base + nmrna; i++ ) {
					k = i - base;
					vdot1 = -lparm->lambda[k] * v[i];
					vdot[i] = vdot1;
				}
				for( i = base + nmrna; i < base + defs->ngenes; i++ ) {
					k = i - base;
					vdot1 = -lparm->lambda[k] * v[i];
					vdot[i] = vdot1;
				}
			}
/* It works only for more than 3 nuclei */
		} else {                    /* then for multiple nuclei -> diffusion */
			register double vdot1;
			for( i = 0; i <  nmrna; i++ ) {     /* first anterior-most nucleus */
				k = i;
				vdot[i] = dirichlet_boundary_coef[k] * vext[defs->egenes - nmrna + k];
			}
			for( i = nmrna; i < defs->ngenes; i++ ) {     /* first anterior-most nucleus */
				k = i;
				vdot1 = -lparm->lambda[k] * v[i];
				vdot1 += D[k] * (v[i + defs->ngenes] - v[i]) * boundary;
				vdot[i] = vdot1;
			}
/* then middle nuclei */
			for(base = defs->ngenes; base < n - defs->ngenes; base += defs->ngenes) {
				for( i = base; i < base + nmrna; i++) {
					k = i - base;
					vdot1 = -lparm->lambda[k] * v[i];
  					vdot1 += D[k] * ((v[i - defs->ngenes] - v[i]) + (v[i + defs->ngenes] - v[i]) );
	  				vdot[i] = vdot1;
				}
				for( i = base + nmrna; i < base + defs->ngenes; i++) {
					k = i - base;
					vdot1 = -lparm->lambda[k] * v[i];
  					vdot1 += D[k] * ((v[i - defs->ngenes] - v[i]) + (v[i + defs->ngenes] - v[i]) );
	  				vdot[i] = vdot1;
				}
			}
/* last: posterior-most nucleus */
			base = n - defs->ngenes;
			for( i = base; i < base + nmrna; i++) {
				k = i - base;
				vdot[i] = dirichlet_boundary_coef[k] * vext[num_nucs * defs->egenes - nmrna + k];
			}
			for( i = base + nmrna; i < base + defs->ngenes; i++) {
				k = i - base;
				vdot1 = -lparm->lambda[k] * v[i];
				vdot1 += D[k] * (v[i - defs->ngenes] - v[i]) * boundary;
				vdot[i] = vdot1;
			}
		}
	} else
		error("DvwdtOrig: Bad rule %i sent to DvwdtOrig", rule);
}

/* This function DddwwdtFocus uses only externals or calculated as inputs 
 * Only foci are calculated
 */

void DddwwdtFocus(double *v, double t, double *vdot, int n)
{
	int        ap;                 /* nuclear index on AP axis [0,1,...,m-1] */
	int        i, j;                                   /* local loop counters */
	int        k;                   /* index of gene k in a specific nucleus */
	int        base;            /* index of first gene in a specific nucleus */
	int nmrna = defs->ngenes / 2;
	gcmd_get_external_inputs(t, vext, num_nucs * defs->egenes);
	for(base = 0, ap = 0; base < n; base += defs->ngenes, ap++) {
		for( i = base; i < base + nmrna; i++) {
			int g;
			k = i - base;
			g = 0;
			for (j = 0; j < nmrna; j++) {
				input_He[g] = (use_external_inputs_only == 1) ? vext[ap * defs->egenes + defs->egenes - nmrna + j] : v[base + j];
				params_He[g] = ( defs->mob_schedule == 'C' ) ? lparm->T[j] : lparm->T[(k*defs->ngenes)+j];
				g++;
			}
			if ( defs->mob_schedule == 'D' ) {
				params_He[k] = lparm->T[k + nmrna];
			}
			for (j = 0; j < defs->egenes - nmrna; j++) {
				input_He[g] = vext[ap * defs->egenes + j];
				params_He[g] = ( defs->mob_schedule == 'C' ) ? lparm->E[j] : lparm->E[(k*defs->egenes)+j];
				g++;
			}
			if ( defs->mob_schedule == 'B' || defs->mob_schedule == 'D' ) {
				params_He[g] = lparm->h[k]; /* basal_txp , n*/
				g++;
				params_He[g] = lparm->m[k]; /* short range repression treshold , n + 1*/
				g++;
				params_He[g] = lparm->mm[k]; /* max contact for all, n + 2*/
				g++;
			} else if ( defs->mob_schedule == 'A' || defs->mob_schedule == 'C' ) {
				params_He[g] = lparm->h[0]; /* basal_txp , n*/
				g++;
				params_He[g] = lparm->m[0]; /* short range repression treshold , n + 1*/
				g++;
				params_He[g] = lparm->mm[0]; /* max contact for all, n + 2*/
				g++;
			}
			for (j = 0; j < 2 * defs->egenes; j++) {
				params_He[g] = lparm->P[j]; /* max bindings,  cooperativity*/
				g++;
			}
			vdot[i + nmrna] = expression( input_He, params_He, k, &(zOn[i + nmrna]), &(zOff[i + nmrna]));
			if ( debug ) {
				fprintf(stdout, "Time = %f\tSeq = %d\tZ_off = %f\tZ_on = %f\tefficiency(Z_on / Z_off) = %f\tpromoterOcc(E) = %f\n", t, k, zOff[i + nmrna], zOn[i + nmrna], zOn[i + nmrna]/zOff[i + nmrna], vdot[i + nmrna]);
			}
		}
	}
}

/* This function DddwwdtOrig uses only externals as inputs */

void DddwwdtOrig(double *v, double t, double *vdot, int n)
{
	int        ap;                 /* nuclear index on AP axis [0,1,...,m-1] */
	int        i, j;                                   /* local loop counters */
	int        k;                   /* index of gene k in a specific nucleus */
	int        base;            /* index of first gene in a specific nucleus */
	static unsigned int samecycle = 0;            /* same cleavage cycle as before? */
	int nmrna = defs->ngenes / 2;
	if (ccycle != samecycle) {
		GetD(t, lparm->d, defs->diff_schedule, D);
		GetRCoef(t, &mit_factor);
		samecycle = ccycle;
	}
	if (rule == INTERPHASE) {
		DddwwdtFocus(v, t, vinput, n);
		for( i = 0; i < nmrna; i++ ) {
            gcdm_get_delayed(vdelayed[i], t - lparm->tau[i]);
        }
            /* next loop does the rest of the equation (R, Ds and lambdas) */
                                                 /* store result in vdot[] */
		if ( n == defs->ngenes ) {       /* first part: one nuc, no diffusion */
			register double vdot1;
			for( base = 0; base < n; base += defs->ngenes ) {
				for( i = base; i < base + nmrna; i++ ) {
					k = i - base;
					vdot1 = -lparm->lambda[k] * v[i];
					vdot1 += lparm->R[k] * vdelayed[k][i + nmrna];
					vdot[i] = vdot1;
				}
				for( i = base + nmrna; i < base + defs->ngenes; i++ ) {
					k = i - base;
					vdot1 = -lparm->lambda[k] * v[i];
					vdot1 += lparm->R[k] * vinput[i];
					vdot[i] = vdot1;
				}
			}
/* It works only for more than 3 nuclei */
		} else {                    /* then for multiple nuclei -> diffusion */
			register double vdot1;
			for( i = 0; i <  nmrna; i++ ) {     /* first anterior-most nucleus */
				k = i;
				vdot[i] = dirichlet_boundary_coef[k] * vext[defs->egenes - nmrna + k];
			}
			for( i = nmrna; i < defs->ngenes; i++ ) {     /* first anterior-most nucleus */
				k = i;
				vdot1 = -lparm->lambda[k] * v[i];
				vdot1 += lparm->R[k] * vinput[i];
				vdot1 += D[k] * (v[i + defs->ngenes] - v[i]) * boundary;
				vdot[i] = vdot1;
			}
/* then middle nuclei */
			for(base = defs->ngenes; base < n - defs->ngenes; base += defs->ngenes) {
				for( i = base; i < base + nmrna; i++) {
					k = i - base;
					vdot1 = -lparm->lambda[k] * v[i];
	  				vdot1 += lparm->R[k] * vdelayed[k][i + nmrna];
  					vdot1 += D[k] * ((v[i - defs->ngenes] - v[i]) + (v[i + defs->ngenes] - v[i]) );
	  				vdot[i] = vdot1;
				}
				for( i = base + nmrna; i < base + defs->ngenes; i++) {
					k = i - base;
					vdot1 = -lparm->lambda[k] * v[i];
	  				vdot1 += lparm->R[k] * vinput[i];
  					vdot1 += D[k] * ((v[i - defs->ngenes] - v[i]) + (v[i + defs->ngenes] - v[i]) );
	  				vdot[i] = vdot1;
				}
			}
/* last: posterior-most nucleus */
			base = n - defs->ngenes;
			for( i = base; i < base + nmrna; i++) {
				k = i - base;
				vdot[i] = dirichlet_boundary_coef[k] * vext[num_nucs * defs->egenes - nmrna + k];
			}
			for( i = base + nmrna; i < base + defs->ngenes; i++) {
				k = i - base;
				vdot1 = -lparm->lambda[k] * v[i];
				vdot1 += lparm->R[k] * vinput[i];
				vdot1 += D[k] * (v[i - defs->ngenes] - v[i]) * boundary;
				vdot[i] = vdot1;
			}
		}
/* during mitosis only diffusion and decay happen */
	} else if (rule == MITOSIS) {
		if ( n == defs->ngenes ) {       /* first part: one nuc, no diffusion */
			register double vdot1;
			for( base = 0; base < n; base += defs->ngenes ) {
				for( i = base; i < base + nmrna; i++ ) {
					k = i - base;
					vdot1 = -lparm->lambda[k] * v[i];
					vdot[i] = vdot1;
				}
				for( i = base + nmrna; i < base + defs->ngenes; i++ ) {
					k = i - base;
					vdot1 = -lparm->lambda[k] * v[i];
					vdot[i] = vdot1;
				}
			}
/* It works only for more than 3 nuclei */
		} else {                    /* then for multiple nuclei -> diffusion */
			register double vdot1;
			for( i = 0; i <  nmrna; i++ ) {     /* first anterior-most nucleus */
				k = i;
				vdot[i] = dirichlet_boundary_coef[k] * vext[defs->egenes - nmrna + k];
			}
			for( i = nmrna; i < defs->ngenes; i++ ) {     /* first anterior-most nucleus */
				k = i;
				vdot1 = -lparm->lambda[k] * v[i];
				vdot1 += D[k] * (v[i + defs->ngenes] - v[i]) * boundary;
				vdot[i] = vdot1;
			}
/* then middle nuclei */
			for(base = defs->ngenes; base < n - defs->ngenes; base += defs->ngenes) {
				for( i = base; i < base + nmrna; i++) {
					k = i - base;
					vdot1 = -lparm->lambda[k] * v[i];
  					vdot1 += D[k] * ((v[i - defs->ngenes] - v[i]) + (v[i + defs->ngenes] - v[i]) );
	  				vdot[i] = vdot1;
				}
				for( i = base + nmrna; i < base + defs->ngenes; i++) {
					k = i - base;
					vdot1 = -lparm->lambda[k] * v[i];
  					vdot1 += D[k] * ((v[i - defs->ngenes] - v[i]) + (v[i + defs->ngenes] - v[i]) );
	  				vdot[i] = vdot1;
				}
			}
/* last: posterior-most nucleus */
			base = n - defs->ngenes;
			for( i = base; i < base + nmrna; i++) {
				k = i - base;
				vdot[i] = dirichlet_boundary_coef[k] * vext[num_nucs * defs->egenes - nmrna + k];
			}
			for( i = base + nmrna; i < base + defs->ngenes; i++) {
				k = i - base;
				vdot1 = -lparm->lambda[k] * v[i];
				vdot1 += D[k] * (v[i - defs->ngenes] - v[i]) * boundary;
				vdot[i] = vdot1;
			}
		}
	} else
		error("DddwwdtOrig: Bad rule %i sent to DvwdtOrig", rule);
}


void DddwwdtGuts(double *v, double t, double *vdot, int n)
{
	int        ap;                 /* nuclear index on AP axis [0,1,...,m-1] */
	int        i, j;                                   /* local loop counters */
	int        k;                   /* index of gene k in a specific nucleus */
	int        base;            /* index of first gene in a specific nucleus */
	static unsigned int samecycle = 0;            /* same cleavage cycle as before? */
/* get D parameters and bicoid gradient according to cleavage cycle */
/* previously it depended on num_nucs*/
	int nmrna = defs->ngenes / 2;
	if (ccycle != samecycle) {
		GetD(t, lparm->d, defs->diff_schedule, D);
		GetRCoef(t, &mit_factor);
		vmat = gcmd_get_maternal_inputs(ccycle);
		samecycle = ccycle;
	}
	gcmd_get_external_inputs(t, vext, num_nucs * defs->egenes);
/* This is how it works (by JR):
    ap      nucleus position on ap axis
    base    index of first gene (0) in current nucleus
    k       index of gene k in current nucleus
            Protein synthesis terms are calculated according to g(u)
           First we do loop for vinput contributions; vinput contains
   the u that goes into g(u)
   Then we do a separate loop or vector func for sqrt or exp
   Then we do vdot contributions (R and lambda part)
   Then we do the spatial part (Ds); we do a special case for
            each end
            Note, however, that before the real loop, we have to do a
            special case for when rhere is one nuc, hence no diffusion
    These loops look a little funky 'cause we don't want any
            divides'                                                       */
	if (rule == INTERPHASE) {
		register double vinput1 = 0;
		if ( defs->mob_schedule == 'B' ) {
			for(base = 0, ap = 0; base < n; base += defs->ngenes, ap++) {
				for( i = base; i < base + nmrna; i++) {
					int g;
					k = i - base;
					g = 0;
					for (j = 0; j < nmrna; j++) {
						input_He[g] = vext[ap * defs->egenes + defs->egenes - nmrna + j];
						params_He[g] = lparm->T[(k*defs->ngenes)+j];
						g++;
					}
					for (j = 0; j < defs->egenes - nmrna; j++) {
						input_He[g] = vext[ap * defs->egenes + j];
						params_He[g] = lparm->E[(k*defs->egenes)+j];
						g++;
					}
					params_He[g] = lparm->h[k]; /* basal_txp , n*/
					g++;
					params_He[g] = lparm->m[k]; /* short range repression treshold , n + 1*/
					g++;
					params_He[g] = lparm->mm[k]; /* max contact for all, n + 2*/
					g++;
					for (j = 0; j < 2 * defs->egenes; j++) {
						params_He[g] = lparm->P[j]; /* max bindings,  cooperativity*/
						g++;
					}
					vinput[i + nmrna] = expression( input_He, params_He, k, &(zOn[i + nmrna]), &(zOff[i + nmrna]));
					if ( debug ) {
						fprintf(stdout, "Time = %f\tSeq = %d\tZ_off = %f\tZ_on = %f\tefficiency(Z_on / Z_off) = %f\tpromoterOcc(E) = %f\n", t, k, zOff[i + nmrna], zOn[i + nmrna], zOn[i + nmrna]/zOff[i + nmrna], vinput[i + nmrna]);
					}
				}
			}
		} else if ( defs->mob_schedule == 'D' ) {
			for(base = 0, ap = 0; base < n; base += defs->ngenes, ap++) {
				for( i = base; i < base + nmrna; i++) {
					int g;
					k = i - base;
					g = 0;
					for (j = 0; j < nmrna; j++) {
						input_He[g] = vext[ap * defs->egenes + defs->egenes - nmrna + j];
						params_He[g] = lparm->T[j];
						g++;
					}
					params_He[k] = lparm->T[k + nmrna];
					for (j = 0; j < defs->egenes - nmrna; j++) {
						input_He[g] = vext[ap * defs->egenes + j];
						params_He[g] = lparm->E[j];
						g++;
					}
					params_He[g] = lparm->h[k]; /* basal_txp , n*/
					g++;
					params_He[g] = lparm->m[k]; /* short range repression treshold , n + 1*/
					g++;
					params_He[g] = lparm->mm[k]; /* max contact for all, n + 2*/
					g++;
					for (j = 0; j < 2 * defs->egenes; j++) {
						params_He[g] = lparm->P[j]; /* max bindings,  cooperativity*/
						g++;
					}
					vinput[i + nmrna] = expression( input_He, params_He, k, &(zOn[i + nmrna]), &(zOff[i + nmrna]));
					if ( debug ) {
						fprintf(stdout, "Time = %f\tSeq = %d\tZ_off = %f\tZ_on = %f\tefficiency(Z_on / Z_off) = %f\tpromoterOcc(E) = %f\n", t, k, zOff[i + nmrna], zOn[i + nmrna], zOn[i + nmrna]/zOff[i + nmrna], vinput[i + nmrna]);
					}
				}
			}
		}
/* Guts can't handle this
		for( i = 0; i < nmrna; i++ ) {
            gcdm_get_delayed(vdelayed[i], t - lparm->tau[i]);
        }
*/
/* next loop does the rest of the equation (R, Ds and lambdas) */
                                                 /* store result in vdot[] */
		if ( n == defs->ngenes ) {       /* first part: one nuc, no diffusion */
			register double vdot1;
			for( base = 0; base < n; base += defs->ngenes ) {
				for( i = base; i < base + nmrna; i++ ) {
					k = i - base;
					vdot1 = -lparm->lambda[k] * v[i];
					vdot1 += lparm->R[k] * vdelayed[k][i + nmrna];
					vdot[i] = vdot1;
				}
				for( i = base + nmrna; i < base + defs->ngenes; i++ ) {
					k = i - base;
					vdot1 = -lparm->lambda[k] * v[i];
					vdot1 += lparm->R[k] * vinput[i];
					vdot[i] = vdot1;
				}
			}
/* It works only for more than 3 nuclei */
		} else {                    /* then for multiple nuclei -> diffusion */
			register double vdot1;
			for( i = 0; i <  nmrna; i++ ) {     /* first anterior-most nucleus */
				k = i;
				vdot[i] = vext[defs->egenes - nmrna + k];
			}
			for( i = nmrna; i < defs->ngenes; i++ ) {     /* first anterior-most nucleus */
				k = i;
				vdot1 = -lparm->lambda[k] * v[i];
				vdot1 += lparm->R[k] * vinput[i];
				vdot1 += D[k] * (v[i + defs->ngenes] - v[i]) * boundary;
				vdot[i] = vdot1;
			}
/* then middle nuclei */
			for(base = defs->ngenes; base < n - defs->ngenes; base += defs->ngenes) {
				for( i = base; i < base + nmrna; i++) {
					k = i - base;
					vdot1 = -lparm->lambda[k] * v[i];
	  				vdot1 += lparm->R[k] * vdelayed[k][i + nmrna];
  					vdot1 += D[k] * ((v[i - defs->ngenes] - v[i]) + (v[i + defs->ngenes] - v[i]) );
	  				vdot[i] = vdot1;
				}
				for( i = base + nmrna; i < base + defs->ngenes; i++) {
					k = i - base;
					vdot1 = -lparm->lambda[k] * v[i];
	  				vdot1 += lparm->R[k] * vinput[i];
  					vdot1 += D[k] * ((v[i - defs->ngenes] - v[i]) + (v[i + defs->ngenes] - v[i]) );
	  				vdot[i] = vdot1;
				}
			}
/* last: posterior-most nucleus */
			base = n - defs->ngenes;
			for( i = base; i < base + nmrna; i++) {
				k = i - base;
				vdot[i] = vext[num_nucs * defs->egenes - nmrna + k];
			}
			for( i = base + nmrna; i < base + defs->ngenes; i++) {
				k = i - base;
				vdot1 = -lparm->lambda[k] * v[i];
				vdot1 += lparm->R[k] * vinput[i];
				vdot1 += D[k] * (v[i - defs->ngenes] - v[i]) * boundary;
				vdot[i] = vdot1;
			}
		}
/* during mitosis only diffusion and decay happen */
	} else if (rule == MITOSIS) {
		if ( n == defs->ngenes ) {       /* first part: one nuc, no diffusion */
			register double vdot1;
			for( base = 0; base < n; base += defs->ngenes ) {
				for( i = base; i < base + nmrna; i++ ) {
					k = i - base;
					vdot1 = -lparm->lambda[k] * v[i];
					vdot[i] = vdot1;
				}
				for( i = base + nmrna; i < base + defs->ngenes; i++ ) {
					k = i - base;
					vdot1 = -lparm->lambda[k] * v[i];
					vdot[i] = vdot1;
				}
			}
/* It works only for more than 3 nuclei */
		} else {                    /* then for multiple nuclei -> diffusion */
			register double vdot1;
			for( i = 0; i <  nmrna; i++ ) {     /* first anterior-most nucleus */
				k = i;
				vdot[i] = vext[defs->egenes - nmrna + k];
			}
			for( i = nmrna; i < defs->ngenes; i++ ) {     /* first anterior-most nucleus */
				k = i;
				vdot1 = -lparm->lambda[k] * v[i];
				vdot1 += D[k] * (v[i + defs->ngenes] - v[i]) * boundary;
				vdot[i] = vdot1;
			}
/* then middle nuclei */
			for(base = defs->ngenes; base < n - defs->ngenes; base += defs->ngenes) {
				for( i = base; i < base + nmrna; i++) {
					k = i - base;
					vdot1 = -lparm->lambda[k] * v[i];
  					vdot1 += D[k] * ((v[i - defs->ngenes] - v[i]) + (v[i + defs->ngenes] - v[i]) );
	  				vdot[i] = vdot1;
				}
				for( i = base + nmrna; i < base + defs->ngenes; i++) {
					k = i - base;
					vdot1 = -lparm->lambda[k] * v[i];
  					vdot1 += D[k] * ((v[i - defs->ngenes] - v[i]) + (v[i + defs->ngenes] - v[i]) );
	  				vdot[i] = vdot1;
				}
			}
/* last: posterior-most nucleus */
			base = n - defs->ngenes;
			for( i = base; i < base + nmrna; i++) {
				k = i - base;
				vdot[i] = vext[num_nucs * defs->egenes - nmrna + k];
			}
			for( i = base + nmrna; i < base + defs->ngenes; i++) {
				k = i - base;
				vdot1 = -lparm->lambda[k] * v[i];
				vdot1 += D[k] * (v[i - defs->ngenes] - v[i]) * boundary;
				vdot[i] = vdot1;
			}
		}
	} else
		error("DddwwdtGuts: Bad rule %i sent to DvwdtOrig", rule);
}
#endif

/*** DvdtOrig: the derivative function; implements the equations **
*******************************************************************/
void Dvdt2Orig(double *v, double *w, double t, double *vdot, int n)
{
	int        ap;                 /* nuclear index on AP axis [0,1,...,m-1] */
	int        i,j;                                   /* local loop counters */
	int        k;                   /* index of gene k in a specific nucleus */
	int        base;            /* index of first gene in a specific nucleus */
	static unsigned int samecycle = 0;            /* same cleavage cycle as before? */
/* get D parameters and bicoid gradient according to cleavage cycle */
/* previously it depended on num_nucs*/
	if (ccycle != samecycle) {
		GetD(t, lparm->d, defs->diff_schedule, D);
		GetM(t, lparm->m, defs->mob_schedule, M);
		GetMM(t, lparm->mm, defs->mob_schedule, MM);
		GetRCoef(t, &mit_factor);
		vmat = gcmd_get_maternal_inputs(ccycle);
		samecycle = ccycle;
	}
	if ( defs->egenes )
		gcmd_get_external_inputs(t, vext, num_nucs * defs->egenes);
	if ( defs->ncouples )
		gcmd_get_coupled_inputs(t, vcouple, wcouple, num_nucs * defs->ncouples, v, w, n, vext, num_nucs * defs->egenes, vmat, num_nucs * defs->mgenes);
/* This is how it works (by JR):
    ap      nucleus position on ap axis
    base    index of first gene (0) in current nucleus
    k       index of gene k in current nucleus
            Protein synthesis terms are calculated according to g(u)
           First we do loop for vinput contributions; vinput contains
   the u that goes into g(u)
   Then we do a separate loop or vector func for sqrt or exp
   Then we do vdot contributions (R and lambda part)
   Then we do the spatial part (Ds); we do a special case for
            each end
            Note, however, that before the real loop, we have to do a
            special case for when rhere is one nuc, hence no diffusion
    These loops look a little funky 'cause we don't want any
            divides'                                                       */
	if (rule == INTERPHASE) {
		register double vinput1 = 0;
		register double vinput4 = 0;
		for(base = 0, ap=0; base < n ; base += defs->ngenes, ap++) {
			for(i=base; i < base + defs->ngenes; i++) {
				k = i - base;
				vinput1 = lparm->h[k];
				vinput4 = 0;
				for(j=0; j < defs->mgenes; j++) {
					vinput1  += lparm->M[(k*defs->mgenes)+j] * vmat[ap * defs->mgenes + j];
				}
				for(j=0; j < defs->ngenes; j++) {
					vinput1 += lparm->T[(k*defs->ngenes)+j] * v[base + j];
					vinput4 += lparm->T[(k*defs->ngenes)+j] * w[base + j];
				}
				for(j=0; j < defs->egenes; j++) {
					vinput1  += lparm->E[(k*defs->egenes)+j] * vext[ap * defs->egenes + j];
				}
				for(j=0; j < defs->ncouples; j++) {
					vinput1 += lparm->P[(k*defs->ncouples)+j] * vcouple[ap * defs->ncouples + j];
					vinput4 += lparm->P[(k*defs->ncouples)+j] * wcouple[ap * defs->ncouples + j];
				}
				bot2[i]   = 1 + vinput1 * vinput1;
				vinput[i] = vinput1;
				vinput2[i] = vinput4;
			}
		}
/***************************************************************************
 *                                                                         *
 *        g(u) = 1/2 * ( u / sqrt(1 + u^2) + 1)                            *
 *                                                                         *
 ***************************************************************************/
		if ( gofu == Sqrt ) {
                            /* now calculate sqrt(1+u2); store it in bot[] */
#ifdef MKL
			vdInvSqrt(n, bot2, bot);
#else
			for(i=0; i < n; i++)                  /* slow traditional style sqrt */
				bot[i] = 1 / sqrt(bot2[i]);
#endif
			for(i=0; i < n; i++) {
				vinput1 = bot[i];
				bot[i]= vinput[i] * vinput1+1;
				bot3[i] = vinput1 * vinput1 * vinput1;
			}
/***************************************************************************
 *                                                                         *
 *        g(u) = 1/2 * (tanh(u) + 1) )                                     *
 *                                                                         *
 ***************************************************************************/
		} else if ( gofu == Tanh ) {
#ifdef MKL
			vdTanh(n, vinput, bot);
			for(i=0; i < n; i++) {
				bot[i] += 1;
 			}
#else
			for(i=0; i < n; i++) {
				bot[i]= tanh(vinput[i])+1;
 			}
#endif
/***************************************************************************
 *                                                                         *
 *        g(u) = 1 / (1 + exp(-2u))                                        *
 *                                                                         *
 ***************************************************************************/
		} else if ( gofu == Exp ) {
                              /* now calculate exp(-u); store it in bot[] */
#ifdef MKL
			vdExp(n, vinput, bot);
#else
			for (i=0; i<n; i++)                    /* slow traditional style exp */
				bot[i] = exp(vinput[i]);
#endif
			for(i=0; i < n; i++) {
				vinput1 = bot[i];
				bot[i]= vinput1*vinput1/(vinput1*vinput1+1);
			}
		} else
			error("DvdtOrig: unknown g(u)");
            /* next loop does the rest of the equation (R, Ds and lambdas) */
                                                 /* store result in vdot[] */
		if ( n == defs->ngenes ) {       /* first part: one nuc, no diffusion */
			register double vdot1, g1;
			for( base=0; base<n; base+=defs->ngenes ) {
				for( i=base; i<base+defs->ngenes; i++ ) {
					k = i - base;
					vdot1 = -lparm->lambda[k] * v[i];
					g1 = bot[i];
					vdot1  += lparm->R[k] * 0.5 * g1 * mit_factor;
					vdot[i] = vdot1;
				}
			}
/* It works only for more than 3 nuclei */
		} else {                    /* then for multiple nuclei -> diffusion */
			register double vdot1,g1;
			for(i = 0; i < defs->ngenes; i++) {     /* first anterior-most nucleus */
				k = i;
				vdot1 = -lparm->lambda[k] * v[i] - w[i];
				g1 = bot[i];
				vdot1 += lparm->R[k] * 0.5 * g1  * mit_factor;
				vdot1 += D[k] * (v[i + defs->ngenes] - v[i]) * boundary;
				vdot1 -= M[k] * ( v[i] - 4 * v[i + defs->ngenes] + 6 * v[i + 2 * defs->ngenes] - 4 * v[i + 3 * defs->ngenes] +  v[i + 4 * defs->ngenes] );
				vdot1 /= lparm->tau[k];
				vdot1 += -lparm->lambda[k] * w[i];
				g1 = bot3[i];
				vdot1 += lparm->R[k] * 0.5 * g1  * mit_factor * vinput2[i];
				vdot[i] = vdot1;
			}
			for(i = defs->ngenes; i < 2 * defs->ngenes; i++) {     /* first anterior-most nucleus */
				k = i - defs->ngenes;
				vdot1 = -lparm->lambda[k] * v[i] - w[i];
				g1 = bot[i];
				vdot1 += lparm->R[k] * 0.5 * g1  * mit_factor;
				vdot1 += D[k] * ((v[i - defs->ngenes] - v[i]) + (v[i + defs->ngenes] - v[i]) );
				vdot1 -= M[k] * ( v[i - defs->ngenes] - 4 * v[i] + 6 * v[i + defs->ngenes] - 4 * v[i + 2 * defs->ngenes] +  v[i + 3 * defs->ngenes] );
				vdot1 /= lparm->tau[k];
				vdot1 += -lparm->lambda[k] * w[i];
				g1 = bot3[i];
				vdot1 += lparm->R[k] * 0.5 * g1  * mit_factor * vinput2[i];
				vdot[i] = vdot1;
			}
/* then middle nuclei */
			for(base = 2 * defs->ngenes; base < n - 2 * defs->ngenes; base += defs->ngenes) {
				for(i = base; i < base + defs->ngenes; i++) {
					k = i - base;
					vdot1 = -lparm->lambda[k] * v[i] - w[i];
					g1 = bot[i];
					vdot1 += lparm->R[k] * 0.5 * g1  * mit_factor;
					vdot1 += D[k] * ((v[i - defs->ngenes] - v[i]) + (v[i + defs->ngenes] - v[i]) );
					vdot1 -= M[k] * ( v[i - 2 * defs->ngenes] - 4 * v[i - defs->ngenes] + 6 * v[i] - 4 * v[i + defs->ngenes] +  v[i + 2 * defs->ngenes] );
					vdot1 /= lparm->tau[k];
					vdot1 += -lparm->lambda[k] * w[i];
					g1 = bot3[i];
	  				vdot1 += lparm->R[k] * 0.5 * g1  * mit_factor * vinput2[i];
					vdot[i] = vdot1;
				}
			}
			base = n - 2 * defs->ngenes;
			for(i = base; i < base + defs->ngenes; i++) {
				k = i - base;
				vdot1 = -lparm->lambda[k] * v[i] - w[i];
				g1 = bot[i];
				vdot1 += lparm->R[k] * 0.5 * g1  * mit_factor;
				vdot1 += D[k] * ((v[i - defs->ngenes] - v[i]) + (v[i + defs->ngenes] - v[i]) );
				vdot1 -= M[k] * ( v[i - 3 * defs->ngenes] - 4 * v[i - 2 * defs->ngenes] + 6 * v[i - defs->ngenes] - 4 * v[i] + v[i + defs->ngenes] );
				vdot1 /= lparm->tau[k];
				vdot1 += -lparm->lambda[k] * w[i];
				g1 = bot3[i];
	  			vdot1 += lparm->R[k] * 0.5 * g1  * mit_factor * vinput2[i];
				vdot[i] = vdot1;
			}
/* last: posterior-most nucleus */
			base = n - defs->ngenes;
			for(i = base; i < base + defs->ngenes; i++) {
				k = i - base;
				vdot1 = -lparm->lambda[k] * v[i] - w[i];
				g1 = bot[i];
				vdot1 += lparm->R[k] * 0.5 * g1  * mit_factor;
				vdot1 += D[k] * (v[i - defs->ngenes] - v[i]);
				vdot1 -= M[k] * ( v[i - 4 * defs->ngenes] - 4 * v[i - 3 * defs->ngenes] + 6 * v[i - 2 * defs->ngenes] - 4 * v[i - defs->ngenes] +  v[i] );
				vdot1 /= lparm->tau[k];
				vdot1 += -lparm->lambda[k] * w[i];
				g1 = bot3[i];
	  			vdot1 += lparm->R[k] * 0.5 * g1  * mit_factor * vinput2[i];
				vdot[i] = vdot1;
			}
		}
/* during mitosis only diffusion and decay happen */
	} else if (rule == MITOSIS) {
		if ( n == defs->ngenes ) {         /* first part: one nuc, no diffusion */
			register double vdot1;
			for( base=0; base<n; base+=defs->ngenes ) {
				for( i=base; i<base+defs->ngenes; i++ ) {
					k = i - base;
					vdot1 = -lparm->lambda[k] * v[i];
					vdot[i] = vdot1;
				}
			}
		} else {                      /* then for multiple nuclei -> diffusion */
			register double vdot1,g1;
			for(i = 0; i < defs->ngenes; i++) {     /* first anterior-most nucleus */
				k = i;
				vdot1 = -lparm->lambda[k] * v[i] - w[i];
				vdot1 += D[k] * (v[i + defs->ngenes] - v[i]) * boundary;
				vdot1 -= MM[k] * ( v[i] - 4 * v[i + defs->ngenes] + 6 * v[i + 2 * defs->ngenes] - 4 * v[i + 3 * defs->ngenes] +  v[i + 4 * defs->ngenes] );
				vdot1 /= lparm->tau[k];
				vdot1 += -lparm->lambda[k] * w[i];
				vdot[i] = vdot1;
			}
			for(i = defs->ngenes; i < 2 * defs->ngenes; i++) {     /* first anterior-most nucleus */
				k = i - defs->ngenes;
				vdot1 = -lparm->lambda[k] * v[i] - w[i];
				vdot1 += D[k] * ((v[i - defs->ngenes] - v[i]) + (v[i + defs->ngenes] - v[i]) );
				vdot1 -= MM[k] * ( v[i - defs->ngenes] - 4 * v[i] + 6 * v[i + defs->ngenes] - 4 * v[i + 2 * defs->ngenes] +  v[i + 3 * defs->ngenes] );
				vdot1 /= lparm->tau[k];
				vdot1 += -lparm->lambda[k] * w[i];
				vdot[i] = vdot1;
			}
/* then middle nuclei */
			for(base = 2 * defs->ngenes; base < n - 2 * defs->ngenes; base += defs->ngenes) {
				for(i = base; i < base + defs->ngenes; i++) {
					k = i - base;
					vdot1 = -lparm->lambda[k] * v[i] - w[i];
					vdot1 += D[k] * ((v[i - defs->ngenes] - v[i]) + (v[i + defs->ngenes] - v[i]) );
					vdot1 -= MM[k] * ( v[i - 2 * defs->ngenes] - 4 * v[i - defs->ngenes] + 6 * v[i] - 4 * v[i + defs->ngenes] +  v[i + 2 * defs->ngenes] );
					vdot1 /= lparm->tau[k];
					vdot1 += -lparm->lambda[k] * w[i];
					vdot[i] = vdot1;
				}
			}
			base = n - 2 * defs->ngenes;
			for(i = base; i < base + defs->ngenes; i++) {
				k = i - base;
				vdot1 = -lparm->lambda[k] * v[i] - w[i];
				vdot1 += D[k] * ((v[i - defs->ngenes] - v[i]) + (v[i + defs->ngenes] - v[i]) );
				vdot1 -= MM[k] * ( v[i - 3 * defs->ngenes] - 4 * v[i - 2 * defs->ngenes] + 6 * v[i - defs->ngenes] - 4 * v[i] + v[i + defs->ngenes] );
				vdot1 /= lparm->tau[k];
				vdot1 += -lparm->lambda[k] * w[i];
				vdot[i] = vdot1;
			}
/* last: posterior-most nucleus */
			base = n - defs->ngenes;
			for(i = base; i < base + defs->ngenes; i++) {
				k = i - base;
				vdot1 = -lparm->lambda[k] * v[i] - w[i];
				vdot1 += D[k] * (v[i - defs->ngenes] - v[i]);
				vdot1 -= MM[k] * ( v[i - 4 * defs->ngenes] - 4 * v[i - 3 * defs->ngenes] + 6 * v[i - 2 * defs->ngenes] - 4 * v[i - defs->ngenes] +  v[i] );
				vdot1 /= lparm->tau[k];
				vdot1 += -lparm->lambda[k] * w[i];
				vdot[i] = vdot1;
			}
		}
	} else
		error("Dvdt2Orig: Bad rule %i sent to DvdtOrig", rule);
}

/***                       Dvdt31stTelDirichlet                          ***
 *                                                                         *
 *      uses explicit Finite-Difference Method of 1st order accuracy       *
 *         for the telegraph equation, calculates next time step           *
 *                Dirichlet boundary conditions are used                   *
 *                                                                         *
 * *********************************************************************** *
 *                                                                         *
 * NOTATIONS :                                                             *
 * v1        <--   input meanings of concentration                         *
 * v0        <--   input meanings of concentration function gradient       *
 * vdot      <--   output meanings of concentration                        *
 * t         <--   current time moment                                     *
 * n         <--   length of v1, v0, vdot                                  *
 *                                                                         *
 *** ****************************************************************** ***/
void Dvdt31stTelDirichlet(double *v1, double *v0, double t, double *vdot, int n)
{
	static unsigned int samecycle = 0;     /* same cleavage cycle as before? */
	double vinput1, vinput3;         /* help variables */
	double theta = vdot[0];          /* time step size */
	double g;                        /* g-function value */
	double gDer;                     /* g-function derivative value */
	double eps;                      /* help cofficient */
	double alpha;                    /* help cofficient */
	double beta;                     /* help cofficient */
	double chi;                      /* help cofficient */
	double prevInpT, curInpT;        /* inputs from considered genes */
	double prevInpE, curInpE;        /* input from external genes */
	double inpM;                     /* inputs from maternal genes */
	int i, j, k, ap;                 /* local indexes */
	if (ccycle != samecycle) {
		GetD(t, lparm->d, defs->diff_schedule, D);
		GetL(t, lparm->m, defs->mob_schedule, M);
		GetLL(t, lparm->mm, defs->mob_schedule, MM);
		GetRCoef(t, &mit_factor);
		vmat = gcmd_get_maternal_inputs(ccycle);
		samecycle = ccycle;
	}
	if (rule == INTERPHASE) {
		if (defs->egenes > 0) {
			gcmd_get_external_inputs(t, vext, num_nucs * defs->egenes);
			gcdm_get_curveSlopeExtInputs(t, vext2, num_nucs * defs->egenes);
		}
		for (ap = 0; ap < num_nucs; ap++) {
			for (i = 0; i < defs->ngenes; i++) {
				chi = 1.0 / (1 + lparm->lambda[i] * lparm->tau[i]);
				eps = lparm->tau[i] * chi;
				alpha = D[i] * chi;
				beta = -lparm->lambda[i] * chi;
				prevInpT = 0.0;
				curInpT = 0.0;
				prevInpE = 0.0;
				curInpE = 0.0;
				inpM = 0.0;
				for (k = 0; k < defs->ngenes; k++) {
					curInpT += lparm->T[defs->ngenes * i + k] * v1[defs->ngenes * ap + k];
					prevInpT += lparm->T[defs->ngenes * i + k] * v0[defs->ngenes * ap + k];
				}
				for (k = 0; k < defs->egenes; k++) {
               curInpE += lparm->E[defs->egenes * i + k] * vext[defs->egenes * ap + k];
					prevInpE += lparm->E[defs->egenes * i + k] * vext2[defs->egenes * ap + k];
				}
				for (k = 0; k < defs->mgenes; k++) {
					inpM += lparm->M[defs->mgenes * i + k] * vmat[defs->mgenes * ap + k];
				}
				vinput1 = curInpT + curInpE + inpM + lparm->h[i];
				vinput3 = 1 / sqrt(1 + vinput1 * vinput1);
				if (gofu == Tanh) {
					g = lparm->R[i] / 2.0 * (1.0 + tanh(vinput1));
					gDer = lparm->R[i] / (2.0 * cosh(vinput1) * cosh(vinput1));
				} else if (gofu == Sqrt) {
					g = 0.5 * lparm->R[i] * (1.0 + vinput1 * vinput3);
					gDer = 0.5 * lparm->R[i] * vinput3 * vinput3 * vinput3;
				} else if (gofu == Lin) {
					if (vinput1 > -1 && vinput1 < 1) {
						g = lparm->R[i] * (vinput1 + 1.0) / 2.0;
						gDer = lparm->R[i] / 2.0;
					}
					else {
						gDer = 0.0;
						if (vinput1 > 1)
							g = lparm->R[i];
						else
							g = 0.0;
					}
				} else {
               error("Dvdt31stTelDirichlet: unknown g(u)");
				}
            /* calculating next time step */
            vdot[defs->ngenes * ap + i] =
               v1[defs->ngenes * ap + i] * (2 * eps / (theta * theta) - 2 * alpha + beta) +
               eps * gDer / theta * (curInpT - prevInpT) + eps * gDer * prevInpE + chi * g +
               v0[defs->ngenes * ap + i] * (-eps / (theta * theta) + 1 / (2.0 * theta));
            /* boundary conditions */
            // Dirichlet conditions
            // the far left or right nucleus
            if (ap == 0 || ap == num_nucs - 1) {
               vdot[defs->ngenes * ap + i] = vext[defs->egenes * ap + (defs->egenes - defs->ngenes) + i];
            // any middle nucleus
            } else {
               vdot[defs->ngenes * ap + i] +=
               alpha * (v1[defs->ngenes * (ap + 1) + i] + v1[defs->ngenes * (ap - 1) + i]);
               vdot[defs->ngenes * ap + i] /= eps / (theta * theta) + 1 / (2.0 * theta);
            }
         }
      }
   } else if (rule == MITOSIS) {
      for (ap = 0; ap < num_nucs; ap++) {
         for (i = 0; i < defs->ngenes; i++) {
            chi = 1.0 / (1 + lparm->lambda[i] * lparm->tau[i]);
				eps = lparm->tau[i] * chi;
				alpha = D[i] * chi;
				beta = -lparm->lambda[i] * chi;
            /* calculating next time step */
            vdot[defs->ngenes * ap + i] =
               v1[defs->ngenes * ap + i] * (2 * eps / (theta * theta) - 2 * alpha + beta) +
               v0[defs->ngenes * ap + i] * (-eps / (theta * theta) + 1 / (2.0 * theta));
            /* boundary conditions */
            // Dirichlet conditions
            // the far left or right nucleus
            if (ap == 0 || ap == num_nucs - 1) {
               vdot[defs->ngenes * ap + i] = vext[defs->egenes * ap + (defs->egenes - defs->ngenes) + i];
            // any middle nucleus
            } else {
               vdot[defs->ngenes * ap + i] +=
                  alpha * (v1[defs->ngenes * (ap + 1) + i] + v1[defs->ngenes * (ap - 1) + i]);
               vdot[defs->ngenes * ap + i] /= eps / (theta * theta) + 1 / (2.0 * theta);
            }
         }
      }
   } else {
      error("Dvdt31stTelDirichlet: Bad rule %i sent to Dvdt31stTelDirichlet", rule);
	}
}


/***                       Dvdt31stTelNeumann                            ***
 *                                                                         *
 *      uses explicit Finite-Difference Method of 1st order accuracy       *
 *         for the telegraph equation, calculates next time step           *
 *                  Neumann boundary conditions are used                   *
 *                                                                         *
 * *********************************************************************** *
 *                                                                         *
 * NOTATIONS :                                                             *
 * v1        <--   input meanings of concentration                         *
 * v0        <--   input meanings of concentration function gradient       *
 * vdot      <--   output meanings of concentration                        *
 * t         <--   current time moment                                     *
 * n         <--   length of v1, v0, vdot                                  *
 *                                                                         *
 *** ****************************************************************** ***/
void Dvdt31stTelNeumann(double *v1, double *v0, double t, double *vdot, int n)
{
	static unsigned int samecycle = 0;     /* same cleavage cycle as before? */
	double vinput1, vinput3;         /* help variables */
	double theta = vdot[0];          /* time step size */
	double g;                        /* g-function value */
	double gDer;                     /* g-function derivative value */
	double eps;                      /* help cofficient */
	double alpha;                    /* help cofficient */
	double beta;                     /* help cofficient */
	double chi;                      /* help cofficient */
	double prevInpT, curInpT;        /* inputs from considered genes */
	double prevInpE, curInpE;        /* input from external genes */
	double inpM;                     /* inputs from maternal genes */
	int i, j, k, ap;                 /* local indexes */
	if (ccycle != samecycle) {
		GetD(t, lparm->d, defs->diff_schedule, D);
		GetL(t, lparm->m, defs->mob_schedule, M);
		GetLL(t, lparm->mm, defs->mob_schedule, MM);
		GetRCoef(t, &mit_factor);
		vmat = gcmd_get_maternal_inputs(ccycle);
		samecycle = ccycle;
	}
	if (rule == INTERPHASE) {
		if (defs->egenes > 0) {
			gcmd_get_external_inputs(t, vext, num_nucs * defs->egenes);
			gcdm_get_curveSlopeExtInputs(t, vext2, num_nucs * defs->egenes);
		}
		for (ap = 0; ap < num_nucs; ap++) {
			for (i = 0; i < defs->ngenes; i++) {
				chi = 1.0 / (1 + lparm->lambda[i] * lparm->tau[i]);
				eps = lparm->tau[i] * chi;
				alpha = D[i] * chi;
				beta = -lparm->lambda[i] * chi;
				prevInpT = 0.0;
				curInpT = 0.0;
				prevInpE = 0.0;
				curInpE = 0.0;
				inpM = 0.0;
				for (k = 0; k < defs->ngenes; k++) {
					curInpT += lparm->T[defs->ngenes * i + k] * v1[defs->ngenes * ap + k];
					prevInpT += lparm->T[defs->ngenes * i + k] * v0[defs->ngenes * ap + k];
				}
				for (k = 0; k < defs->egenes; k++) {
               curInpE += lparm->E[defs->egenes * i + k] * vext[defs->egenes * ap + k];
					prevInpE += lparm->E[defs->egenes * i + k] * vext2[defs->egenes * ap + k];
				}
				for (k = 0; k < defs->mgenes; k++) {
					inpM += lparm->M[defs->mgenes * i + k] * vmat[defs->mgenes * ap + k];
				}
				vinput1 = curInpT + curInpE + inpM + lparm->h[i];
				vinput3 = 1 / sqrt(1 + vinput1 * vinput1);
				if (gofu == Tanh) {
					g = lparm->R[i] / 2.0 * (1.0 + tanh(vinput1));
					gDer = lparm->R[i] / (2.0 * cosh(vinput1) * cosh(vinput1));
				} else if (gofu == Sqrt) {
					g = 0.5 * lparm->R[i] * (1.0 + vinput1 * vinput3);
					gDer = 0.5 * lparm->R[i] * vinput3 * vinput3 * vinput3;
				} else if (gofu == Lin) {
					if (vinput1 > -1 && vinput1 < 1) {
						g = lparm->R[i] * (vinput1 + 1.0) / 2.0;
						gDer = lparm->R[i] / 2.0;
					}
					else {
						gDer = 0.0;
						if (vinput1 > 1)
							g = lparm->R[i];
						else
							g = 0.0;
					}
				} else {
               error("Dvdt31stTelNeumann: unknown g(u)");
				}
            /* calculating next time step */
            vdot[defs->ngenes * ap + i] =
               v1[defs->ngenes * ap + i] * (2 * eps / (theta * theta) - 2 * alpha + beta) +
               eps * gDer / theta * (curInpT - prevInpT) + eps * gDer * prevInpE + chi * g +
               v0[defs->ngenes * ap + i] * (-eps / (theta * theta) + 1 / (2.0 * theta));
            /* boundary conditions */
            // Neumann conditions
            // the far left nucleus
            if (ap == 0) {
              vdot[defs->ngenes * ap + i] +=
                2 * alpha * v1[defs->ngenes * (ap + 1) + i];
            // the far right nucleus
            } else if (ap == num_nucs - 1) {
              vdot[defs->ngenes * ap + i] +=
                2 * alpha * v1[defs->ngenes * (ap - 1) + i];
            // any middle nucleus
            } else {
              vdot[defs->ngenes * ap + i] +=
                alpha * (v1[defs->ngenes * (ap + 1) + i] + v1[defs->ngenes * (ap - 1) + i]);
            }
            vdot[defs->ngenes * ap + i] /= eps / (theta * theta) + 1 / (2.0 * theta);
         }
      }
   } else if (rule == MITOSIS) {
      for (ap = 0; ap < num_nucs; ap++) {
         for (i = 0; i < defs->ngenes; i++) {
            chi = 1.0 / (1 + lparm->lambda[i] * lparm->tau[i]);
				eps = lparm->tau[i] * chi;
				alpha = D[i] * chi;
				beta = -lparm->lambda[i] * chi;
            /* calculating next time step */
            vdot[defs->ngenes * ap + i] =
               v1[defs->ngenes * ap + i] * (2 * eps / (theta * theta) - 2 * alpha + beta) +
               v0[defs->ngenes * ap + i] * (-eps / (theta * theta) + 1 / (2.0 * theta));
            /* boundary conditions */
            // Neumann conditions
            // the far left nucleus
            if (ap == 0) {
              vdot[defs->ngenes * ap + i] +=
                2 * alpha * v1[defs->ngenes * (ap + 1) + i];
            // the far right nucleus
            } else if (ap == num_nucs - 1) {
              vdot[defs->ngenes * ap + i] +=
                2 * alpha * v1[defs->ngenes * (ap - 1) + i];
            // any middle nucleus
            } else {
              vdot[defs->ngenes * ap + i] +=
                alpha * (v1[defs->ngenes * (ap + 1) + i] + v1[defs->ngenes * (ap - 1) + i]);
            }
            vdot[defs->ngenes * ap + i] /= eps / (theta * theta) + 1 / (2.0 * theta);
         }
      }
   } else {
      error("Dvdt31stTelNeumann: Bad rule %i sent to Dvdt31stTelNeumann", rule);
	}
}


/***                       Dvdt32ndTelNeumann                            ***
 *                                                                         *
 *      uses explicit Finite-Difference Method of 2nd order accuracy       *
 *         for the telegraph equation, calculates next time step           *
 *                  Neumann boundary conditions are used                   *
 *                                                                         *
 * *********************************************************************** *
 *                                                                         *
 * NOTATIONS :                                                             *
 * v1        <--   input meanings of concentration                         *
 * v0        <--   input meanings of concentration function gradient       *
 * vdot      <--   output meanings of concentration                        *
 * t         <--   current time moment                                     *
 * n         <--   length of v1, v0, vdot                                  *
 *                                                                         *
 *** ****************************************************************** ***/
void Dvdt32ndTelNeumann(double *v1, double *v0, double t, double *vdot, int n)
{
	static unsigned int samecycle = 0;     /* same cleavage cycle as before? */
	double vinput1, vinput3;         /* help variables */
	double theta = vdot[0];          /* time step size */
	double g;                        /* g-function value */
	double gDer;                     /* g-function derivative value */
	double eps;                      /* help cofficient */
	double alpha;                    /* help cofficient */
	double beta;                     /* help cofficient */
	double chi;                      /* help cofficient */
	double prevInpT, curInpT;        /* inputs from considered genes */
	double prevInpE, curInpE;        /* input from external genes */
	double inpM;                     /* inputs from maternal genes */
	int i, j, k, ap;                 /* local indexes */
	if (ccycle != samecycle) {
		GetD(t, lparm->d, defs->diff_schedule, D);
		GetL(t, lparm->m, defs->mob_schedule, M);
		GetLL(t, lparm->mm, defs->mob_schedule, MM);
		GetRCoef(t, &mit_factor);
		vmat = gcmd_get_maternal_inputs(ccycle);
		samecycle = ccycle;
	}
	if (rule == INTERPHASE) {
		if (defs->egenes > 0) {
			gcmd_get_external_inputs(t, vext, num_nucs * defs->egenes);
			gcdm_get_curveSlopeExtInputs(t, vext2, num_nucs * defs->egenes);
		}
		for (ap = 0; ap < num_nucs; ap++) {
			for (i = 0; i < defs->ngenes; i++) {
				chi = 1.0 / (1 + lparm->lambda[i] * lparm->tau[i]);
				eps = lparm->tau[i] * chi;
				alpha = D[i] * chi;
				beta = -lparm->lambda[i] * chi;
				prevInpT = 0.0;
				curInpT = 0.0;
				prevInpE = 0.0;
				curInpE = 0.0;
				inpM = 0.0;
				for (k = 0; k < defs->ngenes; k++) {
					curInpT += lparm->T[defs->ngenes * i + k] * v1[defs->ngenes * ap + k];
					prevInpT += lparm->T[defs->ngenes * i + k] * v0[defs->ngenes * ap + k];
				}
				for (k = 0; k < defs->egenes; k++) {
					curInpE += lparm->E[defs->egenes * i + k] * vext[defs->egenes * ap + k];
					prevInpE += lparm->E[defs->egenes * i + k] * vext2[defs->egenes * ap + k];
				}
				for (k = 0; k < defs->mgenes; k++) {
					inpM += lparm->M[defs->mgenes * i + k] * vmat[defs->mgenes * ap + k];
				}
				vinput1 = curInpT + curInpE + inpM + lparm->h[i];
				vinput3 = 1 / sqrt(1 + vinput1 * vinput1);
				if (gofu == Tanh) {
					g = lparm->R[i] / 2.0 * (1.0 + tanh(vinput1));
					gDer = lparm->R[i] / (2.0 * cosh(vinput1) * cosh(vinput1));
				} else if (gofu == Sqrt) {
					g = 0.5 * lparm->R[i] * ( 1.0 + vinput1 * vinput3 );
					gDer = 0.5 * lparm->R[i] * vinput3 * vinput3 * vinput3;
				} else if (gofu == Lin) {
          if (vinput1 > -1 && vinput1 < 1) {
            g = lparm->R[i] * (vinput1 + 1.0) / 2.0;
            gDer = lparm->R[i] / 2.0;
          }
          else {
            gDer = 0.0;
            if (vinput1 > 1)
              g = lparm->R[i];
            else
              g = 0.0;
          }
        } else {
					error("Dvdt32ndTelNeumann: unknown g(u)");
				}
	      /* filling j-string of extended matrix of SLAE */
				for (j = 0; j < defs->ngenes; j++) {
					vinput[(defs->ngenes + 1) * i + j] = -eps / (2.0 * theta) * gDer * lparm->T[defs->ngenes * i + j];
				}
				vinput[(defs->ngenes + 1) * i + i] += eps / (theta * theta) + 1 / (2.0 * theta);
				vinput[(defs->ngenes + 1) * i + defs->ngenes] =
          v1[defs->ngenes * ap + i] * (2 * eps / (theta * theta) - 2 * alpha + beta) +
          chi * g +
          v0[defs->ngenes * ap + i] * (-eps / (theta * theta) + 1 / (2.0 * theta)) -
          eps * gDer / (2.0 * theta) * prevInpT + eps * gDer * prevInpE;
				if (ap == 0) {
					vinput[(defs->ngenes + 1) * i + defs->ngenes] +=
            2 * alpha * v1[defs->ngenes * (ap + 1) + i];
				} else if (ap == num_nucs - 1) {
					vinput[(defs->ngenes + 1) * i + defs->ngenes] +=
            2 * alpha * v1[defs->ngenes * (ap - 1) + i];
				} else {
					vinput[(defs->ngenes + 1) * i + defs->ngenes] +=
            alpha * (v1[defs->ngenes * (ap + 1) + i] + v1[defs->ngenes * (ap - 1) + i]);
				}
			}
      /* Gauss method, forward trace */
			for (i = 0; i < defs->ngenes - 1; i++) {
				for (k = i + 1; k < defs->ngenes; k++) {
					vinput1 = vinput[(defs->ngenes + 1) * k + i];
					for (j = i; j < defs->ngenes + 1; j++) {
						vinput[(defs->ngenes + 1) * k + j] -=
              vinput[(defs->ngenes + 1) * i + j] * vinput1 / vinput[(defs->ngenes + 1) * i + i];
					}
				}
      }
      /* Gauss method, back trace */
			for (i = defs->ngenes - 1; i >= 0; i--) {
				vinput1 = 0.0;
				for (j = defs->ngenes - 1; j > i; j--) {
					vinput1 += vinput[(defs->ngenes + 1) * i + j] * vdot[defs->ngenes * ap + j];
				}
				vdot[defs->ngenes * ap + i] =
          (vinput[(defs->ngenes + 1) * i + defs->ngenes] - vinput1) / vinput[(defs->ngenes + 1) * i + i];
			}
		}
	} else if (rule == MITOSIS) {
		for (ap = 0; ap < num_nucs; ap++) {
			for (i = 0; i < defs->ngenes; i++) {
				chi = 1.0 / (1 + lparm->lambda[i] * lparm->tau[i]);
				eps = lparm->tau[i] * chi;
				alpha = D[i] * chi;
				beta = -lparm->lambda[i] * chi;
        /* filling i-string of extended matrix of SLAE */
				for (j = 0; j < defs->ngenes; j++) {
					vinput[(defs->ngenes + 1) * i + j] = 0.0;
				}
				vinput[(defs->ngenes + 1) * i + i] = eps / (theta * theta) + 1 / (2.0 * theta);
				vinput[(defs->ngenes + 1) * i + defs->ngenes] =
          v1[defs->ngenes * ap + i] * (2 * eps / (theta * theta) - 2 * alpha + beta) +
          v0[defs->ngenes * ap + i] * (-eps / (theta * theta) + 1 / (2.0 * theta));
				if (ap == 0) {
					vinput[(defs->ngenes + 1) * i + defs->ngenes] +=
            2 * alpha * v1[defs->ngenes * (ap + 1) + i];
				} else if (ap == num_nucs - 1) {
					vinput[(defs->ngenes + 1) * i + defs->ngenes] +=
            2 * alpha * v1[defs->ngenes * (ap - 1) + i];
				} else {
					vinput[(defs->ngenes + 1) * i + defs->ngenes] +=
            alpha * (v1[defs->ngenes * (ap + 1) + i] + v1[defs->ngenes * (ap - 1) + i]);
				}
			}
      /* Gauss method, forward trace */
			for (i = 0; i < defs->ngenes - 1; i++) {
				for (k = i + 1; k < defs->ngenes; k++) {
					vinput1 = vinput[(defs->ngenes + 1) * k + i];
					for (j = i; j < defs->ngenes + 1; j++) {
						vinput[(defs->ngenes + 1) * k + j] -=
              vinput[(defs->ngenes + 1) * i + j] * vinput1 / vinput[(defs->ngenes + 1) * i + i];
					}
				}
			}
      /* Gauss method, back trace */
			for (i = defs->ngenes - 1; i >= 0; i--) {
				vinput1 = 0.0;
				for (j = defs->ngenes - 1; j > i; j--) {
					vinput1 += vinput[(defs->ngenes + 1) * i + j] * vdot[defs->ngenes * ap + j];
				}
				vdot[defs->ngenes * ap + i] =
          (vinput[(defs->ngenes + 1) * i + defs->ngenes] - vinput1) / vinput[(defs->ngenes + 1) * i + i];
			}
		}
	} else {
		error("Dvdt32ndTelNeumann: Bad rule %i sent to Dvdt32ndTelNeumann", rule);
	}
}


/*** Dvdt3TelMick: uses NSFD (Mickens' scheme) of
 *   1st order accuracy , calculates next time step   *
*******************************************************************/
void Dvdt3TelMick(double *v1, double *v0, double t, double *vdot, int n)
{
	static unsigned int samecycle = 0;     /* same cleavage cycle as before? */
	double vinput1, vinput3;         /* help variables */
	double theta = vdot[0];          /* time step size */
	double g, g2;                    /* g-function value */
	double eps;                      /* help cofficient */
	double alpha;                    /* help cofficient */
	double beta;                     /* help cofficient */
	double chi;                      /* help cofficient */
	double prevInpT, curInpT;        /* inputs from considered genes */
	double prevInpE, curInpE;        /* inputs from external genes */
	double inpM;                     /* input from maternal genes */
	int i, j, k, ap;                 /* local indexes */
	if (ccycle != samecycle) {
		GetD(t, lparm->d, defs->diff_schedule, D);
		GetL(t, lparm->m, defs->mob_schedule, M);
		GetLL(t, lparm->mm, defs->mob_schedule, MM);
		GetRCoef(t, &mit_factor);
		vmat = gcmd_get_maternal_inputs(ccycle);
		samecycle = ccycle;
	}
	if (rule == INTERPHASE) {
		if (defs->egenes > 0) {
			gcmd_get_external_inputs(t, vext, num_nucs * defs->egenes);
    }
    if (defs->egenes > 0) {
			gcmd_get_external_inputs(t - theta, vext2, num_nucs * defs->egenes);
    }
		for (ap = 0; ap < num_nucs; ap++) {
			for (i = 0; i < defs->ngenes; i++) {
				chi = 1.0 / (1 + lparm->lambda[i] * lparm->tau[i]);
				eps = lparm->tau[i] * chi;
				alpha = D[i] * chi;
				beta = -lparm->lambda[i] * chi;
				prevInpT = 0.0;
				curInpT = 0.0;
				prevInpE = 0.0;
				curInpE = 0.0;
				inpM = 0.0;
        for (k = 0; k < defs->ngenes; k++) {
					curInpT += lparm->T[defs->ngenes * i + k] * v1[defs->ngenes * ap + k];
					prevInpT += lparm->T[defs->ngenes * i + k] * v0[defs->ngenes * ap + k];
				}
				for (k = 0; k < defs->egenes; k++) {
					curInpE += lparm->E[defs->egenes * i + k] * vext[defs->egenes * ap + k];
					prevInpE += lparm->E[defs->egenes * i + k] * vext2[defs->egenes * ap + k];
				}
				for (k = 0; k < defs->mgenes; k++) {
					inpM += lparm->M[defs->mgenes * i + k] * vmat[defs->mgenes * ap + k];
        }
        vinput1 = curInpT + curInpE + inpM + lparm->h[i];
				vinput3 = prevInpT + prevInpE + inpM + lparm->h[i];
				if (gofu == Tanh) {
          g = lparm->R[i] / 2.0 * (1.0 + tanh(vinput1));
          g2 = lparm->R[i] / 2.0 * (1.0 + tanh(vinput3));
        }
        else if (gofu == Sqrt) {
          g = 0.5 * lparm->R[i] * (1.0 + vinput1 / sqrt(1 + vinput1 * vinput1));
          g2 = 0.5 * lparm->R[i] * (1.0 + vinput3 / sqrt(1 + vinput3 * vinput3));
        } else {
					error("Dvdt3MickOrig: unknown g(u)");
				}
        /* calculating next time step */
        vdot[defs->ngenes * ap + i] =
          v1[defs->ngenes * ap + i] * (2 * eps / (theta * theta) - 2 * alpha + beta) +
          g * (chi + eps / theta) - g2 * eps / theta +
          v0[defs->ngenes * ap + i] * (-eps / (theta * theta) + 1 / (2.0 * theta));
        if (ap == 0) {
          vdot[defs->ngenes * ap + i] +=
            2 * alpha * v1[defs->ngenes * (ap + 1) + i];
        } else if (ap == num_nucs - 1) {
					vdot[defs->ngenes * ap + i] +=
            2 * alpha * v1[defs->ngenes * (ap - 1) + i];
				} else {
					vdot[defs->ngenes * ap + i] +=
            alpha * (v1[defs->ngenes * (ap + 1) + i] + v1[defs->ngenes * (ap - 1) + i]);
				}
        vdot[defs->ngenes * ap + i] /= eps / (theta * theta) + 1 / (2.0 * theta);
      }
    }
	} else if (rule == MITOSIS) {
		for (ap = 0; ap < num_nucs; ap++) {
			for (i = 0; i < defs->ngenes; i++) {
				chi = 1.0 / (1 + lparm->lambda[i] * lparm->tau[i]);
				eps = lparm->tau[i] * chi;
				alpha = D[i] * chi;
				beta = -lparm->lambda[i] * chi;

        /* calculating next time step */
        vdot[defs->ngenes * ap + i] =
          v1[defs->ngenes * ap + i] * (2 * eps / (theta * theta) - 2 * alpha + beta) +
          v0[defs->ngenes * ap + i] * (-eps / (theta * theta) + 1 / (2.0 * theta));
        if (ap == 0) {
          vdot[defs->ngenes * ap + i] +=
            2 * alpha * v1[defs->ngenes * (ap + 1) + i];
        } else if (ap == num_nucs - 1) {
					vdot[defs->ngenes * ap + i] +=
            2 * alpha * v1[defs->ngenes * (ap - 1) + i];
				} else {
					vdot[defs->ngenes * ap + i] +=
            alpha * (v1[defs->ngenes * (ap + 1) + i] + v1[defs->ngenes * (ap - 1) + i]);
				}
        vdot[defs->ngenes * ap + i] /= eps / (theta * theta) + 1 / (2.0 * theta);
      }
    }
	} else {
		error("Dvdt3MickOrig: Bad rule %i sent to Dvdt3MickOrig", rule);
	}
}

/***                       Dvdt31stJeffDirichlet                         ***
 *                                                                         *
 *      uses explicit Finite-Difference Method of 1st order accuracy       *
 *       for the Jeffreys type equation, calculates next time step         *
 *                Dirichlet boundary conditions are used                   *
 *                                                                         *
 * *********************************************************************** *
 *                                                                         *
 * NOTATIONS :                                                             *
 * v1        <--   input meanings of concentration                         *
 * v0        <--   input meanings of concentration function gradient       *
 * vdot      <--   output meanings of concentration                        *
 * t         <--   current time moment                                     *
 * n         <--   length of v1, v0, vdot                                  *
 *                                                                         *
 *** ****************************************************************** ***/
void Dvdt31stJeffDirichlet(double *v1, double *v0, double t, double *vdot, int n)
{
   static unsigned int samecycle = 0;     /* same cleavage cycle as before? */
   double vinput1, vinput3;         /* help variables */
	double theta = vdot[0];          /* time step size */
   double tin = vdot[1];            /* initial time   */
	double g;                        /* g-function value */
	double gDer;                     /* g-function derivative value */
	double eps;                      /* help cofficient */
	double alpha;                    /* help cofficient */
	double beta;                     /* help cofficient */
	double chi;                      /* help cofficient */
   double delta;                    /* help cofficient */
	double prevInpT, curInpT;        /* inputs from considered genes */
	double prevInpE, curInpE;        /* inputs from external genes */
	double inpM;                     /* input from maternal genes */
   double g10 = vdot[3] - vdot[2];
   double g1L = vdot[5] - vdot[4];
   int i, j, k, ap;                 /* local indexes */

	if (ccycle != samecycle) {
		GetD(t, lparm->d, defs->diff_schedule, D);
		GetL(t, lparm->m, defs->mob_schedule, M);
		/* GetLL(t, lparm->mm, defs->mob_schedule, MM); */
      GetD(t, lparm->mm, defs->diff_schedule, MM);
		GetRCoef(t, &mit_factor);
      vmat = gcmd_get_maternal_inputs(ccycle);
		samecycle = ccycle;
	}
	if (rule == INTERPHASE) {
      if (defs->egenes > 0) {
         gcmd_get_external_inputs(t, vext, num_nucs * defs->egenes);
			gcdm_get_curveSlopeExtInputs(t, vext2, num_nucs * defs->egenes);
		}
		for (ap = 1; ap < num_nucs - 1; ap++) {
			for (i = 0; i < defs->ngenes; i++) {
				chi = 1.0 / (1 + lparm->lambda[i] * lparm->tau[i]);
				eps = lparm->tau[i] * chi;
				alpha = D[i] * chi;
				beta = -lparm->lambda[i] * chi;
            delta = -lparm->tau[i] * MM[i] * chi;
				prevInpT = 0.0;
				curInpT = 0.0;
				prevInpE = 0.0;
				curInpE = 0.0;
				inpM = 0.0;
				for (k = 0; k < defs->ngenes; k++) {
					curInpT += lparm->T[defs->ngenes * i + k] * v1[defs->ngenes * ap + k];
					prevInpT += lparm->T[defs->ngenes * i + k] * v0[defs->ngenes * ap + k];
				}
				for (k = 0; k < defs->egenes; k++) {
               curInpE += lparm->E[defs->egenes * i + k] * vext[defs->egenes * ap + k];
					prevInpE += lparm->E[defs->egenes * i + k] * vext2[defs->egenes * ap + k];
				}
				for (k = 0; k < defs->mgenes; k++) {
					inpM += lparm->M[defs->mgenes * i + k] * vmat[defs->mgenes * ap + k];
				}
				vinput1 = curInpT + curInpE + inpM + lparm->h[i];
				vinput3 = 1 / sqrt(1 + vinput1 * vinput1);
				if (gofu == Tanh) {
					g = lparm->R[i] / 2.0 * (1.0 + tanh(vinput1));
					gDer = lparm->R[i] / (2.0 * cosh(vinput1) * cosh(vinput1));
				} else if (gofu == Sqrt) {
					g = 0.5 * lparm->R[i] * (1.0 + vinput1 * vinput3);
					gDer = 0.5 * lparm->R[i] * vinput3 * vinput3 * vinput3;
				} else if (gofu == Lin) {
                  if (vinput1 > -1 && vinput1 < 1) {
						g = lparm->R[i] * (vinput1 + 1.0) / 2.0;
						gDer = lparm->R[i] / 2.0;
					}
					else {
						gDer = 0.0;
						if (vinput1 > 1)
							g = lparm->R[i];
						else
							g = 0.0;
					}
				} else {
               error("Dvdt31stJeffDirichlet: unknown g(u)");
				}
            /* calculating next time step */
            vdot[defs->ngenes * ap + i] =
               v1[defs->ngenes * ap + i] * (2 * eps / (theta * theta) + 2 * delta / theta - 2 * alpha + beta) +
               eps * gDer / theta * (curInpT - prevInpT) + eps * gDer * prevInpE + chi * g +
               (alpha - delta / theta) * (v1[defs->ngenes * (ap + 1) + i] + v1[defs->ngenes * (ap - 1) + i]) +
               delta / theta * (v0[defs->ngenes * (ap + 1) + i] + v0[defs->ngenes * (ap - 1) + i]) +
               v0[defs->ngenes * ap + i] * (-eps / (theta * theta) + 1 / (2.0 * theta) - 2 * delta / theta);
            vdot[defs->ngenes * ap + i] /= eps / (theta * theta) + 1 / (2.0 * theta);
         }
      }

      /* Dirichelt boundary conditions */
      for (i = 0; i < defs->ngenes; i++) {
         vdot[i] = vext[(defs->egenes - defs->ngenes) + i];
         vdot[defs->ngenes * (num_nucs - 1) + i] = vext[defs->egenes * (num_nucs - 1) + (defs->egenes - defs->ngenes) + i];
      }
   } else if (rule == MITOSIS) {
      for (ap = 1; ap < num_nucs - 1; ap++) {
         for (i = 0; i < defs->ngenes; i++) {
            chi = 1.0 / (1 + lparm->lambda[i] * lparm->tau[i]);
				eps = lparm->tau[i] * chi;
				alpha = D[i] * chi;
				beta = -lparm->lambda[i] * chi;
            delta = -lparm->tau[i] * MM[i] * chi;

            /* calculating next time step */
            vdot[defs->ngenes * ap + i] =
               v1[defs->ngenes * ap + i] * (2 * eps / (theta * theta) + 2 * delta / theta - 2 * alpha + beta) +
               (alpha - delta / theta) * (v1[defs->ngenes * (ap + 1) + i] + v1[defs->ngenes * (ap - 1) + i]) +
               delta / theta * (v0[defs->ngenes * (ap + 1) + i] + v0[defs->ngenes * (ap - 1) + i]) +
               v0[defs->ngenes * ap + i] * (-eps / (theta * theta) + 1 / (2.0 * theta) - 2 * delta / theta);
            vdot[defs->ngenes * ap + i] /= eps / (theta * theta) + 1 / (2.0 * theta);
         }
      }

      /* Dirichelt boundary conditions */
      for (i = 0; i < defs->ngenes; i++) {
         vdot[i] = vext[(defs->egenes - defs->ngenes) + i];
         vdot[defs->ngenes * (num_nucs - 1) + i] = vext[defs->egenes * (num_nucs - 1) + (defs->egenes - defs->ngenes) + i];
      }
   } else {
      error("Dvdt31stJeffDirichlet: Bad rule %i sent to Dvdt31stJeffDirichlet", rule);
	}
}

/***                       Dvdt31stJeffNeumann                           ***
 *                                                                         *
 *      uses explicit Finite-Difference Method of 1st order accuracy       *
 *       for the Jeffreys type equation, calculates next time step         *
 *                  Neumann boundary conditions are used                   *
 *                                                                         *
 * *********************************************************************** *
 *                                                                         *
 * NOTATIONS :                                                             *
 * v1        <--   input meanings of concentration                         *
 * v0        <--   input meanings of concentration function gradient       *
 * vdot      <--   output meanings of concentration                        *
 * t         <--   current time moment                                     *
 * n         <--   length of v1, v0, vdot                                  *
 *                                                                         *
 *** ****************************************************************** ***/
void Dvdt31stJeffNeumann(double *v1, double *v0, double t, double *vdot, int n)
{
  static unsigned int samecycle = 0;     /* same cleavage cycle as before? */
	double vinput1, vinput3;         /* help variables */
	double theta = vdot[0];          /* time step size */
   double tin = vdot[1];            /* initial time   */
	double g;                        /* g-function value */
	double gDer;                     /* g-function derivative value */
	double eps;                      /* help cofficient */
	double alpha;                    /* help cofficient */
	double beta;                     /* help cofficient */
	double chi;                      /* help cofficient */
   double delta;                    /* help cofficient */
	double prevInpT, curInpT;        /* inputs from considered genes */
	double prevInpE, curInpE;        /* inputs from external genes */
	double inpM;                     /* input from maternal genes */
   double g10 = vdot[3] - vdot[2];
   double g1L = vdot[5] - vdot[4];
   int i, j, k, ap;                 /* local indexes */

	if (ccycle != samecycle) {
		GetD(t, lparm->d, defs->diff_schedule, D);
		GetL(t, lparm->m, defs->mob_schedule, M);
		/* GetLL(t, lparm->mm, defs->mob_schedule, MM); */
      GetD(t, lparm->mm, defs->diff_schedule, MM);
		GetRCoef(t, &mit_factor);
      vmat = gcmd_get_maternal_inputs(ccycle);
		samecycle = ccycle;
	}
	if (rule == INTERPHASE) {
      if (defs->egenes > 0) {
			gcmd_get_external_inputs(t, vext, num_nucs * defs->egenes);
			gcdm_get_curveSlopeExtInputs(t, vext2, num_nucs * defs->egenes);
		}
		for (ap = 0; ap < num_nucs; ap++) {
			for (i = 0; i < defs->ngenes; i++) {
				chi = 1.0 / (1 + lparm->lambda[i] * lparm->tau[i]);
				eps = lparm->tau[i] * chi;
				alpha = D[i] * chi;
				beta = -lparm->lambda[i] * chi;
            delta = -lparm->tau[i] * MM[i] * chi;
				prevInpT = 0.0;
				curInpT = 0.0;
				prevInpE = 0.0;
				curInpE = 0.0;
				inpM = 0.0;
				for (k = 0; k < defs->ngenes; k++) {
					curInpT += lparm->T[defs->ngenes * i + k] * v1[defs->ngenes * ap + k];
					prevInpT += lparm->T[defs->ngenes * i + k] * v0[defs->ngenes * ap + k];
				}
				for (k = 0; k < defs->egenes; k++) {
               curInpE += lparm->E[defs->egenes * i + k] * vext[defs->egenes * ap + k];
					prevInpE += lparm->E[defs->egenes * i + k] * vext2[defs->egenes * ap + k];
				}
				for (k = 0; k < defs->mgenes; k++) {
					inpM += lparm->M[defs->mgenes * i + k] * vmat[defs->mgenes * ap + k];
				}
				vinput1 = curInpT + curInpE + inpM + lparm->h[i];
				vinput3 = 1 / sqrt(1 + vinput1 * vinput1);
				if (gofu == Tanh) {
					g = lparm->R[i] / 2.0 * (1.0 + tanh(vinput1));
					gDer = lparm->R[i] / (2.0 * cosh(vinput1) * cosh(vinput1));
				} else if (gofu == Sqrt) {
					g = 0.5 * lparm->R[i] * (1.0 + vinput1 * vinput3);
					gDer = 0.5 * lparm->R[i] * vinput3 * vinput3 * vinput3;
				} else if (gofu == Lin) {
					if (vinput1 > -1 && vinput1 < 1) {
						g = lparm->R[i] * (vinput1 + 1.0) / 2.0;
						gDer = lparm->R[i] / 2.0;
					}
					else {
						gDer = 0.0;
						if (vinput1 > 1)
							g = lparm->R[i];
						else
							g = 0.0;
					}
				} else {
               error("Dvdt31stJeffNeumann: unknown g(u)");
				}

            /* calculating next time step */
            vdot[defs->ngenes * ap + i] =
               v1[defs->ngenes * ap + i] * (2 * eps / (theta * theta) + 2 * delta / theta- 2 * alpha + beta) +
               eps * gDer / theta * (curInpT - prevInpT) + eps * gDer * prevInpE + chi * g +
               v0[defs->ngenes * ap + i] * (-eps / (theta * theta) + 1 / (2.0 * theta) - 2 * delta / theta);
            /* boundary conditions */
            // Neumann conditions
            // the far left nucleus
            if (ap == 0) {
               vdot[defs->ngenes * ap + i] +=
                  2 * (alpha - delta / theta) * v1[defs->ngenes * (ap + 1) + i] +
                  2 * delta / theta * v0[defs->ngenes * (ap + 1) + i];
            // the far right nucleus
            } else if (ap == num_nucs - 1) {
               vdot[defs->ngenes * ap + i] +=
                  2 * (alpha - delta / theta) * v1[defs->ngenes * (ap - 1) + i] +
                  2 * delta / theta * v0[defs->ngenes * (ap - 1) + i];
            // any middle nucleus
            } else {
              vdot[defs->ngenes * ap + i] +=
                  (alpha - delta / theta) * (v1[defs->ngenes * (ap + 1) + i] + v1[defs->ngenes * (ap - 1) + i]) +
                  delta / theta * (v0[defs->ngenes * (ap + 1) + i] + v0[defs->ngenes * (ap - 1) + i]);
            }
            vdot[defs->ngenes * ap + i] /= eps / (theta * theta) + 1 / (2.0 * theta);
         }
      }
   } else if (rule == MITOSIS) {
      for (ap = 0; ap < num_nucs; ap++) {
         for (i = 0; i < defs->ngenes; i++) {
            chi = 1.0 / (1 + lparm->lambda[i] * lparm->tau[i]);
            eps = lparm->tau[i] * chi;
            alpha = D[i] * chi;
            beta = -lparm->lambda[i] * chi;
            delta = -lparm->tau[i] * MM[i] * chi;

            /* calculating next time step */
            vdot[defs->ngenes * ap + i] =
               v1[defs->ngenes * ap + i] * (2 * eps / (theta * theta) + 2 * delta / theta - 2 * alpha + beta) +
               v0[defs->ngenes * ap + i] * (-eps / (theta * theta) + 1 / (2.0 * theta) - 2 * delta / theta);
            /* boundary conditions */
            // Neumann conditions
            // the far left nucleus
            if (ap == 0) {
               vdot[defs->ngenes * ap + i] +=
                  2 * (alpha - delta / theta) * v1[defs->ngenes * (ap + 1) + i] +
                  2 * delta / theta * v0[defs->ngenes * (ap + 1) + i];
            // the far right nucleus
            } else if (ap == num_nucs - 1) {
               vdot[defs->ngenes * ap + i] +=
                  2 * (alpha - delta / theta) * v1[defs->ngenes * (ap - 1) + i] +
                  2 * delta / theta * v0[defs->ngenes * (ap - 1) + i];
            // any middle nucleus
            } else {
              vdot[defs->ngenes * ap + i] +=
                  (alpha - delta / theta) * (v1[defs->ngenes * (ap + 1) + i] + v1[defs->ngenes * (ap - 1) + i]) +
                  delta / theta * (v0[defs->ngenes * (ap + 1) + i] + v0[defs->ngenes * (ap - 1) + i]);
            }
            vdot[defs->ngenes * ap + i] /= eps / (theta * theta) + 1 / (2.0 * theta);
         }
      }
   } else {
      error("Dvdt31stJeffNeumann: Bad rule %i sent to Dvdt31stJeffNeumann", rule);
   }
}


/*** DvdtOrig: the derivative function; implements the equations **
*******************************************************************/
void Dvdt3Orig(double *v, double *w, double t, double *vdot, int n)
{
	int        ap;                 /* nuclear index on AP axis [0,1,...,m-1] */
	int        i,j;                                   /* local loop counters */
	int        k;                   /* index of gene k in a specific nucleus */
	int        base;            /* index of first gene in a specific nucleus */
	static unsigned int samecycle = 0;            /* same cleavage cycle as before? */
	if (ccycle != samecycle) {
		GetD(t, lparm->d, defs->diff_schedule, D);
		GetL(t, lparm->m, defs->mob_schedule, M);
		GetLL(t, lparm->mm, defs->mob_schedule, MM);
		GetRCoef(t, &mit_factor);
		vmat = gcmd_get_maternal_inputs(ccycle);
		samecycle = ccycle;
	}
	if ( defs->egenes )
		gcmd_get_external_inputs(t, vext, num_nucs * defs->egenes);
	if ( defs->ncouples )
		gcmd_get_coupled_inputs(t, vcouple, wcouple, num_nucs * defs->ncouples, v, w, n, vext, num_nucs * defs->egenes, vmat, num_nucs * defs->mgenes);
	if (rule == INTERPHASE) {
		register double vinput1 = 0;
		register double vinput4 = 0;
		for(base = 0, ap=0; base < n ; base += defs->ngenes, ap++) {
			for(i=base; i < base + defs->ngenes; i++) {
				k = i - base;
				vinput1 = lparm->h[k];
				vinput4 = 0;
				for(j=0; j < defs->mgenes; j++) {
					vinput1  += lparm->M[(k*defs->mgenes)+j] * vmat[ap * defs->mgenes + j];
				}
				for(j=0; j < defs->ngenes; j++) {
					vinput1 += lparm->T[(k*defs->ngenes)+j] * v[base + j];
					vinput4 += lparm->T[(k*defs->ngenes)+j] * w[base + j];
				}
				for(j=0; j < defs->egenes; j++) {
					vinput1  += lparm->E[(k*defs->egenes)+j] * vext[ap * defs->egenes + j];
				}
				for(j=0; j < defs->ncouples; j++) {
					vinput1 += lparm->P[(k*defs->ncouples)+j] * vcouple[ap * defs->ncouples + j];
					vinput4 += lparm->P[(k*defs->ncouples)+j] * wcouple[ap * defs->ncouples + j];
				}
				bot2[i]   = 1 + vinput1 * vinput1;
				vinput[i] = vinput1;
				vinput2[i] = vinput4;
			}
		}
		if ( gofu == Sqrt ) {
			for(i=0; i < n; i++)
				bot[i] = 1 / sqrt(bot2[i]);
			for(i=0; i < n; i++) {
				vinput1 = bot[i];
				bot[i]= vinput[i] * vinput1 + 1;
				bot3[i] = vinput1 * vinput1 * vinput1;
				bot5[i] = vinput1 * vinput1 * vinput1 * vinput1 * vinput1;
			}
		} else
			error("DvdtOrig: unknown g(u)");
		if ( n == defs->ngenes ) {       /* first part: one nuc, no diffusion */
			register double vdot1, g1;
			for( base=0; base<n; base+=defs->ngenes ) {
				for( i=base; i<base+defs->ngenes; i++ ) {
					k = i - base;
					vdot1 = -lparm->lambda[k] * v[i];
					g1 = bot[i];
					vdot1  += lparm->R[k] * 0.5 * g1 * mit_factor;
					vdot[i] = vdot1;
				}
			}
/* It works only for more than 3 nuclei */
		} else {
			register double vdot1, g1;
			for(i = 0; i < defs->ngenes; i++) {
				k = i;
				vdot1 = -lparm->lambda[k] * v[i] - w[i];
				g1 = bot[i];
				vdot1 += lparm->R[k] * 0.5 * g1  * mit_factor;
				g1 = (v[i + defs->ngenes] - v[i]) * boundary;
				vdot1 += D[k] * g1;
				vdot1 -= M[k] * g1 * lparm->lambda[k];
				vdot1 += lparm->R[k] * 0.5 * M[k] * g1 * bot3[i];
				g1 = (v[i + defs->ngenes] - v[i]);
				vdot1 += lparm->R[k] * M[k] * g1 * g1 * bot5[i] * (-1.5) * vinput[i];
				vdot1 -= M[k] * ( w[i + defs->ngenes] - w[i] );
				vdot1 /= lparm->tau[k];
				vdot1 -= lparm->lambda[k] * w[i];
				g1 = bot3[i];
				vdot1 += lparm->R[k] * 0.5 * g1  * mit_factor * vinput2[i];
				vdot[i] = vdot1;
			}
/* then middle nuclei */
			for(base = defs->ngenes; base < n - defs->ngenes; base += defs->ngenes) {
				for(i = base; i < base + defs->ngenes; i++) {
					k = i - base;
					vdot1 = -lparm->lambda[k] * v[i] - w[i];
					g1 = bot[i];
					vdot1 += lparm->R[k] * 0.5 * g1  * mit_factor;
					g1 = (v[i - defs->ngenes] - v[i]) + (v[i + defs->ngenes] - v[i]);
					vdot1 += D[k] * g1;
					vdot1 -= M[k] * g1 * lparm->lambda[k];
					vdot1 += lparm->R[k] * 0.5 * M[k] * g1 * bot3[i];
					g1 = 0.5 * (v[i + defs->ngenes] - v[i - defs->ngenes]);
					vdot1 += lparm->R[k] * (-1.5) * vinput[i] * M[k] * g1 * g1 * bot5[i];
					vdot1 -= M[k] * ( (w[i - defs->ngenes] - w[i]) + (w[i + defs->ngenes] - w[i]) );
					vdot1 /= lparm->tau[k];
					vdot1 -= lparm->lambda[k] * w[i];
					g1 = bot3[i];
					vdot1 += lparm->R[k] * 0.5 * g1  * mit_factor * vinput2[i];
					vdot[i] = vdot1;
				}
			}
/* last: posterior-most nucleus */
			base = n - defs->ngenes;
			for(i = base; i < base + defs->ngenes; i++) {
				k = i - base;
				vdot1 = -lparm->lambda[k] * v[i] - w[i];
				g1 = bot[i];
				vdot1 += lparm->R[k] * 0.5 * g1  * mit_factor;
				g1 = (v[i - defs->ngenes] - v[i]) * boundary;
				vdot1 += D[k] * g1;
				vdot1 -= M[k] * g1 * lparm->lambda[k];
				vdot1 += lparm->R[k] * 0.5 * M[k] * g1 * bot3[i];
				g1 = (v[i - defs->ngenes] - v[i]);
				vdot1 += lparm->R[k] * (-1.5) * vinput[i] * M[k] * g1 * g1 * bot5[i];
				vdot1 -= M[k] * ( w[i - defs->ngenes] - w[i] );
				vdot1 /= lparm->tau[k];
				vdot1 -= lparm->lambda[k] * w[i];
				g1 = bot3[i];
				vdot1 += lparm->R[k] * 0.5 * g1  * mit_factor * vinput2[i];
				vdot[i] = vdot1;
			}
		}
/* during mitosis only diffusion and decay happen */
	} else if (rule == MITOSIS) {
		if ( n == defs->ngenes ) {         /* first part: one nuc, no diffusion */
			register double vdot1;
			for( base=0; base<n; base+=defs->ngenes ) {
				for( i=base; i<base+defs->ngenes; i++ ) {
					k = i - base;
					vdot1 = -lparm->lambda[k] * v[i];
					vdot[i] = vdot1;
				}
			}
		} else {                      /* then for multiple nuclei -> diffusion */
			register double vdot1,g1;
			for(i = 0; i < defs->ngenes; i++) {
				k = i;
				vdot1 = -lparm->lambda[k] * v[i] - w[i];
				g1 = (v[i + defs->ngenes] - v[i]) * boundary;
				vdot1 += D[k] * g1;
				vdot1 -= MM[k] * g1 * lparm->lambda[k];
				vdot1 -= MM[k] * ( w[i + defs->ngenes] - w[i] );
				vdot1 /= lparm->tau[k];
				vdot1 -= lparm->lambda[k] * w[i];
				vdot[i] = vdot1;
			}
/* then middle nuclei */
			for(base = defs->ngenes; base < n - defs->ngenes; base += defs->ngenes) {
				for(i = base; i < base + defs->ngenes; i++) {
					k = i - base;
					vdot1 = -lparm->lambda[k] * v[i] - w[i];
					g1 = (v[i - defs->ngenes] - v[i]) + (v[i + defs->ngenes] - v[i]);
					vdot1 += D[k] * g1;
					vdot1 -= MM[k] * g1 * lparm->lambda[k];
					vdot1 -= MM[k] * ( (w[i - defs->ngenes] - w[i]) + (w[i + defs->ngenes] - w[i]) );
					vdot1 /= lparm->tau[k];
					vdot1 -= lparm->lambda[k] * w[i];
					vdot[i] = vdot1;
				}
			}
/* last: posterior-most nucleus */
			base = n - defs->ngenes;
			for(i = base; i < base + defs->ngenes; i++) {
				k = i - base;
				vdot1 = -lparm->lambda[k] * v[i] - w[i];
				g1 = (v[i - defs->ngenes] - v[i]) * boundary;
				vdot1 += D[k] * g1;
				vdot1 -= MM[k] * g1 * lparm->lambda[k];
				vdot1 -= MM[k] * ( w[i - defs->ngenes] - w[i] );
				vdot1 /= lparm->tau[k];
				vdot1 -= lparm->lambda[k] * w[i];
				vdot[i] = vdot1;
			}
		}
	} else
		error("Dvdt3Orig: Bad rule %i sent to Dvdt3Orig", rule);
}


/*** Dvdt2Mob1: the derivative function; implements the equations **
*******************************************************************/
void Dvdt2Mob1(double *v, double *w, double t, double *vdot, int n)
{
	int        ap;                 /* nuclear index on AP axis [0,1,...,m-1] */
	int        i,j;                                   /* local loop counters */
	int        k;                   /* index of gene k in a specific nucleus */
	int        base;            /* index of first gene in a specific nucleus */
#ifdef ALPHA_DU
  int        incx = 1;        /* increment step size for vsqrt input array */
  int        incy = 1;       /* increment step size for vsqrt output array */
#endif
	static unsigned int samecycle = 0;            /* same cleavage cycle as before? */
/* get D parameters and bicoid gradient according to cleavage cycle */
/* previously it depended on num_nucs*/
	if (ccycle != samecycle) {
		GetD(t, lparm->d, defs->diff_schedule, D);
		GetM(t, lparm->m, defs->mob_schedule, M);
		GetRCoef(t, &mit_factor);
		vmat = gcmd_get_maternal_inputs(ccycle);
		samecycle = ccycle;
	}
/*fprintf(stdout,"\n n = %d t = %f r = %d ", n, t, rule);*/
	if ( defs->egenes )
		gcmd_get_external_inputs(t, vext, num_nucs * defs->egenes);
/*for(j=0; j < defs->ngenes; j++) {
	fprintf(stdout,"%d %f ", j, lparm->M[j]);
	for(i = 0; i < defs->egenes; i++)
		fprintf(stdout,"%f ", lparm->E[j * defs->egenes+i]);
	fprintf(stdout,"%f ", lparm->R[j]);
	fprintf(stdout,"%f ", lparm->d[j]);
	fprintf(stdout,"%f ", lparm->lambda[j]);
	fprintf(stdout,"%f ", lparm->h[j]);
	fprintf(stdout,"%f ", lparm->tau[j]);
}*/
/*fprintf(stdout,"vmat\n");
for(j=0; j < defs->mgenes * num_nucs; j++)
	fprintf(stdout,"%d %f\n", j, vmat[j]);
fflush(stdout);*/
/* This is how it works (by JR):
    ap      nucleus position on ap axis
    base    index of first gene (0) in current nucleus
    k       index of gene k in current nucleus
            Protein synthesis terms are calculated according to g(u)
           First we do loop for vinput contributions; vinput contains
   the u that goes into g(u)
   Then we do a separate loop or vector func for sqrt or exp
   Then we do vdot contributions (R and lambda part)
   Then we do the spatial part (Ds); we do a special case for
            each end
            Note, however, that before the real loop, we have to do a
            special case for when rhere is one nuc, hence no diffusion
    These loops look a little funky 'cause we don't want any
            divides'                                                       */
	if (rule == INTERPHASE) {
		register double vinput1 = 0;
		register double vinput4 = 0;
		for(base = 0, ap=0; base < n ; base += defs->ngenes, ap++) {
			for(i=base; i < base + defs->ngenes; i++) {
				k = i - base;
				vinput1 = lparm->h[k];
				vinput4 = 0;
				for(j=0; j < defs->mgenes; j++) {
					vinput1  += lparm->M[(k*defs->mgenes)+j] * vmat[ap * defs->mgenes + j];
/*fprintf(stdout, " ap = %d k = %d j = %d m = %f vmat = %f mv = %f\n", ap, k, j, lparm->M[(k*defs->mgenes)+j] , vmat[ap * defs->mgenes + j], lparm->M[(k*defs->mgenes)+j] * vmat[ap * defs->mgenes + j] );*/
				}
				for(j=0; j < defs->ngenes; j++) {
					vinput1 += lparm->T[(k*defs->ngenes)+j] * v[base + j];
					vinput4 += lparm->T[(k*defs->ngenes)+j] * w[base + j];
				}
				for(j=0; j < defs->egenes; j++) {
					vinput1  += lparm->E[(k*defs->egenes)+j] * vext[ap * defs->egenes + j];
/*fprintf(stdout, " ap = %d k = %d j = %d m = %f vmat = %f mv = %f\n", ap, k, j, lparm->E[(k*defs->egenes)+j] , vext[ap * defs->egenes + j], lparm->E[(k*defs->egenes)+j] * vext[ap * defs->egenes + j] );*/
				}
				bot2[i]   = 1 + vinput1 * vinput1;
				vinput[i] = vinput1;
				vinput2[i] = vinput4;
			}
		}
/***************************************************************************
 *                                                                         *
 *        g(u) = 1/2 * ( u / sqrt(1 + u^2) + 1)                            *
 *                                                                         *
 ***************************************************************************/
		if ( gofu == Sqrt ) {
                            /* now calculate sqrt(1+u2); store it in bot[] */
#ifdef ALPHA_DU
			vsqrt_(bot2,&incx,bot,&incy,&n);    /* superfast DEC vector function */
			for(i=0; i < n; i++) {
				bot[i]= 1 / bot[i];
			}
#else
#ifdef MKL
			vdInvSqrt(n, bot2, bot);
#else
			for(i=0; i < n; i++)                  /* slow traditional style sqrt */
				bot[i] = 1 / sqrt(bot2[i]);
#endif
#endif
			for(i=0; i < n; i++) {
				vinput1 = bot[i];
				bot[i]= vinput[i] * vinput1+1;
				bot3[i] = vinput1 * vinput1 * vinput1;
			}
/***************************************************************************
 *                                                                         *
 *        g(u) = 1/2 * (tanh(u) + 1) )                                     *
 *                                                                         *
 ***************************************************************************/
		} else if ( gofu == Tanh ) {
#ifdef MKL
			vdTanh(n, vinput, bot);
			for(i=0; i < n; i++) {
				bot[i] += 1;
 			}
#else
			for(i=0; i < n; i++) {
				bot[i]= tanh(vinput[i])+1;
 			}
#endif
/***************************************************************************
 *                                                                         *
 *        g(u) = 1 / (1 + exp(-2u))                                        *
 *                                                                         *
 ***************************************************************************/
		} else if ( gofu == Exp ) {
                              /* now calculate exp(-u); store it in bot[] */
#ifdef ALPHA_DU
			vexp_(vinput,&incx,bot,&incy,&n);   /* superfast DEC vector function */
#else
#ifdef MKL
			vdExp(n, vinput, bot);
#else
			for (i=0; i<n; i++)                    /* slow traditional style exp */
				bot[i] = exp(vinput[i]);
#endif
#endif
			for(i=0; i < n; i++) {
				vinput1 = bot[i];
				bot[i]= vinput1*vinput1/(vinput1*vinput1+1);
			}
		} else
			error("DvdtOrig: unknown g(u)");
            /* next loop does the rest of the equation (R, Ds and lambdas) */
                                                 /* store result in vdot[] */
		if ( n == defs->ngenes ) {       /* first part: one nuc, no diffusion */
			register double vdot1, g1;
			for( base=0; base<n; base+=defs->ngenes ) {
				for( i=base; i<base+defs->ngenes; i++ ) {
					k = i - base;
					vdot1 = -lparm->lambda[k] * v[i];
					g1 = bot[i];
					vdot1  += lparm->R[k] * 0.5 * g1 * mit_factor;
					vdot[i] = vdot1;
				}
			}
/* It works only for more than 3 nuclei */
		} else {                    /* then for multiple nuclei -> diffusion */
			register double vdot1,g1;
			for(i = 0; i < defs->ngenes; i++) {     /* first anterior-most nucleus */
				k = i;
				vdot1 = -lparm->lambda[k] * v[i] - w[i];
				g1 = bot[i];
				vdot1 += lparm->R[k] * 0.5 * g1  * mit_factor;
				vdot1 += D[k] * (v[i + defs->ngenes] - v[i]) * boundary;
//				vdot1 -= M[k] * ( v[i] - 4 * v[i + defs->ngenes] + 6 * v[i + 2 * defs->ngenes] - 4 * v[i + 3 * defs->ngenes] +  v[i + 4 * defs->ngenes] );
				vdot1 /= lparm->tau[k];
				vdot1 += -lparm->lambda[k] * w[i];
				g1 = bot3[i];
				vdot1 += lparm->R[k] * 0.5 * g1  * mit_factor * vinput2[i];
				vdot[i] = vdot1;
			}
			for(i = defs->ngenes; i < 2 * defs->ngenes; i++) {     /* first anterior-most nucleus */
				k = i - defs->ngenes;
				vdot1 = -lparm->lambda[k] * v[i] - w[i];
				g1 = bot[i];
				vdot1 += lparm->R[k] * 0.5 * g1  * mit_factor;
				vdot1 += D[k] * ((v[i - defs->ngenes] - v[i]) + (v[i + defs->ngenes] - v[i]) );
//				vdot1 -= M[k] * ( v[i - defs->ngenes] - 4 * v[i] + 6 * v[i + defs->ngenes] - 4 * v[i + 2 * defs->ngenes] +  v[i + 3 * defs->ngenes] );
				vdot1 /= lparm->tau[k];
				vdot1 += -lparm->lambda[k] * w[i];
				g1 = bot3[i];
				vdot1 += lparm->R[k] * 0.5 * g1  * mit_factor * vinput2[i];
				vdot[i] = vdot1;
			}
/* then middle nuclei */
			for(base = 2 * defs->ngenes; base < n - 2 * defs->ngenes; base += defs->ngenes) {
				for(i = base; i < base + defs->ngenes; i++) {
					k = i - base;
					vdot1 = -lparm->lambda[k] * v[i] - w[i];
//					fprintf(stdout,"%f ", vdot);
					g1 = bot[i];
					vdot1 += lparm->R[k] * 0.5 * g1  * mit_factor;
//					fprintf(stdout,"%f ", vdot);
					vdot1 += D[k] * ((v[i - defs->ngenes] - v[i]) + (v[i + defs->ngenes] - v[i]) );
//					vdot1 -= M[k] * ( v[i - 2 * defs->ngenes] - 4 * v[i - defs->ngenes] + 6 * v[i] - 4 * v[i + defs->ngenes] +  v[i + 2 * defs->ngenes] );
//					fprintf(stdout,"%f n", vdot);
					vdot1 /= lparm->tau[k];
					vdot1 += -lparm->lambda[k] * w[i];
					g1 = bot3[i];
	  				vdot1 += lparm->R[k] * 0.5 * g1  * mit_factor * vinput2[i];
					vdot[i] = vdot1;
//					fprintf(stdout,"%f\n", vdot);
				}
			}
			base = n - 2 * defs->ngenes;
			for(i = base; i < base + defs->ngenes; i++) {
				k = i - base;
				vdot1 = -lparm->lambda[k] * v[i] - w[i];
				g1 = bot[i];
				vdot1 += lparm->R[k] * 0.5 * g1  * mit_factor;
				vdot1 += D[k] * ((v[i - defs->ngenes] - v[i]) + (v[i + defs->ngenes] - v[i]) );
//				vdot1 -= M[k] * ( v[i - 3 * defs->ngenes] - 4 * v[i - 2 * defs->ngenes] + 6 * v[i - defs->ngenes] - 4 * v[i] + v[i + defs->ngenes] );
				vdot1 /= lparm->tau[k];
				vdot1 += -lparm->lambda[k] * w[i];
				g1 = bot3[i];
	  			vdot1 += lparm->R[k] * 0.5 * g1  * mit_factor * vinput2[i];
				vdot[i] = vdot1;
			}
/* last: posterior-most nucleus */
			base = n - defs->ngenes;
			for(i = base; i < base + defs->ngenes; i++) {
				k = i - base;
				vdot1 = -lparm->lambda[k] * v[i] - w[i];
				g1 = bot[i];
				vdot1 += lparm->R[k] * 0.5 * g1  * mit_factor;
				vdot1 += D[k] * (v[i - defs->ngenes] - v[i]);
//				vdot1 -= M[k] * ( v[i - 4 * defs->ngenes] - 4 * v[i - 3 * defs->ngenes] + 6 * v[i - 2 * defs->ngenes] - 4 * v[i - defs->ngenes] +  v[i] );
				vdot1 /= lparm->tau[k];
				vdot1 += -lparm->lambda[k] * w[i];
				g1 = bot3[i];
	  			vdot1 += lparm->R[k] * 0.5 * g1  * mit_factor * vinput2[i];
				vdot[i] = vdot1;
			}
		}
/* during mitosis only diffusion and decay happen */
	} else if (rule == MITOSIS) {
		if ( n == defs->ngenes ) {         /* first part: one nuc, no diffusion */
			register double vdot1;
			for( base=0; base<n; base+=defs->ngenes ) {
				for( i=base; i<base+defs->ngenes; i++ ) {
					k = i - base;
					vdot1 = -lparm->lambda[k] * v[i];
					vdot[i] = vdot1;
				}
			}
		} else {                      /* then for multiple nuclei -> diffusion */
			register double vdot1,g1;
			for(i = 0; i < defs->ngenes; i++) {     /* first anterior-most nucleus */
				k = i;
				vdot1 = -lparm->lambda[k] * v[i] - w[i];
				vdot1 += D[k] * (v[i + defs->ngenes] - v[i]) * boundary;
				vdot1 -= M[k] * ( v[i] - 4 * v[i + defs->ngenes] + 6 * v[i + 2 * defs->ngenes] - 4 * v[i + 3 * defs->ngenes] +  v[i + 4 * defs->ngenes] );
				vdot1 /= lparm->tau[k];
				vdot1 += -lparm->lambda[k] * w[i];
				vdot[i] = vdot1;
			}
			for(i = defs->ngenes; i < 2 * defs->ngenes; i++) {     /* first anterior-most nucleus */
				k = i - defs->ngenes;
				vdot1 = -lparm->lambda[k] * v[i] - w[i];
				vdot1 += D[k] * ((v[i - defs->ngenes] - v[i]) + (v[i + defs->ngenes] - v[i]) );
				vdot1 -= M[k] * ( v[i - defs->ngenes] - 4 * v[i] + 6 * v[i + defs->ngenes] - 4 * v[i + 2 * defs->ngenes] +  v[i + 3 * defs->ngenes] );
				vdot1 /= lparm->tau[k];
				vdot1 += -lparm->lambda[k] * w[i];
				vdot[i] = vdot1;
			}
/* then middle nuclei */
			for(base = 2 * defs->ngenes; base < n - 2 * defs->ngenes; base += defs->ngenes) {
				for(i = base; i < base + defs->ngenes; i++) {
					k = i - base;
					vdot1 = -lparm->lambda[k] * v[i] - w[i];
					vdot1 += D[k] * ((v[i - defs->ngenes] - v[i]) + (v[i + defs->ngenes] - v[i]) );
					vdot1 -= M[k] * ( v[i - 2 * defs->ngenes] - 4 * v[i - defs->ngenes] + 6 * v[i] - 4 * v[i + defs->ngenes] +  v[i + 2 * defs->ngenes] );
					vdot1 /= lparm->tau[k];
					vdot1 += -lparm->lambda[k] * w[i];
					vdot[i] = vdot1;
				}
			}
			base = n - 2 * defs->ngenes;
			for(i = base; i < base + defs->ngenes; i++) {
				k = i - base;
				vdot1 = -lparm->lambda[k] * v[i] - w[i];
				vdot1 += D[k] * ((v[i - defs->ngenes] - v[i]) + (v[i + defs->ngenes] - v[i]) );
				vdot1 -= M[k] * ( v[i - 3 * defs->ngenes] - 4 * v[i - 2 * defs->ngenes] + 6 * v[i - defs->ngenes] - 4 * v[i] + v[i + defs->ngenes] );
				vdot1 /= lparm->tau[k];
				vdot1 += -lparm->lambda[k] * w[i];
				vdot[i] = vdot1;
			}
/* last: posterior-most nucleus */
			base = n - defs->ngenes;
			for(i = base; i < base + defs->ngenes; i++) {
				k = i - base;
				vdot1 = -lparm->lambda[k] * v[i] - w[i];
				vdot1 += D[k] * (v[i - defs->ngenes] - v[i]);
				vdot1 -= M[k] * ( v[i - 4 * defs->ngenes] - 4 * v[i - 3 * defs->ngenes] + 6 * v[i - 2 * defs->ngenes] - 4 * v[i - defs->ngenes] +  v[i] );
				vdot1 /= lparm->tau[k];
				vdot1 += -lparm->lambda[k] * w[i];
				vdot[i] = vdot1;
			}
		}
	} else
		error("Dvdt2Orig: Bad rule %i sent to DvdtOrig", rule);
}


/*** Dvdt2Bump: the derivative function for Perkins&Jaeger **
 ***************************************************************************/
void Dvdt2Bump(double *v, double *w, double t, double *vdot, int n)
{
	int        ap;                 /* nuclear index on AP axis [0,1,...,m-1] */
	int        i,j;                                   /* local loop counters */
	int        k;                   /* index of gene k in a specific nucleus */
	int        base;            /* index of first gene in a specific nucleus */
	static unsigned int samecycle = 0;            /* same cleavage cycle as before? */
/* get D parameters and bicoid gradient according to cleavage cycle */
/* previously it depended on num_nucs*/
	if (ccycle != samecycle) {
		GetD(t, lparm->d, defs->diff_schedule, D);
		GetM(t, lparm->m, defs->mob_schedule, M);
		GetRCoef(t, &mit_factor);
//		vmat = gcmd_get_maternal_inputs(ccycle);
		samecycle = ccycle;
	}
/*fprintf(stdout,"\n n = %d t = %f r = %d ", n, t, rule);*/
//	if ( defs->egenes )
//		gcmd_get_external_inputs(t, vext, num_nucs * defs->egenes);
	if (rule == INTERPHASE) {
		register double x = 0;
		register double vinput1 = 0;
		register int ndom = 0;
		for(base = 0, ap = 0; base < n ; base += defs->ngenes, ap++) {
			for(i = base; i < base + defs->ngenes; i++) {
				k = i - base;
				ndom = (int)lparm->h[k];
				x = 100.0 * (double)ap * (double)defs->ngenes / (double)n;
				vinput1 = 0.0;
				for( j = 0; j < ndom; j++) {
					if ( lparm->E[ ( k * ndom ) + j * 6 + 2] <= t && t <= lparm->E[ ( k * ndom ) + j * 6 + 5] ) {
						double alfa = ( lparm->E[ ( k * ndom ) + j * 6 + 5] - t ) / ( lparm->E[ ( k * ndom ) + j * 6 + 5] - lparm->E[ ( k * ndom ) + j * 6 + 2] );
						double bita = ( t - lparm->E[ ( k * ndom ) + j * 6 + 2] ) / ( lparm->E[ ( k * ndom ) + j * 6 + 5] - lparm->E[ ( k * ndom ) + j * 6 + 2] );
						if ( alfa * lparm->E[ ( k * ndom ) + j * 6] + bita * lparm->E[ ( k * ndom ) + j * 6 + 3] <= x && x <= alfa * lparm->E[ ( k * ndom ) + j * 6 + 1] + bita * lparm->E[ ( k * ndom ) + j * 6 + 4] )
							vinput1 = 1.0;
					}
				}
				bot[i] = vinput1;
			}
		}
		if ( n == defs->ngenes ) {       /* first part: one nuc, no diffusion */
			register double vdot1, g1;
			for( base=0; base<n; base+=defs->ngenes ) {
				for( i=base; i<base+defs->ngenes; i++ ) {
					k = i - base;
					vdot1 = -lparm->lambda[k] * v[i];
					g1 = bot[i];
					vdot1  += lparm->R[k] * g1 * mit_factor;
					vdot[i] = vdot1;
				}
			}
/* It works only for more than 3 nuclei */
		} else {                    /* then for multiple nuclei -> diffusion */
			register double vdot1,g1;
			for(i = 0; i < defs->ngenes; i++) {     /* first anterior-most nucleus */
				k = i;
				vdot1 = -lparm->lambda[k] * v[i] - w[i];
				g1 = bot[i];
				vdot1 += lparm->R[k] * g1  * mit_factor;
				vdot1 += D[k] * (v[i + defs->ngenes] - v[i]) * boundary;
				vdot1 -= M[k] * ( v[i] - 4 * v[i + defs->ngenes] + 6 * v[i + 2 * defs->ngenes] - 4 * v[i + 3 * defs->ngenes] +  v[i + 4 * defs->ngenes] );
				vdot1 /= lparm->tau[k];
				vdot1 += -lparm->lambda[k] * w[i];
				g1 = bot3[i];
				vdot1 += lparm->R[k] * g1  * mit_factor * vinput2[i];
				vdot[i] = vdot1;
			}
			for(i = defs->ngenes; i < 2 * defs->ngenes; i++) {     /* first anterior-most nucleus */
				k = i - defs->ngenes;
				vdot1 = -lparm->lambda[k] * v[i] - w[i];
				g1 = bot[i];
				vdot1 += lparm->R[k] * g1  * mit_factor;
				vdot1 += D[k] * ((v[i - defs->ngenes] - v[i]) + (v[i + defs->ngenes] - v[i]) );
				vdot1 -= M[k] * ( v[i - defs->ngenes] - 4 * v[i] + 6 * v[i + defs->ngenes] - 4 * v[i + 2 * defs->ngenes] +  v[i + 3 * defs->ngenes] );
				vdot1 /= lparm->tau[k];
				vdot1 += -lparm->lambda[k] * w[i];
				g1 = bot3[i];
				vdot1 += lparm->R[k] * g1  * mit_factor * vinput2[i];
				vdot[i] = vdot1;
			}
/* then middle nuclei */
			for(base = 2 * defs->ngenes; base < n - 2 * defs->ngenes; base += defs->ngenes) {
				for(i = base; i < base + defs->ngenes; i++) {
					k = i - base;
					vdot1 = -lparm->lambda[k] * v[i] - w[i];
					g1 = bot[i];
					vdot1 += lparm->R[k] * g1  * mit_factor;
					vdot1 += D[k] * ((v[i - defs->ngenes] - v[i]) + (v[i + defs->ngenes] - v[i]) );
					vdot1 -= M[k] * ( v[i - 2 * defs->ngenes] - 4 * v[i - defs->ngenes] + 6 * v[i] - 4 * v[i + defs->ngenes] +  v[i + 2 * defs->ngenes] );
					vdot1 /= lparm->tau[k];
					vdot1 += -lparm->lambda[k] * w[i];
					g1 = bot3[i];
	  				vdot1 += lparm->R[k] * g1  * mit_factor * vinput2[i];
					vdot[i] = vdot1;
				}
			}
			base = n - 2 * defs->ngenes;
			for(i = base; i < base + defs->ngenes; i++) {
				k = i - base;
				vdot1 = -lparm->lambda[k] * v[i] - w[i];
				g1 = bot[i];
				vdot1 += lparm->R[k] * g1  * mit_factor;
				vdot1 += D[k] * ((v[i - defs->ngenes] - v[i]) + (v[i + defs->ngenes] - v[i]) );
				vdot1 -= M[k] * ( v[i - 3 * defs->ngenes] - 4 * v[i - 2 * defs->ngenes] + 6 * v[i - defs->ngenes] - 4 * v[i] + v[i + defs->ngenes] );
				vdot1 /= lparm->tau[k];
				vdot1 += -lparm->lambda[k] * w[i];
				g1 = bot3[i];
	  			vdot1 += lparm->R[k] * g1  * mit_factor * vinput2[i];
				vdot[i] = vdot1;
			}
/* last: posterior-most nucleus */
			base = n - defs->ngenes;
			for(i = base; i < base + defs->ngenes; i++) {
				k = i - base;
				vdot1 = -lparm->lambda[k] * v[i] - w[i];
				g1 = bot[i];
				vdot1 += lparm->R[k] * g1  * mit_factor;
				vdot1 += D[k] * (v[i - defs->ngenes] - v[i]);
				vdot1 -= M[k] * ( v[i - 4 * defs->ngenes] - 4 * v[i - 3 * defs->ngenes] + 6 * v[i - 2 * defs->ngenes] - 4 * v[i - defs->ngenes] +  v[i] );
				vdot1 /= lparm->tau[k];
				vdot1 += -lparm->lambda[k] * w[i];
				g1 = bot3[i];
	  			vdot1 += lparm->R[k] * g1  * mit_factor * vinput2[i];
				vdot[i] = vdot1;
			}
		}
/* during mitosis only diffusion and decay happen */
	} else if (rule == MITOSIS) {
		if ( n == defs->ngenes ) {         /* first part: one nuc, no diffusion */
			register double vdot1;
			for( base=0; base<n; base+=defs->ngenes ) {
				for( i=base; i<base+defs->ngenes; i++ ) {
					k = i - base;
					vdot1 = -lparm->lambda[k] * v[i];
					vdot[i] = vdot1;
				}
			}
		} else {                      /* then for multiple nuclei -> diffusion */
			register double vdot1,g1;
			for(i = 0; i < defs->ngenes; i++) {     /* first anterior-most nucleus */
				k = i;
				vdot1 = -lparm->lambda[k] * v[i] - w[i];
				vdot1 += D[k] * (v[i + defs->ngenes] - v[i]) * boundary;
				vdot1 -= M[k] * ( v[i] - 4 * v[i + defs->ngenes] + 6 * v[i + 2 * defs->ngenes] - 4 * v[i + 3 * defs->ngenes] +  v[i + 4 * defs->ngenes] );
				vdot1 /= lparm->tau[k];
				vdot1 += -lparm->lambda[k] * w[i];
				vdot[i] = vdot1;
			}
			for(i = defs->ngenes; i < 2 * defs->ngenes; i++) {     /* first anterior-most nucleus */
				k = i - defs->ngenes;
				vdot1 = -lparm->lambda[k] * v[i] - w[i];
				vdot1 += D[k] * ((v[i - defs->ngenes] - v[i]) + (v[i + defs->ngenes] - v[i]) );
				vdot1 -= M[k] * ( v[i - defs->ngenes] - 4 * v[i] + 6 * v[i + defs->ngenes] - 4 * v[i + 2 * defs->ngenes] +  v[i + 3 * defs->ngenes] );
				vdot1 /= lparm->tau[k];
				vdot1 += -lparm->lambda[k] * w[i];
				vdot[i] = vdot1;
			}
/* then middle nuclei */
			for(base = 2 * defs->ngenes; base < n - 2 * defs->ngenes; base += defs->ngenes) {
				for(i = base; i < base + defs->ngenes; i++) {
					k = i - base;
					vdot1 = -lparm->lambda[k] * v[i] - w[i];
					vdot1 += D[k] * ((v[i - defs->ngenes] - v[i]) + (v[i + defs->ngenes] - v[i]) );
					vdot1 -= M[k] * ( v[i - 2 * defs->ngenes] - 4 * v[i - defs->ngenes] + 6 * v[i] - 4 * v[i + defs->ngenes] +  v[i + 2 * defs->ngenes] );
					vdot1 /= lparm->tau[k];
					vdot1 += -lparm->lambda[k] * w[i];
					vdot[i] = vdot1;
				}
			}
			base = n - 2 * defs->ngenes;
			for(i = base; i < base + defs->ngenes; i++) {
				k = i - base;
				vdot1 = -lparm->lambda[k] * v[i] - w[i];
				vdot1 += D[k] * ((v[i - defs->ngenes] - v[i]) + (v[i + defs->ngenes] - v[i]) );
				vdot1 -= M[k] * ( v[i - 3 * defs->ngenes] - 4 * v[i - 2 * defs->ngenes] + 6 * v[i - defs->ngenes] - 4 * v[i] + v[i + defs->ngenes] );
				vdot1 /= lparm->tau[k];
				vdot1 += -lparm->lambda[k] * w[i];
				vdot[i] = vdot1;
			}
/* last: posterior-most nucleus */
			base = n - defs->ngenes;
			for(i = base; i < base + defs->ngenes; i++) {
				k = i - base;
				vdot1 = -lparm->lambda[k] * v[i] - w[i];
				vdot1 += D[k] * (v[i - defs->ngenes] - v[i]);
				vdot1 -= M[k] * ( v[i - 4 * defs->ngenes] - 4 * v[i - 3 * defs->ngenes] + 6 * v[i - 2 * defs->ngenes] - 4 * v[i - defs->ngenes] +  v[i] );
				vdot1 /= lparm->tau[k];
				vdot1 += -lparm->lambda[k] * w[i];
				vdot[i] = vdot1;
			}
		}
	} else
		error("Dvdt2Orig: Bad rule %i sent to DvdtOrig", rule);
}


/*** JACOBIAN FUNCTION(S) **************************************************/

/*** JacobnOrig: Jacobian function for the DvdtOrig model; calculates the **
 *               Jacobian matrix (matrix of partial derivatives) for the   *
 *               equations at a give time t; input concentrations come in  *
 *               v (of size n), the Jacobian is returned in jac; note that *
 *               all dfdt's are zero in our case since our equations are   *
 *               autonomous (i.e. have no explicit t in them)              *
 ***************************************************************************
 *                                                                         *
 * The Equation: dv/dx = Rg(u) + D() + lambda()                            *
 *                                                                         *
 * The Jacobian: (in this example: nnucs = 3, ngenes = 3                   *
 *                                                                         *
 *                        i = 1       i = 2        i = 3                   *
 *         b =          1   2   3   1   2   3    1   2   3                 *
 *                                                                         *
 *         a = 1      X11 Y12 Y13  D1   0   0    0   0   0                 *
 * i = 1   a = 2      Y21 X22 Y23   0  D2   0    0   0   0                 *
 *         a = 3      Y31 Y32 X33   0   0  D3    0   0   0                 *
 *                                                                         *
 *         a = 1       D1   0   0 X11 Y12 Y13   D1   0   0                 *
 * i = 2   a = 2        0  D2   0 Y21 X22 Y23    0  D2   0                 *
 *         a = 3        0   0  D3 Y31 Y32 X33    0   0  D3                 *
 *                                                                         *
 *         a = 1        0   0   0  D1   0   0  X11 Y12 Y13                 *
 * i = 3   a = 2        0   0   0   0  D2   0  Y21 X22 Y23                 *
 *         a = 3        0   0   0   0   0   0  Y31 Y32 Y33                 *
 *                                                                         *
 * Where:  Xab = Rg'(u) - 2D{a} - lambda{a}                                *
 *         Yab = Rg'(u)                                                    *
 *         Da  = D{a}                                                      *
 *                                                                         *
 ***************************************************************************/
/* It seems that jacs are not used
void JacobnOrig(double t, double *v, double *dfdt, double **jac, int n)
{
  int        m;                                        /* number of nuclei
  int        ap;                 /* nuclear index on AP axis [0,1,...,m-1]
  int        i, j;                                  /* local loop counters
  int        k, kk;               /* index of gene k in a specific nucleus
  int        base;            /* indices of 1st gene in a specific nucleus
#ifdef ALPHA_DU
  int        incx = 1;        /* increment step size for vsqrt input array
  int        incy = 1;       /* increment step size for vsqrt output array
#endif

  static int num_nucs  = 0;      /* store the number of nucs for next step
  static int bcd_index = 0;   /* the *next* array in bicoid struct for bcd
  static DArrPtr *bcd;              /* pointer to appropriate bicoid struct


/* get D parameters and bicoid gradient according to cleavage cycle

  m = n / defs->ngenes;                        /* m is the number of nuclei
  if (m != num_nucs) {      /* time-varying quantities only vary by ccycle
    GetD(t, lparm->d, defs->diff_schedule, D);
                      /* get diff coefficients, according to diff schedule
    if (num_nucs > m)                  /* started a new iteration in score
      bcd_index = 0;                           /* -> start all over again!
    num_nucs = m;                         /* store # of nucs for next step
    bcd = GetBicoid(t, genotype);                   /* get bicoid gradient
    if( bcd.size != num_nucs)
     error("JacobnOrig: %d nuclei don't match Bicoid!", num_nucs);
    bcd_index++;                   /* store index for next bicoid gradient
  }

/*** INTERPHASE rule *******************************************************

  if (rule == INTERPHASE) {

    register double vinput1 = 0;                    /* used to calculate u
    register double gdot1, vdot1;     /* used to calculate Xab's and Yab's


/***************************************************************************
 *                                                                         *
 *  g(u)  = 1/2 * ( u / sqrt(1 + u^2) + 1)                                 *
 *  g'(u) = 1/2 * ( 1 / ((1 + u^2)^3/2)) * T{ab}                           *
 *                                                                         *
 ***************************************************************************

    if ( gofu == Sqrt ) {

/* this looks confusing, but it's actually quite simple: the two loops be- *
 * low are in reality just one that loops over the first dimension of the  *
 * Jacobian (size n) 'nuclear block' by 'nuclear block' (the i's in the    *
 * long introductory comment above); ap keeps track of the nucleus number, *
 * k keeps track of which gene we're dealing with; bot2 saves 1+u^2

  for (base=0, ap=0; base<n ; base+=defs->ngenes, ap++) {
  	for (i=base; i < base+defs->ngenes; i++) {

	  k = i - base;

	  vinput1  = lparm->h[k];
	  vinput1 += lparm->m[k] * bcd.array[ap];

	  for(j=0; j < defs->ngenes; j++)
	    vinput1 += lparm->T[(k*defs->ngenes)+j] * v[base + j];

	  bot2[i] = 1 + vinput1 * vinput1;
	 }
 }

/* now calculate sqrt(1+u^2); store it in bot[]

#ifdef ALPHA_DU
      vsqrt_(bot2,&incx,bot,&incy,&n);    /* superfast DEC vector function
#else
     for(i=0; i < n; i++)                  /* slow traditional style sqrt
    	bot[i] = sqrt(bot2[i]);
#endif

/* resume loop after vector sqrt above; we finish calculating g'(u) and    *
 * place D's in the diagonal of the off-diagonal blocks (cf diagram above)

  for (base=0; base < n; base+=defs->ngenes) {
  	for (i=base; i < base+defs->ngenes; i++) {

	   k = i - base;

	   gdot1  = 1 / (bot[i] * bot2[i]);
	   gdot1 *= lparm->R[k] * 0.5;

     for (j=base; j < base+defs->ngenes; j++) {

	    kk = j - base;

	    vdot1 = lparm->T[(k*defs->ngenes)+kk] * gdot1;

      if ( k == kk ) {
	      if ( n > defs->ngenes ) {
		      if ( base > 0 && base < n-defs->ngenes ) {
      		  vdot1 -= 2. * D[k];
		      } else {
		        vdot1 -= D[k];
          }
         }
	       vdot1 -= lparm->lambda[k];
        }
	   jac[i][j] = vdot1;
	  }

	  if ( base > 0 )
	    jac[i][i-defs->ngenes] = D[k];

	  if ( base < n-defs->ngenes )
	    jac[i][i+defs->ngenes] = D[k];

	}
 }

/***************************************************************************
 *                                                                         *
 * g(u)  = 1/2 * (tanh(u) + 1) )  or  g(u)  = 1 / ( 1 + e^(-2u))           *
 * g'(u) = Sech(u)^2 /2           or  g'(u) = 2e^(-2u) / (1 + e^(-2u))^2   *
 *                                                                         *
 ***************************************************************************
 *                                                                         *
 * These are actually the same function in different guises; we implement  *
 * Exp below since it's faster                                             *
 *                                                                         *
 ***************************************************************************

    } else if ( (gofu == Tanh) || (gofu == Exp) ) {

/* this looks confusing, but it's actually quite simple: the two loops be- *
 * low are in reality just one that loops over the first dimension of the  *
 * Jacobian (size n) 'nuclear block' by 'nuclear block' (the i's in the    *
 * long introductory comment above); ap keeps track of the nucleus number, *
 * k keeps track of which gene we're dealing with; bot2 saves 1+u^2

      for (base=0, ap=0; base<n ; base+=defs->ngenes, ap++) {
	for (i=base; i < base+defs->ngenes; i++) {

	  k = i - base;

	  vinput1  = lparm->h[k];
	  vinput1 += lparm->m[k] * bcd.array[ap];

	  for(j=0; j < defs->ngenes; j++)
	    vinput1 += lparm->T[(k*defs->ngenes)+j] * v[base + j];

	  bot[i] = -2.0 * vinput1;
	}
      }

/* now calculate exp(-u); store it in bot[]
#ifdef ALPHA_DU
      vexp_(bot,&incx,bot2,&incy,&n);   /* superfast DEC vector function
#else
      for (i=0; i<n; i++)                    /* slow traditional style exp
	bot2[i] = exp(bot[i]);
#endif

/* resume loop after vector exp above; we finish calculating g'(u) and     *
 * place D's in the diagonal of the off-diagonal blocks (cf diagram above)

      for (base=0; base < n; base+=defs->ngenes) {
	for (i=base; i < base+defs->ngenes; i++) {

	  k = i - base;

	  gdot1  = 2. * bot2[i];
	  gdot1 /= (1. + bot2[i]) * (1. + bot2[i]);
	  gdot1 *= lparm->R[k];

	  for (j=base; j < base+defs->ngenes; j++) {

	    kk = j - base;

	    vdot1 = lparm->T[(k*defs->ngenes)+kk] * gdot1;
	    if ( k == kk ) {
	      if ( n > defs->ngenes ) {
      		if ( base > 0 && base < n-defs->ngenes ) {
		        vdot1 -= 2. * D[k];
           } else {
      		  vdot1 -= D[k];
           }
         }
	      vdot1 -= lparm->lambda[k];
	    }
	    jac[i][j] = vdot1;

	  }

	  if ( base > 0 )
	    jac[i][i-defs->ngenes] = D[k];

	  if ( base < n-defs->ngenes )
	    jac[i][i+defs->ngenes] = D[k];

	}
      }

/*** semi-implicit solvers are NOT allowed with heaviside g(u) ************

    } else if ( gofu == Hvs ) {
      error("JacobnOrig: can't use semi-implicit solver on heaviside g(u)!");

    } else {
      error("JacobnOrig: unknown g(u) function!\n");
    }

/* during mitosis only diffusion and decay happen

  } else if (rule == MITOSIS) {

    register double vdot1;

       for (base=0; base < n; base+=defs->ngenes) {
	for (i=base; i < base+defs->ngenes; i++) {
	  k = i - base;

	  for (j=base; j < base+defs->ngenes; j++) {
	    kk = j - base;

	    if ( k == kk ) {
	      vdot1  = -lparm->lambda[k];
	      if ( n < defs->ngenes ) {
      		if ( base > 0 && base < n-defs->ngenes ) {
		        vdot1 -= 2. * D[k];
      		} else {
    		    vdot1 -= D[k];
          }
        }
	    } else
	      vdot1 = 0.;

	    jac[i][j] = vdot1;

	  }

	  if ( base > 0 )
	    jac[i][i-defs->ngenes] = D[k];

	  if ( base < n-defs->ngenes )
	    jac[i][i+defs->ngenes] = D[k];

	}
      }

  } else
    error("JacobnOrig: Bad rule %i sent to JacobnOrig", rule);

  return;

}

/*** GUTS FUNCTIONS ********************************************************/

/*** CalcGuts: calculates guts for genotpye 'gtype' using unfold output in *
 *             'table' and the guts string 'gutsdefs'; it returns the num- *
 *             ber of columns we'll need to print and the guts table in    *
 *             'gtable'                                                    *
 ***************************************************************************/
/* guts are left away for a while*/
//int CalcGuts(char *gtype, NArrPtr table, NArrPtr table2, NArrPtr **gtable, char *gutsdefs)
NArrPtr *CalcGuts(char *gtype, NArrPtr*table, NArrPtr *table2, int *numguts, char *gutsdefs)
{
	int           m; /* number of nuclei at each time */
	int           i; /* just for the loops */
	int           which;               /* for which gene to calculate guts */
	int           l_rule;                             /* local copy for rule */
	unsigned int l_ccycle = 0;
	unsigned long *gutcomps = NULL;  /* bit flags for each word of ID string */
	NArrPtr       *gutsy;       /* temporary place for guts, to be returned */
	char          **w     = NULL;       /* w(ord) string array after parsing */
	char          **dummy = NULL;        /* beginning of w(ord) string array */
	char          *targetptr;     /* for parsing the gutsdefs, and a counter */
	char*art_couples_ids = "1234567890_-+=|~!@#$%^&*()<>?/";
	int all_length;
	char *all_gene_ids;
/* allocate memory for parsed gut strings, then parse the string */
	dummy = (char **)calloc(MAX_RECORD, sizeof(char *));
	w = dummy;
	*numguts = (ParseString(gutsdefs, w) - 1);   /* w contains array of words */
/* if string is empty -> return zero */
	if ( *numguts == -1 ) {
		while (*++w)
			free(*w);
		free(*dummy);
		free(dummy);
		(*numguts)++;
		return NULL;
	}
/* if no guts specified but gene name in string -> error */
	if ( *numguts == 0 )
		error("CalcGuts: no guts specified for target gene %s", w[0]);
/* allocate the gutcomps array and fill it with the bit flags correspon-   *
 * ding to the things that need to get calculated below; each word in an   *
 * ID string corresponds to one long int in the gutcomps array; targetptr  *
 * points to the name of the gene in the geneID string for which we cal-   *
 * culate guts                                                             */
	gutcomps = (unsigned long *)calloc(*numguts, sizeof(unsigned long));
	all_length = defs->ngenes + defs->mgenes + defs->egenes;
	all_gene_ids = (char *)calloc(MAX_RECORD, sizeof(char));
	all_gene_ids = strcpy(all_gene_ids, defs->gene_ids);
	all_gene_ids = strcat(all_gene_ids, defs->mgene_ids);
	all_gene_ids = strcat(all_gene_ids, defs->egene_ids);
	for ( i = 0; i < defs->ncouples; i++) {
		all_gene_ids[ all_length ] = art_couples_ids[i];
		all_length++;
		all_gene_ids[ all_length ] = '\0';
	}
	if ( !(targetptr = GetGutsComps(all_gene_ids, w, gutcomps)) )
		error("CalcGuts: target gene (%s) is not defined in the geneID string", w[0]);
/* allocate gutsy array and set the right genotype in score.c & zygotic.c */
	gutsy = (NArrPtr*)malloc(sizeof(NArrPtr));
	gutsy->size  = table->size;
	gutsy->array = (NucState *)calloc(table->size, sizeof(NucState));
	InitGenotypeIndex(gtype);
/*	Mutate(Name2Gtype(gtype));*/
/* which tells us which gene we calculate guts for */
	which = targetptr - all_gene_ids;
/* free gut ID strings, we don't need them anymore since we have gutcomps */
	while (*++w)
		free(*w);
	free(*dummy);
	free(dummy);
/* then start calculating guts */
	for (i=0; i < table->size; i++) {
		gutsy->array[i].time = table->array[i].time;
/* calculate size of 2D array that holds guts and allocate memory */
		m = table->array[i].state->size / defs->ngenes;
		gutsy->array[i].state = (DArrPtr*)malloc(sizeof(DArrPtr));
		gutsy->array[i].state->size  = (*numguts) * m;
		gutsy->array[i].state->array = (double *)calloc(gutsy->array[i].state->size, sizeof(double));
/* set whether it is MITOSIS or INTERPHASE when calculating guts */
		l_rule = GetRule(gutsy->array[i].time);
		SetRule(l_rule);
		l_ccycle = GetCCycle(gutsy->array[i].time);
		SetCCycle(l_ccycle);
		SetNNucs(m);
/* call the function that calculates the internals of the RHS */
		CalcRhs(table->array[i].state->array, table2->array[i].state->array, gutsy->array[i].time, gutsy->array[i].state->array, table->array[i].state->size, gutsy->array[i].state->size, *numguts, which, gutcomps);
	}
/* clean up */
	free(all_gene_ids);
	free(gutcomps);
/* return the guts and number of columns for PrintGuts() */
	return gutsy;
}

/*** CalcRhs: calculates the components of the right-hand-side (RHS) of ****
 *            the equatiion which make up guts (e.g. regulatory contribu-  *
 *            tions of specific genes, diffusion or protein decay); this   *
 *            function makes use of the derivative function and calculates *
 *            everything outside g(u) in reverse, i.e. starting from the   *
 *            derivative and calculating the desired gut properties back-  *
 *            wards from there; everything within g(u) is simply recon-    *
 *            structed from concentrations and parameters                  *
 ***************************************************************************/

void CalcRhs(double *v, double *w, double t, double *guts, int n, int gn, int numguts, int which, unsigned long *gutcomps)
{
	int    ap;                     /* nuclear index on AP axis [0,1,...,m-1] */
	int    i, j, k;                                            /* loop counters */
	int    base, base1;     /* index counters for data and guts respectively */
	unsigned long *tempctr;     /* temporary counter for traversing gutcomps */
	double *deriv;           /* array for the derivatives at a specific time */
	static unsigned int samecycle = 0;
/* the following calcululates appropriate D's according to the diffusion   *
 * schedule, gets the bicoid gradient and complains if anything is wrong;  *
 * this all only happens after a cell division or at the beginning         */
	if (ccycle != samecycle) {
		GetD(t, lparm->d, defs->diff_schedule, D);
		GetRCoef(t, &mit_factor);
		vmat = gcmd_get_maternal_inputs(ccycle);
		samecycle = ccycle;
	}
/*
	Guts Definition Section (Title: $gutsdefs)
------------------------------------------

CAUTION: The guts code uses the GENE ID STRING in the "The Problem"
section to identify the genes, and so the parameters section and the
gutsdefs sections must agree with it.

Unfold, if run with the -G flag looks in this section for the
definition of what contributions of the RHS of the model ("guts") are
required to be calculated.

It calculates both contributions of different genes to u
(T(a,b)*v(b)) (contribution of gene b to a), and also the decay,
diffusion, synthesis and the derivative terms in the RHS.

Both the target gene (for which the guts is required) and the genes
whose contributions to u (in g(u)) are to be calculated are specified
by letters of the gene ID string (see the problem definition
section):

        B       bicoid                          bcd
        C       caudal                          cad
        E       even-skipped                    eve
        F       fushi tarazu                    ftz
        G       giant                           gt
        H       hunchback                       hb
        I       hairy                           h
        J       huckebein                       j
        K       Kruppel                         Kr
        N       knirps                          kni
        O       odd-paired                      odd
        P       paired                          prd
        R       runt                            run
        S       sloppy-paired                   slp
        T       tailless                        tll

The other contributions are specified as:

        A       Threshold term in u, h(a)
        U       u
        Z       g(u)
        Q       Synthesis, R(a)*g(u)
        L       Decay term, -Lambda(a)*v(a)
        X       Left Diffusion D(a)*(v(a,i-1) - v(a,i))
        Y       Right Diffusion D(a)*(v(a,i+1) - v(a,i))
        D       The derivative i.e. the whole RHS
        V
        W

The first character in the line is the target gene. After it
seperated by spaces or tabs follow the contributions that are required
to be calculated. If two or more contribution terms are placed together
without space between them, the sum of these contributions is
calculated.

For example, if one wants the combined contribution of Kruppel and
Sloppy-Paired to Tailless, and also the derivative, one would add
this line to the gutsdefs section:

T KS D

One may specify any number contribution terms as long as one
definition line remains less than 255 characters (anything more than
that is ignored). However, one may have any number of such lines
with any other or the same target gene. White space is ignored.

Each line's output is put in a section in the output file with each
contribution in a column.

*/
	if ( defs->egenes )
		gcmd_get_external_inputs(t, vext, num_nucs * defs->egenes);
/* call the derivative function to get the total derivative */
	deriv = (double *)calloc(n, sizeof(double));
    if ( model == HEDIRECT || model == HESRR || model == HEURR || model == HEQUENCHING ) {
         DddwwdtGuts(v, t, deriv, n);
    } else {
        if ( defs->ncouples )
            gcmd_get_coupled_inputs_1(t, vcouple, num_nucs * defs->ncouples, v, n, vext, num_nucs * defs->egenes, vmat, num_nucs * defs->mgenes);
        DvdtOrig(v, t, deriv, n);
    }
/* all the code below calculates the requested guts (by checking the bits  *
 * set in the gutcomps array); it does so by forward reconstructing all    *
 * the guts that are part of u and then reverse calculating (starting from *
 * the derivative) the guts outside g(u) such that guts work for any g(u); *
 * most of this is directly derived from the derivative function(s) above  */
/* the next block of code calculates guts which are within u, i.e. all the *
 * regulatory contributions or the threshold of a certain gene             */
	for (base = 0, base1 = 0, ap=0; base < n, base1 < gn; base += defs->ngenes, base1 += numguts, ap++) {
		tempctr = gutcomps;
		for ( i = base1; i < base1 + numguts; i++) {
			for( j = 0; j < defs->ngenes; j++)
				if ( (*tempctr >> j) % 2) {
#ifdef HBHACK
					int left_border = 13;//7;
					int right_border = 30;//24;
					double autocoef = 0.7;
					double gthbcoef = 0;
					if ( num_nucs == defs->nnucs && j == 2 && which == 0 && left_border < ap && ap < right_border ) {
/*						fprintf(stderr, "HBHACK hitted\n");*/
						guts[i] += gthbcoef * lparm->T[(which*defs->ngenes)+j]*v[base + j];
					} else if ( num_nucs == defs->nnucs && j == 2 && which == 2 && left_border < ap && ap < right_border ) {
/*						fprintf(stderr, "HBHACK hitted\n");*/
						guts[i] += autocoef * lparm->T[(which*defs->ngenes)+j]*v[base + j];
					} else if ( num_nucs == defs->nnucs && j == 3 && which == 3 && left_border < ap && ap < right_border ) {
/*						fprintf(stderr, "HBHACK hitted\n");*/
						guts[i] += autocoef * lparm->T[(which*defs->ngenes)+j]*v[base + j];
					} else {
#endif
					guts[i] += lparm->T[(which*defs->ngenes)+j]*v[base + j];
#ifdef HBHACK
					}
#endif
				}
			for( j = 0; j < defs->mgenes; j++)
				if ( (*tempctr >> defs->ngenes + j) % 2)
					guts[i] += lparm->M[(which*defs->mgenes)+j] * vmat[ap*defs->mgenes + j];
			for( j = 0; j < defs->egenes; j++)
				if ( (*tempctr >> defs->ngenes + defs->mgenes + j) % 2)
					guts[i] += lparm->E[(which*defs->egenes)+j] * vext[ap*defs->egenes + j];
			for( j = 0; j < defs->ncouples; j++)
				if ( (*tempctr >> defs->ngenes + defs->mgenes + defs->egenes + j) % 2)
					guts[i] += lparm->P[(which*defs->ncouples)+j] * vcouple[ap * defs->ncouples + j];
			if ( (*tempctr >> defs->ngenes + defs->mgenes + defs->egenes + defs->ncouples) % 2)
				guts[i] += lparm->h[which];
			tempctr++;
		}
	}
	if (rule == INTERPHASE) {
		tempctr = gutcomps;
		k = which;
		for(i = 0; i < numguts; i++) {     /* first anterior-most nucleus */
			if ( (*tempctr >> defs->ngenes + defs->mgenes + defs->egenes + defs->ncouples + 1) % 2)
				guts[i] += -lparm->lambda[which] * v[k];
			if ( (*tempctr >> defs->ngenes + defs->mgenes + defs->egenes + defs->ncouples + 5) % 2)
				guts[i] += deriv[k] - D[which] * (v[k + defs->ngenes] - v[k]) * boundary + lparm->lambda[which] * v[k];
			if ( (*tempctr >> defs->ngenes + defs->mgenes + defs->egenes + defs->ncouples + 3) % 2)
				guts[i] += D[which] * (v[k + defs->ngenes] - v[k]);
			if ( (*tempctr >> defs->ngenes + defs->mgenes + defs->egenes + defs->ncouples + 4) % 2)
				guts[i] += deriv[k];
			tempctr++;
		}
/* then middle nuclei */
		for(base = defs->ngenes, base1 = numguts; base < n - defs->ngenes, base1 < gn - numguts; base += defs->ngenes, base1 += numguts) {
			k = which + base;
			tempctr = gutcomps;
			for(i = base1; i < base1 + numguts; i++) {
				if ( (*tempctr >> defs->ngenes + defs->mgenes + defs->egenes + defs->ncouples + 1) % 2)
					guts[i] += -lparm->lambda[which] * v[k];
				if ( (*tempctr >> defs->ngenes + defs->mgenes + defs->egenes + defs->ncouples + 5) % 2)
					guts[i] += deriv[k] - D[which] * ( (v[k - defs->ngenes] - v[k]) + (v[k + defs->ngenes] - v[k]) ) + lparm->lambda[which] * v[k];
				if ( (*tempctr >> defs->ngenes + defs->mgenes + defs->egenes + defs->ncouples + 2) % 2)
					guts[i] += D[which] * (v[k - defs->ngenes] - v[k]);
				if ( (*tempctr >> defs->ngenes + defs->mgenes + defs->egenes + defs->ncouples + 3) % 2)
					guts[i] += D[which] * (v[k + defs->ngenes] - v[k]);
				if ( (*tempctr >> defs->ngenes + defs->mgenes + defs->egenes + defs->ncouples + 4) % 2)
					guts[i] += deriv[k];
				tempctr++;
			}
		}
/* last: posterior-most nucleus */
		tempctr = gutcomps;
		k = which + n - defs->ngenes;
		base1 = gn - numguts;
		for(i = base1; i < gn; i++) {
			if ( (*tempctr >> defs->ngenes + defs->mgenes + defs->egenes + defs->ncouples + 1) % 2)
				guts[i] += -lparm->lambda[which] * v[k];
			if ( (*tempctr >> defs->ngenes + defs->mgenes + defs->egenes + defs->ncouples + 5) % 2)
				guts[i] += deriv[k] - D[which] * (v[k - defs->ngenes] - v[k]) * boundary + lparm->lambda[which] * v[k];
			if ( (*tempctr >> defs->ngenes + defs->mgenes + defs->egenes + defs->ncouples + 2) % 2)
				guts[i] += D[which] * (v[k - defs->ngenes] - v[k]);
			if ( (*tempctr >> defs->ngenes + defs->mgenes + defs->egenes + defs->ncouples + 4) % 2)
				guts[i] += deriv[k];
			tempctr++;
		}
/* during mitosis only diffusion and decay happen */
	} else if (rule == MITOSIS) {
		tempctr = gutcomps;
		k = which;
		for(i = 0; i < numguts; i++) {
			if ( (*tempctr >> defs->ngenes + defs->mgenes + defs->egenes + defs->ncouples + 1) % 2)
				guts[i] += -lparm->lambda[which] * v[k];
			if ( (*tempctr >> defs->ngenes + defs->mgenes + defs->egenes + defs->ncouples + 3) % 2)
				guts[i] += D[which] * (v[k + defs->ngenes] - v[k]);
			if ( (*tempctr >> defs->ngenes + defs->mgenes + defs->egenes + defs->ncouples + 4) % 2)
				guts[i] += deriv[k];
			tempctr++;
		}
/* then middle nuclei */
		for(base = defs->ngenes, base1 = numguts; base < n - defs->ngenes, base1 < gn - numguts; base += defs->ngenes, base1 += numguts) {
			k = which + base;
			tempctr = gutcomps;
			for(i = base1; i < base1 + numguts; i++) {
				if ( (*tempctr >> defs->ngenes + defs->mgenes + defs->egenes + defs->ncouples + 1) % 2)
					guts[i] += -lparm->lambda[which] * v[k];
				if ( (*tempctr >> defs->ngenes + defs->mgenes + defs->egenes + defs->ncouples + 2) % 2)
					guts[i] += D[which] * (v[k - defs->ngenes] - v[k]);
				if ( (*tempctr >> defs->ngenes + defs->mgenes + defs->egenes + defs->ncouples + 3) % 2)
					guts[i] += D[which] * (v[k + defs->ngenes] - v[k]);
				if ( (*tempctr >> defs->ngenes + defs->mgenes + defs->egenes + defs->ncouples + 4) % 2)
					guts[i] += deriv[k];
				tempctr++;
			}
		}
/* last: posterior-most nucleus */
		tempctr = gutcomps;
		k = which + n - defs->ngenes;
		base1 = gn - numguts;
		for(i = base1; i < gn; i++) {
			if ( (*tempctr >> defs->ngenes + defs->mgenes + defs->egenes + defs->ncouples + 1) % 2)
				guts[i] += -lparm->lambda[which] * v[k];
			if ( (*tempctr >> defs->ngenes + defs->mgenes + defs->egenes + defs->ncouples + 2) % 2)
				guts[i] += D[which] * (v[k - defs->ngenes] - v[k]);
			if ( (*tempctr >> defs->ngenes + defs->mgenes + defs->egenes + defs->ncouples + 4) % 2)
				guts[i] += deriv[k];
			tempctr++;
		}
	}
	free(deriv);
	return;
}

void CalcRhs2(double *v, double *w, double t, double *guts, int n, int gn, int numguts, int which, unsigned long *gutcomps)
{
	int    ap;                     /* nuclear index on AP axis [0,1,...,m-1] */
	int    i, j, k;                                            /* loop counters */
	int    base, base1;     /* index counters for data and guts respectively */
	unsigned long *tempctr;     /* temporary counter for traversing gutcomps */
	double *deriv;           /* array for the derivatives at a specific time */
	static unsigned int samecycle = 0;
/* the following calcululates appropriate D's according to the diffusion   *
 * schedule, gets the bicoid gradient and complains if anything is wrong;  *
 * this all only happens after a cell division or at the beginning         */
	if (ccycle != samecycle) {
		GetD(t, lparm->d, defs->diff_schedule, D);
		GetM(t, lparm->m, defs->mob_schedule, M);
		GetMM(t, lparm->m, defs->mob_schedule, MM);
		GetRCoef(t, &mit_factor);
		vmat = gcmd_get_maternal_inputs(ccycle);
		samecycle = ccycle;
	}
/*
	Guts Definition Section (Title: $gutsdefs)
------------------------------------------

CAUTION: The guts code uses the GENE ID STRING in the "The Problem"
section to identify the genes, and so the parameters section and the
gutsdefs sections must agree with it.

Unfold, if run with the -G flag looks in this section for the
definition of what contributions of the RHS of the model ("guts") are
required to be calculated.

It calculates both contributions of different genes to u
(T(a,b)*v(b)) (contribution of gene b to a), and also the decay,
diffusion, synthesis and the derivative terms in the RHS.

Both the target gene (for which the guts is required) and the genes
whose contributions to u (in g(u)) are to be calculated are specified
by letters of the gene ID string (see the problem definition
section):

        B       bicoid                          bcd
        C       caudal                          cad
        E       even-skipped                    eve
        F       fushi tarazu                    ftz
        G       giant                           gt
        H       hunchback                       hb
        I       hairy                           h
        K       Kruppel                         Kr
        N       knirps                          kni
        O       odd-paired                      odd
        P       paired                          prd
        R       runt                            run
        S       sloppy-paired                   slp
        T       tailless                        tll

The other contributions are specified as:

        A       Threshold term in u, h(a)
        U       u
        Z       g(u)
        J       Synthesis, R(a)*g(u)
        L       Decay term, -Lambda(a)*v(a)
        X       Left Diffusion D(a)*(v(a,i-1) - v(a,i))
        Y       Right Diffusion D(a)*(v(a,i+1) - v(a,i))
        D       The derivative i.e. the whole RHS

The first character in the line is the target gene. After it
seperated by spaces or tabs follow the contributions that are required
to be calculated. If two or more contribution terms are placed together
without space between them, the sum of these contributions is
calculated.

For example, if one wants the combined contribution of Kruppel and
Sloppy-Paired to Tailless, and also the derivative, one would add
this line to the gutsdefs section:

T KS D

One may specify any number contribution terms as long as one
definition line remains less than 255 characters (anything more than
that is ignored). However, one may have any number of such lines
with any other or the same target gene. White space is ignored.

Each line's output is put in a section in the output file with each
contribution in a column.

*/
	if ( defs->egenes )
		gcmd_get_external_inputs(t, vext, num_nucs * defs->egenes);
	if ( defs->ncouples )
		gcmd_get_coupled_inputs_1(t, vcouple, num_nucs * defs->ncouples, v, n, vext, num_nucs * defs->egenes, vmat, num_nucs * defs->mgenes);
/* call the derivative function to get the total derivative */
	deriv = (double *)calloc(n, sizeof(double));
	Dvdt2Orig(v, w, t, deriv, n);
/* all the code below calculates the requested guts (by checking the bits  *
 * set in the gutcomps array); it does so by forward reconstructing all    *
 * the guts that are part of u and then reverse calculating (starting from *
 * the derivative) the guts outside g(u) such that guts work for any g(u); *
 * most of this is directly derived from the derivative function(s) above  */
/* the next block of code calculates guts which are within u, i.e. all the *
 * regulatory contributions or the threshold of a certain gene             */
	for (base = 0, base1 = 0, ap=0; base < n, base1 < gn; base += defs->ngenes, base1 += numguts, ap++) {
		tempctr = gutcomps;
		for ( i = base1; i < base1+numguts; i++) {
			for( j = 0; j < defs->ngenes; j++)
				if ( (*tempctr >> j) % 2)
					guts[i] += lparm->T[(which*defs->ngenes)+j]*v[base + j];
			for( j = 0; j < defs->mgenes; j++)
				if ( (*tempctr >> defs->ngenes + j) % 2)
					guts[i] += lparm->M[(which*defs->mgenes)+j] * vmat[ap*defs->mgenes + j];
			for( j = 0; j < defs->egenes; j++)
				if ( (*tempctr >> defs->ngenes + defs->mgenes + j) % 2)
					guts[i] += lparm->E[(which*defs->egenes)+j] * vext[ap*defs->egenes + j];
			if ( (*tempctr >> defs->ngenes + defs->mgenes + defs->egenes) % 2)
				guts[i] += lparm->h[which];
			tempctr++;
		}
	}
	if (rule == INTERPHASE) {
		tempctr = gutcomps;
		for(i = 0; i < numguts; i++) {     /* first anterior-most nucleus */
			if ( (*tempctr >> defs->ngenes + defs->mgenes + defs->egenes + 3) % 2)
				guts[i] += D[which] * (v[k + defs->ngenes] - v[k]);
			if ( (*tempctr >> defs->ngenes + defs->mgenes + defs->egenes + 4) % 2)
				guts[i] += deriv[which];
			if ( (*tempctr >> defs->ngenes + defs->mgenes + defs->egenes + 7) % 2)
				guts[i] += -M[which] * ( v[which] - 4 * v[which + defs->ngenes] + 6 * v[which + 2 * defs->ngenes] - 4 * v[which + 3 * defs->ngenes] +  v[which + 4 * defs->ngenes] );
			tempctr++;
		}
		tempctr = gutcomps;
		k = which + defs->ngenes;
		for(i = numguts; i < 2 * numguts; i++) {     /* first anterior-most nucleus */
			if ( (*tempctr >> defs->ngenes + defs->mgenes + defs->egenes + 2) % 2)
				guts[i] += D[which] * (v[k - defs->ngenes] - v[k]);
			if ( (*tempctr >> defs->ngenes + defs->mgenes + defs->egenes + 3) % 2)
				guts[i] += D[which] * (v[k + defs->ngenes] - v[k]);
			if ( (*tempctr >> defs->ngenes + defs->mgenes + defs->egenes + 4) % 2)
				guts[i] += deriv[which];
			if ( (*tempctr >> defs->ngenes + defs->mgenes + defs->egenes + 7) % 2)
				guts[i] += -M[which] * ( v[k - defs->ngenes] - 4 * v[k] + 6 * v[k + defs->ngenes] - 4 * v[k + 2 * defs->ngenes] +  v[k + 3 * defs->ngenes] );
			tempctr++;
		}
/* then middle nuclei */
		for(base = 2 * defs->ngenes, base1 = 2 * numguts; base < n - 2 * defs->ngenes, base1 < gn - 2 * numguts; base += defs->ngenes, base1 += numguts) {
			k = which + base;
			tempctr = gutcomps;
			for(i = base1; i < base1 + numguts; i++) {
				if ( (*tempctr >> defs->ngenes + defs->mgenes + defs->egenes + 2) % 2)
					guts[i] += D[which] * (v[k - defs->ngenes] - v[k]);
				if ( (*tempctr >> defs->ngenes + defs->mgenes + defs->egenes + 3) % 2)
					guts[i] += D[which] * (v[k + defs->ngenes] - v[k]);
				if ( (*tempctr >> defs->ngenes + defs->mgenes + defs->egenes + 4) % 2)
					guts[i] += deriv[which];
				if ( (*tempctr >> defs->ngenes + defs->mgenes + defs->egenes + 7) % 2)
					guts[i] += -M[which] * ( v[k - 2 * defs->ngenes] - 4 * v[k - defs->ngenes] + 6 * v[k] - 4 * v[k + defs->ngenes] +  v[k + 2 * defs->ngenes] );
				tempctr++;
			}
		}
		tempctr = gutcomps;
		base1 = gn - 2 * numguts;
		k = which + n - 2 * defs->ngenes;
		for(i = base1; i < base1 + numguts; i++) {
			if ( (*tempctr >> defs->ngenes + defs->mgenes + defs->egenes + 2) % 2)
				guts[i] += D[which] * (v[k - defs->ngenes] - v[k]);
			if ( (*tempctr >> defs->ngenes + defs->mgenes + defs->egenes + 3) % 2)
				guts[i] += D[which] * (v[k + defs->ngenes] - v[k]);
			if ( (*tempctr >> defs->ngenes + defs->mgenes + defs->egenes + 4) % 2)
				guts[i] += deriv[which];
			if ( (*tempctr >> defs->ngenes + defs->mgenes + defs->egenes + 7) % 2)
				guts[i] += -M[which] * ( v[k - 3 * defs->ngenes] - 4 * v[k - 2 * defs->ngenes] + 6 * v[k - defs->ngenes] - 4 * v[k] + v[k + defs->ngenes] );
			tempctr++;
		}
/* last: posterior-most nucleus */
		tempctr = gutcomps;
		k = which + n - defs->ngenes;
		base1 = gn - numguts;
		for(i = base1; i < gn; i++) {
			if ( (*tempctr >> defs->ngenes + defs->mgenes + defs->egenes + 2) % 2)
				guts[i] += D[which] * (v[k - defs->ngenes] - v[k]);
			if ( (*tempctr >> defs->ngenes + defs->mgenes + defs->egenes + 4) % 2)
				guts[i] += deriv[which];
			if ( (*tempctr >> defs->ngenes + defs->mgenes + defs->egenes + 7) % 2)
				guts[i] += -M[which] * ( v[k - 4 * defs->ngenes] - 4 * v[k - 3 * defs->ngenes] + 6 * v[k - 2 * defs->ngenes] - 4 * v[k - defs->ngenes] +  v[k] );
			tempctr++;
		}
/* during mitosis only diffusion and decay happen */
	} else if (rule == MITOSIS) {
		tempctr = gutcomps;
		for(i = 0; i < numguts; i++) {
			if ( (*tempctr >> defs->ngenes + defs->mgenes + defs->egenes + 3) % 2)
				guts[i] += D[which] * (v[k + defs->ngenes] - v[k]);
			if ( (*tempctr >> defs->ngenes + defs->mgenes + defs->egenes + 4) % 2)
				guts[i] += deriv[which];
			if ( (*tempctr >> defs->ngenes + defs->mgenes + defs->egenes + 8) % 2)
				guts[i] += -MM[which] * ( v[which] - 4 * v[which + defs->ngenes] + 6 * v[which + 2 * defs->ngenes] - 4 * v[which + 3 * defs->ngenes] +  v[which + 4 * defs->ngenes] );
			tempctr++;
		}
		tempctr = gutcomps;
		k = which + defs->ngenes;
		for(i = numguts; i < 2 * numguts; i++) {
			if ( (*tempctr >> defs->ngenes + defs->mgenes + defs->egenes + 2) % 2)
				guts[i] += D[which] * (v[k - defs->ngenes] - v[k]);
			if ( (*tempctr >> defs->ngenes + defs->mgenes + defs->egenes + 3) % 2)
				guts[i] += D[which] * (v[k + defs->ngenes] - v[k]);
			if ( (*tempctr >> defs->ngenes + defs->mgenes + defs->egenes + 4) % 2)
				guts[i] += deriv[which];
			if ( (*tempctr >> defs->ngenes + defs->mgenes + defs->egenes + 8) % 2)
				guts[i] += -MM[which] * ( v[k - defs->ngenes] - 4 * v[k] + 6 * v[k + defs->ngenes] - 4 * v[k + 2 * defs->ngenes] +  v[k + 3 * defs->ngenes] );
			tempctr++;
		}
/* then middle nuclei */
		for(base = 2 * defs->ngenes, base1 = 2 * numguts; base < n - 2 * defs->ngenes, base1 < gn - 2 * numguts; base += defs->ngenes, base1 += numguts) {
			k = which + base;
			tempctr = gutcomps;
			for(i = base1; i < base1 + numguts; i++) {
				if ( (*tempctr >> defs->ngenes + defs->mgenes + defs->egenes + 2) % 2)
					guts[i] += D[which] * (v[k - defs->ngenes] - v[k]);
				if ( (*tempctr >> defs->ngenes + defs->mgenes + defs->egenes + 3) % 2)
					guts[i] += D[which] * (v[k + defs->ngenes] - v[k]);
				if ( (*tempctr >> defs->ngenes + defs->mgenes + defs->egenes + 4) % 2)
					guts[i] += deriv[which];
				if ( (*tempctr >> defs->ngenes + defs->mgenes + defs->egenes + 8) % 2)
					guts[i] += -MM[which] * ( v[k - 2 * defs->ngenes] - 4 * v[k - defs->ngenes] + 6 * v[k] - 4 * v[k + defs->ngenes] +  v[k + 2 * defs->ngenes] );
				tempctr++;
			}
		}
		tempctr = gutcomps;
		base1 = gn - 2 * numguts;
		k = which + n - 2 * defs->ngenes;
		for(i = base1; i < base1 + numguts; i++) {
			if ( (*tempctr >> defs->ngenes + defs->mgenes + defs->egenes + 2) % 2)
				guts[i] += D[which] * (v[k - defs->ngenes] - v[k]);
			if ( (*tempctr >> defs->ngenes + defs->mgenes + defs->egenes + 3) % 2)
				guts[i] += D[which] * (v[k + defs->ngenes] - v[k]);
			if ( (*tempctr >> defs->ngenes + defs->mgenes + defs->egenes + 4) % 2)
				guts[i] += deriv[which];
			if ( (*tempctr >> defs->ngenes + defs->mgenes + defs->egenes + 8) % 2)
				guts[i] += -MM[which] * ( v[k - 3 * defs->ngenes] - 4 * v[k - 2 * defs->ngenes] + 6 * v[k - defs->ngenes] - 4 * v[k] + v[k + defs->ngenes] );
			tempctr++;
		}
/* last: posterior-most nucleus */
		tempctr = gutcomps;
		k = which + n - defs->ngenes;
		base1 = gn - numguts;
		for(i = base1; i < gn; i++) {
			if ( (*tempctr >> defs->ngenes + defs->mgenes + defs->egenes + 2) % 2)
				guts[i] += D[which] * (v[k - defs->ngenes] - v[k]);
			if ( (*tempctr >> defs->ngenes + defs->mgenes + defs->egenes + 4) % 2)
				guts[i] += deriv[which];
			if ( (*tempctr >> defs->ngenes + defs->mgenes + defs->egenes + 8) % 2)
				guts[i] += -MM[which] * ( v[k - 4 * defs->ngenes] - 4 * v[k - 3 * defs->ngenes] + 6 * v[k - 2 * defs->ngenes] - 4 * v[k - defs->ngenes] +  v[k] );
			tempctr++;
		}
	}
	free(deriv);
	return;
}

/*** ParseString: parses a line of the $gutsdefs section (dataformatX.X ****
 *                has the details); this function then returns two things: *
 *                1) the number of 'words' separated by whitespace in that *
 *                   string is the function's return value                 *
 *                2) the 'words' themselves are returned in arginp         *
 ***************************************************************************/

int ParseString(char *v, char **arginp)
{
	const int IN  = 1;                   /* are we inside or outside a word? */
	const int OUT = 0;
	int   state   = OUT;          /* state can be either 'IN' or 'OUT' above */
	int   n       = 0;                                  /* counter for words */
	char *mover   = v;        /* pointer-counter for pos within whole string */
	char **myargs = arginp;           /* pointer-counter for different words */
	char *tempo   = NULL;     /* pointer-counter for pos within current word */
/* parsing starts here */
	n = 0;
	state = OUT;
	while ((*mover != '\0') && (*mover != '\n')) {   /* loop thru whole line */
		if (*mover == ' ' || *mover == '\t') { /* whitespace -> done with word */
			if (state == IN) {                            /* word just finished: */
				*tempo = '\0';                       /* finish string by adding \0 */
				myargs++;                                       /* go to next word */
			}
			state = OUT;                           /* whitespace -> outside word */
		} else {
			if (state == OUT) {                                    /* a new word */
				state   = IN;                           /* we're inside a word now */
				*myargs = (char *)calloc(MAX_RECORD, sizeof(char));
				tempo   = *myargs;      /* allocate and assign tempo to new string */
				++n;
			}
			*tempo = *mover;                         /* characters copied here */
			tempo++;                     /* tempo keeps track of pos within word */
		}
		mover++;               /* mover keeps track of pos within whole string */
	}
	if (state == IN) {       /* if we finished within a word, add a final \0 */
		*tempo = '\0';
		myargs++;
	}
	return n;                    /* return the number of words in the string */
}



/*** GetGutsComps: takes a geneID string and an array of ID strings; then **
 *                 returns an array of long ints with bit flags set that   *
 *                 tell CalcGuts() what to calculate for each column of    *
 *                 guts output; this function also returns a pointer to    *
 *                 the current gene in the gene ID string                  *
 ***************************************************************************/

char *GetGutsComps(char *geneidstring, char **specsin, unsigned long *specsout)
{
	const char    *gutsids = "ALXYDQZVWU";                  /* legal guts IDs */
	char          *strcntr     = NULL;       /* ptr-counter within ID string */
	char          *ids         = NULL;          /* string with all legal IDs */
	char          **strarrcntr = NULL;     /* ptr-counter for gut ID strings */
	unsigned long *arrcntr     = NULL;           /* ptr-counter for gutcomps */
	char          *i           = NULL;  /* ptr for finding IDs in ids string */
/* ids = geneIDs plus gutsIDs */
	ids = (char *)calloc(MAX_RECORD, sizeof(char));
	ids = strcpy(ids, geneidstring);
	ids = strcat(ids, gutsids);
/* set pointer-counters to both ID strings and gutcomps */
	arrcntr = specsout;
	strarrcntr = specsin;
	strarrcntr++;                            /* skip first entry (gene name) */
/* the following loop parses the words of the ID string and sets according *
 * bits in the gutscomp array                                              */
	while ( (strcntr = *strarrcntr ) ) {
		while ( *strcntr != '\0' ) {
			if ( !(i = strchr(ids, *strcntr)) )
				error("GetGutsComps: unknown gene in gutsdefs string");
			else if (*strcntr == 'U') {
				if ((*(strcntr+1) != '\0') || (strcntr != *strarrcntr) )
					error("GetGutsComps: U not alone in gutsdefs string");
				*arrcntr = (1 << (strlen(geneidstring)+2)) - 1;
			} else
				*arrcntr |= (1 << (i-ids));
			strcntr++;
		}
		arrcntr++;
		strarrcntr++;
	}
/* clean up and return a pointer to the gut gene in the gene ID string */
	free(ids);
	return strchr(geneidstring, **specsin);
}

/*** MUTATOR FUNCTIONS *****************************************************/
/*** Mutate: calls mutator functions according to genotype string **********
 ***************************************************************************/
void Mutate(char *g_type)
{
	int  i;
	int  c;
	char *record;

	CopyLParm();
	record = g_type;
	c=(int)*record;
	for ( i=0; c != '\0'; i++, c=(int)*(++record) ) {
		if ( c == 'W' )
			continue;
		else if ( c == 'R' )
			R_Mutate(i);
		else if ( c == 'S' )
			RT_Mutate(i);
		else if ( c == 'T' )
			T_Mutate(i);
		else
			error("Mutate: unrecognized letter in genotype string!");
	}
}

void MutateWithBicoidDosage(char *g_type, double dosage)
{
	int  i;
	int  c;
	int has_null_mutation = 0;
	int null_mutated = 0;
	char *record;
	double bcd_factor = 0.5 * dosage;
	CopyLParm();
	record = g_type;
	c=(int)*record;
	for ( i=0; c != '\0'; i++, c=(int)*(++record) ) {
		if ( c == 'W' ) {
			continue;
		} else if ( c == 'R' ) {
			has_null_mutation = 1;
			null_mutated++;
			R_Mutate(i);
		} else if ( c == 'S' ) {
			has_null_mutation = 1;
			null_mutated++;
			RT_Mutate(i);
		} else if ( c == 'T' ) {
			has_null_mutation = 1;
			null_mutated++;
			T_Mutate(i);
		} else {
			error("Mutate: unrecognized letter in genotype string!");
		}
	}
	for ( i = 0; i < defs->ngenes * defs->mgenes; i++) {
		lparm->M[i] *= bcd_factor;
	}
/* A hack to get different parameters for mutants */
/* FIXME */
	if ( defs->mob_schedule == 'X' ) {
		if ( has_null_mutation == 1 ) {
			for ( i = 0; i < defs->ngenes * defs->ncouples; i++ ) {
				lparm->P[i] = parm->P[i];
			}
		} else {
			for ( i = 0; i < defs->ngenes * defs->ncouples; i++ ) {
				lparm->P[i] = 0.0;
			}
		}
	} else if ( defs->mob_schedule == 'Y' ) {
		if ( has_null_mutation == 0 ) {
			for ( i = 0; i < defs->ngenes; i++ ) {
				lparm->d[i] = 0;
			}
		}
	} else if ( defs->mob_schedule == 'Z' ) {
		if ( has_null_mutation == 1 ) {
			double factor = defs->ngenes / ( defs->ngenes - null_mutated );
			for ( i = 0; i < defs->ngenes; i++ ) {
				lparm->d[i] *= factor;
			}
		}
	} else if ( defs->mob_schedule == 'U' ) {
		if ( has_null_mutation == 1 ) {
			for ( i = 0; i < defs->ngenes; i++ ) {
				lparm->d[i] = parm->m[i];
			}
		}
	}
#ifdef GPGPU_CUDA
	cudaMemcpy(d_R, lparm->R, sizeof(double) * defs->ngenes, cudaMemcpyHostToDevice);
	cudaMemcpy(d_T, lparm->T, sizeof(double) * defs->ngenes * defs->ngenes, cudaMemcpyHostToDevice);
	cudaMemcpy(d_E, lparm->E, sizeof(double) * defs->ngenes * defs->egenes, cudaMemcpyHostToDevice);
	cudaMemcpy(d_M, lparm->M, sizeof(double) * defs->ngenes * defs->mgenes, cudaMemcpyHostToDevice);
	cudaMemcpy(d_P, lparm->P, sizeof(double) * defs->ngenes * defs->ncouples, cudaMemcpyHostToDevice);
	cudaMemcpy(d_h, lparm->h, sizeof(double) * defs->ngenes, cudaMemcpyHostToDevice);
	cudaMemcpy(d_lambda, lparm->lambda, sizeof(double) * defs->ngenes, cudaMemcpyHostToDevice);
#endif
}

/*** T_Mutate: mutates genes by setting all their T matrix entries *********
 *             to zero. Used to simulate mutants that express a            *
 *             non-functional protein.                                     *
 ***************************************************************************/
void T_Mutate(int gene)
{
	int i;
	for( i = 0; i < defs->ngenes; i++)
		lparm->T[(i*defs->ngenes)+gene] = 0;
}

void T_MMutate(int gene, double arg)
{
	int i;
	for( i = 0; i < defs->ngenes; i++)
		parm->T[(i*defs->ngenes)+gene] *= arg;
}

void P_Mutate(int gene)
{
	int i, j;
	for( i = 0; i < defs->ncouples; i++) {
		if ( ( defs->couples[i].first_gene_type == zygotic && defs->couples[i].first_gene_id == gene ) || ( defs->couples[i].second_gene_type == zygotic && defs->couples[i].second_gene_id == gene ) ) {
			for( j = 0; j < defs->ngenes; j++) {
				lparm->P[(j*defs->ncouples)+i] = 0;
			}
		}
	}
}

void E_Mutate(int gene)
{
	int i;
	for( i = 0; i < defs->egenes; i++) {
		parm->E[ ( i * defs->egenes ) + gene ] = 0;
	}
}

void D_Mutate(int gene)
{
	int i;
	parm->d[ gene ] = 0;
	if ( defs->mob_schedule == 'U' ) {
		parm->m[ gene ] = 0;
	}
}

/*** R_Mutate: mutates genes by setting their promotor strength R **********
 *             to zero, so there will be no transcription at all           *
 *             anymore. Used to simulate mutants that don't pro-           *
 *             duce any protein anymore.                                   *
 ***************************************************************************/

void R_Mutate(int gene)
{
	lparm->R[gene] = 0;
	dirichlet_boundary_coef[gene] = 0;
}

void R_MMutate(int gene, double arg)
{
	parm->R[gene] *= arg;
}

/*** RT_Mutate: mutates gene by setting both promoter strength R ***********
 *              and T matrix entries to zero. Useful, if you want          *
 *              to be really sure that there is NO protein pro-            *
 *              duction and that maternal contribution also don't          *
 *              contribute anything to gene interactions.                  *
 ***************************************************************************/

void RT_Mutate(int gene)
{
	R_Mutate(gene);
	T_Mutate(gene);
	P_Mutate(gene);
}

void RT_MMutate(int gene, double arg)
{
	R_MMutate(gene, arg);
	T_MMutate(gene, arg);
/*	P_Mutate(gene);*/
}

void RT_Mutate_old(int gene)
{
	int i;
	for( i=0; i<defs->ngenes; i++)
		lparm->T[(i*defs->ngenes)+gene]=0;
	lparm->R[gene]=0;
/* Don't need to zero param.thresh in (trans acting) mutants */
}

void MMutate(char *g_type, double arg)
{
	int  i;
	int  c;
	char *record;
	record = g_type;
	c=(int)*record;
	for ( i=0; c != '\0'; i++, c=(int)*(++record) ) {
		if ( c == 'W' )
			continue;
		else if ( c == 'R' )
			R_MMutate(i, arg);
		else if ( c == 'S' )
			RT_MMutate(i, arg);
		else if ( c == 'T' )
			T_MMutate(i, arg);
		else
			error("Mutate: unrecognized letter in genotype string!");
	}
}

/*** CopyParm: copies all the parameters into the lparm struct *************
 ***************************************************************************/

EqParms* CopyParm(EqParms* orig_parm)
{
	int           i,j;                                /* local loop counters */
	EqParms*       l_parm;              /* copy of parm struct to be returned */
	l_parm = InstallParm();
	for (i=0; i<defs->ngenes; i++ ) {
		l_parm->R[i] = orig_parm->R[i];
		for (j=0; j<defs->ngenes; j++ )
			l_parm->T[(i*defs->ngenes)+j] = orig_parm->T[(i*defs->ngenes)+j];
		for (j=0; j<defs->egenes; j++ )
			l_parm->E[(i*defs->egenes)+j] = orig_parm->E[(i*defs->egenes)+j];
		for (j=0; j<defs->mgenes; j++ )
			l_parm->M[(i*defs->mgenes)+j] = orig_parm->M[(i*defs->mgenes)+j];
		for (j=0; j<defs->ncouples; j++ )
			l_parm->P[(i*defs->ncouples)+j] = orig_parm->P[(i*defs->ncouples)+j];
		l_parm->h[i] = orig_parm->h[i];
		l_parm->lambda[i] = orig_parm->lambda[i];
		l_parm->tau[i] = orig_parm->tau[i];
	}
	if ( (defs->diff_schedule == 'A') || (defs->diff_schedule == 'C') ) {
		l_parm->d[0] = orig_parm->d[0];
	} else {
		for (i=0; i<defs->ngenes; i++)
			l_parm->d[i] = orig_parm->d[i];
	}
	if ( (defs->mob_schedule == 'A') || (defs->mob_schedule == 'C') ) {
		l_parm->m[0] = orig_parm->m[0];
	} else {
		for (i=0; i<defs->ngenes; i++)
			l_parm->m[i] = orig_parm->m[i];
	}
	if ( (defs->mob_schedule == 'A') || (defs->mob_schedule == 'C') ) {
		l_parm->mm[0] = orig_parm->mm[0];
	} else {
		for (i=0; i<defs->ngenes; i++)
			l_parm->mm[i] = orig_parm->mm[i];
	}
	return l_parm;
}
/*** A FUNCTION THAT SETS STATIC STUFF IN ZYGOTIC.C ************************/
/*** SetRule: sets the static variable rule to MITOSIS or INTERPHASE *******
 ***************************************************************************/
void SetRule(int r) {
	rule = r;
}
/*** SetCCycle: sets the static variable cycle to the cycle where we are ****
 ***************************************************************************/
void SetCCycle(int cc) {
	ccycle = cc;
}

void SetNNucs(int n) {
	num_nucs = n;
}

void set_use_external_inputs_only(int u) {
	use_external_inputs_only = u;
}

/*** A FUNCTION THAT RETURN STATIC STUFF FROM ZYGOTIC.C ********************/
/*** GetParameters: returns the parm struct to the caller; note that this **
 *                  function returns the ORIGINAL PARAMETERS as they are   *
 *                  in the data file and NOT THE MUTATED ONES; this is im- *
 *                  portant to prevent limit violations in Score()         *
 ***************************************************************************/

EqParms *GetParameters(void)
{
	return parm;
}

/*** GetMutParameters: same as above but returns the mutated copy of the ***
 *                     parameter struct; important for writing guts        *
 ***************************************************************************/

EqParms *GetMutParameters(void)
{
	return lparm;
}
/*** A FUNCTION THAT READS PARAMETERS FROM THE DATA FILE INTO STRUCTS ******/
/*** ReadParamters: reads the parameters for a simulation run from the *****
 *                  eqparms or input section of the data file as indicated *
 *                  by the section_title argument and does the conversion  *
 *                  of protein half lives into lambda parameters.          *
 ***************************************************************************/

EqParms*ReadParameters(FILE *fp, char *section_title)
{
	EqParms*           l_parm;                 /* local copy of EqParm struct */
	l_parm = InstallParm();
	ReReadParameters(l_parm, fp, section_title);
	return l_parm;
}


void ReReadParameters(EqParms*l_parm, FILE *fp, char *section_title)
{
	double            *tempparm, *tempparm1, *tempparm2, *tempparm3;            /* temporary array to read parms */
	int               i;                               /* local loop counter */
	int               c;                         /* used to parse text lines */
	int               lead_punct;            /* flag for leading punctuation */
	int               linecount = 0;        /* keep track of # of lines read */
	int               Tcount    = 0;           /* keep track of T lines read */
	int               Ecount    = 0;           /* keep track of T lines read */
	int               Mcount    = 0;           /* keep track of T lines read */
	int               Pcount    = 0;           /* keep track of T lines read */
	char              *base;          /* pointer to beginning of line string */
	char              *record;    /* string for reading whole line of params */
	char              **fmt, **fmt1, **fmt2, **fmt3;   /* array of format strings for reading params */
	char              *skip, *skip1, *skip2, *skip3;               /* string of values to be skipped */
	const char        read_fmt[] = "%lg";                   /* read a double */
	const char        skip_fmt[] = "%*lg ";               /* ignore a double */
	base = (char *)calloc(MAX_RECORD, sizeof(char));
	skip = (char *)calloc(MAX_RECORD, sizeof(char));
	skip1 = (char *)calloc(MAX_RECORD, sizeof(char));
	skip2 = (char *)calloc(MAX_RECORD, sizeof(char));
	skip3 = (char *)calloc(MAX_RECORD, sizeof(char));
	fmt  = (char **)calloc(defs->ngenes, sizeof(char *));
	fmt1  = (char **)calloc(defs->egenes, sizeof(char *));
	fmt2  = (char **)calloc(defs->mgenes, sizeof(char *));
	fmt3  = (char **)calloc(defs->ncouples, sizeof(char *));
	tempparm = (double *)calloc(defs->ngenes, sizeof(double));
	tempparm1 = (double *)calloc(defs->egenes, sizeof(double));
	tempparm2 = (double *)calloc(defs->mgenes, sizeof(double));
	tempparm3 = (double *)calloc(defs->ncouples, sizeof(double));
/* create format strings according to the number of genes */
	for ( i = 0; i < defs->ngenes; i++ ) {
		fmt[i] = (char *)calloc(MAX_RECORD, sizeof(char));
		fmt[i] = strcpy(fmt[i], skip);
		fmt[i] = strcat(fmt[i], read_fmt);
		skip   = strcat(skip, skip_fmt);
	}
	for ( i = 0; i < defs->egenes; i++ ) {
		fmt1[i] = (char *)calloc(MAX_RECORD, sizeof(char));
		fmt1[i] = strcpy(fmt1[i], skip1);
		fmt1[i] = strcat(fmt1[i], read_fmt);
		skip1   = strcat(skip1, skip_fmt);
	}
	for ( i = 0; i < defs->mgenes; i++ ) {
		fmt2[i] = (char *)calloc(MAX_RECORD, sizeof(char));
		fmt2[i] = strcpy(fmt2[i], skip2);
		fmt2[i] = strcat(fmt2[i], read_fmt);
		skip2   = strcat(skip2, skip_fmt);
	}
	for ( i = 0; i < defs->ncouples; i++ ) {
		fmt3[i] = (char *)calloc(MAX_RECORD, sizeof(char));
		fmt3[i] = strcpy(fmt3[i], skip3);
		fmt3[i] = strcat(fmt3[i], read_fmt);
		skip3   = strcat(skip3, skip_fmt);
	}
/* initialize the EqParm struct */
	fp = FindSection(fp, section_title);             /* find eqparms section */
	if( !fp )
		error("ReadParameters: cannot locate %s section", section_title);
	while ( strncmp(( base=fgets(base, MAX_RECORD, fp)), "$$", 2)) {
		record = base;
		lead_punct = 0;                                           /* of string */
		c = (int)*record;
		while ( c != '\0' ) {
			if ( isdigit(c) ) {                            /* line contains data */
				record = base;
/* usually read ngenes parameters, but for diff. schedule A or C only read *
 * one d parameter                                                         */
				if ((linecount == 6) && ((defs->diff_schedule=='A') || (defs->diff_schedule=='C'))) {
					if ( 1 != sscanf(record, fmt[0], &tempparm[0]) )
						error("ReadParameters: error reading parms");
				} else if ((linecount == 9) && ((defs->mob_schedule=='A') || (defs->mob_schedule=='C'))) {
					if ( 1 != sscanf(record, fmt[0], &tempparm[0]) )
						error("ReadParameters: error reading parms");
				} else if ((linecount == 10) && ((defs->mob_schedule=='A') || (defs->mob_schedule=='C'))) {
					if ( 1 != sscanf(record, fmt[0], &tempparm[0]) )
						error("ReadParameters: error reading parms");
				} else if (linecount == 2) {
					for ( i=0; i < defs->egenes; i++ ) {
						if ( 1 != sscanf(record, fmt1[i], &tempparm1[i]) )
							error("ReadParameters: error reading parms");
					}
				} else if (linecount == 3) {
					for ( i=0; i < defs->mgenes; i++ ) {
						if ( 1 != sscanf(record, fmt2[i], &tempparm2[i]) )
							error("ReadParameters: error reading parms");
					}
				} else if (linecount == 4) {
					for ( i=0; i < defs->ncouples; i++ ) {
						if ( 1 != sscanf(record, fmt3[i], &tempparm3[i]) )
							error("ReadParameters: error reading couples parms");
					}
				} else {
					for ( i=0; i < defs->ngenes; i++ ) {
						if ( 1 != sscanf(record, fmt[i], &tempparm[i]) )
							error("ReadParameters: error reading parms");
					}
				}
					switch (linecount) {  /* copy read parameters into the right array */
						case 0:                                                        /* R */
							for ( i=0; i < defs->ngenes; i++ )
								l_parm->R[i] = tempparm[i];
							linecount++;
						break;
						case 1:                 /* T: keep track of read lines with Tcount */
							for ( i = 0; i < defs->ngenes; i++ )
								l_parm->T[i+Tcount*defs->ngenes] = tempparm[i];
							Tcount++;
							if ( Tcount == defs->ngenes ) {
								linecount++;
								if ( defs->egenes == 0 ) {
									linecount++;
									if ( defs->mgenes == 0 ) {
										linecount++;
										if ( defs->ncouples == 0 )
											linecount++;
									}
								}
							}
						break;
						case 2:                 /* T: keep track of read lines with Tcount */
							for ( i = 0; i < defs->egenes; i++ )
								l_parm->E[i+Ecount*defs->egenes] = tempparm1[i];
							Ecount++;
							if ( Ecount == defs->ngenes ) {
								linecount++;
								if ( defs->mgenes == 0 ) {
									linecount++;
									if ( defs->ncouples == 0 )
										linecount++;
								}
							}
						break;
						case 3:                 /* T: keep track of read lines with Tcount */
							for ( i = 0; i < defs->mgenes; i++ )
								l_parm->M[i+Mcount*defs->mgenes] = tempparm2[i];
							Mcount++;
							if ( Mcount == defs->ngenes ) {
								linecount++;
								if ( defs->ncouples == 0 )
									linecount++;
							}
						break;
						case 4:                 /* T: keep track of read lines with Tcount */
							for ( i = 0; i < defs->ncouples; i++ )
								l_parm->P[i+Pcount*defs->ncouples] = tempparm3[i];
							Pcount++;
							if ( Pcount == defs->ngenes )
								linecount++;
						break;
						case 5:                                                       /* h */
							for ( i=0; i < defs->ngenes; i++ )
								l_parm->h[i] = tempparm[i];
							linecount++;
						break;
						case 6:                              /* d: consider diff. schedule */
							if ((defs->diff_schedule == 'A') || (defs->diff_schedule == 'C' )) {
								l_parm->d[0] = tempparm[0];
							} else {
								for ( i=0; i < defs->ngenes; i++ )
									l_parm->d[i] = tempparm[i];
							}
							linecount++;
						break;
						case 7:                                                  /* lambda */
							for ( i=0; i < defs->ngenes; i++ ) {
								l_parm->lambda[i] = ( tempparm[i] > 0 ) ? log(2.) / tempparm[i] : 0;
							}                                        /* conversion done here */
							linecount++;
						break;
						case 8:                                                       /* h */
							for ( i=0; i < defs->ngenes; i++ )
								l_parm->tau[i] = tempparm[i];
							linecount++;
						break;
						case 9:                           /* d: consider diff. schedule */
							if ((defs->mob_schedule == 'A') || (defs->mob_schedule == 'C' )) {
								l_parm->m[0] = tempparm[0];
							} else {
								for ( i=0; i < defs->ngenes; i++ )
									l_parm->m[i] = tempparm[i];
							}
							linecount++;
						break;
						case 10:                           /* d: consider diff. schedule */
							if ((defs->mob_schedule == 'A') || (defs->mob_schedule == 'C' )) {
								l_parm->mm[0] = tempparm[0];
							} else {
								for ( i=0; i < defs->ngenes; i++ )
									l_parm->mm[i] = tempparm[i];
							}
							linecount++;
						break;

						default:
							error("ReadParameters: too many lines in parameter section");
					}
				break;                           /* don't do rest of loop anymore! */
			} else if ( isalpha(c) ) {                     /* letter means comment */
				break;
			} else if ( c == '-' ) {                  /* next two elsifs for punct */
				if ( ((int) *(record+1)) == '.' )
					record++;
				lead_punct = 1;
				c=(int)*(++record);
			} else if ( c == '.' ) {
				lead_punct = 1;
				c=(int)*(++record);
			} else if ( ispunct(c) ) {                /* other punct means comment */
				break;
			} else if ( isspace(c) ) {               /* ignore leading white space */
				if ( lead_punct )                 /* white space after punct means */
					break;                                              /* comment */
				else {
					c=(int)*(++record);              /* get next character in record */
				}
			} else {
				error("ReadParameters: illegal character in %s");
			}
		}
	}
	free(tempparm);
	free(tempparm1);
	free(tempparm2);
	free(tempparm3);
	free(base);
	free(skip);
	free(skip1);
	free(skip2);
	free(skip3);
	for ( i = 0; i < defs->ngenes; i++)
		free(fmt[i]);
	free(fmt);
	for ( i = 0; i < defs->egenes; i++)
		free(fmt1[i]);
	free(fmt1);
	for ( i = 0; i < defs->mgenes; i++)
		free(fmt2[i]);
	free(fmt2);
	for ( i = 0; i < defs->ncouples; i++)
		free(fmt3[i]);
	free(fmt3);
}

void ReReadParameters1(EqParms*l_parm, FILE *fp)
{
	int               i, j;                               /* local loop counter */
	int nmrna, nmrna_ext;
	if (model == HEDIRECT || model == HESRR || model == HEQUENCHING || model == HELOGISTIC || model == HEURR) {
		nmrna = defs->ngenes / 2;
		nmrna_ext = nmrna;
	} else {
		nmrna = defs->ngenes;
		nmrna_ext = 0;
	}
	fscanf(fp, "%*s");
	for ( i = 0; i < defs->ngenes; i++ ) {
		fscanf(fp, "%lg", &(l_parm->R[i]));
	}
	for ( i = 0; i < nmrna; i++ ) {
		for ( j = 0; j < nmrna; j++ ) {
			fscanf(fp, "%lg", &(l_parm->T[j + i * defs->ngenes]));
		}
	}
	for ( i = 0; i < nmrna; i++ ) {
		for ( j = 0; j < defs->egenes - nmrna_ext; j++ ) {
			fscanf(fp, "%lg", &(l_parm->E[j + i * defs->egenes]));
		}
	}
	for ( i = 0; i < nmrna; i++ ) {
		for ( j = 0; j < defs->mgenes; j++ ) {
			fscanf(fp, "%lg", &(l_parm->M[j + i * defs->mgenes]));
		}
	}
	for ( i = 0; i < defs->ngenes; i++ ) {
		for ( j = 0; j < defs->ncouples; j++ ) {
			fscanf(fp, "%lg", &(l_parm->P[j + i * defs->ncouples]));
		}
	}
	for ( i = 0; i < defs->ngenes; i++ ) {
		fscanf(fp, "%lg", &(l_parm->h[i]));
	}
	if ((defs->diff_schedule == 'A') || (defs->diff_schedule == 'C' )) {
		fscanf(fp, "%lg", &(l_parm->d[0]));
	} else {
		for ( i = 0; i < defs->ngenes; i++ ) {
			fscanf(fp, "%lg", &(l_parm->d[i]));
		}
	}
	for ( i = 0; i < defs->ngenes; i++ ) {
		fscanf(fp, "%lg", &(l_parm->lambda[i]));
//		fprintf(stderr, "%d %f", i, l_parm->lambda[i]);
		l_parm->lambda[i] = ( l_parm->lambda[i] > 0 ) ? log(2.) / l_parm->lambda[i] : 0;
//		fprintf(stderr, " %f\n", i, l_parm->lambda[i]);
	}
	for ( i = 0; i < defs->ngenes; i++ ) {
		fscanf(fp, "%lg", &(l_parm->tau[i]));
	}
	if ((defs->mob_schedule == 'A') || (defs->mob_schedule == 'C' )) {
		fscanf(fp, "%lg", &(l_parm->m[0]));
	} else {
		for ( i = 0; i < defs->ngenes; i++ ){
			fscanf(fp, "%lg", &(l_parm->m[i]));
		}
	}
	if ((defs->mob_schedule == 'A') || (defs->mob_schedule == 'C' )) {
		fscanf(fp, "%lg", &(l_parm->mm[0]));
	} else {
		for ( i=0; i < defs->ngenes; i++ ) {
			fscanf(fp, "%lg", &(l_parm->mm[i]));
		}
	}
}

/*** A FUNCTION THAT WRITES PARAMETERS INTO THE DATA FILE ******************/

/*** WriteParameters: writes the out_parm struct into a new section in the *
 *                    file specified by filename; the new 'eqparms' sec-   *
 *                    tion is inserted right after the 'input' section;    *
 *                    to achieve this, we need to write to a temporary     *
 *                    file which is then renamed to the output file name   *
 *              NOTE: lambdas are converted back into protein half lives!! *
 ***************************************************************************/

void WriteParameters(char *name, EqParms *p, char *title, int ndigits)
{
	char   *temp;                                     /* temporary file name */
	FILE   *outfile;                                  /* name of output file */
	FILE   *tmpfile;                               /* name of temporary file */
	char*title_of_above;
	title_of_above=(char*)calloc(MAX_RECORD,sizeof(char));
	if ( !strcmp(title, "input") ) {
		sprintf(title_of_above,"genotypes");
	} else if ( ( !strcmp(title, "eqparms")) || ( !strcmp(title, "parameters")) ) {
		sprintf(title_of_above,"input");
	}
	GetSectionPosition(&outfile, &tmpfile, title, title_of_above, name, &temp);
/* now we write the eqparms section into the tmpfile */
	PrintParameters(tmpfile, p, title, ndigits);
	WriteRest(&outfile,&tmpfile,name,&temp);
	free(title_of_above);
}

/*** PrintParameters: prints an eqparms section with 'title' to the stream *
 *                    indicated by fp                                      *
 ***************************************************************************/
void PrintParameters(FILE *fp, EqParms *p, char *title, int ndigits)
{
	int    i, j;                                      /* local loop counters */
	double lambda_tmp;                           /* temporary var for lambda */
	fprintf(fp, "$%s\n", title);
	fprintf(fp, "promoter_strengths:\n");             /* Rs are written here */
	for ( i=0; i<defs->ngenes; i++ )
		fprintf(fp, "%*.*f ", ndigits+4, ndigits, p->R[i]);
	fprintf(fp, "\n");
	fprintf(fp, "genetic_interconnect_matrix:\n");        /* Ts written here */
	for ( i=0; i<defs->ngenes; i++ ) {
		for ( j=0; j<defs->ngenes; j++ )
			fprintf(fp, "%*.*f ", ndigits+4, ndigits, p->T[(i*defs->ngenes)+j]);
		fprintf(fp, "\n");
	}
	fprintf(fp, "external_input_strengths:\n");        /* Es written here */
	if ( defs->egenes != 0 )
		for ( i=0; i<defs->ngenes; i++ ) {
			for ( j=0; j<defs->egenes; j++ )
				fprintf(fp, "%*.*f ", ndigits+4, ndigits, p->E[(i*defs->egenes)+j]);
			fprintf(fp, "\n");
		}
	fprintf(fp, "maternal_connection_strengths:\n");      /* ms written here */
	if ( defs->mgenes != 0 )
		for ( i=0; i<defs->ngenes; i++ ) {
			for ( j=0; j<defs->mgenes; j++ )
				fprintf(fp, "%*.*f ", ndigits+4, ndigits, p->M[(i*defs->mgenes)+j]);
			fprintf(fp, "\n");
		}
	if ( defs->ncouples != 0 ) {
		for ( j = 0; j < defs->ncouples - 1; j++ )
			fprintf(fp, "%c%c_", defs->couples[j].g1, defs->couples[j].g2);
		fprintf(fp, "%c%c\n", defs->couples[defs->ncouples - 1].g1, defs->couples[defs->ncouples - 1].g2);
		for ( i=0; i<defs->ngenes; i++ ) {
			for ( j=0; j<defs->ncouples; j++ )
				fprintf(fp, "%*.*f ", ndigits+4, ndigits, p->P[(i*defs->ncouples)+j]);
			fprintf(fp, "\n");
		}
	} else {
		fprintf(fp, "couples:\n");      /* ms written here */
	}
	fprintf(fp, "promoter_thresholds:\n");            /* hs are written here */
	for ( i=0; i<defs->ngenes; i++ )
		fprintf(fp, "%*.*f ", ndigits+4, ndigits, p->h[i]);
	fprintf(fp, "\n");
	fprintf(fp, "diffusion_parameter(s):\n");         /* ds are written here */
	if ( (defs->diff_schedule == 'A') || (defs->diff_schedule == 'C') )
		fprintf(fp, "%*.*f ", ndigits+4, ndigits, p->d[0]);
	else
		for ( i=0; i<defs->ngenes; i++ )
			fprintf(fp, "%*.*f ", ndigits+4, ndigits, p->d[i]);
	fprintf(fp, "\n");
	fprintf(fp, "protein_half_lives:\n");        /* lambdas are written here */
	for ( i=0; i<defs->ngenes; i++ ) {
		lambda_tmp = log(2.) / p->lambda[i];           /* conversion done here */
		fprintf(fp, "%*.*f ", ndigits+4, ndigits, lambda_tmp);
	}
	fprintf(fp, "\n");
	fprintf(fp, "translational_transcriptional_delays:\n");        /* taus are written here */
	for ( i = 0; i < defs->ngenes; i++ )
		fprintf(fp, "%*.*f ", ndigits+4, ndigits, p->tau[i]);
	fprintf(fp, "\n");
	fprintf(fp, "mobility:\n");         /* ds are written here */
	if ( (defs->mob_schedule == 'A') || (defs->mob_schedule == 'C') )
		fprintf(fp, "%*.*f ", ndigits+4, ndigits, p->m[0]);
	else
		for ( i=0; i<defs->ngenes; i++ )
			fprintf(fp, "%*.*f ", ndigits+4, ndigits, p->m[i]);
	fprintf(fp, "\n");
	fprintf(fp, "mitosis_mobility:\n");         /* ds are written here */
	if ( (defs->mob_schedule == 'A') || (defs->mob_schedule == 'C') )
		fprintf(fp, "%*.*f ", ndigits+4, ndigits, p->mm[0]);
	else
		for ( i=0; i<defs->ngenes; i++ )
			fprintf(fp, "%*.*f ", ndigits+4, ndigits, p->mm[i]);
	fprintf(fp, "\n$$\n");
}

/*added by MacKoel */

/** GetGrad:  returns the grad struct to the caller; note that this        *
 *            function returns the gradient as it is added to parameters   *
 ***************************************************************************/

EqParms *GetGrad(void)
{
	return grad;
}

/*** InstallParm: returns parameter struct with allocated arrays ***********
 ***************************************************************************/

EqParms* InstallParm()
{
	EqParms*       l_parm;              /* copy of parm struct to be returned */
	l_parm = (EqParms*)malloc(sizeof(EqParms));
	l_parm->R = (double *)calloc(defs->ngenes, sizeof(double));
	l_parm->T = (double *)calloc(defs->ngenes * defs->ngenes, sizeof(double));
	l_parm->E = (double *)calloc(defs->ngenes * defs->egenes, sizeof(double));
	l_parm->M = (double *)calloc(defs->ngenes * defs->mgenes, sizeof(double));
	l_parm->P = (double *)calloc(defs->ngenes * defs->ncouples, sizeof(double));
	l_parm->h = (double *)calloc(defs->ngenes, sizeof(double));
	if ( (defs->diff_schedule == 'A') || (defs->diff_schedule == 'C') ) {
		l_parm->d = (double *)malloc(sizeof(double));
	} else {
		l_parm->d = (double *)calloc(defs->ngenes, sizeof(double));
	}
	if ( (defs->mob_schedule == 'A') || (defs->mob_schedule == 'C') ) {
		l_parm->m = (double *)malloc(sizeof(double));
	} else {
		l_parm->m = (double *)calloc(defs->ngenes, sizeof(double));
	}
	if ( (defs->mob_schedule == 'A') || (defs->mob_schedule == 'C') ) {
		l_parm->mm = (double *)malloc(sizeof(double));
	} else {
		l_parm->mm = (double *)calloc(defs->ngenes, sizeof(double));
	}
	l_parm->lambda = (double *)calloc(defs->ngenes, sizeof(double));
	l_parm->tau = (double *)calloc(defs->ngenes, sizeof(double));
	return l_parm;
}

/*** CopyLParm: like CopyParm (and it is used instead) but doesn't    ******
 *   allocate anything                                                ******
 ***************************************************************************/

void CopyLParm()
{
	int i,j;
	if ( model == HEDIRECT ) {
        for (i=0; i<defs->ngenes; i++ ) {
            lparm->R[i] = parm->R[i];
            for (j=0; j<defs->ngenes; j++ )
                lparm->T[(i*defs->ngenes)+j] = exp(parm->T[(i*defs->ngenes)+j]);
            for (j=0; j<defs->egenes; j++ )
                lparm->E[(i*defs->egenes)+j] = exp(parm->E[(i*defs->egenes)+j]);
            for (j=0; j<defs->mgenes; j++ )
                lparm->M[(i*defs->mgenes)+j] = exp(parm->M[(i*defs->mgenes)+j]);
            for (j = 0; j < defs->ncouples; j++ )
                lparm->P[(i*defs->ncouples)+j] = parm->P[(i*defs->ncouples)+j];
            lparm->h[i] = parm->h[i];
            lparm->tau[i] = parm->tau[i];
            lparm->lambda[i] = parm->lambda[i];
        }
	} else if ( model == HESRR || model == HEURR || model == HEQUENCHING ) {
        for (i=0; i<defs->ngenes; i++ ) {
            lparm->R[i] = parm->R[i];
            for (j=0; j<defs->ngenes; j++ )
                lparm->T[(i*defs->ngenes)+j] = parm->T[(i*defs->ngenes)+j];
            for (j=0; j<defs->egenes; j++ )
                lparm->E[(i*defs->egenes)+j] = parm->E[(i*defs->egenes)+j];
            for (j=0; j<defs->mgenes; j++ )
                lparm->M[(i*defs->mgenes)+j] = parm->M[(i*defs->mgenes)+j];
            for (j = 0; j < defs->ncouples; j++ )
                lparm->P[(i*defs->ncouples)+j] = parm->P[(i*defs->ncouples)+j];
            lparm->h[i] = parm->h[i];
            lparm->tau[i] = parm->tau[i];
            lparm->lambda[i] = parm->lambda[i];
        }
	} else {
        for (i=0; i<defs->ngenes; i++ ) {
            lparm->R[i] = parm->R[i];
            for (j=0; j<defs->ngenes; j++ )
                lparm->T[(i*defs->ngenes)+j] = parm->T[(i*defs->ngenes)+j];
            for (j=0; j<defs->egenes; j++ )
                lparm->E[(i*defs->egenes)+j] = parm->E[(i*defs->egenes)+j];
            for (j=0; j<defs->mgenes; j++ )
                lparm->M[(i*defs->mgenes)+j] = parm->M[(i*defs->mgenes)+j];
            for (j=0; j<defs->ncouples; j++ )
                lparm->P[(i*defs->ncouples)+j] = parm->P[(i*defs->ncouples)+j];
            lparm->h[i] = parm->h[i];
            lparm->tau[i] = parm->tau[i];
            lparm->lambda[i] = parm->lambda[i];
        }
	}
	if ( (defs->diff_schedule == 'A') || (defs->diff_schedule == 'C') ) {
		lparm->d[0] = parm->d[0];
	} else {
		for (i=0; i<defs->ngenes; i++)
			lparm->d[i] = parm->d[i];
	}
	if ( (defs->mob_schedule == 'A') || (defs->mob_schedule == 'C') ) {
		lparm->m[0] = parm->m[0];
	} else {
		for (i=0; i<defs->ngenes; i++)
			lparm->m[i] = parm->m[i];
	}
	if ( (defs->mob_schedule == 'A') || (defs->mob_schedule == 'C') ) {
		lparm->mm[0] = parm->mm[0];
	} else {
		for (i=0; i<defs->ngenes; i++)
			lparm->mm[i] = parm->mm[i];
	}
}

/*** CopyRParm: like CopyParm (and it is used in gradient part)       ******
 *   but take structures by ref                                       ******
 ***************************************************************************/

void CopyRParm(EqParms*Pin,EqParms*Pout)
{
	int i,j;
	for (i=0; i<defs->ngenes; i++ ) {
		Pout->R[i] = Pin->R[i];
		for (j=0; j<defs->ngenes; j++ )
			Pout->T[(i*defs->ngenes)+j] = Pin->T[(i*defs->ngenes)+j];
		for ( j = 0; j < defs->egenes; j++ )
			Pout->E[(i*defs->ngenes)+j] = Pin->E[(i*defs->ngenes)+j];
		for ( j = 0; j < defs->mgenes; j++ )
			Pout->M[(i*defs->ngenes)+j] = Pin->M[(i*defs->ngenes)+j];
		for (j=0; j<defs->ncouples; j++ )
			Pout->P[(i*defs->ncouples)+j] = Pin->P[(i*defs->ncouples)+j];
		Pout->h[i] = Pin->h[i];
		Pout->tau[i] = Pin->tau[i];
		Pout->lambda[i] = Pin->lambda[i];
	}
	if ( (defs->diff_schedule == 'A') || (defs->diff_schedule == 'C') ) {
		Pout->d[0] = Pin->d[0];
	} else {
		for (i=0; i<defs->ngenes; i++)
			Pout->d[i] = Pin->d[i];
	}
	if ( (defs->mob_schedule == 'A') || (defs->mob_schedule == 'C') ) {
		Pout->m[0] = Pin->m[0];
	} else {
		for (i=0; i<defs->ngenes; i++)
			Pout->m[i] = Pin->m[i];
	}
	if ( (defs->mob_schedule == 'A') || (defs->mob_schedule == 'C') ) {
		Pout->mm[0] = Pin->mm[0];
	} else {
		for (i=0; i<defs->ngenes; i++)
			Pout->mm[i] = Pin->mm[i];
	}
}

/*** FreeParm: frees parameter struct accessed by ref **********************
 ***************************************************************************/

void FreeParm(EqParms*P)
{
	free(P->R);
	free(P->T);
	free(P->E);
	free(P->M);
	free(P->P);
	free(P->h);
	free(P->d);
	free(P->lambda);
	free(P->tau);
	free(P->m);
	free(P->mm);
}

/*** DpsidtOrig: implements the equations for Lagrangian multiplies        *
 ***************************************************************************/
void DpsidtOrig(double *v, double t, double *vdot, int n)
{
	int ap;                 /* nuclear index on AP axis [0,1,...,m-1] */
	int        i,j;                                   /* local loop counters */
	int        k;                   /* index of gene k in a specific nucleus */
	int        base;            /* index of first gene in a specific nucleus */
#ifdef ALPHA_DU
	int        incx = 1;        /* increment step size for vsqrt input array */
	int        incy = 1;       /* increment step size for vsqrt output array */
#endif
	double dummy,*vconc;
	int pindex;
	static unsigned int samecycle = 0;            /* same cleavage cycle as before? */
/* get D parameters and bicoid gradient according to cleavage cycle */
/* previously it depended on num_nucs*/
	if (ccycle != samecycle) {
		GetD(t, lparm->d, defs->diff_schedule, D);
		GetRCoef(t, &mit_factor);
		vmat = gcmd_get_maternal_inputs(ccycle);                   /* get bicoid gradient */
		samecycle = ccycle;
	}
	if ( defs->egenes )
		gcmd_get_external_inputs(t, vext, num_nucs * defs->egenes);
	vconc=(double*)calloc(n,sizeof(double));
	CF(t,vconc);
	if (rule == INTERPHASE) {
		register double vinput1 = 0;
		for (base = 0, ap=0; base < n ; base += defs->ngenes, ap++) {
			for (i=base; i < base + defs->ngenes; i++) {
				k = i - base;
				vinput1  = lparm->h[k];
				for(j=0; j < defs->ngenes; j++) {
					dummy = vconc[base+j];
					vinput1 += lparm->T[(k*defs->ngenes)+j] * dummy;
				}
				for(j=0; j < defs->mgenes; j++)
					vinput1  += lparm->M[(k*defs->mgenes)+j] * vmat[ap * defs->mgenes + j];
				for(j=0; j < defs->egenes; j++)
					vinput1  += lparm->E[(k*defs->egenes)+j] * vext[ap * defs->egenes + j];
				bot2[i]   = 1 + vinput1 * vinput1;
				vinput[i] = vinput1;
			}
		}/*vinput done */
/***************************************************************************
 *                                                                         *
 *        g(u) = 1/2 * ( u / sqrt(1 + u^2) + 1)                            *
 *                                                                         *
 ***************************************************************************/
    if ( gofu == Sqrt ) {
                            /* now calculate sqrt(1+u2); store it in bot[] */
#ifdef ALPHA_DU
	vsqrt_(bot2,&incx,bot,&incy,&n);    /* superfast DEC vector function */
	for(i=0; i < n; i++) {                  /* slow traditional style sqrt */
		dummy = bot[i];
		bot3[i] = 1/(dummy*dummy*dummy);
	}
#else
#ifdef MKL
	vdInvSqrt(n, bot2, bot);
	for(i=0; i < n; i++) {
		dummy = bot[i];
		bot3[i] = dummy*dummy*dummy;
	}
#else
	for(i=0; i < n; i++) {                  /* slow traditional style sqrt */
		dummy = sqrt(bot2[i]);
		bot[i] = dummy;
		bot3[i] = 1/(dummy*dummy*dummy);
	}
#endif
#endif
/***************************************************************************
 *                                                                         *
 *        g(u) = 1/2 * (tanh(u) + 1) )                                     *
 *                                                                         *
 ***************************************************************************/
    } else if ( gofu == Tanh ) {
      for(i=0; i < n; i++) {
      	dummy = tanh(vinput[i]);
      	bot3[i] = 1-dummy*dummy;
    	}
    }/*bot3/g1 done*/ else
      error("DpsidtOrig: unknown g(u)");
            /* next loop does the rest of the equation (R, Ds and lambdas) */
                                                 /* store result in vdot[] */
   if ( n == defs->ngenes ) {       /* first part: one nuc, no diffusion */
	register double vdot1, g1;
	for( base=0; base<n; base+=defs->ngenes ) {
	  for( i=base; i<base+defs->ngenes; i++ ) {
	    k       = i - base;
	    vdot1   = lparm->lambda[k] * v[i];
      for (pindex = 0; pindex < defs->ngenes; pindex ++) {
  	    g1      = bot3[base+pindex];
  	    vdot1  -= lparm->T[defs->ngenes*pindex+k] *lparm->R[pindex] * 0.5 * g1 * v[base+pindex]  * mit_factor;
      }
	    vdot[i] = vdot1;
	  }
	}

      } else {                    /* then for multiple nuclei -> diffusion */
	register double vdot1,g1;
	for(i=0; i < defs->ngenes; i++){     /* first anterior-most nucleus */
	  k       = i;
	  vdot1   = lparm->lambda[k] * v[i];
    for (pindex = 0; pindex < defs->ngenes; pindex ++) {
  	  g1      = bot3[pindex];
  	  vdot1  -= lparm->T[defs->ngenes*pindex+k] *lparm->R[pindex] * 0.5 * g1 * v[pindex]  * mit_factor;
    }
	  vdot1  -= D[i] * (v[i + defs->ngenes] - v[i] * boundary);
	  vdot[i] = vdot1;
	}
  base = defs->ngenes;
	  for(i=base; i < base + defs->ngenes; i++){
	    k       = i - base;
	    vdot1   = lparm->lambda[k] * v[i];
      for (pindex = 0; pindex < defs->ngenes; pindex ++) {
  	    g1      = bot3[base+pindex];
  	    vdot1  -= lparm->T[defs->ngenes*pindex+k] *lparm->R[pindex] * 0.5 * g1 * v[base+pindex]  * mit_factor;
      }
	    vdot1  -= D[k] * ((boundary*v[i - defs->ngenes] - v[i]) +
			      (v[i + defs->ngenes] - v[i]) );
	    vdot[i] = vdot1;
	  }
                                                     /* then middle nuclei */
	for(base = 2*defs->ngenes; base < n - 2*defs->ngenes; base += defs->ngenes){
	  for(i=base; i < base + defs->ngenes; i++){
	    k       = i - base;
	    vdot1   = lparm->lambda[k] * v[i];
      for (pindex = 0; pindex < defs->ngenes; pindex ++) {
  	    g1      = bot3[base+pindex];
  	    vdot1  -= lparm->T[defs->ngenes*pindex+k] *lparm->R[pindex] * 0.5 * g1 * v[base+pindex]  * mit_factor;
      }
	    vdot1  -= D[k] * ((v[i - defs->ngenes] - v[i]) + (v[i + defs->ngenes] - v[i]) );
	    vdot[i] = vdot1;
	  }
	}
  base = n - 2*defs->ngenes;
	  for(i=base; i < base + defs->ngenes; i++){
	    k       = i - base;
	    vdot1   = lparm->lambda[k] * v[i];
      for (pindex = 0; pindex < defs->ngenes; pindex ++) {
  	    g1      = bot3[base+pindex];
  	    vdot1  -= lparm->T[defs->ngenes*pindex+k] *lparm->R[pindex] * 0.5 * g1 * v[base+pindex]  * mit_factor;
      }
	    vdot1  -= D[k] * ((v[i - defs->ngenes] - v[i]) +
			      (boundary*v[i + defs->ngenes] - v[i]) );
	    vdot[i] = vdot1;
	  }
	                                   /* last: posterior-most nucleus */
  	base = n - defs->ngenes;
	for(i=base; i < base + defs->ngenes; i++){
	  k       = i - base;
	  vdot1   = lparm->lambda[k] * v[i];
    for (pindex = 0; pindex < defs->ngenes; pindex ++) {
  	  g1      = bot3[base+pindex];
  	  vdot1  -= lparm->T[defs->ngenes*pindex+k] * lparm->R[pindex] * 0.5 * g1 * v[base+pindex]  * mit_factor;
    }
	  vdot1  -= D[k] * (v[i - defs->ngenes] - v[i] * boundary);
	  vdot[i] = vdot1;
	}
      }
/* during mitosis only diffusion and decay happen */
  } else if (rule == MITOSIS) {
    if ( n == defs->ngenes ) {         /* first part: one nuc, no diffusion */
      register double vdot1;
      for( base=0; base<n; base+=defs->ngenes ) {
	for( i=base; i<base+defs->ngenes; i++ ) {
	  k       = i - base;
	  vdot1   = lparm->lambda[k] * v[i];
	  vdot[i] = vdot1;
	}
      }
    } else {                      /* then for multiple nuclei -> diffusion */
      register double vdot1;
      for(i=0; i < defs->ngenes; i++) {      /* first anterior-most nucleus */
	k       = i;
	vdot1   = lparm->lambda[k] * v[i];
	vdot1  -= D[i] * (v[i + defs->ngenes] - v[i] * boundary);
	vdot[i] = vdot1;
     }
  base = defs->ngenes;
	  for(i=base; i < base + defs->ngenes; i++){
	    k       = i - base;
	    vdot1   = lparm->lambda[k] * v[i];
	    vdot1  -= D[k] * ((boundary*v[i - defs->ngenes] - v[i]) + (v[i + defs->ngenes] - v[i]) );
	    vdot[i] = vdot1;
	  }
                                                     /* then middle nuclei */
      for(base = 2*defs->ngenes; base < n - 2*defs->ngenes; base += defs->ngenes) {
	for(i=base; i < base + defs->ngenes; i++) {
	  k       = i - base;
	  vdot1   = lparm->lambda[k] * v[i];
	  vdot1  -= D[k] * ((v[i - defs->ngenes] - v[i]) + (v[i + defs->ngenes] - v[i]) );
	  vdot[i] = vdot1;
	}
      }
  base = n - 2*defs->ngenes;
	  for(i=base; i < base + defs->ngenes; i++){
	    k       = i - base;
	    vdot1   = lparm->lambda[k] * v[i];
	    vdot1  -= D[k] * ((v[i - defs->ngenes] - v[i]) +
			      (boundary*v[i + defs->ngenes] - v[i]) );
	    vdot[i] = vdot1;
	  }
                                           /* last: posterior-most nucleus */
  	base = n - defs->ngenes;
      for(i=base; i < base + defs->ngenes; i++) {
	k       = i - base;
	vdot1   = lparm->lambda[k] * v[i];
	vdot1  -= D[k] * (v[i - defs->ngenes] - v[i] * boundary);
	vdot[i] = vdot1;
      }
    }
  } else
    error("DpsidtOrig: Bad rule %i sent to DvdtOrig", rule);
    free(vconc);
  return;
}

/*** DLdqOrig: implements the kernel for Lagrangian gradient               *
 ***************************************************************************/
void DLdqOrig(double t, double *gradvec)
{
	int        ap;                 /* nuclear index on AP axis [0,1,...,m-1] */
	int        i,j;                                   /* local loop counters */
	int        k;                   /* index of gene k in a specific nucleus */
	int        base;            /* index of first gene in a specific nucleus */
#ifdef ALPHA_DU
	int        incx = 1;        /* increment step size for vsqrt input array */
	int        incy = 1;       /* increment step size for vsqrt output array */
#endif
	static int n;
/* Damn sysadmins! Damn once more!*/
/* I've lost everything up to this point!
* Be carefull with Linux/KDE ! *
* Make backups frequently*/
	double dummy, *vconc, *vpsi, g1, dg1;
	static unsigned int samecycle = 0;            /* same cleavage cycle as before? */
/* get D parameters and bicoid gradient according to cleavage cycle */
/* previously it depended on num_nucs*/
	if (ccycle != samecycle) {
		GetDCoef(t, defs->diff_schedule, D);
		GetRCoef(t, &mit_factor);
		vmat = gcmd_get_maternal_inputs(ccycle);
		n = num_nucs * defs->ngenes;
		samecycle = ccycle;
	}
	if ( defs->egenes )
		gcmd_get_external_inputs(t, vext, num_nucs * defs->egenes);
	vconc=(double*)calloc( n, sizeof(double) );
	vpsi=(double*)calloc (n, sizeof(double) );
	CF( t, vconc );
	CFP( t, vpsi);
	for ( k = 0; k < defs->ngenes; k++) {
		register double vdot1 = 0;
		register double vdot2 = 0;
		for ( i = 0; i < n; i += defs->ngenes ) {
			vdot1 -= vpsi[i+k] * vconc[i+k];
		}
		if ( n != defs->ngenes ) {
			i = 0;
			vdot2 += boundary*D[k]*vpsi[i+k]*(vconc[i+k+defs->ngenes]-vconc[i+k]);
			for( i = defs->ngenes; i < n - defs->ngenes; i += defs->ngenes){
				vdot2+=D[k]*vpsi[i+k]*(vconc[i+k+defs->ngenes]-2*vconc[i+k]+vconc[i+k-defs->ngenes]);
			}
			i=n-defs->ngenes;
			vdot2+=boundary*D[k]*vpsi[i+k]*(vconc[i+k-defs->ngenes]-vconc[i+k]);
		}
		gradvec[k*(defs->ngenes+parm_per_gene)+lambda_parm_extension] = vdot1;
		gradvec[k*(defs->ngenes+parm_per_gene)+d_parm_extension] = vdot2;
	}
	if (rule == MITOSIS) {
		for( k = 0; k < defs->ngenes; k++) {
			for( i = 0; i < d_parm_extension; i++) {
				gradvec[k*(defs->ngenes+parm_per_gene)+i] = 0.0;
			}
		}
	} else if (rule == INTERPHASE) {
		register double vinput1 = 0;
		for (base = 0, ap=0; base < n ; base += defs->ngenes, ap++) {
			for (i=base; i < base + defs->ngenes; i++) {
				k = i - base;
				vinput1  = lparm->h[k];
				for( j = 0; j < defs->ngenes; j++){
					dummy = vconc[base+j];
					vinput1 += lparm->T[(k*defs->ngenes)+j] * dummy;
				}
				for(j=0; j < defs->mgenes; j++)
					vinput1  += lparm->M[(k*defs->mgenes)+j] * vmat[ap * defs->mgenes + j];
				for(j=0; j < defs->egenes; j++)
					vinput1  += lparm->E[(k*defs->egenes)+j] * vext[ap * defs->egenes + j];
				bot2[i] = 1 + vinput1 * vinput1;
				vinput[i] = vinput1;
			}
		}/*vinput done */
/***************************************************************************
 *                                                                         *
 *        g(u) = 1/2 * ( u / sqrt(1 + u^2) + 1)                            *
 *                                                                         *
 ***************************************************************************/
    if ( gofu == Sqrt ) {
                            /* now calculate sqrt(1+u2); store it in bot[] */
#ifdef ALPHA_DU
      vsqrt_(bot2,&incx,bot,&incy,&n);    /* superfast DEC vector function */
      for(i=0; i < n; i++) {                  /* slow traditional style sqrt */
      	dummy = bot[i];
        bot[i] = 1 + vinput[i]/dummy;
        bot3[i] = 1/(dummy*dummy*dummy);
    	}
#else
#ifdef MKL
	vdInvSqrt(n, bot2, bot);
	for(i=0; i < n; i++) {
		dummy = bot[i];
		bot[i] = 1.0 + vinput[i] * dummy;
		bot3[i] = dummy * dummy * dummy;
	}
#else
      for(i=0; i < n; i++) {                  /* slow traditional style sqrt */
      	dummy = sqrt(bot2[i]);
        bot[i] = 1 + vinput[i]/dummy;
        bot3[i] = 1/(dummy*dummy*dummy);
      }
#endif
#endif
/***************************************************************************
 *                                                                         *
 *        g(u) = 1/2 * (tanh(u) + 1) )                                     *
 *                                                                         *
 ***************************************************************************/
    } else if ( gofu == Tanh ) {
      for(i=0; i < n; i++) {                  /* slow traditional style sqrt */
      	dummy = tanh(vinput[i]);
        bot[i] = dummy+1;
      	bot3[i] = 1-dummy*dummy;
    	}
    }/*bot3/g1 done*/ else
      error("DLdqOrig: unknown g(u)");
		for(k = 0; k < defs->ngenes; k++) {
			register double vdotr = 0;
			register double vdoth = 0;
			double *vdotm, *vdote;
			vdotm = (double*)calloc(defs->mgenes, sizeof(double));
			for( j = 0; j < defs->mgenes; j++)
				vdotm[j] = 0.0;
			vdote = (double*)calloc(defs->egenes, sizeof(double));
			for( j = 0; j < defs->egenes; j++)
				vdote[j] = 0.0;
			for( i = 0, ap = 0; i < n; i += defs->ngenes, ap++) {
				g1 = bot[ i + k];
				dg1 = bot3[ i + k];
				vdotr += vpsi[i + k] * 0.5 * g1;
				vdoth += vpsi[i + k] * 0.5 * dg1 * lparm->R[k];
				for( j = 0; j < defs->mgenes; j++)
					vdotm[j] += vpsi[i + k] * 0.5 * dg1 * lparm->R[k] * vmat[ap * defs->mgenes + j];
				for( j = 0; j < defs->egenes; j++)
					vdote[j] += vpsi[i + k] * 0.5 * dg1 * lparm->R[k] * vext[ap * defs->egenes + j];
			}
			gradvec[k*(defs->ngenes+parm_per_gene)+r_parm_extension] = mit_factor*vdotr;
			gradvec[k*(defs->ngenes+parm_per_gene)+h_parm_extension] = mit_factor*vdoth;
			for( j = 0; j < defs->egenes; j++) {
				gradvec[k*(defs->ngenes+parm_per_gene) + e_parm_extension + j] = mit_factor*vdote[j];
			}
			for( j = 0; j < defs->mgenes; j++) {
				gradvec[k*(defs->ngenes + parm_per_gene) + m_parm_extension + j] = mit_factor*vdotm[j];
			}
			for( j = 0; j < defs->ngenes; j++) {
				register double vdott = 0;
				for( i = 0, ap = 0; i < n; i += defs->ngenes, ap++) {
					vdott += vpsi[i + k] * 0.5 * dg1 * lparm->R[k] * vconc[i + j];
				}
				gradvec[k*(defs->ngenes+parm_per_gene)+j] = mit_factor * vdott;
			}
			free(vdote);
			free(vdotm);
		}
	}/*end INTERPHASE*/
	free(vpsi);
	free(vconc);
}

void DLdqOrig_g(double t, double *gradvec, int witch)
{
	int        ap;                 /* nuclear index on AP axis [0,1,...,m-1] */
	int        i,j;                                   /* local loop counters */
	int        k;                   /* index of gene k in a specific nucleus */
	int        base;            /* index of first gene in a specific nucleus */
	static int n;
	double dummy, *vconc, *vpsi, g1, dg1;
	static unsigned int samecycle = 0;            /* same cleavage cycle as before? */
	if (ccycle != samecycle) {
		GetDCoef(t, defs->diff_schedule, D);
		GetRCoef(t, &mit_factor);
		vmat = gcmd_get_maternal_inputs(ccycle);
		n = num_nucs * defs->ngenes;
		samecycle = ccycle;
	}
	if ( defs->egenes ) {
		gcmd_get_external_inputs(t, vext, num_nucs * defs->egenes);
	}
	vconc=(double*)calloc( n, sizeof(double) );
	vpsi=(double*)calloc (n, sizeof(double) );
	CF( t, vconc );
	CFP( t, vpsi);
	for ( k = 0; k < n; k++) {
		vpsi[ k ] = 1;
	}
	for ( k = 0; k < defs->ngenes; k++) {
		if ( witch == k*(defs->ngenes+parm_per_gene)+lambda_parm_extension ) {
			for ( i = 0; i < n; i += defs->ngenes ) {
				gradvec[ i + k ] = -vpsi[ i + k ] * vconc[ i + k ];
			}
		}
		if ( n != defs->ngenes ) {
			if ( witch == k*(defs->ngenes+parm_per_gene)+d_parm_extension ) {
				i = 0;
				gradvec[ i + k ] = boundary * D[k] * vpsi[i+k] * ( vconc[ i + k + defs->ngenes ] - vconc[ i + k ] );
				for( i = defs->ngenes; i < n - defs->ngenes; i += defs->ngenes) {
					gradvec[ i + k ] = D[k] * vpsi[ i + k ] * ( vconc[ i + k + defs->ngenes ] - 2 * vconc[ i + k ] + vconc[ i + k - defs->ngenes ] );
				}
				i = n - defs->ngenes;
				gradvec[ i + k ] = boundary * D[k] * vpsi[ i + k ] * ( vconc[ i + k - defs->ngenes ] - vconc[ i + k ] );
			}
		}
	}
/*	if (rule == MITOSIS) {
		for( k = 0; k < defs->ngenes; k++) {
			for( i = 0; i < d_parm_extension; i++) {
				gradvec[k*(defs->ngenes+parm_per_gene)+i] = 0.0;
			}
		}
	} else */
		if (rule == INTERPHASE) {
			register double vinput1 = 0;
			for (base = 0, ap=0; base < n ; base += defs->ngenes, ap++) {
				for (i=base; i < base + defs->ngenes; i++) {
					k = i - base;
					vinput1  = lparm->h[k];
					for( j = 0; j < defs->ngenes; j++){
						dummy = vconc[base+j];
						vinput1 += lparm->T[(k*defs->ngenes)+j] * dummy;
					}
					for(j=0; j < defs->mgenes; j++) {
						vinput1  += lparm->M[(k*defs->mgenes)+j] * vmat[ap * defs->mgenes + j];
					}
					for(j=0; j < defs->egenes; j++) {
						vinput1  += lparm->E[(k*defs->egenes)+j] * vext[ap * defs->egenes + j];
					}
					bot2[i] = 1 + vinput1 * vinput1;
					vinput[i] = vinput1;
				}
			}/*vinput done */
/***************************************************************************
 *                                                                         *
 *        g(u) = 1/2 * ( u / sqrt(1 + u^2) + 1)                            *
 *                                                                         *
 ***************************************************************************/
		if ( gofu == Sqrt ) {
/* now calculate sqrt(1+u2); store it in bot[] */
#ifdef MKL
			vdInvSqrt(n, bot2, bot);
			for(i=0; i < n; i++) {
				dummy = bot[i];
				bot[i] = 1.0 + vinput[i] * dummy;
				bot3[i] = dummy * dummy * dummy;
			}
#else
			for(i=0; i < n; i++) {                  /* slow traditional style sqrt */
				dummy = sqrt(bot2[i]);
				bot[i] = 1 + vinput[i]/dummy;
				bot3[i] = 1/(dummy*dummy*dummy);
			}
#endif

/***************************************************************************
 *                                                                         *
 *        g(u) = 1/2 * (tanh(u) + 1) )                                     *
 *                                                                         *
 ***************************************************************************/
		} else if ( gofu == Tanh ) {
			for(i=0; i < n; i++) {                  /* slow traditional style sqrt */
				dummy = tanh(vinput[i]);
				bot[i] = dummy+1;
				bot3[i] = 1-dummy*dummy;
			}
		}/*bot3/g1 done*/ else {
			error("DLdqOrig: unknown g(u)");
		}
		for(k = 0; k < defs->ngenes; k++) {
/*			register double vdotr = 0;
			register double vdoth = 0;
			double *vdotm, *vdote;
			vdotm = (double*)calloc(defs->mgenes, sizeof(double));
			for( j = 0; j < defs->mgenes; j++)
				vdotm[j] = 0.0;
			vdote = (double*)calloc(defs->egenes, sizeof(double));
			for( j = 0; j < defs->egenes; j++)
				vdote[j] = 0.0;*/
			for( i = 0, ap = 0; i < n; i += defs->ngenes, ap++) {
				g1 = bot[ i + k];
				dg1 = bot3[ i + k];
				if ( witch == k*(defs->ngenes+parm_per_gene)+r_parm_extension ) {
					gradvec[ i + k ] = vpsi[i + k] * 0.5 * g1;
				}
				if ( witch == k*(defs->ngenes+parm_per_gene)+h_parm_extension ) {
					gradvec[ i + k ] = vpsi[i + k] * 0.5 * dg1 * lparm->R[k];
				}
				for( j = 0; j < defs->mgenes; j++) {
					if ( witch == k*(defs->ngenes+parm_per_gene)+m_parm_extension + j ) {
						gradvec[ i + k ] = vpsi[i + k] * 0.5 * dg1 * lparm->R[k] * vmat[ap * defs->mgenes + j];
					}
				}
				for( j = 0; j < defs->egenes; j++) {
					if ( witch == k*(defs->ngenes+parm_per_gene)+e_parm_extension + j ) {
						gradvec[ i + k ] = vpsi[i + k] * 0.5 * dg1 * lparm->R[k] * vext[ap * defs->egenes + j];
					}
				}
			}
/*			gradvec[k*(defs->ngenes+parm_per_gene)+r_parm_extension] = mit_factor*vdotr;
			gradvec[k*(defs->ngenes+parm_per_gene)+h_parm_extension] = mit_factor*vdoth;*/
/*			for( j = 0; j < defs->egenes; j++) {
				gradvec[k*(defs->ngenes+parm_per_gene) + e_parm_extension + j] = mit_factor*vdote[j];
			}
			for( j = 0; j < defs->mgenes; j++) {
				gradvec[k*(defs->ngenes + parm_per_gene) + m_parm_extension + j] = mit_factor*vdotm[j];
			}*/
			for( j = 0; j < defs->ngenes; j++) {
/*				register double vdott = 0;*/
				if ( witch == k*(defs->ngenes+parm_per_gene) + j ) {
					for( i = 0, ap = 0; i < n; i += defs->ngenes, ap++) {
						gradvec[ i + k ] = vpsi[i + k] * 0.5 * dg1 * lparm->R[k] * vconc[i + j];
					}
				}
/*				gradvec[k*(defs->ngenes+parm_per_gene)+j] = mit_factor * vdott;*/
			}
/*			free(vdote);
			free(vdotm);*/
		}
	}/*end INTERPHASE*/
	free(vpsi);
	free(vconc);
}

/*** GetInteg: sets the array of pointers to the gradient                  *
 ***************************************************************************/

void GetInteg(double**integ)
{
	int i, j;
	for( i = 0; i < defs->ngenes; i++) {
		for( j = 0; j < defs->ngenes; j++) {
			integ[i*(defs->ngenes+parm_per_gene) + j ]=&(grad->T[i*defs->ngenes+j]);
		}
		for( j = 0; j < defs->egenes; j++) {
			integ[i*(defs->ngenes+parm_per_gene) + j + e_parm_extension]=&(grad->E[i*defs->egenes+j]);
		}
		for( j = 0; j < defs->mgenes; j++) {
			integ[i*(defs->ngenes+parm_per_gene) + j + m_parm_extension]=&(grad->M[i*defs->mgenes+j]);
		}
		integ[i*(defs->ngenes+parm_per_gene) + h_parm_extension]=&(grad->h[i]);
		integ[i*(defs->ngenes+parm_per_gene) + r_parm_extension]=&(grad->R[i]);
		integ[i*(defs->ngenes+parm_per_gene) + lambda_parm_extension]=&(grad->lambda[i]);
		integ[i*(defs->ngenes+parm_per_gene) + t_parm_extension]=&(grad->tau[i]);
	}
	if ( (defs->diff_schedule == 'A') || (defs->diff_schedule == 'C') ) {
/* these strange loop is here to make DLdqOrig simplier */
		for( i = 0; i < defs->ngenes; i++) {
			integ[i*(defs->ngenes+parm_per_gene) + d_parm_extension]=&(grad->d[0]);
		}
	} else {
		for( i = 0; i < defs->ngenes; i++) {
			integ[i*(defs->ngenes+parm_per_gene)+d_parm_extension]=&(grad->d[i]);
		}
	}
}


/*** ReNewGrad: sets gradient to 0 (called before calculating the gradient)*
 ***************************************************************************/

void ReNewGrad()
{
	int i,j;
	for(i=0;i<defs->ngenes;i++) {
		grad->R[i]=0.0;
		for(j=0;j<defs->ngenes;j++) {
			grad->T[i*defs->ngenes+j]=0.0;
		}
		for( j = 0; j < defs->egenes; j++) {
			grad->E[i*defs->egenes+j] = 0.0;
		}
		for( j = 0; j < defs->mgenes; j++) {
			grad->M[i*defs->mgenes+j] = 0.0;
		}
		grad->h[i]=0.0;
		grad->lambda[i]=0.0;
		grad->tau[i]=0.0;
	}
	if ( (defs->diff_schedule == 'A') || (defs->diff_schedule == 'C') ) {
		grad->d[0]=0.0;
	} else {
		for(i=0;i<defs->ngenes;i++) {
			grad->d[i]=0.0;
		}
	}
}

void InitZygoteGrad()
{
	parm_per_gene = defs->egenes + defs->mgenes + 5;
	e_parm_extension = defs->ngenes;
	m_parm_extension = defs->ngenes + defs->egenes;
	h_parm_extension = defs->ngenes + defs->egenes + defs->mgenes;
	r_parm_extension = defs->ngenes + defs->egenes + defs->mgenes + 1;
	d_parm_extension = defs->ngenes + defs->egenes + defs->mgenes + 2;
	lambda_parm_extension = defs->ngenes + defs->egenes + defs->mgenes + 3;
	t_parm_extension = defs->ngenes + defs->egenes + defs->mgenes + 4;
	grad = InstallParm();
}
