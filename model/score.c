/*****************************************************************
 *                                                               *
 *   score.c                                                     *
 *                                                               *
 *****************************************************************
 *                                                               *
 *   written by JR, modified by Yoginho                          *
 *   scoregut functions by Yousong Wang, June 2002               *
 *                                                               *
 *****************************************************************
 *                                                               *
 * This file contains fuctions that are needed for reading and   *
 * defining facts/limits and for scoring. The functions defined  *
 * here initialize or manipulate facts or data time tables, read *
 * and initialize limits and penalty (if needed) and do the      *
 * actual scoring by least squares.                              *
 *                                                               *
 *****************************************************************/

#ifdef ICC
#include <mathimf.h>
#else
#include <math.h>
#endif
#include <stdlib.h>
#include <ctype.h>
#include <string.h>

#include <gsl/gsl_sort.h>
#include <gsl/gsl_wavelet.h>

#include <error.h>                      /* for error handling functions */
#include <integrate.h>          /* for blastoderm and EPSILON and stuff */
#include <maternal.h>    /* for bias and bcd reading functions & IGNORE */
#include <score.h>                                         /* obviously */
#include <zygotic.h>        /* for EqParm declaration and GetParameters */

#include <IntegralSolve.h>
#include <ODESolve.h>
#include <ffio.h>
#ifdef LIBSVM
#include <libsvm/svm.h>
#elif FANN
#include <floatfann.h>
#endif
/* Static stuff ************************************************************/

/* some major structs */

static GenoType    *facttype;     /* array of structs that hold facts for  */
			          /* each genotype                         */

static GenoType    *focustype;     /* array of structs that hold facts for  */
			          /* each genotype                         */

static Blastoderm **Flies;
static double *genotype_weight;
static double *genotype_score;
static int *genotype_ndp;

static GenoType    *tt;           /* tt holds the times for which there's  */
                                  /* data (one array for each genotype)    */

static SearchSpace *limits = NULL, *detailed_limits = NULL;       /* structure that holds the limits for   */
                                  /* the search space for the annealer (?) */

static GutInfo     gutparms;      /* Yousong's gutinfo structure used to   */
                                  /* print guts below                      */


/* ... and a couple of minor things */

static double      stepsize;        /* static variable for solver stepsize */
static double      accuracy;        /* static variable for solver accuracy */
static int         ndatapoints = 0;     /* number of data points (for RMS) */
static char        *solvername;
static char        *solvername0;
static char        *filename;                               /* infile name */
static FILE        *slogptr = NULL;             /* solver log file pointer */
/* ... plus an init flag */
static int         tt_init_flag = 0;    /* flag: InitTT called yet or not? */
/* virables added my MacKoel */
static EqParms*parm;
static EqParms*grad;
static double **integ;
static int NumPar;
static double Penalty = 0;                          /* variable for penalty */

static GenoType *derivtype;
static NArrPtr **derivfacts;
static NArrPtr **derivsolution;

static Slist *mat_genotypes;         /* temporary linked list for geno- */

static Derivative2 zygotic_deriv = Dvdt2Orig;
static Derivative zygotic_deriv0 = DvdtOrig;

static int **scoring_window;
static int scoring_window_rows;
static int scoring_window_cols;

static char*scoring_type;
static char*scoring_subtype;
static int scoring_subtype_hint;
static int scoring_freq_i;
static int scoring_freq_j;
static int scoring_freq_k;
static int scoring_freq_s;
static int scoring_freq_n;
static int scoring_freq_g;
static gsl_wavelet*scoring_freq_obj;
static gsl_wavelet_workspace*scoring_freq_wksp;
static double*scoring_freq_data;
static double*scoring_freq_model;
static double*scoring_freq_data_abs;
static size_t *scoring_freq_indices;

static double (*gcdm_score_eval)(NArrPtr*answer, char *genotype);
static double dfacts_lambda;
static ScoreTarget* score_target;

static ChaosData**chaos_data_array;

static int ntime_ignore = 0;
static double *time_ignore_vals;
static double NEED_TIME = -99999;

static int *gene_ignore;

static int const onlyHBFlag = 1;
static double const additionalWeight = 1;
static double const HB_BOUNDS[25] = {46.904, 47.119, 47.333, 47.333, 47.452,
		 	 	 	 	 	 	 37.35,   37.428,  38.214, 38.738, 39.214,
		 	 	 	 	 	 	 45.547, 45.880, 46.142, 46.071, 46.261,
                                	 	 	 	 51.952, 51.857, 52.023, 51.952, 51.571,
		 	 	 	 	 	 	 56.880, 56.785, 56.714, 56.023,  55.523};
static int const HB_B_SIZE = 5;
static int const GT_ITERS_HB_B_SIZE = 5;

static int UNFOLD = 0;
static char *unfold_output_file = NULL;
static char *unfold_time_file = NULL;
static int ndigits = 8;

static int GT_ITER = 1;
static double gHbChisq[5] = {0, 0, 0, 0, 0};

static int focus_score = 0;

/*** INITIALIZATION FUNCTIONS **********************************************/

void set_focus_score(int arg) {
	focus_score = arg;
}

void set_need_time( double arg )
{
	NEED_TIME = arg;
}

void set_unfold(char* arg, char *arg2, int arg3, int NOUNFOLD)
{
	UNFOLD = 1 - NOUNFOLD;
	unfold_output_file = arg;
	unfold_time_file = arg2;
	ndigits = arg3;
}

void gcdm_score_init_eval_bcd(char *type)
{
	scoring_type = (char*)calloc(256, sizeof(char));
	if ( !type ) {
		gcdm_score_eval = Eval_bcd;
		scoring_type = strcpy(scoring_type, "chisq");
	} else if (!strcmp(type, "deriv")) {
		gcdm_score_eval = gcdm_score_eval_deriv;
		scoring_type = strcpy(scoring_type, "deriv");
	} else if (!strcmp(type, "freq")) {
		gcdm_score_eval = gcdm_score_eval_freq;
		scoring_type = strcpy(scoring_type, "freq");
	} else if (!strcmp(type, "freq2d")) {
		gcdm_score_eval = gcdm_score_eval_freq2d_bcd;
		scoring_type = strcpy(scoring_type, "freq2d");
	} else if (!strcmp(type, "lsq")) {
		gcdm_score_eval = gcdm_score_eval_lsq;
		scoring_type = strcpy(scoring_type, "lsq");
	} else if (!strcmp(type, "vst")) {
		gcdm_score_eval = gcdm_score_eval_vst_bcd;
		scoring_type = strcpy(scoring_type, "vst");
	} else if (!strcmp(type, "chisq")) {
		gcdm_score_eval = Eval_bcd;
		scoring_type = strcpy(scoring_type, "chisq");
	} else if (!strcmp(type, "abs")) {
		gcdm_score_eval = EvalABS;
		scoring_type = strcpy(scoring_type, "abs");
	} else if (!strcmp(type, "wpgp")) {
		gcdm_score_eval = wPGP;
		scoring_type = strcpy(scoring_type, "wpgp");
	} else {
		gcdm_score_eval = Eval_bcd;
		scoring_type = strcpy(scoring_type, "chisq");
	}
}

void gcdm_score_init_eval(char *type)
{
	scoring_type = (char*)calloc(256, sizeof(char));
	if ( !type ) {
		gcdm_score_eval = Eval;
		scoring_type = strcpy(scoring_type, "chisq");
	} else if (!strcmp(type, "deriv")) {
		gcdm_score_eval = gcdm_score_eval_deriv;
		scoring_type = strcpy(scoring_type, "deriv");
	} else if (!strcmp(type, "freq")) {
		gcdm_score_eval = gcdm_score_eval_freq;
		scoring_type = strcpy(scoring_type, "freq");
	} else if (!strcmp(type, "freq2d")) {
		gcdm_score_eval = gcdm_score_eval_freq2d;
		scoring_type = strcpy(scoring_type, "freq2d");
	} else if (!strcmp(type, "lsq")) {
		gcdm_score_eval = gcdm_score_eval_lsq;
		scoring_type = strcpy(scoring_type, "lsq");
	} else if (!strcmp(type, "vst")) {
		gcdm_score_eval = gcdm_score_eval_vst;
		scoring_type = strcpy(scoring_type, "vst");
	} else if (!strcmp(type, "chisq")) {
		gcdm_score_eval = Eval;
		scoring_type = strcpy(scoring_type, "chisq");
	} else if (!strcmp(type, "abs")) {
		gcdm_score_eval = EvalABS;
		scoring_type = strcpy(scoring_type, "abs");
	} else if (!strcmp(type, "wpgp")) {
		gcdm_score_eval = wPGP;
		scoring_type = strcpy(scoring_type, "wpgp");
	} else {
		gcdm_score_eval = Eval;
		scoring_type = strcpy(scoring_type, "chisq");
	}
}

void init_scoring_window(FILE*fp, int ndivs)
{
	int i, j, n, m;
	FILE*f;
	f = FindSection(fp, "scoring_window");
	if( !f ) {
		warning("init_scoring_window: cannot locate section");
		scoring_window = NULL;
		scoring_window_rows = 0;
		scoring_window_cols = 0;
		return;
	}
	scoring_window_rows = defs->ndivs + 1;
	scoring_window_cols = 2;
	scoring_window = (int**)calloc(scoring_window_rows, sizeof(int*));
	for ( i = 0, n = 0; i < scoring_window_rows; i++, n++ ) {
		scoring_window[i] = (int*)calloc(scoring_window_cols, sizeof(int));
		for ( j = 0, m = 0; j < scoring_window_cols; j++, m++) {
			if ( 1 != fscanf(f, "%d", &scoring_window[i][j]) ) {
				warning("init_scoring_window: error reading this %d %d", i, j);
				break;
			}
		}
		if ( m != scoring_window_cols )
			error("In init_scoring_window %d != %d", m, scoring_window_cols);
	}
	if ( n != scoring_window_rows )
		error("In init_scoring_window %d != %d", n, scoring_window_rows);
}

void init_scoring_subtype_lsq_bcd (FILE*fp)
{
	int normalize = 0, i;
	InitDerivFacts_bcd(fp);
	for ( i = 0; i < nalleles; i++) {
		gcdm_score_convert_facts(derivtype[i].ptr->facts, normalize);
	}
}

void init_scoring_subtype_lsq (FILE*fp)
{
	int normalize = 0, i;
	InitDerivFacts(fp);
	for ( i = 0; i < nalleles; i++) {
		gcdm_score_convert_facts(derivtype[i].ptr->facts, normalize);
	}
}

void init_scoring_subtype_vst (FILE*fp)
{
	int normalize = 0, i;
	for ( i = 0; i < nalleles; i++) {
		gcdm_score_convert_facts_sqrt(facttype[i].ptr->facts, normalize);
	}
}

void init_scoring_subtype_dfacts (FILE*fp)
{
	int i;
	FILE*f;
	f = FindSection(fp, "scoring_dfacts");
	if( !f ) {
		warning("init_scoring_subtype_dfacts: cannot locate section");
		dfacts_lambda = 0.0;
	} else {
		if ( 1 != fscanf(f, "%lf", &dfacts_lambda) ) {
			warning("init_scoring_subtype_dfacts: error reading this lambda");
			dfacts_lambda = 0.0;
		}
	}
	for ( i = 0; i < nalleles; i++) {
		gcdm_score_add_dfacts(facttype[i].ptr->facts);
	}
}

void init_scoring_subtype_bcd(FILE*fp)
{
	FILE*f;
	char*record;
	int i;
	if ( !gcdm_score_eval ) {
		gcdm_score_eval = Eval;
	}
	if ( !scoring_type ) {
		scoring_type = (char*)calloc(256, sizeof(char));
		scoring_type = strcpy(scoring_type, "chisq");
	}
/*	if ( !strcmp(scoring_type, "lsq_s") || !strcmp(scoring_type, "chisq_s") ) {
		init_scoring_subtype_lsq_ (fp);
	}*/
	if ( !strcmp(scoring_type, "lsq") ) {
		init_scoring_subtype_lsq_bcd (fp);
		return;
	} else if ( !strcmp(scoring_type, "vst") ) {
		init_scoring_subtype_vst (fp);
		return;
	}
	if ( strcmp(scoring_type, "freq") && strcmp(scoring_type, "freq2d") ) {
		return;
	}
	f = FindSection(fp, "scoring_subtype");
	if( !f ) {
		error("init_scoring_subtype: cannot locate section");
	}

	record = (char*)calloc(MAX_RECORD, sizeof(char));
	fscanf(fp, "%*s\n");                    /* advance past first title line */
	record = fgets(record, MAX_RECORD, fp);
	if ( !strncmp(record, "daubechies", 10) ) {
		scoring_subtype = (char*)calloc(MAX_RECORD, sizeof(char));
		scoring_subtype = strcpy(scoring_subtype, "daubechies");
	} else if ( !strncmp(record, "haar", 4) ) {
		scoring_subtype = (char*)calloc(MAX_RECORD, sizeof(char));
		scoring_subtype = strcpy(scoring_subtype, "haar");
	} else if ( !strncmp(record, "bspline", 7) ) {
		scoring_subtype = (char*)calloc(MAX_RECORD, sizeof(char));
		scoring_subtype = strcpy(scoring_subtype, "bspline");
	} else {
		error("Unknown freq type %s", record);
	}
	fscanf(fp, "%*s\n");                    /* advance past first title line */
	record = fgets(record, MAX_RECORD, fp);
	sscanf(record, "%d\n", &scoring_subtype_hint);
	fscanf(fp, "%*s\n");                    /* advance past first title line */
	record = fgets(record, MAX_RECORD, fp);
	sscanf(record, "%d %d %d %d\n", &scoring_freq_k, &scoring_freq_i, &scoring_freq_j, &scoring_freq_s);

	if ( !strncmp(scoring_subtype, "daubechies", 10) ) {
		if ( scoring_subtype_hint == 0 ) {
			scoring_freq_obj = gsl_wavelet_alloc (gsl_wavelet_daubechies, (size_t) scoring_freq_k);
		} else {
			scoring_freq_obj = gsl_wavelet_alloc (gsl_wavelet_daubechies_centered, (size_t) scoring_freq_k);
		}
	} else if ( !strncmp(scoring_subtype, "haar", 10) ) {
		if ( scoring_subtype_hint == 0 ) {
			scoring_freq_obj = gsl_wavelet_alloc (gsl_wavelet_haar, (size_t) scoring_freq_k);
		} else {
			scoring_freq_obj = gsl_wavelet_alloc (gsl_wavelet_haar_centered, (size_t) scoring_freq_k);
		}
	} else if ( !strncmp(scoring_subtype, "bspline", 10) ) {
		scoring_freq_k = 100 * scoring_freq_i + scoring_freq_j;
		if ( scoring_subtype_hint == 0 ) {
			scoring_freq_obj = gsl_wavelet_alloc (gsl_wavelet_bspline, (size_t) scoring_freq_k);
		} else {
			scoring_freq_obj = gsl_wavelet_alloc (gsl_wavelet_bspline_centered, (size_t) scoring_freq_k);
		}
	} else {
		error("Unknown freq type %s", record);
	}
	scoring_freq_n = 1;
	while ( scoring_freq_n < defs->nnucs ) {
		scoring_freq_n *= 2 ;
	}
	if ( !strcmp(scoring_type, "freq") ) {
		scoring_freq_wksp = gsl_wavelet_workspace_alloc ((size_t) scoring_freq_n);
		scoring_freq_model = (double*)calloc(scoring_freq_n, sizeof(double));
		scoring_freq_data = (double*)calloc(scoring_freq_n, sizeof(double));
		scoring_freq_data_abs = (double*)calloc(scoring_freq_n, sizeof(double));
		scoring_freq_indices = (size_t*)calloc(scoring_freq_n, sizeof(size_t));
		for ( i = 0; i < scoring_freq_n; i++ ) {
			scoring_freq_data[i] = 0;
			scoring_freq_model[i] = 0;
		}
	} else if ( !strcmp(scoring_type, "freq2d") ) {
/*		scoring_freq_g = 1;
		while ( scoring_freq_g < defs->ngenes ) {
			scoring_freq_g *= 2 ;
		}*/
		scoring_freq_g = scoring_freq_n;
		scoring_freq_wksp = gsl_wavelet_workspace_alloc ((size_t) scoring_freq_g * scoring_freq_n);
		scoring_freq_model = (double*)calloc(scoring_freq_g * scoring_freq_n, sizeof(double));
		scoring_freq_data = (double*)calloc(scoring_freq_g * scoring_freq_n, sizeof(double));
		scoring_freq_data_abs = (double*)calloc(scoring_freq_g * scoring_freq_n, sizeof(double));
		scoring_freq_indices = (size_t*)calloc(scoring_freq_g * scoring_freq_n, sizeof(size_t));
		for ( i = 0; i < scoring_freq_g * scoring_freq_n; i++ ) {
			scoring_freq_data[i] = 0;
			scoring_freq_model[i] = 0;
		}
	}
	derivfacts = (NArrPtr **)calloc(nalleles, sizeof(NArrPtr *));
	for ( i = 0; i < nalleles; i++) {
		derivfacts[i] = Facts2Solution(facttype[i].ptr->facts);
	}

}

void init_scoring_subtype(FILE*fp)
{
	FILE*f;
	char*record;
	int i;
	if ( !gcdm_score_eval ) {
		gcdm_score_eval = Eval;
	}
	if ( !scoring_type ) {
		scoring_type = (char*)calloc(256, sizeof(char));
		scoring_type = strcpy(scoring_type, "chisq");
	}
/*	if ( !strcmp(scoring_type, "lsq_s") || !strcmp(scoring_type, "chisq_s") ) {
		init_scoring_subtype_lsq_ (fp);
	}*/
	if ( !strcmp(scoring_type, "lsq") ) {
		init_scoring_subtype_lsq (fp);
		return;
	} else if ( !strcmp(scoring_type, "vst") ) {
//		init_scoring_subtype_vst (fp);
		return;
	}
	if ( strcmp(scoring_type, "freq") && strcmp(scoring_type, "freq2d") ) {
		return;
	}
	f = FindSection(fp, "scoring_subtype");
	if( !f ) {
		error("init_scoring_subtype: cannot locate section");
	}

	record = (char*)calloc(MAX_RECORD, sizeof(char));
	fscanf(fp, "%*s\n");                    /* advance past first title line */
	record = fgets(record, MAX_RECORD, fp);
	if ( !strncmp(record, "daubechies", 10) ) {
		scoring_subtype = (char*)calloc(MAX_RECORD, sizeof(char));
		scoring_subtype = strcpy(scoring_subtype, "daubechies");
	} else if ( !strncmp(record, "haar", 4) ) {
		scoring_subtype = (char*)calloc(MAX_RECORD, sizeof(char));
		scoring_subtype = strcpy(scoring_subtype, "haar");
	} else if ( !strncmp(record, "bspline", 7) ) {
		scoring_subtype = (char*)calloc(MAX_RECORD, sizeof(char));
		scoring_subtype = strcpy(scoring_subtype, "bspline");
	} else {
		error("Unknown freq type %s", record);
	}
	fscanf(fp, "%*s\n");                    /* advance past first title line */
	record = fgets(record, MAX_RECORD, fp);
	sscanf(record, "%d\n", &scoring_subtype_hint);
	fscanf(fp, "%*s\n");                    /* advance past first title line */
	record = fgets(record, MAX_RECORD, fp);
	sscanf(record, "%d %d %d %d\n", &scoring_freq_k, &scoring_freq_i, &scoring_freq_j, &scoring_freq_s);

	if ( !strncmp(scoring_subtype, "daubechies", 10) ) {
		if ( scoring_subtype_hint == 0 ) {
			scoring_freq_obj = gsl_wavelet_alloc (gsl_wavelet_daubechies, (size_t) scoring_freq_k);
		} else {
			scoring_freq_obj = gsl_wavelet_alloc (gsl_wavelet_daubechies_centered, (size_t) scoring_freq_k);
		}
	} else if ( !strncmp(scoring_subtype, "haar", 10) ) {
		if ( scoring_subtype_hint == 0 ) {
			scoring_freq_obj = gsl_wavelet_alloc (gsl_wavelet_haar, (size_t) scoring_freq_k);
		} else {
			scoring_freq_obj = gsl_wavelet_alloc (gsl_wavelet_haar_centered, (size_t) scoring_freq_k);
		}
	} else if ( !strncmp(scoring_subtype, "bspline", 10) ) {
		scoring_freq_k = 100 * scoring_freq_i + scoring_freq_j;
		if ( scoring_subtype_hint == 0 ) {
			scoring_freq_obj = gsl_wavelet_alloc (gsl_wavelet_bspline, (size_t) scoring_freq_k);
		} else {
			scoring_freq_obj = gsl_wavelet_alloc (gsl_wavelet_bspline_centered, (size_t) scoring_freq_k);
		}
	} else {
		error("Unknown freq type %s", record);
	}
	scoring_freq_n = 1;
	while ( scoring_freq_n < defs->nnucs ) {
		scoring_freq_n *= 2 ;
	}
	if ( !strcmp(scoring_type, "freq") ) {
		scoring_freq_wksp = gsl_wavelet_workspace_alloc ((size_t) scoring_freq_n);
		scoring_freq_model = (double*)calloc(scoring_freq_n, sizeof(double));
		scoring_freq_data = (double*)calloc(scoring_freq_n, sizeof(double));
		scoring_freq_data_abs = (double*)calloc(scoring_freq_n, sizeof(double));
		scoring_freq_indices = (size_t*)calloc(scoring_freq_n, sizeof(size_t));
		for ( i = 0; i < scoring_freq_n; i++ ) {
			scoring_freq_data[i] = 0;
			scoring_freq_model[i] = 0;
		}
	} else if ( !strcmp(scoring_type, "freq2d") ) {
/*		scoring_freq_g = 1;
		while ( scoring_freq_g < defs->ngenes ) {
			scoring_freq_g *= 2 ;
		}*/
		scoring_freq_g = scoring_freq_n;
		scoring_freq_wksp = gsl_wavelet_workspace_alloc ((size_t) scoring_freq_g * scoring_freq_n);
		scoring_freq_model = (double*)calloc(scoring_freq_g * scoring_freq_n, sizeof(double));
		scoring_freq_data = (double*)calloc(scoring_freq_g * scoring_freq_n, sizeof(double));
		scoring_freq_data_abs = (double*)calloc(scoring_freq_g * scoring_freq_n, sizeof(double));
		scoring_freq_indices = (size_t*)calloc(scoring_freq_g * scoring_freq_n, sizeof(size_t));
		for ( i = 0; i < scoring_freq_g * scoring_freq_n; i++ ) {
			scoring_freq_data[i] = 0;
			scoring_freq_model[i] = 0;
		}
	}
	derivfacts = (NArrPtr **)calloc(nalleles, sizeof(NArrPtr *));
	for ( i = 0; i < nalleles; i++) {
		derivfacts[i] = Facts2Solution(facttype[i].ptr->facts);
	}

}

void init_time_ignore(FILE*fp)
{
	int i, n;
	FILE*f;
	f = FindSection(fp, "time_ignore");
	if( !f ) {
		warning("init_time_ignore: cannot locate section");
		time_ignore_vals = NULL;
		ntime_ignore = 0;
		return;
	}
	if ( 1 != fscanf(f, "%d", &ntime_ignore) ) {
		warning("init_time_ignore: error reading");
	}
	time_ignore_vals = (double*)calloc(ntime_ignore, sizeof(double));
	for ( i = 0; i < ntime_ignore; i++ ) {
		if ( 1 != fscanf(f, "%lf", &time_ignore_vals[i]) ) {
			warning("init_time_ignore: error reading this %d", i);
			break;
		}
	}
}

void init_gene_ignore(FILE*fp)
{
	int i, n;
	FILE*f;
	gene_ignore = (int*)calloc( defs->ngenes, sizeof(int));
	f = FindSection(fp, "gene_ignore");
	if( !f ) {
		warning("init_gene_ignore: cannot locate section");
		for ( i = 0; i < defs->ngenes; i++ ) {
            gene_ignore[i] = 0;
		}
		return;
	}
	for ( i = 0; i < defs->ngenes; i++ ) {
		if ( 1 != fscanf(f, "%d", &gene_ignore[i]) ) {
			warning("init_gene_ignore: error reading this %d", i);
			break;
		}
	}
}

int check_scoring_window(int lineage)
{
	int i, j, n;
	if ( !scoring_window_rows || !scoring_window_cols || !scoring_window ) {
		return 1;
	}
	n = 0;
	for ( i = 0; i < scoring_window_rows; i++ ) {
		for ( j = 0; j < scoring_window_cols; j += 2 ) {
			if ( scoring_window[i][j] <= lineage && lineage <= scoring_window[i][j + 1] ) {
				n = 1;
			}
		}
	}
	return n;
}

double*ReadGenotypeWeights(FILE*fp, int na)
{
	double*weights;
	int n, i;
	FILE*f;
	weights = (double*)calloc(na, sizeof(double));
	f = FindSection(fp, "genotype_weights");                   /* find limits section */
	if( !f ) {
		warning("ReadGenotypeWeights: cannot locate section");
		for ( i = 0; i < na; i++)
			weights[i] = 1.0;
		return weights;
	}
	for ( i = 0, n = 0; i < na; i++, n++) {
		if ( 1 != fscanf(f, "%lf\n", &weights[i]) )
			break;
	}
	if ( n != na )
		error("Number of weights %d doesn't match that of genotypes!", n);
	return weights;
}

/*** InitScoring: intializes a) facts-related structs and TTimes and *******
 *                           b) parameter ranges for the Score function.   *
 ***************************************************************************/

void InitScoring_bcd(FILE *fp)
{
	mat_genotypes = GetMatGenotypes(&nalleles);
	genotype_weight = ReadGenotypeWeights(fp, nalleles);
	genotype_score = (double*)calloc(nalleles, sizeof(double));
	genotype_ndp = (int*)calloc(nalleles, sizeof(int));
	InitFacts_bcd(fp);                     /* installs facts static into score.c */
	InitLimits(fp);                    /* installs search space into score.c */
	InitFlies(fp);
/*added by MacKoel*/
	parm = GetParameters();
/*****************/
}

/*** InitScoring: intializes a) facts-related structs and TTimes and *******
 *                           b) parameter ranges for the Score function.   *
 ***************************************************************************/

void InitScoring(FILE *fp)
{
	mat_genotypes = GetMatGenotypes(&nalleles);
	genotype_weight = ReadGenotypeWeights(fp, nalleles);
	genotype_score = (double*)calloc(nalleles, sizeof(double));
	genotype_ndp = (int*)calloc(nalleles, sizeof(int));
	InitFacts(fp);                     /* installs facts static into score.c */
	InitLimits(fp);                    /* installs search space into score.c */
	InitFlies(fp);
/*added by MacKoel*/
	parm = GetParameters();
/*****************/
}

void gcdm_score_init_bcd(FILE *fp)
{
	mat_genotypes = GetMatGenotypes(&nalleles);
	genotype_weight = ReadGenotypeWeights(fp, nalleles);
	genotype_score = (double*)calloc(nalleles, sizeof(double));
	genotype_ndp = (int*)calloc(nalleles, sizeof(int));
	init_scoring_window (fp, defs->ndivs);
	init_time_ignore (fp);
	InitFacts(fp);                     /* installs facts static into score.c */
	InitLimits(fp);                    /* installs search space into score.c */
	gcdm_score_init_flies(fp);
/*added by MacKoel*/
	parm = GetParameters();
/*****************/
	init_scoring_subtype_bcd(fp);
}

void gcdm_score_init(FILE *fp)
{
	mat_genotypes = GetMatGenotypes(&nalleles);
	genotype_weight = ReadGenotypeWeights(fp, nalleles);
	genotype_score = (double*)calloc(nalleles, sizeof(double));
	genotype_ndp = (int*)calloc(nalleles, sizeof(int));
	init_scoring_window (fp, defs->ndivs);
	init_time_ignore (fp);
	init_gene_ignore (fp);
	InitFacts(fp);                     /* installs facts static into score.c */
	if (focus_score == 1) {
		InitFocus(fp);                     /* installs focus static into score.c */
	}
	InitLimits(fp);                    /* installs search space into score.c */
	gcdm_score_init_flies(fp);
/*added by MacKoel*/
	parm = GetParameters();
/*****************/
	init_scoring_subtype(fp);
}

void Scoring_setSolverName(char*name)
{
/*
  if (!solvername) solvername = (char*)calloc(MAX_RECORD,sizeof(char));
  solvername = strcpy(solvername,name);
*/
	solvername = name;
}

void gcdm_score_set_solver(int rhs, char*name)
{
	if ( rhs == 0 )
		solvername = name;
	else if ( rhs == 1 )
		solvername0 = name;
}

int *gcdm_get_ndp()
{
	return genotype_ndp;
}

double *gcdm_get_score()
{
	return genotype_score;
}

double *gcdm_get_hb_bounds_diff()
{
	return gHbChisq;
}

/*** InitFacts: puts facts records into the appropriate DataTable. *********
 *              Returns a pointer to a DataTable, which in turn points to  *
 *              a sized array of DataRecords, one for each time step.      *
 ***************************************************************************/

void InitFacts_bcd(FILE *fp)
{
	int               i;                               /* local loop counter */
	Dlist             *inlist;            /* temporary linked list for facts */
	Slist             *current;                      /* types from data file */
	int ndp = 0;
	if ( !(facttype=(GenoType *)calloc(nalleles, sizeof(GenoType))) )
		error("InitFacts: could not allocate facttype struct");
/*** for loop: read the data for each genotype *****************************/
	ndatapoints = 0;
	for( current = mat_genotypes, i = 0; current; current = current->next, i++) {
		ndp = 0;
		if( !( inlist = ReadData(fp, current->fact_section, &ndp, defs->ngenes + 1)) )
			error("InitFacts: no Dlist to initialize facts");
		else {
			if ( !( facttype[i].genotype=(char *)calloc(MAX_RECORD, sizeof(char))))
				error("InitFacts: could not allocate facts genotype string");
			facttype[i].genotype = strcpy(facttype[i].genotype, current->name);
			facttype[i].ptr = (DataPtr*)malloc(sizeof(DataPtr));
			facttype[i].ptr->facts = List2Facts_bcd(inlist, &ndp);
			free_Dlist(inlist);
			genotype_ndp[i] = ndp;
			ndatapoints += ndp;
		}
	}
	InitTTs();
}

/*** InitFacts: puts facts records into the appropriate DataTable. *********
 *              Returns a pointer to a DataTable, which in turn points to  *
 *              a sized array of DataRecords, one for each time step.      *
 ***************************************************************************/

void InitFacts(FILE *fp)
{
	int               i;                               /* local loop counter */
	Dlist             *inlist;            /* temporary linked list for facts */
	Slist             *current;                      /* types from data file */
	int ndp = 0;
	if ( !(facttype=(GenoType *)calloc(nalleles, sizeof(GenoType))) )
		error("InitFacts: could not allocate facttype struct");
/*** for loop: read the data for each genotype *****************************/
	ndatapoints = 0;
	for( current = mat_genotypes, i = 0; current; current = current->next, i++) {
		ndp = 0;
		if( !( inlist = ReadData(fp, current->fact_section, &ndp, defs->ngenes + 1)) )
			error("InitFacts: no Dlist to initialize facts");
		else {
			if ( !( facttype[i].genotype=(char *)calloc(MAX_RECORD, sizeof(char))))
				error("InitFacts: could not allocate facts genotype string");
			facttype[i].genotype = strcpy(facttype[i].genotype, current->name);
			facttype[i].ptr = (DataPtr*)malloc(sizeof(DataPtr));
			facttype[i].ptr->facts = List2Facts(inlist, &ndp);
			free_Dlist(inlist);
			genotype_ndp[i] = ndp;
			ndatapoints += ndp;
		}
	}
	InitTTs();
}

void InitFocus(FILE *fp)
{
	int               i;                               /* local loop counter */
	Dlist             *inlist;            /* temporary linked list for facts */
	Slist             *current;                      /* types from data file */
	int ndp = 0;
	if ( !(focustype=(GenoType *)calloc(nalleles, sizeof(GenoType))) )
		error("InitFocus: could not allocate facttype struct");
/*** for loop: read the data for each genotype *****************************/
	for( current = mat_genotypes, i = 0; current; current = current->next, i++) {
		if( !( inlist = ReadData(fp, "current->focus_section", &ndp, defs->ngenes + 1)) )
			warning("InitFocus: no Dlist to initialize facts");
		else {
			if ( !( focustype[i].genotype=(char *)calloc(MAX_RECORD, sizeof(char))))
				error("InitFocus: could not allocate focus genotype string");
			focustype[i].genotype = strcpy(focustype[i].genotype, current->name);
			focustype[i].ptr = (DataPtr*)malloc(sizeof(DataPtr));
			focustype[i].ptr->facts = List2Facts(inlist, &ndp);
			free_Dlist(inlist);
		}
	}
}

void set_deriv(Derivative2 arg)
{
	zygotic_deriv = arg;
}

void set_deriv0(Derivative arg)
{
	zygotic_deriv0 = arg;
}

void InitFlies(FILE *fp)
{
	int               i;                               /* local loop counter */
	Slist             *current;                      /* types from data file */
	if ( !(Flies=(Blastoderm**)calloc(nalleles, sizeof(Blastoderm*))) )
		error("InitFlies: could not allocate Flies struct");
	for ( current = mat_genotypes, i = 0; current; current = current->next, i++) {
		Flies[i] = Blastoderm_create(current->name, tt[i].ptr->times, NULL, zygotic_deriv, NULL, NULL, NULL, solvername);
	}
}

void gcdm_score_init_flies(FILE *fp)
{
	int               i;                               /* local loop counter */
	Slist             *current;                      /* types from data file */
	if ( !(Flies=(Blastoderm**)calloc(nalleles, sizeof(Blastoderm*))) )
		error("InitFlies: could not allocate Flies struct");
	for ( current = mat_genotypes, i = 0; current; current = current->next, i++) {
		Flies[i] = gcdm_blastoderm_new(current->name, tt[i].ptr->times, zygotic_deriv0, zygotic_deriv, NULL, DpsidtOrig, NULL, solvername0, solvername);
	}
}

void gcdm_score_set_hints(double dosage, int zero_bias, int rhs)
{
	int               i;                               /* local loop counter */
	Slist             *current;                      /* types from data file */
	for ( current = mat_genotypes, i = 0; current; current = current->next, i++) {
		gcdm_blastoderm_set_hints(Flies[i], dosage, zero_bias, rhs);
	}
}

/*** InitLimits: reads limits section from the data file into the struct ***
 *               limits, which is static to score.c. Then, it initializes  *
 *               the penalty function if necessary.                        *
 *   NOTE:       lambda limits are stored as protein half lives in the     *
 *               data file and therefore get converted upon reading        *
 ***************************************************************************/
void InitLimits(FILE *fp)
{
	limits = ReadLimits(fp);
	if ( limits == NULL )
		error("Couldn't initialise limits");
	if ( limits->pen_vec != NULL ) {
		InitPenalty(fp);         /* installs mmax and vmax into penalty vector */
		detailed_limits = ReReadLimits(fp, "detailed_limits");
	}
}

/*** InitPenalty: initializes vmax[] and mmax static to score.c; these *****
 *                variables are used to calculate the penalty function     *
 *                for scoring.                                             *
 *         NOTE: Should only get called, if penalty function is used       *
 ***************************************************************************/
void InitPenalty(FILE *fp)
{
	int       i, j;                                   /* local loop counters */
	char      *g_type;         /* genotype string of penalty data to be read */
	char      *factpen_section;             /* title of penalty data section */
	char      *bcdpen_section;     /* title of maternal penalty data section */
	Slist     *s_current;
	Dlist     *d_inlist;             /* linked list for reading penalty data */
	Dlist     *d_current;
	Blist     *b_inlist;                 /* linked list for reading maternal */
	Blist     *b_current;                                    /* penalty data */
	double    *vmmax;      /* max. bcd protein conc. in maternal penalty data */
	double    *vmax;           /* array for max. prot concs. in penalty data */
	double    *vemax;      /* max. bcd protein conc. in maternal penalty data */
	int       ndp = 0;  /* dummy for ReadData, no need to count datapts here */
	factpen_section = (char *)calloc(MAX_RECORD, sizeof(char));
	bcdpen_section  = (char *)calloc(MAX_RECORD, sizeof(char));
	g_type          = (char *)calloc(MAX_RECORD, sizeof(char));
	vmmax   = (limits->pen_vec) + 1;               /* locate mmax and vmax */
	vmax   =   (limits->pen_vec) + 1 + defs->mgenes;
	vemax =   (limits->pen_vec) + 1 + defs->mgenes + defs->ngenes;
	for ( i = 0; i < defs->mgenes; i++ )
		vmmax[i] = -1.;
	for ( i = 0; i < defs->ngenes; i++ )
		vmax[i] = -1.;
	for ( i = 0; i < defs->egenes; i++ )
		vemax[i] = -1.;
/* loop to read penalty data for all genotypes */
	for( s_current = mat_genotypes; s_current; s_current = s_current->next) {
		g_type = strcpy(g_type, s_current->name);
/* this part reads the facts data */
		if( !( d_inlist = ReadData(fp, s_current->fact_section, &ndp, defs->ngenes + 1)) )
			error("InitPenalty: no Dlist to initialize penalty");
		else {
			for ( d_current=d_inlist; d_current; d_current=d_current->next )
				for ( j=0; j < defs->ngenes; j++ )
					if ( d_current->d[j+1] > vmax[j] )
						vmax[j] = d_current->d[j+1];
			free_Dlist(d_inlist);
		}
/* read additional penalty sections if necessary */
		sprintf(factpen_section, "penalty_data.%s", g_type);
		if ( FindSection(fp, factpen_section) ) {
			if ( (d_inlist=ReadData(fp, factpen_section, &ndp, defs->ngenes + 1)) ) {
				for ( d_current=d_inlist; d_current; d_current=d_current->next )
					for ( j=0; j < defs->ngenes; j++ )
						if ( d_current->d[j+1] > vmax[j] )
							vmax[j] = d_current->d[j+1];
			} else {
				error("InitPenalty: error reading penalty section for genotype %s", g_type);
			}
			free_Dlist(d_inlist);
		}
/* just in case if all values are -1 */
		for ( i = 0; i < defs->ngenes; i++ )
			if( vmax[i] == -1. )
				vmax[i] = 255.;                          /* set max value to 255 */
/* this part reads the facts data */
		if( defs->egenes && !( d_inlist = ReadData(fp, s_current->ext_section, &ndp, defs->egenes + 1)) )
			error("InitPenalty: no Dlist to initialize penalty");
		else if ( defs->egenes ) {
			for ( d_current=d_inlist; d_current; d_current=d_current->next )
				for ( j=0; j < defs->egenes; j++ )
					if ( d_current->d[j+1] > vemax[j] )
						vemax[j] = d_current->d[j+1];
			free_Dlist(d_inlist);
		}
/* read additional penalty sections if necessary */
		sprintf(factpen_section, "external_penalty_data.%s", g_type);
		if ( FindSection(fp, factpen_section) ) {
			if ( (d_inlist=ReadData(fp, factpen_section, &ndp, defs->egenes + 1)) ) {
				for ( d_current=d_inlist; d_current; d_current=d_current->next )
					for ( j=0; j < defs->egenes; j++ )
						if ( d_current->d[j+1] > vemax[j] )
							vemax[j] = d_current->d[j+1];
			} else {
				error("InitPenalty: error reading penalty section for genotype %s", g_type);
			}
			free_Dlist(d_inlist);
		}
/* just in case if all values are -1 */
		for ( i = 0; i < defs->egenes; i++ )
			if( vemax[i] == -1. )
				vemax[i] = 255.;                          /* set max value to 255 */
/* this part reads the facts data */
		if( defs->mgenes && !( d_inlist = ReadData(fp, s_current->mat_section, &ndp, defs->mgenes)) )
			error("InitPenalty: no Dlist to initialize penalty");
		else if ( defs->mgenes ) {
			for ( d_current=d_inlist; d_current; d_current=d_current->next )
				for ( j=0; j < defs->mgenes; j++ )
					if ( d_current->d[j] > vmmax[j] )
						vmmax[j] = d_current->d[j];
			free_Dlist(d_inlist);
		}
/* read additional penalty sections if necessary */
		sprintf(factpen_section, "maternal_penalty_data.%s", g_type);
		if ( FindSection(fp, factpen_section) ) {
			if ( (d_inlist=ReadData(fp, factpen_section, &ndp, defs->mgenes)) ) {
				for ( d_current=d_inlist; d_current; d_current=d_current->next )
					for ( j=0; j < defs->mgenes; j++ )
						if ( d_current->d[j] > vmmax[j] )
							vmmax[j] = d_current->d[j];
			} else {
				error("InitPenalty: error reading penalty section for genotype %s", g_type);
			}
			free_Dlist(d_inlist);
		}
/* just in case if all values are -1 */
		for ( i = 0; i < defs->mgenes; i++ )
			if( vmmax[i] == -1. )
				vmmax[i] = 255.;                          /* set max value to 255 */
	}
	free(factpen_section);
	free(bcdpen_section);
	free(g_type);
}

/*** InitTTs: Initializes the time points for which we need model output ***
 *            i.e. the time points for which we have data.                 *
 *      NOTE: we do not allow data at t=0 but it still gets included into  *
 *            TTs                                                          *
 ***************************************************************************/

void InitTTs()
{
	int      i, j;                                    /* local loop counters */
/* tt.ptr.times is a DArrPtr static to score.c.                            */
/* Each DArrPtr in the array points to an array of times for which we have */
/* data for each genotype (if you know what I mean...)                     */
	if ( !tt_init_flag ) {
		if ( !(tt=(GenoType *)calloc(nalleles, sizeof(GenoType))) )
			error("InitTTs: could not allocate tt struct");
/* the following loop copies the times from the Facts section of GenoTab   */
/* to the tt array of DArrPtrs                                             */
		for(i=0; i < nalleles; i++) {
			if (!(tt[i].genotype = (char *)calloc(MAX_RECORD, sizeof(char))))
				error("InitTTs: could not allocate tt genotype string");
			tt[i].genotype = strcpy(tt[i].genotype, facttype[i].genotype);
			tt[i].ptr = (DataPtr*)malloc(sizeof(DataPtr));
			tt[i].ptr->times = (DArrPtr*)malloc(sizeof(DArrPtr));
			tt[i].ptr->times->size = 1 + facttype[i].ptr->facts->size;
			if ( !(tt[i].ptr->times->array = (double *)calloc(tt[i].ptr->times->size, sizeof(double))) )
				error("InitTTs: could not allocate bt array"); ;
			tt[i].ptr->times->array[0] = 0.;
			for( j = 1; j < tt[i].ptr->times->size; j++)
				tt[i].ptr->times->array[j] = facttype[i].ptr->facts->record[j-1].time;
		}
		tt_init_flag = 1;         /* static flag (to score.c): TTs now defined */
	}
}



/*** InitStepsize: the only thing this function does is making stepsize ****
 *                 and accuracy for the solver static to score.c so the    *
 *                 Score() function can use it.                            *
 ***************************************************************************/

void InitStepsize(double step, double acc, FILE *slog, char *infile)
{
	stepsize = step;
	accuracy = acc;
	slogptr  = slog;
	filename = infile;
}

/*** REAL SCORING CODE HERE ************************************************/
/*** Score: as the name says, score runs the simulation, gets a solution ***
 *          and then compares it to the data using the Eval least squares  *
 *          function                                                       *
 *   NOTE:  both InitZygote and InitScoring have to be called first!       *
 ***************************************************************************/
double Score_bcd(void)
{
	int        i;                              /* local loop counters */
	FILE       *fp;                                          /* file pointer */
	char       *debugfile;                             /* name of debug file */
	double     chisq   = 0;                    /* summed squared differences */
	double     chisq0   = 0;
/* debugging mode: need debugging file name */
	if ( debug )
		debugfile = (char *)calloc(MAX_RECORD, sizeof(char));
	if( !tt_init_flag )          /* tt_init_flag is a flag static to score.c */
		InitTTs();                         /* initializes tabulated times (tt) */
	if ( (chisq = CheckSearchSpace_bcd()) == FORBIDDEN_MOVE )
		return FORBIDDEN_MOVE;
	for ( i = 0; i < nalleles; i++) {
		Blastoderm_SolveConc(Flies[i], stepsize, accuracy, slogptr);
		if ( debug ) {
			sprintf(debugfile, "%s.%s.pout", filename, facttype[i].genotype);
			fp = fopen(debugfile, "w");
			if( !fp ) {
				perror("printscore");
				exit(1);
			}
			PrintBlastoderm(fp, Flies[i]->Solution, "debug_output", MAX_PRECISION, defs->ngenes);
			fclose(fp);
		}
		if (gutparms.flag) {
			chisq0 = GutEval(Flies[i]->Solution, facttype[i].genotype);
			chisq += chisq0;
		} else {
			chisq0 = Eval(Flies[i]->Solution, facttype[i].genotype);
			chisq += genotype_weight[i] * chisq0;
		}
		genotype_score[i] = chisq0;
	}
	if ( debug ) {
		free( debugfile );
		fprintf(stdout,"\n Score=%*.*f",MAX_PRECISION+4,MAX_PRECISION,chisq);
	}
	return chisq;
}

/*** REAL SCORING CODE HERE ************************************************/
/*** Score: as the name says, score runs the simulation, gets a solution ***
 *          and then compares it to the data using the Eval least squares  *
 *          function                                                       *
 *   NOTE:  both InitZygote and InitScoring have to be called first!       *
 ***************************************************************************/
double Score(void)
{
	int        i;                              /* local loop counters */
	FILE       *fp;                                          /* file pointer */
	char       *debugfile;                             /* name of debug file */
	double     chisq   = 0;                    /* summed squared differences */
	double     chisq0   = 0;
/* debugging mode: need debugging file name */
	if ( debug )
		debugfile = (char *)calloc(MAX_RECORD, sizeof(char));
	if( !tt_init_flag )          /* tt_init_flag is a flag static to score.c */
		InitTTs();                         /* initializes tabulated times (tt) */
	if ( (chisq = CheckSearchSpace()) == FORBIDDEN_MOVE )
		return FORBIDDEN_MOVE;
	for ( i = 0; i < nalleles; i++) {
		Blastoderm_SolveConc(Flies[i], stepsize, accuracy, slogptr);
		if ( debug ) {
			sprintf(debugfile, "%s.%s.pout", filename, facttype[i].genotype);
			fp = fopen(debugfile, "w");
			if( !fp ) {
				perror("printscore");
				exit(1);
			}
			PrintBlastoderm(fp, Flies[i]->Solution, "debug_output", MAX_PRECISION, defs->ngenes);
			fclose(fp);
		}
		if (gutparms.flag) {
			chisq0 = GutEval(Flies[i]->Solution, facttype[i].genotype);
			chisq += chisq0;
		} else {
			chisq0 = Eval(Flies[i]->Solution, facttype[i].genotype);
			chisq += genotype_weight[i] * chisq0;
		}
		genotype_score[i] = chisq0;
	}
	if ( debug ) {
		free( debugfile );
		fprintf(stdout,"\n Score=%*.*f",MAX_PRECISION+4,MAX_PRECISION,chisq);
	}
	return chisq;
}

double gcdm_score_score_bcd(void)
{
	int        i;                              /* local loop counters */
	FILE       *fp;                                          /* file pointer */
	char       *debugfile;                             /* name of debug file */
	double     chisq   = 0;                    /* summed squared differences */
	double     chisq0   = 0;
/* debugging mode: need debugging file name */
	if ( debug )
		debugfile = (char *)calloc(MAX_RECORD, sizeof(char));
	if( !tt_init_flag )          /* tt_init_flag is a flag static to score.c */
		InitTTs();                         /* initializes tabulated times (tt) */
//	if ( (chisq = CheckSearchSpace_bcd()) == FORBIDDEN_MOVE )
//		return FORBIDDEN_MOVE;


	/*printf("START chisq - %f\n\n *****---*****\n", chisq);*/
	//sprintf(debugfile, "%s.%s.pout", filename, facttype[i].genotype);
	for ( i = 0; i < GT_ITERS_HB_B_SIZE; i++)
	{
		//if (i > 0)
/*		gcdm_blastoderm_solve_conc_with_hints(Flies[i], stepsize, accuracy, slogptr, Flies[i]->dosage, Flies[i]->zero_bias, Flies[i]->rhs);*/
		if ( debug ) {
			sprintf(debugfile, "%s.%s.pout", filename, facttype[i].genotype);
			fp = fopen(debugfile, "w");
			if( !fp ) {
				perror("printscore");
				exit(1);
			}
			PrintBlastoderm(fp, Flies[i]->Solution, "debug_output", MAX_PRECISION, defs->ngenes);
			fclose(fp);
		}
		if (gutparms.flag) {
			chisq0 = GutEval(Flies[i]->Solution, facttype[i].genotype);
			chisq = chisq0;
		} else {
			chisq0 = Eval_bcd(Flies[i]->Solution, facttype[i].genotype);
			chisq += genotype_weight[i] * chisq0;
			chisq += gHbChisq[GT_ITER - 1];
			/*printf("current % i chisq - %f weigth - %f \n\n *****\n",

			GT_ITER, chisq, genotype_weight[i]);*/
		}
		genotype_score[i] = chisq0;
		GT_ITER += 1;
/*		GT_ITER = (GT_ITERS_HB_B_SIZE < GT_ITER) ? GT_ITER : GT_ITER--;*/
	}
	if ( debug ) {
		free( debugfile );
		fprintf(stdout,"\n Score=%*.*f",MAX_PRECISION+4,MAX_PRECISION,chisq);
	}
	/*printf("\n\n return %f", chisq);*/
	return chisq;
}

double gcdm_score_score(void)
{
	int        i;                              /* local loop counters */
	FILE       *fp;                                          /* file pointer */
	char       *debugfile;                             /* name of debug file */
	double     chisq   = 0;                    /* summed squared differences */
	double     chisq0   = 0;
/* debugging mode: need debugging file name */
	if ( debug )
		debugfile = (char *)calloc(MAX_RECORD, sizeof(char));
	if( !tt_init_flag )          /* tt_init_flag is a flag static to score.c */
		InitTTs();                         /* initializes tabulated times (tt) */
	if ( (chisq = CheckSearchSpace()) == FORBIDDEN_MOVE )
		return FORBIDDEN_MOVE;
	for ( i = 0; i < nalleles; i++) {
		gcdm_blastoderm_solve_conc_with_hints(Flies[i], stepsize, accuracy, slogptr, Flies[i]->dosage, Flies[i]->zero_bias, Flies[i]->rhs);
		if ( debug ) {
			sprintf(debugfile, "%s.%s.pout", filename, facttype[i].genotype);
			fp = fopen(debugfile, "w");
			if( !fp ) {
				perror("printscore");
				exit(1);
			}
			PrintBlastoderm(fp, Flies[i]->Solution, "debug_output", MAX_PRECISION, defs->ngenes);
			fclose(fp);
		}
		if (gutparms.flag) {
			chisq0 = GutEval(Flies[i]->Solution, facttype[i].genotype);
			chisq = chisq0;
		} else {
			chisq0 = gcdm_score_eval(Flies[i]->Solution, facttype[i].genotype);
			chisq = genotype_weight[i] * chisq0;
		}
		genotype_score[i] = chisq0;
	}
	if ( debug ) {
		free( debugfile );
		fprintf(stdout,"\n Score=%*.*f",MAX_PRECISION+4,MAX_PRECISION,chisq);
	}
	return chisq;
}

double gcdm_score_score_1(void)
{
	int        i;                              /* local loop counters */
	FILE       *fp;                                          /* file pointer */
	char       *debugfile;                             /* name of debug file */
	double     chisq   = 0;                    /* summed squared differences */
	double     chisq0   = 0;
/* debugging mode: need debugging file name */
	if ( debug )
		debugfile = (char *)calloc(MAX_RECORD, sizeof(char));
	if( !tt_init_flag )          /* tt_init_flag is a flag static to score.c */
		InitTTs();                         /* initializes tabulated times (tt) */
	if ( (chisq = gcdm_score_check_limits()) == FORBIDDEN_MOVE )
		return FORBIDDEN_MOVE;
	for ( i = 0; i < nalleles; i++) {
		gcdm_blastoderm_solve_conc_with_hints(Flies[i], stepsize, accuracy, slogptr, Flies[i]->dosage, Flies[i]->zero_bias, Flies[i]->rhs);
		if (gutparms.flag) {
			chisq0 = GutEval(Flies[i]->Solution, facttype[i].genotype);
			chisq += chisq0;
		} else {
			chisq0 = gcdm_score_eval(Flies[i]->Solution, facttype[i].genotype);
			chisq += genotype_weight[i] * chisq0;
		}
		genotype_score[i] = chisq0;
	}
	if ( debug ) {
		free( debugfile );
		fprintf(stdout,"\n Score=%*.*f",MAX_PRECISION+4,MAX_PRECISION,chisq);
	}
	return chisq;
}

void gcdm_scores_bcd(double*chisq, double*wpgp,	double*penalty)
{
	int        i;
	if( !tt_init_flag )          /* tt_init_flag is a flag static to score.c */
		InitTTs();                         /* initializes tabulated times (tt) */
	if ( (*penalty = CheckSearchSpace_bcd()) == FORBIDDEN_MOVE )
		return;
    i = 0;
	for ( i = 0; i < nalleles; i++)
	{
		gcdm_blastoderm_solve_conc_with_hints(Flies[i], stepsize, accuracy, slogptr, Flies[i]->dosage, Flies[i]->zero_bias, Flies[i]->rhs);
		*chisq = Eval(Flies[i]->Solution, facttype[i].genotype);
       /* *wpgp = ;*/
	}
	return;
}

void gcdm_scores(double*chisq, double*wpgp,	double*penalty)
{
	int        i;
	if( !tt_init_flag )          /* tt_init_flag is a flag static to score.c */
		InitTTs();                         /* initializes tabulated times (tt) */
	if ( (*penalty = CheckSearchSpace()) == FORBIDDEN_MOVE )
		return;
    i = 0;
/*	for ( i = 0; i < nalleles; i++) {*/
		gcdm_blastoderm_solve_conc_with_hints(Flies[i], stepsize, accuracy, slogptr, Flies[i]->dosage, Flies[i]->zero_bias, Flies[i]->rhs);
		if ( UNFOLD ) {
			FILE*fp = fopen(unfold_output_file, "w");
			if( !fp ) {
				warning("printscore");
			} else {
				NArrPtr *outtab;
				DArrPtr *tt;
				tt = ReadTimes(unfold_time_file);
				outtab = ConvertAnswer(Flies[i]->Solution, tt);
				PrintBlastoderm(fp, Flies[i]->Solution, "output\n", MAX_PRECISION, defs->ngenes);
				fclose(fp);
				FreeNArrPtr(outtab);
				FreeDArrPtr(tt);
			}
		}
		*chisq = gcdm_score_eval(Flies[i]->Solution, facttype[i].genotype);
        *wpgp = wPGP(Flies[i]->Solution, facttype[i].genotype);
/*	}*/
	return;
}

void gcdm_scores_colmn_bcd(double*chisq, double*wpgp, double*penalty)
{
	int        i;
	int j;
	if( !tt_init_flag )          /* tt_init_flag is a flag static to score.c */
		InitTTs();                         /* initializes tabulated times (tt) */
	if ( (*penalty = CheckSearchSpace_bcd()) == FORBIDDEN_MOVE ) {
		for ( j = 0; j < defs->ngenes; j++) { 	//new
			chisq[j] = FORBIDDEN_MOVE; 	//chisq[i]
    		wpgp[j] = FORBIDDEN_MOVE;
		}
		return;
	}
    i = 0;
/*	for ( i = 0; i < nalleles; i++) {*/
		gcdm_blastoderm_solve_conc_with_hints(Flies[i], stepsize, accuracy, slogptr, Flies[i]->dosage, Flies[i]->zero_bias, Flies[i]->rhs);
		if ( UNFOLD ) {
			FILE*fp = fopen(unfold_output_file, "w");
			if( !fp ) {
				warning("printscore");
			} else {
				NArrPtr *outtab;
				DArrPtr *tt;
				tt = ReadTimes(unfold_time_file);
				outtab = ConvertAnswer(Flies[i]->Solution, tt);
				PrintBlastoderm(fp, outtab, "output\n", ndigits, defs->ngenes);
				fclose(fp);
				FreeNArrPtr(outtab);
				FreeDArrPtr(tt);
			}
		}
		for ( j = 0; j < defs->ngenes; j++) { 	//new
			chisq[j] = eval_colmn(Flies[i]->Solution, facttype[i].genotype,j); 	//chisq[i]
			wpgp[j] = wPGP_colmn(Flies[i]->Solution, facttype[i].genotype,j);    			// wpgp[i]
		} 		//new
/*	}*/
	return;
}

void gcdm_scores_colmn(double*chisq, double*wpgp, double*penalty)
{
	int        i;
	char *sfile;
	int j;
	if( !tt_init_flag )          /* tt_init_flag is a flag static to score.c */
		InitTTs();                         /* initializes tabulated times (tt) */
	if ( (*penalty = CheckSearchSpace()) == FORBIDDEN_MOVE ) {
		for ( j = 0; j < defs->ngenes * nalleles; j++) { 	//new
			chisq[j] = FORBIDDEN_MOVE; 	//chisq[i]
    		wpgp[j] = FORBIDDEN_MOVE;
		}
		return;
	}
/*    i = 0;*/
	for ( i = 0; i < nalleles; i++) {
		gcdm_blastoderm_solve_conc_with_hints(Flies[i], stepsize, accuracy, slogptr, Flies[i]->dosage, Flies[i]->zero_bias, Flies[i]->rhs);
		if ( UNFOLD ) {
			sfile = (char *)calloc(MAX_RECORD, sizeof(char));
			sprintf(sfile, "%s.%d", unfold_output_file, i);
			FILE*fp = fopen(sfile, "w");
			if( !fp ) {
				warning("printscore");
			} else {
				NArrPtr *outtab;
				DArrPtr *tt;
				tt = ReadTimes(unfold_time_file);
				outtab = ConvertAnswer(Flies[i]->Solution, tt);
				PrintBlastoderm(fp, outtab, "output\n", ndigits, defs->ngenes);
				fclose(fp);
/*				FreeNArrPtr(outtab);*/
				FreeDArrPtr(tt);
				free(sfile);
			}
		}
		for ( j = 0; j < defs->ngenes; j++) { 	//new
			chisq[i * defs->ngenes + j] = eval_colmn(Flies[i]->Solution, facttype[i].genotype, j); 	//chisq[i]
			wpgp[i * defs->ngenes + j] = wPGP_colmn(Flies[i]->Solution, facttype[i].genotype, j);    			// wpgp[i]
		} 		//new
	}
	return;
}

void gcdm_scores_colmn_focus(double*chisq, double*wpgp, double*focus, double*penalty)
{
	int        i;
	char *sfile;
	int j;
	if( !tt_init_flag )          /* tt_init_flag is a flag static to score.c */
		InitTTs();                         /* initializes tabulated times (tt) */
	if ( (*penalty = CheckSearchSpace()) == FORBIDDEN_MOVE ) {
		for ( j = 0; j < defs->ngenes * nalleles; j++) { 	//new
			chisq[j] = FORBIDDEN_MOVE; 	//chisq[i]
    		wpgp[j] = FORBIDDEN_MOVE;
		}
		return;
	}
/*    i = 0;*/
	for ( i = 0; i < nalleles; i++) {
		gcdm_blastoderm_solve_conc_with_hints(Flies[i], stepsize, accuracy, slogptr, Flies[i]->dosage, Flies[i]->zero_bias, Flies[i]->rhs);
		if ( UNFOLD ) {
			sfile = (char *)calloc(MAX_RECORD, sizeof(char));
			sprintf(sfile, "%s.%d", unfold_output_file, i);
			FILE*fp = fopen(sfile, "w");
			if( !fp ) {
				warning("printscore");
			} else {
				NArrPtr *outtab;
				DArrPtr *tt;
				tt = ReadTimes(unfold_time_file);
				outtab = ConvertAnswer(Flies[i]->Solution, tt);
				PrintBlastoderm(fp, outtab, "output\n", ndigits, defs->ngenes);
				fclose(fp);
/*				FreeNArrPtr(outtab);*/
				FreeDArrPtr(tt);
				free(sfile);
			}
		}
		for ( j = 0; j < defs->ngenes; j++) { 	//new
			chisq[i * defs->ngenes + j] = eval_colmn(Flies[i]->Solution, facttype[i].genotype, j); 	//chisq[i]
			wpgp[i * defs->ngenes + j] = wPGP_colmn(Flies[i]->Solution, facttype[i].genotype, j);    			// wpgp[i]
			focus[i * defs->ngenes + j] = focus_colmn(Flies[i]->Solution, focustype[i].genotype, j);
		} 		//new
	}
	return;
}

void gcdm_scores_table(double***chisq, double***wpgp, double*penalty, int *ftsize)
{
	int        i;
	char *sfile;
	int j;
	if( !tt_init_flag )          /* tt_init_flag is a flag static to score.c */
		InitTTs();                         /* initializes tabulated times (tt) */
	if ( (*penalty = CheckSearchSpace()) == FORBIDDEN_MOVE ) {
		for ( j = 0; j < nalleles; j++) {
			chisq[j] = NULL;
    		wpgp[j] = NULL;
		}
		return;
	}
	DArrPtr *tt;
	if ( UNFOLD ) {
		tt = ReadTimes(unfold_time_file);
	}
	for ( i = 0; i < nalleles; i++) {
		gcdm_blastoderm_solve_conc_with_hints(Flies[i], stepsize, accuracy, slogptr, Flies[i]->dosage, Flies[i]->zero_bias, Flies[i]->rhs);
		if ( UNFOLD ) {
			sfile = (char *)calloc(MAX_RECORD, sizeof(char));
			sprintf(sfile, "%s.%d", unfold_output_file, i);
			FILE*fp = fopen(sfile, "w");
			if( !fp ) {
				warning("printscore");
			} else {
				NArrPtr *outtab;
				outtab = ConvertAnswer(Flies[i]->Solution, tt);
				PrintBlastoderm(fp, outtab, "output\n", ndigits, defs->ngenes);
				fclose(fp);
				free(sfile);
			}
		}
		chisq[i] = eval_table(Flies[i]->Solution, facttype[i].genotype);
		wpgp[i] = wPGP_table(Flies[i]->Solution, facttype[i].genotype, ftsize);
	}
	if ( UNFOLD ) {
		FreeDArrPtr(tt);
	}
	return;
}

/*** Eval: scores the summed squared differences between equation solution *
 *         and data. Because the times for states written to the Solution  *
 *         structure are read out of the data file itself, we do not check *
 *         for consistency of times in this function---all times with data *
 *         will be in the table, but the table may also contain additional *
 *         times.                                                          *
 ***************************************************************************/

double Eval_bcd(NArrPtr*answer, char *genotype)
{
	const double big_epsilon = BIG_EPSILON;    /* used to recognize new time */
	DataTable    fact_tab;      /* stores a copy of the Facts (from GenoTab) */
	DataPoint    *point;           /* used to extract an element of DataTable */
	int          gindex;                      /* index for specific genotype */
	int          tindex;                       /* index for facts timepoints */
	int          sindex;                    /* index for Solution timepoints */
	int          vindex;                        /* index for facts datapoint */
	double       time;                      /* time for each facts timepoint */
	double       *v;                   /* ptr to solution for each timepoint */
	double       chisq = 0;                /* the final score to be returned */
	int iTestC = 0;
	int iArrC = 0;
	int iFinHbCount;
	int iHbNum = 0;
	int timeCount = 0;
	double* hbArray = NULL;
	double halfMax = 0;
	double tabDiff = 0;
	double gAdditionalChisq = 0;
/* extablish a pointer to the appropriate facts section in GenoTab */
	gindex   = GetIndex(genotype);
	fact_tab = *(facttype[gindex].ptr->facts);
	sindex   = 0;
	char *sfile;
	FILE *fp;
	if (UNFOLD) {
		sfile = (char *)calloc(MAX_RECORD, sizeof(char));
		sprintf(sfile, "hb_%s.%d", unfold_output_file, (GT_ITER - 1));
		fp = fopen(sfile, "w");
		if( !fp ) {
			warning("printscore");
		}
	}
/* the following loop adds all squared differences for the whole Solution */
	for (tindex=0; tindex < fact_tab.size; tindex++)
	{
		time = fact_tab.record[tindex].time;
/* new time step to be compared? -> get the Solution for this time */
		while( fabs(time - answer->array[sindex].time) >= big_epsilon )
			sindex++;
		v = answer->array[sindex].state->array;

/* this loop steps through the Solution for a specific timepoint and       */
/* evaluates the squared diffs
 *                                          */
/*		printf("time_separator %f \n", (fact_tab.record[tindex].time));*/
		iTestC = 0;

		if(fact_tab.record[tindex].time > 40)
		{
			iFinHbCount = answer->array[sindex].state->size / defs->ngenes ;

			hbArray = (double*)malloc(iFinHbCount * sizeof(double));
			iArrC = 0;
		}
		for (vindex=0; vindex < answer->array[sindex].state->size; vindex++)
		{
			if(fact_tab.record[tindex].time > 40)
			{
				if (iTestC % (defs->ngenes) == iHbNum)
				{
					hbArray[iArrC] =  v[vindex];
					iArrC += 1;
				}
			}

			/*fprintf(stdout,"score t = %f i = %d v = %f\n", time, point->index, v[point->index]);*/
			iTestC += 1;
		}
		if(fact_tab.record[tindex].time > 40)
		{
			halfMax = getHalfMax(hbArray, 35.0, 92.0, iArrC);
			tabDiff = HB_BOUNDS[(GT_ITER - 1) * HB_B_SIZE + timeCount] - halfMax;
			if (UNFOLD) {
				if( fp ) {
					fprintf(fp, "%d %f %f %f\n", (GT_ITER - 1), time, HB_BOUNDS[(GT_ITER - 1) * HB_B_SIZE + timeCount], halfMax);
				}
			}
			gAdditionalChisq += additionalWeight * tabDiff * tabDiff;
			/*printf("add chisq - %f, iter - %i, time - %f\n",
					additionalChisq, GT_ITER, time);*/
			free(hbArray);
			timeCount += 1;
		}

	}
	if (UNFOLD) {
		if( fp ) fclose(fp);
		free(sfile);
	}
	gHbChisq[GT_ITER - 1] = gAdditionalChisq;
	return (chisq);                                              /* that's it! */
}

double Eval(NArrPtr*answer, char *genotype)
{
	const double big_epsilon = BIG_EPSILON;    /* used to recognize new time */
	DataTable    fact_tab;      /* stores a copy of the Facts (from GenoTab) */
	DataPoint    *point;           /* used to extract an element of DataTable */
	int          gindex;                      /* index for specific genotype */
	int          tindex;                       /* index for facts timepoints */
	int          sindex;                    /* index for Solution timepoints */
	int          vindex;                        /* index for facts datapoint */
	double       time;                      /* time for each facts timepoint */
	double       *v;                   /* ptr to solution for each timepoint */
	double       chisq = 0;                /* the final score to be returned */
	int iTestC = 0;
	int iArrC = 0;
	int iFinHbCount;
	int iHbNum = 0;
	int timeCount = 0;
	double* hbArray = NULL;
	double halfMax = 0;
	double tabDiff = 0;
	double gAdditionalChisq = 0;
/* extablish a pointer to the appropriate facts section in GenoTab */
	gindex   = GetIndex(genotype);
	fact_tab = *(facttype[gindex].ptr->facts);
	sindex   = 0;
/* the following loop adds all squared differences for the whole Solution */
	for (tindex=0; tindex < fact_tab.size; tindex++) {
		time = fact_tab.record[tindex].time;
/* new time step to be compared? -> get the Solution for this time */
		while( fabs(time - answer->array[sindex].time) >= big_epsilon )
			sindex++;
		v = answer->array[sindex].state->array;
/* this loop steps through the Solution for a specific timepoint and       */
/* evaluates the squared diffs                                             */
		for (vindex=0; vindex < fact_tab.record[tindex].size; vindex++) {
			point = &(fact_tab.record[tindex].array[vindex]);
/*fprintf(stdout,"score t = %f i = %d v = %f\n", time, point->index, v[point->index]);*/
			point->difference = point->conc - v[point->index];
			chisq += point->difference * point->difference;
		}
	}
	return chisq;                                              /* that's it! */
}

double getHalfMax(double *arr, double startIndex, double finishIndex, int length)
{
	double max = 0;
	double k, b, h = 0;
	int i;
	for(i = 0; i <= length / 2; i++)
	{
		if(arr[i] >= max)
			max = arr[i];
		if(i > 1)
		{
			if((arr[i-1] > max/2) && (arr[i] <= max/2))
			{
				h = interpTwoPoints(arr[i-1], arr[i],
						numToPercent(i-1, startIndex, finishIndex, length),
						numToPercent(i, startIndex, finishIndex, length), max/2);
			}
		}

	}
	return h;
}

double interpTwoPoints(double y1, double y2, double x1, double x2, double h)
{
	double k, b;
	k = (y2 -y1) / (x2 - x1);
	b = y2 - k * x2;
	return (h - b) / k;
}

double numToPercent(int num, double startIndex, double finishIndex, int length)
{
	return (startIndex + num * (finishIndex - startIndex + 1) / length );
}

double eval_colmn(NArrPtr*answer, char *genotype, int colmn)
{
	const double big_epsilon = BIG_EPSILON;    /* used to recognize new time */
	DataTable    fact_tab;      /* stores a copy of the Facts (from GenoTab) */
	DataPoint    *point;           /* used to extract an element of DataTable */
	int          gindex;                      /* index for specific genotype */
	int          tindex;                       /* index for facts timepoints */
	int          sindex;                    /* index for Solution timepoints */
	int          vindex;                        /* index for facts datapoint */
	double       time;                      /* time for each facts timepoint */
	double       *v;                   /* ptr to solution for each timepoint */
	double       chisq = 0;                /* the final score to be returned */
/* extablish a pointer to the appropriate facts section in GenoTab */
	gindex   = GetIndex(genotype);
	fact_tab = *(facttype[gindex].ptr->facts);
	sindex   = 0;
/* the following loop adds all squared differences for the whole Solution */
	for (tindex = 0; tindex < fact_tab.size; tindex++) {
		time = fact_tab.record[tindex].time;
/* new time step to be compared? -> get the Solution for this time */
		while( fabs(time - answer->array[sindex].time) >= big_epsilon )
			sindex++;
		v = answer->array[sindex].state->array;
/* this loop steps through the Solution for a specific timepoint and       */
/* evaluates the squared diffs                                             */
		for (vindex = 0; vindex < fact_tab.record[tindex].size; vindex++) {
			point = &(fact_tab.record[tindex].array[vindex]);
/*fprintf(stdout,"score t = %f i = %d v = %f\n", time, point->index, v[point->index]);*/
			point->difference = point->conc - v[point->index];
			if (point->index % defs->ngenes == colmn) {  //new
				chisq += point->difference * point->difference;
			} //new
		}
	}
	return chisq;                                              /* that's it! */
}

double focus_colmn(NArrPtr*answer, char *genotype, int colmn)
{
	const double big_epsilon = BIG_EPSILON;    /* used to recognize new time */
	DataTable    fact_tab;      /* stores a copy of the Facts (from GenoTab) */
	DataPoint    *point;           /* used to extract an element of DataTable */
	int          gindex;                      /* index for specific genotype */
	int          tindex;                       /* index for facts timepoints */
	int          sindex;                    /* index for Solution timepoints */
	int          vindex;                        /* index for facts datapoint */
	double       time;                      /* time for each facts timepoint */
	double       *v;                   /* ptr to solution for each timepoint */
	double       *focus_expr;
	double       chisq = 0;                /* the final score to be returned */
	static int done_unfold = 0;
	int              rule;                         /* MITOSIS or INTERPHASE? */
	unsigned int ccycle;                   /* what cleavage cycle are we in? */
	int num_nucs;
/* extablish a pointer to the appropriate facts section in GenoTab */
	gindex   = GetIndex(genotype);
	fact_tab = *(focustype[gindex].ptr->facts);
	sindex   = 0;
/* the following loop adds all squared differences for the whole Solution */
	for (tindex = 0; tindex < fact_tab.size; tindex++) {
		time = fact_tab.record[tindex].time;
		if (done_unfold == 0 && UNFOLD == 1) {
			fprintf(stdout, "time = %19.15f\n", time);
		}
/* new time step to be compared? -> get the Solution for this time */
		while( fabs(time - answer->array[sindex].time) >= big_epsilon )
			sindex++;
		v = answer->array[sindex].state->array;
		rule = GetRule(time);
		SetRule(rule);
		ccycle = GetCCycle(time);
		SetCCycle(ccycle);
		num_nucs = GetNNucs(time);
		SetNNucs(num_nucs);
		focus_expr = (double*)malloc(sizeof(double) * num_nucs * defs->ngenes);
		DddwwdtFocus(v, time, focus_expr, num_nucs * defs->ngenes);
/* this loop steps through the Solution for a specific timepoint and       */
/* evaluates the squared diffs                                             */
		for (vindex = 0; vindex < fact_tab.record[tindex].size; vindex++) {
			point = &(fact_tab.record[tindex].array[vindex]);
/*fprintf(stdout,"score t = %f i = %d v = %f\n", time, point->index, v[point->index]);*/
			point->difference = point->conc - focus_expr[point->index];
			if (point->index % defs->ngenes == colmn) {  //new
				chisq += point->difference * point->difference;
			} //new
			if (done_unfold == 0 && UNFOLD == 1) {
				fprintf(stdout, " %19.15f", focus_expr[point->index]);
				if (vindex % (defs->ngenes/2) == (defs->ngenes/2) - 1) {
					fprintf(stdout, "\n");
				}
			}
		}
		free(focus_expr);
	}
	done_unfold = 1;
	return chisq;                                              /* that's it! */
}

double** eval_table(NArrPtr*answer, char *genotype)
{
	const double big_epsilon = BIG_EPSILON;    /* used to recognize new time */
	DataTable    fact_tab;      /* stores a copy of the Facts (from GenoTab) */
	DataPoint    *point;           /* used to extract an element of DataTable */
	int          gindex;                      /* index for specific genotype */
	int          tindex;                       /* index for facts timepoints */
	int          sindex;                    /* index for Solution timepoints */
	int          vindex;                        /* index for facts datapoint */
	int          index;                         			/* index of gene */
	double       time;                      /* time for each facts timepoint */
	double       *v;                   /* ptr to solution for each timepoint */
	double       **chisq;                /* the final score to be returned */
/* extablish a pointer to the appropriate facts section in GenoTab */
	gindex   = GetIndex(genotype);
	fact_tab = *(facttype[gindex].ptr->facts);
	sindex   = 0;
	chisq = (double**)calloc(fact_tab.size, sizeof(double*));
/* the following loop adds all squared differences for the whole Solution */
	for (tindex = 0; tindex < fact_tab.size; tindex++) {
		time = fact_tab.record[tindex].time;
/* new time step to be compared? -> get the Solution for this time */
		while( fabs(time - answer->array[sindex].time) >= big_epsilon )
			sindex++;
		v = answer->array[sindex].state->array;
/* this loop steps through the Solution for a specific timepoint and       */
/* evaluates the squared diffs                                             */
		chisq[tindex] = (double*)calloc(defs->ngenes, sizeof(double));
		for (index = 0; index < defs->ngenes; index++) {
			chisq[tindex][index] = 0;
		}
		for (vindex = 0; vindex < fact_tab.record[tindex].size; vindex++) {
			point = &(fact_tab.record[tindex].array[vindex]);
/*fprintf(stdout,"score t = %f i = %d v = %f\n", time, point->index, v[point->index]);*/
			point->difference = point->conc - v[point->index];
			index = point->index % defs->ngenes;
			chisq[tindex][index] += point->difference * point->difference;
		}
	}
	return chisq;                                              /* that's it! */
}

double wPGP(NArrPtr*answer, char *genotype)
{
	const double big_epsilon = BIG_EPSILON;    /* used to recognize new time */
	DataTable    fact_tab;      /* stores a copy of the Facts (from GenoTab) */
	DataPoint    *point;           /* used to extract an element of DataTable */
	int          gindex;                      /* index for specific genotype */
	int          tindex;                       /* index for facts timepoints */
	int          sindex;                    /* index for Solution timepoints */
	int          vindex;                        /* index for facts datapoint */
	double       time;                      /* time for each facts timepoint */
	double       *v;                   /* ptr to solution for each timepoint */
	double       reward_nom = 0;
	double       reward_denom = 0;
    double       penalty_nom = 0;
	double       penalty_denom = 0;
	double wpgp, reward, penalty, val;
	double max_r = 255;
/* extablish a pointer to the appropriate facts section in GenoTab */
	gindex   = GetIndex(genotype);
	fact_tab = *(facttype[gindex].ptr->facts);
	sindex   = 0;
/* the following loop adds all squared differences for the whole Solution */
	for (tindex=0; tindex < fact_tab.size; tindex++) {
		time = fact_tab.record[tindex].time;
/* new time step to be compared? -> get the Solution for this time */
		while( fabs(time - answer->array[sindex].time) >= big_epsilon )
			sindex++;
		v = answer->array[sindex].state->array;
/* this loop steps through the Solution for a specific timepoint and       */
/* evaluates the squared diffs                                             */
		for (vindex=0; vindex < fact_tab.record[tindex].size; vindex++) {
			point = &(fact_tab.record[tindex].array[vindex]);
/*fprintf(stdout,"score t = %f i = %d v = %f\n", time, point->index, v[point->index]);*/
			val = ( point->conc < v[point->index] ) ? point->conc : v[point->index];
/*			reward_nom += v[point->index] * val;
			reward_denom += v[point->index] * v[point->index];
			val = point->conc - v[point->index];
			val = ( val > 0 ) ? val : -val;
			penalty_nom += (max_r - v[point->index]) * val;
			penalty_denom += (max_r - v[point->index]) * (max_r - v[point->index]);*/
			reward_nom += point->conc * val;
			reward_denom += point->conc * point->conc;
			val = v[point->index] - point->conc;
			val = ( val > 0 ) ? val : 0;
			penalty_nom += (max_r - point->conc) * val;
			penalty_denom += (max_r - point->conc) * (max_r - point->conc);
		}
	}
	penalty = penalty_nom / penalty_denom;
	reward = reward_nom / reward_denom;
	wpgp = 0.5 + 0.5 * (penalty - reward);
	return wpgp;                                              /* that's it! */
}

double wPGP_colmn(NArrPtr*answer, char *genotype, int colmn)
{
	const double big_epsilon = BIG_EPSILON;    /* used to recognize new time */
	DataTable    fact_tab;      /* stores a copy of the Facts (from GenoTab) */
	DataPoint    *point;           /* used to extract an element of DataTable */
	int          gindex;                      /* index for specific genotype */
	int          tindex;                       /* index for facts timepoints */
	int          sindex;                    /* index for Solution timepoints */
	int          vindex;                        /* index for facts datapoint */
	double       time;                      /* time for each facts timepoint */
	double       *v;                   /* ptr to solution for each timepoint */
	double       reward_nom = 0;
	double       reward_denom = 0;
    double       penalty_nom = 0;
	double       penalty_denom = 0;
	double wpgp = 0, reward, penalty, val;
	double max_r = 255;
/* extablish a pointer to the appropriate facts section in GenoTab */
	gindex   = GetIndex(genotype);
	fact_tab = *(facttype[gindex].ptr->facts);
	sindex   = 0;
/* the following loop adds all squared differences for the whole Solution */
	for (tindex = 0; tindex < fact_tab.size; tindex++) {
		time = fact_tab.record[tindex].time;
/* new time step to be compared? -> get the Solution for this time */
		while( fabs(time - answer->array[sindex].time) >= big_epsilon )
			sindex++;
		v = answer->array[sindex].state->array;
/* this loop steps through the Solution for a specific timepoint and       */
/* evaluates the squared diffs                                             */
		for (vindex = 0; vindex < fact_tab.record[tindex].size; vindex++) {
			point = &(fact_tab.record[tindex].array[vindex]);
			if (point->index % defs->ngenes == colmn) { //new
			/*fprintf(stdout,"score t = %f i = %d v = %f\n", time, point->index, v[point->index]);*/
				val = ( point->conc < v[point->index] ) ? point->conc : v[point->index];
/*			reward_nom += v[point->index] * val;
			reward_denom += v[point->index] * v[point->index];
			val = point->conc - v[point->index];
			val = ( val > 0 ) ? val : -val;
			penalty_nom += (max_r - v[point->index]) * val;
			penalty_denom += (max_r - v[point->index]) * (max_r - v[point->index]);*/
				reward_nom += point->conc * val;
				reward_denom += point->conc * point->conc;
				val = v[point->index] - point->conc;
				val = ( val > 0 ) ? val : 0;
				penalty_nom += (max_r - point->conc) * val;
				penalty_denom += (max_r - point->conc) * (max_r - point->conc);
			} //new
		}
	}
	if (penalty_denom > 0 && reward_denom) {
		penalty = penalty_nom / penalty_denom;
		reward = reward_nom / reward_denom;
		wpgp = 0.5 + 0.5 * (penalty - reward);
	}
	return wpgp;                                              /* that's it! */
}

double** wPGP_table(NArrPtr*answer, char *genotype, int *ftsize)
{
	const double big_epsilon = BIG_EPSILON;    /* used to recognize new time */
	DataTable    fact_tab;      /* stores a copy of the Facts (from GenoTab) */
	DataPoint    *point;           /* used to extract an element of DataTable */
	int          gindex;                      /* index for specific genotype */
	int          tindex;                       /* index for facts timepoints */
	int          sindex;                    /* index for Solution timepoints */
	int          vindex;                        /* index for facts datapoint */
	int          index;                         			/* index of gene */
	double       time;                      /* time for each facts timepoint */
	double       *v;                   /* ptr to solution for each timepoint */
	double       *reward_nom;
	double       *reward_denom;
    double       *penalty_nom;
	double       *penalty_denom;
	double **wpgp, *reward, *penalty, val;
	double *max_r;
/* extablish a pointer to the appropriate facts section in GenoTab */
	gindex   = GetIndex(genotype);
	fact_tab = *(facttype[gindex].ptr->facts);
	sindex   = 0;
	*ftsize = fact_tab.size;
	wpgp = (double**)calloc(fact_tab.size, sizeof(double*));
	reward_nom = (double*)calloc(defs->ngenes, sizeof(double));
	reward_denom = (double*)calloc(defs->ngenes, sizeof(double));
	reward = (double*)calloc(defs->ngenes, sizeof(double));
	penalty_nom = (double*)calloc(defs->ngenes, sizeof(double));
	penalty_denom = (double*)calloc(defs->ngenes, sizeof(double));
	penalty = (double*)calloc(defs->ngenes, sizeof(double));
	max_r = (double*)calloc(defs->ngenes, sizeof(double));
/* the following loop adds all squared differences for the whole Solution */
	for (tindex = 0; tindex < fact_tab.size; tindex++) {
		time = fact_tab.record[tindex].time;
/* new time step to be compared? -> get the Solution for this time */
		while( fabs(time - answer->array[sindex].time) >= big_epsilon )
			sindex++;
		v = answer->array[sindex].state->array;
/* this loop steps through the Solution for a specific timepoint and       */
/* evaluates the squared diffs                                             */
		wpgp[tindex] = (double*)calloc(defs->ngenes, sizeof(double));
		for (index = 0; index < defs->ngenes; index++) {
			max_r[index] = 0;
			wpgp[tindex][index] = 0;
		}
		for (vindex = 0; vindex < fact_tab.record[tindex].size; vindex++) {
			point = &(fact_tab.record[tindex].array[vindex]);
			index = point->index % defs->ngenes;
			max_r[index] = (max_r[index] > point->conc) ? max_r[index] : point->conc;
		}
		for (vindex = 0; vindex < fact_tab.record[tindex].size; vindex++) {
			point = &(fact_tab.record[tindex].array[vindex]);
			index = point->index % defs->ngenes;
			val = ( point->conc < v[point->index] ) ? point->conc : v[point->index];
			reward_nom[index] += point->conc * val;
			reward_denom[index] += point->conc * point->conc;
			val = v[point->index] - point->conc;
			val = ( val > 0 ) ? val : 0;
			penalty_nom[index] += (max_r[index] - point->conc) * val;
			penalty_denom[index] += (max_r[index] - point->conc) * (max_r[index] - point->conc);
		}
		for (index = 0; index < defs->ngenes; index++) {
			if (penalty_denom[index] > 0 && reward_denom[index]) {
				penalty[index] = penalty_nom[index] / penalty_denom[index];
				reward[index] = reward_nom[index] / reward_denom[index];
				wpgp[tindex][index] = 0.5 + 0.5 * (penalty[index] - reward[index]);
			}
		}
	}
	free(reward_nom);
	free(reward_denom);
	free(reward);
	free(penalty_nom);
	free(penalty_denom);
	free(penalty);
	free(max_r);
	return wpgp;                                              /* that's it! */
}

double gcdm_score_eval_vst_bcd(NArrPtr*answer, char *genotype)
{
	const double big_epsilon = BIG_EPSILON;
	DataTable    fact_tab;
	DataPoint    *point;
	int          gindex;
	int          tindex;
	int          sindex;
	int          vindex;
	double       time;
	double       *v;
	double       chisq = 0;
	double difference, dot;
	gindex   = GetIndex(genotype);
	fact_tab = *(facttype[gindex].ptr->facts);
	sindex   = 0;
	for (tindex=0; tindex < fact_tab.size; tindex++) {
		time = fact_tab.record[tindex].time;
		while( fabs(time - answer->array[sindex].time) >= big_epsilon )
			sindex++;
		v = answer->array[sindex].state->array;
		for (vindex=0; vindex < fact_tab.record[tindex].size; vindex++) {
			point = &(fact_tab.record[tindex].array[vindex]);
			dot = sqrt(v[point->index]);
			difference = point->conc - dot;
			point->difference = 0.5 * difference / dot;
			chisq += difference * difference;
		}
	}
	return chisq;                                              /* that's it! */
}

double gcdm_score_eval_vst(NArrPtr*answer, char *genotype)
{
	const double big_epsilon = BIG_EPSILON;
	DataTable    fact_tab;
	DataPoint    *point;
	int          gindex;
	int          tindex;
	int          sindex;
	int          vindex;
	double       time;
	double       *v;
	double       chisq = 0;
	double difference, dot;
	gindex   = GetIndex(genotype);
	fact_tab = *(facttype[gindex].ptr->facts);
	sindex   = 0;
	for (tindex=0; tindex < fact_tab.size; tindex++) {
		time = fact_tab.record[tindex].time;
		while( fabs(time - answer->array[sindex].time) >= big_epsilon )
			sindex++;
		v = answer->array[sindex].state->array;
		for (vindex=0; vindex < fact_tab.record[tindex].size; vindex++) {
			point = &(fact_tab.record[tindex].array[vindex]);
			dot = sqrt(v[point->index]);
			difference = ( point->conc > big_epsilon ) ? sqrt(point->conc) - dot : point->conc - dot ;
			point->difference = 0.5 * difference / dot;
			chisq += difference * difference;
		}
	}
	return chisq;                                              /* that's it! */
}

double gcdm_score_eval_deriv(NArrPtr*answer, char *genotype)
{
	const double big_epsilon = BIG_EPSILON;    /* used to recognize new time */
	DataTable    fact_tab;      /* stores a copy of the Facts (from GenoTab) */
	DataPoint    *point;           /* used to extract an element of DataTable */
	int          gindex;                      /* index for specific genotype */
	int          tindex;                       /* index for facts timepoints */
	int          sindex;                    /* index for Solution timepoints */
	int          vindex;                        /* index for facts datapoint */
	double       time;                      /* time for each facts timepoint */
	double       *v;                   /* ptr to solution for each timepoint */
	double       chisq = 0;                /* the final score to be returned */
	double c1, c2, c3, c4, ch;
/* extablish a pointer to the appropriate facts section in GenoTab */
	gindex   = GetIndex(genotype);
	fact_tab = *(facttype[gindex].ptr->facts);
	sindex   = 0;
/* the following loop adds all squared differences for the whole Solution */
	for (tindex=0; tindex < fact_tab.size; tindex++) {
		time = fact_tab.record[tindex].time;
/* new time step to be compared? -> get the Solution for this time */
		while( fabs(time - answer->array[sindex].time) >= big_epsilon )
			sindex++;
		v = answer->array[sindex].state->array;
/* this loop steps through the Solution for a specific timepoint and       */
/* evaluates the squared diffs                                             */
		for (vindex = 1; vindex < fact_tab.record[tindex].size - 1; vindex++) {
			point = &(fact_tab.record[tindex].array[vindex - 1]);
			c1 = point->conc;
			c3 = v[point->index];
			point = &(fact_tab.record[tindex].array[vindex + 1]);
			c2 = point->conc;
			c4 = v[point->index];
			ch = 0.5 * (c2 - c1) - 0.5 * (c4 - c3);
			point->difference = ch;
			chisq += ch * ch;
		}
	}
	return chisq;                                              /* that's it! */
}

double gcdm_score_eval_freq(NArrPtr*answer, char *genotype)
{
	int          gindex;
	int          tindex;
	int          sindex;
	int          vindex;
	int ap, i;
	double       chisq = 0, dot;
	gindex   = GetIndex(genotype);
	for ( tindex = 0; tindex < derivfacts[gindex]->size; tindex++ ) {
		for ( sindex = 0; sindex < defs->ngenes; sindex++) {
			for ( vindex = sindex, ap = 0; vindex < derivfacts[gindex]->array[tindex].state->size; vindex += defs->ngenes, ap++ ) {
				scoring_freq_data[ap] = derivfacts[gindex]->array[tindex].state->array[vindex];
				scoring_freq_model[ap] = answer->array[tindex].state->array[vindex];
			}

			gsl_wavelet_transform_forward (scoring_freq_obj, scoring_freq_data, 1, scoring_freq_n, scoring_freq_wksp);
			gsl_wavelet_transform_forward (scoring_freq_obj, scoring_freq_model, 1, scoring_freq_n, scoring_freq_wksp);
			for ( i = 0; i < scoring_freq_n; i++ ) {
				scoring_freq_data_abs[i] = fabs (scoring_freq_data[i]);
			}
			gsl_sort_index (scoring_freq_indices, scoring_freq_data_abs, 1, scoring_freq_n);
			for (i = 0; (i + scoring_freq_s) < scoring_freq_n; i++) {
				scoring_freq_data[scoring_freq_indices[i]] = 0;
				scoring_freq_model[scoring_freq_indices[i]] = 0;
			}
			for ( i = 0; i < scoring_freq_n; i++ ) {
				dot = scoring_freq_data[i] - scoring_freq_model[i];
				chisq += dot * dot;
				scoring_freq_data[i] = 0;
				scoring_freq_model[i] = 0;
			}
		}
	}
	return chisq;
}

double gcdm_score_eval_freq2d_bcd(NArrPtr*answer, char *genotype)
{
	int          gindex;
	int          tindex;
	int          sindex;
	int          vindex;
	int ap, i, n;
	double       chisq = 0, dot;
	gindex   = GetIndex(genotype);
	for ( tindex = 0; tindex < derivfacts[gindex]->size; tindex++ ) {
		for ( sindex = 0; sindex < defs->ngenes; sindex++) {
			n = derivfacts[gindex]->array[tindex].state->size;
			for ( vindex = sindex, ap = 0; vindex < n; vindex += defs->ngenes, ap++ ) {
				scoring_freq_data[sindex * scoring_freq_n + ap] = derivfacts[gindex]->array[tindex].state->array[vindex];
				scoring_freq_model[sindex * scoring_freq_n + ap] = answer->array[tindex].state->array[vindex];
			}
		}
		gsl_wavelet2d_nstransform_forward (scoring_freq_obj, scoring_freq_data, scoring_freq_n, scoring_freq_g, scoring_freq_n, scoring_freq_wksp);
		gsl_wavelet2d_nstransform_forward (scoring_freq_obj, scoring_freq_model, scoring_freq_n, scoring_freq_g, scoring_freq_n, scoring_freq_wksp);
		for ( i = 0; i < scoring_freq_g * scoring_freq_n; i++ ) {
			scoring_freq_data_abs[i] = fabs (scoring_freq_data[i]);
		}
		gsl_sort_index (scoring_freq_indices, scoring_freq_data_abs, 1, scoring_freq_g * scoring_freq_n);
		for (i = 0; (i + scoring_freq_s) < scoring_freq_g * scoring_freq_n; i++) {
			scoring_freq_data[scoring_freq_indices[i]] = 0;
			scoring_freq_model[scoring_freq_indices[i]] = 0;
		}
		for ( i = 0; i < scoring_freq_g * scoring_freq_n; i++ ) {
			dot = scoring_freq_data[i] - scoring_freq_model[i];
			chisq += dot * dot;
			scoring_freq_data[i] = 0;
			scoring_freq_model[i] = 0;
		}
	}
	return chisq;
}

double gcdm_score_eval_freq2d(NArrPtr*answer, char *genotype)
{
/*	int          gindex;
	int          tindex;
	int          sindex;
	int          vindex;
	int ap, i, n;
	double       chisq = 0, dot;
	gindex   = GetIndex(genotype);
	for ( tindex = 0; tindex < derivfacts[gindex]->size; tindex++ ) {
		for ( sindex = 0; sindex < defs->ngenes; sindex++) {
			n = derivfacts[gindex]->array[tindex].state->size;
			for ( vindex = sindex, ap = 0; vindex < n; vindex += defs->ngenes, ap++ ) {
				scoring_freq_data[sindex * scoring_freq_n + ap] = derivfacts[gindex]->array[tindex].state->array[vindex];
				scoring_freq_model[sindex * scoring_freq_n + ap] = answer->array[tindex].state->array[vindex];
			}
		}
		gsl_wavelet2d_nstransform_forward (scoring_freq_obj, scoring_freq_data, scoring_freq_n, scoring_freq_g, scoring_freq_n, scoring_freq_wksp);
		gsl_wavelet2d_nstransform_forward (scoring_freq_obj, scoring_freq_model, scoring_freq_n, scoring_freq_g, scoring_freq_n, scoring_freq_wksp);
		for ( i = 0; i < scoring_freq_g * scoring_freq_n; i++ ) {
			scoring_freq_data_abs[i] = fabs (scoring_freq_data[i]);
		}
		gsl_sort_index (scoring_freq_indices, scoring_freq_data_abs, 1, scoring_freq_g * scoring_freq_n);
		for (i = 0; (i + scoring_freq_s) < scoring_freq_g * scoring_freq_n; i++) {
			scoring_freq_data[scoring_freq_indices[i]] = 0;
			scoring_freq_model[scoring_freq_indices[i]] = 0;
		}
		for ( i = 0; i < scoring_freq_g * scoring_freq_n; i++ ) {
			dot = scoring_freq_data[i] - scoring_freq_model[i];
			chisq += dot * dot;
			scoring_freq_data[i] = 0;
			scoring_freq_model[i] = 0;
		}
	}
	return chisq;*/
}

double gcdm_score_eval_lsq(NArrPtr*answer, char *genotype)
{
	const double big_epsilon = BIG_EPSILON;    /* used to recognize new time */
	DataTable    fact_tab;      /* stores a copy of the Facts (from GenoTab) */
	DataTable    df_fact_tab;      /* stores a copy of the Facts (from GenoTab) */
	DataPoint    *point, *df_point;           /* used to extract an element of DataTable */
	int          gindex;                      /* index for specific genotype */
	int          tindex;                       /* index for facts timepoints */
	int          sindex;                    /* index for Solution timepoints */
	int          vindex;                        /* index for facts datapoint */
	double       time;                      /* time for each facts timepoint */
	double       *v;                   /* ptr to solution for each timepoint */
	double       chisq = 0;                /* the final score to be returned */
	double dot;
/* extablish a pointer to the appropriate facts section in GenoTab */
	gindex   = GetIndex(genotype);
	fact_tab = *(facttype[gindex].ptr->facts);
	df_fact_tab = *(derivtype[gindex].ptr->facts);
	sindex   = 0;
/* the following loop adds all squared differences for the whole Solution */
	for (tindex=0; tindex < fact_tab.size; tindex++) {
		time = fact_tab.record[tindex].time;
/* new time step to be compared? -> get the Solution for this time */
		while( fabs(time - answer->array[sindex].time) >= big_epsilon )
			sindex++;
		v = answer->array[sindex].state->array;
/* this loop steps through the Solution for a specific timepoint and       */
/* evaluates the squared diffs                                             */
		for (vindex=0; vindex < fact_tab.record[tindex].size; vindex++) {
			point = &(fact_tab.record[tindex].array[vindex]);
			df_point = &(df_fact_tab.record[tindex].array[vindex]);
/*fprintf(stdout,"score t = %f i = %d v = %f\n", time, point->index, v[point->index]);*/
			point->difference = point->conc - v[point->index];
			dot = df_point->conc;
			chisq += point->difference * point->difference * dot;
		}
	}
	return chisq;                                              /* that's it! */
}

double gcdm_score_eval_dfacts(NArrPtr*answer, char *genotype)
{
	const double big_epsilon = BIG_EPSILON;    /* used to recognize new time */
	DataTable    fact_tab;      /* stores a copy of the Facts (from GenoTab) */
	DataPoint    *point;           /* used to extract an element of DataTable */
	int          gindex;                      /* index for specific genotype */
	int          tindex, ti;                       /* index for facts timepoints */
	int          sindex;                    /* index for Solution timepoints */
	int          vindex;                        /* index for facts datapoint */
	double       time;                      /* time for each facts timepoint */
	double       *v;                   /* ptr to solution for each timepoint */
	double       chisq = 0;                /* the final score to be returned */
	double dot = 0, argument = 0, *mean, *times;
	int i, j, g;
/* extablish a pointer to the appropriate facts section in GenoTab */
	gindex   = GetIndex(genotype);
	fact_tab = *(facttype[gindex].ptr->facts);
	sindex   = 0;
	mean = (double*)calloc(defs->ngenes, sizeof(double));
	times = (double*)calloc(defs->ngenes, sizeof(double));
/* the following loop adds all squared differences for the whole Solution */
	for (tindex=0; tindex < fact_tab.size; tindex++) {
		time = fact_tab.record[tindex].time;
/* new time step to be compared? -> get the Solution for this time */
		while( fabs(time - answer->array[sindex].time) >= big_epsilon )
			sindex++;
		v = answer->array[sindex].state->array;
/* this loop steps through the Solution for a specific timepoint and       */
/* evaluates the squared diffs                                             */
		for ( i = 0; i < defs->ngenes; i++ ) {
			mean[i] = 0.0;
			times[i] = -1;
		}
		for (vindex=0; vindex < fact_tab.record[tindex].size; vindex++) {
			point = &(fact_tab.record[tindex].array[vindex]);
			g = point->index % defs->ngenes;
			mean[g] += v[point->index];
		}
		for ( i = 0; i < defs->ngenes; i++ ) {
			for (j = 0; j < fact_tab.size; j++) {
				ti = tindex + j;
				if ( 0 < ti && ti < fact_tab.size) {
					if ( fact_tab.record[ti - 1].dfacts[i] <= fact_tab.record[ti].dfacts[i] ) {
						if ( mean[i] > fact_tab.record[ti - 1].dfacts[i] && mean[i] <= fact_tab.record[ti].dfacts[i] ) {
							if ( fabs(mean[i] - fact_tab.record[ti - 1].dfacts[i]) < fabs(mean[i] - fact_tab.record[ti].dfacts[i]) ) {
								times[i] = fact_tab.record[ti - 1].time;
								break;
							} else {
								times[i] = fact_tab.record[ti].time;
								break;
							}
						}
					} else {
						if ( mean[i] < fact_tab.record[ti - 1].dfacts[i] && mean[i] >= fact_tab.record[ti].dfacts[i] ) {
							if ( fabs(mean[i] - fact_tab.record[ti - 1].dfacts[i]) < fabs(mean[i] - fact_tab.record[ti].dfacts[i]) ) {
								times[i] = fact_tab.record[ti - 1].time;
								break;
							} else {
								times[i] = fact_tab.record[ti].time;
								break;
							}
						}
					}
				}
				ti = tindex - j;
				if ( 0 < ti && ti < fact_tab.size) {
					if ( fact_tab.record[ti - 1].dfacts[i] <= fact_tab.record[ti].dfacts[i] ) {
						if ( mean[i] > fact_tab.record[ti - 1].dfacts[i] && mean[i] <= fact_tab.record[ti].dfacts[i] ) {
							if ( fabs(mean[i] - fact_tab.record[ti - 1].dfacts[i]) < fabs(mean[i] - fact_tab.record[ti].dfacts[i]) ) {
								times[i] = fact_tab.record[ti - 1].time;
								break;
							} else {
								times[i] = fact_tab.record[ti].time;
								break;
							}
						}
					} else {
						if ( mean[i] < fact_tab.record[ti - 1].dfacts[i] && mean[i] >= fact_tab.record[ti].dfacts[i] ) {
							if ( fabs(mean[i] - fact_tab.record[ti - 1].dfacts[i]) < fabs(mean[i] - fact_tab.record[ti].dfacts[i]) ) {
								times[i] = fact_tab.record[ti - 1].time;
								break;
							} else {
								times[i] = fact_tab.record[ti].time;
								break;
							}
						}
					}
				}
			}
			if ( times[i] < 0 ) {
				if ( fabs(mean[i] - fact_tab.record[0].dfacts[i]) < fabs(mean[i] - fact_tab.record[fact_tab.size - 1].dfacts[i]) ) {
					times[i] = fact_tab.record[0].time;
				} else {
					times[i] = fact_tab.record[fact_tab.size - 1].time;
				}
			}
		}
		for ( i = 0; i < defs->ngenes; i++ ) {
			dot = ( time - times[i] );
			argument += dot * dot;
		}
	}
	free(times);
	free(mean);
	argument *= dfacts_lambda;
	if ( argument > 88.7228391 ) {
		return FORBIDDEN_MOVE;
	} else {
		chisq = ( exp( argument ) - 2.718281828459045 );
	}
	if ( chisq <= 0 ) {
		chisq = 0;
	}
	return chisq;                                              /* that's it! */
}

int gcdm_chaos_interpret_op(double op1, double var, double const1)
{
	int res = -1;
	int op = (int)op1;
	if ( op == 0 ) {
		if( var == const1) {
			return res;
		}
	}
	if ( op == 1 ) {
		if( var < const1) {
			return res;
		}
	}
	if ( op == 2 ) {
		if( var > const1) {
			return res;
		}
	}
	if ( op == 3 ) {
		if( var <= const1 ) {
			return res;
		}
	}
	if ( op == 4 ) {
		if( var >= const1 ) {
			return res;
		}
	}
	if ( op == 5 ) { /* artificial op == always yes for -1 in the data */
		return res;
	}
	res = 0;
	return res;
}

ChaosData*ReadChaosData(FILE *f, char*section)
{
	int i=0;
	int j=0;
	int k=0;
	int ijk=0;
	FILE *fp;
	ChaosData*chaos_data = malloc(sizeof(ChaosData));
	fp = FindSection(f, section);
	if( !fp ) {
		error("readChaos: cannot locate chaos 0 section");
	}
	fscanf(fp,"%*s");
	fscanf(fp,"%lf", &(chaos_data->w));
	fscanf(fp,"%*s");
	fscanf(fp,"%d",&(chaos_data->ng));
	fscanf(fp,"%*s");
	fscanf(fp,"%d",&(chaos_data->ntc));
	fscanf(fp,"%*s");
	fscanf(fp,"%d",&(chaos_data->nbc));
	fscanf(fp,"%*s");
	chaos_data->bc = (double *) calloc(chaos_data->nbc,sizeof(double));
	for ( i = 0; i < chaos_data->nbc; i++) {
		fscanf(fp,"%lf",&(chaos_data->bc[i]));
	}
	chaos_data->deltamc = (double *) calloc((chaos_data->ng * chaos_data->ntc * chaos_data->ntc),sizeof(double));
	fscanf(fp,"%*s");
	for ( i = 0; i < chaos_data->ng; i++) {
		for ( j = 0; j < chaos_data->ntc; j++) {
			for ( k = 0; k < chaos_data->ntc; k++) {
				ijk = i * chaos_data->ntc * chaos_data->ntc + j * chaos_data->ntc + k;
				fscanf(fp,"%lf", &(chaos_data->deltamc[ijk]));
			}
		}
	}
	chaos_data->ltc = (int *) calloc((chaos_data->ntc),sizeof(int));
	chaos_data->nltc = 0;
	fscanf(fp,"%*s");
	for ( j = 0; j < chaos_data->ntc; j++) {
		fscanf( fp, "%d", &(chaos_data->ltc[j]));
		chaos_data->nltc += chaos_data->ltc[j];
	}
	return chaos_data;
}

void gcdm_score_init_chaos ( FILE*fp )
{
	int i;
	FILE*f;
	char*section;
	chaos_data_array = (ChaosData**)calloc(nalleles, sizeof(ChaosData*));
	for ( i = 0; i < nalleles; i++) {
		section = (char*)calloc( MAX_RECORD, sizeof(char));
		sprintf(section, "chaos%d", i);
		chaos_data_array[i] = ReadChaosData(fp, section);
	}
}

double gcdm_chaos_calc_etime(double*data, int gindex)
{
	DataTable    fact_tab;
	double time;
	int tindex;
	int op_result = 0;
	double tc = -1;
	double op =-1;
	double rule = 0;
	double var = -1;
	double const1 = -1;
	int start = 0;
	int kk = 0;
	int boll = 0;
	int inext = 0;
	int nkk = 0;
	int nrules = 0;
	int i = 0;
	double*bc;
	int bcLENGTH;
	bc = chaos_data_array[gindex]->bc;
	bcLENGTH = chaos_data_array[gindex]->nbc;
	fact_tab = *(facttype[gindex].ptr->facts);
	for( i = 0; i < bcLENGTH; i = i + 3) {
		op = bc[i];
		if(op != -1) {
			var = data[((int)bc[i+1])];
/*fprintf(stderr, "var=%f i=%d (int)bc[i+1]=%d\n", var, i, ((int)bc[i+1]));*/
		}
		const1 = bc[i+2];
/*fprintf(stderr, "const=%f op=%f i=%d\n", const1, op, i);*/
		if(op == -1) {
			nrules = (int)bc[i+1];
		}
		if (kk == 0) {
			tc = const1;
			start = i;
			inext = start + nrules * 3;
			nkk = (int)bc[i+1];
			op_result = 0;
			kk = 1;
		} else {
			op_result = gcdm_chaos_interpret_op(op, var, const1);
			if (op_result >= 0) {
				i = inext;
				kk = 0;
				continue;
			} else {
				if(kk == nkk) {
					tindex = (int)tc;
					if ( 0 <= tindex && tindex < fact_tab.size ) {
						time = fact_tab.record[tindex].time;
					} else {
						time = -1.0;
					}
/*fprintf(stderr, "tc=%f tindex=%d etime=%f\n", tc, tindex, time);*/
					return time;
				}
			}
			kk++;
		}
	}
	tindex = (int)tc;
	if ( 0 <= tindex && tindex < fact_tab.size ) {
		time = fact_tab.record[tindex].time;
	} else {
		time = -1.0;
	}
/*fprintf(stderr, "tc=%f tindex=%d etime=%f\n", tc, tindex, time);*/
	return time;
}

double gcdm_score_eval_chaos(NArrPtr*answer, char *genotype)
{
	const double big_epsilon = BIG_EPSILON;    /* used to recognize new time */
	DataTable    fact_tab;      /* stores a copy of the Facts (from GenoTab) */
	DataPoint    *point;           /* used to extract an element of DataTable */
	int          gindex;                      /* index for specific genotype */
	int          tindex, ti;                       /* index for facts timepoints */
	int          sindex;                    /* index for Solution timepoints */
	int          vindex;                        /* index for facts datapoint */
	double       time, etime;                      /* time for each facts timepoint */
	double       *v;                   /* ptr to solution for each timepoint */
	double       chisq = 0;                /* the final score to be returned */
	double dot = 0, argument = 0, *mean;
	int i, j, g;
/* extablish a pointer to the appropriate facts section in GenoTab */
	gindex   = GetIndex(genotype);
/*fprintf(stderr, "gindex=%d\n", gindex);*/
	fact_tab = *(facttype[gindex].ptr->facts);
	sindex   = 0;
	mean = (double*)calloc(defs->ngenes, sizeof(double));
/* the following loop adds all squared differences for the whole Solution */
	for (tindex=0; tindex < fact_tab.size; tindex++) {
		if ( fact_tab.record[tindex].size > 0 ) {
			time = fact_tab.record[tindex].time;
/* new time step to be compared? -> get the Solution for this time */
			while( fabs(time - answer->array[sindex].time) >= big_epsilon )
				sindex++;
			v = answer->array[sindex].state->array;
/* this loop steps through the Solution for a specific timepoint and       */
/* evaluates the squared diffs                                             */
			for ( i = 0; i < defs->ngenes; i++ ) {
				mean[i] = 0.0;
			}
			for (vindex=0; vindex < fact_tab.record[tindex].size; vindex++) {
				point = &(fact_tab.record[tindex].array[vindex]);
				g = point->index % defs->ngenes;
				mean[g] += v[point->index];
			}
/*fprintf(stderr, "time=%f\n", time);*/
			etime = gcdm_chaos_calc_etime( mean, gindex);
			dot = ( time - etime );
			argument += dot * dot;
		}
	}
	free(mean);
	argument *= chaos_data_array[gindex]->w;
/*fprintf(stderr, "argument=%f", argument);*/
	if ( argument > 88.7228391 ) {
		return FORBIDDEN_MOVE;
	} else {
		chisq = ( exp( argument ) - 2.718281828459045 );
	}
	if ( chisq <= 0 ) {
		chisq = 0;
	}
/*fprintf(stderr, " chisq=%f\n", chisq);*/
	return chisq;                                              /* that's it! */
}

double CheckSearchSpace_bcd()
{
	int i, j;
	for(i=0; i < defs->ngenes; i++) {
		if( parm->R[i] > limits->Rlim[i]->upper)
			return(FORBIDDEN_MOVE);
		if( parm->R[i] < limits->Rlim[i]->lower)
			return(FORBIDDEN_MOVE);
		if( parm->lambda[i] > limits->lambdalim[i]->upper)
			return(FORBIDDEN_MOVE);
		if( parm->lambda[i] < limits->lambdalim[i]->lower)
			return(FORBIDDEN_MOVE);
		if( parm->tau[i] > limits->taulim[i]->upper)
			return(FORBIDDEN_MOVE);
		if( parm->tau[i] < limits->taulim[i]->lower)
			return(FORBIDDEN_MOVE);
	}
	if( (defs->diff_schedule == 'A') || (defs->diff_schedule == 'C' ) ) {
		if( parm->d[0] > limits->dlim[0]->upper)
			return(FORBIDDEN_MOVE);
 		if( parm->d[0] < limits->dlim[0]->lower)
			return(FORBIDDEN_MOVE);
	} else {
		for(i=0; i < defs->ngenes; i++) {
			if( parm->d[i] > limits->dlim[i]->upper)
				return(FORBIDDEN_MOVE);
			if( parm->d[i] < limits->dlim[i]->lower)
				return(FORBIDDEN_MOVE);
		}
	}
	if( (defs->mob_schedule == 'A') || (defs->mob_schedule == 'C' ) ) {
		if( parm->m[0] > limits->mlim[0]->upper)
			return(FORBIDDEN_MOVE);
 		if( parm->m[0] < limits->mlim[0]->lower)
			return(FORBIDDEN_MOVE);
	} else {
		for(i=0; i < defs->ngenes; i++) {
			if( parm->m[i] > limits->mlim[i]->upper)
				return(FORBIDDEN_MOVE);
			if( parm->m[i] < limits->mlim[i]->lower)
				return(FORBIDDEN_MOVE);
		}
	}
	if( (defs->mob_schedule == 'A') || (defs->mob_schedule == 'C' ) ) {
		if( parm->mm[0] > limits->mmlim[0]->upper)
			return(FORBIDDEN_MOVE);
 		if( parm->mm[0] < limits->mmlim[0]->lower)
			return(FORBIDDEN_MOVE);
	} else {
		for(i=0; i < defs->ngenes; i++) {
			if( parm->mm[i] > limits->mmlim[i]->upper)
				return(FORBIDDEN_MOVE);
			if( parm->mm[i] < limits->mmlim[i]->lower)
				return(FORBIDDEN_MOVE);
		}
	}
	if( limits->pen_vec == NULL ) {
		for(i=0; i<defs->ngenes; i++) {
			for(j=0; j<defs->ngenes; j++) {
				if (parm->T[(i * defs->ngenes) + j] > limits->Tlim[(i * defs->ngenes) + j]->upper)
					return(FORBIDDEN_MOVE);
				if (parm->T[(i * defs->ngenes) + j] < limits->Tlim[(i * defs->ngenes) + j]->lower)
					return(FORBIDDEN_MOVE);
			}
			for(j=0; j<defs->egenes; j++) {
				if (parm->E[(i * defs->egenes) + j] > limits->Elim[(i * defs->egenes) + j]->upper)
					return(FORBIDDEN_MOVE);
				if (parm->E[(i * defs->egenes) + j] < limits->Elim[(i * defs->egenes) + j]->lower)
					return(FORBIDDEN_MOVE);
			}
			for(j=0; j<defs->mgenes; j++) {
				if (parm->M[(i * defs->mgenes) + j] > limits->Mlim[(i * defs->mgenes) + j]->upper)
					return(FORBIDDEN_MOVE);
				if (parm->M[(i * defs->mgenes) + j] < limits->Mlim[(i * defs->mgenes) + j]->lower)
					return(FORBIDDEN_MOVE);
			}
			if (parm->h[i] > limits->hlim[i]->upper)
				return(FORBIDDEN_MOVE);
			if (parm->h[i] < limits->hlim[i]->lower)
				return(FORBIDDEN_MOVE);
		}
	} else {
		if ( ( Penalty = GetPenalty_bcd() ) == FORBIDDEN_MOVE )
			return FORBIDDEN_MOVE;
		else
			return Penalty;
	}
	return 0;
}

double CheckSearchSpace()
{
	int i, j;
	for(i=0; i < defs->ngenes; i++) {
		if( parm->R[i] > limits->Rlim[i]->upper) {
			fprintf(stderr, "Forbiden R %d\n", i);
			return(FORBIDDEN_MOVE);
		}
		if( parm->R[i] < limits->Rlim[i]->lower) {
			fprintf(stderr, "Forbiden R %d\n", i);
			return(FORBIDDEN_MOVE);
		}
		if( parm->lambda[i] > limits->lambdalim[i]->upper) {
			fprintf(stderr, "Forbiden %d lambda %f over limit %f\n", i, parm->lambda[i], limits->lambdalim[i]->upper);
			return(FORBIDDEN_MOVE);
		}
		if( parm->lambda[i] < limits->lambdalim[i]->lower) {
			fprintf(stderr, "Forbiden %d lambda %f below limit %f\n", i, parm->lambda[i], limits->lambdalim[i]->lower);
			return(FORBIDDEN_MOVE);
		}
		if( parm->tau[i] > limits->taulim[i]->upper) {
			fprintf(stderr, "Forbiden tau %d\n", i);
			return(FORBIDDEN_MOVE);
		}
		if( parm->tau[i] < limits->taulim[i]->lower) {
			fprintf(stderr, "Forbiden tau %d\n", i);
			return(FORBIDDEN_MOVE);
		}
	}
	if( (defs->diff_schedule == 'A') || (defs->diff_schedule == 'C' ) ) {
		if( parm->d[0] > limits->dlim[0]->upper) {
			fprintf(stderr, "Forbiden d %d\n", i);
			return(FORBIDDEN_MOVE);
		}
 		if( parm->d[0] < limits->dlim[0]->lower) {
			fprintf(stderr, "Forbiden d %d\n", i);
			return(FORBIDDEN_MOVE);
		 }
	} else {
		for(i=0; i < defs->ngenes; i++) {
			if( parm->d[i] > limits->dlim[i]->upper) {
				fprintf(stderr, "Forbiden d %d\n", i);
				return(FORBIDDEN_MOVE);
			}
			if( parm->d[i] < limits->dlim[i]->lower) {
				fprintf(stderr, "Forbiden d %d\n", i);
				return(FORBIDDEN_MOVE);
			}
		}
	}
	if( (defs->mob_schedule == 'A') || (defs->mob_schedule == 'C' ) ) {
		i = 0;
		if( parm->m[0] > limits->mlim[0]->upper) {
			fprintf(stderr, "Forbiden m %d\n", i);
			return(FORBIDDEN_MOVE);
		}
 		if( parm->m[0] < limits->mlim[0]->lower) {
			fprintf(stderr, "Forbiden m %d\n", i);
			return(FORBIDDEN_MOVE);
		 }
	} else {
		for(i=0; i < defs->ngenes; i++) {
			if( parm->m[i] > limits->mlim[i]->upper) {
				fprintf(stderr, "Forbiden m %d\n", i);
				return(FORBIDDEN_MOVE);
			}
			if( parm->m[i] < limits->mlim[i]->lower) {
				fprintf(stderr, "Forbiden m %d\n", i);
				return(FORBIDDEN_MOVE);
			}
		}
	}
	if( (defs->mob_schedule == 'A') || (defs->mob_schedule == 'C' ) ) {
		i = 0;
		if( parm->mm[0] > limits->mmlim[0]->upper) {
			fprintf(stderr, "Forbiden mm %d\n", i);
			return(FORBIDDEN_MOVE);
		}
 		if( parm->mm[0] < limits->mmlim[0]->lower) {
			fprintf(stderr, "Forbiden mm %d\n", i);
			return(FORBIDDEN_MOVE);
		 }
	} else {
		for(i=0; i < defs->ngenes; i++) {
			if( parm->mm[i] > limits->mmlim[i]->upper) {
				fprintf(stderr, "Forbiden mm %d\n", i);
				return(FORBIDDEN_MOVE);
			}
			if( parm->mm[i] < limits->mmlim[i]->lower) {
				fprintf(stderr, "Forbiden mm %d\n", i);
				return(FORBIDDEN_MOVE);
			}
		}
	}
	if( limits->pen_vec == NULL ) {
		for(i=0; i<defs->ngenes; i++) {
			for(j=0; j<defs->ngenes; j++) {
				if (parm->T[(i * defs->ngenes) + j] > limits->Tlim[(i * defs->ngenes) + j]->upper)
					return(FORBIDDEN_MOVE);
				if (parm->T[(i * defs->ngenes) + j] < limits->Tlim[(i * defs->ngenes) + j]->lower)
					return(FORBIDDEN_MOVE);
			}
			for(j=0; j<defs->egenes; j++) {
				if (parm->E[(i * defs->egenes) + j] > limits->Elim[(i * defs->egenes) + j]->upper)
					return(FORBIDDEN_MOVE);
				if (parm->E[(i * defs->egenes) + j] < limits->Elim[(i * defs->egenes) + j]->lower)
					return(FORBIDDEN_MOVE);
			}
			for(j=0; j<defs->mgenes; j++) {
				if (parm->M[(i * defs->mgenes) + j] > limits->Mlim[(i * defs->mgenes) + j]->upper)
					return(FORBIDDEN_MOVE);
				if (parm->M[(i * defs->mgenes) + j] < limits->Mlim[(i * defs->mgenes) + j]->lower)
					return(FORBIDDEN_MOVE);
			}
			if (parm->h[i] > limits->hlim[i]->upper)
				return(FORBIDDEN_MOVE);
			if (parm->h[i] < limits->hlim[i]->lower)
				return(FORBIDDEN_MOVE);
		}
	} else {
		Penalty = GetPenalty();
		if ( Penalty == FORBIDDEN_MOVE || isnan(Penalty) )  {
			fprintf(stderr, "Forbiden Penalty\n");
			return FORBIDDEN_MOVE;
		} else
			return Penalty;
	}
	return 0;
}

double gcdm_score_check_limits()
{
	int i, j;
	for(i=0; i < defs->ngenes; i++) {
		if( parm->R[i] > limits->Rlim[i]->upper)
			return(FORBIDDEN_MOVE);
		if( parm->R[i] < limits->Rlim[i]->lower)
			return(FORBIDDEN_MOVE);
		if( parm->lambda[i] > limits->lambdalim[i]->upper)
			return(FORBIDDEN_MOVE);
		if( parm->lambda[i] < limits->lambdalim[i]->lower)
			return(FORBIDDEN_MOVE);
		if( parm->tau[i] > limits->taulim[i]->upper)
			return(FORBIDDEN_MOVE);
		if( parm->tau[i] < limits->taulim[i]->lower)
			return(FORBIDDEN_MOVE);
	}
	if( (defs->diff_schedule == 'A') || (defs->diff_schedule == 'C' ) ) {
		if( parm->d[0] > limits->dlim[0]->upper)
			return(FORBIDDEN_MOVE);
 		if( parm->d[0] < limits->dlim[0]->lower)
			return(FORBIDDEN_MOVE);
	} else {
		for(i=0; i < defs->ngenes; i++) {
			if( parm->d[i] > limits->dlim[i]->upper)
				return(FORBIDDEN_MOVE);
			if( parm->d[i] < limits->dlim[i]->lower)
				return(FORBIDDEN_MOVE);
		}
	}
	if( (defs->mob_schedule == 'A') || (defs->mob_schedule == 'C' ) ) {
		if( parm->m[0] > limits->mlim[0]->upper)
			return(FORBIDDEN_MOVE);
 		if( parm->m[0] < limits->mlim[0]->lower)
			return(FORBIDDEN_MOVE);
	} else {
		for(i=0; i < defs->ngenes; i++) {
			if( parm->m[i] > limits->mlim[i]->upper)
				return(FORBIDDEN_MOVE);
			if( parm->m[i] < limits->mlim[i]->lower)
				return(FORBIDDEN_MOVE);
		}
	}
	if( (defs->mob_schedule == 'A') || (defs->mob_schedule == 'C' ) ) {
		if( parm->mm[0] > limits->mmlim[0]->upper)
			return(FORBIDDEN_MOVE);
 		if( parm->mm[0] < limits->mmlim[0]->lower)
			return(FORBIDDEN_MOVE);
	} else {
		for(i=0; i < defs->ngenes; i++) {
			if( parm->mm[i] > limits->mmlim[i]->upper)
				return(FORBIDDEN_MOVE);
			if( parm->mm[i] < limits->mmlim[i]->lower)
				return(FORBIDDEN_MOVE);
		}
	}
	if( limits->pen_vec == NULL ) {
		for(i=0; i<defs->ngenes; i++) {
			for(j=0; j<defs->ngenes; j++) {
				if (parm->T[(i * defs->ngenes) + j] > limits->Tlim[(i * defs->ngenes) + j]->upper)
					return(FORBIDDEN_MOVE);
				if (parm->T[(i * defs->ngenes) + j] < limits->Tlim[(i * defs->ngenes) + j]->lower)
					return(FORBIDDEN_MOVE);
			}
			for(j=0; j<defs->egenes; j++) {
				if (parm->E[(i * defs->egenes) + j] > limits->Elim[(i * defs->egenes) + j]->upper)
					return(FORBIDDEN_MOVE);
				if (parm->E[(i * defs->egenes) + j] < limits->Elim[(i * defs->egenes) + j]->lower)
					return(FORBIDDEN_MOVE);
			}
			for(j=0; j<defs->mgenes; j++) {
				if (parm->M[(i * defs->mgenes) + j] > limits->Mlim[(i * defs->mgenes) + j]->upper)
					return(FORBIDDEN_MOVE);
				if (parm->M[(i * defs->mgenes) + j] < limits->Mlim[(i * defs->mgenes) + j]->lower)
					return(FORBIDDEN_MOVE);
			}
			if (parm->h[i] > limits->hlim[i]->upper)
				return(FORBIDDEN_MOVE);
			if (parm->h[i] < limits->hlim[i]->lower)
				return(FORBIDDEN_MOVE);
		}
	}
	return 0;
}

double gcdm_score_check_detailed_limits()
{
	int i, j;
	for( i = 0; i < defs->ngenes; i++) {
		for( j = 0; j < defs->ngenes; j++) {
			if (parm->T[(i * defs->ngenes) + j] > detailed_limits->Tlim[(i * defs->ngenes) + j]->upper)
				return(FORBIDDEN_MOVE);
			if (parm->T[(i * defs->ngenes) + j] < detailed_limits->Tlim[(i * defs->ngenes) + j]->lower)
				return(FORBIDDEN_MOVE);
		}
		for( j = 0; j < defs->egenes; j++) {
			if (parm->E[(i * defs->egenes) + j] > detailed_limits->Elim[(i * defs->egenes) + j]->upper)
				return(FORBIDDEN_MOVE);
			if (parm->E[(i * defs->egenes) + j] < detailed_limits->Elim[(i * defs->egenes) + j]->lower)
				return(FORBIDDEN_MOVE);
		}
		for( j = 0; j < defs->mgenes; j++) {
			if (parm->M[(i * defs->mgenes) + j] > detailed_limits->Mlim[(i * defs->mgenes) + j]->upper)
				return(FORBIDDEN_MOVE);
			if (parm->M[(i * defs->mgenes) + j] < detailed_limits->Mlim[(i * defs->mgenes) + j]->lower)
				return(FORBIDDEN_MOVE);
		}
		if (parm->h[i] > detailed_limits->hlim[i]->upper)
			return(FORBIDDEN_MOVE);
		if (parm->h[i] < detailed_limits->hlim[i]->lower)
			return(FORBIDDEN_MOVE);
	}
	return 0;
}

double gcdm_get_penalty2()
{
	double penalty = Penalty;
	if ( penalty == FORBIDDEN_MOVE )
		return FORBIDDEN_MOVE;
	return penalty * penalty;
}

double gcdm_get_penalty()
{
	double penalty = Penalty;
	if ( penalty == FORBIDDEN_MOVE )
		return FORBIDDEN_MOVE;
	return penalty;
}

double gcdm_score_penalty_abs(void)
{
	int i;
	double linf = 0;
	for ( i = 0; i < nalleles; i++) {
		linf += EvalABS(Flies[i]->Solution, facttype[i].genotype);
	}
	return linf;
}

double gcdm_score_penalty_chisq_bcd(void)
{
	int i;
	double linf = 0;
	for ( i = 0; i < nalleles; i++) {
		linf += Eval_bcd(Flies[i]->Solution, facttype[i].genotype);
	}
	return linf;
}

double gcdm_score_penalty_chisq(void)
{
	int i;
	double linf = 0;
	for ( i = 0; i < nalleles; i++) {
		linf += Eval(Flies[i]->Solution, facttype[i].genotype);
	}
	return linf;
}

double gcdm_score_penalty_lsq(void)
{
	int i;
	double linf = 0;
	for ( i = 0; i < nalleles; i++) {
		linf += gcdm_score_eval_lsq(Flies[i]->Solution, facttype[i].genotype);
	}
	return linf;
}

double gcdm_score_penalty_dfacts(void)
{
	int i;
	double linf = 0;
	for ( i = 0; i < nalleles; i++) {
		linf += gcdm_score_eval_dfacts(Flies[i]->Solution, facttype[i].genotype);
	}
	return linf;
}

double gcdm_score_penalty_chaos(void)
{
	int i;
	double linf = 0;
	for ( i = 0; i < nalleles; i++) {
		linf += gcdm_score_eval_chaos(Flies[i]->Solution, facttype[i].genotype);
	}
	return linf;
}

double gcdm_score_grad_score_bcd(void)
{
	int i;
	double linf = 0, dot;
	PsiGrad_bcd();
	for ( i = 0; i < NumPar; i++) {
		dot = (*(integ[i]));
		linf += dot * dot;
	}
	return linf;
}

double gcdm_score_grad_score(void)
{
	int i;
	double linf = 0, dot;
	PsiGrad ();
	for ( i = 0; i < NumPar; i++) {
		dot = (*(integ[i]));
		linf += dot * dot;
	}
	return linf;
}

double ScoreABS_bcd(void)
{
	int i;
	double linf;
	if( !tt_init_flag )
		InitTTs();
	linf = CheckSearchSpace_bcd();
	for ( i=0; i<nalleles; i++) {
		Blastoderm_SolveConc(Flies[i], stepsize, accuracy, slogptr);
		linf += EvalABS(Flies[i]->Solution, facttype[i].genotype);
	}
	return linf;
}

double ScoreABS(void)
{
	int i;
	double linf;
	if( !tt_init_flag )
		InitTTs();
	linf = CheckSearchSpace();
	for ( i=0; i<nalleles; i++) {
		Blastoderm_SolveConc(Flies[i], stepsize, accuracy, slogptr);
		linf += EvalABS(Flies[i]->Solution, facttype[i].genotype);
	}
	return linf;
}

double EvalABS(NArrPtr*answer,char *genotype)
{
	const double big_epsilon = BIG_EPSILON;    /* used to recognize new time */
	DataTable    fact_tab;      /* stores a copy of the Facts (from GenoTab) */
	DataPoint    *point;           /* used to extract an element of DataTable */
	int          gindex;                      /* index for specific genotype */
	int          tindex;                       /* index for facts timepoints */
	int          sindex;                    /* index for Solution timepoints */
	int          vindex;                        /* index for facts datapoint */
	double       time;                      /* time for each facts timepoint */
	double       *v;                   /* ptr to solution for each timepoint */
	double       linf;
	gindex   = GetIndex(genotype);
	fact_tab = *(facttype[gindex].ptr->facts);
	sindex   = 0;
	linf = 0;
	for (tindex=0; tindex < fact_tab.size; tindex++) {
		time = fact_tab.record[tindex].time;
/* new time step to be compared? -> get the Solution for this time */
		while( fabs(time - answer->array[sindex].time) >= big_epsilon )
			sindex++;
			v = answer->array[sindex].state->array;
/* this loop steps through the Solution for a specific timepoint and       */
			for (vindex=0; vindex < fact_tab.record[tindex].size; vindex++) {
				point = &(fact_tab.record[tindex].array[vindex]);
				point->difference = point->conc - v[point->index];
				linf += ( point->difference > 0 ) ? point->difference : -point->difference;
			}
	}
	return linf;
}

/*** SCOREGUT FUNCTIONS ****************************************************/
/* SetGuts: sets the gut info in score.c for printing out guts *************
 ***************************************************************************/
void SetGuts (int gutflag, int ndigits)
{
	gutparms.flag    = gutflag;
	gutparms.ndigits = ndigits;
}

/*** GutEval: this is the same as Eval, i.e it calculates the summed squa- *
 *            red differences between equation solution and data, with the *
 *            addition that individual squared differences between data-   *
 *            points are written to STDOUT in the unfold output format     *
 ***************************************************************************/

double GutEval(NArrPtr*answer,char *genotype)
{
	const double big_epsilon = BIG_EPSILON;    /* used to recognize new time */
	int          i, j;                                      /* loop counters */
	DataTable    fact_tab;      /* stores a copy of the Facts (from GenoTab) */
	DataPoint    point;           /* used to extract an element of DataTable */
	NArrPtr      *gut;         /* individual square root diff for a datapoint */
	NArrPtr      *outgut;                             /* output gut structure */
	int          gindex;                      /* index for specific genotype */
	int          tindex;                       /* index for facts timepoints */
	int          sindex;                    /* index for Solution timepoints */
	int          vindex;                        /* index for facts datapoint */
	double       time;                      /* time for each facts timepoint */
	double       difference;      /* diff btw data and model (per datapoint) */
	double       *v;                   /* ptr to solution for each timepoint */
	double       chisq = 0;                /* the final score to be returned */
/* extablish a pointer to the appropriate facts section in GenoTab */
	gindex   = GetIndex(genotype);
	fact_tab = *(facttype[gindex].ptr->facts);
	sindex   = 0;
/* initialize the gut structure */
	gut = (NArrPtr*)malloc(sizeof(NArrPtr));
	gut->array = (NucState *)calloc(answer->size, sizeof(NucState));
	gut->size  = answer->size;
	for (i=0; i<gut->size; i++) {
		gut->array[i].time        = answer->array[i].time;
		gut->array[i].state = (DArrPtr*)malloc(sizeof(DArrPtr));
		gut->array[i].state->size  = answer->array[i].state->size;
		gut->array[i].state->array = (double *)calloc(gut->array[i].state->size, sizeof(double));
		for ( j = 0; j < gut->array[i].state->size; j++)
			gut->array[i].state->array[j] = -1.0;
	}
/* the following loop adds all squared differences for the whole Solution */
	for (tindex = 0; tindex < fact_tab.size; tindex++) {
		time = fact_tab.record[tindex].time;
/* new time step to be compared? -> get the Solution for this time */
		while( fabs(time - answer->array[sindex].time) >= big_epsilon )
			sindex++;
		v = answer->array[sindex].state->array;
/* this loop steps through the Solution for a specific time point and      */
/* evaluates the squared diffs; additionally it stores individual squared  */
/* diffs in the guts array                                                 */
		for ( vindex = 0; vindex < fact_tab.record[tindex].size; vindex++) {
			point = fact_tab.record[tindex].array[vindex];
			difference = point.conc - v[point.index];
			chisq += difference * difference;
			gut->array[sindex].state->array[point.index] = difference * difference;
		}
	}
/* strip gut struct of cell division times and print it to stdout */
	outgut = ConvertAnswer( gut, tt[gindex].ptr->times);
	PrintBlastoderm(stdout, outgut, strcat(genotype, "\n"), gutparms.ndigits, defs->ngenes);
/* release the guts struct */
	FreeNArrPtr(gut);
	free(outgut->array);
	free(outgut);
	return chisq;                                              /* that's it! */
}
/*** FUNCTIONS THAT RETURN SCORE.C-SPECIFIC STUFF **************************/
/*** GetTTimes: this function returns the times for which there's data *****
 *              for a given genotype                                       *
 *     CAUTION: InitTTs has to be called first!                            *
 ***************************************************************************/
DArrPtr*GetTTimes(char *genotype)
{
	int         i;
	if ( tt_init_flag ) {
		i = GetIndex(genotype);
		return tt[i].ptr->times;
	} else
		error("GetTTimes: called without initialized TTs");
	return tt[0].ptr->times;               /* just to make the compiler happy */
}


/*** GetLimits:  returns a pointer to the static limits struct in score.c **
 *     CAUTION:  InitScoring must be called first!                         *
 ***************************************************************************/
SearchSpace *GetLimits(void)
{
	return limits;
}

SearchSpace *GetDetailedLimits(void)
{
	return detailed_limits;
}

/*** GetPenalty: calculates penalty from static limits, vmax and mmax ******
 *   CAUTION:    InitPenalty must be called first!                         *
 ***************************************************************************/

double GetPenalty_bcd(void)
{
	int           i, j;                                /* local loop counter */
	double        argument = 0;    /* variable for penalty function argument */
	double        penalty  = 0;                      /* holds penalty result */
	double        Lambda, param;                                  /* penalty Lambda */
	double        *vemax;                            /* mmax in penalty vector */
	double        *vmmax;                            /* mmax in penalty vector */
	double        *vmax;          /* pointer to vmax array in penalty vector */
	static int    donethis = 0;         /* KLUDGE: only print this info once */
	if ( limits->pen_vec == NULL )
		return -1;
	Lambda = *((limits->pen_vec));  /* Lambda: first entry in penalty vector */
	vmmax   = (limits->pen_vec) + 1;               /* locate mmax and vmax */
	vmax   =   (limits->pen_vec) + 1 + defs->mgenes;
	vemax =   (limits->pen_vec) + 1 + defs->mgenes + defs->ngenes;
/* print debugging info */
	if ( debug && !donethis ) {
		printf("Penalty:\n\n");
		printf("Lambda:    %10.8f\n", Lambda);
		for ( i=0; i<defs->mgenes; i++ )
			printf("vmax[%d]: %6.2f\n", i, vmmax[i]);
		for ( i=0; i<defs->ngenes; i++ )
			printf("vmax[%d]: %6.2f\n", i, vmax[i]);
		for ( i=0; i<defs->egenes; i++ )
			printf("vmax[%d]: %6.2f\n", i, vemax[i]);
		printf("\n");
	}
/* calculate penalty */
	if ( model == HEDIRECT ) {
		for( i = 0; i < defs->ngenes; i++) {
			for( j = 0; j < defs->ngenes; j++) {
				param = ( parm->T[(i*defs->ngenes)+j] >= 0 ) ? parm->T[(i*defs->ngenes)+j] : -parm->T[(i*defs->ngenes)+j];
				argument = ( param > argument ) ? param : argument;
			}
			for(j=0; j < defs->egenes; j++) {
				param = ( parm->E[(i*defs->egenes)+j] >= 0 ) ? parm->E[(i*defs->egenes)+j] : -parm->E[(i*defs->egenes)+j];
				argument = ( param > argument ) ? param : argument;
			}
		}
		if ( argument > 88.7228391 )
			return FORBIDDEN_MOVE;
		else
            penalty = Lambda * (defs->egenes + defs->ngenes) * exp(argument);
	} else if ( model == HESRR || model == HEQUENCHING ) {
		for( i = 0; i < defs->ngenes; i++) {
			for( j = 0; j < defs->ngenes; j++) {
				param = parm->T[(i*defs->ngenes)+j];
				argument += param * param;
			}
			for(j=0; j < defs->egenes; j++) {
				param = parm->E[(i*defs->egenes)+j];
				argument += param * param;
			}
		}
		penalty = Lambda * argument;
	} else {
		for( i = 0; i < defs->ngenes; i++) {
			for( j = 0; j < defs->ngenes; j++) {
				argument += (parm->T[(i*defs->ngenes)+j]*vmax[j]) * (parm->T[(i*defs->ngenes)+j]*vmax[j]);
			}
			for(j=0; j < defs->egenes; j++) {
				argument += (parm->E[(i*defs->egenes)+j]*vemax[j]) * (parm->E[(i*defs->egenes)+j]*vemax[j]);
			}
			for(j=0; j < defs->mgenes; j++) {
				argument += (parm->M[(i*defs->mgenes)+j]*vmmax[j]) * (parm->M[(i*defs->mgenes)+j]*vmmax[j]);
			}
			for(j=0; j < defs->ncouples; j++) {
				argument += (parm->P[(i*defs->ncouples)+j]*255) * (parm->P[(i*defs->ncouples)+j]*255);
			}
			argument += parm->h[i] * parm->h[i];
		}
/* e was 2.718281828 in old code---doubt it matters!! */
/* 88.7228391 is the maximum save value to use with exp (see man exp) */
		if ( (Lambda * argument) > 88.7228391 )
			return FORBIDDEN_MOVE;
		else
			penalty = (exp( Lambda * argument ) - 2.718281828459045);
	}
	if(penalty <= 0)
		penalty = 0;
	donethis = 1;
	return penalty;
}

double GetPenalty(void)
{
	int           i, j;                                /* local loop counter */
	double        argument = 0;    /* variable for penalty function argument */
	double        penalty  = 0;                      /* holds penalty result */
	double        Lambda, param;                                  /* penalty Lambda */
	double        *vemax;                            /* mmax in penalty vector */
	double        *vmmax;                            /* mmax in penalty vector */
	double        *vmax;          /* pointer to vmax array in penalty vector */
	static int    donethis = 0;         /* KLUDGE: only print this info once */
	if ( limits->pen_vec == NULL )
		return -1;
	Lambda = *((limits->pen_vec));  /* Lambda: first entry in penalty vector */
	vmmax   = (limits->pen_vec) + 1;               /* locate mmax and vmax */
	vmax   =   (limits->pen_vec) + 1 + defs->mgenes;
	vemax =   (limits->pen_vec) + 1 + defs->mgenes + defs->ngenes;
/* print debugging info */
	if ( debug && !donethis ) {
		fprintf(stderr, "Penalty:\n\n");
		fprintf(stderr, "Lambda:    %10.8f\n", Lambda);
		for ( i=0; i<defs->mgenes; i++ )
			fprintf(stderr, "vmax[%d]: %6.2f\n", i, vmmax[i]);
		for ( i=0; i<defs->ngenes; i++ )
			fprintf(stderr, "vmax[%d]: %6.2f\n", i, vmax[i]);
		for ( i=0; i<defs->egenes; i++ )
			fprintf(stderr, "vmax[%d]: %6.2f\n", i, vemax[i]);
		fprintf(stderr, "\n");
	}
/* calculate penalty */
	if ( model == HEDIRECT ) {
		for( i = 0; i < defs->ngenes; i++) {
			for( j = 0; j < defs->ngenes; j++) {
				param = ( parm->T[(i*defs->ngenes)+j] >= 0 ) ? parm->T[(i*defs->ngenes)+j] : -parm->T[(i*defs->ngenes)+j];
				argument = ( param > argument ) ? param : argument;
			}
			for(j=0; j < defs->egenes; j++) {
				param = ( parm->E[(i*defs->egenes)+j] >= 0 ) ? parm->E[(i*defs->egenes)+j] : -parm->E[(i*defs->egenes)+j];
				argument = ( param > argument ) ? param : argument;
			}
		}
		if ( argument > 88.7228391 )
			return FORBIDDEN_MOVE;
		else
            penalty = Lambda * (defs->egenes + defs->ngenes) * exp(argument);
	} else if ( model == HESRR || model == HEURR || model == HEQUENCHING ) {
		for( i = 0; i < defs->ngenes; i++) {
			for( j = 0; j < defs->ngenes; j++) {
				param = parm->T[(i*defs->ngenes)+j];
				argument += param * param;
			}
			for(j=0; j < defs->egenes; j++) {
				param = parm->E[(i*defs->egenes)+j];
				argument += param * param;
			}
		}
		penalty = Lambda * argument;
		if ( Lambda < 0 ) {
			for( i = 0; i < (int)(defs->ngenes / 2); i++) {
				for( j = 0; j < (int)(defs->ngenes / 2); j++) {
					param = parm->T[(i*defs->ngenes)+j];
					if ( j != i &&  param > 0) {
						argument += param * param;
					}
				}
				for( j = 0; j < (int)(defs->egenes / 2); j++) {
					param = parm->E[(i*defs->egenes)+j];
					if ( j == 0 &&  param < 0) {
						argument += param * param;
					}
					if ( j == 1 &&  param < 0) {
						argument += param * param;
					}
					if ( i == 0 && j == 2 &&  param < 0) {
						argument += param * param;
					}
					if ( i > 0 && j == 2 &&  param > 0) {
						argument += param * param;
					}
					if ( j == 3 &&  param > 0) {
						argument += param * param;
					}
				}
			}
			penalty = -Lambda * argument;
		}
	} else {
		for( i = 0; i < defs->ngenes; i++) {
			for( j = 0; j < defs->ngenes; j++) {
				argument += (parm->T[(i*defs->ngenes)+j]*vmax[j]) * (parm->T[(i*defs->ngenes)+j]*vmax[j]);
			}
			for(j=0; j < defs->egenes; j++) {
				argument += (parm->E[(i*defs->egenes)+j]*vemax[j]) * (parm->E[(i*defs->egenes)+j]*vemax[j]);
			}
			for(j=0; j < defs->mgenes; j++) {
				argument += (parm->M[(i*defs->mgenes)+j]*vmmax[j]) * (parm->M[(i*defs->mgenes)+j]*vmmax[j]);
			}
			for(j=0; j < defs->ncouples; j++) {
				argument += (parm->P[(i*defs->ncouples)+j]*255) * (parm->P[(i*defs->ncouples)+j]*255);
			}
			argument += parm->h[i] * parm->h[i];
		}
/* e was 2.718281828 in old code---doubt it matters!! */
/* 88.7228391 is the maximum save value to use with exp (see man exp) */
		if ( (Lambda * argument) > 88.7228391 )
			return FORBIDDEN_MOVE;
		else
			penalty = (exp( Lambda * argument ) - 2.718281828459045);
	}
	if(penalty <= 0)
		penalty = 0;
	donethis = 1;
	return penalty;
}

double gcdm_score_penalty_penalty(void)
{
	int           i, j;                                /* local loop counter */
	double        argument = 0;    /* variable for penalty function argument */
	double        penalty  = 0;                      /* holds penalty result */
	double        Lambda;                                  /* penalty Lambda */
	double        *vemax;                            /* mmax in penalty vector */
	double        *vmmax;                            /* mmax in penalty vector */
	double        *vmax;          /* pointer to vmax array in penalty vector */
	if ( limits->pen_vec == NULL )
		return -1;
	Lambda = *((limits->pen_vec));  /* Lambda: first entry in penalty vector */
	vmmax   = (limits->pen_vec) + 1;               /* locate mmax and vmax */
	vmax   =   (limits->pen_vec) + 1 + defs->mgenes;
	vemax =   (limits->pen_vec) + 1 + defs->mgenes + defs->ngenes;
/* calculate penalty */
	for( i = 0; i < defs->ngenes; i++) {
		for( j = 0; j < defs->ngenes; j++) {
			argument += (parm->T[(i*defs->ngenes)+j]*vmax[j]) * (parm->T[(i*defs->ngenes)+j]*vmax[j]);
		}
		for( j = 0; j < defs->egenes; j++) {
			argument += (parm->E[(i*defs->egenes)+j]*vemax[j]) * (parm->E[(i*defs->egenes)+j]*vemax[j]);
		}
		for( j = 0; j < defs->mgenes; j++) {
			argument += (parm->M[(i*defs->mgenes)+j]*vmmax[j]) * (parm->M[(i*defs->mgenes)+j]*vmmax[j]);
		}
		for( j = 0; j < defs->ncouples; j++) {
			argument += (parm->P[(i*defs->ncouples)+j]*255) * (parm->P[(i*defs->ncouples)+j]*255);
		}
		argument += parm->h[i] * parm->h[i];
	}
/* e was 2.718281828 in old code---doubt it matters!! */
/* 88.7228391 is the maximum save value to use with exp (see man exp) */
	if ( (Lambda * argument) > 88.7228391 )
		return FORBIDDEN_MOVE;
	else
		penalty = (exp( Lambda * argument ) - 2.718281828459045);
	if(penalty <= 0)
		penalty = 0;
	return penalty;
}

/*** GetNDatapoints:  returns the number of data points (static to score.c)*
 *          CAUTION:  InitScoring must be called first!                    *
 ***************************************************************************/

int GetNDatapoints(void)
{
  return ndatapoints;
}

/*** A FUNCTION TO CONVERT PENALTY INTO EXPLICIT LIMITS ********************/

/*** Penalty2Limits: uses the inverse function of g(u) to calculate upper **
 *                   and lower limits for T, m and h using the penalty     *
 *                   lambda parameter; these limits can only be an appro-  *
 *                   ximation, since all T, m and h are added up to yield  *
 *                   u for g(u); we try to compensate for this summation   *
 *                   dividing the limits by sqrt(n); this function then    *
 *                   sets the penalty vector to NULL and supplies explicit *
 *                   limits for T, m and h, which can be used for scram-   *
 *                   bling parameters and such                             *
 *          CAUTION: this func DOES NOT RESET pen_vec, caller must do this *
 ***************************************************************************
 *                                                                         *
 * A short comment on penalty limits (JJ, Aug 7, 2001):                    *
 *                                                                         *
 * In order to produce random values of T, m and h that are                *
 * within reasonable limits when no explicit limits are used for           *
 * them, we do the following approximation:                                *
 *                                                                         *
 * First, we determine the limits of u in g(u) within which                *
 * there is no penalty assigned. This is true for all g(u) be-             *
 * tween Lambda and (1-Lambda). Therefore, we calculate the in-            *
 * verse function of g(u) for those to values to get upper and             *
 * lower limits for u (based on ideas by Eric Mjolsness).                  *
 * Since we need to sum up all T * vmax, m * mmax and h to get             *
 * u, we'll compensate by dividing the limits by the sqrt of the           *
 * number of genes in the problem. This way we think, we'll get            *
 * reasonable limits for single parameters (idea by JR).                   *
 * All the above happens in the Penalty2Limits function in sco-            *
 * re.c. When comparing parameters to limits, don't forget to              *
 * multiply Ts with vmax and ms with mmax. This happens in main            *
 * of scramble below. hs are compared as they are.                         *
 * See JJs lab notes for further detail on g(u)-inverse and such.          *
 *                                                                         *
 ***************************************************************************/

void Penalty2Limits(SearchSpace *limits)
{
	int      i, j;                                    /* local loop counters */
	double   Lambda;                                       /* penalty Lambda */
	Range    u;                         /* explicit range for u (as in g(u)) */
	Range    gu;               /* range of g(u), in which there's no penalty */
	Range    x;    /* these two are used to store intermediate results below */
	Range    y;                       /* x = 2gu - 1 ; y = sqrt( 1 - x * x ) */
	Lambda = limits->pen_vec[0];    /* use explicit variable name for Lambda */
	gu.lower = Lambda;              /* range within which there's no penalty */
	gu.upper = 1 - Lambda;
	x.lower = ( 2 * gu.lower - 1 );  /* the following calculates the inverse */
	x.upper = ( 2 * gu.upper - 1 );  /* function of g(u) for gu limits above */
                                        /* (see JJs lab notes for details) */
	y.lower = sqrt( 1 - x.lower * x.lower );
	y.upper = sqrt( 1 - x.upper * x.upper );
	u.lower = x.lower / y.lower;
	u.upper = x.upper / y.upper;
/*	u.lower = u.lower / sqrt(defs->ngenes + defs->egenes + defs->mgenes);
	u.upper = u.upper / sqrt(defs->ngenes + defs->egenes + defs->mgenes);*/
	u.lower = fabs(u.lower) * sqrt (fabs(u.lower) / ((double)defs->ngenes + (double)defs->egenes + (double)defs->mgenes)) / 255.0 / u.lower;
	u.upper = sqrt (u.upper / ((double)defs->ngenes + (double)defs->egenes + (double)defs->mgenes)) / 255.0;
/*fprintf(stdout, "l = %f u = %f", u.lower, u.upper);*/
	limits->Tlim = (Range **)calloc(defs->ngenes * defs->ngenes, sizeof(Range *));
	limits->Elim = (Range **)calloc(defs->ngenes * defs->egenes, sizeof(Range *));
	limits->Mlim = (Range **)calloc(defs->ngenes * defs->mgenes, sizeof(Range *));
	limits->Plim = (Range **)calloc(defs->ngenes * defs->ncouples, sizeof(Range *));
	limits->hlim = (Range **)calloc(defs->ngenes, sizeof(Range *));
	for ( i=0; i<defs->ngenes; i++ ) {
		for ( j=0; j<defs->ngenes; j++ ) {
			limits->Tlim[(i*defs->ngenes)+j] = (Range *)malloc(sizeof(Range));
			limits->Tlim[(i*defs->ngenes)+j]->lower = u.lower;
			limits->Tlim[(i*defs->ngenes)+j]->upper = u.upper;
		}
		for ( j=0; j<defs->egenes; j++ ) {
			limits->Elim[(i*defs->egenes)+j] = (Range *)malloc(sizeof(Range));
			limits->Elim[(i*defs->egenes)+j]->lower = u.lower;
			limits->Elim[(i*defs->egenes)+j]->upper = u.upper;
		}
		for ( j=0; j<defs->mgenes; j++ ) {
			limits->Mlim[(i*defs->mgenes)+j] = (Range *)malloc(sizeof(Range));
			limits->Mlim[(i*defs->mgenes)+j]->lower = u.lower;
			limits->Mlim[(i*defs->mgenes)+j]->upper = u.upper;
		}
		for ( j=0; j<defs->ncouples; j++ ) {
			limits->Plim[(i*defs->ncouples)+j] = (Range *)malloc(sizeof(Range));
			limits->Plim[(i*defs->ncouples)+j]->lower = u.lower;
			limits->Plim[(i*defs->ncouples)+j]->upper = u.upper;
		}
		limits->hlim[i] = (Range *)malloc(sizeof(Range));
		limits->hlim[i]->lower = u.lower;
		limits->hlim[i]->upper = u.upper;
	}
}

void Penalty2Limits2(SearchSpace *limits)
{
	int      i, j;                                    /* local loop counters */
	double   Lambda;                                       /* penalty Lambda */
	Range    u;                         /* explicit range for u (as in g(u)) */
	Range    gu;               /* range of g(u), in which there's no penalty */
	Range    x;    /* these two are used to store intermediate results below */
	Range    y;                       /* x = 2gu - 1 ; y = sqrt( 1 - x * x ) */
	Lambda = limits->pen_vec[0];    /* use explicit variable name for Lambda */
	gu.lower = Lambda;              /* range within which there's no penalty */
	gu.upper = 1 - Lambda;
	x.lower = ( 2 * gu.lower - 1 );  /* the following calculates the inverse */
	x.upper = ( 2 * gu.upper - 1 );  /* function of g(u) for gu limits above */
                                        /* (see JJs lab notes for details) */
	y.lower = sqrt( 1 - x.lower * x.lower );
	y.upper = sqrt( 1 - x.upper * x.upper );
	u.lower = x.lower / y.lower;
	u.upper = x.upper / y.upper;
	u.lower = u.lower / sqrt(defs->ngenes + defs->egenes + defs->mgenes);
	u.upper = u.upper / sqrt(defs->ngenes + defs->egenes + defs->mgenes);
/*fprintf(stdout, "l = %f u = %f", u.lower, u.upper);*/
	limits->Tlim = (Range **)calloc(defs->ngenes * defs->ngenes, sizeof(Range *));
	limits->Elim = (Range **)calloc(defs->ngenes * defs->egenes, sizeof(Range *));
	limits->Mlim = (Range **)calloc(defs->ngenes * defs->mgenes, sizeof(Range *));
	limits->Plim = (Range **)calloc(defs->ngenes * defs->ncouples, sizeof(Range *));
	limits->hlim = (Range **)calloc(defs->ngenes, sizeof(Range *));
	for ( i=0; i<defs->ngenes; i++ ) {
		for ( j=0; j<defs->ngenes; j++ ) {
			limits->Tlim[(i*defs->ngenes)+j] = (Range *)malloc(sizeof(Range));
			limits->Tlim[(i*defs->ngenes)+j]->lower = u.lower;
			limits->Tlim[(i*defs->ngenes)+j]->upper = u.upper;
		}
		for ( j=0; j<defs->egenes; j++ ) {
			limits->Elim[(i*defs->egenes)+j] = (Range *)malloc(sizeof(Range));
			limits->Elim[(i*defs->egenes)+j]->lower = u.lower;
			limits->Elim[(i*defs->egenes)+j]->upper = u.upper;
		}
		for ( j=0; j<defs->mgenes; j++ ) {
			limits->Mlim[(i*defs->mgenes)+j] = (Range *)malloc(sizeof(Range));
			limits->Mlim[(i*defs->mgenes)+j]->lower = u.lower;
			limits->Mlim[(i*defs->mgenes)+j]->upper = u.upper;
		}
		for ( j=0; j<defs->ncouples; j++ ) {
			limits->Plim[(i*defs->ncouples)+j] = (Range *)malloc(sizeof(Range));
			limits->Plim[(i*defs->ncouples)+j]->lower = u.lower;
			limits->Plim[(i*defs->ncouples)+j]->upper = u.upper;
		}
		limits->hlim[i] = (Range *)malloc(sizeof(Range));
		limits->hlim[i]->lower = u.lower;
		limits->hlim[i]->upper = u.upper;
	}
}


void AddDetailedLimits(SearchSpace *limits)
{
	int      i, j;                                    /* local loop counters */
	if ( detailed_limits == NULL )
		return;
	for ( i=0; i<defs->ngenes; i++ ) {
		for ( j=0; j<defs->ngenes; j++ ) {
			if ( limits->Tlim[(i*defs->ngenes)+j]->lower < detailed_limits->Tlim[(i*defs->ngenes)+j]->lower )
				limits->Tlim[(i*defs->ngenes)+j]->lower = detailed_limits->Tlim[(i*defs->ngenes)+j]->lower;
			if ( limits->Tlim[(i*defs->ngenes)+j]->upper > detailed_limits->Tlim[(i*defs->ngenes)+j]->upper )
				limits->Tlim[(i*defs->ngenes)+j]->upper = detailed_limits->Tlim[(i*defs->ngenes)+j]->upper;
		}
		for ( j=0; j<defs->egenes; j++ ) {
			if ( limits->Elim[(i*defs->egenes)+j]->lower < detailed_limits->Elim[(i*defs->egenes)+j]->lower )
				limits->Elim[(i*defs->egenes)+j]->lower = detailed_limits->Elim[(i*defs->egenes)+j]->lower;
			if ( limits->Elim[(i*defs->egenes)+j]->upper > detailed_limits->Elim[(i*defs->egenes)+j]->upper )
				limits->Elim[(i*defs->egenes)+j]->upper = detailed_limits->Elim[(i*defs->egenes)+j]->upper;
		}
		for ( j=0; j<defs->mgenes; j++ ) {
			if ( limits->Mlim[(i*defs->mgenes)+j]->lower < detailed_limits->Mlim[(i*defs->mgenes)+j]->lower )
				limits->Mlim[(i*defs->mgenes)+j]->lower = detailed_limits->Mlim[(i*defs->mgenes)+j]->lower;
			if ( limits->Mlim[(i*defs->mgenes)+j]->upper > detailed_limits->Mlim[(i*defs->mgenes)+j]->upper )
				limits->Mlim[(i*defs->mgenes)+j]->upper = detailed_limits->Mlim[(i*defs->mgenes)+j]->upper;
		}
		for ( j=0; j<defs->ncouples; j++ ) {
			if ( limits->Plim[(i*defs->ncouples)+j]->lower < detailed_limits->Plim[(i*defs->ncouples)+j]->lower )
				limits->Plim[(i*defs->ncouples)+j]->lower = detailed_limits->Plim[(i*defs->ncouples)+j]->lower;
			if ( limits->Plim[(i*defs->ncouples)+j]->upper > detailed_limits->Plim[(i*defs->ncouples)+j]->upper )
				limits->Plim[(i*defs->ncouples)+j]->upper = detailed_limits->Plim[(i*defs->ncouples)+j]->upper;
		}
		if ( limits->hlim[i]->lower < detailed_limits->hlim[i]->lower )
			limits->hlim[i]->lower = detailed_limits->hlim[i]->lower;
		if ( limits->hlim[i]->upper > detailed_limits->hlim[i]->upper )
			limits->hlim[i]->upper = detailed_limits->hlim[i]->upper;
	}
}


/*** FUNCTIONS THAT READ STUFF INTO STRUCTURES *****************************/
/*** ReadLimits: reads the limits section of a data file and returns the  **
 *               approriate SearchSpace struct to the calling function     *
 ***************************************************************************/
SearchSpace *ReadLimits(FILE *fp)
{
	SearchSpace       *l_limits;
	l_limits = ReReadLimits(fp, "limits");
	return l_limits;
}

SearchSpace *ReReadLimits(FILE *fp, char*section)
{
	SearchSpace       *l_limits;
	char              *record;    /* string for reading whole line of limits */
	int               i, j;                            /* local loop counter */
	char              **fmt1;     /* format strings for reading lower limits */
	char              **fmt2;     /* format strings for reading upper limits */
	char              **efmt1;     /* format strings for reading lower limits */
	char              **efmt2;     /* format strings for reading upper limits */
	char              **mfmt1;     /* format strings for reading lower limits */
	char              **mfmt2;     /* format strings for reading upper limits */
	char              **pfmt1;     /* format strings for reading lower limits */
	char              **pfmt2;     /* format strings for reading upper limits */
	char              *skip;               /* string of values to be skipped */
	const char        read_fmt[]  = "%lg";                  /* read a double */
	const char        skip_fmt1[] = "%*lg, %*lg) (";   /* ignore lower limit */
	const char        skip_fmt2[] = "%*lg) (%*lg, ";   /* ignore upper limit */
/* find limits section and check if penalty or explicit ranges are used    */
	fp = FindSection(fp, section);                   /* find limits section */
	if( !fp ) {
		warning("ReadLimits: cannot locate limits section %s", section);
		return NULL;
	}
	l_limits = (SearchSpace *)malloc(sizeof(SearchSpace));
	record = (char *)calloc(MAX_RECORD, sizeof(char *));
	skip = (char *)calloc(MAX_RECORD, sizeof(char *));
	fmt1 = (char **)calloc(defs->ngenes, sizeof(char *));
	fmt2 = (char **)calloc(defs->ngenes, sizeof(char *));
	efmt1 = (char **)calloc(defs->egenes, sizeof(char *));
	efmt2 = (char **)calloc(defs->egenes, sizeof(char *));
	mfmt1 = (char **)calloc(defs->mgenes, sizeof(char *));
	mfmt2 = (char **)calloc(defs->mgenes, sizeof(char *));
	pfmt1 = (char **)calloc(defs->ncouples, sizeof(char *));
	pfmt2 = (char **)calloc(defs->ncouples, sizeof(char *));
/* create format strings according to the number of genes */
	skip = strcpy(skip, "(");                      /* first for lower limits */
	for ( i=0; i<defs->ngenes; i++ ) {
		fmt1[i] = (char *)calloc(MAX_RECORD, sizeof(char));
		fmt1[i] = strcpy(fmt1[i], skip);
		fmt1[i] = strcat(fmt1[i], read_fmt);
		skip    = strcat(skip, skip_fmt1);
	}
	skip = strcpy(skip, "(%*lg, ");                 /* then for upper limits */
	for ( i=0; i<defs->ngenes; i++ ) {
		fmt2[i] = (char *)calloc(MAX_RECORD, sizeof(char));
		fmt2[i] = strcpy(fmt2[i], skip);
		fmt2[i] = strcat(fmt2[i], read_fmt);
		skip    = strcat(skip, skip_fmt2);
	}
	skip = strcpy(skip, "(");                      /* first for lower limits */
	for ( i=0; i<defs->egenes; i++ ) {
		efmt1[i] = (char *)calloc(MAX_RECORD, sizeof(char));
		efmt1[i] = strcpy(efmt1[i], skip);
		efmt1[i] = strcat(efmt1[i], read_fmt);
		skip    = strcat(skip, skip_fmt1);
	}
	skip = strcpy(skip, "(%*lg, ");                 /* then for upper limits */
	for ( i=0; i<defs->egenes; i++ ) {
		efmt2[i] = (char *)calloc(MAX_RECORD, sizeof(char));
		efmt2[i] = strcpy(efmt2[i], skip);
		efmt2[i] = strcat(efmt2[i], read_fmt);
		skip    = strcat(skip, skip_fmt2);
	}
	skip = strcpy(skip, "(");                      /* first for lower limits */
	for ( i=0; i<defs->mgenes; i++ ) {
		mfmt1[i] = (char *)calloc(MAX_RECORD, sizeof(char));
		mfmt1[i] = strcpy(mfmt1[i], skip);
		mfmt1[i] = strcat(mfmt1[i], read_fmt);
		skip    = strcat(skip, skip_fmt1);
	}
	skip = strcpy(skip, "(%*lg, ");                 /* then for upper limits */
	for ( i=0; i<defs->mgenes; i++ ) {
		mfmt2[i] = (char *)calloc(MAX_RECORD, sizeof(char));
		mfmt2[i] = strcpy(mfmt2[i], skip);
		mfmt2[i] = strcat(mfmt2[i], read_fmt);
		skip    = strcat(skip, skip_fmt2);
	}
	skip = strcpy(skip, "(");                      /* first for lower limits */
	for ( i=0; i<defs->ncouples; i++ ) {
		pfmt1[i] = (char *)calloc(MAX_RECORD, sizeof(char));
		pfmt1[i] = strcpy(pfmt1[i], skip);
		pfmt1[i] = strcat(pfmt1[i], read_fmt);
		skip    = strcat(skip, skip_fmt1);
	}
	skip = strcpy(skip, "(%*lg, ");                 /* then for upper limits */
	for ( i=0; i<defs->ncouples; i++ ) {
		pfmt2[i] = (char *)calloc(MAX_RECORD, sizeof(char));
		pfmt2[i] = strcpy(pfmt2[i], skip);
		pfmt2[i] = strcat(pfmt2[i], read_fmt);
		skip    = strcat(skip, skip_fmt2);
	}
	fscanf(fp, "%*s\n");                    /* advance past first title line */
	record = fgets(record, MAX_RECORD, fp);    /* read Lamda for penalty */
	if ( !strncmp(record, "N/A", 3) ) {
		l_limits->pen_vec = NULL;
	} else {
		l_limits->pen_vec = (double *)calloc(1 + defs->ngenes + defs->mgenes + defs->egenes, sizeof(double));
		if ( 1 != sscanf(record, "%lg", l_limits->pen_vec) )
			error("ReadLimits: error reading Lambda for penalty");
	}
/* initialize limits struct according to penalty or not                    *
 * first the stuff that's not dependent on penalty                         */
	l_limits->Rlim      = (Range **)calloc(defs->ngenes, sizeof(Range *));
	for ( i=0; i<defs->ngenes; i++ ) {
		l_limits->Rlim[i]      = (Range *)malloc(sizeof(Range));
	}
	l_limits->dlim      = (Range **)calloc(defs->ngenes, sizeof(Range *));
	if ( (defs->diff_schedule == 'A') || (defs->diff_schedule == 'C') ) {
		l_limits->dlim[0] = (Range *)malloc(sizeof(Range));
	} else {
		for ( i=0; i<defs->ngenes; i++ )
			l_limits->dlim[i] = (Range *)malloc(sizeof(Range));
	}
	l_limits->mlim      = (Range **)calloc(defs->ngenes, sizeof(Range *));
	if ( (defs->mob_schedule == 'A') || (defs->mob_schedule == 'C') ) {
		l_limits->mlim[0] = (Range *)malloc(sizeof(Range));
	} else {
		for ( i=0; i<defs->ngenes; i++ )
			l_limits->mlim[i] = (Range *)malloc(sizeof(Range));
	}
	l_limits->mmlim      = (Range **)calloc(defs->ngenes, sizeof(Range *));
	if ( (defs->mob_schedule == 'A') || (defs->mob_schedule == 'C') ) {
		l_limits->mmlim[0] = (Range *)malloc(sizeof(Range));
	} else {
		for ( i=0; i<defs->ngenes; i++ )
			l_limits->mmlim[i] = (Range *)malloc(sizeof(Range));
	}
	l_limits->lambdalim = (Range **)calloc(defs->ngenes, sizeof(Range *));
	for ( i=0; i<defs->ngenes; i++ ) {
		l_limits->lambdalim[i] = (Range *)malloc(sizeof(Range));
	}
	l_limits->taulim = (Range **)calloc(defs->ngenes, sizeof(Range *));
	for ( i=0; i<defs->ngenes; i++ ) {
		l_limits->taulim[i] = (Range *)malloc(sizeof(Range));
	}
/* ... and now the stuff that depends on whether we use penalty or not     */
	if ( l_limits->pen_vec == 0 ) {
		l_limits->Tlim = (Range **)calloc(defs->ngenes * defs->ngenes, sizeof(Range *));
		for ( i=0; i<defs->ngenes; i++ ) {
			for ( j=0; j<defs->ngenes; j++ )
				l_limits->Tlim[(i * defs->ngenes) + j] = (Range *)malloc(sizeof(Range));
		}
		l_limits->Elim = (Range **)calloc(defs->ngenes * defs->egenes, sizeof(Range *));
		for ( i=0; i<defs->ngenes; i++ ) {
			for ( j=0; j<defs->egenes; j++ )
				l_limits->Elim[(i * defs->egenes) + j] = (Range *)malloc(sizeof(Range));
		}
		l_limits->Mlim = (Range **)calloc(defs->ngenes * defs->mgenes, sizeof(Range *));
		for ( i=0; i<defs->ngenes; i++ ) {
			for ( j=0; j<defs->mgenes; j++ )
				l_limits->Mlim[(i * defs->mgenes) + j] = (Range *)malloc(sizeof(Range));
		}
		l_limits->Plim = (Range **)calloc(defs->ngenes * defs->ncouples, sizeof(Range *));
		for ( i=0; i<defs->ngenes; i++ ) {
			for ( j=0; j<defs->ncouples; j++ )
				l_limits->Plim[(i * defs->ncouples) + j] = (Range *)malloc(sizeof(Range));
		}
		l_limits->hlim = (Range **)calloc(defs->ngenes, sizeof(Range *));
		for ( i=0; i<defs->ngenes; i++ ) {
			l_limits->hlim[i] = (Range *)malloc(sizeof(Range));
		}
	} else {
		l_limits->Tlim = NULL;
		l_limits->Mlim = NULL;
		l_limits->Plim = NULL;
		l_limits->Elim = NULL;
		l_limits->hlim = NULL;
	}
/* ... and finally read the actual values from the data file               */
	fscanf(fp, "%*s\n");                          /* advance past title line */
	record = fgets(record, MAX_RECORD, fp);     /* loop to read R limits */
	for ( i=0; i<defs->ngenes; i++ ) {
		if ( 1 != sscanf(record, (const char *)fmt1[i], &l_limits->Rlim[i]->lower) )
			error("ReadLimits: error reading promoter strength (R) limits");
		if ( 1 != sscanf(record, (const char *)fmt2[i], &l_limits->Rlim[i]->upper) )
			error("ReadLimits: error reading promoter strength (R) limits");
	}
	fscanf(fp, "%*s\n");
/* using explicit limits? */
	if ( l_limits->pen_vec == 0 ) {
		for ( i=0; i<defs->ngenes; i++ ) {     /* loops to read T matrix limits */
			record = fgets(record, MAX_RECORD, fp);
			for ( j=0; j<defs->ngenes; j++ ) {
				if ( 1 != sscanf(record, (const char *)fmt1[j], &l_limits->Tlim[(i * defs->ngenes) + j]->lower ) )
	  				error("ReadLimits:: error reading T matrix limits");
				if ( 1 != sscanf(record, (const char *)fmt2[j], &l_limits->Tlim[(i * defs->ngenes) + j]->upper ) )
					error("ReadLimits:: error reading T matrix limits");
			}
		}
		fscanf(fp, "%*s\n");
		if ( defs->egenes > 0 )
			for ( i=0; i<defs->ngenes; i++ ) {
				record = fgets(record, MAX_RECORD, fp);
				for ( j=0; j<defs->egenes; j++ ) {
					if ( 1 != sscanf(record, (const char *)efmt1[j], &l_limits->Elim[(i * defs->egenes) + j]->lower ) )
	  					error("ReadLimits:: error reading E matrix limits");
					if ( 1 != sscanf(record, (const char *)efmt2[j], &l_limits->Elim[(i * defs->egenes) + j]->upper ) )
						error("ReadLimits:: error reading E matrix limits");
				}
			}
		fscanf(fp, "%*s\n");
		if ( defs->mgenes > 0 )
			for ( i=0; i<defs->ngenes; i++ ) {
				record = fgets(record, MAX_RECORD, fp);
					for ( j=0; j<defs->mgenes; j++ ) {
						if ( 1 != sscanf(record, (const char *)mfmt1[j], &l_limits->Mlim[(i * defs->mgenes) + j]->lower ) )
	  						error("ReadLimits:: error reading M matrix limits");
						if ( 1 != sscanf(record, (const char *)mfmt2[j], &l_limits->Mlim[(i * defs->mgenes) + j]->upper ) )
							error("ReadLimits:: error reading M matrix limits");
					}
			}
		fscanf(fp, "%*s\n");
		if ( defs->ncouples > 0 )
			for ( i=0; i<defs->ngenes; i++ ) {
				record = fgets(record, MAX_RECORD, fp);
					for ( j=0; j<defs->ncouples; j++ ) {
						if ( 1 != sscanf(record, (const char *)pfmt1[j], &l_limits->Plim[(i * defs->ncouples) + j]->lower ) )
	  						error("ReadLimits:: error reading M matrix limits");
						if ( 1 != sscanf(record, (const char *)pfmt2[j], &l_limits->Plim[(i * defs->ncouples) + j]->upper ) )
							error("ReadLimits:: error reading M matrix limits");
					}
			}
		fscanf(fp, "%*s\n");
		record = fgets(record, MAX_RECORD, fp);   /* loop to read h limits */
		for ( i=0; i<defs->ngenes; i++ ) {
			if ( 1 != sscanf(record, (const char *)fmt1[i], &l_limits->hlim[i]->lower) )
				error("ReadLimits: error reading promoter threshold (h) limits");
			if ( 1 != sscanf(record, (const char *)fmt2[i], &l_limits->hlim[i]->upper) )
				error("ReadLimits: error reading promoter threshold (h) limits");
		}
/* using penalty? -> ignore this part of the limit section */
	} else {
		for ( i = 0; i < 9; i++ )
			fscanf(fp, "%*s\n");
	}
	fscanf(fp, "%*s\n");        /* diffusion paramter limit(s) are read here */
	record = fgets(record, MAX_RECORD, fp);
	if ( (defs->diff_schedule == 'A') || (defs->diff_schedule == 'C') ) {
		if ( 1 != sscanf(record, (const char *)fmt1[0],  &l_limits->dlim[0]->lower) )
			error("ReadLimits: error reading diffusion parameter limit (d)");
		if ( 1 != sscanf(record, (const char *)fmt2[0], &l_limits->dlim[0]->upper) )
			error("ReadLimits: error reading diffusion parameter limit (d)");
	} else {
		for ( i=0; i<defs->ngenes; i++ ) {
			if ( 1 != sscanf(record, (const char *)fmt1[i], &l_limits->dlim[i]->lower) )
				error("ReadLimits: error reading diffusion parameter limits (d)");
			if ( 1 != sscanf(record, (const char *)fmt2[i], &l_limits->dlim[i]->upper) )
				error("ReadLimits: error reading diffusion parameter limits (d)");
		}
	}
	fscanf(fp, "%*s\n");
	record = fgets(record, MAX_RECORD, fp);  /* loop to read lambda lims */
	for ( i=0; i<defs->ngenes; i++ ) {
		if ( 1 != sscanf(record, (const char *)fmt1[i], &l_limits->lambdalim[i]->upper) )
			error("ReadLimits: error reading lambda limits");
		if ( 1 != sscanf(record, (const char *)fmt2[i], &l_limits->lambdalim[i]->lower) )
			error("ReadLimits: error reading lambda limits");
		if ( l_limits->lambdalim[i]->upper > 0 ) {
			l_limits->lambdalim[i]->upper = log(2.) / l_limits->lambdalim[i]->upper;
			l_limits->lambdalim[i]->lower = log(2.) / l_limits->lambdalim[i]->lower;
		} else {
			double tmp = l_limits->lambdalim[i]->lower;
			l_limits->lambdalim[i]->lower = l_limits->lambdalim[i]->upper;
			l_limits->lambdalim[i]->upper = tmp;
		}
	}
	fscanf(fp, "%*s\n");
	record = fgets(record, MAX_RECORD, fp);  /* loop to read lambda lims */
	for ( i = 0; i < defs->ngenes; i++ ) {
		if ( 1 != sscanf(record, (const char *)fmt1[i], &l_limits->taulim[i]->lower) )
			error("ReadLimits: error reading lambda limits");
		if ( 1 != sscanf(record, (const char *)fmt2[i], &l_limits->taulim[i]->upper) )
			error("ReadLimits: error reading lambda limits");
	}
	fscanf(fp, "%*s\n");        /* diffusion paramter limit(s) are read here */
	record = fgets(record, MAX_RECORD, fp);
	if ( (defs->mob_schedule == 'A') || (defs->mob_schedule == 'C') ) {
		if ( 1 != sscanf(record, (const char *)fmt1[0],  &l_limits->mlim[0]->lower) )
			error("ReadLimits: error reading diffusion parameter limit (d)");
		if ( 1 != sscanf(record, (const char *)fmt2[0], &l_limits->mlim[0]->upper) )
			error("ReadLimits: error reading diffusion parameter limit (d)");
	} else {
		for ( i=0; i<defs->ngenes; i++ ) {
			if ( 1 != sscanf(record, (const char *)fmt1[i], &l_limits->mlim[i]->lower) )
				error("ReadLimits: error reading diffusion parameter limits (d)");
			if ( 1 != sscanf(record, (const char *)fmt2[i], &l_limits->mlim[i]->upper) )
				error("ReadLimits: error reading diffusion parameter limits (d)");
		}
	}
	fscanf(fp, "%*s\n");        /* diffusion paramter limit(s) are read here */
	record = fgets(record, MAX_RECORD, fp);
	if ( (defs->mob_schedule == 'A') || (defs->mob_schedule == 'C') ) {
		if ( 1 != sscanf(record, (const char *)fmt1[0],  &l_limits->mmlim[0]->lower) )
			error("ReadLimits: error reading diffusion parameter limit (d)");
		if ( 1 != sscanf(record, (const char *)fmt2[0], &l_limits->mmlim[0]->upper) )
			error("ReadLimits: error reading diffusion parameter limit (d)");
	} else {
		for ( i=0; i<defs->ngenes; i++ ) {
			if ( 1 != sscanf(record, (const char *)fmt1[i], &l_limits->mmlim[i]->lower) )
				error("ReadLimits: error reading diffusion parameter limits (d)");
			if ( 1 != sscanf(record, (const char *)fmt2[i], &l_limits->mmlim[i]->upper) )
				error("ReadLimits: error reading diffusion parameter limits (d)");
		}
	}
	free(record);
	free(skip);
	for (i=0; i<defs->ngenes; i++ ) {
		free(fmt1[i]);
		free(fmt2[i]);
	}
	free(fmt1);
	free(fmt2);
	for (i=0; i<defs->egenes; i++ ) {
		free(efmt1[i]);
		free(efmt2[i]);
	}
	free(efmt1);
	free(efmt2);
	for (i=0; i<defs->mgenes; i++ ) {
		free(mfmt1[i]);
		free(mfmt2[i]);
	}
	free(mfmt1);
	free(mfmt2);
	return l_limits;
}

/*** List2Facts: takes a Dlist and returns the corresponding DataTable *****
 *               structure we use for facts data.                          *
 *                                                                         *
 * An extensive comment about indices: (by JJ) *****************************
 *                                                                         *
 *               All data points with -1 as a value are NOT read from the  *
 *               data file. The difference between such ignored and zero   *
 *               values is crucial: -1 data points WILL NOT BE COMPARED TO *
 *               simulation data, whereas 0 means NO PROTEIN AT THAT TIME  *
 *               IN THAT NUCLEUS.                                          *
 *               Index numbers help maintain the integrity of the data.    *
 *               An index number is defined as the array index at which a  *
 *               protein concentration would be if the data was complete,  *
 *               i.e. available for all nuclei at all times. In this way   *
 *               a sparse set of data can be compared to a complete set of *
 *               simulation output.                                        *
 *               Thus, indices are defined as starting from 1 for each     *
 *               DataRecord (each time step) and increase by one for each  *
 *               gene in each nucleus in the order predefined by JR.       *
 *                                                                         *
 ***************************************************************************/

DataTable *List2Facts_bcd(Dlist *inlist, int *nsize)
{
	int TIME_IGNORE = 0;
	int       i = 0;
	int       j;                                      /* local loop counters */
	double    now = -999999999.;            /* assigns data to specific time */
	Dlist     *current;                    /* holds current element of Dlist */
	DataTable *D;                                 /* local copy of DataTable */
	D = (DataTable *)malloc(sizeof(DataTable)); /* Initialize DataTable structure */
	D->size = 0;
	D->record = NULL;
	(*nsize) = 0;
/*** for loop: steps through linked list and transfers facts into Data-    *
 *             Records, one for each time step                             *
 ***************************************************************************/
	for (current=inlist; current; current=current->next) {
		if ( current->d[0] != now ) {             /* a new time point: allocate */
			now = current->d[0];                             /* the time is now! */
			D->size++;                           /* one DataRecord for each time */
			D->record = (DataRecord *)realloc(D->record,D->size*sizeof(DataRecord)); /* allocate DataRecord */
			D->record[D->size-1].time = now;          /* next three lines define */
			D->record[D->size-1].size = 0;                /* DataRecord for each */
			D->record[D->size-1].array = NULL;                      /* time step */
			i = 0;
			TIME_IGNORE = check_time_ignore( now );
		}
		if ( check_scoring_window ( current->lineage ) ) {
			for(j=1; j <= defs->ngenes; j++) {     /* always: read concs into array */
				if ( current->d[j] != IGNORE && TIME_IGNORE == 0 ) { /* valid conc? if IGNORE -> ignore! */
					D->record[D->size-1].size++;            /* one more in this record */
					D->record[D->size-1].array = (DataPoint *)realloc(D->record[D->size-1].array, D->record[D->size-1].size * sizeof(DataPoint)); /* reallocate memory for array! */
/* the following two lines assign concentration value and index ************/
					D->record[D->size-1].array[D->record[D->size-1].size-1].conc = current->d[j];
					D->record[D->size-1].array[D->record[D->size-1].size-1].index = i;
					(*nsize)++;
				}
				i++;
			}
		} else {
			i += defs->ngenes;
		}
	}
	return D;
}

DataTable *List2Facts(Dlist *inlist, int *nsize)
{
	int TIME_IGNORE = 0;
	int       i = 0;
	int       j;                                      /* local loop counters */
	double    now = -999999999.;            /* assigns data to specific time */
	Dlist     *current;                    /* holds current element of Dlist */
	DataTable *D;                                 /* local copy of DataTable */
	D = (DataTable *)malloc(sizeof(DataTable)); /* Initialize DataTable structure */
	D->size = 0;
	D->record = NULL;
	(*nsize) = 0;
/*** for loop: steps through linked list and transfers facts into Data-    *
 *             Records, one for each time step                             *
 ***************************************************************************/
	for (current=inlist; current; current=current->next) {
		if ( current->d[0] != now ) {             /* a new time point: allocate */
			now = current->d[0];                             /* the time is now! */
			D->size++;                           /* one DataRecord for each time */
			D->record = (DataRecord *)realloc(D->record,D->size*sizeof(DataRecord)); /* allocate DataRecord */
			D->record[D->size-1].time = now;          /* next three lines define */
			D->record[D->size-1].size = 0;                /* DataRecord for each */
			D->record[D->size-1].array = NULL;                      /* time step */
			i = 0;
			TIME_IGNORE = check_time_ignore( now );
		}
		if ( check_scoring_window ( current->lineage ) ) {
			for(j=1; j <= defs->ngenes; j++) {     /* always: read concs into array */
				if ( current->d[j] != IGNORE && TIME_IGNORE == 0 && gene_ignore[j-1] == 0 ) { /* valid conc? if IGNORE -> ignore! */
					D->record[D->size-1].size++;            /* one more in this record */
					D->record[D->size-1].array = (DataPoint *)realloc(D->record[D->size-1].array, D->record[D->size-1].size * sizeof(DataPoint)); /* reallocate memory for array! */
/* the following two lines assign concentration value and index ************/
					D->record[D->size-1].array[D->record[D->size-1].size-1].conc = current->d[j];
					D->record[D->size-1].array[D->record[D->size-1].size-1].index = i;
					(*nsize)++;
				}
				i++;
			}
		} else {
			i += defs->ngenes;
		}
	}
	return D;
}

int check_time_ignore( double now )
{
	int i, res = 0;
	const double big_epsilon = BIG_EPSILON;
	for ( i = 0; i < ntime_ignore; i++ ) {
		if ( fabs ( now - time_ignore_vals[i] ) < big_epsilon ) {
			res = 1;
			break;
		}
	}
	if ( NEED_TIME > 0 && fabs ( now - NEED_TIME ) > big_epsilon ) {
		res = 1;
	}
	return res;
}

/* functions added my MacKoel */
/* calculates Lagrangians and gradient by calling functions in integrate */
void PsiGrad_bcd()
{
	int i;
	ReNewGrad();
	for ( i=0; i<nalleles; i++) {
		Blastoderm_setThisGenotype(Flies[i]);
		Blastoderm_SolvePsi(Flies[i], stepsize, accuracy, slogptr);
		BlastodermGrad(Flies[i], integ, NumPar, stepsize, accuracy, NULL);
	}
	if(limits->pen_vec != NULL)
		AddPenGrad_bcd();
}

/* functions added my MacKoel */
/* calculates Lagrangians and gradient by calling functions in integrate */
void PsiGrad()
{
	int i;
	ReNewGrad();
	for ( i=0; i<nalleles; i++) {
		Blastoderm_setThisGenotype(Flies[i]);
		Blastoderm_SolvePsi(Flies[i], stepsize, accuracy, slogptr);
		BlastodermGrad(Flies[i], integ, NumPar, stepsize, accuracy, NULL);
	}
	if(limits->pen_vec != NULL)
		AddPenGrad();
}

void gcdm_score_grad()
{
	int i;
	ReNewGrad();
	for ( i=0; i<nalleles; i++) {
		Blastoderm_setThisGenotype(Flies[i]);
		Blastoderm_SolvePsi(Flies[i], stepsize, accuracy, slogptr);
		BlastodermGrad(Flies[i], integ, NumPar, stepsize, accuracy, NULL);
	}
}

void PsiGrad_idx_bcd(int index)
{
	int i;
	ReNewGrad();
	for ( i=0; i<nalleles; i++) {
		Blastoderm_setThisGenotype(Flies[i]);
		Blastoderm_SolvePsi(Flies[i], stepsize, accuracy, slogptr);
		BlastodermGrad(Flies[i], integ, NumPar, stepsize, accuracy, NULL);
	}
	if(limits->pen_vec != NULL)
		AddPenGrad_bcd();
}

void PsiGrad_idx(int index)
{
	int i;
	ReNewGrad();
	for ( i=0; i<nalleles; i++) {
		Blastoderm_setThisGenotype(Flies[i]);
		Blastoderm_SolvePsi(Flies[i], stepsize, accuracy, slogptr);
		BlastodermGrad(Flies[i], integ, NumPar, stepsize, accuracy, NULL);
	}
	if(limits->pen_vec != NULL)
		AddPenGrad();
}

/* AddPenGrad: adds penalty to gradient*/

void AddPenGrad_bcd()
{
	int i,j;
	double    *vmmax;      /* max. bcd protein conc. in maternal penalty data */
	double    *vemax;      /* max. bcd protein conc. in maternal penalty data */
	double    *vmax;           /* array for max. prot concs. in penalty data */
	double grad1,plambda;
	plambda = *((limits->pen_vec));  /* Lambda: first entry in penalty vector */
	vmmax = (limits->pen_vec) + 1;       /* mmax stored in limits->pen_vec[1] */
	vmax = (limits->pen_vec) + 1 + defs->mgenes;     /* vmax stored in penalty vector array */
	vemax = (limits->pen_vec) + 1 + defs->ngenes + defs->mgenes;     /* vmax stored in penalty vector array */
	if ( Penalty == FORBIDDEN_MOVE )
		Penalty = GetPenalty_bcd();
	grad1 = -2.0*plambda*Penalty;
	for( i = 0; i < defs->ngenes; i++) {
		for( j = 0; j < defs->mgenes; j++) {
			grad->M[i*defs->mgenes+j] += grad1*parm->M[i*defs->mgenes+j]*vmmax[j]*vmmax[j];
		}
		for( j = 0; j < defs->egenes; j++) {
			grad->E[i*defs->egenes+j] += grad1*parm->E[i*defs->egenes+j]*vemax[j]*vemax[j];
		}
		grad->h[i] += grad1*parm->h[i];
		for( j = 0; j < defs->ngenes; j++) {
			grad->T[i*defs->ngenes+j] += grad1*parm->T[i*defs->ngenes+j]*vmax[j]*vmax[j];
		}
	}
}

/* AddPenGrad: adds penalty to gradient*/

void AddPenGrad()
{
	int i,j;
	double    *vmmax;      /* max. bcd protein conc. in maternal penalty data */
	double    *vemax;      /* max. bcd protein conc. in maternal penalty data */
	double    *vmax;           /* array for max. prot concs. in penalty data */
	double grad1,plambda;
	plambda = *((limits->pen_vec));  /* Lambda: first entry in penalty vector */
	vmmax = (limits->pen_vec) + 1;       /* mmax stored in limits->pen_vec[1] */
	vmax = (limits->pen_vec) + 1 + defs->mgenes;     /* vmax stored in penalty vector array */
	vemax = (limits->pen_vec) + 1 + defs->ngenes + defs->mgenes;     /* vmax stored in penalty vector array */
	if ( Penalty == FORBIDDEN_MOVE )
		Penalty = GetPenalty();
	grad1 = -2.0*plambda*Penalty;
	for( i = 0; i < defs->ngenes; i++) {
		for( j = 0; j < defs->mgenes; j++) {
			grad->M[i*defs->mgenes+j] += grad1*parm->M[i*defs->mgenes+j]*vmmax[j]*vmmax[j];
		}
		for( j = 0; j < defs->egenes; j++) {
			grad->E[i*defs->egenes+j] += grad1*parm->E[i*defs->egenes+j]*vemax[j]*vemax[j];
		}
		grad->h[i] += grad1*parm->h[i];
		for( j = 0; j < defs->ngenes; j++) {
			grad->T[i*defs->ngenes+j] += grad1*parm->T[i*defs->ngenes+j]*vmax[j]*vmax[j];
		}
	}
}

/*** InitScoring2: transforms parameters first time                  *******
 * and allocates static structures                                         *
 ***************************************************************************/

void InitScoring2()
{
	JumperPsi=Jump;
	CF = V_Tconc;
	CFP = V_Pconc;
/*  PrintParameters(stdout,parm,"init_scoring2",16);*/
	grad = GetGrad();
	NumPar = defs->ngenes * ( defs->ngenes + defs->egenes + defs->mgenes/* + defs->ncouples + 2*/ + 5 );
	integ = (double**)calloc( NumPar, sizeof(double*) );
	GetInteg(integ);
}

DataRecord*Jump(char*genotype,double time_point)
{
	const double big_epsilon = BIG_EPSILON;    /* used to recognize new time */
	DataTable    fact_tab;      /* stores a copy of the Facts (from GenoTab) */
	DataRecord    *current;           /* used to extract an element of DataTable */
	int          gindex;
	int          tindex;                       /* index for facts timepoints */
	double       time;                      /* time for each facts timepoint */
/* extablish a pointer to the appropriate facts section in GenoTab */
	gindex   = GetIndex(genotype);
	fact_tab = *(facttype[gindex].ptr->facts);
/* the following loop adds all squared differences for the whole Solution */
	current = fact_tab.record;
	tindex = 0;
	do {
		time = current->time;
		current++;
		tindex++;
	} while( ( tindex < fact_tab.size ) && ( fabs(time - time_point) >= big_epsilon ) );
	if ( tindex > fact_tab.size ) {
		error("Jump: undefined time point: %f",time_point);
	}
	current--;
	return current;
}

/* FreeScoring2: frees buffers */
void DiffJump()
{
	int gindex;
	for ( gindex=0; gindex<nalleles; gindex++) {
		Blastoderm_SolveJump(Flies[gindex], stepsize, accuracy, slogptr);
		PrintBlastoderm(stdout, Flies[gindex]->Solution, "jump_debug_output", MAX_PRECISION, defs->ngenes);
	}
}



/*** A FUNCTION THAT WRITES LIMITS INTO THE DATA FILE ******************/

/*** WriteLimits:
 ***************************************************************************/

void WriteLimits(char *name, SearchSpace *p, char *title, int ndigits)
{
	char   *temp;                                     /* temporary file name */
	FILE   *outfile;                                  /* name of output file */
	FILE   *tmpfile;                               /* name of temporary file */
	char*title_of_above;
	title_of_above=(char*)calloc(MAX_RECORD,sizeof(char));
	sprintf(title_of_above,"tweak");
	GetSectionPosition(&outfile,&tmpfile,title,title_of_above,name,&temp);
/* now we write the eqparms section into the tmpfile */
	PrintLimits(tmpfile, p, title, ndigits);
	WriteRest(&outfile,&tmpfile,name,&temp);
	free(title_of_above);
}

/*** PrintLimits:
 ***************************************************************************/
void PrintLimits(FILE *fp, SearchSpace *p, char *title, int ndigits)
{
	int    i, j;                                      /* local loop counters */
	double lambda_tmp;                           /* temporary var for lambda */
	fprintf(fp, "$%s\n", title);
	fprintf(fp, "Lambda_for_penalty:\nN/A\n");
	fprintf(fp, "promoter_strengths_ranges:\n");             /* Rs are written here */
	for ( i=0; i<defs->ngenes; i++ )
		fprintf(fp, "(%.*f, %.*f) ", ndigits, p->Rlim[i]->lower, ndigits, p->Rlim[i]->upper);
	fprintf(fp, "\n");
	fprintf(fp, "genetic_interconnect_matrix_ranges:\n");        /* Ts written here */
	for ( i=0; i<defs->ngenes; i++ ) {
		for ( j=0; j<defs->ngenes; j++ )
			fprintf(fp, "(%.*f, %.*f) ", ndigits, p->Tlim[(i*defs->ngenes)+j]->lower, ndigits, p->Tlim[(i*defs->ngenes)+j]->upper);
		fprintf(fp, "\n");
	}
	fprintf(fp, "external_connection_strengths_ranges:\n");      /* ms written here */
	for ( i=0; i<defs->ngenes; i++ ) {
		for ( j=0; j<defs->egenes; j++ )
			fprintf(fp, "(%.*f, %.*f) ", ndigits, p->Elim[(i*defs->ngenes)+j]->lower, ndigits, p->Elim[(i*defs->ngenes)+j]->upper);
		fprintf(fp, "\n");
	}
	fprintf(fp, "maternal_connection_strengths_ranges:\n");      /* ms written here */
	for ( i=0; i<defs->ngenes; i++ ) {
		for ( j=0; j<defs->mgenes; j++ )
			fprintf(fp, "(%.*f, %.*f) ", ndigits, p->Mlim[(i*defs->ngenes)+j]->lower, ndigits, p->Mlim[(i*defs->ngenes)+j]->upper);
		fprintf(fp, "\n");
	}
	fprintf(fp, "promoter_thresholds_ranges:\n");            /* hs are written here */
	for ( i=0; i<defs->ngenes; i++ )
		fprintf(fp, "(%.*f, %.*f) ", ndigits, p->hlim[i]->lower, ndigits, p->hlim[i]->upper);
	fprintf(fp, "\n");
	fprintf(fp, "diffusion_parameter(s)_ranges:\n");         /* ds are written here */
	if ( (defs->diff_schedule == 'A') || (defs->diff_schedule == 'C') )
		fprintf(fp, "(%.*f, %.*f) ", ndigits, p->dlim[0]->lower, ndigits, p->dlim[0]->upper);
	else
		for ( i=0; i<defs->ngenes; i++ )
	fprintf(fp, "(%.*f, %.*f) ", ndigits, p->dlim[i]->lower, ndigits, p->dlim[i]->upper);
	fprintf(fp, "\n");
	fprintf(fp, "protein_half_lives_ranges:\n");        /* lambdas are written here */
	for ( i=0; i<defs->ngenes; i++ ) {
		lambda_tmp = log(2.) / p->lambdalim[i]->upper;           /* conversion done here */
		fprintf(fp, "(%.*f, ", ndigits, lambda_tmp);
		lambda_tmp = log(2.) / p->lambdalim[i]->lower;           /* conversion done here */
		fprintf(fp, "%.*f) ", ndigits, lambda_tmp);
	}
	fprintf(fp, "\n$$\n");
/*  fflush(fp);*/
}


/* eo functions added my MacKoel */

double EvalDeriv(NArrPtr *deriv, char *genotype)
{
	const double big_epsilon = BIG_EPSILON;    /* used to recognize new time */
	DataTable *deriv_tab;
	DataPoint *point;           /* used to extract an element of DataTable */
	int          gindex;                      /* index for specific genotype */
	int          tindex;                       /* index for facts timepoints */
	int          sindex;                    /* index for Solution timepoints */
	int          vindex;                        /* index for facts datapoint */
	double       time;                      /* time for each facts timepoint */
	double       *v;                   /* ptr to solution for each timepoint */
	double       chisq = 0;                /* the final score to be returned */
	gindex = GetIndex(genotype);
	deriv_tab = derivtype[gindex].ptr->facts;
	sindex   = 0;
	for (tindex=0; tindex < deriv_tab->size; tindex++) {
		time = deriv_tab->record[tindex].time;
/* new time step to be compared? -> get the Solution for this time */
		while( fabs(time - deriv->array[sindex].time) >= big_epsilon )
			sindex++;
		v = deriv->array[sindex].state->array;
/* this loop steps through the Solution for a specific timepoint and       */
/* evaluates the squared diffs                                             */
		for ( vindex = 0; vindex < deriv_tab->record[tindex].size; vindex++) {
			point = &(deriv_tab->record[tindex].array[vindex]);
			point->difference = point->conc - v[point->index];
			chisq += point->difference * point->difference;
		}
	}
	return chisq;                                              /* that's it! */
}

NArrPtr*Facts2Solution(DataTable *facts)
{
	NArrPtr*Solution;
	int i, j;
	int n;
	Solution = (NArrPtr*)malloc(sizeof(NArrPtr));
	Solution->size = facts->size;
	Solution->array = (NucState *)calloc(Solution->size, sizeof(NucState));
	for( i = 0; i < Solution->size; i++) {
		Solution->array[i].time = facts->record[i].time;
		n = defs->ngenes * GetNNucs(facts->record[i].time);
		Solution->array[i].state = (DArrPtr*)malloc(sizeof(DArrPtr));
		Solution->array[i].state->size = n;
		Solution->array[i].state->array = (double *)calloc( n, sizeof(double));
		for ( j = 0; j < n; j++) {
			Solution->array[i].state->array[j] = 0;
		}
		for ( j = 0; j < facts->record[i].size; j++) {
			Solution->array[i].state->array[facts->record[i].array[j].index] = facts->record[i].array[j].conc;
		}
	}
	return Solution;
}

void gcdm_score_add_dfacts(DataTable *facts)
{
	const double big_epsilon = BIG_EPSILON;
	int i, j, ngenes;
	int g, index;
	double *means;
	double dot;
	ngenes = defs->ngenes;
	for( i = 0; i < facts->size; i++) {
		means = (double*)calloc(ngenes, sizeof(double));
		for( g = 0; g < ngenes; g++) {
			means[g] = 0.0;
		}
		for ( j = 0; j < facts->record[i].size; j++) {
			index = facts->record[i].array[j].index;
			g = index % ngenes;
			dot = facts->record[i].array[j].conc;
			means[g] += dot;
		}
		facts->record[i].dfacts = means;
	}
}

void gcdm_score_convert_facts(DataTable *facts, int normalize)
{
	const double big_epsilon = BIG_EPSILON;
	int i, j, ngenes;
	int g, index;
	double *means;
	int *hits;
	double dot;
	ngenes = defs->ngenes;
	if ( normalize ) {
		means = (double*)calloc(ngenes, sizeof(double));
		hits = (int*)calloc(ngenes, sizeof(int));
		for( i = 0; i < ngenes; i++) {
			means[i] = 0.0;
			hits[i] = 0;
		}
		for( i = 0; i < facts->size; i++) {
			for ( j = 0; j < facts->record[i].size; j++) {
				index = facts->record[i].array[j].index;
				g = index % ngenes;
				dot = facts->record[i].array[j].conc;
				if ( dot > big_epsilon ) {
					means[g] += 1.0 / dot;
					hits[g]++;
				}
			}
		}
	}
	for( i = 0; i < facts->size; i++) {
		for ( j = 0; j < facts->record[i].size; j++) {
			index = facts->record[i].array[j].index;
			g = index % ngenes;
			dot = facts->record[i].array[j].conc;
			if ( dot > big_epsilon ) {
				facts->record[i].array[j].conc = 1.0 / dot;
			} else {
				facts->record[i].array[j].conc = 1.0;
			}
			if ( normalize ) {
				if ( hits[g] > 0 ) {
					facts->record[i].array[j].conc /= means[g];
				}
			}
		}
	}
	if ( normalize ) {
		free(means);
		free(hits);
	}
}

void gcdm_score_convert_facts_sqrt(DataTable *facts, int normalize)
{
	const double big_epsilon = BIG_EPSILON;
	int i, j, ngenes;
	int g, index;
	double *means;
	int *hits;
	double dot;
	ngenes = defs->ngenes;
	for( i = 0; i < facts->size; i++) {
		for ( j = 0; j < facts->record[i].size; j++) {
			index = facts->record[i].array[j].index;
			dot = facts->record[i].array[j].conc;
			if ( dot > big_epsilon ) {
				facts->record[i].array[j].conc = sqrt ( dot );
			}
		}
	}
}


void SolveDeriv(NArrPtr*facts, char *genotype, NArrPtr*Solution)
{
	int i;
	InitGenotypeIndex(genotype);
	for( i = 0; i < Solution->size; i++) {
		int n = facts->array[i].state->size;
		double time = facts->array[i].time;
		int rule;
		unsigned int ccycle;
		rule = GetRule(time);
		SetRule(rule);
		ccycle = GetCCycle(time);
		SetCCycle(ccycle);
		Mutate(genotype);
		DvdtOrig( facts->array[i].state->array, time, Solution->array[i].state->array, n);
	}
}

double DerivScore_bcd()
{
	int i;
	double chisq;
	if( !tt_init_flag )
		InitTTs();
	chisq = CheckSearchSpace_bcd();
	if ( chisq == FORBIDDEN_MOVE )
		return FORBIDDEN_MOVE;
	for ( i = 0; i < nalleles; i++) {
		SolveDeriv(derivfacts[i], derivtype[i].genotype, derivsolution[i]);
		chisq += EvalDeriv(derivsolution[i], derivtype[i].genotype);
	}
	return chisq;
}

double DerivScore()
{
	int i;
	double chisq;
	if( !tt_init_flag )
		InitTTs();
	chisq = CheckSearchSpace();
	if ( chisq == FORBIDDEN_MOVE )
		return FORBIDDEN_MOVE;
	for ( i = 0; i < nalleles; i++) {
		SolveDeriv(derivfacts[i], derivtype[i].genotype, derivsolution[i]);
		chisq += EvalDeriv(derivsolution[i], derivtype[i].genotype);
	}
	return chisq;
}

void InitDerivScore_bcd(FILE *fp)
{
	InitFacts_bcd(fp);
	InitLimits(fp);
	InitDerivFacts_bcd(fp);
	InitDerivSolution();
/*added by MacKoel*/
	parm = GetParameters();
/*****************/
}

void InitDerivScore(FILE *fp)
{
	InitFacts(fp);
	InitLimits(fp);
	InitDerivFacts(fp);
	InitDerivSolution();
/*added by MacKoel*/
	parm = GetParameters();
/*****************/
}

void InitDerivFacts_bcd(FILE *fp)
{
	int i, b;
	Dlist *inlist;
	Slist *current;
	char *section;
	section = (char*)calloc(MAX_RECORD, sizeof(char));
	if ( !(derivtype=(GenoType *)calloc(nalleles, sizeof(GenoType))) )
		error("InitDerivFacts: could not allocate facttype struct");
	for ( current = mat_genotypes, i = 0; current; current = current->next, i++) {
		sprintf(section, "%s_df", current->fact_section);
		if( !( inlist = ReadData( fp, section, &b, defs->ngenes + 1) ) )
			error("InitDerivFacts: no Dlist to initialize facts");
		else {
			if ( !( derivtype[i].genotype = (char *)calloc(MAX_RECORD, sizeof(char)) ) )
				error("InitDerivFacts: could not allocate facts genotype string");
			derivtype[i].genotype = strcpy(derivtype[i].genotype, current->name);
			derivtype[i].ptr = (DataPtr*)malloc(sizeof(DataPtr));
			derivtype[i].ptr->facts = List2Facts_bcd(inlist, &b);
			free_Dlist(inlist);
		}
	}
	free(section);
}

void InitDerivFacts(FILE *fp)
{
	int i, b;
	Dlist *inlist;
	Slist *current;
	char *section;
	section = (char*)calloc(MAX_RECORD, sizeof(char));
	if ( !(derivtype=(GenoType *)calloc(nalleles, sizeof(GenoType))) )
		error("InitDerivFacts: could not allocate facttype struct");
	for ( current = mat_genotypes, i = 0; current; current = current->next, i++) {
		sprintf(section, "%s_df", current->fact_section);
		if( !( inlist = ReadData( fp, section, &b, defs->ngenes + 1) ) )
			error("InitDerivFacts: no Dlist to initialize facts");
		else {
			if ( !( derivtype[i].genotype = (char *)calloc(MAX_RECORD, sizeof(char)) ) )
				error("InitDerivFacts: could not allocate facts genotype string");
			derivtype[i].genotype = strcpy(derivtype[i].genotype, current->name);
			derivtype[i].ptr = (DataPtr*)malloc(sizeof(DataPtr));
			derivtype[i].ptr->facts = List2Facts(inlist, &b);
			free_Dlist(inlist);
		}
	}
	free(section);
}

void InitDerivSolution()
{
	int i;
	derivfacts = (NArrPtr **)calloc(nalleles, sizeof(NArrPtr *));
	derivsolution = (NArrPtr **)calloc(nalleles, sizeof(NArrPtr *));
	for ( i = 0; i < nalleles; i++) {
		derivfacts[i] = Facts2Solution(facttype[i].ptr->facts);
		derivsolution[i] = Facts2Solution(facttype[i].ptr->facts);
	}
}

void SearchSpaceDelete(SearchSpace*p)
{
	int i, j;
	for ( i = 0; i < defs->ngenes; i++) {
		free(p->Rlim[i]);
		free(p->lambdalim[i]);
		free(p->taulim[i]);
	}
	free(p->Rlim);
	free(p->lambdalim);
	free(p->taulim);
	if ( p->pen_vec ) {
		free(p->pen_vec);
	} else {
		for ( i = 0; i < defs->ngenes; i++) {
			for ( j = 0; j < defs->ngenes; j++)
				free(p->Tlim[(i*defs->ngenes)+j]);
			for ( j = 0; j < defs->mgenes; j++)
				free(p->Mlim[(i*defs->mgenes)+j]);
			for ( j = 0; j < defs->egenes; j++)
				free(p->Elim[(i*defs->egenes)+j]);
			free(p->hlim[i]);
		}
		free(p->Elim);
		free(p->Mlim);
		free(p->Tlim);
		free(p->hlim);
	}
	if ( (defs->diff_schedule == 'A') || (defs->diff_schedule == 'C') )
		free(p->dlim[0]);
	else
		for ( i = 0; i < defs->ngenes; i++)
			free(p->dlim[i]);
	free(p->dlim);
	if ( (defs->mob_schedule == 'A') || (defs->mob_schedule == 'C') )
		free(p->mlim[0]);
	else
		for ( i = 0; i < defs->ngenes; i++)
			free(p->mlim[i]);
	free(p->mlim);
	if ( (defs->mob_schedule == 'A') || (defs->mob_schedule == 'C') )
		free(p->mmlim[0]);
	else
		for ( i = 0; i < defs->ngenes; i++)
			free(p->mmlim[i]);
	free(p->mmlim);
	free(p);
}

int gcmd_get_facts(int gindex, double curr_time, double*v, int n)
{
	const double big_epsilon = BIG_EPSILON;    /* used to recognize new time */
	DataTable    fact_tab;      /* stores a copy of the Facts (from GenoTab) */
	DataPoint    *point;           /* used to extract an element of DataTable */
	int          tindex;                       /* index for facts timepoints */
	int          sindex;                    /* index for Solution timepoints */
	int          vindex;                        /* index for facts datapoint */
	double 	time;
/* extablish a pointer to the appropriate facts section in GenoTab */
	fact_tab = *(facttype[gindex].ptr->facts);
	sindex   = -1;
/* the following loop adds all squared differences for the whole Solution */
	for ( tindex = 0; tindex < fact_tab.size; tindex++) {
		time = fact_tab.record[tindex].time;
		if( fabs(time - curr_time) < big_epsilon ) {
			sindex = tindex;
			break;
		}
	}
	if ( sindex == -1 )
		return sindex;
	for ( vindex = 0; vindex < fact_tab.record[sindex].size; vindex++) {
		point = &(fact_tab.record[sindex].array[vindex]);
		v[point->index] = point->conc;
	}
	return sindex;
}

ScoreTarget*ReadScoreTarget(FILE*fp)
{
	char      namebuf[MAX_RECORD];      /* title of genotype     */
	char      weightbuf[MAX_RECORD];      /* section title of bias section     */
	char      indexbuf[MAX_RECORD];     /* section title of data section     */
	char      rankbuf[MAX_RECORD];       /* section title of bcd section      */
	char      *record;                  /* pointer to current data record    */
	int size;
	ScoreTarget*score_target;
/*** open the data file and locate genotype section ************************/
	fp = FindSection(fp, "scoring_target_def");
	if( !fp )
		error("ReadScoreTarget: cannot locate scoring_target_def");
	if ( !(record=(char *)calloc(MAX_RECORD, sizeof(char))) )
		error("ReadScoreTarget: error allocating record");
	score_target = (ScoreTarget*)malloc(sizeof(ScoreTarget));
	score_target->def = NULL;
	size = 0;
/*** read titles *********************************************************/
	while ( strncmp((record=fgets(record, MAX_RECORD, fp)), "$$", 2)) {
		if ( 4 != sscanf(record, "%s %s %s %s", namebuf, indexbuf, weightbuf, rankbuf) )
			error("ReadScoreTarget: error reading %s", record);
/* we eventually want to get rid of the hard wired genotype identifiers    *
 * and replace them by some kind of mechanism by which we can include any  *
 * genes we want in a scoring or annealing run. Think about this!          */
		score_target->def = (ScoreTargetDef*)realloc( score_target->def, ( size + 1 ) * sizeof(ScoreTargetDef) );
		score_target->def[size].name = (char*)calloc(MAX_RECORD, sizeof(char));
		score_target->def[size].name = strcpy(score_target->def[size].name, namebuf);
		score_target->def[size].weight = atof(weightbuf);
		score_target->def[size].rank = atof(rankbuf);
		score_target->def[size].index = atoi(indexbuf);
		size++;
	}
	free(record);
	score_target->size = size;
	return score_target;
}

ScoreTarget*get_score_target()
{
	return score_target;
}

void InitScoreTarget_bcd(FILE *fp)
{
	int i;
	score_target = ReadScoreTarget(fp);
	for ( i = 0; i < score_target->size; i++ ) {
		if ( !strcmp(score_target->def[i].name, "eval") ) {
			score_target->def[i].f = gcdm_score_score_1;
		} else if ( !strcmp(score_target->def[i].name, "penalty") ) {
			score_target->def[i].f = gcdm_score_penalty_penalty;
		} else if ( !strcmp(score_target->def[i].name, "dfacts") ) {
			init_scoring_subtype_dfacts(fp);
			score_target->def[i].f = gcdm_score_penalty_dfacts;
		} else if ( !strcmp(score_target->def[i].name, "chaos") ) {
			gcdm_score_init_chaos(fp);
			score_target->def[i].f = gcdm_score_penalty_chaos;
		} else if ( !strcmp(score_target->def[i].name, "dlimits") ) {
			score_target->def[i].f = gcdm_score_check_detailed_limits;
		} else if ( !strcmp(score_target->def[i].name, "grad") ) {
			InitZygoteGrad ();
			InitScoring2 ();
			score_target->def[i].f = gcdm_score_grad_score_bcd;
		} else {
			error("InitScoreTarget: wrong name %s", score_target->def[i].name);
		}
	}
}

void InitScoreTarget(FILE *fp)
{
	int i;
	score_target = ReadScoreTarget(fp);
	for ( i = 0; i < score_target->size; i++ ) {
		if ( !strcmp(score_target->def[i].name, "eval") ) {
			score_target->def[i].f = gcdm_score_score_1;
		} else if ( !strcmp(score_target->def[i].name, "penalty") ) {
			score_target->def[i].f = gcdm_score_penalty_penalty;
		} else if ( !strcmp(score_target->def[i].name, "dfacts") ) {
			init_scoring_subtype_dfacts(fp);
			score_target->def[i].f = gcdm_score_penalty_dfacts;
		} else if ( !strcmp(score_target->def[i].name, "chaos") ) {
			gcdm_score_init_chaos(fp);
			score_target->def[i].f = gcdm_score_penalty_chaos;
		} else if ( !strcmp(score_target->def[i].name, "dlimits") ) {
			score_target->def[i].f = gcdm_score_check_detailed_limits;
		} else if ( !strcmp(score_target->def[i].name, "grad") ) {
			InitZygoteGrad ();
			InitScoring2 ();
			score_target->def[i].f = gcdm_score_grad_score;
		} else {
			error("InitScoreTarget: wrong name %s", score_target->def[i].name);
		}
	}
}

double gcdm_score_score_targeted()
{
	int i;
	double value;
	value = 0;
	for ( i = 0; i < score_target->size; i++) {
		value += score_target->def[i].weight * score_target->def[i].f();
	}
	return value;
}

void gcdm_score_calc_g_thing(int ndigits, int mode)
{
	int i, j, k, m;
	char *charid;
	int parm_per_gene = defs->egenes + defs->mgenes + 5;
	int e_parm_extension = defs->ngenes;
	int m_parm_extension = defs->ngenes + defs->egenes;
	int h_parm_extension = defs->ngenes + defs->egenes + defs->mgenes;
	int r_parm_extension = defs->ngenes + defs->egenes + defs->mgenes + 1;
	int d_parm_extension = defs->ngenes + defs->egenes + defs->mgenes + 2;
	int lambda_parm_extension = defs->ngenes + defs->egenes + defs->mgenes + 3;
	int t_parm_extension = defs->ngenes + defs->egenes + defs->mgenes + 4;
	NArrPtr*gsol, *outgut;
	charid = (char*)calloc(MAX_RECORD, sizeof(char));
	m = 0;
	for ( i = 0; i < nalleles; i++) {
		Blastoderm_setThisGenotype(Flies[i]);
		Blastoderm_SolvePsi(Flies[i], stepsize, accuracy, slogptr);
		if ( mode == -1 ) {
			outgut = ConvertAnswer( Flies[i]->Solution, tt[i].ptr->times);
			PrintBlastoderm (stdout, outgut, Flies[i]->genotype_ID, ndigits, defs->ngenes);
			free(outgut);
		} else if ( mode >= 0 ) {
			for ( j = 0; j < defs->ngenes; j++ ) {
				gsol = calcgthing(Flies[i], j * ( defs->ngenes + parm_per_gene ) + r_parm_extension);
				sprintf(charid, "%s_R[%c]", Flies[i]->genotype_ID, defs->gene_ids[j] );
				outgut = ConvertAnswer( gsol, tt[i].ptr->times);
				if ( mode >= 1 ) {
					PrintBlastoderm (stdout, outgut, charid, ndigits, defs->ngenes);
				} else {
					PrintBlastodermUgly (stdout, outgut, charid, ndigits, defs->ngenes, m);
				}
				FreeNArrPtr(gsol);
				free(outgut);
				m++;
			}
			for ( j = 0; j < defs->ngenes; j++ ) {
				for ( k = 0; k < defs->ngenes; k++ ) {
					gsol = calcgthing(Flies[i], j * ( defs->ngenes + parm_per_gene ) + k);
					sprintf(charid, "%s_T[%c<-%c]", Flies[i]->genotype_ID, defs->gene_ids[j], defs->gene_ids[k]);
					outgut = ConvertAnswer( gsol, tt[i].ptr->times);
					if ( mode >= 1 ) {
						PrintBlastoderm (stdout, outgut, charid, ndigits, defs->ngenes);
					} else {
						PrintBlastodermUgly (stdout, outgut, charid, ndigits, defs->ngenes, 1);
					}
					FreeNArrPtr(gsol);
					free(outgut);
					m++;
				}
				for ( k = 0; k < defs->egenes; k++ ) {
					gsol = calcgthing(Flies[i], j * ( defs->ngenes + parm_per_gene ) + e_parm_extension + k);
					sprintf(charid, "%s_E[%c<-%c]", Flies[i]->genotype_ID, defs->gene_ids[j], defs->egene_ids[k]);
					outgut = ConvertAnswer( gsol, tt[i].ptr->times);
					if ( mode >= 1 ) {
						PrintBlastoderm (stdout, outgut, charid, ndigits, defs->ngenes);
					} else {
						PrintBlastodermUgly (stdout, outgut, charid, ndigits, defs->ngenes, 1);
					}
					FreeNArrPtr(gsol);
					free(outgut);
					m++;
				}
/*				for ( k = 0; k < defs->mgenes; k++ ) {
					gsol = calcgthing(Flies[i], j * ( defs->ngenes + parm_per_gene ) + m_parm_extension + k);
					sprintf(charid, "%s_M[%d;%d]", Flies[i]->genotype_ID, j, k);
					outgut = ConvertAnswer( gsol, tt[i].ptr->times);
					if ( mode >= 1 ) {
						PrintBlastoderm (stdout, outgut, charid, ndigits, defs->ngenes);
					} else {
						PrintBlastodermUgly (stdout, outgut, charid, ndigits, defs->ngenes, 1);
					}
					FreeNArrPtr(gsol);
					free(outgut);
					m++;
				}*/
/*				gsol = calcgthing(Flies[i], j * ( defs->ngenes + parm_per_gene ) + h_parm_extension);
				sprintf(charid, "%s_h[%d]", Flies[i]->genotype_ID, j );
				outgut = ConvertAnswer( gsol, tt[i].ptr->times);
				if ( mode >= 1 ) {
					PrintBlastoderm (stdout, outgut, charid, ndigits, defs->ngenes);
				} else {
					PrintBlastodermUgly (stdout, outgut, charid, ndigits, defs->ngenes, 1);
				}
				FreeNArrPtr(gsol);
				free(outgut);
				m++;*/
			}
			for ( j = 0; j < defs->ngenes; j++ ) {
				gsol = calcgthing(Flies[i], j * ( defs->ngenes + parm_per_gene ) + d_parm_extension);
				sprintf(charid, "%s_d[%c]", Flies[i]->genotype_ID, defs->gene_ids[j] );
				outgut = ConvertAnswer( gsol, tt[i].ptr->times);
				if ( mode >= 1 ) {
					PrintBlastoderm (stdout, outgut, charid, ndigits, defs->ngenes);
				} else {
					PrintBlastodermUgly (stdout, outgut, charid, ndigits, defs->ngenes, 1);
				}
				FreeNArrPtr(gsol);
				free(outgut);
				m++;
			}
			for ( j = 0; j < defs->ngenes; j++ ) {
				gsol = calcgthing(Flies[i], j * ( defs->ngenes + parm_per_gene ) + lambda_parm_extension);
				sprintf(charid, "%s_lambda[%c]", Flies[i]->genotype_ID, defs->gene_ids[j] );
				outgut = ConvertAnswer( gsol, tt[i].ptr->times);
				if ( mode >= 1 ) {
					PrintBlastoderm (stdout, outgut, charid, ndigits, defs->ngenes);
				} else {
					PrintBlastodermUgly (stdout, outgut, charid, ndigits, defs->ngenes, 1);
				}
				FreeNArrPtr(gsol);
				free(outgut);
				m++;
			}
		}
	}
}

void gcdm_score_calc_g_num_bcd(int ndigits, int mode, double derstep)
{
	int i, j, k, m;
	char *charid;
	NArrPtr*gsol, *outgut;
	double parderstep, orig;
	charid = (char*)calloc(MAX_RECORD, sizeof(char));
	m = 0;
	for ( i = 0; i < nalleles; i++) {
		gcdm_blastoderm_solve_conc_with_hints(Flies[i], stepsize, accuracy, slogptr, Flies[i]->dosage, Flies[i]->zero_bias, Flies[i]->rhs);
		gsol = gcdm_clone_answer( Flies[i]->Solution, tt[i].ptr->times);
		for ( j = 0; j < defs->ngenes; j++ ) {
			parderstep = derstep * fabs(parm->R[j]);
			orig = parm->R[j];
			parm->R[j] += parderstep;
			sprintf(charid, "%s_R[%c]", Flies[i]->genotype_ID, defs->gene_ids[j] );
			gcdm_blastoderm_solve_conc_with_hints(Flies[i], stepsize, accuracy, slogptr, Flies[i]->dosage, Flies[i]->zero_bias, Flies[i]->rhs);
			outgut = gcdm_combine_answer( 1.0 / parderstep, gsol, -1.0 / parderstep, Flies[i]->Solution, tt[i].ptr->times);
			if ( mode >= 1 ) {
				PrintBlastoderm (stdout, outgut, charid, ndigits, defs->ngenes);
			} else {
				PrintBlastodermUgly (stdout, outgut, charid, ndigits, defs->ngenes, m);
			}
			parm->R[j] = orig;
			FreeNArrPtr(outgut);
			m++;
		}
		for ( j = 0; j < defs->ngenes; j++ ) {
			for ( k = 0; k < defs->ngenes; k++ ) {
				parderstep = derstep * fabs(parm->T[j * defs->ngenes + k]);
				orig = parm->T[j * defs->ngenes + k];
				parm->T[j * defs->ngenes + k] += parderstep;
				sprintf(charid, "%s_T[%c<-%c]", Flies[i]->genotype_ID, defs->gene_ids[j], defs->gene_ids[k]);;
				gcdm_blastoderm_solve_conc_with_hints(Flies[i], stepsize, accuracy, slogptr, Flies[i]->dosage, Flies[i]->zero_bias, Flies[i]->rhs);
				outgut = gcdm_combine_answer( 1.0 / parderstep, gsol, -1.0 / parderstep, Flies[i]->Solution, tt[i].ptr->times);
				if ( mode >= 1 ) {
					PrintBlastoderm (stdout, outgut, charid, ndigits, defs->ngenes);
				} else {
					PrintBlastodermUgly (stdout, outgut, charid, ndigits, defs->ngenes, 1);
				}
				parm->T[j * defs->ngenes + k] = orig;
				FreeNArrPtr(outgut);
				m++;
			}
			for ( k = 0; k < defs->egenes; k++ ) {
				parderstep = derstep * fabs(parm->E[j * defs->egenes + k]);
				orig = parm->E[j * defs->egenes + k];
				parm->E[j * defs->egenes + k] += parderstep;
				sprintf(charid, "%s_E[%c<-%c]", Flies[i]->genotype_ID, defs->gene_ids[j], defs->egene_ids[k]);;
				gcdm_blastoderm_solve_conc_with_hints(Flies[i], stepsize, accuracy, slogptr, Flies[i]->dosage, Flies[i]->zero_bias, Flies[i]->rhs);
				outgut = gcdm_combine_answer( 1.0 / parderstep, gsol, -1.0 / parderstep, Flies[i]->Solution, tt[i].ptr->times);
				if ( mode >= 1 ) {
					PrintBlastoderm (stdout, outgut, charid, ndigits, defs->ngenes);
				} else {
					PrintBlastodermUgly (stdout, outgut, charid, ndigits, defs->ngenes, 1);
				}
				parm->E[j * defs->egenes + k] = orig;
				FreeNArrPtr(outgut);
				m++;
			}
/*				for ( k = 0; k < defs->mgenes; k++ ) {
					gsol = calcgthing(Flies[i], j * ( defs->ngenes + parm_per_gene ) + m_parm_extension + k);
					sprintf(charid, "%s_M[%d;%d]", Flies[i]->genotype_ID, j, k);
					outgut = ConvertAnswer( gsol, tt[i].ptr->times);
					if ( mode >= 1 ) {
						PrintBlastoderm (stdout, outgut, charid, ndigits, defs->ngenes);
					} else {
						PrintBlastodermUgly (stdout, outgut, charid, ndigits, defs->ngenes, 1);
					}
					FreeNArrPtr(gsol);
					free(outgut);
					m++;
				}*/
/*				gsol = calcgthing(Flies[i], j * ( defs->ngenes + parm_per_gene ) + h_parm_extension);
				sprintf(charid, "%s_h[%d]", Flies[i]->genotype_ID, j );
				outgut = ConvertAnswer( gsol, tt[i].ptr->times);
				if ( mode >= 1 ) {
					PrintBlastoderm (stdout, outgut, charid, ndigits, defs->ngenes);
				} else {
					PrintBlastodermUgly (stdout, outgut, charid, ndigits, defs->ngenes, 1);
				}
				FreeNArrPtr(gsol);
				free(outgut);
				m++;*/
		}
		for ( j = 0; j < defs->ngenes; j++ ) {
			parderstep = derstep * fabs(parm->d[j]);
			orig = parm->d[j];
			parm->d[j] += parderstep;
			sprintf(charid, "%s_d[%c]", Flies[i]->genotype_ID, defs->gene_ids[j] );
			gcdm_blastoderm_solve_conc_with_hints(Flies[i], stepsize, accuracy, slogptr, Flies[i]->dosage, Flies[i]->zero_bias, Flies[i]->rhs);
			outgut = gcdm_combine_answer( 1.0 / parderstep, gsol, -1.0 / parderstep, Flies[i]->Solution, tt[i].ptr->times);
			if ( mode >= 1 ) {
				PrintBlastoderm (stdout, outgut, charid, ndigits, defs->ngenes);
			} else {
				PrintBlastodermUgly (stdout, outgut, charid, ndigits, defs->ngenes, m);
			}
			parm->d[j] = orig;
			FreeNArrPtr(outgut);
			m++;
		}
		for ( j = 0; j < defs->ngenes; j++ ) {
			parderstep = derstep * fabs(parm->lambda[j]);
			orig = parm->lambda[j];
			parm->lambda[j] += parderstep;
			sprintf(charid, "%s_lambda[%c]", Flies[i]->genotype_ID, defs->gene_ids[j] );
			gcdm_blastoderm_solve_conc_with_hints(Flies[i], stepsize, accuracy, slogptr, Flies[i]->dosage, Flies[i]->zero_bias, Flies[i]->rhs);
			outgut = gcdm_combine_answer( 1.0 / parderstep, gsol, -1.0 / parderstep, Flies[i]->Solution, tt[i].ptr->times);
			if ( mode >= 1 ) {
				PrintBlastoderm (stdout, outgut, charid, ndigits, defs->ngenes);
			} else {
				PrintBlastodermUgly (stdout, outgut, charid, ndigits, defs->ngenes, m);
			}
			parm->lambda[j] = orig;
			FreeNArrPtr(outgut);
			m++;
		}
		FreeNArrPtr(gsol);
	}
}

void gcdm_score_calc_g_num(int ndigits, int mode, double derstep)
{
	int i, j, k, m;
	char *charid;
	NArrPtr*gsol, *outgut;
	double parderstep, orig;
	charid = (char*)calloc(MAX_RECORD, sizeof(char));
	m = 0;
	for ( i = 0; i < nalleles; i++) {
		gcdm_blastoderm_solve_conc_with_hints(Flies[i], stepsize, accuracy, slogptr, Flies[i]->dosage, Flies[i]->zero_bias, Flies[i]->rhs);
		gsol = gcdm_clone_answer( Flies[i]->Solution, tt[i].ptr->times);
		int ndims = defs->ngenes / 2;
		if ( i == 0 && mode == 0 ) {
			PrintBlastodermUgly (stdout, gsol, charid, ndigits, defs->ngenes, 2);
		}
		for ( j = 0; j < defs->ngenes; j++ ) {
			parderstep = derstep * fabs(parm->R[j]);
			orig = parm->R[j];
			parm->R[j] += parderstep;
			if ( mode == 0 ) {
				sprintf(charid, "'s[%c]'", defs->gene_ids[j] );
			}
			gcdm_blastoderm_solve_conc_with_hints(Flies[i], stepsize, accuracy, slogptr, Flies[i]->dosage, Flies[i]->zero_bias, Flies[i]->rhs);
			outgut = gcdm_combine_answer( 1.0 / parderstep, gsol, -1.0 / parderstep, Flies[i]->Solution, tt[i].ptr->times);
			if ( mode > 1 ) {
				PrintBlastoderm (stdout, outgut, charid, ndigits, ndims);
			} else {
				PrintBlastodermUgly (stdout, outgut, charid, ndigits, defs->ngenes, mode);
			}
			parm->R[j] = orig;
			FreeNArrPtr(outgut);
			m++;
		}
		for ( j = 0; j < ndims; j++ ) {
			for ( k = 0; k < ndims; k++ ) {
				parderstep = derstep * fabs(parm->T[j * defs->ngenes + k]);
				orig = parm->T[j * defs->ngenes + k];
				parm->T[j * defs->ngenes + k] += parderstep;
				if ( mode == 0 ) {
					sprintf(charid, "'%c%c'", defs->gene_ids[j], defs->gene_ids[k]);
				}
				gcdm_blastoderm_solve_conc_with_hints(Flies[i], stepsize, accuracy, slogptr, Flies[i]->dosage, Flies[i]->zero_bias, Flies[i]->rhs);
				outgut = gcdm_combine_answer( 1.0 / parderstep, gsol, -1.0 / parderstep, Flies[i]->Solution, tt[i].ptr->times);
				if ( mode > 1 ) {
					PrintBlastoderm (stdout, outgut, charid, ndigits, ndims);
				} else {
					PrintBlastodermUgly (stdout, outgut, charid, ndigits, defs->ngenes, mode);
				}
				parm->T[j * defs->ngenes + k] = orig;
				FreeNArrPtr(outgut);
				m++;
			}
		}
		j = 0;
		k = ndims + 1;
/*		for ( j = 0; j < ndims; j++ ) {
			for ( k = 0; k < ndims; k++ ) {*/
				parderstep = derstep * fabs(parm->T[j * defs->ngenes + k]);
				orig = parm->T[j * defs->ngenes + k];
				parm->T[j * defs->ngenes + k] += parderstep;
				if ( mode == 0 ) {
					sprintf(charid, "'%c%c'", defs->gene_ids[j], defs->gene_ids[k]);
				}
				gcdm_blastoderm_solve_conc_with_hints(Flies[i], stepsize, accuracy, slogptr, Flies[i]->dosage, Flies[i]->zero_bias, Flies[i]->rhs);
				outgut = gcdm_combine_answer( 1.0 / parderstep, gsol, -1.0 / parderstep, Flies[i]->Solution, tt[i].ptr->times);
				if ( mode > 1 ) {
					PrintBlastoderm (stdout, outgut, charid, ndigits, ndims);
				} else {
					PrintBlastodermUgly (stdout, outgut, charid, ndigits, defs->ngenes, mode);
				}
				parm->T[j * defs->ngenes + k] = orig;
				FreeNArrPtr(outgut);
				m++;
/*			}
		}*/
		j = 1;
		k = ndims;
/*		for ( j = 0; j < ndims; j++ ) {
			for ( k = 0; k < ndims; k++ ) {*/
				parderstep = derstep * fabs(parm->T[j * defs->ngenes + k]);
				orig = parm->T[j * defs->ngenes + k];
				parm->T[j * defs->ngenes + k] += parderstep;
				if ( mode == 0 ) {
					sprintf(charid, "'%c%c'", defs->gene_ids[j], defs->gene_ids[k]);
				}
				gcdm_blastoderm_solve_conc_with_hints(Flies[i], stepsize, accuracy, slogptr, Flies[i]->dosage, Flies[i]->zero_bias, Flies[i]->rhs);
				outgut = gcdm_combine_answer( 1.0 / parderstep, gsol, -1.0 / parderstep, Flies[i]->Solution, tt[i].ptr->times);
				if ( mode > 1 ) {
					PrintBlastoderm (stdout, outgut, charid, ndigits, ndims);
				} else {
					PrintBlastodermUgly (stdout, outgut, charid, ndigits, defs->ngenes, mode);
				}
				parm->T[j * defs->ngenes + k] = orig;
				FreeNArrPtr(outgut);
				m++;
/*			}
		}*/
		for ( j = 0; j < ndims; j++ ) {
			for ( k = 0; k < ndims; k++ ) {
				parderstep = derstep * fabs(parm->E[j * defs->egenes + k]);
				orig = parm->E[j * defs->egenes + k];
				parm->E[j * defs->egenes + k] += parderstep;
				if ( mode == 0 ) {
					sprintf(charid, "'%c%c'", defs->gene_ids[j], defs->egene_ids[k]);
				}
				gcdm_blastoderm_solve_conc_with_hints(Flies[i], stepsize, accuracy, slogptr, Flies[i]->dosage, Flies[i]->zero_bias, Flies[i]->rhs);
				outgut = gcdm_combine_answer( 1.0 / parderstep, gsol, -1.0 / parderstep, Flies[i]->Solution, tt[i].ptr->times);
				if ( mode > 1 ) {
					PrintBlastoderm (stdout, outgut, charid, ndigits, ndims);
				} else {
					PrintBlastodermUgly (stdout, outgut, charid, ndigits, defs->ngenes, mode);
				}
				parm->E[j * defs->egenes + k] = orig;
				FreeNArrPtr(outgut);
				m++;
			}
		}
		int hen = 0;
		for ( j = 0; j < defs->ngenes; j++ ) {
			for ( k = 0; k < defs->ncouples; k++ ) {
				parderstep = derstep * fabs(parm->P[j * defs->ncouples + k]);
				orig = parm->P[j * defs->ncouples + k];
				parm->P[j * defs->ncouples + k] += parderstep;
				if ( mode == 0 ) {
					if ( hen < ndims ) {
						sprintf(charid, "'K[%c]'", defs->gene_ids[hen]);
					} else if ( hen < 2 * ndims ) {
						sprintf(charid, "'K[%c]'", defs->egene_ids[hen - ndims]);
					} else if ( hen < 3 * ndims ) {
						sprintf(charid, "'W[%c]'", defs->gene_ids[hen - 2 * ndims]);
					} else {
						sprintf(charid, "'W[%c]'", defs->egene_ids[hen - 3 * ndims]);
					}
				}
				gcdm_blastoderm_solve_conc_with_hints(Flies[i], stepsize, accuracy, slogptr, Flies[i]->dosage, Flies[i]->zero_bias, Flies[i]->rhs);
				outgut = gcdm_combine_answer( 1.0 / parderstep, gsol, -1.0 / parderstep, Flies[i]->Solution, tt[i].ptr->times);
				if ( mode > 1 ) {
					PrintBlastoderm (stdout, outgut, charid, ndigits, ndims);
				} else {
					PrintBlastodermUgly (stdout, outgut, charid, ndigits, defs->ngenes, mode);
				}
				parm->P[j * defs->ncouples + k] = orig;
				FreeNArrPtr(outgut);
				m++;
				hen++;
			}
		}
		j = 0;
/*		for ( j = 0; j < ndims; j++ ) {*/
			parderstep = derstep * fabs(parm->h[j]);
			orig = parm->h[j];
			parm->h[j] += parderstep;
			if ( mode == 0 ) {
				sprintf(charid, "'h[%c]'", defs->gene_ids[j] );
			}
			gcdm_blastoderm_solve_conc_with_hints(Flies[i], stepsize, accuracy, slogptr, Flies[i]->dosage, Flies[i]->zero_bias, Flies[i]->rhs);
			outgut = gcdm_combine_answer( 1.0 / parderstep, gsol, -1.0 / parderstep, Flies[i]->Solution, tt[i].ptr->times);
			if ( mode > 1 ) {
				PrintBlastoderm (stdout, outgut, charid, ndigits, ndims);
			} else {
				PrintBlastodermUgly (stdout, outgut, charid, ndigits, defs->ngenes, mode);
			}
			parm->h[j] += orig;
			FreeNArrPtr(outgut);
			m++;
/*		}*/
		j = 4;
/*		for ( j = 0; j < ndims; j++ ) {*/
			parderstep = derstep * fabs(parm->h[j]);
			orig = parm->h[j];
			parm->h[j] += parderstep;
			if ( mode == 0 ) {
				sprintf(charid, "'e[%c]'", defs->gene_ids[j] );
			}
			gcdm_blastoderm_solve_conc_with_hints(Flies[i], stepsize, accuracy, slogptr, Flies[i]->dosage, Flies[i]->zero_bias, Flies[i]->rhs);
			outgut = gcdm_combine_answer( 1.0 / parderstep, gsol, -1.0 / parderstep, Flies[i]->Solution, tt[i].ptr->times);
			if ( mode > 1 ) {
				PrintBlastoderm (stdout, outgut, charid, ndigits, ndims);
			} else {
				PrintBlastodermUgly (stdout, outgut, charid, ndigits, defs->ngenes, mode);
			}
			parm->h[j] += orig;
			FreeNArrPtr(outgut);
			m++;
/*		}*/
		j = 5;
/*		for ( j = 0; j < ndims; j++ ) {*/
			parderstep = derstep * fabs(parm->h[j]);
			orig = parm->h[j];
			parm->h[j] += parderstep;
			if ( mode == 0 ) {
				sprintf(charid, "'e[%c]'", defs->gene_ids[j] );
			}
			gcdm_blastoderm_solve_conc_with_hints(Flies[i], stepsize, accuracy, slogptr, Flies[i]->dosage, Flies[i]->zero_bias, Flies[i]->rhs);
			outgut = gcdm_combine_answer( 1.0 / parderstep, gsol, -1.0 / parderstep, Flies[i]->Solution, tt[i].ptr->times);
			if ( mode > 1 ) {
				PrintBlastoderm (stdout, outgut, charid, ndigits, ndims);
			} else {
				PrintBlastodermUgly (stdout, outgut, charid, ndigits, defs->ngenes, mode);
			}
			parm->h[j] += orig;
			FreeNArrPtr(outgut);
			m++;
/*		}*/
/*		for ( j = 0; j < defs->ngenes; j++ ) {
			parderstep = derstep * fabs(parm->d[j]);
			orig = parm->d[j];
			parm->d[j] += parderstep;
			sprintf(charid, "%s_d[%c]", Flies[i]->genotype_ID, defs->gene_ids[j] );
			gcdm_blastoderm_solve_conc_with_hints(Flies[i], stepsize, accuracy, slogptr, Flies[i]->dosage, Flies[i]->zero_bias, Flies[i]->rhs);
			outgut = gcdm_combine_answer( 1.0 / parderstep, gsol, -1.0 / parderstep, Flies[i]->Solution, tt[i].ptr->times);
			if ( mode >= 1 ) {
				PrintBlastoderm (stdout, outgut, charid, ndigits, defs->ngenes);
			} else {
				PrintBlastodermUgly (stdout, outgut, charid, ndigits, defs->ngenes, m);
			}
			parm->d[j] = orig;
			FreeNArrPtr(outgut);
			m++;
		}*/
		for ( j = 0; j < defs->ngenes; j++ ) {
			parderstep = derstep * fabs(parm->lambda[j]);
			orig = parm->lambda[j];
			parm->lambda[j] += parderstep;
			if ( mode == 0 ) {
				sprintf(charid, "'l[%c]'", defs->gene_ids[j] );
			}
			gcdm_blastoderm_solve_conc_with_hints(Flies[i], stepsize, accuracy, slogptr, Flies[i]->dosage, Flies[i]->zero_bias, Flies[i]->rhs);
			outgut = gcdm_combine_answer( 1.0 / parderstep, gsol, -1.0 / parderstep, Flies[i]->Solution, tt[i].ptr->times);
			if ( mode > 1 ) {
				PrintBlastoderm (stdout, outgut, charid, ndigits, ndims);
			} else {
				PrintBlastodermUgly (stdout, outgut, charid, ndigits, defs->ngenes, mode);
			}
			parm->lambda[j] = orig;
			FreeNArrPtr(outgut);
			m++;
		}
		for ( j = 0; j < ndims; j++ ) {
			parderstep = derstep * fabs(parm->tau[j]);
			orig = parm->tau[j];
			parm->tau[j] += parderstep;
			if ( mode == 0 ) {
				sprintf(charid, "'t[%c]'", defs->gene_ids[j] );
			}
			gcdm_blastoderm_solve_conc_with_hints(Flies[i], stepsize, accuracy, slogptr, Flies[i]->dosage, Flies[i]->zero_bias, Flies[i]->rhs);
			outgut = gcdm_combine_answer( 1.0 / parderstep, gsol, -1.0 / parderstep, Flies[i]->Solution, tt[i].ptr->times);
			if ( mode > 1 ) {
				PrintBlastoderm (stdout, outgut, charid, ndigits, ndims);
			} else {
				PrintBlastodermUgly (stdout, outgut, charid, ndigits, defs->ngenes, mode);
			}
			parm->tau[j] = orig;
			FreeNArrPtr(outgut);
			m++;
		}
		j = 0;
/*		for ( j = 0; j < ndims; j++ ) {*/
			parderstep = derstep * fabs(parm->m[j]);
			orig = parm->m[j];
			parm->m[j] += parderstep;
			if ( mode == 0 ) {
				sprintf(charid, "'r[%c]'", defs->gene_ids[j] );
			}
			gcdm_blastoderm_solve_conc_with_hints(Flies[i], stepsize, accuracy, slogptr, Flies[i]->dosage, Flies[i]->zero_bias, Flies[i]->rhs);
			outgut = gcdm_combine_answer( 1.0 / parderstep, gsol, -1.0 / parderstep, Flies[i]->Solution, tt[i].ptr->times);
			if ( mode > 1 ) {
				PrintBlastoderm (stdout, outgut, charid, ndigits, ndims);
			} else {
				PrintBlastodermUgly (stdout, outgut, charid, ndigits, defs->ngenes, mode);
			}
			parm->m[j] = orig;
			FreeNArrPtr(outgut);
			m++;
/*		}*/
		j = 0;
/*		for ( j = 0; j < defs->ngenes / 2; j++ ) {*/
			parderstep = derstep * fabs(parm->mm[j]);
			orig = parm->mm[j];
			parm->mm[j] += parderstep;
			if ( mode == 0 ) {
				sprintf(charid, "'n[%c]'", defs->gene_ids[j] );
			}
			gcdm_blastoderm_solve_conc_with_hints(Flies[i], stepsize, accuracy, slogptr, Flies[i]->dosage, Flies[i]->zero_bias, Flies[i]->rhs);
			outgut = gcdm_combine_answer( 1.0 / parderstep, gsol, -1.0 / parderstep, Flies[i]->Solution, tt[i].ptr->times);
			if ( mode > 1 ) {
				PrintBlastoderm (stdout, outgut, charid, ndigits, defs->ngenes);
			} else {
				PrintBlastodermUgly (stdout, outgut, charid, ndigits, defs->ngenes, mode);
			}
			parm->mm[j] = orig;
			FreeNArrPtr(outgut);
			m++;
/*		}*/
		FreeNArrPtr(gsol);
	}
}

#ifdef LIBSVM
	double gcdm_score_libsvm(struct svm_model*model, double *prob_estimates)
	{
			struct svm_node *x;
			int max_nr_attr = 64;
			double predict_label;
//			x = (struct svm_node *) realloc(x, max_nr_attr * sizeof(struct svm_node));
			NArrPtr *outtab0, *outtab5;
			DArrPtr *tt;
			tt = ReadTimes(unfold_time_file);
			outtab0 = ConvertAnswer(Flies[0]->Solution, tt);
			outtab5 = ConvertAnswer(Flies[5]->Solution, tt);
			x = ConvertAnswerToLibSVM(outtab0, outtab5, &max_nr_attr);
			predict_label = svm_predict_probability(model, x, prob_estimates);
			return(predict_label);
	}
#elif FANN
	fann_type *gcdm_score_fann(struct fann *ann)
	{
			fann_type *x;
			fann_type *predict_label;
/*			struct 	fann_error 	errdat;*/
			NArrPtr *outtab0, *outtab5;
			DArrPtr *tt;
			tt = ReadTimes(unfold_time_file);
			outtab0 = ConvertAnswer(Flies[0]->Solution, tt);
			outtab5 = ConvertAnswer(Flies[nalleles - 1]->Solution, tt);
			x = ConvertAnswerToFANN(outtab0, outtab5);
			predict_label = fann_run(ann, x);
/*			printf("%d\n", (int)fann_get_errno(&errdat));
			printf("%d\n", (int)FANN_E_CANT_ALLOCATE_MEM);
			printf("%d\n", (int)FANN_E_CANT_TRAIN_ACTIVATION);
			printf("%d\n", (int)FANN_E_CANT_USE_ACTIVATION);
			printf("%d\n", (int)FANN_E_TRAIN_DATA_MISMATCH);
			printf("%d\n", (int)FANN_E_CANT_USE_TRAIN_ALG);
			printf("%d\n", (int)FANN_E_TRAIN_DATA_SUBSET);
			printf("%d\n", (int)FANN_E_INDEX_OUT_OF_BOUND);
			printf("%d\n", (int)FANN_E_SCALE_NOT_PRESENT);
			printf("%d\n", (int)FANN_E_INPUT_NO_MATCH);
			printf("%d\n", (int)FANN_E_OUTPUT_NO_MATCH);
			printf("%d\n", (int)FANN_E_WRONG_PARAMETERS_FOR_CREATE);*/
			return(predict_label);
	}
#endif
