/***************************************************************************
 *            zygotic.h
 *
 *  Wed May 11 13:54:38 2005
 *  Copyright  2005  $USER
 *  kozlov@spbcas.ru
 ****************************************************************************/

/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef _ZYGOTIC_H
#define _ZYGOTIC_H

#ifdef __cplusplus
extern "C"
{
#endif

/*****************************************************************
 *                                                               *
 *   zygotic.h                                                   *
 *                                                               *
 *****************************************************************
 *                                                               *
 *   written by JR, modified by Yoginho and Manu                 *
 *                                                               *
 *****************************************************************
 *                                                               *
 * This file is for functions that deal with the right hand side *
 * of the ODEs. We have an initializing function called          *
 * InitZygote that reads parameters, defs data and search space  *
 * limits from a data file and installs a couple of things like  *
 * the solver and temporary arrays for the dvdt function.        *
 * Then we have dvdt_orig, which is the inner loop of the model. *
 * It propagates the equations from one time point to another    *
 * and gets called whenever Blastoderm is in PROPAGATE mode.     *
 * Last but not least, we have a few mutator functions in this   *
 * file. They change the local lparm EqParm struct.              *
 *                                                               *
 *****************************************************************/
/* this def needed for func. defs that refer to (* FILE) */
#include <stdio.h>
#ifndef MAX_RECORD
#define MAX_RECORD            4096   /* max. length of lines read from file */
#endif
/* The following defines the maximum float precision that is supported by  */
/* the code.                                                               */
#define MAX_PRECISION          16
#include <maternal.h>
/*** CONSTANTS *************************************************************/
/* these are the propagation rules for dvdt_orig */
#define INTERPHASE              0
#define MITOSIS                 1

/*** AN ENUM ***************************************************************/

/* This is the g(u)-function enum which describes the different types of   *
 * g(u) functions we can use in derivative functions                       */

typedef enum FGFunc {
	He,
	Sqrt,
	Tanh,
	Exp,
	Hvs,
	Lin
} FGFunc;

/*** A GLOBAL **************************************************************/

FGFunc gofu;                            /* the g(u) function we're using */

/*** STRUCTS ***************************************************************
 *   The following two structs are newstyle structs that define the pro-   *
 *   blem at hand (TheProblem) and hold the equation parameters (EqParms). *
 ***************************************************************************/

typedef struct EqParms {
	double *R;           /* strength of each promoter--always >= 0. */
	double *T;           /* the genetic interconnect matrix */
	double *E;           /* the external input regulatory matrix */
	double *M;          /* the maternal (constant in time) input regulatory matrix */
	double *P;          /* the couples input regulatory matrix */
	double *h;           /* reg. coeff. for generic TFs on synthesis of gene */
	double *d;           /* spatial interaction at gastrulation--always >= 0. */
	double *lambda; /*protein half lives--always >= 0. */
	double *tau;        /* delay times for the proteins */
	double *m;        /* delay times for the proteins */
	double *mm;        /* delay times for the proteins */
} EqParms;

/*added by MacKoel*/

typedef void (*ConcFuncType)(double, double*);

ConcFuncType CF, CFP;

/*** FUNCTION PROTOTYPES ***************************************************/

/* Initialization Functions */

/*** InitZygote: makes pm and pd visible to all functions in zygotic.c and *
 *               reads EqParms and TheProblem. It then initializes bicoid  *
 *               and bias (including BTimes) in maternal.c. Lastly, it     *
 *               allocates memory for structures used by the derivative    *
 *               function DvdtOrig that are static to zygotic.c.           *
 ***************************************************************************/

void InitZygote(FILE *fp, char* parm_section);

/* Cleanup functions */

/*** FreeZygote: frees memory for D, vinput, bot2 and bot arrays ***********
 ***************************************************************************/

void FreeZygote(void);

/*** FreeMutant: frees mutated parameter struct ****************************
 ***************************************************************************/
void FreeMutant(void);
/* Derivative Function(s) */
/* Derivative functions calculate the derivatives for the solver. **********/
/*** DvdtOrig: the original derivative function; implements the equations **
 *             as published in Reinitz & Sharp (1995), Mech Dev 49, 133-58 *
 *             plus different g(u) functions as used by Yousong Wang in    *
 *             spring 2002.                                                *
 ***************************************************************************/
void DvdtDelay(double *v, double **vd, double t, double *vdot, int n);

void DvdtOrig(double *v, double t, double *vdot, int n);

void DvwdtOrig(double *v, double t, double *vdot, int n);

void DdvwdtOrig(double *v, double t, double *vdot, int n);

void DddvwdtOrig(double *v, double t, double *vdot, int n);

void DddvwdtDual(double *v, double t, double *vdot, int n);

void DddwwdtOrig(double *v, double t, double *vdot, int n);

void DddwwdtGuts(double *v, double t, double *vdot, int n);

void DddwwdtFocus(double *v, double t, double *vdot, int n);

void set_use_external_inputs_only(int u);

#ifdef XTRA
void gcdm_init_seq(FILE*fp);
#endif

/* Jacobian Function(s) */
/* Calculate the Jacobian for a given model at a given time; these funcs ***
 * are used by certain implicit solvers                                    */
/*** JacobnOrig: Jacobian function for the DvdtOrig model; calculates the **
 *               Jacobian matrix (matrix of partial derivatives) for the   *
 *               equations at a give time t; input concentrations come in  *
 *               v (of size n), the Jacobian is returned in jac; note that *
 *               all dfdt's are zero in our case since our equations are   *
 *               autonomous (i.e. have no explicit t in them)              *
 ***************************************************************************/
/*void JacobnOrig(double t, double *v, double *dfdt, double **jac, int n);*/
/*** GUTS FUNCTIONS ********************************************************/
/*** CalcGuts: calculates guts for genotpye 'gtype' using unfold output in *
 *             'table' and the guts string 'gutsdefs'; it returns the num- *
 *             ber of columns we'll need to print and the guts table in    *
 *             'gtable'                                                    *
 ***************************************************************************/
//int CalcGuts(char *gtype, NArrPtr table, NArrPtr table2, NArrPtr **gtable, char *gutsdefs);
NArrPtr *CalcGuts(char *gtype, NArrPtr*table, NArrPtr *table2, int *numguts, char *gutsdefs);
/*** CalcRhs: calculates the components of the right-hand-side (RHS) of ****
 *            the equatiion which make up guts (e.g. regulatory contribu-  *
 *            tions of specific genes, diffusion or protein decay); this   *
 *            function makes use of the derivative function and calculates *
 *            everything outside g(u) in reverse, i.e. starting from the   *
 *            derivative and calculating the desired gut properties back-  *
 *            wards from there; everything within g(u) is simply recon-    *
 *            structed from concentrations and parameters                  *
 ***************************************************************************/
void CalcRhs(double *v, double *w, double t, double *guts, int n, int gn, int numguts, int which, unsigned long *gutcomps);
/*** ParseString: parses a line of the $gutsdefs section (dataformatX.X ****
 *                has the details); this function then returns two things: *
 *                1) the number of 'words' separated by whitespace in that *
 *                   string is the function's return value                 *
 *                2) the 'words' themselves are returned in arginp         *
 ***************************************************************************/
int ParseString(char *v, char **arginp);
/*** GetGutsComps: takes a geneID string and an array of ID strings; then **
 *                 returns an array of long ints with bit flags set that   *
 *                 tell CalcRhs() what to calculate for each column of     *
 *                 guts output; this function also returns a pointer to    *
 *                 the current gene in the gene ID string                  *
 ***************************************************************************/
char *GetGutsComps(char *geneidstring, char **specsin, unsigned long *specsout);
/* Mutator functions */

/*** Mutate: calls mutator functions according to genotype string **********
 ***************************************************************************/
void Mutate(char *g_type);
/*** T_Mutate: mutates genes by setting all their T matrix entries *********
 *             to zero. Used to simulate mutants that express a            *
 *             non-functional protein.                                     *
 ***************************************************************************/
void T_Mutate(int g_type);
void E_Mutate(int gene);
void D_Mutate(int gene);
/*** R_Mutate: mutates genes by setting their promotor strength R **********
 *             to zero, so there will be no transcription at all           *
 *             anymore. Used to simulate mutants that don't pro-           *
 *             duce any protein anymore.                                   *
 ***************************************************************************/
void R_Mutate(int g_type);
/*** RT_Mutate: mutates gene by setting both promoter strength R ***********
 *              and T matrix entries to zero. Useful, if you want          *
 *              to be really sure that there is NO protein pro-            *
 *              duction and that maternal contribution also don't          *
 *              contribute anything to gene interactions.                  *
 ***************************************************************************/
void RT_Mutate(int g_type);
/*** CopyParm: copies all the parameters into the lparm struct *************
 ***************************************************************************/
EqParms* CopyParm(EqParms* orig_parm);
/* A function that sets static stuff in zygotic.c */
/*** SetRule: sets the static variable rule to MITOSIS or INTERPHASE *******
 ***************************************************************************/
void SetRule(int r);
/* A function that return static stuff from zygotic.c */
/*** GetParameters: returns the parm struct to the caller; note that this **
 *                  function returns the ORIGINAL PARAMETERS as they are   *
 *                  in the data file and NOT THE MUTATED ONES; this is im- *
 *                  portant to prevent limit violations in Score()         *
 ***************************************************************************/
EqParms *GetParameters(void);
/*** GetMutParameters: same as above but returns the mutated copy of the ***
 *                     parameter struct; important for writing guts        *
 ***************************************************************************/
EqParms *GetMutParameters(void);
/* A function that reads EqParms from the data file */
/*** ReadParamters: reads the parameters for a simulation run from the *****
 *                  eqparms or input section of the data file as indicated *
 *                  by the section_title argument and does the conversion  *
 *                  of protein half lives into lambda parameters.          *
 ***************************************************************************/

EqParms*ReadParameters(FILE *fp, char *section_title);
/* Functions that write or print EqParms */
/*** WriteParameters: writes the out_parm struct into a new section in the *
 *                    file specified by filename; the new 'eqparms' sec-   *
 *                    tion is inserted right after the 'input' section;    *
 *                    to achieve this, we need to write to a temporary     *
 *                    file which is then renamed to the output file name   *
 *              NOTE: lambdas are converted back into protein half lives!! *
 ***************************************************************************/
void WriteParameters(char *name, EqParms *p, char *title, int ndigits);
/*** PrintParameters: prints an eqparms section with 'title' to the stream *
 *                    indicated by fp                                      *
 ***************************************************************************/
void PrintParameters(FILE *fp, EqParms *p, char *title, int ndigits);
/***** WriteDerivLog: write to solver log file *****************************/
void WriteDerivLog(char *deriv, int rule, int num_nuc);
/*** GetInteg: sets the array of pointers to the gradient                  *
 ***************************************************************************/
void GetInteg(double**integ);
/*** ReNewGrad: sets gradient to 0 (called before calculating the gradient)*
 ***************************************************************************/
void ReNewGrad();
/*** DLdqOrig: implements the kernel for Lagrangian gradient               *
 ***************************************************************************/
void DLdqOrig(double t, double *gradvec);
/*** DpsidtOrig: implements the equations for Lagrangian multiplies        *
 ***************************************************************************/
void DpsidtOrig(double *v, double t, double *vdot, int n);
/*** FreeParm: frees parameter struct accessed by ref **********************
 ***************************************************************************/
void FreeParm(EqParms*P);
/*** CopyLParm: like CopyParm (and it is used in gradient part)       ******
 *   but take structures by ref                                       ******
 ***************************************************************************/
void CopyRParm(EqParms*Pin,EqParms*Pout);
/*** CopyLParm: like CopyParm (and it is used instead) but doesn't    ******
 *   allocate anything                                                ******
 ***************************************************************************/
void CopyLParm();
/*** InstallParm: returns parameter struct with allocated arrays ***********
 ***************************************************************************/
EqParms* InstallParm();
/** GetGrad:  returns the grad struct to the caller; note that this        *
 *            function returns the gradient as it is added to parameters   *
 ***************************************************************************/
EqParms *GetGrad(void);
/*** SetCCycle: sets the static variable cycle to the cycle where we are ****
 ***************************************************************************/
void SetCCycle(int cc);
void InitZygoteGrad();
void SetNNucs(int n);
void Dvdt2Orig(double *v, double *w, double t, double *vdot, int n);
void Dvdt3Orig(double *v, double *w, double t, double *vdot, int n);

void Dvdt31stTelDirichlet(double *v1, double *v0, double t, double *vdot, int n);

void Dvdt31stTelNeumann(double *v1, double *v0, double t, double *vdot, int n);

void Dvdt32ndTelNeumann(double *v1, double *v0, double t, double *vdot, int n);

void Dvdt3TelMick(double *v1, double *v0, double t, double *vdot, int n);

void Dvdt31stJeffDirichlet(double *v1, double *v0, double t, double *vdot, int n);

void Dvdt31stJeffNeumann(double *v1, double *v0, double t, double *vdot, int n);

void Dvdt2Mob1(double *v, double *w, double t, double *vdot, int n);
void Dvdt2Bump(double *v, double *w, double t, double *vdot, int n);
void ReReadParameters(EqParms*l_parm, FILE *fp, char *section_title);
void ReReadParameters1(EqParms*l_parm, FILE *fp);
void P_Mutate(int gene);
void MutateWithBicoidDosage(char *g_type, double dosage);

void MMutate(char *g_type, double arg);
void RT_MMutate(int gene, double arg);
void R_MMutate(int gene, double arg);
void T_MMutate(int gene, double arg);

void DLdqOrig_g(double t, double *gradvec, int witch);

#ifdef __cplusplus
}
#endif

#endif /* _ZYGOTIC_H */
