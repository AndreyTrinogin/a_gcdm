/***************************************************************************
 *            score.h
 *
 *  Wed May 11 13:55:27 2005
 *  Copyright  2005  $USER
 *  kozlov@spbcas.ru
 ****************************************************************************/

/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef _SCORE_H
#define _SCORE_H

#ifdef __cplusplus
extern "C"
{
#endif

/*****************************************************************
 *                                                               *
 *   score.h                                                     *
 *                                                               *
 *****************************************************************
 *                                                               *
 *   written by JR, modified by Yoginho                          *
 *                                                               *
 *****************************************************************
 *                                                               *
 * This header file contains stuff that is needed for reading    *
 * and defining facts and limits and for scoring functions. The  *
 * functions declared here initialize or manipulate facts or     *
 * data time tables, read and initialize limits and penalty (if  *
 * needed and do the actual scoring.                             *
 *                                                               *
 * Should be included in code that does scoring (e.g. printscore *
 * or annealing code).                                           *
 *                                                               *
 *****************************************************************/

/* this def needed for old func. defs that refer to (* FILE) */
#include <stdio.h>
/* maternal.h should ALWAYS be included when score.h is used */
#include <maternal.h>
#define FORBIDDEN_MOVE    DBL_MAX      /* the biggest possible score, ever */

/*** STRUCTURES ************************************************************/

/* range struct for limits and such */

typedef struct Range {
	double   lower;
	double   upper;
} Range;

/***************************************************************************
 * The following describes the search space to the scoring function.       *
 * There are two ways to specify limits. Lambda, R, and d always get a     *
 * range for each variable---an upper & lower limit. Elements of T, h      *
 * and m that contribute to u in g(u) can be given a range.                *
 * This is probably the way to go as it definitely results in an           *
 * ergodic search space. However, as I write this code (10/95) we have     *
 * only one set of runs using this method. The alternative, which has      *
 * been mostly used, is to treat T, h and m with a penalty function on     *
 * u of the form:                                                          *
 *                                                                         *
 *           | 0 if exp(Lambda*\sum((T^ij)v_max_j)^2 + h^2 +               *
 *           |                                     (m_i mmax)^2 - 1 < 0    *
 * penalty = |                                                             *
 *           | exp(Lambda*\sum((T^ij)v_max_j)^2 + h^2 +(m_i mmax)^2        *
 *           |                                                otherwise    *
 *                                                                         *
 * where vmax_i is the highest level of gene i in the data, similarly      *
 * for mmax and bcd. This method can be non-ergodic if h and T or m are    *
 * being altered and variables change one at a time. In any case, one      *
 * scheme or the other must be used and the programmer must insure that    *
 * one or another set of pointers (see below) are NULL.                    *
 *                                                                         *
 * NOTE: - Lambda is NOT the same stuff as lambda in equation params or    *
 *         the lambda of the Lam schedule (don't get confused!)            *
 ***************************************************************************/

typedef struct {
	double    *pen_vec;    /* pointer to array that defines penalty function */
	Range     **Rlim;                      /* limits fore promoter strengths */
	Range     **Tlim;        /* limits for T matrix, NULL if pen_vec != NULL */
	Range     **Mlim;               /* limits for m, NULL if pen_vec != NULL */
	Range     **Elim;               /* limits for m, NULL if pen_vec != NULL */
	Range     **Plim;               /* limits for m, NULL if pen_vec != NULL */
	Range     **hlim;               /* limits for h, NULL if pen_vec != NULL */
	Range     **dlim;                 /* limit(s) for diffusion parameter(s) */
	Range     **lambdalim;           /* limits for lambda (prot. decay rate) */
	Range     **taulim;                 /* limit(s) for diffusion parameter(s) */
	Range     **mlim;                 /* limit(s) for diffusion parameter(s) */
	Range     **mmlim;
} SearchSpace;

/* Yousong's GutInfo struct for writing square diff guts */

typedef struct GutInfo {
	int      flag;                    /* for setting the gut flag in score.c */
	int      ndigits;                                /* gut output precision */
} GutInfo;

/* added by MacKoel */

/* FUNCTION PROTOTYPES *****************************************************/

/* Initialization Functions */

/*** InitScoring: intializes a) facts-related structs and TTimes and *******
 *                           b) parameter ranges for the Score function.   *
 ***************************************************************************/
void        InitScoring_bcd   (FILE *fp);
void        InitScoring   (FILE *fp);

/*** InitFacts: puts facts records into the appropriate DataTable. *********
 *              Returns a pointer to a DataTable, which in turn points to  *
 *              a sized array of DataRecords, one for each time step.      *
 ***************************************************************************/
void        InitFacts_bcd     (FILE *fp);
void        InitFacts     (FILE *fp);

void set_focus_score(int arg);
void        InitFocus     (FILE *fp);

/*** InitLimits: reads limits section from the data file into the struct ***
 *               limits, which is static to score.c. Then, it initiali-    *
 *               zes the penalty function if necessary.                    *
 *   NOTE:       lambda limits are stored as protein half lives in the     *
 *               data file and therefore get converted upon reading        *
 *               (compare to ReadParameters in zygotic.c for EqParms)      *
 ***************************************************************************/

void        InitLimits    (FILE *fp);

/*** InitPenalty: initializes vmax[] and mmax static to score.c; these *****
 *                variables are used to calculate the penalty function     *
 *                for scoring.                                             *
 *         NOTE: Should only get called, if penalty function is used       *
 ***************************************************************************/

void        InitPenalty   (FILE *fp);

/*** InitTTs: Initializes the time points for which we need model output ***
 *            i.e. the time points for which we have data.                 *
 *      NOTE: we do not allow data at t=0 but it still gets included into  *
 *            TTs                                                          *
 ***************************************************************************/

void        InitTTs       (void);

/*** InitStepsize: the only thing this function does is making step *******
 *                 static to score.c so the Score function can use it.    *
 **************************************************************************/

void InitStepsize(double step, double accuracy, FILE *slog, char* infile);
/* Actual Scoring Functions */

/*** Score: as the name says, score runs the simulation, gets a solution   *
 *          and then compares it to the data using the Eval least squares  *
 *          function.                                                      *
 *   NOTE:  both InitZygote and InitScoring have to be called first!       *
 ***************************************************************************/

double      Score_bcd(void);
double      Score(void);
double      ScoreABS_bcd(void);
double      ScoreABS(void);
double CheckSearchSpace_bcd();
double CheckSearchSpace();
double EvalABS(NArrPtr*answer,char *genotype);
double eval_colmn(NArrPtr*answer, char *genotype, int colmn);
double focus_colmn(NArrPtr*answer, char *genotype, int colmn);
double** eval_table(NArrPtr*answer, char *genotype);
/*** Eval: scores the summed squared differences between equation solution *
 *         and data. Because the times for states written to the Solution  *
 *         structure are read out of the data file itself, we do not check *
 *         for consistency of times in this function---all times with data *
 *         will be in the table, but the table may also contain additional *
 *         times.                                                          *
 ***************************************************************************/

double      Eval_bcd(NArrPtr*answer, char *genotype);
double      Eval(NArrPtr*answer, char *genotype);

double getHalfMax(double *arr, double startIndex, double finishIndex, int length);
double interpTwoPoints(double y1, double y2, double x1, double x2, double h);
double numToPercent(int num, double startIndex, double finishIndex, int length);

/*** Scoregut functions */

/* SetGuts: sets the gut info in score.c for printing out guts *************
 ***************************************************************************/

void SetGuts (int srsflag, int ndigits);

/*** GutEval: this is the same as Eval, i.e it calculates the summed squa- *
 *            red differences between equation solution and data, with the *
 *            addition that individual squared differences between data-   *
 *            points are written to STDOUT in the unfold output format     *
 ***************************************************************************/

double GutEval(NArrPtr*Solution, char *genotype);
/* Functions that return info about score.c-specific stuff */

/*** GetTTimes: this function returns the times for which there's data *****
 *              for a given genotype                                       *
 *     CAUTION: InitTTs has to be called first!                            *
 ***************************************************************************/

DArrPtr*GetTTimes     (char *genotype);

/*** GetLimits:  returns a pointer to the static limits struct in score.c **
 *     CAUTION:  InitScoring must be called first!                         *
 ***************************************************************************/

SearchSpace *GetLimits    (void);
SearchSpace *GetDetailedLimits(void);
void AddDetailedLimits(SearchSpace *limits);

/*** GetPenalty: calculates penalty from static limits, vmax and mmax ******
 *      CAUTION: InitPenalty must be called first!                         *
 ***************************************************************************/

double      GetPenalty_bcd    (void);
double      GetPenalty    (void);

/*** GetLimits:  returns the number of data points (static to score.c) *****
 *     CAUTION:  InitScoring must be called first!                         *
 ***************************************************************************/

int         GetNDatapoints(void);
/* A function for converting penalty to explicit limits */

/*** Penalty2Limits: uses the inverse function of g(u) to calculate upper **
 *                   and lower limits for T, m and h using the penalty     *
 *                   lambda parameter; these limits can only be an appro-  *
 *                   ximation, since all T, m and h are added up to yield  *
 *                   u for g(u); we try to compensate for this summation   *
 *                   dividing the limits by sqrt(n); this function then    *
 *                   sets the penalty vector to NULL and supplies explicit *
 *                   limits for T, m and h, which can be used for scram-   *
 *                   bling parameters and such                             *
 ***************************************************************************/

void Penalty2Limits(SearchSpace *limits);
void Penalty2Limits2(SearchSpace *limits);
/* Functions used to read stuff into structs */

/*** ReadLimits: reads the limits section of a data file and returns the  **
 *               approriate SearchSpace struct to the calling function     *
 ***************************************************************************/

SearchSpace *ReadLimits   (FILE *fp);

SearchSpace *ReReadLimits   (FILE *fp, char*section);

/*** List2Facts: takes a Dlist and returns the corresponding DataTable *****
 *               structure we use for facts data.                          *
 ***************************************************************************/

DataTable   *List2Facts_bcd   (Dlist *inlist, int *nsize);
DataTable   *List2Facts   (Dlist *inlist, int *nsize);

/*** STRUCTURES added by MacKoel *******************************************/

/* FUNCTION PROTOTYPES *****************************************************/

/* Initialization Functions */

/*** InitScoring2: transforms parameters first time                  *******
 * and allocates static structures                                         *
 ***************************************************************************/
void        InitScoring2();
void InitFlies(FILE *fp);

/* calculates Lagrangians and gradient by calling functions in integrate */
void PsiGrad_bcd();
void PsiGrad();
void PsiGrad_idx_bcd(int index);
void PsiGrad_idx(int index);
void FactsFree();

/* AddPenGrad: adds penalty to gradient*/
void AddPenGrad_bcd();
void AddPenGrad();

DataRecord*Jump(char*genotype,double time_point);
/* PrintSurfs: outputs approximation variables */
void DiffJump();
void WriteLimits(char *name, SearchSpace *p, char *title, int ndigits);
void PrintLimits(FILE *fp, SearchSpace *p, char *title, int ndigits);
void Scoring_setSolverName(char*name);
void Scoring_initAttr(double error, double time, double step);
double ScoreAttr();
void Scoring_getAttrBuffers(double *d, int *n);
int GetNDatapointsAttr();
double ScoringAttr();
void WriteScoringLog(double score);
void ScoringSetLogOptions(char*name,int iter);
double*GetSteadyConc(int i);
double SteadyScore();
void SteadyError();
void InitSteadyScoring2();
void InitSteadyScoring(FILE *fp);
void SteadyGrad();
void SteadyGrad_idx();
int GetSteadyDatapoints();

void InitDerivSolution();
void InitDerivFacts_bcd(FILE *fp);
void InitDerivFacts(FILE *fp);
void InitDerivScore_bcd(FILE *fp);
void InitDerivScore(FILE *fp);
double DerivScore_bcd();
double DerivScore();
void SolveDeriv(NArrPtr*facts, char *genotype, NArrPtr*Solution);
NArrPtr*Facts2Solution(DataTable *facts);
double EvalDeriv(NArrPtr *deriv, char *genotype);
void SearchSpaceDelete(SearchSpace*p);
int gcmd_get_facts(int gindex, double curr_time, double*v, int n);
void set_deriv(Derivative2 arg);
void set_deriv0(Derivative arg);
double *gcdm_get_hb_bounds_diff();
double *gcdm_get_score();
int *gcdm_get_ndp();
double gcdm_get_penalty();
double gcdm_get_penalty2();
void gcdm_score_set_solver(int rhs, char*name);
void gcdm_score_init_flies(FILE *fp);
double gcdm_score_score_bcd(void);
double gcdm_score_score(void);
void gcdm_score_set_hints(double dosage, int zero_bias, int rhs);
void init_scoring_window(FILE*fp, int ndivs);
int check_scoring_window(int lineage);
void gcdm_score_init_bcd(FILE *fp);
void gcdm_score_init(FILE *fp);
double gcdm_score_eval_deriv(NArrPtr*answer, char *genotype);
void gcdm_score_init_eval_bcd(char *type);
void gcdm_score_init_eval(char *type);
double gcdm_score_eval_freq(NArrPtr*answer, char *genotype);
double gcdm_score_eval_freq2d_bcd(NArrPtr*answer, char *genotype);
double gcdm_score_eval_freq2d(NArrPtr*answer, char *genotype);
void init_scoring_subtype_bcd(FILE*fp);
void init_scoring_subtype(FILE*fp);
double gcdm_score_eval_lsq(NArrPtr*answer, char *genotype);
void gcdm_score_convert_facts(DataTable *facts, int normalize);
void gcdm_score_convert_facts_sqrt(DataTable *facts, int normalize);
double gcdm_score_eval_vst_bcd(NArrPtr*answer, char *genotype);
double gcdm_score_eval_vst(NArrPtr*answer, char *genotype);

double wPGP(NArrPtr*answer, char *genotype);
double wPGP_colmn(NArrPtr*answer, char *genotype, int colmn);
double** wPGP_table(NArrPtr*answer, char *genotype, int *ftsize);
void gcdm_scores_bcd(double*chisq, double*wpgp,	double*penalty);
void gcdm_scores(double*chisq, double*wpgp,	double*penalty);
void gcdm_scores_colmn_bcd(double*chisq, double*wpgp, double*penalty);
void gcdm_scores_colmn(double*chisq, double*wpgp, double*penalty);
void gcdm_scores_table(double***chisq, double***wpgp, double*penalty, int *ftsize);

double gcdm_score_penalty_chisq_bcd(void);
double gcdm_score_penalty_chisq(void);
double gcdm_score_penalty_lsq(void);
double gcdm_score_penalty_abs(void);
void init_scoring_subtype_lsq_bcd (FILE*fp);
void init_scoring_subtype_lsq (FILE*fp);
double gcdm_score_penalty_dfacts(void);
double gcdm_score_penalty_penalty(void);
double gcdm_score_grad_score_bcd(void);
double gcdm_score_grad_score(void);
void init_scoring_subtype_dfacts (FILE*fp);
double gcdm_score_check_limits();
double gcdm_score_check_detailed_limits();
void gcdm_score_add_dfacts(DataTable *facts);

void init_scoring_subtype_vst (FILE*fp);

double gcdm_score_score_1(void);

typedef struct ScoreTargetDef {
	int index;
	double weight;
	double rank;
	char*name;
	double (*f)();
} ScoreTargetDef;

typedef struct ScoreTarget {
	int size;
	ScoreTargetDef*def;
} ScoreTarget;

ScoreTarget*ReadScoreTarget(FILE*fp);
ScoreTarget*get_score_target();
void InitScoreTarget_bcd(FILE *fp);
void InitScoreTarget(FILE *fp);
double gcdm_score_score_targeted();
void gcdm_score_calc_g_thing(int ndigits, int mode);

double gcdm_chaos_calc_etime(double*data, int gindex);
double gcdm_score_eval_chaos(NArrPtr*answer, char *genotype);
void gcdm_score_init_chaos ( FILE*fp );
ChaosData*ReadChaosData(FILE *f, char*section);
int gcdm_chaos_interpret_op(double op1, double var, double const1);

void set_need_time( double arg );
void set_unfold(char* arg, char *arg2, int arg3, int NOUNFOLD);
int check_time_ignore( double now );
void init_time_ignore(FILE*fp);

void init_gene_ignore(FILE*fp);

void gcdm_score_grad();

void gcdm_score_calc_g_num_bcd(int ndigits, int mode, double derstep);
void gcdm_score_calc_g_num(int ndigits, int mode, double derstep);
#ifdef LIBSVM
#include <libsvm/svm.h>
#elif FANN
#include <floatfann.h>
#endif
#ifdef LIBSVM
	double gcdm_score_libsvm(struct svm_model*model, double *prob_estimates);
#elif FANN
	fann_type *gcdm_score_fann(struct fann *ann);
#endif
#ifdef __cplusplus
}
#endif

#endif /* _SCORE_H */
