/***************************************************************************
                          IntegralSolve.c  -  description
                             -------------------
    begin                : ��� ��� 5 2004
    copyright            : (C) 2004 by Konstantin Kozlov
    email                : kostya@spbcas.ru
 ***************************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <limits.h>
#include <float.h>
#ifdef ICC
#include <mathimf.h>
#else
#include <math.h>
#endif

#include <IntegralSolve.h>
#include <SolutionTimeInterval.h>


void V_IntSimpson(int n,double**integ,SolutionTimeInterval*range,V_IntKernel kernel,double stephint,double accuracy)
{
 int step_counter;
 int nsteps;
 double msteps;
 double delta;
 double rstep;
 double r;
 double*vdot;
 int kounter;

 delta = range->time_finish - range->time_start;                 /* how far do we have to propagate? */

 if ( delta == 0 ) return;

 msteps = floor(fabs(delta)/stephint + 0.5);       /* see comment on stephint above */

 if ( (int)msteps % 2 != 0 ) msteps+=1.0;      /* we'll have to do at least two step */

 rstep = delta / msteps;                  /* real stepsize calculated here */
 nsteps = (int)(msteps/2.);                                  /* int number of steps */

 vdot=(double*)calloc(n,sizeof(double));

 r = range->time_start;

 (*kernel)(r,vdot);

 for ( kounter = 0; kounter < n; kounter++) {
     *(integ[kounter]) += vdot[kounter];
    }
 r += rstep;

 for(step_counter=1;step_counter<nsteps;step_counter++)
  {
     (*kernel)(r,vdot);

     for ( kounter = 0; kounter < n; kounter++) {
	 *(integ[kounter]) += 4*vdot[kounter];
        }
   r += rstep;
    (*kernel)(r,vdot);

     for ( kounter = 0; kounter < n; kounter++) {
         *(integ[kounter]) += 2*vdot[kounter];
        }
     r += rstep;
  }

 (*kernel)(r,vdot);

 for ( kounter = 0; kounter < n; kounter++) {
     *(integ[kounter]) += 4*vdot[kounter];
    }
 r = range->time_finish;

 (*kernel)(r,vdot);

 rstep /= 3.;

 for ( kounter = 0; kounter < n; kounter++) {
     *(integ[kounter]) += vdot[kounter];
     *(integ[kounter]) *= rstep;
    }

 free(vdot);
}
