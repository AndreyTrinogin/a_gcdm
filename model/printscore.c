/******************************************************************
 *                                                                *
 *   printscore.c                                                 *
 *                                                                *
 ******************************************************************
 *                                                                *
 *   written by JR, modified by Yoginho                           *
 *   -g option by Yousong Wang, Feb 2002                          *
 *   -a option by Marcel Wolf, Apr 2002                           *
 *                                                                *
 ******************************************************************
 *                                                                *
 * printscore.c contains main() for the printscore utility.       *
 *                                                                *
 * printscore prints score and penalty, as well as the root mean  *
 * square (RMS) of a gene circuit to STDOUT                       *
 *                                                                *
 * See the dataformatX.X file for further details on the current  *
 * data file format (X.X stands for the current code version).    *
 *                                                                *
 ******************************************************************/

#ifdef ICC
#include <mathimf.h>
#else
#include <math.h>
#endif
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>                                          /* for getopt */

#include <error.h>
#include <ODESolve.h>
#include <maternal.h>
#include <score.h>
#include <zygotic.h>
#include <integrate.h>
#include <tweak.h>
#ifdef LIBSVM
#include <libsvm/svm.h>
#elif FANN
#include <floatfann.h>
#endif

/*** Constants *************************************************************/

#define  OPTS      ":A:a:BdDE:f:F:g:GHhi:j:JnoO:pRrs:S:tT:vV:x:X:Yz:Z"  /* command line option string */


/*** Help, usage and version messages **************************************/

static const char usage[]  =

"Usage: gcdm_printscore [-a <accuracy>] [-D] [-f <float_prec>] [-g <g(u)>] [-G]\n"
"                  [-h] [-i <stepsize>] [-o] [-p] [-s <solver>] [-v]\n"
"                  [-x <sect_title>]\n"
"                  <datafile>\n";

static const char help[]   =

"Usage: gcdm_printscore [options] <datafile>\n\n"

"Arguments:\n"
"  <datafile>          data file for which we evaluate score and RMS\n\n"

"Options:\n"
"  -a <accuracy>       solver accuracy for adaptive stepsize ODE solvers\n"
"  -D                  debugging mode, prints all kinds of debugging info\n"
"  -E <index of input> switch off external input, can be given multiple times for several inputs\n"
"  -f <float_prec>     float precision of output is <float_prec>\n"
"  -F <index of gene>  switch off diffusion for this gene, can be given multiple times for several genes\n"
"  -g <g(u)>           chooses g(u): e = exp, h = hvs, s = sqrt, t = tanh\n"
"  -G                  gut mode: prints squared diffs for all datapoints\n"
"  -h                  prints this help message\n"
"  -i <stepsize>       sets ODE solver stepsize (in minutes)\n"
"  -o                  use oldstyle cell division times (3 div only)\n"
"  -p                  prints penalty in addition to score and RMS\n"
"  -s <solver>         selects solver\n"
"  -S <cost func>      selects quality functional: chisq (default), abs or deriv\n"
"  -v                  print version and compilation date\n"
"  -x <sect_title>     uses equation paramters from section <sect_title>\n\n"
"  -X <equations>      selects equations\n\n"
"  -z <gast_time>      set custom gastrulation time (max. 10'000 (min));\n"
"                      custom gastrulation MUST be later than the normal one\n"

"Please report bugs to <kozlov@spbcas.ru>. Thank you!\n";

static const char verstring[] =

"%s version %s\n"
"compiled by:      %s\n"
"         on:      %s\n"
"      using:      %s\n"
"      flags:      %s\n"
"       date:      %s at %s\n";

static int functional_flag = 0;
static int extra_file_flag = 0;
static int focus_score = 0;

/*** Main program **********************************************************/

int main(int argc, char **argv)
{
	int           c;                   /* used to parse command line options */
	FILE          *infile;                     /* pointer to input data file */
	char          *dumpfile;               /* name of debugging model output */
	FILE          *dumpptr;          /* pointer for dumping raw model output */
	char          *slogfile;                      /* name of solver log file */
	FILE          *slog;                          /* solver log file pointer */
/* the follwoing few variables are read as arguments to command line opts  */
	int           ndigits     = 12;     /* precision of output, default = 12 */
	int           gutndigits  = 6;      /* precision for gut output, def = 6 */
	int           penaltyflag = 0;              /* flag for printing penalty */
	int           boarderflag = 0;              /* flag for compare boarders */
	int           ndataflag = 0;              /* flag for printing penalty */
	int           rmsflag     = 1;     /* flag for printing root mean square */
	int           gutflag     = 0;         /* flag for root square diff guts */
	int           detailflag     = 0;         /* flag for root square diff guts */
	int           targetedflag     = 0;
	int           svmflag     = 0;
	double        stepsize    = 1.;                   /* stepsize for solver */
	double        accuracy    = 0.001;                /* accuracy for solver */
	char            *timefile = NULL;                  /* file for -j option */
	char            *svmfile = NULL;                  /* file for -j option */
	char          *section_title;                  /* parameter section name */
/* two format strings */
	char          *format;                           /* output format string */
	char          *precision;   /* precision string for output format string */
/* output values */
	double        chisq       = 0.; /* sum of squared differences for output */
	double        penalty     = 0.;                  /* variable for penalty */
	double        rms         = 0.;                /* root mean square (RMS) */
	double        ms          = 0.;             /* mean square (= rms * rms) */
	double time;
	int           ndp         = 0;                   /* number of datapoints */
	int UNFOLD = 0;
	int NOUNFOLD = 0;
 /* the following lines define a pointers to:                               */
/*            - pd:    dvdt function, currently only DvdtOrig in zygotic.c */
/*            - pj:    Jacobian function, in zygotic.c                     */
/*                                                                         */
/* NOTE: ps (solver) is declared as global in integrate.h                  */

/*  void (*pd)(double *, double, double *, int, int);
  void (*pj)(double, double *, double *, double **, int);*/
/* we need only solver name */
	char *solvername, *solvername0, *solver;
	Derivative2 pd = Dvdt2Orig;
/* external declarations for command line option parsing (unistd.h) */
	extern char   *optarg;                   /* command line option argument */
	extern int    optind;              /* pointer to current element of argv */
	extern int    optopt;             /* contain option character upon error */
	int loop_kounter;
	short isCustom = 0;
/* following part sets default values for deriv, Jacobian and solver funcs */
	int rhs, zero_bias;
	double dosage;
	int i, j;
	int e_mutate = 0;
	int *e_mutate_genes = NULL;
	int d_mutate = 0;
	int *d_mutate_genes = NULL;
	solvername = (char *)calloc(MAX_RECORD, sizeof(char));
	solvername = strcpy(solvername, "rkso");
	solvername0 = (char *)calloc(MAX_RECORD, sizeof(char));
	solvername0 = strcpy(solvername0, "bs");
	rhs = 1;
	dosage = 2.0;
	zero_bias = 0;
	solver = NULL;
	section_title = (char *)calloc(MAX_RECORD, sizeof(char));
	section_title = strcpy(section_title, "eqparms");  /* default is eqparms */
/* following part parses command line for options and their arguments      */
	optarg = NULL;
	while ( (c = getopt(argc, argv, OPTS)) != -1 )
		switch (c) {
			case 'a':
				accuracy = atof(optarg);
				if ( accuracy <= 0 )
					error("printscore: accuracy (%g) is too small", accuracy);
			break;
			case 'A':
				if( !(strcmp(optarg, "msq")) )
					functional_flag = 0;
				else if ( !(strcmp(optarg, "xtr")))
					functional_flag = 2;
				else if ( !(strcmp(optarg, "Xtr")))
					functional_flag = 4;
				else if ( !(strcmp(optarg, "abs")))
					functional_flag = 3;
				else if ( !(strcmp(optarg, "ari"))) //new
					functional_flag = 5; 			//new
				else if ( !(strcmp(optarg, "tab")))
					functional_flag = 1;
				else
					error("printscore: %s is an invalid flag", optarg);
			break;
			case 'B':                                 /* -D runs in debugging mode */
				NOUNFOLD = 1;
			break;
			case 'd':                                 /* -D runs in debugging mode */
				detailflag = 1;
			break;
			case 'D':                                 /* -D runs in debugging mode */
				debug = 1;
			break;
			case 'E':
				e_mutate_genes = (int*)realloc(e_mutate_genes, ( e_mutate + 1 ) * sizeof(int));
				e_mutate_genes[e_mutate] = atoi(optarg);
				e_mutate++;
			break;
			case 'f':
				ndigits    = atoi(optarg);          /* -f determines float precision */
				gutndigits = atoi(optarg);
				if ( ndigits < 0 )
					error("printscore: what exactly would a negative precision be???");
				if ( ndigits > MAX_PRECISION )
					error("printscore: max. float precision is %d!", MAX_PRECISION);
			break;
			case 'F':
				d_mutate_genes = (int*)realloc(d_mutate_genes, ( d_mutate + 1 ) * sizeof(int));
				d_mutate_genes[d_mutate] = atoi(optarg);
				d_mutate++;
			break;
			case 'g':                                    /* -g choose g(u) function */
				if( !(strcmp(optarg, "s")) )
					gofu = Sqrt;
				else if ( !(strcmp(optarg, "t")))
					gofu = Tanh;
				else if ( !(strcmp(optarg, "e")))
					gofu = Exp;
				else if ( !(strcmp(optarg, "h")))
					gofu = Hvs;
				else if ( !(strcmp(optarg, "l")))
					gofu = Lin;
				else if ( !(strcmp(optarg, "n")))
					gofu = He;
				else
					error("printscore: %s is an invalid g(u), should be e, h, s, t or l", optarg);
			break;
			case 'G':                                              /* -G guts mode */
				gutflag = 1;
			break;
			case 'h':                                            /* -h help option */
				PrintMsg(help, 0);
			break;
			case 'H':
				integrate_set_zero_mrna(1);
			break;
			case 'i':                                      /* -i sets the stepsize */
				stepsize = atof(optarg);
				if ( stepsize < 0 )
					error("printscore: going backwards? (hint: check your -i)");
				if ( stepsize == 0 )
					error("printscore: going nowhere? (hint: check your -i)");
				if ( stepsize > MAX_STEPSIZE )
					error("printscore: stepsize %g too large (max. is %g)", stepsize, MAX_STEPSIZE);
			break;
			case 'j':
				timefile = (char *)calloc(MAX_RECORD, sizeof(char));
				timefile = strcpy(timefile, optarg);
				UNFOLD = 1;
			break;
			case 'J':             /* -J compare boarders */
				boarderflag = 1;
			break;
			case 'n':                                              /* -G guts mode */
				ndataflag = 1;
			break;
			case 'o':             /* -o sets old division style (ndivs = 3 only! ) */
				olddivstyle = 1;
			break;
			case 'O':
				if (!strcmp(optarg, "hyb")) {
					model = HYBRID;
				} else if (!strcmp(optarg, "simple")) {
					model = SIMPLE;
				} else if (!strcmp(optarg, "nomit")) {
					model = NOMITOSIS;
				} else if (!strcmp(optarg, "double")) {
					model = DOUBLING;
				} else if (!strcmp(optarg, "hed")) {
					model = HEDIRECT;
				} else if (!strcmp(optarg, "hes")) {
					model = HESRR;
				} else if (!strcmp(optarg, "heu")) {
					model = HEURR;
				} else if (!strcmp(optarg, "heq")) {
					model = HEQUENCHING;
				} else {
					error("printscore: invalid model (-O %s)", optarg);
				}
			break;
			case 'p':
				penaltyflag = 1;
			break;
			case 'R':
				focus_score = 1;
				set_focus_score (1);
			break;
			case 'r':
/*				error("printscore: -r is not supported anymore, use -g instead");*/
				set_use_external_inputs_only(1);
			break;
			case 's':
				solver = (char *)calloc(MAX_RECORD, sizeof(char));
				solver = strcpy(solver, optarg);
			break;
			case 'S':
				if (!strcmp(optarg, "deriv")) {
					gcdm_score_init_eval("deriv");
				} else if (!strcmp(optarg, "freq")) {
					gcdm_score_init_eval("freq");
				} else if (!strcmp(optarg, "freq2d")) {
					gcdm_score_init_eval("freq2d");
				} else if (!strcmp(optarg, "lsq")) {
					gcdm_score_init_eval("lsq");
				} else if (!strcmp(optarg, "vst")) {
					gcdm_score_init_eval("vst");
				} else if (!strcmp(optarg, "chisq")) {
					gcdm_score_init_eval("chisq");
				} else if (!strcmp(optarg, "abs")) {
					gcdm_score_init_eval("abs");
				} else if (!strcmp(optarg, "wpgp")) {
					gcdm_score_init_eval("wpgp");
				} else {
					error("printscore: invalid deriv (-S %s)", optarg);
				}
			break;
			case 't':
				targetedflag = 1;
			break;
			case 'T':
				time = atof(optarg);
				set_need_time(time);
			break;
			case 'v':                                  /* -v prints version number */
				fprintf(stderr, verstring, *argv, VERS, USR, MACHINE, COMPILER, FLAGS, __DATE__, __TIME__);
				exit(0);
			case 'V':
				svmfile = (char *)calloc(MAX_RECORD, sizeof(char));
				svmfile = strcpy(svmfile, optarg);
				svmflag = 1;
			break;
			case 'x':
				if ( (strcmp(optarg, "input")) && (strcmp(optarg, "eqparms")) && (strcmp(optarg, "tonameters")) && (strcmp(optarg, "toneqparms")) && (strcmp(optarg, "parameters")) )
					error("unfold: invalid section title (%s)", optarg);
				section_title = strcpy(section_title, optarg);
			break;
			case 'X':
				if (!strcmp(optarg, "orig2")) {
					rhs = 0;
					set_deriv(Dvdt2Orig);
				} else if (!strcmp(optarg, "orig3")) {
					rhs = 0;
					set_deriv(Dvdt3Orig);
				} else if (!strcmp(optarg, "orig31stDir")) {
					rhs = 0;
					set_deriv(Dvdt31stTelDirichlet);
            } else if (!strcmp(optarg, "orig31stNeu")) {
					rhs = 0;
					set_deriv(Dvdt31stTelNeumann);
				} else if (!strcmp(optarg, "orig32ndNeu")) {
					rhs = 0;
					set_deriv(Dvdt32ndTelNeumann);
				} else if (!strcmp(optarg, "orig3M")) {
					rhs = 0;
					set_deriv(Dvdt3TelMick);
            } else if (!strcmp(optarg, "orig31stJeffDir")) {
					rhs = 0;
					set_deriv(Dvdt31stJeffDirichlet);
            } else if (!strcmp(optarg, "orig31stJeffNeu")) {
					rhs = 0;
					set_deriv(Dvdt31stJeffNeumann);
				} else if (!strcmp(optarg, "bump")) {
					rhs = 0;
					set_deriv(Dvdt2Bump);
				} else if (!strcmp(optarg, "mob1")) {
					rhs = 0;
					set_deriv(Dvdt2Mob1);
				} else if (!strcmp(optarg, "orig")) {
					rhs = 1;
#ifdef XTRA
				} else if (!strcmp(optarg, "wmrna")) {
					rhs = 1;
					set_deriv0(DvwdtOrig);
				} else if (!strcmp(optarg, "dmrna")) {
					rhs = 1;
					set_deriv0(DdvwdtOrig);
					ODESolve_set_dde(gcdm_add_delayed);
				} else if (!strcmp(optarg, "ddmrna")) {
					rhs = 1;
					set_deriv0(DddvwdtOrig);
					ODESolve_set_dde(gcdm_add_delayed);
				} else if (!strcmp(optarg, "ddual")) {
					rhs = 1;
					set_deriv0(DddvwdtDual);
					ODESolve_set_dde(gcdm_add_delayed);
				} else if (!strcmp(optarg, "dbmrna")) {
					rhs = 1;
					set_deriv0(DddwwdtOrig);
					ODESolve_set_dde(gcdm_add_delayed);
#endif
				} else {
					error("printscore: invalid deriv (-X %s)", optarg);
				}
			break;
			case 'z':
				isCustom = 1;
				custom_gast = atof(optarg);
				if ( custom_gast < 0. )
					error("printscore: gastrulation time must be positive");
				if ( custom_gast > 10000. )
					error("printscore: gastrulation time must be smaller than 10'000");
			break;
			case 'Z':
				extra_file_flag = 1;
			break;
			case 'Y':
				extra_file_flag = 2;
			break;
			case ':':
				error("printscore: need an argument for option -%c", optopt);
			break;
			case '?':
			default:
				error("printscore: unrecognized option -%c", optopt);
		}
/* error check */
	if ( (argc - (optind - 1 )) < 2 )
		PrintMsg(usage, 1);
/* dynamic allocation of output format strings */
	precision = (char *)calloc(MAX_RECORD, sizeof(char));
	format    = (char *)calloc(MAX_RECORD, sizeof(char));
/* initialize guts */
	if ( gutflag )
		SetGuts(gutflag, gutndigits);
/* let's get started and open data file here */
	infile = fopen(argv[optind],"r");
	if ( !infile )
		file_error("printscore");
/* if debugging: initialize solver log file */
	if ( debug ) {
		slogfile = (char *)calloc(MAX_RECORD, sizeof(char));
		sprintf(slogfile, "%s.slog", argv[optind]);
		slog = fopen(slogfile, "w");              /* delete existing slog file */
		fclose(slog);
		slog = fopen(slogfile, "a");            /* now keep open for appending */
	}
/* Initialization code here: InitZygote() initializes everything needed    *
 * for running the model, InitScoring() everything for scoring; InitStep-  *
 * size sets solver stepsize and accuracy in score.c to pass it on to      *
 * Blastoderm                                                              */
	InitZygote(infile, section_title);
	if ( extra_file_flag == 1 ) {
		FILE*extra;
		EqParms* eq = GetParameters();
		extra = fopen(argv[optind + 1], "r");
		ReReadParameters(eq, extra, section_title);
		fclose(extra);
	}
	if ( extra_file_flag == 2 ) {
		FILE*extra;
		EqParms* eq = GetParameters();
		extra = fopen(argv[optind + 1], "r");
		ReReadParameters1(eq, extra);
		fclose(extra);
		//extra = fopen("testpar", "w");
		//PrintParameters(extra, eq, "input", 8);
		//fclose(extra);
	}
#ifdef XTRA
	gcdm_init_seq(infile);
#endif
	if ( e_mutate > 0 ) {
		for ( i = 0 ; i < e_mutate ; i++ ) {
			E_Mutate( e_mutate_genes[i] );
		}
	}
	if ( d_mutate > 0 ) {
		for ( i = 0 ; i < d_mutate ; i++ ) {
			D_Mutate( d_mutate_genes[i] );
		}
	}
	if ( rhs == 0 ) {
		if ( solver )
			gcdm_score_set_solver(0, solver);
		else
			gcdm_score_set_solver(0, solvername);
		gcdm_score_set_solver(1, solvername0);
	} else if ( rhs == 1 ) {
		gcdm_score_set_solver(0, solvername);
		if ( solver )
			gcdm_score_set_solver(1, solver);
		else
			gcdm_score_set_solver(1, solvername0);
	}
	gcdm_score_init(infile);
	gcdm_score_set_hints(dosage, zero_bias, rhs);
	InitStepsize(stepsize, accuracy, slog, argv[optind]);
	if ( UNFOLD ) {
		slogfile = (char *)calloc(MAX_RECORD, sizeof(char));
		if ( extra_file_flag == 1 ) {
			sprintf(slogfile, "%s.uof", argv[optind + 1]);
		} else {
			sprintf(slogfile, "%s.uof", argv[optind]);
		}
		set_unfold(slogfile, timefile, ndigits, NOUNFOLD);
	}
	if ( functional_flag == 0 ) {
/* Scoring happens here (and also printing of guts if -G) */
		chisq = gcdm_score_score();
	} else if ( functional_flag == 2 ) {
	    double wpgp;
		gcdm_scores(&chisq, &wpgp, &penalty);
        sprintf(precision, "%d", ndigits);
        format = strcpy(format, " chisq = %.");
        format = strcat(format, precision);
        format = strcat(format, "f");
        printf(format, chisq);
        format = strcpy(format, " wpgp = %.");
        format = strcat(format, precision);
        format = strcat(format, "f");
        printf(format, wpgp);
        format = strcpy(format, "     penalty = %.");
		format = strcat(format, precision);
		format = strcat(format, "f");
		printf(format, penalty);
        printf("\n");
        fclose(infile);
/* clean up before you go home...*/
        FreeZygote();
        free(precision);
        free(format);
        free(section_title);
        return 0;
	} else if ( functional_flag == 4 ) {
	    double wpgp;
		gcdm_scores(&chisq, &wpgp, &penalty);
        sprintf(precision, "%d", ndigits);
        format = strcpy(format, " chisq = %.");
        format = strcat(format, precision);
        format = strcat(format, "f");
        printf(format, chisq);
        format = strcpy(format, " wpgp = %.");
        format = strcat(format, precision);
        format = strcat(format, "f");
        printf(format, wpgp);
        format = strcpy(format, "     penalty = %.");
		format = strcat(format, precision);
		format = strcat(format, "f");
		printf(format, penalty);
		ndp = GetNDatapoints();
		ms  = chisq / (double)ndp;
		rms = sqrt(ms);
		format = strcpy(format, "     rms = %.");
		format = strcat(format, precision);
		format = strcat(format, "f");
		printf(format, rms);
		rms = ndp*log(ms);
		format = strcpy(format, "     aik-2k = %.");
		format = strcat(format, precision);
		format = strcat(format, "f");
		printf(format, rms);
		printf("\n");
        fclose(infile);
/* clean up before you go home...*/
        FreeZygote();
        free(precision);
        free(format);
        free(section_title);
        return 0;
	} else if ( functional_flag == 3 ) {
/* Scoring happens here (and also printing of guts if -G) */
		chisq = ScoreABS();
	}
	else if ( functional_flag == 5 ) {  //new
		double* wpgp; //new
		double* chisq; //new
		double *focus;
		chisq = (double*)calloc(defs->ngenes * nalleles, sizeof(double)); //new
		wpgp = (double*)calloc(defs->ngenes * nalleles, sizeof(double)); //new
		if (focus_score == 1) {
			focus = (double*)calloc(defs->ngenes * nalleles, sizeof(double)); //new
		}
		double chisq_sum;
		chisq_sum = 0;
		if (focus_score == 1) {
			gcdm_scores_colmn_focus(chisq, wpgp, focus, &penalty);
		} else {
			gcdm_scores_colmn(chisq, wpgp, &penalty);
		}
        sprintf(precision, "%d", ndigits);
        format = strcpy(format, " chisq [%d] = %.");
        format = strcat(format, precision);
        format = strcat(format, "f");
		format = strcat(format, " wpgp [%d] = %.");
        format = strcat(format, precision);
        format = strcat(format, "f");
		if (focus_score == 1) {
			format = strcat(format, " focus [%d] = %.");
        	format = strcat(format, precision);
        	format = strcat(format, "f");
		}
		format = strcat(format, "\n");

		for ( j = 0; j < nalleles; j++) {
			for ( i = 0; i < defs->ngenes; i++){  //new
				chisq_sum = chisq_sum + chisq[j * nalleles + i];
				if (focus_score == 1) {
					printf(format, j, chisq[j * defs->ngenes + i], j, wpgp[j * defs->ngenes + i], focus[j * defs->ngenes + i]);
				} else {
					printf(format, j, chisq[j * defs->ngenes + i], j, wpgp[j * defs->ngenes + i]);
				}
			}
		}

		format = strcpy(format, " chisq_sum = %.");
        format = strcat(format, precision);
        format = strcat(format, "f");
		format = strcat(format, "\n");
		printf(format,chisq_sum );


        format = strcpy(format, "     penalty = %.");
		format = strcat(format, precision);
		format = strcat(format, "f");
		printf(format, penalty);
		ndp = GetNDatapoints();


		ms  = chisq_sum / (double)ndp;  //new


		rms = sqrt(ms);
		format = strcpy(format, "     rms = %.");
		format = strcat(format, precision);
		format = strcat(format, "f");
		printf(format, rms);
		rms = ndp*log(ms);
		format = strcpy(format, "     aik-2k = %.");
		format = strcat(format, precision);
		format = strcat(format, "f\n");
		printf(format, rms);

		double scoreboarders = 0.0L;
		double * hb_bounds_diff = NULL;
		if ( boarderflag ){
			scoreboarders = gcdm_score_score_bcd();
			hb_bounds_diff = gcdm_get_hb_bounds_diff();
			format = strcpy(format, "     hb_bounds_diff = %.");
			format = strcat(format, precision);
			format = strcat(format, "f\n");
			for(i = 0; i < 5; i++){
				printf(format, hb_bounds_diff[i]);
			}
		}

		printf("\n");
        fclose(infile);
/* clean up before you go home...*/
        FreeZygote();
        free(precision);
        free(format);
        free(section_title);
        return 0;

	}		//new
	else if ( functional_flag == 1 ) {
		double*** wpgp; //new
		double*** chisq; //new
		chisq = (double***)calloc(nalleles, sizeof(double**));
		wpgp = (double***)calloc(nalleles, sizeof(double**));
		double chisq_sum, wpgp_sum;
		chisq_sum = 0;
		int ftsize, ftindex;
		gcdm_scores_table(chisq, wpgp, &penalty, &ftsize);
		if (!chisq[0]) return 0;
        sprintf(precision, "%d", ndigits);
        format = strcpy(format, " %.");
        format = strcat(format, precision);
        format = strcat(format, "f");

		for ( j = 0; j < nalleles; j++) {
			printf("chisq[%d]\n", j);
			for ( ftindex = 0; ftindex < ftsize; ftindex++) {
				for ( i = 0; i < defs->ngenes; i++){  //new
					chisq_sum += chisq[j][ftindex][i];
					printf(format, chisq[j][ftindex][i]);
				}
				printf("\n");
			}
			printf("wpgp[%d]\n", j);
			for ( ftindex = 0; ftindex < ftsize; ftindex++) {
				for ( i = 0; i < defs->ngenes; i++){  //new
					wpgp_sum += wpgp[j][ftindex][i];
					printf(format, wpgp[j][ftindex][i]);
				}
				printf("\n");
			}
		}

		format = strcpy(format, "chisq_sum=%.");
        format = strcat(format, precision);
        format = strcat(format, "f");
		printf(format,chisq_sum );

		format = strcpy(format, " wpgp_sum=%.");
        format = strcat(format, precision);
        format = strcat(format, "f");
		printf(format, wpgp_sum );

        format = strcpy(format, " penalty=%.");
		format = strcat(format, precision);
		format = strcat(format, "f");
		printf(format, penalty);
		ndp = GetNDatapoints();

		ms  = chisq_sum / (double)ndp;  //new

		rms = sqrt(ms);
		format = strcpy(format, " rms=%.");
		format = strcat(format, precision);
		format = strcat(format, "f");
		printf(format, rms);
		rms = ndp*log(ms);
		format = strcpy(format, " aik-2k=%.");
		format = strcat(format, precision);
		format = strcat(format, "f\n");
		printf(format, rms);

		double scoreboarders = 0.0L;
		double * hb_bounds_diff = NULL;
		if ( boarderflag ){
			scoreboarders = gcdm_score_score_bcd();
			hb_bounds_diff = gcdm_get_hb_bounds_diff();
			format = strcpy(format, "     hb_bounds_diff = %.");
			format = strcat(format, precision);
			format = strcat(format, "f\n");
			for(i = 0; i < 5; i++){
				printf(format, hb_bounds_diff[i]);
			}
		}

		if (svmflag == 1) {
#ifdef LIBSVM
			struct svm_model* model;
			double predict_label;
			if ((model = svm_load_model(svmfile)) == 0) {
				fprintf(stderr, "can't open model file %s\n", argv[optind + 2]);
				exit(1);
			}
			int svm_type = svm_get_svm_type(model);
			int nr_class = svm_get_nr_class(model);
			double *prob_estimates = NULL;
			int j;
			int *labels = (int *) malloc(nr_class * sizeof(int));
			svm_get_labels(model, labels);
			prob_estimates = (double *) malloc(nr_class * sizeof(double));
/*			fprintf(stdout, "labels");
			for(j = 0; j < nr_class; j++) {
				fprintf(stdout, " %d", labels[j]);
			}
			fprintf(stdout, "\n");*/
			free(labels);
			predict_label = gcdm_score_libsvm(model, prob_estimates);
			fprintf(stdout, "Label=%g P", predict_label);
			for(j = 0; j < nr_class; j++) {
				fprintf(stdout, " %g", prob_estimates[j]);
			}
			fprintf(stdout, "\n");

			free(prob_estimates);
#elif FANN
			fann_type *calc_out;
			struct fann *ann;
			if ((ann = fann_create_from_file(svmfile)) == 0) {
				fprintf(stderr, "can't open model file %s\n", argv[optind + 2]);
				exit(1);
			}
			calc_out = gcdm_score_fann(ann);
			format = strcpy(format, " ANNtest = %.");
			format = strcat(format, precision);
			format = strcat(format, "f\n");
			printf(format, 1 - calc_out[0]);
			fflush(stdout);
			printf("errno=%d\n", fann_get_errno((struct fann_error *)ann));
			fann_destroy(ann);
#else
			fprintf(stderr, "Neither LibSVM nor FANN were not linked!\n");
#endif
		}

		printf("\n");
        fclose(infile);
/* clean up before you go home...*/
        FreeZygote();
        free(precision);
        free(format);
        free(section_title);
        return 0;

	}
	if ( gutflag ) {              /* exit here if we've printed guts already */
		FreeZygote();
		free(precision);
		free(format);
		free(section_title);
		return 0;
	}
/* next few lines format & print the output to the appropriate precision   */
	sprintf(precision, "%d", ndigits);
	format = strcpy(format, " chisq = %.");
	format = strcat(format, precision);
	format = strcat(format, "f");
	printf(format, chisq);
	if ( penaltyflag ) {                 /* in case of -p, print penalty too */
		penalty = GetPenalty();
		if ( penalty == -1 ) {
			printf("    explicit limits !");
		} else {
			format = strcpy(format, "     penalty = %.");
			format = strcat(format, precision);
			format = strcat(format, "f");
			printf(format, penalty);
		}
	}
	if ( rmsflag ) {                                            /* print rms */
		ndp = GetNDatapoints();
		penalty = GetPenalty();
/*		ms  = (chisq - penalty) / (double)ndp;
		rms = sqrt(ms);*/
		ms  = chisq / (double)ndp;
		rms = sqrt(ms);
		format = strcpy(format, "     rms = %.");
		format = strcat(format, precision);
		format = strcat(format, "f");
		printf(format, rms);
	}
	if ( ndataflag ) {                                            /* print rms */
		format = strcpy(format, "     datapoints = %d");
		printf(format, ndp);
	}
	if ( detailflag ) {
		int k;
		int *ndps;
		double *scores;
		penalty = GetPenalty();
		ndps = gcdm_get_ndp();
		scores = gcdm_get_score();
		for ( k = 0; k < nalleles; k++ ) {
			printf("\nscore=%.16f    ndatapoint=%d    rms=%.16f", scores[k], ndps[k], sqrt(scores[k]/(double)ndps[k]));
		}
	}
	if ( targetedflag ) {
		int i;
		double value, f;
		value = 0;
/*		init_scoring_subtype_dfacts(infile);*/
		InitScoreTarget(infile);
		ScoreTarget*score_target = get_score_target();
		for ( i = 0; i < score_target->size; i++) {
			f = score_target->def[i].weight * score_target->def[i].f();
			value += f;
			printf("\n%d.score = %.16f", i, f);
		}
		printf("\nscore = %.16f", value);
	}
	printf("\n");
	fclose(infile);
/* clean up before you go home...*/
	FreeZygote();
	free(precision);
	free(format);
	free(section_title);
	return 0;
}

