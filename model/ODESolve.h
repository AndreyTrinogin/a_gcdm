/***************************************************************************
                          ODESolve.h  -  description
                             -------------------
    begin                :  5 2004
    copyright            : (C) 2004 by Konstantin Kozlov
    email                : kostya@spbcas.ru
 ***************************************************************************/
/*****************************************************************
 *                                                               *
 *   solvers.h                                                   *
 *                                                               *
 *****************************************************************
 *                                                               *
 * solvers.h contains the interface to the solver functions, i.e *
 * solver function prototypes and the p_deriv global which ser-  *
 * ves as a definition of the interface to the derivative func.  *
 *                                                               *
 *****************************************************************
 *                                                               *
 * NOTE: *ONLY* general solvers allowed here; they *MUST* comply *
 *       to the generic solver interface (see solvers.c for de-  *
 *       tails)                                                  *
 *                                                               *
 *****************************************************************/
#ifndef ODESOLVE_INCLUDED
#define ODESOLVE_INCLUDED

#include <float.h>
#ifdef ICC
#include <mathimf.h>
#else
#include <math.h>
#endif
#include <stdlib.h>
#include <stdio.h>
#include <ODESolutionPoly.h>

typedef void (*Derivative)(double *, double, double *, int);
typedef void (*Derivative2)(double *, double *, double, double *, int);
typedef void (*Jacobian)(double, double *, double *, double **, int);
typedef ODESolutionPoly* (*Solver)(double *vin, double *vout, double tin, double tout, double stephint, double accuracy, int n, FILE *slog);
typedef ODESolutionPoly* (*Solver2)(double *vin, double *win, double *vout, double *wout, double tin, double tout, double stephint, double accuracy, int n, FILE *slog);

typedef struct ODESolve {
  int nintervals;
  ODESolutionPoly**intervals;
  Derivative ekernel;
  Derivative2 ekernel2;
  Jacobian ikernel;
  Solver integrator;
  Solver2 integrator2;
  Solver integrator_attr;
} ODESolve;

ODESolve*ODESolve_new(char*name2, Derivative2 efunc2, char*name, Derivative efunc, Jacobian ifunc);
ODESolve*ODESolve_create(char*name, Derivative efunc, Jacobian ifunc);
ODESolve*ODESolve_create2(char*name, Derivative2 efunc);
ODESolutionPoly* ODESolve_Solve(ODESolve*p,double *vin, double *vout, double tin, double tout, double stephint, double accuracy, int n, FILE *slog);
ODESolutionPoly* ODESolve_Solve2(ODESolve*p,double *vin, double *win, double *vout, double *wout, double tin, double tout, double stephint, double accuracy, int n, FILE *slog);
void ODESolve_delete(ODESolve*p);
void ODESolve_SetIntegratorAttr(char*name, ODESolve*p);
void ODESolve_SetIntegrator(char*name, ODESolve*p);
void ODESolve_SetIntegrator2(char*name, ODESolve*p);
ODESolutionPoly* ODESolve_SolveAttr(ODESolve*p,double *vin, double *vout, double tin, double tout, double stephint, double accuracy, int n, FILE *slog);
void ODESolve_SetAttrFeatures(double error, double time, double step);
void ODESolve_GetActualAttrTime(double*time);
void ODESolve_SetAttrScoreBuffer(double*AttrScoreBuffer,int*AttrNDatapoitsBuffer);
void ODESolve_ResetScoreBuffer();
void ODESolve_FillScoreBuffer(double *vin, double *vout, int n);

/*** FUNCTION PROTOTYPES ***************************************************/

/*** Euler: propagates vin (of size n) from tin to tout by the Euler *******
 *          method; the result is returned by vout                         *
 ***************************************************************************/

ODESolutionPoly*Euler(double *vin, double *vout, double tin, double tout, double stephint, double accuracy, int n, FILE *slog);




/*** Meuler: propagates vin (of size n) from tin to tout by the Modified ***
 *           Euler method (this is NOT the midpoint method, see Rk2());    *
 *           the result is returned by vout                                *
 ***************************************************************************/

ODESolutionPoly*Meuler(double *vin, double *vout, double tin, double tout, double stephint, double accuracy, int n, FILE *slog);




/*** Heun: propagates vin (of size n) from tin to tout by Heun's method ****
 *         the result is returned by vout                                  *
 ***************************************************************************/

ODESolutionPoly*Heun(double *vin, double *vout, double tin, double tout, double stephint, double accuracy, int n, FILE *slog);




/*** Rk2: propagates vin (of size n) from tin to tout by the Midpoint or ***
 *        Second-Order Runge-Kutta method; the result is returned by vout  *
 ***************************************************************************/

ODESolutionPoly*Rk2(double *vin, double *vout, double tin, double tout, double stephint, double accuracy, int n, FILE *slog);




/*** Rk4: propagates vin (of size n) from tin to tout by the Fourth-Order **
 *        Runge-Kutta method; the result is returned by vout               *
 ***************************************************************************
 *                                                                         *
 * written by Joel Linton (somewhere around 1998)                          *
 * fixed and modified by Yoginho (somewhere around 2001)                   *
 *                                                                         *
 ***************************************************************************/

ODESolutionPoly*Rk4(double *vin, double *vout, double tin, double tout, double stephint, double accuracy, int n, FILE *slog);




/*** Rkck: propagates vin (of size n) from tin to tout by the Runge-Kutta **
 *         Cash-Karp method, which is an adaptive-stepsize Rk method; it   *
 *         uses a fifth-order Rk formula with an embedded forth-oder for-  *
 *         mula for calucalting the error; its result is returned by vout  *
 ***************************************************************************
 *                                                                         *
 * This solver was written by Marcel Wolf, Spring 2002.                    *
 *                                                                         *
 ***************************************************************************/

ODESolutionPoly*Rkck(double *vin, double *vout, double tin, double tout, double stephint, double accuracy, int n, FILE *slog);




/*** Rkf: propagates vin (of size n) from tin to tout by the Runge-Kutta ***
 *        Fehlberg method, which is a the original adaptive-stepsize Rk    *
 *        method (Cash-Karp is an improved version of this); it uses a     *
 *        fifth-order Rk formula with an embedded forth-oder formula for   *
 *        calucalting the error; its result is returned by vout            *
 ***************************************************************************
 *                                                                         *
 * This solver was written by Marcel Wolf, Spring 2002.                    *
 *                                                                         *
 ***************************************************************************/

ODESolutionPoly*Rkf(double *vin, double *vout, double tin, double tout, double stephint, double accuracy, int n, FILE *slog);




/***** Milne: propagates vin (of size n) from tin to tout by Milne-Simpson *
 *            which is a predictor-corrector method; the result is retur-  *
 *            ned by vout                                                  *
 ***************************************************************************
 *                                                                         *
 * This solver was implemented by Konstantin Koslov, Dec 2001/Jan 2002     *
 *                                                                         *
 ***************************************************************************
 *                                                                         *
 * THIS SOLVER SEEMS TO BE BUGGY FOR SOME REASON, DO NOT USE IT!!!!!       *
 *                                                                         *
 ***************************************************************************/

ODESolutionPoly*Milne(double *vin, double *vout, double tin, double tout, double stephint, double accuracy, int n, FILE *slog);




/***** Adams: propagates vin (of size n) from tin to tout by Adams-Moulton *
 *            which is an implicit predictor-corrector method of second    *
 *            order; the result is returned by vout                        *
 ***************************************************************************
 *                                                                         *
 * This solver was implemented by Konstantin Koslov, Spring 2002           *
 * Slightly modified by Manu, July 2002                                    *
 *                                                                         *
 ***************************************************************************/

ODESolutionPoly*Adams(double *vin, double *vout, double tin, double tout, double stephint, double accuracy, int n, FILE *slog);




/***** BuST: propagates v(t) from t1 to t2 by Bulirsch-Stoer; this method **
 *           uses Richardson extrapolation to estimate v's at a hypothe-   *
 *           tical stepsize of 0; the extrapolation also yields an error   *
 *           estimate, which is used to adapt stepsize and change the or-  *
 *           der of the method as required; the result is returned by vout *
 ***************************************************************************
 *                                                                         *
 * This solver was implemented by Manu, July 2002                          *
 *                                                                         *
 ***************************************************************************/

ODESolutionPoly*BuSt(double *vin, double *vout, double tin, double tout, double stephint, double accuracy, int n, FILE *slog);


ODESolutionPoly*BuStAttr(double *vin, double *vout, double tin, double tout,
	 double stephint, double accuracy, int n, FILE *slog);

void ODESolve_GetActualAttrTime(double *time);

/***** BaDe: propagates v(t) from t1 to t2 by Bader-Deuflhard; this method *
 *           uses Richardson extrapolation to estimate v's at a hypothe-   *
 *           tical stepsize of 0; the extrapolation also yields an error   *
 *           estimate, which is used to adapt stepsize and change the or-  *
 *           der of the method as required; the result is returned by vout *
 ***************************************************************************
 *                                                                         *
 * This solver was implemented by Yogi, based on BuSt, Aug 2002            *
 *                                                                         *
 ***************************************************************************/

ODESolutionPoly*BaDe(double *vin, double *vout, double tin, double tout, double stephint, double accuracy, int n, FILE *slog);


ODESolutionPoly*Rkso(double *vin, double *win, double *vout, double *wout, double tin, double tout,
	 double stephint, double accuracy, int n, FILE *slog);

ODESolutionPoly*exFinDif(double *vin, double *win, double *vout, double *wout, double tin, double tout,
	 double stephint, double accuracy, int n, FILE *slog);
/***** WriteSolvLog: write to solver log file ******************************/

void WriteSolvLog(char *solver, double tin, double tout, double h, int n,
		  int nderivs, FILE *slog);

/***************************************************************************
                          DDESolve.c  -  description
                             -------------------
    begin                : Jan 2013
    copyright            : (C) 2013 by Muzhichenko Vladimir
    email                : vmuzhichenko@gmail.com
 ***************************************************************************/

typedef void (*addHistory)(double *v, double t);

void ODESolve_set_dde(addHistory p);

void ODESolve_set_dir(int p);

ODESolutionPoly*dde11(double *vin, double *vout,
                     double tin, double tout, double stephint, double accuracy,
                     int n, FILE *slog);

ODESolutionPoly*dde21(double *vin, double *vout,
                    double tin, double tout, double stephint, double accuracy,
                    int n, FILE *slog);

ODESolutionPoly*dde21d(double *vin, double *vout,
                    double tin, double tout, double stephint, double accuracy,
                    int n, FILE *slog);

ODESolutionPoly*dde41(double *vin, double *vout,
                    double tin, double tout, double stephint, double accuracy,
                    int n, FILE *slog);

#endif
