make distclean
autoreconf -fi
# && libtoolize --force && intltoolize --force
export LD_LIBRARY_PATH=$HOME/lib:$LD_LIBRARY_PATH
#CFLAGS="-g -DXTRA -I$HOME/include/libcbgs -DLIBSVM" LIBS="-L$HOME/lib -lcbgs -lsvm" ./configure --prefix=$HOME
CFLAGS="-g -DXTRA -I$HOME/include/libcbgs -DFANN" LIBS="-L$HOME/lib -lcbgs -lfann" ./configure --prefix=$HOME
make && make install
